var barsData = {
  'Jan': 55,
  'Feb': 99,
  'Mar': 101,
  'Apr': 80,
  'May': 26,
  'Jun': 55,
  'Jul': 55,
  'Aug': 99,
  'Sep': 101,
  'Oct': 80,
  'Nov': 26,
  'Dec': 55
};

var barsOptions = {
  'height': 300,
  'title': '',
  'width': 400,
  'fixPadding': 10,
  'barFont': [0, 12, "bold"],
  'labelFont': [0, 10, 0]
};

graphite(barsData, barsOptions, bars);
