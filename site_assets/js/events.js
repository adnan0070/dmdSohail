
function addImpressionSubscriptionAll(obj){

	for(var i =0; i < obj.length; i++){
		ga('ec:addImpression', {
			'id':obj[i]['pack_code'],                  
			'name': obj[i]['pack_title'],
			'category': obj[i]['pack_time_period'],       
			'list': 'Search Results',                           
		});
	}

	ga('send', 'pageview');

}

// Called when a link to a product is clicked
function addSubscriptionClick(obj,method){

	ga('ec:addProduct', {   
		'id': obj['pack_code'],                  
		'name': obj['pack_title'],
		'category': obj['pack_time_period'],              
	});

	ga('ec:setAction', 'click', {    
		'list': 'Search Results'     
	});
	ga('send', 'event', 'UX', 'click', 'Results', {
		hitCallback: function() {
			document.location = 'https://www.digitalmediadeliveries.com/user/'+method+'/'+obj['pack_code'];
		}//https://www.digitalmediadeliveries.com/
	});
	sendSubscriptionDetail(obj);
	subscriptionAddToCart(obj);
	

}

function sendSubscriptionDetail(obj){

	ga('ec:addProduct', {
		'id': obj['pack_code'],                  
		'name': obj['pack_title'],
		'category': obj['pack_time_period'],  
	});

	ga('ec:setAction', 'detail');
	ga('send', 'pageview');
}


//Add to Cart
function subscriptionAddToCart(obj){

	ga('ec:addProduct', {
		'id': obj['pack_code'],                  
		'name': obj['pack_title'],
		'category': obj['pack_time_period'],  
		'price':  obj['pack_price'],
		'quantity': 1
	});
	ga('ec:setAction', 'add');
	ga('send', 'event', 'UX', 'click', 'add to cart'); 
}


function subscriptionCheckOut(obj) {

	ga('ec:addProduct', {
		'id': obj['pack_code'],                  
		'name': obj['pack_title'],
		'category': obj['pack_time_period'],  
		'price':  obj['pack_price'],
		'quantity': 1
	});
	
	ga('ec:setAction','checkout', {
		'step': 1,             
		'option': 'Visa'      
	});
	ga('send', 'pageview');    

	onSubscriptionComplete();

}

function onSubscriptionComplete(){
	ga('ec:setAction', 'checkout_option', {
		'step': 2,
		'option': 'VISA'
	});

	ga('send', 'event', 'Checkout', 'Option', {
		hitCallback: function() {
			console.log('Checkout');
		}
	});
}


function onSubscriptionPurchase(obj){
	ga('ecommerce:addTransaction', {
		'id': obj['pack_code'],                   
		'affiliation': 'Subscription',  
		'revenue': obj['pack_price'],               
		'shipping': '0',                  
		'tax': '0',
		'currency': 'USD'                    
	});

	ga('ecommerce:addItem', {
		'id': obj['pack_code'],                     
		'name': obj['pack_title'],    
		'sku': 'DD23444',                 
		'category': obj['pack_time_period'],         
		'price': obj['pack_price'],                 
		'quantity': '1',
		'currency': 'USD'               
	});

	ga('ecommerce:send');
	// ga('ec:addProduct', {
	// 	'id': obj['pack_code'],                  
	// 	'name': obj['pack_title'],
	// 	'category': obj['pack_time_period'],  
	// 	'price':  obj['pack_price'],
	// 	'quantity': 1
	// });


	// ga('ec:setAction', 'purchase', {
	// 	'id': obj['transaction_id'],
	// 	'affiliation': 'none',
	// 	'revenue': obj['pack_price'],
	// 	'tax': '0',
	// });
	// console.log(obj);

	// ga('send', 'pageview');  
}






// Product Events

function addImpressionProductAll(obj,store){

	for(var i =0; i < obj.length; i++){
		ga('ec:addImpression', {
			'id':obj[i]['product_code'],                  
			'name': obj[i]['product_name'],
			'category': 'store/'+store,       
			'list': 'Search Results',                           
		});
	}

	ga('send', 'pageview');

}

// Called when a link to a product is clicked
function addProductClick(obj,store){

	ga('ec:addProduct', {   
		'id': obj['product_code'],                  
		'name': obj[i]['product_name'],
		'category': 'store/'+store,              
	});

	ga('ec:setAction', 'click', {    
		'list': 'Search Results'     
	});

	ga('send', 'event', 'UX', 'click', 'Results');
	sendProductDetail(obj,store);
	productAddToCart(obj,store);
	

}

function sendProductDetail(obj,store){

	ga('ec:addProduct', {
		'id': obj['product_code'],                  
		'name': obj['product_name'],
		'category': 'store/'+store,  
	});

	ga('ec:setAction', 'detail');
	ga('send', 'pageview');
}


//Add to Cart
function productAddToCart(obj,store){

	ga('ec:addProduct', {
		'id': obj['product_code'],                  
		'name': obj['product_name'],
		'category': 'store/'+store, 
		'price':  obj['product_price'],
		'quantity': 1
	});
	ga('ec:setAction', 'add');
	ga('send', 'event', 'UX', 'click', 'add to cart'); 
}


function productCheckOut(obj) {

	ga('ec:addProduct', {
		'id': obj['pack_code'],                  
		'name': obj['pack_title'],
		'category': obj['pack_time_period'],  
		'price':  obj['pack_price'],
		'quantity': 1
	});
	
	ga('ec:setAction','checkout', {
		'step': 1,             
		'option': 'Visa'      
	});
	ga('send', 'pageview');    

	onSubscriptionComplete();

}

function onProductComplete(){
	ga('ec:setAction', 'checkout_option', {
		'step': 1,
		'option': 'VISA'
	});

	ga('send', 'event', 'Checkout', 'Option', {
		hitCallback: function() {
			console.log('Checkout');
		}
	});
}


function onProductPurchase(obj){
	ga('ec:addProduct', {
		'id': obj['pack_code'],                  
		'name': obj['pack_title'],
		'category': obj['pack_time_period'],  
		'price':  obj['pack_price'],
		'quantity': 1
	});


	ga('ec:setAction', 'purchase', {
		'id': obj['transaction_id'],
		'affiliation': 'none',
		'revenue': obj['pack_price'],
		'tax': '0',
	});

	ga('send', 'pageview');  
}






