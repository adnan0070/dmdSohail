<?php
class Product_model extends CI_model
{


    public function create_product($product)
    {

        $user_id = $this->session->userdata('user_id');
        $store_id = $this->session->userdata('store_id');
        $product['store_id'] = $store_id;
        $this->db->insert('dmd_store_products', $product);

    }

    public function create_product_file($product)
    {

        $this->db->insert('dmd_delivery_for_pro', $product);

    }
    public function update($product, $pro_id)
    {
 
        $this->db->where('product_code', $pro_id);
        $res =  $this->db->update('dmd_store_products', $product);
        return $res;
    }
    public function update_product($product, $pro_id)
    {
         //print_r($product);die();
        $this->db->where('product_code', $pro_id);
        $update = $this->db->update('dmd_store_products', $product);
        if ($update) {
            $this->session->set_flashdata('success_msg',
                'your product successfully updated!!');
            redirect('product');
        } else {
            $this->session->set_flashdata('error_msg', 'try again something going wrong');
            redirect('product/' . $pro_id);
        }
    }


    public function product_check($slug)
    {

        $this->db->select('*');
        $this->db->from('dmd_store_products');
        $this->db->where('product_slug', $slug);
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }

    }
    public function filter_purchase($data = array())
    {
        if ($data) {
            $filterdata = array();
            if(isset($data['from']) && isset($data['to'])) {
                $dates = "'" . date('Y-m-d', strtotime($data["from"])) . "' AND '" . date('Y-m-d', strtotime($data["to"])) ."'";
                unset($data['from']);
                unset($data['to']);
            }
            if(isset($data['period'])){
                $period = $data['period'];
                unset($data['period']);
            }
            foreach ($data as $key => $val) {
                if ($key == 'dmd_customers%shipping_country'){ $key = 'dmd_orders%shipping_country'; }
                $key1 = str_replace("%", ".", $key);
                $filterdata[$key1] = $val;
            }

            $store_id = $this->session->userdata('store_id');
            $user_id = $this->session->userdata('user_id');
            $filterdata['dmd_orders.user_id'] = $user_id;
            /*$this->db->select('*,dmd_orders.id as order_id,dmd_orders.created_date as order_date');
            $this->db->from('dmd_orders');*/
            $this->db->join('dmd_order_items', 'dmd_order_items.order_id = dmd_orders.id',
                'left');
            $this->db->join('dmd_customers', 'dmd_customers.id = dmd_orders.customer_id',
                'left');
            $this->db->select('*,dmd_orders.id as order_id,dmd_orders.created_date as order_date,dmd_orders.shipping_country as purchase_country');
            $this->db->order_by("`dmd_orders`.`created_date`", "DESC");
            
            if(isset($dates)) {
                //$filterdata['dmd_orders.created_date'] = " BETWEEN " . $dates;
                $this->db->where("`dmd_orders`.`created_date` BETWEEN " . $dates);
            }
            if(isset($period)){
                $this->db->where("DATE_FORMAT(`dmd_orders`.`created_date`, '%Y-%m-%d') >= DATE_SUB(CURDATE(), INTERVAL $period DAY)"); 
            }
            $query = $this->db->get_where('dmd_orders', $filterdata);
            //echo $this->db->last_query();
            if ($query->num_rows() > 0) {
                $result = $query->result_array();

                //$this->db->select('sum(dmd_orders.grand_total) as gtotal');
                //            $this->db->join('dmd_order_items', 'dmd_order_items.order_id = dmd_orders.id',
                //                'left');
                //            $query2 = $this->db->get_where('dmd_orders',$filterdata);
                //            $result['grand_sum'] = $query2->row_array();

                $sum = 0;
                foreach ($result as $purchase) {
                    $sum += $purchase['grand_total'];
                    ?>
                    <tr>
						<!-- <td class="paragraph-text-sm-grey"><?php echo $purchase['order_id']; ?></td>
						<td class="paragraph-text-sm-grey"><?php echo $purchase['order_date']; ?></td>
						<td class="paragraph-text-sm-grey">$<?php echo $purchase['grand_total']; ?></td>
						<td class="paragraph-text-sm-grey"><?php echo $purchase['status']; ?></td>
						<td class="paragraph-text-sm-grey"><?php echo $purchase['deliver_status']; ?></td>
						<td class="paragraph-text-sm-grey"><?php echo $purchase['email']; ?></td> -->

                        <td class="paragraph-text-sm-grey"><?php echo $purchase['order_number']; ?></td>
                        <td class="paragraph-text-sm-grey"><?php echo date('Y-m-d', strtotime($purchase['order_date'])); ?></td>
                        <td class="paragraph-text-sm-grey">$<?php echo $purchase['grand_total']; ?></td>
                        <td class="paragraph-text-sm-grey"><?php echo $purchase['status']; ?></td>
                        <td class="paragraph-text-sm-grey"><?php echo $purchase['f_name'] . ' ' . $purchase['l_name']; ?></td>
                        <td class="paragraph-text-sm-grey"><?php echo $purchase['email']; ?></td>
                        <td class="paragraph-text-sm-grey"><?php echo $purchase['purchase_country']; ?></td>
                        




                    </tr>
                    <?php
                }
                ?>
                <input type="hidden" value="<?php echo count($result); ?>" id="purchase_count" />
                <input type="hidden" value="<?php echo $sum; ?>" id="getto" />
                <?php

                
            } else {
                echo 'No records found!!';
            }


        } else {
            return false;
        }
    }

    public function getAllpurchase($id = "")
    {
        $user_id = $this->session->userdata('user_id');
        $store_id = $this->session->userdata('store_id');
        
        $this->db->select('*,dmd_orders.id as order_id,dmd_orders.created_date as order_date,dmd_orders.shipping_country as purchase_country');
        $this->db->from('dmd_orders');
        $this->db->join('dmd_order_items', 'dmd_order_items.order_id = dmd_orders.id', 'left');
        $this->db->join('dmd_customers', 'dmd_customers.id = dmd_orders.customer_id', 'left');
        $this->db->where('dmd_orders.user_id', $user_id);
        if ($id != "") {
            $this->db->where('dmd_order_items.product_id', $id);
        }
        $this->db->order_by('order_date','DESC');
        $query = $this->db->get();
        $result = array();
        if ($query->num_rows() > 0) {
            $result['result'] = $query->result_array();
            $this->db->where('dmd_orders.user_id', $user_id);
            $this->db->select('sum(dmd_orders.grand_total) as gtotal');
            $this->db->from('dmd_orders');
            $this->db->join('dmd_order_items', 'dmd_order_items.order_id = dmd_orders.id', 'left');
            $this->db->where('dmd_orders.status', 'paid');
            if ($id != "") {
                $this->db->where('dmd_order_items.product_id', $id);
            }
            $query2 = $this->db->get();
            $result['grand_sum'] = $query2->row_array();
            $result['products'] = $this->getOrderproduct($id);
            $result['pid'] = $id;
            return $result;
        } else {
            $result['result'] = array();
            return array();
        }
    }

    public function getOrderproduct($id="")
    {

        $user_id = $this->session->userdata('user_id');
        $store_id = $this->session->userdata('store_id');
        $this->db->select('*');
        $this->db->from('dmd_order_items');
        $this->db->join('dmd_store_products', 'dmd_order_items.product_id = dmd_store_products.product_code',
            'left');
        $this->db->where('user_id', $user_id);
        if($id!=""){
         $this->db->where('dmd_store_products.product_code', $id);
     }
     $this->db->group_by('dmd_order_items.product_id');
     $query = $this->db->get();
        //echo $this->db->last_query();
     if ($query->num_rows() > 0) {
        return $query->result_array();
    } else {
        return array();
    }

}
public function getAll($id = "")
{

    $user_role = $this->session->userdata('user_role');

    $user_id = $this->session->userdata('user_id');
    $store_id = $this->session->userdata('store_id');

    $this->db->select('*');
    $this->db->from('dmd_store_products');
    if($user_role!='2'){
        $this->db->where('user_id', $user_id);
        $this->db->where('store_id', $store_id);
    }
    if ($id != "") {
        $this->db->where('product_code', $id);
    }
    $query = $this->db->get();
        //echo $this->db->last_query();
    if ($query->num_rows() > 0) {
        return $query->result_array();
    } else {
        return array();
    }

}
function get_product($code)
{
   $this->db->select('*');
   $this->db->from('dmd_store_products');
   $this->db->where('product_code', $code);
   $this->db->where('is_feature', 1);                      
   $query = $this->db->get();
   return $query->row_array();
}

function getAllFeaturedProducts($store_id)
{
    $store_id = $this->session->userdata('store_id');
    $this->db->select('*');
    $this->db->from('dmd_store_products');
    $this->db->where('store_id', $store_id);
    $this->db->where('is_feature', 1);  
    $this->db->where('product_visibility', 'live');                      
    $query = $this->db->get();
    return $query->result_array();
}

function get_un_featured($store_id)
{
   $store_id = $this->session->userdata('store_id');
   $this->db->select('*');
   $this->db->from('dmd_store_products');
   $this->db->where('store_id', $store_id);
   $this->db->where('is_feature', 0);                      
   $query = $this->db->get();
   return $query->result_array();
}

function update_fetured_product($product, $product_code=""){
    if($product_code!=""){
        $this->db->where('product_code',$product_code);   
        $this->db->update('dmd_store_products', $product);
    }
}
function getAllmail($id = "", $obj = "")
{
    if ($id != "") {
        if ($obj == "edit") {
            $store_id = $this->session->userdata('store_id');
            $this->db->select('*');
            $this->db->from('dmd_email_delivery_pro');
            $this->db->where('store_id', $store_id);

            $this->db->where('id', $id);
        } else {
            $store_id = $this->session->userdata('store_id');
            $this->db->select('*');
            $this->db->from('dmd_email_delivery_pro');
            $this->db->where('store_id', $store_id);

            $this->db->where('product_id', $id);
        }
        $query = $this->db->get();
            //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
}
function getAllfile($id = "", $obj = "")
{
    if ($id != "") {
        if ($obj == "edit") {
            $store_id = $this->session->userdata('store_id');
            $this->db->select('*');
            $this->db->from('dmd_delivery_for_pro');
            $this->db->where('store_id', $store_id);

            $this->db->where('id', $id);
        } else {
            $store_id = $this->session->userdata('store_id');
            $this->db->select('*');
            $this->db->from('dmd_delivery_for_pro');
            $this->db->where('store_id', $store_id);

            $this->db->where('product_id', $id);
        }
        $query = $this->db->get();
            //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
}
function getAllpurchasefile($id = "", $obj = "")
{
    if ($id != "") {

                //$store_id = $this->session->userdata('store_id');
        $this->db->select('*');
        $this->db->from('dmd_order_files');
        $this->db->where('item_id', $id);

        $query = $this->db->get();
            //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
}
public function get_StoreProducts($store_id = "")
{
        /*$this->db->select('*');
        $this->db->from('dmd_store_products');
        $this->db->where('store_id', $store_id);
        $this->db->where('product_visibility', 'live');
        $this->db->limit(3);*/
        
        //$query = $this->db->query("SELECT pro.*,dmd_coupon.discount as co_discount,dmd_coupon.max_use,dmd_coupon.expire_date FROM `dmd_store_products` as pro left join dmd_coupon on(dmd_coupon.id=pro.enable_coupon) WHERE pro.`store_id` = '" .  $store_id . "' AND pro.`product_visibility` = 'live'  LIMIT 3 OFFSET 0");
        $query = $this->db->query("SELECT pro.*,dmd_coupon.discount as co_discount,dmd_coupon.max_use,dmd_coupon.expire_date,dmd_coupon.active_date FROM `dmd_store_products` as pro left join dmd_coupon on(dmd_coupon.id=pro.enable_coupon) WHERE pro.`store_id` = '" .  $store_id . "' AND pro.`product_visibility` = 'live'  LIMIT 9 OFFSET 0");
        //$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }

    }
    public function get_SingleProducts($product_id = "")
    {
        $this->db->select('*');
        $this->db->from('dmd_store_products');
        $this->db->where('id', $product_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }

    }
    public function get_SingleProduct_Bycode($product_code = "")
    {
        $this->db->select('*');
        $this->db->from('dmd_store_products');
        $this->db->where('product_code', $product_code);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }

    }

    public function get_RandProduct($store_id = '', $product_code='')
    {
        $this->db->select('*');
        $this->db->from('dmd_store_products');
        $this->db->where('store_id', $store_id);
        // $this->db->where('product_code !=', $product_code);
        $this->db->order_by('rand() ');
        $this->db->limit(2);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }

    }
    public function get_CrossProduct($store_id = '', $product_code,$cross_sell_products)
    {
        $this->db->select('*');
        $this->db->from('dmd_store_products');
        $this->db->where_in('id', $cross_sell_products);
        $this->db->where('store_id', $store_id);
        $this->db->where('product_code !=', $product_code);
        $this->db->order_by('rand() ');
        $this->db->limit(3);
        $query = $this->db->get();
        // echo  $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }

    }

    public function get_upsell_Product($store_id = '', $product_code,$up_sell_products)
    {
        $this->db->select('*');
        $this->db->from('dmd_store_products');
        $this->db->where_in('id', $up_sell_products);
        $this->db->where('store_id', $store_id);
        $this->db->where('product_code !=', $product_code);
        $this->db->order_by('rand() ');
        $this->db->limit(3);
        $query = $this->db->get();
        // echo  $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }

    }

    public function export_filter_purchases($data = array())
    {
        if($data){
            if(isset($data['from']) && isset($data['to'])) {
                $dates = "'" . date('Y-m-d', strtotime($data["from"])) . "' AND '" . date('Y-m-d', strtotime($data["to"])) ."'";
                unset($data['from']);
                unset($data['to']);
            }

            foreach ($data as $key => $value) {
                $data['`dmd_orders`.' . $key] = $value;
                unset($data[$key]);
            }


            $this->db->select(
                '`dmd_customers`.`f_name` AS f_name,
                `dmd_customers`.`l_name` AS l_name,
                `dmd_customers`.`email` AS email,
                `dmd_customers`.`off_sale` AS off_sale,
                SUM(`dmd_orders`.`grand_total`) AS total_sale'
            );
            $this->db->from('dmd_customers');
            $this->db->join('dmd_orders', '`dmd_customers`.`id` = `dmd_orders`.`customer_id`');
            $this->db->where($data);
            
            if(isset($dates)) {
                $this->db->where("`dmd_orders`.`created_date` BETWEEN " . $dates);
            }

            $this->db->group_by("`dmd_orders`.`customer_id`");
            $query = $this->db->get();
            //$qu = $this->db->get_compiled_select();
            //return $qu;

            $result = $query->result_array();
            return $result;
        }else{
            return false;
        }
    }

    public function load_product($offset="0", $limit="9", $search="")
    {
        $offset = 'OFFSET ' . $offset;
        $limit = 'LIMIT ' . $limit;
        
        $liked = '';
        if(!empty($search)){
            $liked = " AND (pro.`product_name` LIKE '%$search%' OR pro.`product_short_description` LIKE '%$search%')";
            $offset = '';
            $limit = '';
        }

        $store_id = $this->session->userdata('store_id');

        $q = "SELECT pro.*,`dmd_coupon`.`discount` AS co_discount,
        `dmd_coupon`.`max_use`, `dmd_coupon`.`expire_date`, `dmd_coupon`.`active_date` 
        FROM `dmd_store_products` AS pro 
        LEFT JOIN `dmd_coupon` ON(`dmd_coupon`.`id` = pro.`enable_coupon`) 
        WHERE pro.`store_id` = '" .  $store_id . "' AND pro.`product_visibility` = 'live' $liked $limit $offset";

        $query = $this->db->query($q);
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $abc["count"] = $query->num_rows();
            $abc["result"] = $query->result_array();
            return $abc;
        } else {
            return array();
        }
    }

    public function total_store_product_count()
    {
        $store_id = $this->session->userdata('store_id');
        $q = "SELECT pro.*,dmd_coupon.discount as co_discount,dmd_coupon.max_use,dmd_coupon.expire_date FROM `dmd_store_products` as pro left join dmd_coupon on(dmd_coupon.id=pro.enable_coupon) WHERE pro.`store_id` = '" .  $store_id . "' AND pro.`product_visibility` = 'live'";

        $query = $this->db->query($q);
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return '0';
        }
    }


    public function getAll_CustomerOrders($customer_id)
    {
        $store_id = $this->session->userdata('store_id');
        $this->db->select('*');
        $this->db->from('dmd_orders');
        $this->db->where('customer_id', $customer_id);
        $this->db->where('store_id', $store_id);
        $this->db->order_by("created_date", "DESC");
        $query = $this->db->get(); 
       // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }

    }
    public function getAll_OrdersItems_Byid($order_id)
    { 
        $this->db->select('*');
        $this->db->from('dmd_order_items');
        $this->db->where('order_id', $order_id);
        $query = $this->db->get();
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
    public function checkoutComplete($id)
    {
        $this->db->select('SUM(product_quantity) AS checkout_qty, SUM(total_price) AS checkout_price');
        $this->db->from('dmd_order_items');
        $this->db->where('product_id', $id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function checkoutBegin($id)
    {
        // get order id list against given product code
        $this->db->select('order_id');
        $this->db->from('dmd_order_items');
        $this->db->where('product_id', $id);
        $query = $this->db->get();
        $result = $query->result_array();

        // get order status against given order id list
        $order_id_list = array();
        if($result){
            $order_id_list = array();
            foreach ($result as $key => $value) {
                $order_id_list[] = $value["order_id"];
            }
        }

        // get order id from order table against given order id list with ststus pending
        if($order_id_list){
            $this->db->select('id');
            $this->db->from('dmd_orders');
            $this->db->where_in('id', $order_id_list);
            $this->db->where('status', 'pending');
            $query = $this->db->get();
            $result_order = $query->result_array();
            return count($result_order);
        }else{
            return "";
        }
    }

    public function range_price_product($data)
    {
        $store_id = $this->session->userdata('store_id');
        // $q = "SELECT pro.*,dmd_coupon.discount as co_discount,dmd_coupon.max_use,dmd_coupon.expire_date FROM `dmd_store_products` as pro left join dmd_coupon on(dmd_coupon.id=pro.enable_coupon) WHERE pro.`store_id` = '" .  $store_id . "' AND pro.`product_visibility` = 'live' $liked LIMIT $limit OFFSET $offset";
        
        $this->db->select("pro.*,dmd_coupon.discount as co_discount,dmd_coupon.max_use,dmd_coupon.expire_date");
        $this->db->from("dmd_store_products pro");
        $this->db->join('dmd_coupon', 'dmd_coupon.id = pro.enable_coupon', 'left');
        $this->db->where("pro.store_id", $store_id);
        $this->db->where("pro.product_visibility", "live");
        $this->db->where("pro.product_price BETWEEN " . $data["min"] . " AND " . $data["max"]);

        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function get_is_download($product_code)
    {
        $store_id = $this->session->userdata('store_id');
        $this->db->select('is_download');
        $this->db->from('dmd_store_products');
        $this->db->where('product_code', $product_code);
        $this->db->where('store_id', $store_id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return array();
        }

    }

    public function store_payment_method_check()
    {
        $store_id = $this->session->userdata('store_id');
        $user_id = $this->get_user_id_by_store_id($store_id);
        $this->db->select('*');
        $this->db->from('payment_methods');
        $this->db->where('status', 1);
        $this->db->where('user_id', $user_id['user_id']);
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return array();
        }
    }

    public function get_user_id_by_store_id($store_id)
    {

        $this->db->select('user_id');
        $this->db->from('dmd_stores');
        $this->db->where('id', $store_id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return array();
        }
    }

}// end model
