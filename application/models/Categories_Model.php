<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Categories_Model extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function categoryList($id="")
	{
		$this->db->select('*');
		$this->db->from('categories');
		// $this->db->order_by('title', 'ASC');
		$query = $this->db->get();
		if($query->num_rows() >0){
			return $query->result_array();
		}
		return false;
	}
	function getRows($id=""){
        if(!empty($id)){
            $query = $this->db->get_where('categories', array('id' => $id));
            return $query->row_array();
        }else{
            $query = $this->db->get('categories');
            return $query->result_array();
        }
    }
	public function save_category($data=array())
	{
		if(!empty($data)){
		//print_r($data);die();
			$this->db->insert('categories',$data);
			$inserted_id = $this->db->insert_id();
			return $inserted_id;
		}else{
		//print_r($data);die("fals");
			return false;
		}
	}
	 public function update_Category($id, $data){
       $this->db->where('id', $id);
       $this->db->set($data);
        $return =  $this->db->update('categories', $data);
        return $return;

    }
     public function delete_Category($id){
        if(!empty($id)){
            $this->db->where('categories.id',$id);
            return $this->db->delete('categories');
        }

    }
}