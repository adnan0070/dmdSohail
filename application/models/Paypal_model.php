<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

use Omnipay\Omnipay;
use Omnipay\Common\CreditCard;

class Paypal_model extends CI_model
{
	public function chargePayment($data=array()){
		// Setup payment gateway
		$gateway = Omnipay::create('PayPal_Pro');
		$gateway->setUsername('zaintestpersonal@gmail.com');
		$gateway->setPassword('Zain@master123');
		$gateway->setDeveloperMode(true);
		//$gateway->testMode(true);

		// Send purchase request
		$response = $gateway->purchase($data)->send();

		// Process response
		if ($response->isSuccessful()) {
		    // Payment was successful
		    //return $response;
		    return $response->getTransactionReference();
		} elseif ($response->isRedirect()) {
		    // Redirect to offsite payment gateway
		    $response->redirect();
		}else {
		    // Payment failed
		    return $response->getMessage();
		}
	}

	public function createCustomerProfile($data=array())
	{
		$gateway = Omnipay::create('PayPal_Pro');
		$gateway->setUsername('zaintestpersonal@gmail.com');
		$gateway->setPassword('Zain@master123');
		$gateway->setDeveloperMode(true);
		
		//return $gateway;
        $request = $gateway->createCard($data);
        $response = $request->send();

        if ($response->isSuccessful()) {
        	return $response->getCardReference();
		}else{
			return $response->getMessage();
		}
	}
}