<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Customization_Model extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_packages()
	{
		$this->db->select('*');
		$this->db->from('customization_packages');
		// $this->db->order_by('title', 'ASC');
		$query = $this->db->get()->result_array();
		return $query;
	}

  public function get($id){
    $query = $this->db->get_where('customization_packages', array('id' => $id));
    return  $query->row_array();
  }
  public function get_product($id){
    $query = $this->db->get_where('dmd_store_products', array('id' => $id));
    return  $query->row_array();
  }

  public function insert_package($params)
  {
    $this->db->insert('customization_packages',$params);
    return $this->db->insert_id();
  }

  public function insert_request($params)
  {
    $this->db->insert('customization_request`',$params);
    return $this->db->insert_id();
  }

  public function update($id, $params){
   $this->db->where('id', $id);
   return $this->db->update('customization_packages', $params);
 }

 public function update_request($id, $params){
   $this->db->where('id', $id);
   return $this->db->update('customization_request', $params);
 }

 public function update_product($id, $params){
   $this->db->where('id', $id);
   return $this->db->update('dmd_store_products', $params);
 }

 public function update_store($id, $params){
   $this->db->where('id', $id);
   return $this->db->update('dmd_stores', $params);
 }

 public function delete($id)
 {
  $delete = $this->db->query("DELETE FROM customization_packages WHERE id='$id'");      
  return $delete ? true : false;
}


public function get_all_requests()
{
  $this->db->select('*');
  $this->db->from('customization_request');
  $this->db->order_by('id', 'DESC');
  $query = $this->db->get()->result_array();
  return $query;
}


public function getblogs($id=""){
  if(!empty($id)){
    $query = $this->db->get_where('articles', array('id' => $id));
    return $query->row_array();
  }
  else{
   $this->db->select('articles.*, categories.title as category');
   $this->db->from('articles');
   $this->db->join('categories','articles.Category_id = categories.id');
   return $this->db->get()->result_array();
 }
}


public function get_user($id){
  $query = $this->db->get_where('dmd_users', array('id' => $id));
  return  $query->row_array();
}
public function get_store($id){
  $query = $this->db->get_where('dmd_stores', array('id' => $id));
  return  $query->row_array();
}

public function get_request($id){
  $query = $this->db->get_where('customization_request', array('id' => $id));
  return  $query->row_array();
}

function get_group_products($arrayID){
  $this->db->select('*');
  $this->db->from('dmd_store_products');
  $this->db->where_in('id', $arrayID);
  $query = $this->db->get();
  return $query->result_array();
}
}