<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Article_Model extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function article_List($id="")
	{
		$this->db->select('*');
		$this->db->from('articles');
		// $this->db->order_by('title', 'ASC');
		$query = $this->db->get();
		if($query->num_rows() >0){
			return $query->result_array();
		}
		return false;
	}
	function getRows($id=""){
        if(!empty($id)){
            $query = $this->db->get_where('articles', array('id' => $id));
            return $query->row_array();
        }else{
        	$this->db->select('articles.*, categories.title as category');
			$this->db->from('articles');
			$this->db->join('categories','articles.Category_id = categories.id');
            return $this->db->get()->result_array();
        }
    }
	public function save_article($data)
	{
		$this->db->insert('articles',$data);
		$inserted_id = $this->db->insert_id();
		return $inserted_id;
	}
	 public function update_article($id, $data){
       $this->db->where('id', $id);
       //$this->db->set($data);
        $return =  $this->db->update('articles', $data);
        return $return;

    }
    public function delete_article($id){
        $delete = $this->db->query("DELETE FROM articles WHERE id='$id'");      
        return $delete ? true : false;
    }
     public function upload_files($path, $title, $image,$input_name)
    {
        $this->load->library('image_lib');
        $date = date('dmYHis');
        $ext = strrchr($image, ".");
        
        $fileName = $title.'_'.$date.''.$ext;
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|gif|png',
            'max_size' => 5000,
            'max_width' => 50000,
            'max_height' => 50000,
            'file_name' => $fileName,
            'overwrite'     => 1,                       
        );
        // print_r($filename);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload($input_name)) 
        {
            $this->upload->data();
            $config2['image_library'] = 'gd2';
            $config2['source_image'] = $path.'/'.$fileName;;
            $config2['new_image'] = './upload/'.$fileName;
            //$config2['create_thumb'] = TRUE;
            $config2['maintain_ratio'] = TRUE;
            $config2['width'] = 210;
            $config2['height'] = 70;
            $this->image_lib->clear();
            $this->image_lib->initialize($config2);
            $this->image_lib->resize();
            return $this->upload->data();
        }
        else 
        {
            return false;
        } 
    }

    function getblogs($id=""){
        if(!empty($id)){
            $query = $this->db->get_where('articles', array('id' => $id));
            return $query->row_array();
        }else{
        	$this->db->select('articles.*, categories.title as category');
			$this->db->from('articles');
			$this->db->join('categories','articles.Category_id = categories.id');
            return $this->db->get()->result_array();
        }
    }
}