<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

use Omnipay\Omnipay;
use Omnipay\AuthorizeNet\Message\CIMResponse;

class Authorizenet_model extends CI_model
{

	protected $ApiLoginId = '3txw8Tsr8YKk';
	protected $TransactionKey = '5uzYST67f5658x6g';
	//protected $CardNumber = '4111111111111111';
	public function setKeys($appid,$transId){
      $this->ApiLoginId = $appid;
      $this->TransactionKey = $transId;
	}
	public function chargePayment($data=array()){

		// Setup payment gateway
		$gateway = Omnipay::create('AuthorizeNet_AIM');
		$gateway->setApiLoginId($this->ApiLoginId);
		$gateway->setTransactionKey($this->TransactionKey);
		$gateway->setDeveloperMode(true);
		//$gateway->testMode(true);

		// Send purchase request
		$response = $gateway->purchase($data)->send();

		// Process response
		if ($response->isSuccessful()) {
		    // Payment was successful
		    //return $response;
		    return $response->getTransactionReference();
		} elseif ($response->isRedirect()) {
		    // Redirect to offsite payment gateway
		    $response->redirect();
		}else {
		    // Payment failed
		    return $response->getMessage();
		}
	}

	public function createCustomerProfile($data=array())
	{
		$gateway = Omnipay::create('AuthorizeNet_CIM');
		$gateway->setApiLoginId($this->ApiLoginId);
		$gateway->setTransactionKey($this->TransactionKey);
		$gateway->setDeveloperMode(true);
		
		//return $gateway;
        $request = $gateway->createCard($data);
        $response = $request->send();

        if ($response->isSuccessful()) {
        	return $response->getCardReference();
		}else{
			return $response->getMessage();
			
		}

	}


}//end model
?>