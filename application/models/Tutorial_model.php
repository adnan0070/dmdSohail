<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tutorial_model extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}

	public function get_all()
	{
		$this->db->select('*');
		$this->db->from('tutorial');
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get()->result_array();
		return $query;
	}

  public function get($id){
    $query = $this->db->get_where('tutorial', array('id' => $id));
    return  $query->row_array();
  }

  public function insert($params)
  {
    $this->db->insert('tutorial',$params);
    return $this->db->insert_id();
  }

  public function update($id, $params){
   $this->db->where('id', $id);
   return $this->db->update('tutorial', $params);
 }

 public function delete($id)
 {
  $delete = $this->db->query("DELETE FROM tutorial WHERE id='$id'");      
  return $delete ? true : false;
}

}