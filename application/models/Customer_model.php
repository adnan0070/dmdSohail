<?php
class Customer_model extends CI_model
{
	public function insert_excel($data = array())
	{
		foreach ($data as $key => $value) {

            // check customer exists or not
            if(!$this->check_customer($value['email'],$this->session->userdata('store_id'))){
              $value['store_id'] = $this->session->userdata('store_id');  
              $insert = $this->db->insert('dmd_customers', $value); 
          }

      }

      if($insert){
        return $this->db->insert_id();
    }else{
        return false;
    }
}

public function check_customer($email,$store_id){

  $this->db->select('*');
  $this->db->from('dmd_customers');
  $this->db->where('email', $email);
  $this->db->where('store_id', $store_id);
  $query = $this->db->get();
  if ($query->num_rows() > 0) {
     return true;
 } else {
     return false;
 }


}

public function get_customers()
{
  $this->db->where('store_id',$this->session->userdata('store_id'));
  $query = $this->db->get('dmd_customers');
  return $query->result_array();
}

public function get_customers_with_sales()
{
    $store_id = $this->session->userdata('store_id');
        //$store_id = "18";

    $sql = "SELECT `dmd_customers`.`id` AS cst_id, 
    `dmd_customers`.`f_name` AS f_name,
    `dmd_customers`.`l_name` AS l_name, 
    `dmd_customers`.`email` AS email,
    `dmd_customers`.`shipping_country` AS shipping_country,
    SUM(`dmd_order_items`.`product_quantity`) AS count_sale, 
    SUM(`dmd_order_items`.`total_price`) AS total_sale 
    FROM `dmd_customers`
    LEFT JOIN `dmd_order_items` ON `dmd_customers`.`id` = `dmd_order_items`.`customer_id`
    WHERE `dmd_customers`.`store_id` = '$store_id'
    GROUP BY `dmd_customers`.`id`";

    $query = $this->db->query($sql);
    return $query->result_array();
}    

public function get_products()
{
    $this->db->where('store_id',$this->session->userdata('store_id'));
    $this->db->where('user_id',$this->session->userdata('user_id'));
    $query = $this->db->get('dmd_store_products');
    return $query->result_array();
}

public function filter_customers($data = array())
{
  if($data){
     if(isset($data['from']) && isset($data['to'])) {
        $dates = "'" . date('Y-m-d', strtotime($data["from"])) . "' AND '" . date('Y-m-d', strtotime($data["to"])) ."'";
        unset($data['from']);
        unset($data['to']);
    }


    foreach ($data as $key => $value) {
        if($key == "product"){
            $product = $value;
        }else{
            $data['`dmd_customers`.' . $key] = $value;
        }
        unset($data[$key]);
    }


    $sumq = ',SUM(`dmd_orders`.`grand_total`) AS total_sale';
    $check_customer = true;

    if(isset($data['`dmd_customers`.email']) && $data['`dmd_customers`.email']!=''){
       $query = $this->db->query('select customer_id from dmd_orders where customer_id=(select id from 
          dmd_customers where email="'.$data['`dmd_customers`.email'].'" and store_id="'.$this->session->userdata('store_id').'" limit 1 )');

       if($query->num_rows()>0){
         $check_customer = true;
     }else{
      $sumq='';
      $check_customer = false;
  }
}

$this->db->select(
    '`dmd_customers`.`f_name` AS f_name,
    `dmd_customers`.`l_name` AS l_name,
    `dmd_customers`.`email` AS email,
    `dmd_customers`.`off_sale` AS off_sale' . $sumq
);
$this->db->from('dmd_customers');

if($check_customer){
   $this->db->join('dmd_orders', '`dmd_customers`.`id` = `dmd_orders`.`customer_id`','left');	
   $this->db->group_by("`dmd_orders`.`customer_id`");
}

if(isset($product)){
    $this->db->join('dmd_order_items', '`dmd_customers`.`id` = `dmd_order_items`.`customer_id`','left');  
    $this->db->group_by("`dmd_order_items`.`customer_id`");
    $this->db->where("`dmd_order_items`.`product_id` = " . $product);
}

$data['`dmd_customers`.store_id'] = $this->session->userdata('store_id');
$this->db->where($data);

if(isset($dates)) {
    $this->db->where("`dmd_orders`.`created_date` BETWEEN " . $dates);
}


$query = $this->db->get();
            //return $this->db->last_query();

$result = $query->result_array();
$s = "";
foreach ($result as $key => $value) {

   if(isset($value["total_sale"])){
      $total = $value["total_sale"];
  }else{
      $total = '0';
  }

  $s .= '<tr>';
  $s .= '<td>' . $value["f_name"] . " " . $value["l_name"] . '</td>';
  $s .= '<td>' . $value["email"] . '</td>';
  $s .= '<td>' . $value["off_sale"] . '</td>';
  $s .= '<td>' . $total . '</td>';
  $s .= '</tr>';
}
$filterData["table"] = $s;
$filterData["count"] = count($result) . " CUSTOMERS";
return $filterData;
}else{
 return false;
}
}

public function export_filter_customers($data = array())
{
  if($data){
     if(isset($data['from']) && isset($data['to'])) {
        $dates = "'" . date('Y-m-d', strtotime($data["from"])) . "' AND '" . date('Y-m-d', strtotime($data["to"])) ."'";
        unset($data['from']);
        unset($data['to']);
    }

    foreach ($data as $key => $value) {
        $data['`dmd_customers`.' . $key] = $value;
        unset($data[$key]);
    }


    $this->db->select(
        '`dmd_customers`.`f_name` AS f_name,
        `dmd_customers`.`l_name` AS l_name,
        `dmd_customers`.`email` AS email,
        `dmd_customers`.`off_sale` AS count_sale,
        SUM(`dmd_order_items`.`product_quantity`) AS count_sale, 
        SUM(`dmd_order_items`.`total_price`) AS total_sale'
    );
    $this->db->from('dmd_customers');
    $this->db->join('dmd_order_items', '`dmd_customers`.`id` = `dmd_order_items`.`customer_id`','left');
    $data['`dmd_customers`.store_id'] = $this->session->userdata('store_id');
    $this->db->where($data);

    if(isset($dates)) {
       $this->db->where("`dmd_orders`.`created_date` BETWEEN " . $dates);
   }

   $this->db->group_by("`dmd_order_items`.`customer_id`");
   $query = $this->db->get();
            //$qu = $this->db->get_compiled_select();
			//return $qu;

   $result = $query->result_array();
   return $result;
}else{
 return false;
}
}

public function login_customer($email, $pass)
{
    $this->db->select('*,dmd_customers.id as customerid');
    $this->db->from('dmd_customers');
    $this->db->join('dmd_stores', 'dmd_stores.id = dmd_customers.store_id', 'left');
    $this->db->where('dmd_customers.email', $email);
    $this->db->where('dmd_customers.password', $pass);

        //echo $this->db->last_query();

    if ($query = $this->db->get()) {
        return $query->row_array();
    } else {
        return false;
    }
}

public function password_check($id, $pass)
{
    $this->db->select('*');
    $this->db->from('dmd_customers');
    $this->db->where('dmd_customers.id', $id);
    $this->db->where('dmd_customers.password', md5($pass));

    if ($query = $this->db->get()) {
        return $query->row_array();
            //return true;
    } else {
        return false;
    }
}
public function getCustomer_Byid($id)
{
    $this->db->select('*');
    $this->db->from('dmd_customers');
    $this->db->where('dmd_customers.id', $id); 

    if ($query = $this->db->get()) {
        return $query->row_array();

    } else {
        return false;
    }
}

public function check_rating($data)
{
    $this->db->select('*');
    $this->db->from('product_rating');
    $this->db->where('customer_id', $data["customer_id"]);
    $this->db->where('product_id', $data["product_id"]);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
        return true;
    } else {
        return false;
    }

}
}