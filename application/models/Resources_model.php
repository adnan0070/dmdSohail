<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Resources_Model extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}

	public function get_all()
	{
		$this->db->select('*');
		$this->db->from('resources');
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get()->result_array();
		return $query;
	}

  public function get($id){
    $query = $this->db->get_where('resources', array('id' => $id));
    return  $query->row_array();
  }

  public function insert($params)
  {
    $this->db->insert('resources',$params);
    return $this->db->insert_id();
  }

  public function update($id, $params){
    // print_r($params);die;
   $this->db->where('id', $id);
   return $this->db->update('resources', $params);
 }

 public function delete($id)
 {
  $delete = $this->db->query("DELETE FROM resources WHERE id='$id'");      
  return $delete ? true : false;
}





public function get_all_tags()
{
  $this->db->select('*');
  $this->db->from('meta_tag');
  $this->db->order_by('id', 'DESC');
  $query = $this->db->get()->result_array();
  return $query;
}

public function get_tag($id){
  $query = $this->db->get_where('meta_tag', array('id' => $id));
  return  $query->row_array();
}

public function insert_tag($params)
{
  $this->db->insert('meta_tag',$params);
  return $this->db->insert_id();
}

public function update_tag($id, $params){
 $this->db->where('id', $id);
 return $this->db->update('meta_tag', $params);
}

public function delete_tag($id)
{
  $delete = $this->db->query("DELETE FROM meta_tag WHERE id='$id'");      
  return $delete ? true : false;
}




public function get_all_trackers()
{
  $this->db->select('*');
  $this->db->from('dmd_tracking');
  $this->db->order_by('id', 'DESC');
  $query = $this->db->get()->result_array();
  return $query;
}

public function get_tracker($id){
  $query = $this->db->get_where('dmd_tracking', array('id' => $id));
  return  $query->row_array();
}

public function insert_tracker($params){
  $this->db->insert('dmd_tracking',$params);
  return $this->db->insert_id();
}

public function update_tracker($id, $params){
 $this->db->where('id', $id);
 return $this->db->update('dmd_tracking', $params);
}

public function delete_tracker($id)
{
  $delete = $this->db->query("DELETE FROM dmd_tracking WHERE id='$id'");      
  return $delete ? true : false;
}

function countStores($params = array()){
  if(!empty($params['date'])){
    $this->db->where('store_created', $params['date']);
  }
  return $this->db->count_all_results('dmd_stores');

}

function countProducts($params = array()){
  if(!empty($params['date'])){
    $this->db->where('created_product', $params['date']);
  }
  return $this->db->count_all_results('dmd_store_products');

}

function sumSales($params = array()){

 if(!empty($params['date'])){
  $this->db->where('created_date', $params['date']);
  $earning = $this->db->query("SELECT sum(total_amount) as total_earn FROM dmd_orders WHERE  created_date='" . $params['date'] . "'");
}else{
  $earning = $this->db->query("SELECT sum(total_amount) as total_earn FROM dmd_orders ");
}
return $earning->row_array();

}

}