<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* End of file blog_model.php */
/* Location: ./application/models/blog_model.php */

class Blog_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_last_ten_entries()
	{
		$this->db->order_by('timestamp', 'desc');
		$query = $this->db->get('blog', 10);
		return $query->result();
	}
	
	

	function insert_entry()
	{
		$this->title = $this->input->post('title'); 
		$this->entry = $this->input->post('entry');


		$this->db->insert('blog', $this);
	}

	function get_categories()
	{
		$this->db->select();
		return $this->db->get('categories')->result_array();
	}

	public function get_blogs_with_categories($limit,$page,$cat)
	{
		if($cat)
		{
			$this->db->where('categories.id',$cat);
		}
		
		$this->db->limit($limit, $page);

		$this->db->select('categories.title as cat_title, articles.id ,articles.Category_id, articles.title as art_title, articles.meta_description, articles.content, articles.file, articles.uploaded_on');
		$this->db->join('categories', 'categories.id = articles.Category_id');
		$query = $this->db->get('articles');
		$data['result'] = $query->result_array();
		//echo $this->db->last_query();
		return $data;
	}

	public function count_blogs($id="")
	{
		if($id){$this->db->where('categories.id',$id);}

		$this->db->select('categories.title as cat_title, articles.id ,articles.Category_id, articles.title as art_title, articles.meta_description, articles.content, articles.file, articles.uploaded_on');
		$this->db->join('categories', 'categories.id = articles.Category_id');
		//$query = $this->db->get('articles');

		return $this->db->get('articles')->num_rows();
	}

	function get_blog_detail($id)
	{
		$this->db->where('id',$id);
		$this->db->select();
		return $this->db->get('articles')->row_array();
	}
}