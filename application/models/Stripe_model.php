<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

use Omnipay\Omnipay;
use Omnipay\Common\CreditCard;

class Stripe_model extends CI_model
{
    private $pay;
    private $card;

    function setCard($value){
        try{
            $card = [
                'number' => $value['number'],
                'expiryMonth' => $value['expiryMonth'],
                'expiryYear' => $value['expiryYear'],
                'cvv' => $value['cvv']
            ];
            $ccard = new CreditCard($card);
            $ccard->validate();
            $this->card = $ccard;
            return true;
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
 
    }
 
    function makePayment($value){
        try{
 
            // Setup payment Gateway
            $gateway = Omnipay::create('Stripe');
            $gateway->setApiKey('sk_test_MImkct9yCei3jX1x1YQUP5iP');
            
            // Send purchase request
            $response = $gateway->purchase(
                [
                    'amount' => $value['amount'],
                    'currency' => $value['currency'],
                    'card' => $this->card
                ]
            )->send();
 
            // Process response
            if ($response->isSuccessful()) {
 
                return $response->getTransactionReference();
 
            } elseif ($response->isRedirect()) {
 
                // Redirect to offsite payment gateway
                //return $response->getMessage();
                return $response->redirect();
 
            } else {
               // Payment failed
               return $response->getMessage();
            }
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }
}