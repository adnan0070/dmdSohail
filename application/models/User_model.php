<?php

require 'vendor/autoload.php';
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("AUTHORIZENET_LOG_FILE", "phplog");


class User_model extends CI_model
{


    public function register_user($user,$code)
    {


        $this->db->insert('dmd_users', $user);
        $user_id = $this->db->insert_id();
        //set default package
        if($code == ''){
            $package = $this->db->query("SELECT *
                FROM packages
                WHERE pack_price =  ( SELECT MIN(pack_price) FROM packages ) ");

            $default_package = $package->row_array();
        // $default_package = $default_package['pack_code'];
        // add store package
        //$store_id = $this->session->userdata("store_id");
            $old_date = date('d-m-Y');
            $days = $default_package['pack_days'];
            $next_due_date = date('Y-m-d', strtotime($old_date . " +".$days." days"));
            $data = array(
            //"store_id" => $store_id,
                "user_id" => $user_id,
                "package_id" => $default_package['pack_code'],
                "memory_limit" => $default_package['memory_limit'],
                "product_limit" =>$default_package['pack_products'],
                "end_date" => $next_due_date,
                "payment_status" => 0 //default free package
            );

        }else{
           $package = $this->db->query("SELECT *  from packages where pack_code = '" . $code ."' ");
           $default_package = $package->row_array();
           $data = array(
            //"store_id" => $store_id,
            "user_id" => $user_id,
            "package_id" => $default_package['pack_code'],
            "memory_limit" => 0,
            "product_limit" =>0,
            "payment_status" => 2 //need to buy package
        );
       }
       $this->db->insert('store_package', $data);
   }

 //by admin
   public function add_user($user,$code)
   {


    $this->db->insert('dmd_users', $user);
    $user_id = $this->db->insert_id();
    //set Package
    $package = $this->db->query("SELECT *  from packages where pack_code = '" . $code ."' ");
    $default_package = $package->row_array();
    $old_date = date('d-m-Y');
    $days = $default_package['pack_days'];
    $next_due_date = date('Y-m-d', strtotime($old_date . " +".$days." days"));
    $data = array(
        "user_id" => $user_id,
        "package_id" => $default_package['pack_code'],
        "memory_limit" => $default_package['memory_limit'],
        "product_limit" =>$default_package['pack_products'],
        "sub_admins" =>$default_package['pack_sub_admins'],
        "end_date" => $next_due_date,
        "affiliate" =>2,
        "payment_status" => 1, //no need to buy
        "default" => 0 //need to buy package
    );
    
    $this->db->insert('store_package', $data);
}


public function get_user($id)
{
 $this->db->select('*');
 $this->db->from('dmd_users');
 $this->db->where('dmd_users.id', $id);
 if ($query = $this->db->get()) {
    return $query->row_array();
} else {
    return false;
}
}

public function login_user($email, $pass)
{
    $this->db->select('*,dmd_users.id as userid, dmd_users.status as user_status,dmd_stores.id as store_id, dmd_stores.status as store_status');
    $this->db->from('dmd_users');
    $this->db->join('dmd_stores', 'dmd_stores.user_id = dmd_users.id', 'left');
    $this->db->where('dmd_users.email', $email);
    $this->db->where('dmd_users.password', $pass);

    if ($query = $this->db->get()) {
        return $query->row_array();
    } else {
        return false;
    }
}

public function password_check($id, $pass)
{
    $this->db->select('*');
    $this->db->from('dmd_users');
    $this->db->where('dmd_users.id', $id);
    $this->db->where('dmd_users.password', $pass);

    if ($query = $this->db->get()) {
        return $query->row_array();
            //return true;
    } else {
        return false;
    }
}

  //get payment methods configrations of admin
public function get_admin_keys($admin_id, $name){
    $this->db->select('*');
    $this->db->from('payment_methods');
    $this->db->where('user_id', $admin_id);
    $this->db->where('name', $name);
    if ($query = $this->db->get()) {
        return $query->row_array();
    } else {
        return false;
    }
}

public function get_admin(){

    $admin = $this->db->query("select * from dmd_users where user_role = 2");
    return $admin->row_array();
}

public function get_tutorial_video()
{
    $this->db->select('tutorialfile');
    $this->db->from('settings');
    $query = $this->db->get()->row_array();
    return $query;
    
}

public function email_check($email, $getuser = "")
{

    $this->db->select('*');
    $this->db->from('dmd_users');
    $this->db->where('email', $email);
    $query = $this->db->get();

    if ($query->num_rows() > 0) {
        if ($getuser == 'getuser') {
            return $query->row_array();
        } else {
            return false;
        }
    } else {
        return true;
    }

}
public function emailVerify($vcode)
{

    $this->db->select('*');
    $this->db->from('dmd_users');
    $this->db->where('email_verification_code', $vcode);
    $query = $this->db->get();

    if ($query->num_rows() > 0) {
        $verify['verify'] = '1';
        $this->db->where('email_verification_code', $vcode);
        $this->db->update('dmd_users', $verify);
        return true;
    } else {
        return false;
    }

}
public function Verifycode($vcode)
{

    $this->db->select('*');
    $this->db->from('dmd_users');
    $this->db->where('email_verification_code', $vcode);
    $query = $this->db->get();
        //echo $this->db->last_query();die();

    if ($query->num_rows() > 0) {
        return $query->row_array();
    } else {
        return false;
    }

}
public function ForgotPassword($email)
{
    $this->db->select('email');
    $this->db->from('dmd_users');
    $this->db->where('email', $email);
    $this->db->where('verify', 1);
    $query = $this->db->get();
    return $query->row_array();
}
public function add_subscription($data = array())
{
    $insert = $this->db->insert('subscription', $data);
    if ($insert) {
        return $this->db->insert_id();
    } else {
        return false;
    }
}
public function callConfig()
{
    $config['protocol'] = 'smtp';
    $config['smtp_crypto'] = 'tls';
    $config['smtp_host'] = 'smtp.gmail.com';
    $config['smtp_user'] = 'eegamesstudio@gmail.com';
    $config['smtp_pass'] = 'PUBLIC1234';
    $config['smtp_port'] = 587;
    $config['smtp_timeout'] = 5;
    $config['newline'] = "\r\n";
    $config['mailtype'] = 'html';
    $config['charset'] = 'iso-8859-1';


    $this->load->library('email', $config);
    $this->email->initialize($config);
    $this->email->from('eegamesstudio@gmail.com', 'DMD');
}
public function resetPass($passwordData)
{
    $newpass['password'] = md5($passwordData['password']);
    $this->db->where('email', $passwordData['email']);
    $update = $this->db->update('dmd_users', $newpass);
    if ($update) {
        $this->session->set_flashdata('success_msg',
            'Your password has just been successfully changed. You can now log in with your new password.');
        redirect('user/login/');
    } else {
        $this->session->set_flashdata('error_msg', 'error try again');
        redirect('user/resetpass/' . $passwordData['vcode']);
    }

}
public function resetPassEmail($passwordData)
{
    $email = $data['email'];
    $query1 = $this->db->query("SELECT *  from dmd_users where email = '" . $email .
        "' ");
    $row = $query1->result_array();
    if ($query1->num_rows() > 0) {

            /*$email_message = str_replace("<Name>", $row[0]['display_name'], $email_message);
            $email_message = str_replace("<buttontext>", $button_activate, $email_message);*/
            $email_message = $this->load->view('front_pages/emtemp/reset_password','',true);
            $link_reset = base_url() . '/user/resetPass/' . $row[0]['email_verification_code'];            
            $email_message = str_replace("{name}", $row[0]['display_name'], $email_message);
            $email_message = str_replace("{link_activation}", $link_reset, $email_message);
            $email_message = str_replace("{email}", $email, $email_message);

            $this->callConfig();
            $to = $email;

            // send email to user

            $this->email->to($to);


            $this->email->subject($subject);
            $this->email->message($email_message);

            $this->email->send();
            // need send email

            //redirect(base_url() . 'Jobseeker/index', 'refresh');
        } else {
            $this->session->set_flashdata('error_msg', 'Email not found try again!');
            redirect('user/ForgotPass');
        }
    }
    public function sendpassword($data)
    {
        $email = $data['email'];
        $query1 = $this->db->query("SELECT *  from dmd_users where email = '" . $email .
            "' ");
        $row = $query1->result_array();
        if ($query1->num_rows() > 0) {

             /* 
            $email_message = str_replace("<Name>", $row[0]['display_name'], $email_message);
            $email_message = str_replace("<buttontext>", $button_activate, $email_message);
            */
            $email_message = $this->load->view('front_pages/emtemp/forgot','',true);
            $link_forgot = base_url() . '/user/resetPass/' . $row[0]['email_verification_code'];            
            $email_message = str_replace("{name}", $row[0]['display_name'], $email_message);
            $email_message = str_replace("{link_activation}", $link_forgot, $email_message);
            $email_message = str_replace("{email}", $email, $email_message);

            $to = $email;

            /*$headers = "";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";*/
            $subject = 'Reset Password for DMD account';

            $this->callConfig();


            // send email to user

            $this->email->to($to);


            $this->email->subject($subject);
            $this->email->message($email_message);

            $this->email->send();
            //mail($to, $subject, $email_message, $headers);

            // need send email
            //echo $email_message;
            //redirect(base_url() . 'Jobseeker/index', 'refresh');
        } else {
            $this->session->set_flashdata('error_msg', 'Email not found try again!');
            redirect('user/ForgotPass');
        }
    }

    public function billing_history($id="")
    {

        if(!empty($id)){
            $this->db->where('id', $id);
        }
        
        $this->db->select('*');
        $this->db->from('billing_history');
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function billing_history_for_admin()
    {

        $this->db->select('billing_history.*, dmd_users.*');
        $this->db->from('billing_history');
        $this->db->join('dmd_users','billing_history.user_id = dmd_users.id','left');
        //$this->db->where('billing_history.user_id', $this->session->userdata('user_id'));
        $this->db->order_by('billing_history.created_date', 'desc');
        $query = $this->db->get();
        //echo $this->db->last_query();die;

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_email($type,$store_id)
    {
        $this->db->select("*");
        $this->db->from("email_templates");
        $this->db->where("user_id", $store_id);
        $this->db->where("type", $type);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function get_teams($id="")
    {
        $this->db->select("*");
        $this->db->from("teams");

        if($id != ""){
            $this->db->where("id", $id);
        }

        $query = $this->db->get();

        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function set_teams($data = array())
    {
        $insert = $this->db->insert('teams', $data);

        if ($insert) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function edit_teams($id="", $data=array())
    {
        //print_r($data);die();
        $this->db->where('id', $id);
        $update = $this->db->update('teams', $data);
        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_teams($id){
        $delete = $this->db->delete('teams',array('id'=>$id));
        return $delete?true:false;
    }

    public function check_memory($getsize="")
    {
        $user_id = $this->session->userdata('user_id');
        //get package detail of current user
        $current_package = $this->db->get_where('store_package', array('user_id' => $user_id));
        $current_package = $current_package->row_array();
        $start_date = $current_package['start_date'];
        $end_date = $current_package['end_date'];
        //print_r($current_package);
        
        // get size of uploaded files for current user
        $size_query = "SELECT SUM(f.`memory_size`) AS file_memory 
        FROM (SELECT `memory_size` FROM `dmd_delivery_for_pro` 
        WHERE `user_id` = '" . $user_id . "'
        AND `media_date` BETWEEN DATE('". $start_date ."') AND DATE('". $end_date ."')
        UNION ALL
        SELECT `memory_size` FROM `dmd_store_products` 
        WHERE `user_id` = '" . $user_id . "'
        AND `media_date` BETWEEN DATE('". $start_date ."') AND DATE('". $end_date ."') 
        UNION ALL
        SELECT `memory_size` FROM `dmd_stores` 
        WHERE `user_id` = '" . $user_id . "'
        AND `media_date` = 'NULL' 
        UNION ALL
        SELECT `memory_size` FROM `checkout_customization` 
        WHERE `user_id` = '" . $user_id . "'
        AND `media_date` BETWEEN DATE('". $start_date ."') AND DATE('". $end_date ."')) f";
        
        $size = $this->db->query($size_query);
        $sizep = $size->row_array();
        // print_r($sizep);die;
        $used_memory = $sizep['file_memory']; //KB

        $feature_query = "SELECT SUM(p.`feature_memory_size`) AS file_memory 
        FROM (SELECT `feature_memory_size` FROM `dmd_store_products` 
        WHERE `user_id` = '" . $user_id .  "' ) p";
        $feature_size = $this->db->query($feature_query);
        $feature_sizep = $feature_size->row_array();
        if($feature_sizep){
            $feature_used_memory = $feature_sizep['file_memory']; //KB

            $used_memory = $used_memory+$feature_used_memory;
        }

        $webinar_query = "SELECT SUM(p.`webinar_memory_size`) AS file_memory 
        FROM (SELECT `webinar_memory_size` FROM `dmd_store_products` 
        WHERE `user_id` = '" . $user_id .  "' ) p";
        $webinar_size = $this->db->query($webinar_query);
        $webinar_sizep = $webinar_size->row_array();
        if($webinar_sizep){
            $webinar_used_memory = $webinar_sizep['file_memory']; //KB

            $used_memory = $used_memory+$webinar_used_memory;
        }
        

        // get allowed size of user
        /*$package_limit = $this->db->get_where('packages', array('pack_code' => $current_package['package_id']));
        $package_limit = $package_limit->row_array();
        $package_memory_limit_in_mb = $package_limit['memory_limit'];
        $package_memory_limit = $package_memory_limit_in_mb * 1024; //KB*/


        $package_memory_limit_in_mb = $current_package['memory_limit'];
        $package_memory_limit = (int)$package_memory_limit_in_mb * 1024; //KB
        
        if ($getsize == '') {
            //return storage memory is available or not
            return ($used_memory < $package_memory_limit) ? TRUE : FALSE;
        }else if ($getsize == 'size') {
            //return available remaining storage size in MBs
            $remaining_storage_in_kb = ($package_memory_limit - $used_memory);
            $remaining_storage_in_mb = ($remaining_storage_in_kb / 1024);
            $remaining_storage_in_mb = round($remaining_storage_in_mb, 2);
            return $remaining_storage_in_mb;
        }else if($getsize == 'get'){
            //return storage used overview with available memory limit
            //$used_memory_in_mb = ($used_memory / 1024);
            //$used_memory_in_mb = round($used_memory_in_mb, 2);
            $used_memory_in_mb = $this->format_memory($used_memory);
            $package_memory_limit_in_mb = $this->format_memory($package_memory_limit);
            return $used_memory_in_mb . " / " . $package_memory_limit_in_mb;
        }else if($getsize == 'limit'){
            return $package_memory_limit_in_mb;
        }else if($getsize == 'expiry'){
            return $end_date;
        }else if($getsize == 'used'){
            return $used_memory;
        }
    }

    public function format_memory($size)
    {
        if ($size < 1024) {
            $size = number_format($size, 2);
            $size .= ' KB';
        } else {
            if ($size / 1024 < 1024) {
                $size = number_format($size / 1024, 2);
                $size .= ' MB';
            } else if ($size / 1024 / 1024 < 1024) {
                $size = number_format($size / 1024 / 1024, 2);
                $size .= ' GB';
            }
        }
        return $size;
    }

    public function get_prev_package($user_id){
        $this->db->select();
        $this->db->where('user_id',$user_id);
        return $this->db->get('store_package')->row_array();
    }
    public function get_last_purchase($user_id){
     $this->db->select('amount, trans_id');
     $this->db->limit(1);
     $this->db->order_by('id',"DESC");
     $this->db->where('user_id',$user_id);
     return $this->db->get('billing_history')->row_array();
 }

 public function get_pagination_data($table, $limit, $page){
    $this->db->limit($limit, $page);
    $query = $this->db->get($table);
    return $query->result_array();
}

public function get_pagination_data_count($table){
    $query = $this->db->get($table);
    return $query->num_rows();
}

public function delete_user($user_id)
{
    return  $this->db->delete('dmd_users', array('id' => $user_id));

}

public function delete_store($user_id="",$store_id="")
{
    if($user_id == ""){ $user_id = $this->session->userdata('user_id'); }
    if($store_id == ""){ $store_id = $this->session->userdata('store_id'); }

    $this->db->delete('dmd_stores',             array('id'       => $store_id, 'user_id' => $user_id));
    $this->db->delete('dmd_orders',             array('store_id' => $store_id, 'user_id' => $user_id));
    $this->db->delete('dmd_customers',          array('store_id' => $store_id, 'user_id' => $user_id));
    $this->db->delete('dmd_coupon',             array('store_id' => $store_id, 'user_id' => $user_id));
    $this->db->delete('checkout_customization', array('store_id' => $store_id, 'user_id' => $user_id));
    $this->db->delete('billing_history',        array('store_id' => $store_id, 'user_id' => $user_id));
    $this->db->delete('store_package',          array('store_id' => $store_id, 'user_id' => $user_id));
    $this->db->delete('payment_methods',        array('store_id' => $store_id, 'user_id' => $user_id));
    $this->db->delete('email_templates',        array('store_id' => $store_id, 'user_id' => $user_id));
    $this->db->delete('dmd_integrations',        array('store_id' => $store_id, 'user_id' => $user_id));

    $this->delete_product($user_id, $store_id);
    return true;
}

public function delete_product($user_id="", $store_id="")
{
    if($user_id == ""){ $user_id = $this->session->userdata('user_id'); }
    if($store_id == ""){ $store_id = $this->session->userdata('store_id'); }

    $product_id_array = $this->db->select('product_code, pro_image')->get_where('dmd_store_products',array('store_id' => $store_id, 'user_id' => $user_id))->result_array();
    if($product_id_array){
        foreach ($product_id_array as $key => $value) {
            $this->db->delete('dmd_store_products', array('product_code' => $value['product_code']));
            $this->db->delete('product_rating', array('product_id' => $value['product_code']));
            $this->db->delete('dmd_order_items', array('product_id' => $value['product_code']));
            $this->db->delete('dmd_order_files', array('product_id' => $value['product_code']));
            $this->db->delete('dmd_delivery_for_pro', array('product_id' => $value['product_code']));
            $this->db->delete('comments', array('product_id' => $value['product_code']));

            $old_path = getcwd() . '/site_assets/products/' . $value['pro_image'];
            unlink($old_path);
            $delete_files = $this->db->delete("dmd_delivery_for_pro", array('product_id' => $value['product_code']));
            $delete_emails = $this->db->delete("dmd_email_delivery_pro", array('product_id' => $value['product_code']));
            $path = "./site_assets/products/".$value['product_code'];
            $this->removeDirectory($path);

        }
        return true;
    }else{
        return false;
    }
}

public function removeDirectory($path) {
  $files = glob($path . '/*');
  foreach ($files as $file) {
      is_dir($file) ? removeDirectory($file) : unlink($file);
  }
  rmdir($path);
  return;
}


function getGroupUser($arrayID){
        // print_r($arrayID);die;
    $this->db->select('id, email');
    $this->db->from('dmd_users');
    $this->db->where_in('id', $arrayID);
    $query = $this->db->get();
    return $query->result_array();
}


} //end model

?>