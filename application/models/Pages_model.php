<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pages_model extends CI_Model
{
	
	public function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	//	$this->load->model('Portfolio_model','portfolio_model');
	}
	/*public function getPageDetails($school_id,$page_slug,$globalOption,$page_template)
	{
		$data = array();
        $data['school_id'] = $school_id;
        $data['school_slug'] = '';
        $data['school_details'] = array();
        $schoolThemeDetails = $this->getSchoolThemedetails(0);
        $sizeColor = $this->getSizeColor(0);
        $data['get_category']= $this->portfolio_model->get_category();
        //print_r($sizeColor);
        //die('Template_Model');
        $caption_position_maps = $this->getcaption_and_map_position($page_slug,$school_id);
        $page_details = $this->get_page_details_by_slug($page_slug,$school_id);
        if($page_details->another_link != '' and $page_template==''){
            if (!preg_match("/^(http|ftp):/", $page_details->another_link)) {
               $page_details->another_link = 'http://'.$page_details->another_link;
            }
            $url = $page_details->another_link;
            redirect("$url");
        }
        $data['caption_position_maps'] = $caption_position_maps;
		$data['page_template'] = $page_template;
        $data['globalOption'] = $globalOption;
        $data['schoolThemeDetails'] = $schoolThemeDetails->theme_id;    
        $data['title'] = $page_details->title;
        $data['size'] = $sizeColor;
        $data['description'] = $page_details->key_description;
        $data['keywords'] = $page_details->keywords;
        $data['theme'] = 'switch';
        $data['page'] = 'page';
        $data['page_id'] = $page_details->id;
        $data['page_details'] = $page_details;     
		
		return $data;
	}*/
	public function getSchoolDetails($school_id) {	
		$sql = "SELECT * FROM switch_schoollist
		WHERE id = '$school_id' ";		
		$query = $this->db->query($sql);
		$result = $query->first_row();
		return $result;
	}
	public function getPositionDetails()
	{
		$sql = "SELECT * FROM switch_theme_type ORDER BY id ASC";
		
		$rs = $this->db->query($sql);
		//echo $this->db->last_query(); exit;
		$rec = $rs->result();

		return $rec;
	}
	public function get_hierarchy() {
		
		$sql = "SELECT * FROM `custom_pages` ";
		
		//echo $sql; die('Template_Model');
		$query = $this->db->query($sql);
		$items = array();
		foreach($query->result() as $row){
			$items[] =  $row;
		}		
		$childs = $this->arrange_hierarchy($items, 0);
		$childs['count'] = count($childs);		
		return $childs;
	}//end get_hierarchy
	public function get_pages_that_domain($school_id = 0,$domain_id) {
		if($school_id == 0) {
			$sql = "SELECT * FROM `custom_pages` where domain_id='$domain_id' ";

		} else {
			$sql = "SELECT * FROM `switch_content_page_$school_id` domain_id='$domain_id'";			
		}
		//echo $sql; die('Template_Model');
		$query = $this->db->query($sql);
		$items = array();
		foreach($query->result() as $row){
			$items[] =  $row;
		}		
		//$childs = $this->arrange_hierarchy($items, 0);
		//$childs['count'] = count($childs);		
		return $items;
	}

	public function get_teams()
	{
		$this->db->select("*");
		$this->db->from("teams");

		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return false;
		}
	}
	public function get_store_pages_content($store_name, $slug)
	{
		$this->db->select($slug);
		$this->db->from("dmd_stores");
		$this->db->where('store_name', $store_name);
		$result = $this->db->get()->row();
		return $result->$slug;
	}

	public function get_pages($key="", $val=""){

		$this->db->select('*');
		$this->db->from('custom_pages');

		if(!empty($key)){
			$this->db->where($key, $val);
		}

		$result = $this->db->get()->result();
		return $result;
	}

	public function get_hierarchy_for_menu() {
		$school_id = 0;
		if($school_id == 0) {
			$sql = "SELECT * FROM `custom_pages` where   (show_page_on='TOC' or show_page_on='Both')  ";

		} else {
			$sql = "SELECT * FROM `custom_pages` where   (show_page_on='TOC' or show_page_on='Both') ";			
		}
		
		$query = $this->db->query($sql);
		$items = array();
		foreach($query->result() as $row){
			$items[] =  $row;
		}		
		$childs = $this->arrange_hierarchy($items, 0);
		$childs['count'] = count($childs);		
		return $childs;
	}


	public function get_hierarchy_for_footer_menu() {
		$school_id = 0;
		if($school_id == 0) {
			$sql = "SELECT * FROM `custom_pages` where  (show_page_on='Footer' or show_page_on='Both')  ";

		} else {
			$sql = "SELECT * FROM `custom_pages` where  (show_page_on='Footer' or show_page_on='Both') ";
		}

		$query = $this->db->query($sql);
		$items = array();
		foreach($query->result() as $row){
			$items[] =  $row;
		}
		$childs = $this->arrange_hierarchy($items, 0);
		$childs['count'] = count($childs);
		return $childs;
	}
	public function get_page_by_slug($slug) {
		// $this->db->where('domain_id',$domain_id);
		$this->db->where('slug',$slug);
		$query= $this->db->get('custom_pages');
		
		return $query->result();
	}
	public function get_domain_data() {
		
		$query= $this->db->get('domain');
		
		return $query->result();
	}
	private function arrange_hierarchy(&$items, $parent_id){
		$childs = array();
		foreach($items as $item){
			if($item->parent_id == $parent_id){
				$item->childs = $this->arrange_hierarchy($items, $item->id);
				$childs[] = $item;
			}
		}		
		return $childs;		
	}
	public function get_news() {	
		
		$this->db->order_by("id", "desc");
		$this->db->limit(3);
		$query = $this->db->get('bs_news');
            //return $this->get_results($query);	

		$data = $query->result_array();	
		return $data;	
	}
	public function get_events() {	
		
		$this->db->order_by("id", "desc");
		$this->db->limit(4);
		$query = $this->db->get('events');
            //return $this->get_results($query);	

		$data = $query->result_array();	
		return $data;	
	}

	public function check_slug($table, $slug) {
		$this->db->select("id");
		$this->db->from($table);
		$this->db->where("slug", $slug);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function getMenu($school_id, $slug) {
		$sql = "SELECT * FROM switch_content_page_$school_id
		WHERE `slug` = '$slug' ";		
		$query = $this->db->query($sql);
		$result = $query->first_row();
		return $result;
	}
	public function delete_page($id) {
		$delete = $this->db->query("DELETE FROM custom_pages where id='$id'");		
		return $delete ? true : false;
	}
	public function get_page_content($id) {
		$sql = "SELECT * FROM custom_pages
		WHERE `id` = '$id' ";		
		$query = $this->db->query($sql);
		$result = $query->first_row();
		return $result;
	}
	
	public function save_page($table, $data) {	
		$this->db->insert($table, $data);
		return true;
	}

	public function update($id = "", $data = array())
	{
		if (!empty($data) && !empty($id)) {
			$update = $this->db->update('custom_pages', $data, array('id' => $id));
			return $update ? true : false;
		} else {
			return false;
		}
	}

	function savePosition($table,$data,$id) {

		$school_id = $table->school_id;
		if($id == 0){
			$this->db->insert('switch_table_toolbox_'.$school_id, $data);
			$id = $this->db->insert_id();
		}else{
			$this->db->where('id', $id);
			$this->db->update('switch_table_toolbox_'.$school_id, $data); 
		}
	}//end save
	private function getcheck_page_exits($school_id,$page_id) {
		$sql = "SELECT COUNT(*) AS TOTAL FROM switch_table_toolbox_$school_id
		WHERE page_id = $page_id";
		$query = $this->db->query($sql);		
		$result = $query->first_row();
		return $result->TOTAL;
	}
	function savePagePosition($table,$data,$id) {		
		$school_id = $table->school_id;
		if($id > 0) {
			if($this->getcheck_page_exits($school_id,$id) == 0){
				$this->db->insert('switch_table_toolbox_'.$school_id, $data);
				return true;
			}else{
				$this->db->where('page_id', $id);
				$this->db->update('switch_table_toolbox_'.$school_id, $data);
				return true;
			}
		} else {
			$this->db->insert('switch_table_toolbox_'.$school_id, $data);
			return true;
		}
		
	}//end save
	function get_full_details($table_name,$field_name,$condition,$shortorder,$groupbyorder,$searchvalue,$getvalue,$limit, $start)
	{
		if($limit > 0)
		{
			$this->db->limit($limit, $start);
		}
		
		if(count($shortorder) > 0)
		{
			//print_r($shortorder); die;
			foreach($shortorder as $shortorder1)
			{

				$this->db->order_by($shortorder1[0],$shortorder1[1]);
			}
		}
		
		$this->db->group_by($groupbyorder);
		
		if(count($searchvalue) > 0)
		{
			foreach($searchvalue as $searchvalue1)
			{
				$this->db->like($searchvalue1[0],$searchvalue1[1],'both');
			}
		}

		$fulldetails = $this->db->select($field_name)->get_where($table_name, $condition);
		if($fulldetails->num_rows() > 0)
		{
			if($getvalue!=0)
			{
				$return=$fulldetails->result_object();
			}
			else
			{
				$return=$fulldetails->row();
			}	
		}
		else
		{
			$return=array();
		}
		return $return;
	}

	public function update_global_activation($data){
		$savedata = array(
			'map_id' => 1,
			'domain_id' => $data['domain_id'],
			'title' => $data['title'],
			'fblink'=> $data['facebook'],
			'twlink'=> $data['twitter'],
			'googlelink'=> $data['google'],
			'pinterest'=> $data['pinterest'],
			'mailto'=> $data['mailto'],
			'print'=> $data['print'],
			'colour'=>$data['colour'],
			'textsize'=> $data['textsize'],
			'globalActivated'=> $data['globalActivated'],
			'mailAddress'=> $data['mailAddress']
	//			'address' => $data['address']
		);

		$this->db->where('map_id', 1);
			//$return=$this->db->update('sub_events', $savedata);
			//print_r($return);die('modiel');
		$this->db->update('global_activation', $savedata);
			//print_r($this->db->last_query());die('moel');
		return true;
	}
	
	
	
}	