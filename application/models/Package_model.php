<?php
class Package_model extends CI_model
{


    public function create_package($product)
    {


        $this->db->insert('packages', $product);

    }

    public function update_package($product, $pro_id)
    {
        $this->db->where('pack_code', $pro_id);
        unset($product['user_id']);
        $update = $this->db->update('packages', $product);
        if ($update) {
            $this->session->set_flashdata('success_msg',
                'your package successfully updated!!');
            redirect('package');
        } else {
            $this->session->set_flashdata('error_msg', 'try again something going wrong');
            redirect('package/' . $pro_id);
        }
    }


    public function package_check($slug)
    {

        $this->db->select('*');
        $this->db->from('packages');
        $this->db->where('pack_slug', $slug);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }

    }
    

    public function getAllpurchase($id = "")
    {

        $user_id = $this->session->userdata('user_id');
        $store_id = $this->session->userdata('store_id');
        $this->db->select('*,dmd_orders.id as order_id,dmd_orders.created_date as order_date');
        $this->db->from('dmd_orders');
        $this->db->join('dmd_order_items', 'dmd_order_items.order_id = dmd_orders.id',
            'left');
        $this->db->join('dmd_customers', 'dmd_customers.id = dmd_orders.customer_id',
            'left');
        $this->db->where('dmd_orders.store_id', $store_id);
        if ($id != "") {
            $this->db->where('dmd_order_items.product_id', $id);
        }
        $query = $this->db->get();
        $result = array();
        if ($query->num_rows() > 0) {
            $result['result'] = $query->result_array();
            $this->db->where('dmd_orders.store_id', $store_id);
            $this->db->select('sum(dmd_orders.grand_total) as gtotal');
            $this->db->from('dmd_orders');
            $this->db->join('dmd_order_items', 'dmd_order_items.order_id = dmd_orders.id',
                'left');
            if ($id != "") {
                $this->db->where('dmd_order_items.product_id', $id);
            }
            $query2 = $this->db->get();
            $result['grand_sum'] = $query2->row_array();
            $result['products'] = $this->getOrderproduct($id);
            $result['pid'] = $id;
            return $result;
        } else {
            $result['result'] = array();
            return array();
        }

    }
    public function getOrderproduct($id="")
    {

        $user_id = $this->session->userdata('user_id');
        $store_id = $this->session->userdata('store_id');
        $this->db->select('*');
        $this->db->from('dmd_order_items');
        $this->db->join('dmd_store_products', 'dmd_order_items.product_id = dmd_store_products.product_code',
            'left');
        $this->db->where('store_id', $store_id);
        if($id!=""){
           $this->db->where('dmd_store_products.product_code', $id);
       }
       $this->db->group_by('dmd_order_items.product_id');
       $query = $this->db->get();
        //echo $this->db->last_query();
       if ($query->num_rows() > 0) {
        return $query->result_array();
    } else {
        return array();
    }

}
public function getAll($id = "")
{
    $this->db->select('*');
    $this->db->from('packages');
    if ($id != "") {
        $this->db->where('pack_code', $id);
    }
    $query = $this->db->get();
        //echo $this->db->last_query();
    if ($query->num_rows() > 0) {
        return $query->result_array();
    } else {
        return array();
    }

}



public function getByPeriod($period)
{
    $this->db->select('*');
    $this->db->from('packages');
    $this->db->where('pack_time_period', $period);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
        return $query->result_array();
    } else {
        return array();
    }

}
function getAllmail($id = "", $obj = "")
{
    if ($id != "") {
        if ($obj == "edit") {
            $store_id = $this->session->userdata('store_id');
            $this->db->select('*');
            $this->db->from('dmd_email_delivery_pro');
            $this->db->where('store_id', $store_id);

            $this->db->where('id', $id);
        } else {
            $store_id = $this->session->userdata('store_id');
            $this->db->select('*');
            $this->db->from('dmd_email_delivery_pro');
            $this->db->where('store_id', $store_id);

            $this->db->where('product_id', $id);
        }
        $query = $this->db->get();
            //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
}
function getAllfile($id = "", $obj = "")
{
    if ($id != "") {
        if ($obj == "edit") {
            $store_id = $this->session->userdata('store_id');
            $this->db->select('*');
            $this->db->from('dmd_delivery_for_pro');
            $this->db->where('store_id', $store_id);

            $this->db->where('id', $id);
        } else {
            $store_id = $this->session->userdata('store_id');
            $this->db->select('*');
            $this->db->from('dmd_delivery_for_pro');
            $this->db->where('store_id', $store_id);

            $this->db->where('product_id', $id);
        }
        $query = $this->db->get();
            //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
}
public function get_StoreProducts($store_id = "")
{
    $this->db->select('*');
    $this->db->from('dmd_store_products');
    $this->db->where('store_id', $store_id);
    $this->db->where('product_visibility', 'live');
    $this->db->limit(3);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
        return $query->result_array();
    } else {
        return array();
    }

}
public function get_SingleProducts($product_id = "")
{
    $this->db->select('*');
    $this->db->from('packages');
    $this->db->where('id', $product_id);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
        return $query->result();
    } else {
        return array();
    }

}
public function get_SingleProduct_Bycode($product_code = "")
{
    $this->db->select('*');
    $this->db->from('packages');
    $this->db->where('pack_code', $product_code);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
        return $query->result();
    } else {
        return array();
    }

}

public function get_RandProduct($store_id = '', $product_code='')
{
    $this->db->select('*');
    $this->db->from('dmd_store_products');
    $this->db->where('store_id', $store_id);
        // $this->db->where('product_code !=', $product_code);
    $this->db->order_by('rand() ');
    $this->db->limit(2);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
        return $query->result();
    } else {
        return array();
    }

}
public function get_CrossProduct($store_id = '', $product_code,$cross_sell_products)
{
    $this->db->select('*');
    $this->db->from('dmd_store_products');
    $this->db->where_in('id', $cross_sell_products);
    $this->db->where('store_id', $store_id);
    $this->db->where('product_code !=', $product_code);
    $this->db->order_by('rand() ');
    $this->db->limit(3);
    $query = $this->db->get();
        // echo  $this->db->last_query();
    if ($query->num_rows() > 0) {
        return $query->result();
    } else {
        return array();
    }

}

public function export_filter_purchases($data = array())
{
    if($data){
        if(isset($data['from']) && isset($data['to'])) {
            $dates = "'" . date('Y-m-d', strtotime($data["from"])) . "' AND '" . date('Y-m-d', strtotime($data["to"])) ."'";
            unset($data['from']);
            unset($data['to']);
        }

        foreach ($data as $key => $value) {
            $data['`dmd_orders`.' . $key] = $value;
            unset($data[$key]);
        }


        $this->db->select(
            '`dmd_customers`.`f_name` AS f_name,
            `dmd_customers`.`l_name` AS l_name,
            `dmd_customers`.`email` AS email,
            `dmd_customers`.`off_sale` AS off_sale,
            SUM(`dmd_orders`.`grand_total`) AS total_sale'
        );
        $this->db->from('dmd_customers');
        $this->db->join('dmd_orders', '`dmd_customers`.`id` = `dmd_orders`.`customer_id`');
        $this->db->where($data);
        
        if(isset($dates)) {
            $this->db->where("`dmd_orders`.`created_date` BETWEEN " . $dates);
        }

        $this->db->group_by("`dmd_orders`.`customer_id`");
        $query = $this->db->get();
            //$qu = $this->db->get_compiled_select();
            //return $qu;

        $result = $query->result_array();
        return $result;
    }else{
        return false;
    }
}

public function load_product($offset="0", $limit="3", $search="")
{   
 $liked = "";
 if(!empty($search)){
    $liked = " AND product_name like '%$search%'";
}

$q = "SELECT * FROM `dmd_store_products` 
WHERE `store_id` = '" . $this->session->userdata('store_id') 
. "' $liked LIMIT $limit OFFSET $offset";

$query = $this->db->query($q);
        //echo $this->db->last_query();
if ($query->num_rows() > 0) {
    return $query->result_array();
} else {
    return array();
}
}
}
