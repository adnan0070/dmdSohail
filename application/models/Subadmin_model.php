<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Subadmin_model extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_all()
	{
   $this->db->order_by('id', 'DESC');
   $id = $this->session->userdata('user_id');
   $query = $this->db->get_where('sub_admin', array('user_id' => $id)); 
   return $query->result_array();
 }

 public function get($id){
  $query = $this->db->get_where('sub_admin', array('id' => $id));
  return  $query->row_array();
}

public function insert($params)
{
  $this->db->insert('sub_admin',$params);
  return $this->db->insert_id();
}


public function update($id, $params){
 $this->db->where('id', $id);
 return $this->db->update('sub_admin', $params);
}

public function delete($id)
{
  $delete = $this->db->query("DELETE FROM sub_admin WHERE id='$id'");      
  return $delete ? true : false;
}


public function get_subadmin($email, $password){
 $query = $this->db->get_where('sub_admin', array('email' => $email, 'password' => $password, ) );
 return  $query->row_array();
}


public function get_package($user_id){
 $query = $this->db->get_where('store_package', array('user_id' => $user_id) );
 return  $query->row_array();
}

}