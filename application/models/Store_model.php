<?php
class Store_model extends CI_model {

	public function create_coupon($coupon) {
		$insert = $this->db->insert('dmd_coupon', $coupon);
		if ($insert) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}
	public function create_tax($coupon) {
		$insert = $this->db->insert('dmd_tax', $coupon);
		if ($insert) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function check_coupon($code){
		$this->db->select('*');
		$this->db->from('dmd_coupon');
		$this->db->where('code', $code);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public function edit_coupon($id = "", $data = array()) {
		if (!empty($data) && !empty($id)) {

			$data['active_date'] = date('Y-m-d',strtotime($data['active_date']));
			$data['expire_date'] = date('Y-m-d',strtotime($data['expire_date']));
			$update = $this->db->update('dmd_coupon', $data, array('id' => $id));
			return $update ? true : false;
		} else {
			return false;
		}
	}
	public function check_tax($code){
		$this->db->select('*');
		$this->db->from('dmd_tax');
		$this->db->where('name', $code);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public function edit_tax($id = "", $data = array()) {
		if (!empty($data) && !empty($id)) {
			$update = $this->db->update('dmd_tax', $data, array('id' => $id));
			return $update ? true : false;
		} else {
			return false;
		}
	}

	public function store_check($store_url,$store_id="") {

		$this->db->select('*');
		$this->db->from('dmd_stores');
		if($store_id!=""){
			$this->db->where('id!=', $store_id);  
		}
		$this->db->where('store_url', $store_url);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return false;
		} else {
			return true;
		}

	}
	public function store_check_name($store_name="") {

		$this->db->select('*');
		$this->db->from('dmd_stores');
		$this->db->where('store_name', $store_name);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return false;
		} else {
			return true;
		}

	}
	public function deleteCoupon($table, $id = "") {
		if ($table) {
			$this->db->where('id', $id);
			$query = $this->db->delete($table);

			$this->db->where('enable_coupon', $id);
			$arr =array(
				'enable_coupon'=>0, 
			);
			$query = $this->db->update('dmd_store_products',$arr);
			return $query;
		}
	}
	
	public function create_store($store) {
		$query = $this->db->insert('dmd_stores', $store);
		$this->session->set_userdata('store_id',$this->db->insert_id());
	}
	public function update_store($store,$id="") {
		if($id!=""){
			$this->db->where('id',$id);   
			$this->db->update('dmd_stores', $store);
		}
	}

	public function get_store_customization($id) {
		$this->db->select('logo, banner_text, banner_image, welcome_head, welcome_text, about_head, about_text');
		$query = $this->db->get_where('dmd_stores', array('id' => $id));
		return $query->row_array();
	}

	public function update_store_customization($store, $id){
		$this->db->where('id',$id);
		$this->db->update('dmd_stores', $store);
	}

	public function update_checkout_customization($store, $store_id){
		$this->db->where('id',$store_id);
		$this->db->update('checkout_customization', $store);
	}

	public function getRows($table, $id = "") {
		if ($table) {
			if (!empty($id)) {
				$query = $this->db->get_where($table, array('id' => $id));
				return $query->row_array();
			} else {
				$query = $this->db->get($table);
				return $query->result_array();
			}
		}
	}

	public function get_stores($id = "") {
		$query = $this->db->get_where('dmd_stores', array('user_id' => $id));
		return $query->result_array();
	}

	public function get_coupons($table, $column, $val = "") {
		if ($table) {
			if (!empty($val)) {
				if($table == 'dmd_coupon'){
					$this->db->order_by('`dmd_coupon`.`expire_date`', 'DESC');
				}
				$query = $this->db->get_where($table, array($column => $val));
				if($query){
					return $query->result_array();
				}else{
					return array();
				}
			}
		}
	}

	public function get_StoreRows($table, $column, $val = "") {
		if ($table) {
			if (!empty($val)) {
				$query = $this->db->get_where($table, array($column => $val));
				return $query->row_array();
			} else {
				$query = $this->db->get($table);
				return $query->result_array();
			}
		}
	}
	

	public function get_pm($name) {
		$query = $this->db->query("SELECT * FROM payment_methods WHERE name = '" . $name . "' and user_id='".$this->session->userdata('user_id')."'");
		if($query){
			return $query->row_array();
		}else{
			return false;
		}
	}

	public function searchCoupon($table, $name = "", $status = "") {
		if ($table) {
			if (!empty($name)) {
				$user_id = $this->session->userdata("user_id");
				//$query = $this->db->get_where($table, array('name' => $name, 'status' => $status));
				$q     = "SELECT * FROM `" . $table . "` WHERE `name` LIKE '%" . $name . "%' AND status = '" . $status . "' AND user_id = '" . $user_id . "'";
				$query = $this->db->query($q);
				return $query->result_array();
			} else {
				$query = $this->db->get($table);
				return $query->result_array();
			}
		}
	}

	public function create_pm($data = array()) {
		$insert = $this->db->insert('payment_methods', $data);
		if ($insert) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function update_pm($id = "", $data = array()) {
		//print_r($data);die;
		if (!empty($data) && !empty($id)) {
			if($data["default"] == "1"){
				$q = "UPDATE `payment_methods` SET `default` = 0 WHERE `default` = 1 and user_id='".$this->session->userdata('user_id')."'";
				$this->db->query($q);
			}
			$update = $this->db->update('payment_methods', $data, array('id' => $id));
			return $update ? true : false;
		} else {
			return false;
		}
	}

	public function check_pm(){
		$this->db->select('name');
		$this->db->from('payment_methods');
		$this->db->where('user_id',$this->session->userdata('user_id'));
		$query = $this->db->get();

		if ($query) {
			return $query->result_array();
		} else {
			return true;
		}
	}

	public function delete_email($id) {
		$delete = $this->db->delete('email_templates', array('id' => $id));
		return $delete ? true : false;
	}

	public function create_email($data = array()) {
		$insert = $this->db->insert('email_templates', $data);
		if ($insert) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function update_email($id = "", $data = array()) {
		if (!empty($data) && !empty($id)) {
			$update = $this->db->update('email_templates', $data, array('id' => $id));
			return $update ? true : false;
		} else {
			return false;
		}
	}

	public function email_templete_main($id = "") {
		if ($id != "") {
			$q     = "SELECT * FROM email_templates WHERE id = " . $id.' and user_id="'.$this->session->userdata('user_id').'"';
			$query = $this->db->query($q);
			return $query->row_array();
		} else {
			$query = $this->db->query("SELECT * FROM email_templates where user_id='".$this->session->userdata('user_id')."'");
			return $query->result_array();
		}
	}

	public function enable_pm()
	{
		$store_id = $this->session->userdata("store_id");
		$user_id = $this->db->get_where('dmd_stores', array('id' => $store_id));
		$user_id = $user_id->row_array();
		$user_id = $user_id["user_id"];

		$this->db->select("*");
		$this->db->from("payment_methods");
		$this->db->where("status", "1");
		$this->db->where("user_id", $user_id);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();
	}

	public function enable_admin_pm()
	{
		$admin = $this->db->query("select * from dmd_users where user_role = 2");
		$admin = $admin->row_array();
		$admin_id = $admin['id'];

		$this->db->select("*");
		$this->db->from("payment_methods");
		$this->db->where("status", "1");
		$this->db->where("user_id", $admin_id);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();
	}

	public function get_store_logo()
	{
		$this->db->select('image');
		$this->db->from('checkout_customization');
		$this->db->where('store_id', $this->session->userdata('store_id'));
		$query = $this->db->get();
		return $query->row_array();
	}



	public function check_integration()
	{
		$store_id = $this->session->userdata('store_id');
		$user_ID = $this->session->userdata('user_id');
		$query = $this->db->query("select * from dmd_integrations where store_id='".$store_id."'");
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}

	public function get_integration_detail($name)
	{
		$this->db->select('*');
		$this->db->from('dmd_integrations');
		$this->db->where('name', $name);
		$this->db->where('store_id', $this->session->userdata('store_id'));
		$query = $this->db->get();
		$configuration = array();
		if($query->num_rows()>0){
			$configuration = $query->row_array();
			$status = $configuration['status'];
			$configuration = json_decode($configuration['configuration'],true);
			$configuration['status'] = $status;
			return $configuration;
		}else{
			return false;
		}
	}

}
?>