<?php
defined('BASEPATH') or exit('No direct script access allowed');
function auth()
{
	$ci = &get_instance();
	if (empty($ci->session->userdata('user_id'))) {
		redirect('user/login');
	}
}


function encode($string)
{
	return urlencode(base64_encode($string));
}

function decode($string)
{
	return base64_decode(urldecode($string));
}

function getStatistic(){
	$ci = &get_instance();
	$ci->load->model('Resources_model');
	$data['allStoreCount']   =  $ci->Resources_model->countStores();
	$data['allProductCount'] =  $ci->Resources_model->countProducts();
	$allSales                =  $ci->Resources_model->sumSales();

	if($allSales){
		$data['allSaleSum']  =  $allSales['total_earn'];
	}else{

		$data['allSaleSum']  =  0;
	}
	
	$params['date'] = time();
	$data['todayStoreCount']   =  $ci->Resources_model->countStores($params);
	$data['todayProductCount'] =  $ci->Resources_model->countProducts($params);
	$todaySales                =  $ci->Resources_model->sumSales($params);

	if($todaySales['total_earn'] !=''){
		$data['todaySaleSum']    =  $todaySales['total_earn'];
	}else{

		$data['todaySaleSum']   =  0;
	}
	


	return $data;
}

function getMetatags(){
	$ci = &get_instance();
	$ci->load->model('Resources_model');
	$tags     =  $ci->Resources_model->get_all_tags();
	// print_r($tags);die;
	return $tags;
}

function getTracking(){
	$ci = &get_instance();
	$ci->load->model('Resources_model');
	$trackers     =  $ci->Resources_model->get_all_trackers();
	return $trackers;

}
