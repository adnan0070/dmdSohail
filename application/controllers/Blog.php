<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('Blog_model', 'blog');
		$this->load->library('pagination');
	}

	function index()
	{
		
		//$this->load->model('Blog_model', 'blog');
		
		$data["results"] = $this->blog->get_last_ten_entries();

		
		$this->load->view('blog_view', $data);
		

	}
	
	
	function add()
	{

		$this->load->view('blog_new');
	}

	function insert_page(){

		$this->load->model('Blog_model', 'blog');
		$this->blog->insert_entry();
		echo('<h2>Blog entry added..thanks!</h2>');
	}

	public function blogs_with_categories()
	{
		$data['categories'] = $this->blog->get_categories();

		$parameter  = @$_REQUEST['cat'];

		if($parameter){
			$data['select'] = $parameter;
		}else{
			$data['All'] = true;
		}

		$config = array();
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";


		$config["base_url"] = base_url() . "/blog/blogs_with_categories";
		$config["total_rows"] = $this->blog->count_blogs($parameter);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$config['suffix'] = '?'.http_build_query($_REQUEST, '', "&");
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$blog_data = $this->blog->get_blogs_with_categories($config["per_page"], $page, $parameter);
		$data['categories_detail'] = $blog_data['result'];
		$str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );

		$this->load->view('front_pages/front_header');
		$this->load->view('front_pages/blogs',$data);
		$this->load->view('front_pages/front_footer');
	}

	public function blog_detail($id)
	{
		$data['categories'] = $this->blog->get_categories();
		//$data['categories_detal'] = $this->blog->get_blogs_with_categories();
		$data['blog'] = $this->blog->get_blog_detail($id);
		$this->load->view('front_pages/front_header');
		$this->load->view('front_pages/blog_detail',$data);
		$this->load->view('front_pages/front_footer');
	} 

	public function features()
	{
		$this->load->view('front_pages/front_header');
		$this->load->view('front_pages/static/features');
		$this->load->view('front_pages/front_footer');
	} 
	
	
}

?>