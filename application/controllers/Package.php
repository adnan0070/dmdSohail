<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Package extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('package_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));

    }
    public function index($id = "")
    {
        $products['products'] = $this->package_model->getAll($id);
        $call_view = 'manage_package';
        if ($id != "") {
            $user_id = $this->session->userdata('user_id');
            $store_id = $this->session->userdata('store_id');
            $other_products = $this->db->query('select * from packages where pack_code!="'. $id .'"');
            $call_view = 'packages_stores';
        }

        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/' . $call_view, $products);
        $this->load->view('front_pages/dashboard/dash_footer');
    }


    public function generateProCode($digits_needed = 10)
    {


        $random_number = ''; // set up a blank string

        $count = 0;

        while ($count < $digits_needed) {
            $random_digit = mt_rand(0, 9);

            $random_number .= $random_digit;
            $count++;
        }
        return $random_number;
    }

    public function generateProCodeB($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    public function update()
    {
        if (isset($_REQUEST['create'])) {

            $save_id = $_REQUEST['save_id'];

            $product = $_REQUEST['product'];

            $product['pack_title'] = $product['pack_title'];

            $slug = preg_replace("/-$/", "", preg_replace('/[^a-z0-9]+/i', "-", strtolower($product['pack_title'])));
            $query = "SELECT COUNT(*) AS NumHits FROM packages WHERE  pack_slug  LIKE '$slug%'";
            $result = $this->db->query($query);
            $row = $result->row_array();
            $numHits = $row['NumHits'];

            $use_slug = ($numHits > 1) ? ($slug . '-' . $numHits) : $slug;

            $product['pack_slug'] = $use_slug;
            // check fields validation
            $empty_message = "";
            // if ($_REQUEST['old_image'] == "") {
            //     if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] == '') {
            //         $empty_message .= "Product Image is required field<br>";
            //     }
            // }
            if ($product['pack_time_period'] == 'Days') {
                if ($product['pack_days'] == '') {
                    $empty_message .= "Package Days is a required field<br><br>";
                }
            }else{
                $product['pack_days'] = '';
            }
            if ($product['pack_discount'] != '') {
                if ($product['pack_discount'] < $product['pack_price'] ) {
                    $empty_message .= "Package Orignal price should be less then Price <br><br>";
                }
            }
            if ($product['pack_sub_admins'] == '') {
              $product['pack_sub_admins'] = 0;
          }

          foreach ($product as $key => $val) {
            if(($key != 'pack_days') && ($key != 'pack_sub_admins') && ($key != 'pack_discount')){
                if ($val == "") {
                    $empty_message .= $key . " is required field<br>";
                }
            }
        }

        if ($empty_message != "") {
            $this->session->set_flashdata('error_msg', $empty_message);
            $product['save_id'] = $save_id;
            // $product['pack_image'] = $_REQUEST['old_image_path'];
            $product['pack_slug'] = $_REQUEST['slug_name'];

            $this->session->set_flashdata('fields', $product);
            redirect('package/add');
        }

            // if ($_REQUEST['old_image'] == "") {
            //     // upload product image
            //     $name_parts = pathinfo($_FILES['userfile']['name']);

            //     $name_full = preg_replace('/\s+/', '', $name_parts['filename']);
            //     $productCode = $save_id;
            //     $file_name = $productCode . '-' . $name_full;
            //     $file_ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
            //     $file_name = $productCode . '-' . uniqid().'.'.$file_ext;
            //     $config = array(
            //         'upload_path' => './site_assets/package',
            //         'allowed_types' => 'gif|jpg|png|jpeg',
            //         'file_name' => $file_name,
            //         'max_size' => '0',
            //         'overwrite' => false,
            //         );


            //     $this->load->library('upload', $config);

            //     if ($this->upload->do_upload('userfile')) {

            //         $upload_data = $this->upload->data();

            //         $data_ary = array(
            //             'title' => $upload_data['client_name'],
            //             'file' => $upload_data['file_name'],
            //             'width' => $upload_data['image_width'],
            //             'height' => $upload_data['image_height'],
            //             'type' => $upload_data['image_type'],
            //             'size' => $upload_data['file_size'],
            //             'path' => $upload_data['full_path'],
            //             'date' => time(),
            //             );

            //         $data = array('upload_data' => $data_ary);
            //         // delete old image path
            //         $old_path = getcwd() . '/site_assets/package/' . $_REQUEST['old_image_path'];
            //         unlink($old_path);
            //         $old_path2 = getcwd() . '/site_assets/package/' . $_REQUEST['old_image_thumb'];
            //         unlink($old_path2);
            //         $product['pack_image'] = $data_ary['file'];
            //         $resize = $this->do_resize($data_ary['file']);
            //         if ($resize) {
            //             $expo_thumb = explode(".", $data_ary['file']);
            //             $product['pack_image_thumb'] = $expo_thumb[0] . '_thumb' . '.' . $expo_thumb[1];
            //         }



            //     } else {

            //         $this->session->set_flashdata('error_msg', $this->upload->display_errors());
            //         redirect('package/' . $save_id);

            //     }
            // } else {

            //     $product['pack_image'] = $_REQUEST['old_image'];
            // 

        $user_id = $this->session->userdata('user_id');
        $product['user_id'] = $user_id;
        $product['pack_time_period'] = $product['pack_time_period'];
        $product['pack_price'] = (float)$product['pack_price'];
        $product['pack_products'] = (int)$product['pack_products'];
        $product['memory_limit'] = (int)$product['memory_limit'];


        $admin = $this->db->query("select * from dmd_users where user_role = 2");
        $admin = $admin->row_array();
        $admin_id = $admin['id'];
        $keys = $this->db->query("select * from payment_methods where user_id = '" . $admin_id .
           "'and `name`='stripe'");
        if ($keys->num_rows() > 0) {
           $keys = $keys->row_array();
           $stkey = json_decode($keys['configuration'], 'array');
           $pubkey = $stkey['publishableApiKey'];
           $apikey = $stkey['apiKey'];

           $this->load->library('stripegateway', array("secret_key" => $apikey,
               "public_key" => $pubkey));
           $data['name'] = $product['pack_title'];
           $stripe_product_id = $this->stripegateway->create_product($data);
           $product['stripe_product_id'] = $stripe_product_id;
           $data['stripe_product_id'] = $stripe_product_id;
           if($product['pack_time_period'] == 'Month'){
            $interval = 'month';
        }else{
            $interval = 'year';
        }

        $data['interval'] = $interval;
        $data['currency'] = 'usd';
        $data['amount'] = $product['pack_price'];
        $stripe_plan_id = $this->stripegateway->create_plan($data);
        $product['stripe_plan_id'] = $stripe_plan_id;




        $this->package_model->update_package($product, $save_id);
    }else{
        echo" Please update payment source!";
    }

}
}
public function do_resize($filename)
{

    $source_path = $_SERVER['DOCUMENT_ROOT'] . '/site_assets/package/' . $filename;
    $target_path = $_SERVER['DOCUMENT_ROOT'] . '/site_assets/package/';
    $config_manip = array(
        'image_library' => 'gd2',
        'source_image' => $source_path,
        'new_image' => $target_path,
        'maintain_ratio' => true,
        'create_thumb' => true,
        'thumb_marker' => '_thumb',
        'width' => 150,
        'height' => 150);
    $this->load->library('image_lib', $config_manip);
    if (!$this->image_lib->resize()) {
        echo $this->image_lib->display_errors();
    } else {
        return true;
    }
        // clear //
    $this->image_lib->clear();
}
public function check_product($id)
{
    $qrystr = "SELECT id FROM dmd_store_products  WHERE product_code= " . $id;
    $qryresult = $this->db->query($qrystr);
    $product = $qryresult->row_array();
    if (isset($product['id']) && $product['id'] != '') {
        return true;
    } else {
        return false;
    }
}

public function delete($id)
{
    $qrystr = "SELECT * FROM packages  WHERE pack_code= " . $id;
    $qryresult = $this->db->query($qrystr);
    $product = $qryresult->row_array();
    if (isset($product['id']) && $product['id'] != '') {
        $delete = $this->db->delete("packages", array('pack_code' => $id));
        if ($delete) {
                // delete old image path
            $old_path = getcwd() . '/site_assets/package/' . $product['pack_image'];
            unlink($old_path);
            $this->session->set_flashdata('success_msg',
                'your package successfully deleted!');
            redirect('package');
        } else {
            $this->session->set_flashdata('error_msg', 'try again something going wrong');
            redirect('package/');
        }
    } else {
        $this->session->set_flashdata('error_msg',
            'try again this package not exists in our system');
        redirect('product/');
    }
}

public function add()
{

            //echo('asdad');die;

    if ($this->input->post('create')) {
        //if (isset($_REQUEST['create_product'])) {
            //print_r($_REQUEST);die;

        $productCode = $this->generateProCode(8);
        $product = $_REQUEST['product'];
        $product['pack_code'] = $productCode;
        $title = $product['pack_title'];
        if ($product['pack_discount'] != '') {
            $title = $title." discount";
        }
        $slug = preg_replace("/-$/", "", preg_replace('/[^a-z0-9]+/i', "-", strtolower($title)));
        $query = "SELECT COUNT(*) AS NumHits FROM packages WHERE  pack_slug  LIKE '$slug%'";
        $result = $this->db->query($query);
        $row = $result->row_array();
        $numHits = $row['NumHits'];

        $use_slug = ($numHits > 0) ? ($slug . '-' . $numHits) : $slug;

        $product['pack_slug'] = $use_slug;
            // check fields validation
        $empty_message = "";
            // if ($_FILES['userfile']['name'] == '') {
            //     $empty_message .= "Product Image is a required field<br><br>";
            // }
        if ($product['pack_time_period'] == 'Days') {
            if ($product['pack_days'] == '') {
                $empty_message .= "Package Days is a required field<br><br>";
            }
        }else{
            $product['pack_days'] = '';
        }
        if ($product['pack_discount'] != '') {
            if ($product['pack_discount'] < $product['pack_price'] ) {
                $empty_message .= "Package Orignal price should be less then Price <br><br>";
            }
        }
        if ($product['pack_sub_admins'] == '') {
          $product['pack_sub_admins'] = 0;
      }
      foreach ($product as $key => $val) {
        if ($val == "") {
            switch ($key) {
                case 'pack_title':
                $empty_message .= "Product Title is a required field<br><br>";
                break;
                case 'pack_price':
                $empty_message .= "Product Price is a required field<br><br>";
                break;
                case 'memory_limit':
                $empty_message .= "Product Memory Limit is a required field<br><br>";
                break;
                case 'pack_products':
                $empty_message .= "Number of Products is a required field<br><br>";
                break;
                case 'pack_description':
                $empty_message .= "Product Description is a required field<br><br>";
                break;
                default:
                $empty_message .= ''; 
                break;
            }
        }
    }

    if ($empty_message != "") {
        $this->session->set_flashdata('error_msg', $empty_message);
        $this->session->set_flashdata('fields', $product);
        redirect('package/add');
    }
    $product_check = $this->package_model->package_check($slug);
    if ($product_check) {
                // upload product image
                // $name_parts = pathinfo($_FILES['userfile']['name']);

                // $name_full = preg_replace('/\s+/', '', $name_parts['filename']);
                // $file_name = $productCode . '-' . $name_full;
                // $file_ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
                // $file_name = $productCode . '-' . uniqid().'.'.$file_ext;
                // $config = array(
                //     'upload_path' => './site_assets/package',
                //     'allowed_types' => 'gif|jpg|png|jpeg',
                //     'file_name' => $file_name,
                //     'max_size' => '1000',
                //     'overwrite' => false,
                //     );

                // $this->load->library('upload', $config);

                // if ($this->upload->do_upload('userfile')) {

                //     $upload_data = $this->upload->data();

                //     $data_ary = array(
                //         'title' => $upload_data['client_name'],
                //         'file' => $upload_data['file_name'],
                //         'width' => $upload_data['image_width'],
                //         'height' => $upload_data['image_height'],
                //         'type' => $upload_data['image_type'],
                //         'size' => $upload_data['file_size'],
                //         'path' => $upload_data['full_path'],
                //         'date' => time(),
                //         );

                //     $data = array('upload_data' => $data_ary);
                //     $product['pack_image'] = $data_ary['file'];
                //     $resize = $this->do_resize($data_ary['file']);
                //     if ($resize) {
                //         $expo_thumb = explode(".", $data_ary['file']);
                //         $product['pack_image_thumb'] = $expo_thumb[0] . '_thumb' . '.' . $expo_thumb[1];
                //     }



                    //print_r($product);die();
        $product['pack_time_period'] = $product['pack_time_period'];
        $product['pack_price'] = (float)$product['pack_price'];
        $product['pack_products'] = (int)$product['pack_products'];
        $product['memory_limit'] = (int)$product['memory_limit'];

        $admin = $this->db->query("select * from dmd_users where user_role = 2");
        $admin = $admin->row_array();
        $admin_id = $admin['id'];
        $keys = $this->db->query("select * from payment_methods where user_id = '" . $admin_id .
           "'and `name`='stripe'");
        if ($keys->num_rows() > 0) {
           $keys = $keys->row_array();
           $stkey = json_decode($keys['configuration'], 'array');
           $pubkey = $stkey['publishableApiKey'];
           $apikey = $stkey['apiKey'];

           $this->load->library('stripegateway', array("secret_key" => $apikey,
               "public_key" => $pubkey));
           $data['name'] = $product['pack_title'];
           $stripe_product_id = $this->stripegateway->create_product($data);
           $product['stripe_product_id'] = $stripe_product_id;
           $data['stripe_product_id'] = $stripe_product_id;
           if($product['pack_time_period'] == 'Month'){
            $interval = 'month';
        }else{
            $interval = 'year';
        }

        $data['interval'] = $interval;
        $data['currency'] = 'usd';
        $data['amount'] = $product['pack_price'];
        $stripe_plan_id = $this->stripegateway->create_plan($data);
        $product['stripe_plan_id'] = $stripe_plan_id;



        $this->package_model->create_package($product);
        $this->session->set_flashdata('success_msg',
            'Your package successfully created!');
        redirect('package');
    }

                // } else {
                    //print_r($this->upload->display_errors());
                    // $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                    // redirect('package/add');

                // }
} else {

    $this->session->set_flashdata('error_msg', 'Package already exists,Try again.');
    redirect('package/add');


}


}

$this->load->view('front_pages/dashboard/dash_header');
$products['product_name'] = $this->package_model->getAll();
$this->load->view('front_pages/dashboard/packages_stores', $products);
$this->load->view('front_pages/dashboard/dash_footer');
}
public function detail($id)
{
    if ($id != "") {
        $products['products'] = $this->package_model->getAll($id);
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/product_detail', $products);
        $this->load->view('front_pages/dashboard/dash_footer');
    }

}
public function sendToDownload($id)
{

    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/product_to_download');
    $this->load->view('front_pages/dashboard/dash_footer');
}
public function purchase($id = "")
{
    $this->load->view('front_pages/dashboard/dash_header');
    $data['purchases'] = $this->package_model->getAllpurchase($id);
    $data['id'] = $id;
    $this->load->view('front_pages/dashboard/purchase', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}
public function PurchaseAction($id = "")
{
    if ($id != "") {
        $data['files'] = $this->package_model->getAllfile($id);
        $data['mails'] = $this->package_model->getAllmail($id);
        $data['id'] = $id;
        $data['mid'] = $id;
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/manage_purchase', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }


}
public function filter()
{
    if ($this->input->post()) {
        $data = array_filter($this->input->post());
        if ($data) {
            $result = $this->package_model->filter_purchase($data);
            if ($result) {
                print_r(json_encode($result));
            }
        }
    }
}
public function addMail($id = "")
{
    if ($id != "") {
        $data['id'] = $id;
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/add_email_for_delivery', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }


}
public function editMail($pid, $fid)
{
    if ($fid != "") {
        $data['file'] = $this->package_model->getAllmail($fid, "edit");
        $data['file_id'] = $fid;
        $data['id'] = $pid;
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/add_email_for_delivery', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }


}
public function addFile($id = "")
{
    if ($id != "") {
        $data['id'] = $id;
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/add_file_for_delivery', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }


}
public function editFile($pid, $fid)
{
    if ($fid != "") {
        $data['file'] = $this->package_model->getAllfile($fid, "edit");
        $data['file_id'] = $fid;
        $data['id'] = $pid;
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/add_file_for_delivery', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }
}
}
