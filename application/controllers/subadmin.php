<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subadmin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('Subadmin_model', 'sub_admin');
		$this->load->library('pagination');
		$this->callConfig();
	}

	function index()
	{
		$this->load->view('front_pages/dashboard/dash_header');
		$users = $this->sub_admin->get_all($this->session->userdata('user_id'));
		$package = $this->sub_admin->get_package($this->session->userdata('user_id'));

		$this->session->set_userdata('pack_sub_admin', $package['sub_admins']);
		$this->session->set_userdata('count_sub_admin', count($users));
		$data['users'] = $users;
		$data['package'] = $package;
		$this->load->view('front_pages/dashboard/sub_admins', $data);
		$this->load->view('front_pages/dashboard/dash_footer');
	}
	
	public function callConfig()
	{
		$config['protocol'] = 'smtp';
		$config['smtp_crypto'] = 'ssl';
		$config['smtp_host'] = 'smtpout.secureserver.net';
		$config['smtp_user'] = 'do-not-reply@digitalmediadeliveries.com';
		$config['smtp_pass'] = 'WyWtAsHyHuHaDbAoTs2019';
		$config['smtp_port'] = 465;
		$config['smtp_timeout'] = 5;
		$config['newline'] = "\r\n";
		$config['mailtype'] = 'html';
		$config['charset'] = 'iso-8859-1';

		$this->load->library('email', $config);
		$this->email->initialize($config);
		$this->email->from('do-not-reply@digitalmediadeliveries.com', 'DMD');
	}


	function add()
	{
		if($this->input->post('sub_admin') == 'add'){
			$this->form_validation->set_rules('first_name', 'First Name', 'required', array('required' => 'Title is required'));
			$this->form_validation->set_rules('last_name', 'Last Name', 'required', array('required' => 'Price is required'));
			$this->form_validation->set_rules('email', 'Email', 'required|is_unique[sub_admin.email]|is_unique[dmd_users.email]', array('is_unique' => 'This user is already assigned the role as sub admin or signed as a subscriber!'));
			if($this->form_validation->run())     
			{
				$password = uniqid();
				$display_name = $this->input->post('first_name') .' '.$this->input->post('last_name');
				$params['user_id'] = $this->session->userdata('user_id');
				$params['first_name'] = $this->input->post('first_name');
				$params['last_name'] = $this->input->post('last_name');
				$params['display_name'] = $display_name;
				$params['password'] = md5($password);
				$params['email'] = $this->input->post('email');
				$this->sub_admin->insert($params);
				$this->session->set_flashdata('success_msg', 'Sub admin added successfully');

				$email_message = $this->load->view('front_pages/emtemp/subadmin', '', true);
				$email_message = str_replace("{name}", $display_name, $email_message);
				$email_message = str_replace("{password}", $password, $email_message);
				$email_message = str_replace("{email}", $this->input->post('email'), $email_message);


				$to = $this->input->post('email');

				$this->email->to($to);
				$this->email->subject($subject);
				$this->email->message($email_message);
				$this->email->send();


				redirect('subadmin');    
			}
		}

		$this->load->view('front_pages/dashboard/dash_header');
		$this->load->view('front_pages/dashboard/add_sub_admin');
		$this->load->view('front_pages/dashboard/dash_footer');
	}

	function update($id){

		$id = decode($id);
		$data['user'] = $this->sub_admin->get($id);
		if($data['user']){
			if($this->input->post('sub_admin') == 'update'){

				$params['status'] = $this->input->post('status');
				$this->sub_admin->update($id, $params);
				$this->session->set_flashdata('success_msg', 'Sub admin update successfully');

			}
			
		}

		redirect('subadmin');

		// $this->load->view('front_pages/dashboard/dash_header');
		// $this->load->view('front_pages/dashboard/edit_sub_admin', $data);
		// $this->load->view('front_pages/dashboard/dash_footer');
	}


	public function delete(){
		$id = decode($this->uri->segment(3));
		$user  = $this->sub_admin->get($id);
		if($user)
		{
			$result = $this->sub_admin->delete($id);
			$this->session->set_flashdata('success_msg', 'Sub admin deleted successfully');
		}else{
			$this->session->set_flashdata('error_msg', 'Sub admin is not exist!');
		}

		redirect('subadmin');
	}


}

?>