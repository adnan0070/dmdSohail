<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcomee extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->callConfig();
        //  Path to simple_html_dom
        include APPPATH . 'third_party/2checkout/lib/Twocheckout.php';
    }
    function mypdf(){


       $data = [];
		//load the view and saved it into $html variable
		$html=$this->load->view('welcome_message', $data, true);

        //this the the PDF filename that user will get to download
		$pdfFilePath = "output_pdf_name.pdf";

        //load mPDF library
		$this->load->library('m_pdf');

       //generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);

        //download it.
		$this->m_pdf->pdf->Output($pdfFilePath, "D");
   }
    public function Tcheckout()
    {
        Twocheckout::format('json');
        // Your sellerId(account number) and privateKey are required to make the Payment API Authorization call.

        Twocheckout::privateKey('9AE4BE0A-F3C2-4E19-BFB9-59FA79922F19');
        Twocheckout::sellerId('901395558');

        try {
            $charge = Twocheckout_Charge::auth(array(
                "sellerId" => '901395558',
                "merchantOrderId" => "123",
                "token" => $_REQUEST['token'],
                "currency" => 'USD',
                "total" => '10.00',
                "billingAddr" => array(
                    "name" => 'Testing Tester',
                    "addrLine1" => '123 Test St',
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => 'testingtester@2co.com',
                    "phoneNumber" => '555-555-5555'),
                "shippingAddr" => array(
                    "name" => 'Testing Tester',
                    "addrLine1" => '123 Test St',
                    "city" => 'Columbus',
                    "state" => 'OH',
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => 'testingtester@2co.com',
                    "phoneNumber" => '555-555-5555')));
            $this->assertEquals('APPROVED', $charge['response']['responseCode']);
        }
        catch (Twocheckout_Error $e) {
            $this->assertEquals('Unauthorized', $e->getMessage());
        }
    }
    
    
      public function formHtml(){
        ?>
          <form id="myCCForm" action="<?php echo base_url('/welcomee/Tcheckout'); ?>" method="post">
    <input id="token" name="token" type="hidden" value="">
    <div>
        <label>
            <span>Card Number</span>
        </label>
        <input id="ccNo" type="text" size="20" value="" autocomplete="off" required />
    </div>
    <div>
        <label>
            <span>Expiration Date (MM/YYYY)</span>
        </label>
        <input type="text" size="2" id="expMonth" required />
        <span> / </span>
        <input type="text" size="2" id="expYear" required />
    </div>
    <div>
        <label>
            <span>CVC</span>
        </label>
        <input id="cvv" size="4" type="text" value="" autocomplete="off" required />
    </div>
    <input type="submit" value="Submit Payment">
</form>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://www.2checkout.com/checkout/api/2co.min.js"></script>

<script>
    // Called when token created successfully.
        var successCallback = function(data) {
        var myForm = document.getElementById('myCCForm');

        // Set the token as the value for the token input
        myForm.token.value = data.response.token.token;

        // IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.
        myForm.submit();
    };

    // Called when token creation fails.
    var errorCallback = function(data) {
        if (data.errorCode === 200) {
            tokenRequest();
        } else {
            alert(data.errorMsg);
        }
    };

    var tokenRequest = function() {
        // Setup token request arguments
        var args = {
            sellerId: '901395558',
            publishableKey: "66EC3C40-E9FE-4F32-88BE-340637615610",
            ccNo: $("#ccNo").val(),
            cvv: $("#cvv").val(),
            expMonth: $("#expMonth").val(),
            expYear: $("#expYear").val()
        };

        // Make the token request
        TCO.requestToken(successCallback, errorCallback, args);
    };

    $(function() {
        // Pull in the public encryption key for our environment
        TCO.loadPubKey('sandbox');

        $("#myCCForm").submit(function(e) {
            // Call our token request function
            tokenRequest();

            // Prevent form from submitting
            return false;
        });
    });
</script>
        <?php
    }
    
    
    
    
    public function callConfig()
    {
        $config['protocol'] = 'smtp';
        $config['smtp_crypto'] = 'tls';
        $config['smtp_host'] = 'smtp.gmail.com';
        $config['smtp_user'] = 'eegamesstudio@gmail.com';
        $config['smtp_pass'] = 'PUBLIC1234';
        $config['smtp_port'] = 25;
        $config['smtp_timeout'] = 5;
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';


        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->from('eegamesstudio@gmail.com', 'DMD');
    }
    public function testone()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);


        $subject = 'Your DMD account has been created, Please activate';
        $email_message = '<style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
</style>';
        $email_check['email_verification_code'] = 12345;
        $email_message .= 'Hello <Name>,
            You created an account on Digital Media Delivery. Please click the button below to activate your account<br>
            <buttontext><br>
            Or, copy and paste this link into your browser:<br>
            <link_activation>
            Your username is: <email><br>
            Please activate your account within 7 days of this email.<br>
            Thanks,<br>
            Digital Media Delivery Team<br>
            www.digitalmediadelivery.com<br>
            This is an automated message, please do not reply.';
        $button_activate = '<a href="' . base_url() . '/user/emailVerify/' . $email_check['email_verification_code'] .
            '" class="button">Activate Your Account</a>';
        $link_activate = base_url() . '/user/emailVerify/' . $email_check['email_verification_code'];
        $email_message = str_replace("<Name>", "asif", $email_message);
        $email_message = str_replace("<buttontext>", $button_activate, $email_message);
        $email_message = str_replace("<link_activation>", $link_activate, $email_message);
        $email_message = str_replace("<email>", "altaf", $email_message);


        $this->email->to('altafarif00@gmail.com');


        $this->email->subject('Email Test');
        $this->email->message($email_message);

        if ($this->email->send()) {
            echo "there  ffffffffffffff";
        } else {
            echo $this->email->print_debugger();
            echo "false";
        }
    }
    public function index()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $config['protocol'] = 'smtp';
        $config['smtp_crypto'] = 'tls';
        $config['smtp_host'] = 'smtp.gmail.com';
        $config['smtp_user'] = 'eegamesstudio@gmail.com';
        $config['smtp_pass'] = 'PUBLIC1234';
        $config['smtp_port'] = 25;
        $config['smtp_timeout'] = 5;
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';


        $this->load->library('email', $config);
        $this->email->initialize($config);


        $this->email->from('eegamesstudio@gmail.com', 'DMD');
        $this->email->to('altafarif00@gmail.com');


        $this->email->subject('Email Test');
        $this->email->message('<b>Testing the email class.</b>');

        if ($this->email->send()) {
            echo "there  ffffffffffffff";
        } else {
            echo $this->email->print_debugger();
            echo "false";
        }
        //$this->load->view('welcome_message');
    }
}
