<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pages extends CI_Controller
{
	
	function __construct() {

		parent::__construct();
		$site_url_ex = explode('/',base_url());
		$site_url_count = count($site_url_ex);
		
		if($site_url_count==5){
			define('SITE_SUB_DOMAIN',$site_url_ex[3]);
		}
		else if($site_url_count==4){
			define('SITE_SUB_DOMAIN', 'main_domain');

		}
		
		$this->load->helper('form');
		$this->load->helper('html');
		$this->load->helper('url');
		$this->load->helper('cookie');
		$this->load->helper('date');
		$this->load->helper('string');		
		$this->load->library('session');
		$this->load->model('Pages_model');
		$this->load->library('pagination'); //call pagination library
		$this->load->library('cart');
		$this->pagination_config = array();
		$this->pagination_config['per_page'] = 10;
		//$this->load->library('uri');
	}


	public function front() 
	{
		$data = array();
		
		if(isset($_REQUEST['id'])){
			$slug = $_REQUEST['id'];
			if($slug == "storeRefundPolicy" || $slug == "storePrivacyPolicy" || $slug == "storeTermsServices") 
			{
				if ($slug == 'storeRefundPolicy'){
					$page = 'sales_policy';
				}
				if ($slug == 'storePrivacyPolicy'){
					$page = 'privacy_policy';
				}
				if ($slug == 'storeTermsServices'){
					$page = 'terms_services';
				}
				$store_name = $this->session->userdata('store_name');
				$data['view_items'] = $this->Pages_model->get_store_pages_content($store_name, $page);
				print($data['view_items']);
				
			}
			else{
				$data['view_items'] = $this->Pages_model->get_pages("slug", $slug);
				print($data['view_items'][0]->content);
			}
			
		}else{
			$slug = $this->uri->segment(3);
			$data['view_items'] = $this->Pages_model->get_pages("slug", $slug);
			$data['slug'] = $slug;
			

			if($slug == "about_us" 
				|| $slug == "support"
				|| $slug == "affiliate_program") 
			{
				$view = "quick_links";
				$header = "front_header";
				$footer = "front_footer";
			}

			if($slug == "legal_notice" 
				|| $slug == "dmca_notice" 
				|| $slug == "terms_service") 
			{
				$view = "legal_links";
				$header = "front_header";
				$footer = "front_footer";
			}

			if($slug == "storeRefundPolicy" || $slug == "storePrivacyPolicy" || $slug == "storeTermsServices") 
			{
				if ($slug == 'storeRefundPolicy'){
					$page = 'sales_policy';
				}
				if ($slug == 'storePrivacyPolicy'){
					$page = 'privacy_policy';
				}
				if ($slug == 'storeTermsServices'){
					$page = 'terms_services';
				}
				$store_name = $this->session->userdata('store_name');
				$data['view_items'] = $this->Pages_model->get_store_pages_content($store_name, $page);
				$data['slug'] = $slug;

				$view = "policy_links";
				$header = "store/store_header";
				$footer = "store/store_footer";
			}


			$this->load->view('front_pages/'. $header);
			$this->load->view('front_pages/static/' . $view, $data, false);
			$this->load->view('front_pages/'.$footer);
		}
	}

	public function show_teams()
	{
		$data = array();
		$data['teams'] = $this->Pages_model->get_teams();

		if(isset($_REQUEST['showTeam'])){
			if(isset($data['teams']) && !empty($data['teams'])) {
				$div = '';
				$div .= '<div class="row">';
				$div .= '<div class="col-md-12 text-center static-our-team">';
				$div .=	'<p class="static-team-info">';
				$div .=	'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam nemo ducimus eius, magnam error quisquam sunt voluptate labore, excepturi numquam! Alias libero optio sed harum debitis! Veniam, quia in eum.';
				$div .= '</p>';
				$div .= '</div>';


				foreach ($data['teams'] as $key => $value) {
					$div .= '<div class="col-md-3 text-center static-our-team">';
					$div .= '<img src="' . base_url('site_assets/team/') . $value["image"] . '" class="static-team-photo">';
					$div .= '</div>';

					$div .= '<div class="col-md-9 static-our-team">';
					$div .= '<p class="static-team-name">' . $value["name"] . '</p>';
					$div .= '<p class="static-team-job">' . $value["designation"] . '</p>';
					$div .= '<p class="static-team-info">' . $value["description"] . '</p>';
					$div .= '</div>';
				}

				$div .= '</div>';

				print($div);
			}
				//print("run");

		}else{
			$this->load->view('front_pages/front_header');
			$this->load->view('front_pages/static/quick_links', $data, false);
			$this->load->view('front_pages/front_footer');
		}



	}

	public function page_list()
	{
		$data = array();
		$data['menu_items'] = $this->Pages_model->get_pages();

		$this->load->view('front_pages/dashboard/dash_header');
		$this->load->view('front_pages/dashboard/pages',$data);
		$this->load->view('front_pages/dashboard/dash_footer');
	}

	public function add_new_pages() {
		 //$id_of_page = $this->input->get('id', TRUE);
		// $domain_id =$this->loginUser->domain_id;
			/*if (!(empty($id_of_page))) {
			//$data['page_content'] = $this->Pages_model->get_page_content($id_of_page);
				$content = $data['page_content']->content;
				$content = json_decode($content, true);
			// $data['caption'] =$content['caption'];
				$data['caption'] =$data['page_content'];

		//	$data['social_icon']= explode(',', $content['socialIcon']);
		//	$data['social_icon'] = preg_replace("/[^a-zA-Z0-9#@:_]+/", "", $data['social_icon']);
			// $data['social_icon_text'] =	$content['social_text'];
			//$data['social_icon_text'] =	'';
			// $data['mailto_text'] =	$content['mailto_text'];
			//$data['mailto_text'] =	'';
			// $data['social_color'] =	$content['social_colour'];
			//$data['social_color'] =	'';


			}
			$school_id=0;	
			$data['menu_items'] = $this->Pages_model->get_hierarchy();*/
        //$this->template->load_admin('pages/add_pages',$data);
			$this->load->view('front_pages/dashboard/dash_header');
			$this->load->view('front_pages/dashboard/add_pages');
			$this->load->view('front_pages/dashboard/dash_footer');

		}

		public function page_insert() {
			if($this->input->post()){
				$data  = $this->input->post();

			//validation
				$empty_message = "";
				foreach ($data as $key => $val) {
					if ($val == "") {
						$empty_message .= $key . " is required.<br><br>";
					}
				}

				if ($empty_message != "") {
					$this->session->set_flashdata('message', $empty_message);
					redirect("pages/add_new_pages");
				}

				$table = "custom_pages";
				$user_id = $this->session->userdata("user_id");

				$data['created_by'] = $user_id;
				$data['updated_by'] = $user_id;
				$data['created_on'] = date('Y-m-d H:i:s');
				$data['updated_on'] = date('Y-m-d H:i:s');

				$slug = $this->Pages_model->check_slug($table, $data["slug"]);

				if($slug > 0){
					$data["slug"] .= "-" . time();
				}

				$insert = $this->Pages_model->save_page($table, $data);

				if($insert){
					$this->session->set_flashdata('message', 'You have successfully added the page!');
				}else{
					$this->session->set_flashdata('message', 'New page has been failed to added!');
				} 
				redirect("pages/add_new_pages");
			}

		}
		public function delete(){

			$id = $this->uri->segment(3);
		//print_r($id);die();
			$result = $this->Pages_model->delete_page($id);

			if($result){
				$this->session->set_flashdata('message', 'Delete operation has been successfully made!');
			}else{
				$this->session->set_flashdata('message', 'Delete operation has been failed!');
			}
			redirect("pages/page_list");

		}

		public function edit()
		{
			$id = $this->uri->segment(3);
			$data = array();
			$data['page_items'] = $this->Pages_model->get_pages("id", $id);

			$this->load->view('front_pages/dashboard/dash_header');
			$this->load->view('front_pages/dashboard/add_pages',$data);
			$this->load->view('front_pages/dashboard/dash_footer');

		}


		public function update()
		{
			if($this->input->post()){
				$data = $this->input->post();
			//print_r($data);die();
			//validation
				$empty_message = "";
				foreach ($data as $key => $val) {
					if ($val == "") {
						$empty_message .= $key . " is required.<br><br>";
					}
				}

				if ($empty_message != "") {
					$this->session->set_flashdata('message', $empty_message);
					redirect("pages/add_new_pages");
				}

				$table = "custom_pages";
				$user_id = $this->session->userdata("user_id");
				$id = $data["id"];
				unset($data["id"]);

				$data['created_by'] = $user_id;
				$data['updated_by'] = $user_id;
				$data['created_on'] = date('Y-m-d H:i:s');
				$data['updated_on'] = date('Y-m-d H:i:s');

						/*$slug = $this->Pages_model->check_slug($table, $data["slug"]);

						if($slug > 0){
	        	//$data["slug"] .= "-" . time();
							$this->session->set_flashdata('message', 'Given slug name is alredy existed');
						}*/


			//print_r($data);die();

						$result = $this->Pages_model->update($id, $data);

						if($result){
							$this->session->set_flashdata('message', 'Update operation has been successfully made!');
						}else{
							$this->session->set_flashdata('message', 'Update operation has been failed!');
						}
						redirect("pages/page_list");

					}

				}


















				public function upload_files(){
		     // $domain_id =$this->loginUser->domain_id;

							$ds          = DIRECTORY_SEPARATOR;  //1			 
							$storeFolder = '/upload/page_images/';   //2

							if (!empty($_FILES)) {

								$tempFile = $_FILES['file']['tmp_name'];   //3             
								$targetPath = SIMAGE_PATH. $storeFolder;  //4

								$targetFile =  $targetPath. $_FILES['file']['name'];  //5
								move_uploaded_file($tempFile,$targetFile); //6

							}

						}

						public function upload_files_for_editor(){

		     // $domain_id = $this->loginUser->domain_id;

							$ds          = DIRECTORY_SEPARATOR;  //1			 
							$storeFolder = '/upload/page_images/';   //2

							if (!empty($_FILES)) {

								$tempFile = $_FILES['file']['tmp_name'];   //3             
								$targetPath = SIMAGE_PATH. $storeFolder;  //4

								$targetFile =  $targetPath. $_FILES['file']['name'];  //5
								move_uploaded_file($tempFile,$targetFile); //6
			   // print_r($_FILES);
								$path=base_url().'upload/page_images/'.$_FILES['file']['name'];

								$response['success'] = true;
								$response['location'] = $path;
								echo json_encode($response);
								exit();

							}

						}

						public function delete_upload_files(){
		 // $domain_id =$this->loginUser->domain_id;
							$name_of_file = $this->input->get('name', TRUE);
		//$path = $_SERVER['DOCUMENT_ROOT'];
							unlink(SIMAGE_PATH."/upload/page_images/".$name_of_file);

						}

						public function global_icon(){

							$agreementdetails=($this->uri->segment(2) != '') ? $this->Pages_model->get_full_details('global_activation','*',array('map_id' => 1),array(),array(),array(),0,0, 0) : array();
		//print_r($agreementdetails);die('asdf');
							if($agreementdetails == null){
								$agreementdetails = (object)array('id'=>0,'title'=>'');
							}
							$data['agreementdetails']=$agreementdetails;	
							$this->template->load_admin('pages/add_global_icons',$data);
						}
						public function update_map_global_icon()
						{   
							$domain_info= $this->session->userdata('domain_info');

							$savedata=array();
		// $savedata['domain_id'] = $domain_info['id'];
							$savedata['title'] = $this->input->post('title');
							$savedata['facebook'] = $this->input->post('facebook');
							$savedata['twitter'] = $this->input->post('twitter');
							$savedata['google'] = $this->input->post('google');
							$savedata['mailto'] = $this->input->post('mailto');
							$savedata['pinterest'] = $this->input->post('pinterest');
//		$savedata['address'] = $this->input->post('address');
							$savedata['print'] = $this->input->post('print');
							$savedata['textsize'] = $this->input->post('textsize');
							$savedata['colour'] = $this->input->post('colour');
							$savedata['globalActivated'] = $this->input->post('globalActivated');
							$savedata['mailAddress'] = $this->input->post('mailAddress');


							if($this->Pages_model->update_global_activation($savedata))
							{
								$this->session->set_flashdata('success_msg','Data Updated successfully.');
								redirect(base_url().'admin/pages/global_icon');
			//$this->template->load_admin('pages/add_global_icons');
							}
							else
							{
								$this->session->set_userdata('error_msg','Data Not Updated.');
			//redirect(base_url().'pages/add_maps_global_icon');
							}
						}


					}