<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customization extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'cart'));
        $this->load->model('User_model');
        $this->load->model('store_model');
        $this->load->model('package_model');
        $this->load->model('Customization_model');
        $this->load->model('Product_model');
        $this->load->model('user_model');


        $integration = $this->store_model->get_integration_detail('mailchimp');
        if($integration){
            $this->load->library('mailchimp', $integration);
        }

        $integration = $this->store_model->get_integration_detail('getresponse');
        if($integration){
            $this->load->library('getresponse', $integration);
        }

        $this->callConfig();
    }

    public function callConfig()
    {
        $config['protocol'] = 'smtp';
        $config['smtp_crypto'] = 'ssl';
        $config['smtp_host'] = 'smtpout.secureserver.net';
        $config['smtp_user'] = 'do-not-reply@digitalmediadeliveries.com';
        $config['smtp_pass'] = 'WyWtAsHyHuHaDbAoTs2019';
        $config['smtp_port'] = 465;
        $config['smtp_timeout'] = 5;
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->from('do-not-reply@digitalmediadeliveries.com', 'DMD');
    }

    public function store_customization_packages()
    {
        $data['packages'] = $this->Customization_model->get_packages();
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/admin_store_customization_packages',$data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function packages()
    {
        if ($this->user_model->check_memory()) {

            $data['packages'] = $this->Customization_model->get_packages();
            $this->load->view('front_pages/dashboard/dash_header');
            $this->load->view('front_pages/dashboard/store_customization_packages',$data);
            $this->load->view('front_pages/dashboard/dash_footer');
        } else {
            $this->session->set_flashdata('error_msg_limit',
                'Your memory limit exceeded please upgrade your package Thanks');
            redirect('user/subscription');
        }
    }

    public function add_package()
    {
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/add_store_customization_package');
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function add()
    {
        if($this->input->post('submitted') == 'add') 
        {
          $this->form_validation->set_rules('title', 'Title', 'required', array('required' => 'Title is required'));
          $this->form_validation->set_rules('price', 'Price', 'required', array('required' => 'Price is required'));
          if($this->form_validation->run())     
          { 

            if($this->input->post('enable_form')){
                $enableForm = 1;
            }
            else{
                $enableForm = 0;
            }
            $labels = $this->input->post('label');
            $description  = implode(" * ",$labels);

            $params = array(
                'pack_title' => $this->input->post('title'),
                'pack_price' => $this->input->post('price'),
                'pack_description' => $description,
                'enable_form' => $enableForm,
                'pack_created' => date('Y-m-d H:i:s'),
                'pack_updated' => date('Y-m-d H:i:s')
            );
            $inser_id = $this->Customization_model->insert_package($params);
            redirect('customization/store_customization_packages');
        }
    }
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/add_store_customization_package');
    $this->load->view('front_pages/dashboard/dash_footer');
}


public function edit_package($id='')
{
    if($this->input->post('submitted') == 'edit') 
    {
      $this->form_validation->set_rules('title', 'Title', 'required', array('required' => 'Title is required'));
      $this->form_validation->set_rules('price', 'Price', 'required', array('required' => 'Price is required'));
      if($this->form_validation->run())     
      {
       if($this->input->post('enable_form')){
        $enableForm = 1;
    }
    else{
        $enableForm = 0;
    }
// print_r($this->input->post('label'));die;
    $labels = $this->input->post('label');
    $description  = implode(" * ",$labels);
    $params = array(
        'pack_title' => $this->input->post('title'),
        'pack_price' => $this->input->post('price'),
        'pack_description' => $description,
        'enable_form' => $enableForm,
        'pack_updated' => date('Y-m-d H:i:s')
    );
    $id = $this->input->post('pack_id');
    $inser_id = $this->Customization_model->update($id, $params);
    redirect('customization/store_customization_packages');
}
}
$package  = $this->Customization_model->get($id);
$data['package'] = $package;
$this->load->view('front_pages/dashboard/dash_header');
$this->load->view('front_pages/dashboard/edit_store_customization_package', $data);
$this->load->view('front_pages/dashboard/dash_footer');
}


public function buy(){
    $packageID = decode($this->uri->segment(3));
    $data['package']  = $this->Customization_model->get($packageID);
    $data['enable_pm'] = $this->store_model->enable_admin_pm();
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/customization_subscription', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
    // print_r($packageID);die;
}

public function requests()// customization request
{
 $result = $this->Customization_model->get_all_requests();

 for($i=0; $i< count($result); $i++){

    $store   =  $this->Customization_model->get_store( $result[$i]['store_id']);
    $user    =  $this->Customization_model->get_user( $result[$i]['user_id']);
    $package =  $this->Customization_model->get( $result[$i]['package_id']);

    $result[$i]['store']   = $store['store_name'];
    $result[$i]['url']   = $store['store_url'];
    $result[$i]['email']   = $user['email'];
    $result[$i]['package'] = $package['pack_title'];
}
$data['requests'] = $result;
$this->load->view('front_pages/dashboard/dash_header');
$this->load->view('front_pages/dashboard/customization_request', $data);
$this->load->view('front_pages/dashboard/dash_footer');
}



public function delete_package(){
    $id = $this->uri->segment(3);
    $package  = $this->Customization_model->get($id);
    if($package)
    {
      $result = $this->Customization_model->delete($id);
  }

  redirect('customization/store_customization_packages');
}


public function paypal_subscription()
{
    if ($this->input->post()) {
        $postData = $this->input->post();
        $package =  $this->Customization_model->get($postData['package']);

        // print_r($package);die;
        $user_id = $this->session->userdata('user_id');
        $store_id = $this->session->userdata('store_id');
        $admin = $this->user_model->get_admin();
        $keys = $this->user_model->get_admin_keys($admin['id'], $name = 'paypal');
        $conf = json_decode(@$keys['configuration']);
        $buss_email = $conf->paypalEmail;
        $parms = array(
            "sandbox" => "false",
            "business" => $buss_email,
            "paypal_lib_ipn_log" => true,
            "paypal_lib_ipn_log_file" => "site_assets/logs/paypal_ipn.log",
            "paypal_lib_button_path" => "site_assets/images/",
            "paypal_lib_currency_code" => "USD");

        $this->load->library('paypal_lib', $parms);
        $returnURL = base_url('customization/paypal_success_subscription/'.encode($postData['package']));
        $cancelURL = base_url('customization/paypal_cancel_subscription');
        $notifyURL = base_url('customization/paypal_ipn_subscription');

        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);

        $this->paypal_lib->add_field('txn_type', "recurring_payment");
        $this->paypal_lib->add_field('item_number', $postData['package']);
        $this->paypal_lib->add_field('item_name', $package['pack_title']);
        $this->paypal_lib->add_field('cmd', "_xclick-subscriptions");
        $this->paypal_lib->add_field('a3', $package['pack_price']);

           // $postData["amount"] = $this->getAmount2($postData['package']);
        $this->paypal_lib->add_field('a1', $package['pack_price']);
        $this->paypal_lib->add_field('p1', 1);
        $this->paypal_lib->add_field('t1', 'M');

        $this->paypal_lib->add_field('p3', 1);
        $this->paypal_lib->add_field('t3', 'M');
        $this->paypal_lib->add_field('src', 1);
        $this->paypal_lib->add_field('sra', 1);

        $this->paypal_lib->add_field('custom', $user_id . "|" . $store_id);

        $this->paypal_lib->paypal_auto_form();
    }
}

public function paypal_ipn_subscription()
{
        //$data = implode("|",$_REQUEST);
    $dataP = json_encode($_REQUEST, JSON_UNESCAPED_SLASHES);
    $this->db->insert('customer_card',['customer_id' => $dataP]);

        //$data = file_get_contents('php://input');
    $data = $_REQUEST;

    if(isset($data['txn_id'])){

        $existed = $this->db->get_where('billing_history', ['trans_id' => $data['txn_id']])->result_array();
        if(count($existed) == 0){
            if ($data["payment_status"] == "Completed"){
                $custom = explode('|', $data["custom"]);
                $user_id = $custom[0];
                $store_id = $custom[1];
                    // insert billing history
            //     $this->db->insert('billing_history', array(
            //         'user_id' => $user_id,
            //         "store_id" => $store_id,
            //         "trans_id" => $data['txn_id'],
            //         "payment_response" => json_encode($data, JSON_UNESCAPED_SLASHES),
            //         'amount' => $data['payment_gross']));

            //     $this->db->insert('customization_request', array(
            //         'user_id' => $user_id,
            //         "store_id" => $store_id,
            //         "trans_id" => $data['txn_id'],
            //         "payment_response" => json_encode($data, JSON_UNESCAPED_SLASHES),
            //         'amount' => $data['payment_gross'],
            //         'status' => '0'));
            }

        }

    }
}

public function paypal_success_subscription()
{
  $packageID = decode($this->uri->segment(3));
  $package =  $this->Customization_model->get($packageID);
  $user_id = $this->session->userdata('user_id');
  $store_id = $this->session->userdata('store_id');
  $trx_id = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 10);
  $this->db->insert('billing_history', array(
    'user_id' => $user_id,
    "store_id" => $store_id,
    "trans_id" =>  $trx_id,
    "payment_response" =>  $trx_id,
    'amount' => $package['pack_price'],
    'created_date' => date('Y-m-d H:i:s')));

  $params=  array(
    'user_id' => $user_id,
    "store_id" => $store_id,
    "package_id" => $packageID,
    "trans_id" =>   $trx_id,
    "payment_response" =>  $trx_id,
    'amount' => $package['pack_price'],
    'status' => '0',
    'created_date' => date('Y-m-d H:i:s'),
    'updated_date' => date('Y-m-d H:i:s')
);

  $insertID =  $this->Customization_model->insert_request($params);
  if($package['enable_form'] == 1){
    redirect('customization/form/'.encode( $insertID));

}
$this->session->set_flashdata('error_msg', 'Your package has been updated succesfully Thanks');
redirect(base_url('customization/packages'));    
}


public function form(){
    $data['insertID'] = decode($this->uri->segment(3));
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/customization_form', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}



public function formData(){

   $formParams['logo']       = '';
   $formParams['banner']     = '';
   $formParams['bannerText'] = $this->input->post('bannerText');
   $formParams['welcome']    = $this->input->post('welcome');
   $formParams['about']      = $this->input->post('about');

   if(!empty($_FILES['logo']['name']))//logo
   { 
    $config['upload_path']   = 'site_assets/stores';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $new_name                = time()."_".$_FILES['logo']['name'];
    $config['file_name']     = $new_name;
    $config['max_size']      = '0';
    $config['max_width']     = '0';
    $config['max_height']    = '0';
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('logo'))
    {
      $uploadData         = $this->upload->data();
      $formParams['logo'] = $uploadData['file_name'];
  } 
}

  if(!empty($_FILES['banner']['name'])) //banner
  { 

    $config['upload_path']    = 'site_assets/stores';
    $config['allowed_types']  = 'jpg|jpeg|png|gif';
    $new_name                 = time()."_".$_FILES['banner']['name'];
    $config['file_name']      = $new_name;
    $config['max_size']       = '0';
    $config['max_width']      = '0';
    $config['max_height']     = '0';
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('banner'))
    {
      $uploadData = $this->upload->data();
      $formParams['banner'] = $uploadData['file_name'];
  } 
}

$params['form_data'] = json_encode($formParams);
// print_r($params);die;
$result =  $this->Customization_model->update_request($this->input->post('requestID'), $params);

redirect("customization/featured_products/".encode($this->input->post('requestID')));

}


public function featured_products(){
    $data['requestID'] = decode($this->uri->segment(3));
    $data['products'] =$this->Product_model->getAll($id ="");

    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/customization_request_products', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}


function request_featured_products(){

    $arrID = $this->input->post('productIDs');

    if(!empty($arrID)){
        $ids  = implode(",",$arrID);
        $params['products'] = $ids;
        $result =  $this->Customization_model->update_request($this->input->post('requestID'), $params);
        $this->session->set_flashdata('message',
         'Your customization request has been sent successfully Thanks');
    }else{
        $this->session->set_flashdata('message',
         'Your customization request has been sent successfully Thanks');
    }

    if ($this->user_model->check_memory()) {
    } else {
        $this->session->set_flashdata('error_msg_limit',
            'Your memory limit exceeded please upgrade your package Thanks');
        redirect('user/subscription');
    }

    redirect('customization/packages');
}

public function requestData()
{
    $id = decode($this->uri->segment(3));

    $request = $this->Customization_model->get_request($id);
    $productIDs = explode(',', $request['products']);
    $data['products'] = $this->Customization_model->get_group_products($productIDs);
    $form = json_decode($request['form_data']);
    $data['storeID'] = $request['store_id'];
    $data['store'] =  $this->Customization_model->get_store($request['store_id']);
    $data['requestID'] = $id;


    if($form){
      $formData['logo'] = $form->logo;
      $formData['banner'] = $form->banner;
      $formData['bannerText'] = $form->bannerText;
      $formData['welcome'] = $form->welcome;
      $formData['about'] = $form->about;
  }else{
    $formData['logo'] = '';
    $formData['banner'] = '';
    $formData['bannerText'] = '';
    $formData['welcome'] = '';
    $formData['about'] = '';
}
$data['formData']  = $formData;
    // print_r(count($data['products']));die;
$this->load->view('front_pages/dashboard/dash_header');
$this->load->view('front_pages/dashboard/customization_collection', $data);
$this->load->view('front_pages/dashboard/dash_footer');

}

function customize_form(){
    $data['storeID'] =  decode($this->uri->segment(3));
    $data['requestID'] =  decode($this->uri->segment(4));

    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/admin_customize_store', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function customize_store(){

 $params['banner_text']     = $this->input->post('bannerText');
 $params['welcome_head']    = "Welcome to our store.";
 $params['welcome_text'] = $this->input->post('welcome');
 $params['about_head']    = "About us ";
 $params['about_text']      = $this->input->post('about');
 $request = $this->Customization_model->get_request($this->input->post('requestID'));


 $this->db->select("*");
 $this->db->from('checkout_customization');
 $this->db->where('store_id', $this->input->post('storeID'));
 $query = $this->db->get();
 if ($query->num_rows() > 0) {
    $check_data = $query->row_array();

} else {
    $checkout_data['store_id'] =  $request['$store_id'];
    $checkout_data['store_id'] =  $request['user_id'];
    $check_data['id'] =  $this->db->insert('checkout_customization', $checkout_data);
    $check_data['memory_size'] = 0;
}
$logo_data['memory_size'] = $check_data['memory_size'];
   if(!empty($_FILES['logo']['name']))//logo
   { 
    $config['upload_path']   = 'site_assets/stores';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $new_name                = time()."_".$_FILES['logo']['name'];
    $config['file_name']     = $new_name;
    $config['max_size']      = '0'; //(float)$this->user_model->check_memory("size") * 1024,
    $config['max_width']     = '0';
    $config['max_height']    = '0';
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('logo'))
    {
      $uploadData         = $this->upload->data();
      $params['logo']  = $uploadData['file_name'];
      $logo_data['memory_size'] = $uploadData['file_size'];
      $logo_data['image'] = $uploadData['file_name'];
      $logo_data['media_date'] = date('Y-m-d');
  }
}

  if(!empty($_FILES['banner']['name'])) //banner
  { 

    $config['upload_path']    = 'site_assets/stores';
    $config['allowed_types']  = 'jpg|jpeg|png|gif';
    $new_name                 = time()."_".$_FILES['banner']['name'];
    $config['file_name']      = $new_name;
    $config['max_size']        = '0'; //(float)$this->user_model->check_memory("size") * 1024,
    $config['max_width']      = '0';
    $config['max_height']     = '0';
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('banner'))
    {
      $uploadData = $this->upload->data();
      $params['banner_image'] = $uploadData['file_name'];
      $params['memory_size'] = $uploadData['file_size'];
  }
}

// $params['form_data'] = json_encode($formParams);
$store   =  $this->Customization_model->get_store($this->input->post('storeID'));




$this->store_model->update_checkout_customization($logo_data, $this->input->post('storeID')); 


$result =  $this->Customization_model->update_store($this->input->post('storeID'), $params);
$this->session->set_flashdata('success_msg',
 'Store has been updated successfully Thanks');

$par['status'] = 1;
$res =  $this->Customization_model->update_request($this->input->post('requestID'), $par);
redirect('customization/requestData/'.encode($this->input->post('requestID')));

}

function customize_product(){

    $data['product'] =  $this->Customization_model->get_product(decode($this->uri->segment(3)));
    $data['requestID'] =  decode($this->uri->segment(4));

    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/admin_customize_product',$data);
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function customize_as_feature(){
    // if ($this->user_model->check_memory()) {

    if($this->input->post('submitted') == 'update'){
     $this->form_validation->set_rules('text','description','trim|required');
     if($this->form_validation->run())     
     {
         if(!empty($_FILES['image']['name']))
         { 
            $config['upload_path']   = 'site_assets/products';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $new_name                = time()."_".$_FILES['image']['name'];
            $config['file_name']     = $new_name;
            $config['max_size']       = (float)$this->user_model->check_memory("size") * 1024;
            $config['max_width']     = '0';
            $config['max_height']    = '0';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('image'))
            {
             $uploadData  = $this->upload->data();
             $params['feature_image'] = $uploadData['file_name'];
             $params['feature_memory_size'] = $uploadData['file_size'];
             $params['feature_description']   = $this->input->post('text');
             $params['is_feature']  = 1;

             $result  =  $this->Customization_model->update_product($this->input->post('productID'), $params);
             $this->session->set_flashdata('success_msg','Product has been updated successfully Thanks');
             redirect('customization/requestData/'.encode($this->input->post('requestID')));
         }else{
            $this->session->set_flashdata('error_msg', $this->upload->display_errors());

            redirect('customization/customize_product/'.encode($this->input->post('productID')).'/'.encode($this->input->post('requestID')));
        }
    }
    else{
       $this->session->set_flashdata('error_msg', 'Select product image.');

       redirect('customization/customize_product/'.encode($this->input->post('productID')).'/'.encode($this->input->post('requestID')));
   }
}
else{
    $this->session->set_flashdata('error_msg', 'Enter product description.');
    redirect('customization/customize_product/'.encode($this->input->post('productID')).'/'.encode($this->input->post('requestID')));
}
}
else{
    redirect('customization/requests');
}
// }
// else {
//     print_r($this->user_model->check_memory());die;
//     if(!empty($this->input->post('productID'))  && !empty($this->input->post('requestID'))){
//         $this->session->set_flashdata('error_msg',
//          'Subscriber memory limit exceeded please ask first to upgrade your package Thanks.');
//         redirect('customization/customize_product/'.encode($this->input->post('productID')).'/'.encode($this->input->post('requestID')));

//     }else{
//         $this->session->set_flashdata('error_msg',
//             'Subscriber memory limit exceeded please ask first to upgrade your package Thanks.');
//         redirect('customization/requests');
//     }
// }

}




public function mailchimp_integration($data)
{
    $mailchimp_config = array(
        'email_address' => $data['email'],
        'status'        => 'subscribed',
        'merge_fields'  =>  array('FNAME' => $data['f_name'], 'LNAME' => $data['l_name'])
    );

    $mailchimp = $this->store_model->get_integration_detail('mailchimp');
    if($mailchimp){
        if($mailchimp['status'] == 'on'){
            $list_id = $mailchimp['listid'];
            $result = $this->mailchimp->post("lists/$list_id/members", $mailchimp_config);
        }
    } 
}

public function getresponse_integration($data)
{
    $getresponse_config = array(
        'email' => $data['email'],
        'name'  => $data['f_name'] . ' ' . $data['l_name']
    );

    $getresponse = $this->store_model->get_integration_detail('getresponse');
    if($getresponse){
        if($getresponse['status'] == 'on'){
            $getresponse_config['campaign'] = array('campaignId' => $getresponse['campaignId']);

            $this->getresponse->addContact($getresponse_config);
        }
    } 
}

public function authorize_net_subscription()
{
    if ($this->input->post()) {
        $postData = $this->input->post();
        $store_name = $this->session->userdata('store_name');
        $store_id = $this->session->userdata('store_id');
        $user_id = $this->session->userdata('user_id');

            //load API
        $admin = $this->db->query("select * from dmd_users where user_role = 2");
        $admin = $admin->row_array();
        $admin_id = $admin['id'];

        $keys = $this->db->query("SELECT * FROM `payment_methods` WHERE `user_id` = '" . $admin_id . "' AND `name`= 'authorize'");
        if ($keys->num_rows() > 0) {
           $keys = $keys->row_array();
           $stkey = json_decode($keys['configuration'], 'array');

           $dataapi = array(
               'strAPILoginId' => $stkey['loginID'],
               'strTransactionKey' => $stkey['transactionKey'],
               // 'strValidationMode' => 'testMode');liveMode
             'strValidationMode' => 'liveMode');
                //print_r($dataapi);die();
           $this->load->library('AuthorizeAPI', $dataapi);
       }

            //print_r($postData);die();

       $arrCustomerInfo = array();
       $arrCustomerInfo['firstname'] = $postData['f_name'];
       $arrCustomerInfo['lastname'] = $postData['l_name'];
       $arrCustomerInfo['companipy_name'] = $store_name;
       $arrCustomerInfo['ad_street'] = $postData['street'];
       $arrCustomerInfo['ad_city'] = $postData['city'];
       $arrCustomerInfo['ad_state'] = $postData['country'];
       $arrCustomerInfo['ad_zip'] = $postData['zip'];
       $arrCustomerInfo['ad_country'] = $postData['country'];
       $arrCustomerInfo['ph_number'] = '1111';
       $arrCustomerInfo['em_email'] = $postData['email'];

       $this->authorizeapi->setCustomerAddress($arrCustomerInfo);

       $this->authorizeapi->setCreditCardParameters($postData['creditcard'], $postData['expiry'], $postData['cvc']);

       $arrCustomerAddCCResponse = $this->authorizeapi->addCustomerPaymentProfile($user_id, 'cc');

       $result = json_decode($arrCustomerAddCCResponse, 'array');


       if($result["success"] == 0) {
        if (strpos($result["error"], 'duplicate') !== false) {
            $id_err = $result["error"];
            $id_err = explode("ID", $id_err);
            $id_err = explode(" ", ltrim($id_err[1], " "));
            $profile_id = $id_err[0];
            $result["customerProfileId"] = $profile_id;

                    // add existing
            $this->authorizeapi->setCreditCardParameters($postData['creditcard'], $postData['expiry'],
                $postData['cvc']);

            $arrCustomerAddCCResponse1 = $this->authorizeapi->addCustomerPaymentProfile($user_id,
                'cc', true, $result["customerProfileId"]);
            $arrCustomerAddCCResponse1 = json_decode($arrCustomerAddCCResponse1, 'array');

            $flag = false;
            if ($arrCustomerAddCCResponse1["error"] ==
                'A duplicate customer payment profile already exists.') {

                $check_card = $this->db->query('select customerPaymentProfileId from cst_payment_profiles 
                    where card_number="' . $this->repCard($postData["creditcard"]) .
                    '"');
            $paymentProfile = $check_card->row_array();
            $result["customerPaymentProfileId"] = $paymentProfile['customerPaymentProfileId'];
        }
        if (isset($arrCustomerAddCCResponse1['customerPaymentProfileId']) && $arrCustomerAddCCResponse1['customerPaymentProfileId'] !=
            '') {
                        //echo "there";
            $flag = true;
        $paymentProfile = array(
            'customer_id' => $user_id,
            'customerProfileId' => $result["customerProfileId"],
            'customerPaymentProfileId' => $arrCustomerAddCCResponse1["customerPaymentProfileId"],
            'card_number' => $this->repCard($postData["creditcard"]),
            'card_expiry' => $postData["expiry"],
            'card_cvc' => $postData["cvc"]);

        $insert = $this->db->insert('cst_payment_profiles', $paymentProfile);
        $result["customerPaymentProfileId"] = $arrCustomerAddCCResponse1['customerPaymentProfileId'];
    }

                    //die();
}

}elseif($result["success"] == 1){
    $this->db->select("*");
    $this->db->from("cst_payment_profiles");
    $this->db->where("card_number", $this->repCard($postData["creditcard"]));
    $query = $this->db->get();

    if ($query->num_rows() == 0) {

        $paymentProfile = array(
            'customer_id' => $user_id,
            'customerProfileId' => $result["customerProfileId"],
            'customerPaymentProfileId' => $result["customerPaymentProfileId"],
            'card_number' => $this->repCard($postData["creditcard"]),
            'card_expiry' => $postData["expiry"],
            'card_cvc' => $postData["cvc"]);

        $insert = $this->db->insert('cst_payment_profiles', $paymentProfile);
    }
}

$amt = number_format($postData['amount'], 2);
$arrChargeResponse = $this->authorizeapi->chargeCCeCheck($result["customerProfileId"],
    $result["customerPaymentProfileId"], $amt);

$arrChargeResponse = json_decode($arrChargeResponse, 'array');

   $prev_package = $this->user_model->get_prev_package($savedata['user_id']);//get prev package

   if (isset($arrChargeResponse['transId']) && $arrChargeResponse['transId'] != '') {
                //print_r($arrChargeResponse);die();
    $savedata['package_id'] = $postData['package'];
    $savedata['user_id'] = $user_id;
    $savedata['store_id'] = $store_id;
    $savedata['default'] = "1";
    $savedata['payment_status'] = 1;
    $old_date = date('d-m-Y');
    $next_due_date = date('Y-m-d', strtotime($old_date . ' +30 days'));
    $savedata['end_date'] = $next_due_date;

    $handle_package = $this->handle_package($savedata['package_id']);
    $savedata['memory_limit'] = $handle_package['memory_limit'];
    $savedata['product_limit'] = $handle_package['product_limit'];

     //affiliate

    if ($prev_package['affiliate'] == 0){
     $savedata['affiliate'] = "1";
 }
             //affiliate


 $this->db->where('user_id', $savedata['user_id']);
 $this->db->update('store_package', $savedata);

 $this->db->insert('billing_history', array(
    'user_id' => $savedata['user_id'],
    "store_id" => $savedata['store_id'],
    "trans_id" => $arrChargeResponse['transId'],
    "payment_response" => json_encode($arrChargeResponse),
    'amount' => $postData['amount'])
);


 $this->session->set_flashdata('success_msg',
     'Your package has been updated successfully Thanks');
 redirect(base_url('user/dashboard'));
}else{
   $this->db->where('user_id', $savedata['user_id']);
   $this->db->update('store_package', array('payment_status' => 0));

   $this->session->set_flashdata('error_msg2',
     'Your package has been failed to update');
   redirect(base_url());

}
}
}

public function stripe_subscription()
{
   if ($this->input->post()) {
       $post_data = $this->input->post();
       $data = array();

            //if resubmit stripe token
       // if (isset($post_data['stripeToken'])) {
       //     $session_stripeToken = $this->session->userdata('stripeToken');
       //     if (isset($session_stripeToken) && ($session_stripeToken == $post_data['stripeToken'])) {
       //         $this->session->set_flashdata('error_msg',
       //             'You have apparently resubmitted the request. Please do not do that and try again.');
       //         redirect('cart/checkout');
       //     } else {
       //         $this->session->set_userdata('stripeToken', $post_data['stripeToken']);
       //     }
       // } else {
       //     $this->session->set_flashdata('error_msg',
       //         'Your payment details cannot be processed. You have not been charged. Please confirm that you have JavaScript enabled and try again.');
       //     redirect('cart/checkout');
       // }
 // print_r($post_data['stripeToken']);die;
       $package =  $this->Customization_model->get($post_data['package']);
       $user_id = $this->session->userdata('user_id');

       $admin = $this->db->query("select * from dmd_users where user_role = 2");
       $admin = $admin->row_array();
       $admin_id = $admin['id'];

       $keys = $this->db->query("select * from payment_methods where user_id = '" . $admin_id .
           "'and `name`='stripe'");
       if ($keys->num_rows() > 0) {
           $keys = $keys->row_array();
           $stkey = json_decode($keys['configuration'], 'array');
           $pubkey = $stkey['publishableApiKey'];
           $apikey = $stkey['apiKey'];

                //print_r($stkey);die();

           $this->load->library('stripegateway', array("secret_key" => $apikey,
               "public_key" => $pubkey));

           $data["amount"] = $package['pack_price'];
           $data["currency"] = "usd";
           $data["customer_id"] = $this->session->userdata('user_id');
           $data["store_id"] = $this->session->userdata('store_id');
           $data["stripeToken"] =$post_data['stripeToken'];
           $data["order_id"] = $post_data['package'];
           $data["transId"] = "";

           $data["transId"] = $this->stripegateway->charge_payment($data);

// print_r($data["transId"]);die;
           if ($data["transId"] != "failed" && $data["transId"] != "") {

               $user_id = $this->session->userdata('user_id');
               $store_id = $this->session->userdata('store_id');

               $this->db->insert('billing_history', array(
                'user_id' => $user_id,
                "store_id" => $store_id,
                "trans_id" =>   $data['transId'],
                "payment_response" =>  json_encode($data['transId']),
                'amount' => $package['pack_price']));

               $this->db->insert('customization_request', array(
                'user_id' => $user_id,
                "store_id" => $store_id,
                "package_id" => $package['id'],
                "trans_id" =>  $data['transId'],
                "payment_response" =>  json_encode($data['transId']),
                'amount' => $package['pack_price'],
                'status' => '0'));


               $this->session->set_flashdata('success_msg',
                'Your package has been updated successfully Thanks');
               if($package['enable_form'] == 1){
                redirect('customization/form/'.encode( $insertID));

            }
            $this->session->set_flashdata('error_msg', 'Your package has been updated succesfully Thanks');
            redirect(base_url('customization/packages'));    

        }else{
           $this->db->where('user_id', $user_id);
           $this->db->update('store_package', array('payment_status' => 0));

           $this->session->set_flashdata('error_msg2',
            'Your package has been failed to update');
           redirect(base_url());
       }
   }
}
}


public function send_message(){
    $arr = $this->input->post('users');
    $usersArr = explode(",",$arr);
    $users =   $this->User_model->getGroupUser($usersArr);
    $emails = array();

    for($a =0; $a < count($users); $a++){
      array_push($emails, $users[$a]['email']);
  }


  $subject = 'Digitalmediadeliveries';

  $email_message = $this->load->view('front_pages/emtemp/content_email', '', true);
  $email_message = str_replace("{content}", $this->input->post('message'), $email_message);

  $to = $emails;
  $this->email->to($to);


  $this->email->subject($subject);
  $this->email->message($email_message);

  if($this->email->send()){
     $this->session->set_flashdata('success_msg', "Message has been delivered successfully. ");
 }else{
     $this->session->set_flashdata('error_msg', "Server is not  responding, Please try again. ");
 }
 unset($_POST);
 $this->requests();

}




} //end controller
