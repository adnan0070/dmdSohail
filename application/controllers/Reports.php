<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reports extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('product_model');
        $this->load->library('session');

    }
    public function index()
    {
        $this->load->view('front_pages/dashboard/dash_header');
        // get products
        $result['products'] = $this->product_model->getAll();

        /*$sales = $this->db->query('SELECT  MONTHNAME(created_date) as month , COUNT(created_date) as total 
        FROM dmd_orders WHERE created_date >= NOW() - INTERVAL 1 YEAR and user_id="'.$this->session->userdata('user_id').'" GROUP BY MONTH(created_date)');

        $sales = $sales->result_array();

        $result['sales'] = $sales;*/
        $this->load->view('front_pages/dashboard/reports_last', $result);
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    public function filter2(){
       if ($this->input->post()) {
       $empty_message = "";
        $flag = "false";
        $data = $this->input->post();
        foreach ($data as $key => $val) {
            if ($val == "") {
                $empty_message .= $key . " is required field<br>";
            }
            else{
                $flag = "true";
            }
        }
        if ($empty_message != "" && $flag=="false") {
            $this->session->set_flashdata('error_msg',"please chose any one field for filter");
            redirect('/reports');
        }
        $data = array_filter($this->input->post());
            
                $month = array();
                $dataPoints = array();
               
            
            
            if ($data) {
                $date_day = "1 YEAR";
                $dates  ="";
                $join="";
                $where_plus="";
                if (isset($data['from']) && isset($data['to'])) {
                    $dates = " and dmd_orders.created_date between '" . date('Y-m-d', strtotime($data["from"])) . "' AND '" . date('Y-m-d',
                        strtotime($data["to"])) . "'";
                    unset($data['from']);
                    unset($data['to']);
                }
                if (isset($data['period'])) {
                    $period = $data['period'];
                    $date_day = $period." day";
                    unset($data['period']);
                }
                if(isset($_REQUEST['product'])&&$_REQUEST['product']!=""){
                    $join = 'left join dmd_order_items on (dmd_orders.id=dmd_order_items.order_id)';
                    $where_plus = ' and dmd_order_items.product_id="'.$_REQUEST['product'].'"'; 
                }
                /*$sales = $this->db->query('SELECT  MONTHNAME(dmd_orders.created_date) as month , COUNT(dmd_orders.created_date) as total 
        FROM dmd_orders '.$join.'  WHERE dmd_orders.created_date >= NOW() - INTERVAL '.$date_day.' '.$dates.$where_plus.' and user_id="'.$this->session->userdata('user_id').'" GROUP BY MONTH(dmd_orders.created_date)');
                //echo $this->db->last_query();
                $sales = $sales->result_array();*/
                $user_role = $this->session->userdata('user_role');
                 for($i=1;$i<=12;$i++){
                   $month = date('F', mktime(0,0,0,$i, 1, date('Y')));
                   //echo "select count(*) as total from dmd_orders ".$join." where MONTHNAME(dmd_orders.created_date)='".$month."' and dmd_orders.created_date >= NOW() - INTERVAL ".$date_day." ".$dates.$where_plus." and dmd_orders.user_id='".$this->session->userdata('user_id')."' ";
                   if($user_role!='2'){
                    $da = $this->db->query("select count(*) as total from dmd_orders ".$join." where MONTHNAME(dmd_orders.created_date)='".$month."' and dmd_orders.created_date >= NOW() - INTERVAL ".$date_day." ".$dates.$where_plus." and dmd_orders.user_id='".$this->session->userdata('user_id')."' and dmd_orders.store_id='".$this->session->userdata('store_id')."' ");
                   }
                   else{
                    $da = $this->db->query("select count(*) as total from dmd_orders ".$join." where MONTHNAME(dmd_orders.created_date)='".$month."' and dmd_orders.created_date >= NOW() - INTERVAL ".$date_day." ".$dates.$where_plus."");
                   }
                   $da = $da->row_array();
                   $mon = substr($month, 0, 3);
                   $dataPoints[] = array("y" =>$da['total'] , "label" => $mon);   
                }
                
                
                
                
                //$result = $this->product_model->filter_purchase($data);
                $this->session->set_flashdata('dataPoints', $dataPoints);
                redirect('/reports');
                
            }
        } 
    }
    
    public function filter()
    {
        $data = $this->input->post();
        if ($data) {
            
            if( ! empty($data['from']) && ! empty($data['to'])){
                $data['period'] = '';
            }
            $data = array_filter($data);
            //return print_r($data);

            $this->db->reset_query();

            if (isset($data['from']) && isset($data['to'])) {
                $from = date('Y-m-d', strtotime($data["from"]));
                $to = date('Y-m-d', strtotime($data["to"]));
                unset($data['from']);
                unset($data['to']);

                $this->db->where('dmd_orders.created_date BETWEEN "' . $from . '" AND "' . $to . '"');
            }

            if(isset($data['product'])){
                $this->db->join('dmd_order_items', 'dmd_orders.id = dmd_order_items.id','left');
                if($data['product'] != 'all'){
                    $this->db->where('dmd_order_items.product_id', $data['product']);
                }
            }
            
            //set default period
            $period = "1 YEAR";
            $select = 'COUNT(`dmd_orders`.`created_date`) AS y, MONTHNAME(`dmd_orders`.`created_date`) AS label';
            $group_by = 'MONTH(dmd_orders.created_date)';
            $order_by = 'dmd_orders.created_date';
            if (isset($data['period'])) {
                switch ($data['period']) {
                    case '7':
                        $select = 'COUNT(`dmd_orders`.`created_date`) AS y, DAYNAME(`dmd_orders`.`created_date`) AS label';
                        $group_by = 'DAY(dmd_orders.created_date)';
                        $order_by = 'dmd_orders.created_date';
                        break;
                    case '30':
                        $select = 'COUNT(`dmd_orders`.`created_date`) AS y, DAY(`dmd_orders`.`created_date`) AS label';
                        $group_by = 'DAY(dmd_orders.created_date)';
                        $order_by = 'dmd_orders.created_date';
                        break;
                    case '90':
                        $select = 'COUNT(`dmd_orders`.`created_date`) AS y, MONTHNAME(`dmd_orders`.`created_date`) AS label';
                        $group_by = 'MONTH(dmd_orders.created_date)';
                        $order_by = 'dmd_orders.created_date';
                        break;
                    default:
                        $select = 'COUNT(`dmd_orders`.`created_date`) AS y, MONTHNAME(`dmd_orders`.`created_date`) AS label';
                        $group_by = 'MONTH(dmd_orders.created_date)';
                        $order_by = 'dmd_orders.created_date';
                        break;
                }
                
                $period = (string)$data['period'] . " DAY";
                unset($data['period']);
            }

            $user_role = $this->session->userdata('user_role');
            if($user_role != 2){
                $this->db->where('dmd_orders.user_id', $this->session->userdata('user_id'));
            }
  
            $this->db->select($select);
            $this->db->from('dmd_orders');
            $this->db->where('dmd_orders.created_date >= NOW() - INTERVAL ' . $period);
            $this->db->group_by($group_by);
            $this->db->order_by($order_by, 'ASC');

            //return print_r($this->db->get_compiled_select());
            $sales = $this->db->get();
            if($sales->num_rows() > 0){
                $data = $sales->result_array();
                $result['xAxis'] = array_column($data, 'label');
                $result['yAxis'] = array_column($data, 'y');
                //print_r($result);
                echo json_encode($result, JSON_NUMERIC_CHECK);
            }else{
                return array();
            }
        }
    }

    public function billing()
    {
        $data = $this->input->post();
        if ($data) {
            
            if( ! empty($data['from']) && ! empty($data['to'])){
                $data['period'] = '';
            }
            $data = array_filter($data);
            //return print_r($data);

            $this->db->reset_query();

            if (isset($data['from']) && isset($data['to'])) {
                $from = date('Y-m-d', strtotime($data["from"]));
                $to = date('Y-m-d', strtotime($data["to"]));
                unset($data['from']);
                unset($data['to']);

                $this->db->where('dmd_orders.created_date BETWEEN "' . $from . '" AND "' . $to . '"');
            }

            /*if(isset($data['product'])){
                $this->db->join('dmd_order_items', 'dmd_orders.id = dmd_order_items.id','left');
                if($data['product'] != 'all'){
                    $this->db->where('dmd_order_items.product_id', $data['product']);
                }
            }*/
            
            //set default period
            $period = "1 YEAR";
            $select = 'COUNT(`billing_history`.`created_date`) AS y, MONTHNAME(`billing_history`.`created_date`) AS label';
            $group_by = 'MONTH(billing_history.created_date)';
            $order_by = 'YEAR(billing_history.created_date)';
            if (isset($data['period'])) {
                switch ($data['period']) {
                    case '7':
                        $select = 'COUNT(`billing_history`.`created_date`) AS y, DAYNAME(`billing_history`.`created_date`) AS label';
                        $group_by = 'DAY(billing_history.created_date)';
                        $order_by = 'DAY(billing_history.created_date)';
                        break;
                    case '30':
                        $select = 'COUNT(`billing_history`.`created_date`) AS y, DAY(`billing_history`.`created_date`) AS label';
                        $group_by = 'DAY(billing_history.created_date)';
                        $order_by = 'DAY(billing_history.created_date)';
                        break;
                    case '90':
                        $select = 'COUNT(`billing_history`.`created_date`) AS y, MONTHNAME(`billing_history`.`created_date`) AS label';
                        $group_by = 'MONTH(billing_history.created_date)';
                        $order_by = 'YEAR(billing_history.created_date)';
                        break;
                    default:
                        $select = 'COUNT(`billing_history`.`created_date`) AS y, MONTHNAME(`billing_history`.`created_date`) AS label';
                        $group_by = 'MONTH(billing_history.created_date)';
                        $order_by = 'YEAR(billing_history.created_date)';
                        break;
                }
                
                $period = (string)$data['period'] . " DAY";
                unset($data['period']);
            }

            //$user_role = $this->session->userdata('user_role');
            /*if($user_role != 2){
                $this->db->where('billing_history.user_id', $this->session->userdata('user_id'));
            }*/
  
            $this->db->select($select);
            $this->db->from('billing_history');
            $this->db->where('billing_history.created_date >= NOW() - INTERVAL ' . $period);
            $this->db->group_by($group_by);
            $this->db->order_by($order_by, 'ASC');

            //return print_r($this->db->get_compiled_select());
            $sales = $this->db->get();
            if($sales->num_rows() > 0){
                $data = $sales->result_array();
                $result['xAxis'] = array_column($data, 'label');
                $result['yAxis'] = array_column($data, 'y');
                //print_r($result);
                echo json_encode($result, JSON_NUMERIC_CHECK);
            }else{
                return array();
            }
        }
    }

}
