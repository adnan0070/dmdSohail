<?php
defined('BASEPATH') or exit('No direct script access allowed');

ob_start();

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class Product extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('user_model');
        $this->load->model('store_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->library("cart");
        $this->callConfig();

    }
    public function callConfig()
    {
        $config['protocol'] = 'smtp';
        $config['smtp_crypto'] = 'ssl';
        $config['smtp_host'] = 'smtpout.secureserver.net';
        $config['smtp_user'] = 'do-not-reply@digitalmediadeliveries.com';
        $config['smtp_pass'] = 'WyWtAsHyHuHaDbAoTs2019';
        $config['smtp_port'] = 465;
        $config['smtp_timeout'] = 5;
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';


        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->from('do-not-reply@digitalmediadeliveries.com', 'DMD');
    }
    
    public function index($id = "")
    {

        $this->load->view('front_pages/dashboard/dash_header');
        $products['products'] = $this->product_model->getAll($id);
        $call_view = 'manage_products';
        if ($id != "") {
            $user_id = $this->session->userdata('user_id');
            $store_id = $this->session->userdata('store_id');
            $other_products = $this->db->query('select * from dmd_store_products where user_id="' .
                $user_id . '" and store_id="' . $store_id . '"
                and product_code!="' . $id . '"');
            $products['product_name'] = array();
            if ($other_products->num_rows() > 0) {
                $products['product_name'] = $other_products->result_array();
            }

            $call_view = 'addproduct';
        }
        $this->load->view('front_pages/dashboard/' . $call_view, $products);
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function featured(){
        $this->load->view('front_pages/dashboard/dash_header');
        $store_id = $this->session->userdata('store_id');
        $products['products'] = $this->product_model->getAllFeaturedProducts($store_id);

        $this->load->view('front_pages/dashboard/featured_products', $products);
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function addFeatured()
    {
        $this->load->view('front_pages/dashboard/dash_header');
        $store_id = $this->session->userdata('store_id');

        $data['products'] = $this->product_model->get_un_featured($store_id);

        $this->load->view('front_pages/dashboard/add_featured', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    public function updateFeatureProduct()
    {
     $product_id =  $_REQUEST['product_code'];
     $pro = $_REQUEST['pro'];
        // check fields validation
     if ($this->user_model->check_memory()) {
        $empty_message = "";

        foreach ($pro as $key => $val) {
            if ($val == "") {
                $empty_message .= str_replace("_"," ",$key) . " is required field<br>";
            }
        }

        if ($_REQUEST['old_file'] == "") {
            if ($_FILES['image']['name'] == '') {
                $empty_message .= "Product image is required field<br>";
            }

        }



        if ($empty_message != "") {
            $this->session->set_flashdata('error_msg', $empty_message);
            redirect('product/addFeatured/');
        } 
            // upload file
        //$_FILES['image']['name'];
        if ($_REQUEST['old_file'] == "") {
            $name_parts = pathinfo($_FILES['image']['name']);

                    //$name_full = preg_replace('/\s+/', '', $name_parts['image']);

            $file_ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            $file_name = uniqid() . '.' . $file_ext;

                //$file_name = uniqid() . '-' . $name_full;
            $config = array(
                'upload_path' => './site_assets/products',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'file_name' => $file_name,
                'max_size' => $this->user_model->check_memory("size") * 1024,
                'overwrite' => false,
            );

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $upload_data = $this->upload->data();
                $data_ary = array(
                    'title' => $upload_data['client_name'],
                    'file' => $upload_data['file_name'],
                    'width' => $upload_data['image_width'],
                    'height' => $upload_data['image_height'],
                    'type' => $upload_data['image_type'],
                    'size' => $upload_data['file_size'],
                    'path' => $upload_data['full_path'],
                    'date' => time(),
                );

                $data = array('upload_data' => $data_ary);
                $old_path = getcwd() . '/site_assets/products/' . $_REQUEST['old_file'];
                @unlink($old_path);

                $pro['feature_image'] = $data_ary['file'];
            } else {
                    //print_r($this->upload->display_errors());
                $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                redirect('product/featured/');

            }
        } 
        else {
            $pro['feature_image'] = $_REQUEST['old_file'];
            $pro['feature_memory_size'] = $upload_data['file_size'];
        } 
        
        $this->product_model->update_fetured_product($pro, $product_id);
        $this->session->set_flashdata('success_msg', 'Featured product successfully updated!');
        redirect('product/featured/');
    } else {
        $this->session->set_flashdata('error_msg_limit',
            'Your memory limit exceeded please upgrade your package Thanks');

        redirect('user/subscription');

    }
}

public function editFeature($code='')
{
    if ($code !='')
    {
        $this->load->view('front_pages/dashboard/dash_header');
        $product = $this->product_model->get_product($code);
        $data['product'] =$product;
        $this->load->view('front_pages/dashboard/edit_featured', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    else{
      $this->session->set_flashdata('error_msg', 'Something wrong while edit featured product!');
      redirect('product/featured/');
  }
  
}

public function RemoveFeature($product_code='')
{
    if ($product_code !=''){
        $product =array(
            'is_feature' => 0,
        );
        $this->product_model->update_fetured_product($product, $product_code);
        $this->session->set_flashdata('success_msg', 'Feature product remove successfully'); 
    }
    else{ 
        $this->session->set_flashdata('error_msg', 'Something wrong while rempoving feature product!'); 
    }
    redirect('product/featured/');                                                          

}
public function generateProCode($digits_needed = 10)
{


        $random_number = ''; // set up a blank string

        $count = 0;

        while ($count < $digits_needed) {
            $random_digit = mt_rand(0, 9);

            $random_number .= $random_digit;
            $count++;
        }
        return $random_number;
    }

    public function generateProCodeB($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }


    public function sendProduct()
    {
        // check fields validation
        $empty_message = "";
        $data = $_REQUEST['customer'];
        foreach ($data as $key => $val) {
            if ($val == "") {
                $empty_message .= $key . " is required field<br>";
            }
        }

        if ($empty_message != "") {
            $this->session->set_flashdata('error_msg', $empty_message);
            redirect('product/sendToDownload/' . $_REQUEST['product_id']);
        }
        // check customer exist or not
        $store_id = $this->session->userdata('store_id');

        // check customer exists or not
        $query = $this->db->query("select * from dmd_customers where email='" . $data['email'] .
            "'");

        //if exists get customer data from db
        $new_cus = false;
        if ($query->num_rows() > 0) {
            $customer = $query->row_array();
            /*$customer["password"] = $this->generateProCode();
            $this->db->where('id', $customer["id"]);
            $this->db->update("dmd_customers", array("password" => md5($customer["password"])));*/
        } else {
            $new_cus = true;
            // create customer
            $user_pass = $this->generateProCode();
            $customer = array(
                "f_name" => $data['f_name'],
                "l_name" => $data['l_name'],
                "email" => $data['email'],
                //"shipping_country" => $data['country'],
                "store_id" => $this->session->userdata('user_id'),
                "password" => md5($user_pass)
            );

            $insert = $this->db->insert('dmd_customers', $customer);
            if ($insert) {
                $customer['id'] = $this->db->insert_id();
                $customer['password'] = $user_pass;
                $this->new_cst_email($customer);
            } else {
                $this->session->set_flashdata('error_msg',
                    'Customer profile creation has been failed!');
                redirect('product/sendToDownload/' . $_REQUEST['product_id']);
            }
        }
        // send product to email
        $store_name = $this->session->userdata('store_name');
        $subject = 'Your purchase from ' . $store_name;
        // get store_url
        $store_url = $this->db->get_where('dmd_stores', array('id' => $store_id));
        $store_url = $store_url->row_array();
       /* $email_message = 'Hello <name>
        Your purchase from <storename> is complete<br/>
        View your purchase <a href="' . $store_url['store_url'] .
        '">Dashboard</a><br/>
        For questions contact storesupport@store.com<br/>
        You received this email from DMD because <storename> uses DMD as a<br/>
        content delivery partner. This is not a marketing email';*/

        $email_message = $this->load->view('front_pages/emtemp/send_product', '', true);
        $cust_name = $data['f_name'] . " " . $data['l_name'];
        $email_message = str_replace("{name}", $cust_name, $email_message);
        $email_message = str_replace("{storename}", $store_name, $email_message);
        $email_message = str_replace("{link}", $store_url['store_url'], $email_message);
        $to = $customer['email'];
        $getproduct = $this->db->get_where('dmd_store_products', array('store_id' => $store_id,
            'product_code' => $_REQUEST['product_id']));
        $getproduct = $getproduct->row_array();
        //echo $this->db->last_query();
        // ptoduct emails
        $this->product_email($_REQUEST['product_id'],  $to, $cust_name);
        //



        $orderData = array(
            //"transID" => ,
            "order_number" => time(),
            "customer_id" => $customer['id'],
            "store_id" => $customer["store_id"],
            'user_id' => $this->session->userdata('user_id'),
            //"shipping_country" => $customer['shipping_country'],
            "total_amount" => $getproduct['product_price'], //$postData['amount'],
            "grand_total" => $getproduct['product_price'], //$postData['amount'],
            "status" => 'paid',
            "created_date" => date('Y-m-d H:i:s'),
            "deliver_status" => 'delivered',
            "Payment_method" => "free");


        $insert_order = $this->db->insert('dmd_orders', $orderData);
        $order_id = $this->db->insert_id();
        if ($insert_order) {
            $order_items = array(
                'product_id' => $_REQUEST['product_id'],
                'order_id' => $order_id,
                'product_quantity' => 1,
                'customer_id' => $customer['id'],
                'product_price' => $getproduct['product_price'],
                //'coupon_discount' => $products['discount'],
                'total_price' => ($getproduct['product_price']));
            $insert_items = $this->db->insert('dmd_order_items', $order_items);
            $this->callConfig();
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($email_message);
            //print($this->email);die();
            $this->email->send();
//
            $this->callConfig();
            $deivery_message = $this->load->view('front_pages/emtemp/delivery_email', '', true);
            $deivery_message = str_replace("{content}", $data['message'], $deivery_message);
            $customer_name = $data['f_name'].' '.$data['l_name'];
            $deivery_message = str_replace("{name}", $customer_name, $deivery_message);
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($deivery_message);
                //print($this->email);die();
            $this->email->send();
//

            $EmailData = array(
                'email' => $customer["email"], 
                'type' => "purchase_thankyou",
                'store_id' => $customer["store_id"]
            );
            $this->final_email($EmailData);


            $this->session->set_flashdata('email', $customer['email']);
            $this->session->set_flashdata('success_msg', 'Product has been delivered');
            redirect('product/sendToDownload/' . $_REQUEST['product_id']);

        }


    }
    public function product_email($product_id, $cuemail, $customer_name='')
    {

        $email = $this->db->query('select * from dmd_email_delivery_pro where product_id="' .
            $product_id . '"'); //$this->user_model->get_email($data["type"]);
        if ($email->num_rows() > 0) {
            foreach ($email->result_array() as $email) {
              $this->callConfig();

              $message = $this->load->view('front_pages/emtemp/delivery_email', '', true);
              $message = str_replace("{content}", $email["email_desp"], $message);
              $message = str_replace("{name}", $customer_name, $message); 
              $subject = $email["subject"];
              $to = $cuemail;

              $this->email->to($to);
              $this->email->subject($subject);
              $this->email->message($message);
                //print($this->email);die();
              $this->email->send();
              $this->email->clear(TRUE);

          }
      }
  }
  public function final_email($data)
  {
    $this->callConfig();

    $email = $this->user_model->get_email($data["type"],$data["store_id"]);

    if ($email) {
        $subject = $email["subject"];
        $message = $email["content"];
        $to = $data['email'];
    }

        //print_r($email);die();

    $this->email->to($to);
    $this->email->subject($subject);
    $this->email->message($message);
        //print($this->email);die();
    $this->email->send();
        //$this->email->get_debugger_messages();die();
}
public function new_cst_email($customer)
{
    $customer["store_name"] = $this->session->userdata("store_name");
    $store_url = $_SERVER['HTTP_HOST'] . "/customer/login";

    $to = $customer["email"];
    $subject = "Welcome to Our Community";

        /*$message = "";
        $message .= "To sign in our site, use these credentials during checkout or on the <a href='" .
        $store_url . "'>Dashboard</a> page:";
        $message .= '<br>';
        $message .= "<b>Email: </b>";
        $message .= $to;
        $message .= '<br>';
        $message .= "<b>Password: </b>";
        $message .= $customer["password"];
        $message .= '<br><br>';
        $message .= '<p> You are receiving this email from DMD because <b>' . $customer["store_name"] .
        '</b> uses our content delivery platform to deliver files to customers.</p>'; */

        $message = $this->load->view('front_pages/emtemp/new_customer', '', true);
        $store_url = $_SERVER['HTTP_HOST'] . "/customer/login";

        $message = str_replace("{store_url}",  $store_url, $message);
        $message = str_replace("{store_name}", $customer["store_name"], $message);
        $message = str_replace("{password}", $customer["password"], $message);
        $message = str_replace("{email}", $to, $message);
        

        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
    }

    public function sendProduct2()
    {
        // check fields validation
        $empty_message = "";
        $product = $_REQUEST['customer'];
        foreach ($product as $key => $val) {
            if ($val == "") {
                $empty_message .= $key . " is required field<br>";
            }
        }

        if ($empty_message != "") {
            $this->session->set_flashdata('error_msg', $empty_message);
            redirect('product/sendToDownload/' . $_REQUEST['product_id']);
        }
        // check customer exist or not
        $store_id = $this->session->userdata('store_id');

        $query = $this->db->query('select * from dmd_customers where email="' . $product['email'] .
            '" and store_id="' . $store_id . '"');
        if ($query->num_rows() > 0) {
            $product = $query->row_array();
        } else {
            // save customer
            $product['user_id'] = $this->session->userdata('user_id');
            $product['store_id'] = $store_id;

            $query = $this->db->insert('dmd_customers', $product);
            $product['id'] = $this->db->insert_id;

        }
        // send product to email
        $store_name = $this->session->userdata('store_name');
        $subject = 'Your purchase from ' . $store_name;

        $email_message .= 'Hello <name>
        Your purchase from <storename> is complete
        View invoice- btn (missing design)
        View your purchase �btn
        For questions contact storesupport@store.com
        --
        You received this email from DMD because <storename> uses DMD as a
        content delivery partner. This is not a marketing email hello';
        //$button_activate = '<a href="' . base_url() . '/user/emailVerify/' . $email_check['email_verification_code'] .
        //   '" class="button">Activate Your Account</a>';
        //$link_activate = base_url() . '/user/emailVerify/' . $product['email_verification_code'];



        $email_message = str_replace("<name>", $product['display_name'], $email_message);
        $email_message = str_replace("<storename>", $store_name, $email_message);
        //$email_message = str_replace("<link_activation>", $link_activate, $email_message);
        //$email_message = str_replace("<email>", $email_check['email'], $email_message);
        // get felivery file

        $getfile = $this->db->get_where('dmd_delivery_for_pro', array('store_id' => $store_id,
            'product_id' => $_REQUEST['product_id']));
        $getfile = $getfile->result_array();

        $getproduct = $this->db->get_where('dmd_store_products', array('store_id' => $store_id,
            'id' => $_REQUEST['product_id']));
        $getproduct = $getproduct->row_array();

        $to = $product['email'];
        $path = base_url() . 'site_assets/products/' . $_REQUEST['product_id'] . '/';
        // boundary
        $semi_rand = md5(time());
        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

        // headers for attachment
        $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" .
        " boundary=\"{$mime_boundary}\"";
        $email_message .= "--{$mime_boundary}\n";
        foreach ($getfile as $files) {

            $file = $path . $files['upload_file'];
            $fname = basename($file);
            $content = file_get_contents($file);
            $data = chunk_split(base64_encode($content));
            $email_message .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$fname\"\n" .
            "Content-Disposition: attachment;\n" . " filename=\"$files[$x]\"\n" .
            "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
            $email_message .= "--{$mime_boundary}\n";
        }
        // purchase script
        $data = array(
            'customer_id' => $product['id'],
            "store_id" => $store_id,
            "shipping_country" => $product['shipping_country'],
            'total_amount' => $getproduct['product_price'],
            'grand_total' => $getproduct['product_price'],
            'deliver_status' => 'delivered');

        $create_order = $this->db->insert('dmd_orders', $data);
        if ($create_order) {


            mail($to, $subject, $email_message, $headers);
        }

        $this->session->set_flashdata('email', $product['email']);
        $this->session->set_flashdata('success_msg', 'Product has been delivered');
        redirect('product/sendToDownload/' . $_REQUEST['product_id']);


    }
    public function update()
    {
        //print_r($_REQUEST);die();
        if (isset($_REQUEST['create_product'])) {

            $save_id = $_REQUEST['save_id'];

            $product = $_REQUEST['product'];
            
            $is_download = @$_REQUEST['is_download'];

            $product['product_name'] = $product['product_name'];

            $slug = preg_replace("/-$/", "", preg_replace('/[^a-z0-9]+/i', "-", strtolower($product['product_name'])));
            $query = "SELECT COUNT(*) AS NumHits FROM dmd_store_products WHERE  product_slug  LIKE '$slug%'";
            $result = $this->db->query($query);
            $row = $result->row_array();
            $numHits = $row['NumHits'];

            $use_slug = ($numHits > 1) ? ($slug . '-' . $numHits) : $slug;

            $product['product_slug'] = $use_slug;
            $product['product_price'] = (float)$product['product_price'];
            // check fields validation
            $duration = 0;
            $empty_message = "";
            if ($_REQUEST['old_image'] == "") {
                if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] == '') {
                    $empty_message .= "Product Image is required field<br>";
                }
            }//old_webinar
            if(@$_REQUEST['is_webinar']){
                if ($_REQUEST['old_image'] == "") {
                   if($_FILES['webinarfile']['name'] ==''){ 
                       $empty_message .= "Webinar video is required field<br>";
                   }
               }
           }
           $hour = $this->input->post('hour');
           $min  =  $this->input->post('minute');
           $sec  = $this->input->post('second');
           if($hour > 0){
            $hour = $hour*60*60; 
        }
        if($min > 0){
           $min = $min*60;
       }

       $duration = $hour+$min+$sec;

       foreach ($product as $key => $val) {
        if ($val == "") {
            $empty_message .= $key . " is required field<br>";
        }
    }

    if($product['product_price'] == 0){
        $empty_message .= "price should be greater than zero<br>";
    }

    if ($empty_message != "") {
        $this->session->set_flashdata('error_msg', $empty_message);
        $product['save_id'] = $save_id;
        $product['pro_image'] = $_REQUEST['old_image_path'];
        $product['product_slug'] = $_REQUEST['slug_name'];

        $this->session->set_flashdata('fields', $product);
        redirect('product/' . $save_id);
                //redirect('product/add');
    }

    if ($_REQUEST['old_image'] == "") {

                // upload product image
        if ($this->user_model->check_memory()) {
            $name_parts = pathinfo($_FILES['userfile']['name']);

            $name_full = preg_replace('/\s+/', '', $name_parts['filename']);
            $file_ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
            $productCode = $save_id;
            $file_name = $productCode . '-' . uniqid() . '.' . $file_ext;
            $config = array(
                'upload_path' => './site_assets/products',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'file_name' => $file_name,
                'max_size' => $this->user_model->check_memory("size") * 1024,
                'width'         =>  300,
                'height'        => 300,
                'overwrite' => false,
            );


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('userfile')) {

                $upload_data = $this->upload->data();

                $data_ary = array(
                    'title' => $upload_data['client_name'],
                    'file' => $upload_data['file_name'],
                    'width' => $upload_data['image_width'],
                    'height' => $upload_data['image_height'],
                    'type' => $upload_data['image_type'],
                    'size' => $upload_data['file_size'],
                    'path' => $upload_data['full_path'],
                    'date' => time(),
                );

                $data = array('upload_data' => $data_ary);
                        // delete old image path
                $old_path = getcwd() . '/site_assets/products/' . $_REQUEST['old_image_path'];
                unlink($old_path);
                $old_path2 = getcwd() . '/site_assets/products/' . $_REQUEST['old_image_thumb'];
                unlink($old_path2);
                $product['pro_image'] = $data_ary['file'];
                $resize = $this->do_resize($data_ary['file']);
                if ($resize) {
                    $expo_thumb = explode(".", $data_ary['file']);
                    $product['pro_image_thumb'] = $expo_thumb[0] . '_thumb' . '.' . $expo_thumb[1];
                }
                $product['memory_size'] = $upload_data['file_size'];
                $product['media_date'] = date('Y-m-d');
                        //$this->newsModel->addNews($ad_ne_data);
                        // save data to db


            } else {
                        //print_r($this->upload->display_errors());
                $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                redirect('product/' . $save_id);

            }
        } else {
            $this->session->set_flashdata('error_msg_limit',
                'Your memory limit exceeded please upgrade your package Thanks');
            $product['pro_image'] = $_REQUEST['old_image_path'];
                    //redirect('user/subscription');

        }
    } else {

        $product['pro_image'] = $_REQUEST['old_image'];
    }




             //video uploading
    if ($_REQUEST['old_webinar'] == "") {
        if(@$_REQUEST['is_webinar']){
            $config['upload_path']    = './site_assets/products/';
            $config['allowed_types']  = '*';
            $new_name = time()."_".str_replace(' ', '', $_FILES['webinarfile']['name']);
            $config['file_name'] = $new_name;
            $config['max_size'] = (float)$this->user_model->check_memory("size") * 1024;
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('webinarfile'))
            {
                $webinar_data = $this->upload->data();
                $product['webinar_video'] = $webinar_data['file_name'];
                $product['webinar_memory_size'] = $webinar_data['file_size'];
                $product['is_webinar'] = '1';

            }else{
                $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                redirect('product/' . $save_id);
            }
        }
    }
            //end video uploading




    $user_id = $this->session->userdata('user_id');
    $product['webinar_play_duration'] = $duration;
    $product['user_id'] = $user_id;
    $product['upsel_product'] = json_encode(@$_REQUEST['upsel_product']);
    $product['cross_sell_product'] = json_encode(@$_REQUEST['cross_sell_product']);
    $product['save_enable'] = @$_REQUEST['save_enable'];
    $product['is_download'] = $is_download;
    $this->product_model->update_product($product, $save_id);

}
}
public function do_resize($filename)
{

    $source_path = $_SERVER['DOCUMENT_ROOT'] . '/site_assets/products/' . $filename;
    $target_path = $_SERVER['DOCUMENT_ROOT'] . '/site_assets/products/';
    $config_manip = array(
        'image_library' => 'gd2',
        'source_image' => $source_path,
        'new_image' => $target_path,
        'maintain_ratio' => true,
        'create_thumb' => true,
        'thumb_marker' => '_thumb',
        'width' => 150,
        'height' => 150);
    $this->load->library('image_lib', $config_manip);
    if (!$this->image_lib->resize()) {
        echo $this->image_lib->display_errors();
    } else {
        return true;
    }
        // clear //
    $this->image_lib->clear();
}
public function check_product($id)
{
    $qrystr = "SELECT id FROM dmd_store_products  WHERE product_code= " . $id;
    $qryresult = $this->db->query($qrystr);
    $product = $qryresult->row_array();
    if (isset($product['id']) && $product['id'] != '') {
        return true;
    } else {
        return false;
    }
}
public function archived($id)
{
    $check_pro = $this->check_product($id);
    if ($check_pro) {
        $data = array("product_visibility" => 'archived');
        $this->db->where("product_code", $id);
        $archive = $this->db->update("dmd_store_products", $data);
        if ($archive) {

            $this->session->set_flashdata('success_msg',
                'your product successfully archived!');
            redirect('product');
        } else {
            $this->session->set_flashdata('error_msg', 'try again something going wrong');
            redirect('product/');
        }
    } else {
        $this->session->set_flashdata('error_msg',
            'try again this product not exists in our system');
        redirect('product/');
    }
}
public function try1(){

}
function removeDirectory($path) {
  $files = glob($path . '/*');
  foreach ($files as $file) {
      is_dir($file) ? removeDirectory($file) : unlink($file);
  }
  rmdir($path);
  return;
}
public function delete($id)
{
    $qrystr = "SELECT * FROM dmd_store_products  WHERE product_code= " . $id;
    $qryresult = $this->db->query($qrystr);
    $product = $qryresult->row_array();
    if (isset($product['id']) && $product['id'] != '') {
        $delete = $this->db->delete("dmd_store_products", array('product_code' => $id));
        if ($delete) {
                // delete old image path
            $old_path = getcwd() . '/site_assets/products/' . $product['pro_image'];
            unlink($old_path);
            $delete_files = $this->db->delete("dmd_delivery_for_pro", array('product_id' => $id));
            $delete_emails = $this->db->delete("dmd_email_delivery_pro", array('product_id' => $id));
            $path = "./site_assets/products/".$id;
            $this->removeDirectory($path);

            $this->session->set_flashdata('success_msg',
                'your product successfully deleted!');
            redirect('product');
        } else {
            $this->session->set_flashdata('error_msg', 'try again something going wrong');
            redirect('product/');
        }
    } else {
        $this->session->set_flashdata('error_msg',
            'try again this product not exists in our system');
        redirect('product/');
    }
}

public function duplicate($id)
{
    if ($this->chCountp() != 'out') {
        $qrystr = "SELECT * FROM dmd_store_products  WHERE product_code= " . $id;
        $qryresult = $this->db->query($qrystr);
        $product = $qryresult->row_array();
        if (isset($product['id']) && $product['id'] != '') {
                unset($product['id']); //Remove ID from array
                unset($product['created_product']);

                // make slug of duplicate
                $slug = preg_replace("/-$/", "", preg_replace('/[^a-z0-9]+/i', "-", strtolower($product['product_name'])));
                $query = "SELECT COUNT(*) AS NumHits FROM dmd_store_products WHERE  product_slug  LIKE '$slug%'";
                $result = $this->db->query($query);
                $row = $result->row_array();
                $numHits = $row['NumHits'];

                $use_slug = ($numHits > 0) ? ($slug . '-' . $numHits) : $slug;
                $product['product_slug'] = $use_slug;
                $productCode = $this->generateProCode(8);
                $expo_file = explode($product['product_code'], $product['pro_image']);
                $product['product_code'] = $productCode;
                $file_path = $product['product_code'] . $expo_file[1];
                $contents = file_get_contents(base_url() . "site_assets/products/" . $product['pro_image']);
                $save_path = getcwd() . '/site_assets/products/' . $file_path;
                file_put_contents($save_path, $contents);


                $product['pro_image'] = $file_path;
                $resize = $this->do_resize($file_path);
                if ($resize) {
                    $expo_thumb = explode(".", $file_path);
                    // thumb copy
                    $contents2 = file_get_contents(base_url() . "site_assets/products/" . $product['pro_image_thumb']);
                    $save_path2 = getcwd() . '/site_assets/products/' . $expo_thumb[0] . '_thumb' .
                    '.' . $expo_thumb[1];
                    file_put_contents($save_path2, $contents2);


                    $product['pro_image_thumb'] = $expo_thumb[0] . '_thumb' . '.' . $expo_thumb[1];
                }


                /*$qrystr = " INSERT INTO dmd_store_products";
                $qrystr .= " ( " . implode(", ", array_keys($product)) . ") ";
                $qrystr .= " VALUES ('" . implode("', '", array_values($product)) . "')";*/
                $runq = $this->db->insert("dmd_store_products", $product);
                if ($runq) {
                    $this->session->set_flashdata('success_msg',
                        'your duplicate product successfully created!');
                    redirect('product');
                } else {
                    $this->session->set_flashdata('error_msg', 'try again something going wrong');
                    redirect('product/');
                }
            } else {
                $this->session->set_flashdata('error_msg',
                    'try again this product not exists in our system');
                redirect('product/');
            }

        } else {
            $this->session->set_flashdata('error_msg_limit',
                'Your product limit exceeded please upgrade your package Thanks');

            redirect('user/subscription');

        }
    }

    public function updatefile()
    {
        if ($this->user_model->check_memory()) {
            // check fields validation
            $product = $_REQUEST['product'];
            $check_pro = $this->check_product($product['product_id']);
            if ($check_pro) {


                $empty_message = "";
                if ($_REQUEST['old_file'] == "") {
                    if ($_FILES['userfile']['name'] == '') {
                        $empty_message .= "File is required field<br>";
                    }
                }
                foreach ($product as $key => $val) {
                    if ($val == "") {
                        $empty_message .= $key . " is required field<br>";
                    }
                }

                if ($empty_message != "") {
                    $this->session->set_flashdata('error_msg', $empty_message);
                    $this->session->set_flashdata('fields', $product);
                    redirect('product/addFile/' . $product['product_id']);
                }
                // upload file
                if ($_REQUEST['old_file'] == "") {
                    $name_parts = pathinfo($_FILES['userfile']['name']);

                    $name_full = preg_replace('/\s+/', '', $name_parts['filename']);
                    $file_ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
                    $file_name = $product['product_id'] . '-' . uniqid() . '.' . $file_ext;
                    //$file_name = $product['product_id'] . '-' . $name_full;
                    if (!file_exists('./site_assets/products/' . $product['product_id'])) {
                        mkdir('./site_assets/products/' . $product['product_id'], 0777, true);
                    }
                    $config = array(
                        'upload_path' => './site_assets/products/' . $product['product_id'],
                        'allowed_types' => 'gif|jpg|png|jpeg|txt|pdf|mp4|mp3|ogg|doc|docx|xls|xlsx|ppt|pptx|csv|wav|wmv|avi|mov|mpg|rar|zip|xml|tif|tiff|html|js|ods',
                        'file_name' => $file_name,
                        'max_size' => $this->user_model->check_memory("size") * 1024,
                        'overwrite' => false,
                    );

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('userfile')) {

                        $upload_data = $this->upload->data();

                        $data_ary = array(
                            'title' => $upload_data['client_name'],
                            'file' => $upload_data['file_name'],
                            'width' => $upload_data['image_width'],
                            'height' => $upload_data['image_height'],
                            'type' => $upload_data['image_type'],
                            'size' => $upload_data['file_size'],
                            'path' => $upload_data['full_path'],
                            'date' => time(),
                        );

                        $data = array('upload_data' => $data_ary);
                        $old_path = getcwd() . '/site_assets/products/' . $product['product_id'] . '/' .
                        $_REQUEST['old_image_path'];
                        unlink($old_path);


                        //$this->newsModel->addNews($ad_ne_data);
                        // save data to db

                        $product['upload_file'] = $data_ary['file'];
                        $product['upload_type'] = $upload_data['file_type'];
                        $product['memory_size'] = $upload_data['file_size'];
                        $product['media_date'] = date('Y-m-d');
                    } else {
                        //print_r($this->upload->display_errors());
                        $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                        redirect('product/addFile/' . $product['product_id']);

                    }
                } else {
                    $product['upload_file'] = $_REQUEST['old_file'];
                }


                $store_id = $this->session->userdata('store_id');
                $product['store_id'] = $store_id;


                $this->db->where('id', $_REQUEST['file_id']);
                $this->db->update("dmd_delivery_for_pro", $product);
                $this->session->set_flashdata('success_msg',
                    'File has been successfully updated!');
                redirect('product/PurchaseAction/' . $product['product_id']);


            } else {
                $this->session->set_flashdata('error_msg',
                    'try again this product not exists in our system');
                redirect('product/');
            }
        } else {
            $this->session->set_flashdata('error_msg_limit',
                'Your memory limit exceeded please upgrade your package Thanks');

            redirect('user/subscription');

        }


    }
    public function updatemail()
    {
        // check fields validation
        $product = $_REQUEST['product'];
        $check_pro = $this->check_product($product['product_id']);
        if ($check_pro) {


            $empty_message = "";
            foreach ($product as $key => $val) {
                if ($val == "") {
                    $empty_message .= $key . " is required field<br>";
                }
            }

            if ($empty_message != "") {
                $this->session->set_flashdata('error_msg', $empty_message);
                $this->session->set_flashdata('fields', $product);
                redirect('product/addMail/' . $product['product_id']);
            }
            //$this->product_model->create_product_file($product);
            $this->db->where('id', $_REQUEST['file_id']);
            $this->db->update('dmd_email_delivery_pro', $product);
            $this->session->set_flashdata('success_msg',
                'email has been successfully updated!');
            redirect('product/PurchaseAction/' . $product['product_id']);


        } else {
            $this->session->set_flashdata('error_msg',
                'try again this product not exists in our system');
            redirect('product/addMail/' . $product['product_id']);
        }


    }
    public function savemail()
    {
        // check fields validation
        $product = $_REQUEST['product'];
        $check_pro = $this->check_product($product['product_id']);
        if ($check_pro) {


            $empty_message = "";
            foreach ($product as $key => $val) {
                if ($val == "") {
                    $empty_message .= $key . " is required field<br>";
                }
            }

            if ($empty_message != "") {
                $this->session->set_flashdata('error_msg', $empty_message);
                $this->session->set_flashdata('fields', $product);
                redirect('product/addMail/' . $product['product_id']);
            }
            $store_id = $this->session->userdata('store_id');
            $product['store_id'] = $store_id;


            //$this->product_model->create_product_file($product);
            $this->db->insert('dmd_email_delivery_pro', $product);
            $this->session->set_flashdata('success_msg',
                'New email has been successfully added!');
            redirect('product/PurchaseAction/' . $product['product_id']);


        } else {
            $this->session->set_flashdata('error_msg',
                'try again this product not exists in our system');
            redirect('product/');
        }


    }
    public function updatecheckout()
    {
        // check fields validation
        if ($this->user_model->check_memory()) {
            $empty_message = "";
            if ($_REQUEST['old_file'] == "") {
                if ($_FILES['userfile']['name'] == '') {
                    $empty_message .= "File is required field<br>";
                }
            }


            if ($empty_message != "") {
                $this->session->set_flashdata('error_msg', $empty_message);
                redirect('store/checkoutPage/');
            }
            // upload file
            if ($_REQUEST['old_file'] == "") {
                $name_parts = pathinfo($_FILES['userfile']['name']);

                $name_full = preg_replace('/\s+/', '', $name_parts['filename']);

                $file_ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
                $file_name = uniqid() . '.' . $file_ext;

                //$file_name = uniqid() . '-' . $name_full;
                $config = array(
                    'upload_path' => './site_assets/stores/' . $product['product_id'],
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'file_name' => $file_name,
                    'max_size' => $this->user_model->check_memory("size") * 1024,
                    'overwrite' => false,
                );

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('userfile')) {

                    $upload_data = $this->upload->data();

                    $data_ary = array(
                        'title' => $upload_data['client_name'],
                        'file' => $upload_data['file_name'],
                        'width' => $upload_data['image_width'],
                        'height' => $upload_data['image_height'],
                        'type' => $upload_data['image_type'],
                        'size' => $upload_data['file_size'],
                        'path' => $upload_data['full_path'],
                        'date' => time(),
                    );

                    $data = array('upload_data' => $data_ary);
                    $old_path = getcwd() . '/site_assets/stores/' . $_REQUEST['old_image_path'];
                    unlink($old_path);

                    $product['image'] = $upload_data['file_name'];
                    $product['memory_size'] = $upload_data['file_size'];
                    $product['media_date'] = date('Y-m-d');
                } else {
                    //print_r($this->upload->display_errors());
                    $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                    redirect('store/checkoutPage/');

                }
            } else {
                $product['image'] = $_REQUEST['old_file'];
            }

            $product['display'] = $_REQUEST['display'];


            $store_id = $this->session->userdata('store_id');
            $product['store_id'] = $store_id;


            $this->db->where('store_id', $store_id);
            $this->db->update("checkout_customization", $product);

            $store = array(
                'logo' => $product['image']
            );
            $this->store_model->update_store_customization($store,$store_id);
            
            // $checkout_logo['image'] =   $product['image'],
            // $checkout_logo['memory_size'] = $upload_data['file_size'];
            // $checkout_logo['media_date'] = date('Y-m-d');
            // $this->store_model->update_checkout_customization($checkout_logo,$store_id); 

            $this->session->set_flashdata('success_msg', 'Store Logo successfully updated.');
            redirect('store/checkoutPage/');
        } else {
            $this->session->set_flashdata('error_msg_limit',
                'Your memory limit exceeded please upgrade your package Thanks');

            redirect('user/subscription');

        }


    }
    public function savecheckout()
    {
        // check fields validation
        if ($this->user_model->check_memory()) {
            $empty_message = "";
            if ($_FILES['userfile']['name'] == '') {
                $empty_message .= "File is required field<br>";
            }
            if ($empty_message != "") {
                $this->session->set_flashdata('error_msg', $empty_message);
                redirect('store/checkoutPage/');
            }
            // upload file
            $name_parts = pathinfo($_FILES['userfile']['name']);

            $name_full = preg_replace('/\s+/', '', $name_parts['filename']);
            $file_ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
            $file_name = uniqid() . '.' . $file_ext;
            //$file_name = uniqid() . '-' . $name_full;

            $config = array(
                'upload_path' => './site_assets/stores/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'file_name' => $file_name,
                'max_size' => $this->user_model->check_memory("size") * 1024,
                'overwrite' => false,
            );

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('userfile')) {

                $upload_data = $this->upload->data();

                $data_ary = array(
                    'title' => $upload_data['client_name'],
                    'file' => $upload_data['file_name'],
                    'width' => $upload_data['image_width'],
                    'height' => $upload_data['image_height'],
                    'type' => $upload_data['image_type'],
                    'size' => $upload_data['file_size'],
                    'path' => $upload_data['full_path'],
                    'date' => time(),
                );

                $data = array('upload_data' => $data_ary);


                //$this->newsModel->addNews($ad_ne_data);
                // save data to db

                $product['image'] = $data_ary['file'];
                $product['memory_size'] = $upload_data['file_size'];
                $product['media_date'] = date('Y-m-d');
                if (isset($_REQUEST['display'])) {
                    $product['display'] = $_REQUEST['display'];
                }


                $store_id = $this->session->userdata('store_id');
                $product['store_id'] = $store_id;
                $user_id = $this->session->userdata('user_id');
                $product['user_id'] = $user_id;




                //$this->product_model->create_product_file($product);
                $this->db->insert('checkout_customization', $product);

                $store = array(
                    'logo' => $product['image']
                );
                $this->store_model->update_store_customization($store,$store_id);


                $this->session->set_flashdata('success_msg', 'Store Logo successfully updated.');
                redirect('store/checkoutPage/');
            } else {
                //print_r($this->upload->display_errors());
                $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                redirect('store/checkoutPage/');

            }
        } else {
            $this->session->set_flashdata('error_msg_limit',
                'Your memory limit exceeded please upgrade your package Thanks');

            redirect('user/subscription');

        }

    }
    public function savefile()
    {
        $product = $_REQUEST['product'];
        if (isset($product['product_id']) && $product['product_id'] != "") {
            // check fields validation
            if ($this->user_model->check_memory()) {
                $check_pro = $this->check_product($product['product_id']);
                if ($check_pro) {


                    $empty_message = "";
                    if ($_FILES['userfile']['name'] == '') {
                        $empty_message .= "File is required field<br>";
                    }
                    foreach ($product as $key => $val) {
                        if ($val == "") {
                            $empty_message .= $key . " is required field<br>";
                        }
                    }

                    if ($empty_message != "") {
                        $this->session->set_flashdata('error_msg', $empty_message);
                        $this->session->set_flashdata('fields', $product);
                        redirect('product/addFile/' . $product['product_id']);
                    }
                    // upload file
                    $name_parts = pathinfo($_FILES['userfile']['name']);

                    $name_full = preg_replace('/\s+/', '', $name_parts['filename']);
                    $file_ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
                    $file_name = $product['product_id'] . '-' . uniqid() . '.' . $file_ext;
                    //$file_name = $product['product_id'] . '-' . $name_full;
                    if (!file_exists('./site_assets/products/' . $product['product_id'])) {
                        mkdir('./site_assets/products/' . $product['product_id'], 0777, true);
                    }
                    //'gif|jpg|png|jpeg|txt|pdf|mp4|mp3|ogg|doc|docx|xls|xlsx|ppt|pptx|csv', old files
                    $config = array(
                        'upload_path' => './site_assets/products/' . $product['product_id'],
                        'allowed_types' => 'gif|jpg|png|jpeg|txt|pdf|mp4|mp3|ogg|doc|docx|xls|xlsx|ppt|pptx|csv|wav|wmv|avi|mov|mpg|rar|zip|xml|tif|tiff|html|js|ods',
                        'file_name' => $file_name,
                        'max_size' => ($this->user_model->check_memory("size")*1024),
                        'overwrite' => false,
                    );

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('userfile')) {

                        $upload_data = $this->upload->data();

                        $data_ary = array(
                            'title' => $upload_data['client_name'],
                            'file' => $upload_data['file_name'],
                            'width' => $upload_data['image_width'],
                            'height' => $upload_data['image_height'],
                            'type' => $upload_data['image_type'],
                            'size' => $upload_data['file_size'],
                            'path' => $upload_data['full_path'],
                            'date' => time(),
                        );

                        $data = array('upload_data' => $data_ary);


                        //$this->newsModel->addNews($ad_ne_data);
                        // save data to db

                        $product['upload_file'] = $data_ary['file'];
                        $product['upload_type'] = $upload_data['file_type'];
                        $product['memory_size'] = $upload_data['file_size'];
                        $product['media_date'] = date('Y-m-d');

                        $store_id = $this->session->userdata('store_id');
                        $user_id = $this->session->userdata('user_id');
                        $product['store_id'] = $store_id;
                        $product['user_id'] = $user_id;


                        $this->product_model->create_product_file($product);
                        $this->session->set_flashdata('success_msg',
                            'New file has been successfully added!');
                        redirect('product/PurchaseAction/' . $product['product_id']);
                    } else {
                        //print_r($this->upload->display_errors());
                        $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                        redirect('product/addFile/' . $product['product_id']);

                    }
                } else {
                    $this->session->set_flashdata('error_msg',
                        'try again this product not exists in our system');
                    redirect('product/');
                }
            } else {
                $this->session->set_flashdata('error_msg_limit',
                    'Your memory limit exceeded please upgrade your package Thanks');

                redirect('user/subscription');

            }
        } else {
            $this->session->set_flashdata('error_msg_limit',
                'Your uploaded file limit exceeded or not select product');
            redirect('product/');
        }


    }
    public function chCountp()
    {
        // check product excceded or not
        // get uploaded products by user
        $products = $this->db->query('select count(*) as totpro from dmd_store_products where user_id="' .
            $this->session->userdata('user_id') . '"');
        $totpro = $products->row_array();
        $totpro = $totpro['totpro'];
        // get default package id
        $default_pack = $this->db->get_where('store_package', array('user_id' => $this->
            session->userdata('user_id')));
        $default_pack = $default_pack->row_array();

        $depackage = $this->db->get_where('packages', array('pack_code' => $default_pack['package_id']));
        $depackage = $depackage->row_array();
        if ($totpro >= $depackage['pack_products']) {
            return "out";
        } else {
            return "fine";
        }
    }

    public function add(){

        $store_id = $this->session->userdata('store_id');
        if (!isset($store_id) && $store_id == "") {
            redirect('user/Dashboard');
        }
        if ($this->user_model->check_memory()) {
            if ($this->chCountp() != 'out') {
                if (isset($_REQUEST['create_product'])) {

                 $productCode = $this->generateProCode(8);
                 $product = $_REQUEST['product'];
                 $product['product_code'] = $productCode;
                 $product['product_name'] = $product['product_name'];
                 $slug = preg_replace("/-$/", "", preg_replace('/[^a-z0-9]+/i', "-", strtolower($product['product_name'])));
                 $query = "SELECT COUNT(*) AS NumHits FROM dmd_store_products WHERE  product_slug  LIKE '$slug%'";
                 $result = $this->db->query($query);
                 $row = $result->row_array();
                 $numHits = $row['NumHits'];

                 $use_slug = ($numHits > 0) ? ($slug . '-' . $numHits) : $slug;

                 $product['product_slug'] = $use_slug;
                 $product['product_price'] = (float)$product['product_price'];


                 $empty_message = "";
                 if ($_FILES['userfile']['name'] == '') {
                    $empty_message .= "Product Image is required field<br>";
                }

                foreach ($product as $key => $val) {
                    if ($val == "") {
                        $empty_message .= $key . " is required field<br>";
                    }
                }

                if($product['product_price'] == 0){
                    $empty_message .= "price should be greater than zero<br>";
                }

                if ($empty_message != "") {
                    $this->session->set_flashdata('error_msg', $empty_message);
                    $this->session->set_flashdata('fields', $product);
                    redirect('product/add');
                }
                $product_check = $this->product_model->product_check($slug);



                $config['upload_path']   = './site_assets/products/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['file_name'] =  time().(preg_replace('/[^A-Za-z0-9]/', "", $_FILES["userfile"]['name']));
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('userfile')){
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error_msg', 'Image Upload: ' . $error);
                    $this->session->set_flashdata('fields', $product);

                    redirect("Product/add");
                }else {

                    $upload_data = $this->upload->data();

                    $data_ary = array(
                        'title' => $upload_data['client_name'],
                        'file' => $upload_data['file_name'],
                        'width' => $upload_data['image_width'],
                        'height' => $upload_data['image_height'],
                        'type' => $upload_data['image_type'],
                        'size' => $upload_data['file_size'],
                        'path' => $upload_data['full_path'],
                        'date' => time(),
                    );

                    $data = array('upload_data' => $data_ary);
                    $product['pro_image'] = $data_ary['file'];
                    $product['memory_size'] = $upload_data['file_size'];
                    $product['media_date'] = date('Y-m-d');
                        // $resize = $this->do_resize($data_ary['file']);
                        // if ($resize) {
                        //     $expo_thumb = explode(".", $data_ary['file']);
                        //     $product['pro_image_thumb'] = $expo_thumb[0] . '_thumb' . '.' . $expo_thumb[1];
                        // }
                    $user_id = $this->session->userdata('user_id');
                    $product['user_id'] = $user_id;
                    $product['save_enable'] = @$_REQUEST['save_enable'];

                    $product['cross_sell_product'] = json_encode($this->input->post('cross_sell_product'));
                    $product['upsel_product'] = json_encode($this->input->post('upsel_product'));

                    $product['is_download'] = @$_REQUEST['is_download'];
                    foreach ($product as $key => $val) {
                        if ($val == "") {
                            if ($key == "save_enable") {
                                $product[$key] = 0;
                            } else {
                                $product[$key] = "";
                            }
                        }
                    }
                    $this->product_model->create_product($product);
                    $this->session->set_flashdata('success_msg',
                        'Your product successfully created. Add product files now!');
                    redirect(base_url('Tutorial/add_webinar/') . $product['product_code']);




                }     


            }
            $this->load->view('front_pages/dashboard/dash_header');
            $products['product_name'] = $this->product_model->getAll();
            $this->load->view('front_pages/dashboard/addproduct',$products);
            $this->load->view('front_pages/dashboard/dash_footer');

        } else {
            $this->session->set_flashdata('error_msg_limit',
                'Your product limit exceeded please upgrade your package Thanks');

            redirect('user/subscription');

        }
    } else {
        $this->session->set_flashdata('error_msg_limit',
            'Your memory limit exceeded please upgrade your package Thanks');

        redirect('user/subscription');

    }


}

function add_webinar($code=''){

   $product = $this->product_model->get_SingleProduct_Bycode($code);
   if($product){
       if(!empty($this->input->post('code'))){
           if(!empty($_FILES['mediaFile']['name'])){
            if (!file_exists('./site_assets/products/' . $code)) {
                mkdir('./site_assets/products/' . $code, 0777, true);
            }

            $webinar_config['upload_path']    = 'site_assets/products/'.$code;
            $webinar_config['allowed_types']  = '*';
            $new_name = time()."_".str_replace(' ', '', $_FILES['mediaFile']['name']);
            $webinar_config['file_name'] = $new_name;
            $webinar_config['max_size'] = (float)$this->user_model->check_memory("size") * 1024;
            $this->load->library('upload',$webinar_config);
            $this->upload->initialize($webinar_config);
            if($this->upload->do_upload('mediaFile'))
            {
                $webinar_data = $this->upload->data();
                $params['webinar_video'] = $webinar_data['file_name'];
                $params['webinar_memory_size'] = $webinar_data['file_size'];
                $params['is_webinar'] = '1';

                $hour = $this->input->post('hour');
                $min  =  $this->input->post('minute');
                $sec  = $this->input->post('second');
                if($hour > 0){
                    $hour = $hour*60*60; 
                }
                if($min > 0){
                 $min = $min*60;
             }
             $params['webinar_play_duration'] = $hour+$min+$sec;
             $this->product_model->update($params, $code);
             $this->session->set_flashdata('success_msg', 'your product successfully updated!');
             redirect(base_url('Product/PurchaseAction/') . $code);

         }else{
            $this->session->set_flashdata('error_msg', $this->upload->display_errors());
        }
    }
    else{
        $this->session->set_flashdata('error_msg', 'Upload video');
    }

    redirect('Product/add_webinar/'.$code);

}else{
  $this->load->view('front_pages/dashboard/dash_header');
  $data['code'] = $code;
  $this->load->view('front_pages/dashboard/webinar', $data);
  $this->load->view('front_pages/dashboard/dash_footer');
}
}else{
    redirect('Home');
}


}
public function addpp()
{

    $store_id = $this->session->userdata('store_id');
    if (!isset($store_id) && $store_id == "") {
        redirect('user/Dashboard');
    }
    if ($this->user_model->check_memory()) {
        if ($this->chCountp() != 'out') {
            if (isset($_REQUEST['create_product'])) {

                $productCode = $this->generateProCode(8);
                $product = $_REQUEST['product'];
                $product['product_code'] = $productCode;
                $product['product_name'] = $product['product_name'];
                $slug = preg_replace("/-$/", "", preg_replace('/[^a-z0-9]+/i', "-", strtolower($product['product_name'])));
                $query = "SELECT COUNT(*) AS NumHits FROM dmd_store_products WHERE  product_slug  LIKE '$slug%'";
                $result = $this->db->query($query);
                $row = $result->row_array();
                $numHits = $row['NumHits'];

                $use_slug = ($numHits > 0) ? ($slug . '-' . $numHits) : $slug;

                $product['product_slug'] = $use_slug;
                $product['product_price'] = (float)$product['product_price'];
                    // check fields validation
                    //print_r($product);die();
                $empty_message = "";
                if ($_FILES['userfile']['name'] == '') {
                    $empty_message .= "Product Image is required field<br>";
                }
                if(@$_REQUEST['is_webinar']){
                 if($_FILES['webinarfile']['name'] ==''){ 
                    $empty_message .= "Webinar video is required field<br>";
                }
            }
            foreach ($product as $key => $val) {
                if ($val == "") {
                    $empty_message .= $key . " is required field<br>";
                }
            }

            if($product['product_price'] == 0){
                $empty_message .= "price should be greater than zero<br>";
            }

            if ($empty_message != "") {
                $this->session->set_flashdata('error_msg', $empty_message);
                $this->session->set_flashdata('fields', $product);
                redirect('product/add');
            }
            $product_check = $this->product_model->product_check($slug);
            if ($product_check) {
                        // upload product image
                $product['upsel_product'] = json_encode(@$_REQUEST['upsel_product']);
                    // $name_parts = pathinfo($_FILES['image']['name']);


                $file_ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
                $file_name = uniqid() . '.' . $file_ext;

                $config = array(
                 'upload_path' => './site_assets/products',
                 'allowed_types' => 'gif|jpg|png|jpeg',
                 'file_name'     => $file_name,
                 'max_size'      => (float)$this->user_model->check_memory("size") * 1024,
                            //'width'         =>  300,
                            //'height'        => 300,

                 'overwrite'     => false);
                    // print_r(' '.$file_ext . '  <br>');
                    // print_r(' ' .$file_name  .' <br> ');
                    // print_r($config);die;
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('userfile')) {

                    $upload_data = $this->upload->data();

                    $data_ary = array(
                        'title' => $upload_data['client_name'],
                        'file' => $upload_data['file_name'],
                        'width' => $upload_data['image_width'],
                        'height' => $upload_data['image_height'],
                        'type' => $upload_data['image_type'],
                        'size' => $upload_data['file_size'],
                        'path' => $upload_data['full_path'],
                        'date' => time(),
                    );

                    $data = array('upload_data' => $data_ary);

                            //video uploading
                    if(@$_REQUEST['is_webinar']){
                        $config['upload_path']    = 'site_assets/products/';
                        $config['allowed_types']  = '*';
                        $new_name = time()."_".str_replace(' ', '', $_FILES['webinarfile']['name']);
                        $config['file_name'] = $new_name;
                        $config['max_size'] = (float)$this->user_model->check_memory("size") * 1024;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('webinarfile'))
                        {
                            $webinar_data = $this->upload->data();
                            $product['webinar_video'] = $webinar_data['file_name'];
                            $product['webinar_memory_size'] = $webinar_data['file_size'];
                            $product['is_webinar'] = '1';

                        }else{
                            $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                            redirect('product/add');
                        }
                    }

                            //end video uploading


                            //$this->newsModel->addNews($ad_ne_data);
                            // save data to db

                    $product['pro_image'] = $data_ary['file'];
                    $product['memory_size'] = $upload_data['file_size'];
                    $product['media_date'] = date('Y-m-d');
                    // $resize = $this->do_resize($data_ary['file']);
                    // if ($resize) {
                    //     $expo_thumb = explode(".", $data_ary['file']);
                    //     $product['pro_image_thumb'] = $expo_thumb[0] . '_thumb' . '.' . $expo_thumb[1];
                    // }


                    $user_id = $this->session->userdata('user_id');
                    $product['user_id'] = $user_id;
                    $product['save_enable'] = @$_REQUEST['save_enable'];
                    $product['cross_sell_product'] = json_encode(@$_REQUEST['cross_sell_product']);
                    $product['is_download'] = @$_REQUEST['is_download'];
                    foreach ($product as $key => $val) {
                        if ($val == "") {
                            if ($key == "save_enable") {
                                $product[$key] = 0;
                            } else {
                                $product[$key] = "";
                            }
                        }
                    }


                    $this->product_model->create_product($product);
                    $this->session->set_flashdata('success_msg',
                        'Your product successfully created. Add product files now!');
                    redirect(base_url('product/PurchaseAction/') . $product['product_code']);


                } else {
                            //print_r($this->upload->display_errors());
                    $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                    redirect('product/add');

                }
            } else {

                $this->session->set_flashdata('error_msg', 'Product already exists,Try again.');
                redirect('product/add');


            }


        }

        $this->load->view('front_pages/dashboard/dash_header');
        $products['product_name'] = $this->product_model->getAll();
        $this->load->view('front_pages/dashboard/addproduct', $products);
        $this->load->view('front_pages/dashboard/dash_footer');

    } else {
        $this->session->set_flashdata('error_msg_limit',
            'Your product limit exceeded please upgrade your package Thanks');

        redirect('user/subscription');

    }
} else {
    $this->session->set_flashdata('error_msg_limit',
        'Your memory limit exceeded please upgrade your package Thanks');

    redirect('user/subscription');

}
}
public function detail($id)
{
    if ($id != "") {
        $products['products'] = $this->product_model->getAll($id);
        $products["checkoutComplete"] = $this->product_model->checkoutComplete($id);
        $products["checkoutBegin"] = $this->product_model->checkoutBegin($id);
            //print_r($products);die();
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/product_detail', $products);
        $this->load->view('front_pages/dashboard/dash_footer');
    }

}
public function sendToDownload($id)
{

    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/product_to_download', array('data_id' => $id));
    $this->load->view('front_pages/dashboard/dash_footer');
}
public function purchase($id = "")
{
    $data['purchases'] = $this->product_model->getAllpurchase($id);
    $data['id'] = $id;

        //print_r($data['purchases']);

    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/purchase', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}
public function PurchaseAction($id = "")
{
    if ($id != "") {
        $data['files'] = $this->product_model->getAllfile($id);
        $data['mails'] = $this->product_model->getAllmail($id);
        $data['id'] = $id;
        $data['mid'] = $id;
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/manage_purchase', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }


}
public function purchaseFile($pid)
{
        //print_r($)
    $chk = $this->check_cust();
    if ($chk) {
            //$data['files'] = $this->product_model->getAllpurchasefile($pid);
        $data = $this->product_model->get_is_download($pid);
        $data['files'] = $this->product_model->getAllfile($pid);
        $this->load->view('front_pages/store/store_header');
        $this->load->view('front_pages/store/product_videos', $data);
        $this->load->view('front_pages/store/store_footer');
    } else {
        $this->session->set_flashdata('error_msg',
            'Please login first for view purchaseable files');
        redirect(base_url());
    }

}
public function purchaseFileDetail($pid)
{

    $data = [];
    $this->load->view('front_pages/store/store_header');
    $this->load->view('front_pages/store/product_video_detail', $data);
    $this->load->view('front_pages/store/store_footer');


}
public function check_cust()
{
    $customer_details = $this->session->userdata('customer_details');
        // check customer
    $chk = $this->db->query("select id from dmd_customers where id='" . $customer_details['customerid'] .
        "'");
    if ($chk->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
public function filter()
{
    if ($this->input->post()) {
        $data = array_filter($this->input->post());
        if ($data) {
            $result = $this->product_model->filter_purchase($data);
            if ($result) {
                print_r(json_encode($result));
            }
        }
    }
}
public function addMail($id = "")
{
    if ($id != "") {
        $data['id'] = $id;
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/add_email_for_delivery', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }


}
public function editMail($pid, $fid)
{
    if ($fid != "") {
        $data['file'] = $this->product_model->getAllmail($fid, "edit");
        $data['file_id'] = $fid;
        $data['id'] = $pid;
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/add_email_for_delivery', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }


}
public function addFile($id = "")
{


    if ($id != "") {
        $data['id'] = $id;
        $data['check_memory'] = $this->user_model->check_memory("get");
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/add_file_for_delivery', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }


}
public function editFile($pid, $fid)
{
    if ($fid != "") {
        $data['file'] = $this->product_model->getAllfile($fid, "edit");
        $data['file_id'] = $fid;
        $data['id'] = $pid;
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/add_file_for_delivery', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }
}
}
