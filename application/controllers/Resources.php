<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resources extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('Resources_model');
		$this->load->library('pagination');
	}

	function index()
	{
		$data["resources"] = $this->Resources_model->get_all();	
		$this->load->view('front_pages/dashboard/dash_header');
		$this->load->view('front_pages/dashboard/admin_resources',$data);
		$this->load->view('front_pages/dashboard/dash_footer');
	}
	
	
	function add()
	{
		if($this->input->post('submitted') == 'add'){
			// print_r($this->input->post('fileType'));die;

			$this->form_validation->set_rules('name','Resource','trim|required');
			$this->form_validation->set_rules('title','Title','trim|required');
			$this->form_validation->set_rules('label','Label','trim|required');
			$this->form_validation->set_rules('url','URL','trim|required');
			$this->form_validation->set_rules('description','Description','trim|required');	
			if($this->form_validation->run())     
			{
				$params['name']  = $this->input->post('name');
				$params['title'] = $this->input->post('title');
				$params['label'] = $this->input->post('label');
				$params['url']   = $this->input->post('url');
				$params['description'] = $this->input->post('description');
				$params['created_date'] = date('Y-m-d H:i:s');
				$params['updated_date'] = date('Y-m-d H:i:s');

				if($this->input->post('fileType') == 1){
					if(!empty($_FILES['mediaFile']['name']))
					{ 

						$config['upload_path']    = 'site_assets/resources/';
						$config['allowed_types']  = 'jpg|jpeg|png|gif';
						$new_name                 = time()."_".$_FILES['mediaFile']['name'];
						$config['file_name']      = $new_name;
						$config['max_size']       = '0';
						$config['max_width']      = '0';
						$config['max_height']     = '0';
						$this->load->library('upload',$config);
						$this->upload->initialize($config);
						if($this->upload->do_upload('mediaFile'))
						{
							$uploadData = $this->upload->data();
							$params['file'] = $uploadData['file_name'];
							$params['file_type'] = 1;


							$inser_id = $this->Resources_model->insert($params);
							$this->session->set_flashdata('success_msg', 'Resources content has been added successfully.');
							unset($_POST);
						}else{
							$this->session->set_flashdata('error_msg', $this->upload->display_errors());
						}
					}else{
						$this->session->set_flashdata('error_msg', 'Select resource Image  file');
					}
				}
				else{
					//video
					if(!empty($_FILES['mediaFile']['name']))
					{ 
						$config['upload_path']    = 'site_assets/resources/';
						$config['allowed_types'] ='*';
						$new_name = time()."_".str_replace(' ', '', $_FILES['mediaFile']['name']);
						$config['file_name'] = $new_name;
						$config['max_size'] = '10240';
						$this->load->library('upload',$config);
						$this->upload->initialize($config);
						if($this->upload->do_upload('mediaFile'))
						{
							$uploadData = $this->upload->data();
							$params['file'] = $uploadData['file_name'];
							$params['file_type'] = 2;
							$inser_id = $this->Resources_model->insert($params);
							$this->session->set_flashdata('success_msg', 'Resources content has been added successfully.');
							unset($_POST);
						}else{
							$this->session->set_flashdata('error_msg', $this->upload->display_errors());
						}
					}else{
						$this->session->set_flashdata('error_msg', 'Select resource video file');
					}

				}

			}
		}
		$this->load->view('front_pages/dashboard/dash_header');
		$this->load->view('front_pages/dashboard/add_resource');
		$this->load->view('front_pages/dashboard/dash_footer');
	}


	function edit()
	{
		$id = decode($this->uri->segment(3));
		$data['resource']  = $this->Resources_model->get($id);
		if($this->input->post('submitted') == 'edit'){
			// print_r($this->input->post('fileType'));die;

			$this->form_validation->set_rules('name','Resource','trim|required');
			$this->form_validation->set_rules('title','Title','trim|required');
			$this->form_validation->set_rules('label','Label','trim|required');
			$this->form_validation->set_rules('url','URL','trim|required');
			$this->form_validation->set_rules('description','Description','trim|required');	
			if($this->form_validation->run())     
			{
				$params['name']  = $this->input->post('name');
				$params['title'] = $this->input->post('title');
				$params['label'] = $this->input->post('label');
				$params['url']   = $this->input->post('url');
				$params['description'] = $this->input->post('description');
				$params['updated_date'] = date('Y-m-d H:i:s');

				// print_r($params);die;
				if($this->input->post('fileType') == 1){
					if(!empty($_FILES['mediaFile']['name']))
					{ 

						$config['upload_path']    = 'site_assets/resources/';
						$config['allowed_types']  = 'jpg|jpeg|png|gif';
						$new_name                 = time()."_".$_FILES['mediaFile']['name'];
						$config['file_name']      = $new_name;
						$config['max_size']       = '0';
						$config['max_width']      = '0';
						$config['max_height']     = '0';
						$this->load->library('upload',$config);
						$this->upload->initialize($config);
						if($this->upload->do_upload('mediaFile'))
						{
							$uploadData = $this->upload->data();
							$params['file'] = $uploadData['file_name'];
							$params['file_type'] = 1;

							$result = $this->Resources_model->update($id, $params);
							$this->session->set_flashdata('success_msg', 'Resource content has been updated successfully.');
							$data['resource']  = $this->Resources_model->get($id);
							unset($_POST);
						}else{
							$this->session->set_flashdata('error_msg', $this->upload->display_errors());
						}
					}else{
						$result = $this->Resources_model->update($id, $params);
						$this->session->set_flashdata('success_msg', 'Resource content has been updated successfully.');
						$data['resource']  = $this->Resources_model->get($id);
						unset($_POST);
					}
				}
				else{
					//video
					if(!empty($_FILES['mediaFile']['name']))
					{ 
						$config['upload_path']    = 'site_assets/resources/';
						$config['allowed_types'] ='*';
						$new_name = time()."_".str_replace(' ', '', $_FILES['mediaFile']['name']);
						$config['file_name'] = $new_name;
						$config['max_size'] = '10240';
						$this->load->library('upload',$config);
						$this->upload->initialize($config);
						if($this->upload->do_upload('mediaFile'))
						{
							$uploadData = $this->upload->data();
							$params['file'] = $uploadData['file_name'];
							$params['file_type'] = 2;
							$result = $this->Resources_model->update($id, $params);
							$this->session->set_flashdata('success_msg', 'Resource content has been updated successfully.');
							$data['resource']  = $this->Resources_model->get($id);
							unset($_POST);
						}else{
							$this->session->set_flashdata('error_msg', $this->upload->display_errors());
						}
					}else{
						$result = $this->Resources_model->update($id, $params);
						$this->session->set_flashdata('success_msg', 'Resource content has been updated successfully.');
						$data['resource']  = $this->Resources_model->get($id);
						unset($_POST);
					}

				}

			}
		}
		$this->load->view('front_pages/dashboard/dash_header');
		$this->load->view('front_pages/dashboard/edit_resource', $data);
		$this->load->view('front_pages/dashboard/dash_footer');
	}
	public function delete(){

		$id = decode($this->uri->segment(3));
		$res  = $this->Resources_model->get($id);
		if($res)
		{
			$result = $this->Resources_model->delete($id);
			$this->session->set_flashdata('success_msg', 'Resources has been removed successfully.');
		}

		redirect('Resources');
	}

	
	

}

?>