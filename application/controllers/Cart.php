<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('product_model');
		$this->load->model('store_model');
		$this->load->model('cart_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library("cart");

	} 
	public function index()
	{

		$domain = $_SERVER['HTTP_HOST'];
		$domain_arr = $this->store_model->get_StoreRows('dmd_stores','store_url',$domain);
		if(isset($domain_arr) && !empty($domain_arr)){
			$store = array();
			$store['storeid'] = $domain_arr['id']; 
			$get_products_arr = $this->product_model->get_StoreProducts($store['storeid']);	

			if(isset($get_products_arr)){

				$store['products'] = $get_products_arr;
			}
		// print_r($this->cart->contents());
			$this->load->view('front_pages/store/store_header');
			$this->load->view('front_pages/store/home',$store);
			$this->load->view('front_pages/store/store_footer');

		}else{

			$this->load->view('front_pages/front_header');
			$this->load->view('front_pages/home');
			$this->load->view('front_pages/front_footer');
		}

	}


	public function add_to_cart(){
	   // check coupon enable
		$exp_date = date('Y-m-d');
		$discount = 0;
		$actualprice = $this->input->post('product_price');  
		$getCoupon = $this->db->query('select * from dmd_coupon where id=(select enable_coupon from dmd_store_products where 
			product_code="'.$this->input->post('product_id').'") and expire_date>="'.$exp_date.'" and max_use!="0" and status="live"');
		if($getCoupon->num_rows()>0){
			$getCoupon = $getCoupon->row_array();
			$discount = $getCoupon['discount'];
		}

		$data = array(
			'id' => $this->input->post('product_id'),
			'name' => $this->input->post('product_name'),
			'price' => $actualprice - ($actualprice * ($discount / 100)),
			'Origprice' => $this->input->post('product_price'),
			'discount' => $discount,
			'qty' => $this->input->post('quantity'),
		);

		$item_added = $this->cart->insert($data);
		if($item_added){

			$this->session->set_flashdata('success_cart','Item added successfully.');
			echo 'Item added successfully.';
			// redirect('home/showCart');
		}

	}


	public function update_cart(){

		// Recieve post values,calcute them and update
		$cart_info = $_POST['cart'] ;
        /*$exp_date = date('Y-m-d');
		$discount = 0;
		$actualprice = $cart['Origprice'];  
		$getCoupon = $this->db->query('select * from dmd_coupon where id=(select enable_coupon from dmd_store_products where 
			product_code="'.$this->input->post('product_id').'") and expire_date>="'.$exp_date.'" and max_use!="0" and status="live"');
		if($getCoupon->num_rows()>0){
			$getCoupon = $getCoupon->row_array();
			$discount = $getCoupon['discount'];
		}*/
        
		foreach( $cart_info as $id => $cart)
		{
			$rowid = $cart['rowid'];
			$price = $cart['price'];
			$amount = $price * $cart['qty'];
			$qty = $cart['qty'];

			$data = array(
				'rowid' => $rowid,
				'price' => $price,
				'amount' => $amount,
				'qty' => $qty
			);

		// $this->cart->update($data);
			$this->cart->update($data);
		// print_r($data);
		}
		// redirect('Home/showCart');
	}
	public function showProduct(){

		$id = $this->input->post('product_id');
		if(isset($id)){
			$get_product_arr['product'] = $this->product_model->get_SingleProducts($id);
		}
  		// print_r($get_product_arr);
		echo json_encode($this->load->view('front_pages/store/popup_product',$get_product_arr));
  		// echo json_encode(array('product' => $get_products_arr));
	}

	public function remove($rowid)
	{

		if ($rowid==="all"){
		// Destroy data which store in session.
			$this->cart->destroy();
		}else{
		// Destroy selected rowid in session.
			$data = array(
				'rowid' => $rowid,
				'qty' => 0
			);
		// Update cart data, after cancel.
			$this->cart->update($data);
		}

		$this->session->set_flashdata('success_cart','Item removed successfully from cart.');
		redirect('home/showCart');
	}

	public function checkout()
	{
		
		$store=array();
		$domain = $_SERVER['HTTP_HOST'];
        $cart_count = count($this->cart->contents());
        if($cart_count==0){
            redirect('home/showCart');
        }
		$domain_arr = $this->store_model->get_StoreRows('dmd_stores', 'store_url', $domain);
		$enable_pm = $this->store_model->enable_pm();

		if (isset($domain_arr) && !empty($domain_arr)) {
			$store['storeid'] = $domain_arr['id'];
			$store['sub_store_name'] = $domain_arr['store_name'];
			$store['enable_pm'] = $enable_pm;
			$store_logo = $this->store_model->get_store_logo();
			if($store_logo){
				$store['store_logo'] = $store_logo['image'];
			}
		}

		$this->load->view('front_pages/store/store_header', $store);
		$this->load->view('front_pages/store/checkout', $store);
		$this->load->view('front_pages/store/store_footer', $store);
	}

	public function checkout_form()
	{
		if($this->input->post()){
			$data = $this->input->post();

			print_r($data);
		}
	}

}
