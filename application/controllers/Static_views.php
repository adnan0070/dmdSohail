<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Static_views extends CI_Controller {

	
	public function __construct(){
		parent::__construct();
		$this->load->library(array('session', 'cart'));
	} 
	
	/*public function about()
	{
		$data['about'] ="checked";
		$this->load->view('front_pages/front_header');
		$this->load->view('front_pages/static/quick_links',$data);
		$this->load->view('front_pages/front_footer');
	}

	public function support()
	{
		$data['support'] ="checked";
		$this->load->view('front_pages/front_header');
		$this->load->view('front_pages/static/quick_links', $data);
		$this->load->view('front_pages/front_footer');
	}

	public function teams()
	{
		$data['teams'] ="checked";
		$this->load->view('front_pages/front_header');
		$this->load->view('front_pages/static/quick_links', $data);
		$this->load->view('front_pages/front_footer');
	}

	public function affiliates()
	{
		$data['affiliates'] ="checked";
		$this->load->view('front_pages/front_header');
		$this->load->view('front_pages/static/quick_links', $data);
		$this->load->view('front_pages/front_footer');
	}

	public function legalNotice()
	{
		$data['legal'] ="checked";
		$this->load->view('front_pages/front_header');
		$this->load->view('front_pages/static/legal_links', $data);
		$this->load->view('front_pages/front_footer');
	}	

	public function dmcaNotice(){
		$data['dmca'] ="checked";
		$this->load->view('front_pages/front_header');
		$this->load->view('front_pages/static/legal_links', $data);
		$this->load->view('front_pages/front_footer');
	}

	public function termsOfService(){
		$data['terms'] ="checked";
		$this->load->view('front_pages/front_header');
		$this->load->view('front_pages/static/legal_links', $data);
		$this->load->view('front_pages/front_footer');
	}

	public function storeRefundPolicy()	{
		$data['refund'] ="checked";
		$this->load->view('front_pages/store/store_header');
		$this->load->view('front_pages/static/policy_links', $data);
		$this->load->view('front_pages/store/store_footer');
	}

	public function storePrivacyPolicy(){

		$data['privacy'] ="checked";
		$this->load->view('front_pages/store/store_header');
		$this->load->view('front_pages/static/policy_links', $data);
		$this->load->view('front_pages/store/store_footer');
	}*/


	public function productVideos(){
		$this->load->view('front_pages/store/store_header');
		$this->load->view('front_pages/store/product_videos');
		$this->load->view('front_pages/store/store_footer');
	}

	public function productVideoDetail(){
		$this->load->view('front_pages/store/store_header');
		$this->load->view('front_pages/store/product_video_detail');
		$this->load->view('front_pages/store/store_footer');
	}

}
