<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class Customer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('customer_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));

        $this->load->model('product_model');
        $this->load->model('store_model');
        $this->load->library("cart");

    }
    public function check_cust()
    {
        $customer_details = $this->session->userdata('customer_details');
        // check customer
        $chk = $this->db->query("select id from dmd_customers where id='" . $customer_details['customerid'] .
            "'");
        if ($chk->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function comments()
    {
        $chk = $this->check_cust();
        if ($chk) {
            if ($this->input->post()) {
                // check fields validation
                $empty_message = "";

                foreach ($this->input->post() as $key => $val) {
                    if ($val == "") {
                        $empty_message .= $key . " is required field<br>";
                    }
                }
                if ($empty_message != "") {
                    $this->session->set_flashdata('error_msg', $empty_message);


                    if (isset($_REQUEST['front'])) {
                        redirect('home/showProduct/' . $this->input->post('product_id'));
                    }

                    redirect('customer/productDetail/' . $this->input->post('product_id') . '/' . $this->
                        input->post('item_id'));

                } else {

                    $data = array();
                    $data['content'] = $this->input->post('comment_text');
                    $data['product_id'] = $this->input->post('product_id');
                    $data['customer_id'] = $this->input->post('customer_id');
                    foreach ($data as $key => $val) {
                        if ($val == "") {
                            $data[$key] = "";
                        }
                    }


                    $create = $this->db->insert('comments', $data);
                    if ($create) {
                        $msg = "Your comment has been posted successfully!";
                        $this->session->set_flashdata('success_msg', $msg);
                    }


                    if (isset($_REQUEST['front'])) {
                        redirect('home/showProduct/' . $this->input->post('product_id'));
                    }

                    redirect('customer/productDetail/' . $this->input->post('product_id') . '/' . $this->
                        input->post('item_id'));
                }
            }
        }
    }
    public function count_total_rating($where)
    {
        $this->db->select('AVG(rating) as average');
        $this->db->where('product_id', $where);
        $this->db->from('product_rating');
        return $query = $this->db->get()->result_array();
    }
    public function get_rating_data($blogid)
    {
        $this->db->select('*');
        $this->db->from('dmd_customers u');
        $this->db->join('product_rating r', 'r.customer_id = u.id');
        $this->db->where('product_id', $blogid);
        return $query = $this->db->get()->result();
    }
     // Fetch records
    public function getProrating($userid,$productid) {


      $id = $productid;
      // User rating
      $this->db->select('rating');
      $this->db->from('product_rating');
      //$this->db->where("customer_id", $userid);
      $this->db->where("product_id", $id);
      $userRatingquery = $this->db->get();

      $userpostResult = $userRatingquery->result_array();

      $userRating = 0;
      if(count($userpostResult)>0){
       $userRating = $userpostResult[0]['rating'];
   }

      // Average rating
   $this->db->select('ROUND(AVG(rating),1) as averageRating');
   $this->db->from('product_rating');
   $this->db->where("product_id", $id);
   $ratingquery = $this->db->get();

   $postResult = $ratingquery->result_array();

   $rating = $postResult[0]['averageRating'];

   if($rating == ''){
       $rating = 0;
   }

   $posts_arr = array("id"=>$id,"rating"=>$userRating,"averagerating"=>$rating);

   return $posts_arr;
}

     // Save user rating
public function userRating($userid,$postid,$rating){
    $this->db->select('*');
    $this->db->from('product_rating');
    $this->db->where("product_id", $postid);
    $this->db->where("customer_id", $userid);
    $userRatingquery = $this->db->get();

    $userRatingResult = $userRatingquery->result_array();
    if(count($userRatingResult) > 0){

      $postRating_id = $userRatingResult[0]['id'];
      // Update
      $value=array('rating'=>$rating);
      $this->db->where('id',$postRating_id);
      $this->db->update('product_rating',$value);
  }else{
      $userRating = array(
        "product_id" => $postid,
        "customer_id" => $userid,
        "rating" => $rating
    );

      $this->db->insert('product_rating', $userRating);
  }

    // Average rating
  $this->db->select('ROUND(AVG(rating),1) as averageRating');
  $this->db->from('product_rating');
  $this->db->where("product_id", $postid);
  $ratingquery = $this->db->get();

  $postResult = $ratingquery->result_array();

  $rating = $postResult[0]['averageRating'];

  if($rating == ''){
     $rating = 0;
 }

 return $rating;
}

    // Update rating
public function updateRating()
{
    $chk = $this->check_cust();
    if ($chk) {
        $cuid = $this->session->userdata('customer_details');
        $cuid = $cuid['customerid'];


            // POST values
        $postid = $this->input->post('postid');
        $rating = $this->input->post('rating');

            // Update user rating and get Average rating of a post
        $averageRating = $this->userRating($cuid, $postid, $rating);

        echo $averageRating;
        exit;
    }
}
public function productDetail($proId, $itemid)
{
    $chk = $this->check_cust();
    if ($chk) {
        $cuid = $this->session->userdata('customer_details');
        $cuid = $cuid['customerid'];
            $blogid = $proId; //this is static id of blog but you are not enter static id you enter your dynami c id of blog or post
            
            $order['get_avg_rating'] = $this->count_total_rating($blogid);
            $order['rating_data'] = $this->get_rating_data($blogid);
            $rate  = $this->getProrating($cuid,$proId);
            $order['rating'] = $rate['rating'];
            $order['averagerating'] = $rate['averagerating'];
            
            $cuid = $this->session->userdata('customer_details');
            $cuid = $cuid['customerid'];
            $ord_items = $this->db->query('select item.*,item.id as itemid,pro.* from dmd_order_items as item left join dmd_store_products as pro
               on(pro.product_code=item.product_id)where pro.product_code = "' . $proId .
               '" and item.id="' . $itemid . '"');
            $order['order_items'] = array();
            if ($ord_items->num_rows() > 0) {
                $order['order_items'] = $ord_items->row_array();
            }
            $file = $this->db->query('select file.*,file.product_id as proid,del.* from dmd_order_files as file left join dmd_delivery_for_pro as del on (file.file_path=del.upload_file) where file.item_id="' .
                $itemid . '"');
            $order['files'] = array();
            if ($file->num_rows() > 0) {
                $order['files'] = $file->result_array();
            }
            // list of comments
            $order['comments'] = array();
            // check comment by customer
            $order['already'] = "0";
            $chk_by_customer = $this->db->query('select id from comments where customer_id = "' .
                $cuid . '" and product_id="' . $proId . '" ');
            if ($chk_by_customer->num_rows() > 0) {
                $order['already'] = "1";
            }
            $comments = $this->db->query('select cu.*,cust.* from comments as cu left join dmd_customers as cust on(cust.id=cu.customer_id)
                where product_id="' . $proId . '"');
            if ($comments->num_rows() > 0) {
                $order['comments'] = $comments->result_array();
            }
            $order['sub_store_name'] = $this->session->userdata('store_name');
            //print_r($order);die();
            $this->load->view('front_pages/store/store_header', $order);
            $this->load->view('front_pages/store/product_video_detail', $order);
            $this->load->view('front_pages/store/store_footer');


            //echo json_encode(array('status' => 'success','msg' => 'test','table'=>$list));
        } else {
            $this->session->set_flashdata('error_msg',
                'Please login first for view purchaseable files');
            redirect(base_url());
        }
    }
    public function purchase()
    {
        $chk = $this->check_cust();
        if ($chk) {
            $cuid = $this->session->userdata('customer_details');
            $cuid = $cuid['customerid'];
            $ord_items = $this->db->query('select item.*,item.id as itemid,pro.* from dmd_order_items as item left join dmd_store_products as pro
               on(pro.product_code=item.product_id) where item.customer_id="' . $cuid .'" and pro.store_id = "' . $this->session->userdata('store_id') . '" ORDER BY item.created_date DESC');
            //echo $this->db->last_query();die();
            $order['order_items'] = array();
            $order['sub_store_name'] = $this->session->userdata('store_name');

            if ($ord_items->num_rows() > 0) {
                $order['order_items'] = $ord_items->result_array();
            }
            //print_r($order);
            $this->load->view('front_pages/store/store_header', $order);
            $this->load->view('front_pages/store/purchase_list', $order);
            $this->load->view('front_pages/store/store_footer');

            //echo json_encode(array('status' => 'success','msg' => 'test','table'=>$list));
        } else {
            $this->session->set_flashdata('error_msg',
                'Please login first for view purchaseable files');
            redirect(base_url());
        }
    }
    public function index()
    {
        $data["customers"] = $this->customer_model->get_customers_with_sales();
        $data["products"] = $this->customer_model->get_products();

        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/customers', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    public function import()
    {
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/import_customer');
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function import_file()
    {
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'csv|xlsx|xls';
        $config['file_name'] = "CUSTOMERS_" . $this->session->userdata("user_id");
        $config['overwrite'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('customerfile')) {
            $this->session->set_flashdata('import_csv', $this->upload->display_errors());
            redirect("/customer/import");
        } else {
            $data = $this->upload->data();
            $file = "upload/" . $data["file_name"];

            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file);
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($file);
            $sheet = $spreadsheet->getActiveSheet();
            $lastRow = $sheet->getHighestRow();
            $lastColumn = $sheet->getHighestColumn();
            $range = 'A1:' . $lastColumn . $lastRow;
            $sheetData = $sheet->rangeToArray($range, null, true, false);


            foreach ($sheetData as $key => $value) {
                foreach ($value as $k => $v) {
                    $v = trim($v);
                    if (empty($v)) {
                        unlink($file);
                        $this->session->set_flashdata("import_csv",
                            "An empty value has been found. Customer data has been failed to import.");
                        redirect("/customer/import");
                    }
                }
            }

            /*print_r($v);
            die;*/
            $headings = array_shift($sheetData);

            //add extra data
            array_push($headings, "user_id", "store_id");
            $user_id = $this->session->userdata("user_id");
            $store_id = $this->session->userdata("store_id");

            for ($i = 0; $i < count($sheetData); $i++) {
                array_push($sheetData[$i], $user_id, $store_id);
            }

            $data = array_map(function ($values)use ($headings)
            {
                return array_combine($headings, $values); }
                , $sheetData);

            $result = $this->customer_model->insert_excel($data);

            if ($result) {
                $this->session->set_flashdata("import_csv",
                    "Customer data has been successfully imported.");
                unlink($file);
            } else {
                $this->session->set_flashdata("import_csv",
                    "Customer data has been failed to import.");
            }

            redirect("/customer/import");

            /*echo "<pre>";
            print_r($data);
            echo "</pre>";*/
        }
    }

    public function filter()
    {
        if ($this->input->post()) {
            $data = array_filter($this->input->post());
            //print_r($data);die();
            if ($data) {
                $result = $this->customer_model->filter_customers($data);
                if ($result) {
                    //print_r($result);
                    print_r(json_encode($result));
                }
            }
        }
    }

    public function export_file()
    {
        $customers = array();

        if ($this->input->post()) {
            $data = array_filter($this->input->post());

            if ($data) {
                $customers = $this->customer_model->export_filter_customers($data);
            } else {
             $customers = $this->customer_model->get_customers_with_sales();
               // $customers = $this->customer_model->get_customers();
         }
     }
        /*print_r($data);
        die;*/
        if ($customers) {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->setActiveSheetIndex(0);
            $sheet->setCellValue('A1', 'Name');
            $sheet->setCellValue('B1', 'Email');
            $sheet->setCellValue('C1', '# of Sales');
            $sheet->setCellValue('D1', 'Total Sales');

            $i = 1;
            foreach ($customers as $key => $value) {
                $i++;
                $sheet->setCellValue('A' . $i, $value["f_name"] . " " . $value["l_name"]);
                $sheet->setCellValue('B' . $i, $value["email"]);
                $sheet->setCellValue('C' . $i, $value["count_sale"]);
                $sheet->setCellValue('D' . $i, $value["total_sale"]);
            }

            $writer = new Csv($spreadsheet);

            $filename = 'Customers';

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
            header('Cache-Control: max-age=0');

            $writer->save('php://output');
        } else {
            $this->session->set_flashdata('error_msg', 'No export data has been found.');
            redirect('customer/');
        }
    }

    public function login()
    {
        $domain = $_SERVER['HTTP_HOST'];
        $domain_arr = $this->store_model->get_StoreRows('dmd_stores', 'store_url', $domain);
        if (isset($domain_arr) && !empty($domain_arr)) {
            if($domain_arr['status'] == 0 || $domain_arr['status'] == 2){
                redirect('http://' . $domain);
            }

            $store = array();
            $store['storeid'] = $domain_arr['id'];
            $store['sub_store_name'] = $domain_arr['store_name'];
            $this->session->set_userdata('store_id', $domain_arr['id']);
            $this->session->set_userdata('store_name', $domain_arr['store_name']);
            $get_products_arr = $this->product_model->get_StoreProducts($store['storeid']);

            if (isset($get_products_arr)) {

                $store['products'] = $get_products_arr;
            }

            $this->load->view('front_pages/store/store_header', $store);
            $this->load->view('front_pages/store/login', $store);
            $this->load->view('front_pages/store/store_footer');

        } else {

            $this->load->view('front_pages/front_header');
            $this->load->view('front_pages/home');
            $this->load->view('front_pages/front_footer');
        }
    }
    public function login_user()
    {
        $user_login = array('email' => $_REQUEST['email'], 'password' => md5($_REQUEST['password']));

        $data = $this->customer_model->login_customer($user_login['email'], $user_login['password']);
        if ($data) {

            $this->session->set_userdata('customer_details', $data);

            redirect('customer/dashboard');
        } else {
            $this->session->set_flashdata('error_msg',
                'You have entered an invalid email or password, Try again.');
            redirect('customer/login');

        }


    }
    public function profile()
    {

        $domain = $_SERVER['HTTP_HOST'];
        $domain_arr = $this->store_model->get_StoreRows('dmd_stores', 'store_url', $domain);
        if (isset($domain_arr) && !empty($domain_arr)) {
            if ($this->session->userdata('customer_details')) {
                $store = array();
                $store['storeid'] = $domain_arr['id'];
                $store['sub_store_name'] = $domain_arr['store_name'];
                $customer_data = $this->session->userdata('customer_details');
                $store['customer_data'] = $this->customer_model->getCustomer_Byid($customer_data['customerid']);

                $this->load->view('front_pages/store/store_header', $store);
                $this->load->view('front_pages/store/profile', $store);
                $this->load->view('front_pages/store/store_footer');
            } else {
                redirect(base_url());
            }
        }
    }
    public function edit()
    {
        //print_r($_REQUEST);die();
        if ($this->session->userdata('customer_details')) {
            $user = array(
                'f_name' => $_REQUEST['firstName'],
                'l_name' => $_REQUEST['lastName'],
            );
            $customer_data = $this->session->userdata('customer_details');
            // check fields validation
            $empty_message = "";
            foreach ($user as $key => $val) {
                if ($val == "") {
                    $empty_message .= $key . " is required field<br>";
                }
            }
            if ($empty_message != "") {
                $this->session->set_flashdata('error_msg', $empty_message);
                redirect('customer/profile');
            }

            if (isset($_REQUEST['currentPassword']) && !empty($_REQUEST['ConfPassword'])) {
                if (isset($_REQUEST['password']) && !empty($_REQUEST['ConfPassword']) && $_REQUEST['password'] ==
                    $_REQUEST['ConfPassword'])
                    $user['password'] = md5($_REQUEST['password']);
                $result = $this->customer_model->password_check($customer_data['customerid'], $_REQUEST['currentPassword']);

                if ($result) {
                    // die;
                    $this->db->where('id', $customer_data['customerid']);
                    $update = $this->db->update("dmd_customers", $user);
                    if ($update) {

                        $this->session->set_flashdata('success_msg',
                            'Your information has been updated!');
                        redirect('customer/profile');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
                        redirect('customer/profile');
                    }
                } else {
                    $this->session->set_flashdata('error_msg',
                        'Current Password is not correct! Try again.');
                    redirect('customer/profile');
                }
            } else {

                $this->db->where('id', $customer_data['customerid']);
                $update = $this->db->update("dmd_customers", $user);
                if ($update) {

                    $this->session->set_flashdata('success_msg',
                        'Your information has been updated!');
                    redirect('customer/profile');
                } else {
                    $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
                    redirect('customer/profile');
                }
            }
        }


    }
    public function dashboard()
    {
        $domain = $_SERVER['HTTP_HOST'];
        $domain_arr = $this->store_model->get_StoreRows('dmd_stores', 'store_url', $domain);
        if (isset($domain_arr) && !empty($domain_arr)) {
            if ($this->session->userdata('customer_details')) {
                $store = array();
                $store['storeid'] = $domain_arr['id'];
                $store['sub_store_name'] = $domain_arr['store_name'];
                $store['customer_data'] = $this->session->userdata('customer_details');

                $store['customer_orders'] = $this->product_model->getAll_CustomerOrders($store['customer_data']['customerid']);
                /*echo '<pre>';
                print_r($store);
                echo '</pre>';*/
                $this->load->view('front_pages/store/store_header', $store);
                $this->load->view('front_pages/store/dashboard', $store);
                $this->load->view('front_pages/store/store_footer');
            } else {
                redirect(base_url());
            }


        }

    }

    public function order_items_details()
    {

        $id = $this->input->post('order_id');
        $order['order_items'] = $this->product_model->getAll_OrdersItems_Byid($id);
        $list = $this->load->view('front_pages/store/items_list', $order, true);
        echo json_encode(array(
            'status' => 'success',
            'msg' => 'test',
            'table' => $list));
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('customer/login', 'refresh');
    }

    public function checkRating()
    {
        if($this->input->post()){
            $data = $this->input->post();
            $check = $this->customer_model->check_rating($data);
            echo $check;
        }
    }

}
