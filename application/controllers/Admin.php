<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('store_model');
        $this->load->model('Categories_Model');
        $this->load->model('Article_Model');
        $this->load->model('User_model');
        $this->load->model('Resources_model');
        $this->load->model('Package_model');
        //$this->load->model('authorizenet_model');
        $this->load->library(array(
            'session',
            'cart'
        ));
        // $this->callConfig();
    }
    function dashboard()
    {
         //print_r($_SESSION);

        // get totalproducts
        $store_id = $this->session->userdata('store_id');
        $user_id = $this->session->userdata('user_id');
        $getq = $this->db->query("select count(id) as totpro from dmd_store_products ");
        $getq = $getq->row_array();
        $overview['products'] = $getq['totpro'];
        // get today sales
        $gettoday = $this->db->query("SELECT count(id) as tottoday FROM `dmd_orders` WHERE DATE(`created_date`) = CURDATE()");
        $gettoday = $gettoday->row_array();
        $overview['gettoday'] = $gettoday['tottoday'];


        // get default package id
        $default_pack = $this->db->get('store_package');
        $default_pack = $default_pack->row_array();
        $depackage = $this->db->get('packages');
        $depackage = $depackage->row_array();
        $overview['pack_product'] = $depackage['pack_products'];

        // get today sales
        $gettoday = $this->db->query("SELECT count(id) as tottoday FROM `dmd_orders` WHERE DATE(`created_date`) = CURDATE() and user_id='" . $user_id . "'");
        $gettoday = $gettoday->row_array();
        $overview['gettoday'] = $gettoday['tottoday'];

        // get month sales
        $getmonth = $this->db->query("SELECT count(id) as totmonth FROM dmd_orders WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND YEAR(created_date) = YEAR(CURRENT_DATE())");
        $getmonth = $getmonth->row_array();
        $overview['getmonth'] = $getmonth['totmonth'];

        // get lifetime sales
        $getotal = $this->db->query("SELECT count(id) as total_sales FROM dmd_orders ");
        $getotal = $getotal->row_array();
        $overview['getotal'] = $getotal['total_sales'];

        // over all earning
        $earning = $this->db->query("SELECT sum(grand_total) as total_earn FROM dmd_orders");
        $earning = $earning->row_array();
        $overview['earning'] = $earning['total_earn'];
        //print_r($_SESSION);
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/admin_dash',$overview);
        $this->load->view('front_pages/dashboard/dash_footer');

    }
    function addblog()
    {
        if (isset($_REQUEST['blog'])) {
            // check fields validation
            $store = $_REQUEST['blog'];
            $empty_message = "";
            foreach ($store as $key => $val) {
                if ($val == "") {
                    $empty_message .= $key . " is required field<br>";
                }
            }
            if ($empty_message != "") {
                $this->session->set_flashdata('error_msg', $empty_message);
                redirect('admin/blog');
            }
        }
    }

    function blog(){
       $this->load->view('front_pages/dashboard/dash_header');
       $this->load->view('front_pages/dashboard/blog_new');
       $this->load->view('front_pages/dashboard/dash_footer');
   }
   function updateSetting()
   {
        //print_r($_SESSION);

    if (isset($_REQUEST['store'])) {
            // check fields validation
        $store = $_REQUEST['store'];
        $empty_message = "";
        foreach ($store as $key => $val) {
            if ($val == "") {
                $empty_message .= $key . " is required field<br>";
            }
        }
        if ($empty_message != "") {
            $this->session->set_flashdata('error_msg', $empty_message);
            redirect('admin/setting');
        }

        if ($_REQUEST['old_image'] == "") {
                // upload product image
            $name_parts = pathinfo($_FILES['userfile']['name']);

            $name_full = preg_replace('/\s+/', '', $name_parts['filename']);

            $file_ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
            $file_name =   uniqid().'.'.$file_ext;
            $productCode = $save_id;
                //$file_name = $name_full;
            $config = array(
                'upload_path' => './site_assets/video',
                'allowed_types' => 'mp4|ogg|WebM',
                'file_name' => $file_name,
                'max_size' => '0',
                'overwrite' => false,
            );


            $this->load->library('upload', $config);
            if (!$_FILES) {
                $is_file_error = true;
                $this->session->set_flashdata('error_msg', 'Select a video file.');
                redirect('admin/setting');

            }

            if ($this->upload->do_upload('userfile')) {

                $upload_data = $this->upload->data();

                $data_ary = array(
                    'title' => $upload_data['client_name'],
                    'file' => $upload_data['file_name'],
                    'width' => $upload_data['image_width'],
                    'height' => $upload_data['image_height'],
                    'type' => $upload_data['image_type'],
                    'size' => $upload_data['file_size'],
                    'path' => $upload_data['full_path'],
                    'date' => time(),
                );

                $data = array('upload_data' => $data_ary);
                    // delete old image path
                $old_path = getcwd() . '/site_assets/video/' . $_REQUEST['old_image_path'];
                unlink($old_path);
                $store['userfile'] = $data_ary['file'];
                $store['file_type'] = $upload_data['file_type'];
                    //$this->newsModel->addNews($ad_ne_data);
                    // save data to db


            } else {
                    //print_r($this->upload->display_errors());
                $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                redirect('admin/setting');

            }

        } else {

            $store['userfile'] = $_REQUEST['old_image'];
        }
        
        // home page video
        
        if ($_REQUEST['old_image_home'] == "") {
                // upload product image
            $name_parts = pathinfo($_FILES['homefile']['name']);

            $name_full = preg_replace('/\s+/', '', $name_parts['filename']);

            $file_ext = pathinfo($_FILES['homefile']['name'], PATHINFO_EXTENSION);
            $file_name =   uniqid().'.'.$file_ext;
            $productCode = $save_id;
                //$file_name = $name_full;
            $config = array(
                'upload_path' => './site_assets/video',
                'allowed_types' => 'mp4|ogg|WebM',
                'file_name' => $file_name,
                'max_size' => '0',
                'overwrite' => false,
            );


            $this->load->library('upload', $config);
            if (!$_FILES) {
                $is_file_error = true;
                $this->session->set_flashdata('error_msg', 'Select a video file.');
                redirect('admin/setting');

            }

            if ($this->upload->do_upload('homefile')) {

                $upload_data = $this->upload->data();

                $data_ary = array(
                    'title' => $upload_data['client_name'],
                    'file' => $upload_data['file_name'],
                    'width' => $upload_data['image_width'],
                    'height' => $upload_data['image_height'],
                    'type' => $upload_data['image_type'],
                    'size' => $upload_data['file_size'],
                    'path' => $upload_data['full_path'],
                    'date' => time(),
                );

                $data = array('upload_data' => $data_ary);
                    // delete old image path
                $old_path = getcwd() . '/site_assets/video/' . $_REQUEST['old_image_path_home'];
                unlink($old_path);
                $store['homefile'] = $data_ary['file'];
                $store['file_type'] = $upload_data['file_type'];
                    //$this->newsModel->addNews($ad_ne_data);
                    // save data to db


            } else {
                    //print_r($this->upload->display_errors());
                $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                redirect('admin/setting');

            }

        } else {

            $store['homefile'] = $_REQUEST['old_image_home'];
        }
        
        /// end
 /// tutorial video
        // if ($_REQUEST['old_tutorial_video'] == "") {
        //     $name_parts = pathinfo($_FILES['tutorialfile']['name']);
        //     $name_full = preg_replace('/\s+/', '', $name_parts['filename']);
        //     $file_ext = pathinfo($_FILES['tutorialfile']['name'], PATHINFO_EXTENSION);
        //     $file_name =   uniqid().'.'.$file_ext;
        //     $productCode = $save_id;
        
        //     $config = array(
        //         'upload_path' => './site_assets/video',
        //         'allowed_types' => 'mp4|ogg|WebM',
        //         'file_name' => $file_name,
        //         'max_size' => '0',
        //         'overwrite' => false,
        //     );


        //     $this->load->library('upload', $config);
        //     if (!$_FILES) {
        //         $is_file_error = true;
        //         $this->session->set_flashdata('error_msg', 'Select a Tutorial video file.');
        //         redirect('admin/setting');

        //     }

        //     if ($this->upload->do_upload('tutorialfile')) {

        //         $upload_data = $this->upload->data();

        //         $data_ary = array(
        //             'title' => $upload_data['client_name'],
        //             'file' => $upload_data['file_name'],
        //             'width' => $upload_data['image_width'],
        //             'height' => $upload_data['image_height'],
        //             'type' => $upload_data['image_type'],
        //             'size' => $upload_data['file_size'],
        //             'path' => $upload_data['full_path'],
        //             'date' => time(),
        //         );

        //         $data = array('upload_data' => $data_ary);
        
        //         $old_path = getcwd() . '/site_assets/video/' . $_REQUEST['old_tutorial_video_path'];
        //         unlink($old_path);
        //         $store['tutorialfile'] = $data_ary['file'];
        //         $store['file_type'] = $upload_data['file_type'];
        


        //     } else {
        
        //         $this->session->set_flashdata('error_msg', $this->upload->display_errors());
        //         redirect('admin/setting');

        //     }

        // } else {

        //     $store['tutorialfile'] = $_REQUEST['old_tutorial_video'];
        // }
        // end 
        foreach ($store as $key => $val) {
            if ($val == "") {
                $store[$key] = "";
            }
        }

        $this->db->update('settings', $store);
        $this->session->set_flashdata('success_msg', 'Data has been updated');
        redirect('admin/setting');


    }
}
function setting()
{
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/settings_stores');
    $this->load->view('front_pages/dashboard/dash_footer');

}
function packages()
{
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/packages_stores');
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function teams()
{
    $result = $this->user_model->get_teams();
    $data["teams"] = $result;

    //print_r($data);die();

    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/teams', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function add_team($id="")
{

    if(!empty($id)){
        $result = $this->user_model->get_teams($id);
        $data["teams"] = $result;
    }else{
        $data = array();
    }

    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/add_team', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function save_team()
{
    if($this->input->post()){
        $data = $this->input->post();
        //echo base_url('site_assets/team/');
        $config['upload_path']   = './site_assets/team/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = $data["name"];
        $config['overwrite'] = TRUE;
         //$config['max_size']      = 100; 
        $config['max_width']     = 1024;
        $config['max_height']    = 768;  
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = $this->upload->display_errors();
            $this->session->set_flashdata('message', 'Image Upload: ' . $error);
            $this->session->set_flashdata('teams', $data);
            //print_r($error);
            redirect("/admin/add_team", $data);
        }else { 
            $image = $this->upload->data();
            $data["image"] = $image['file_name'];
            //print_r($data);//die;

            $result = $this->user_model->set_teams($data);

            if($result){
                $this->session->set_flashdata('message', 'New team member has been added successfully!');
                redirect("/admin/teams");
            }else{
                $this->session->set_flashdata('message', 'New team member has been failed to add!');
                redirect("/admin/add_team");
            }
        } 
    } 
}

public function update_team($id="")
{
    if(!empty($id)){
        if($this->input->post()){
            $data = $this->input->post();    
            //print_r($data);

            $config['upload_path']   = './site_assets/team/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $data["name"];
            $config['overwrite'] = TRUE;
            $config['max_size']      = 0; 
            $config['max_width']     = 1024;
            $config['max_height']    = 768;  
            $this->load->library('upload', $config);

            if (!empty($_FILES['userfile']['name'])) {
                $this->upload->do_upload('userfile');
                $image = $this->upload->data();
                $data["image"] = $image['file_name'];
                //print_r($error);
                //redirect("/admin/teams");
            }
                //print_r($data);//die; 

            $result = $this->user_model->edit_teams($id, $data);

            if($result){
                $this->session->set_flashdata('message', 'Team member has been updated successfully!');
                redirect("/admin/teams");
            }else{
                $this->session->set_flashdata('message', 'Team member has been failed to update!');
                redirect("/admin/teams");
            }
            
        }
    }
}
public function categories()
{
    $data = array();
    $data['posts'] = $this->Categories_Model->categoryList();
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/categories', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function add()
{
    $data = array();
    $postData = array();

            //if add request is submitted
    if($this->input->post()){
        $empty_message = "";
        foreach ($this->input->post() as $key => $val) {
            if ($val == "") {
                switch ($key) {
                    case "title":
                    $empty_message .= "Title is a required field.<br><br>";
                    break;
                    default:
                    $empty_message = "";
                } 
            }
        }
        if ($empty_message != "") {
            $this->session->set_flashdata('error_msg', $empty_message);
            redirect('admin/add');
        }

                //prepare post data
        $postData = array('title' => $this->input->post('title'));
        $insert = $this->Categories_Model->save_category($postData);
        if($insert){
            $this->session->set_flashdata('success_msg', 'Category has been added successfully.');
            redirect('admin/categories');
        }else{
            $this->session->set_flashdata('error_msg', 'Category has been failed to add.');
            redirect('admin/categories');
        }
    }
            //validate submitted form data
            //if($this->form_validation->run() == true){
                //insert post data
            //}

    $data['Categories_Model'] = $postData;
    $data['title'] = 'Add Categories';
    $data['action'] = 'Add';

        //load the add page view
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/add_Categories', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function edit_Category(){
    $data = array();
    $postData = array();
    $id = $this->input->post('id');

        //if add request is submitted
    if($this->input->post('postSubmit')){
            //form field validation rules
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $postData = array(
            'title' => $this->input->post('title'),
        );

            //validate submitted form data
        if($this->form_validation->run() == true){
                //insert post data
            $insert = $this->Categories_Model->update_Category($id, $postData);

            if($insert){
                $this->session->set_userdata('success_msg', 'Category has been adited successfully.');
                redirect('admin/categories');
            }else{
                $data['error_msg'] = 'Some problems occurred, please try again.';
            }
        }
    }
    $id  = $_GET['id'];
    $data['Categories_Model'] = $postData;
        // $data['title'] = 'Edit Contacts';
    $data['action'] = 'Edit';
    $data['post'] = $this->Categories_Model->getRows($id);
        // print_r($data);
        // die;
        //load the add page view
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/add_Categories', $data);
    $this->load->view('front_pages/dashboard/dash_footer');

}

public function delete_Category(){
    $id  = $_GET['id'];
    $post = $this->Categories_Model->getRows($id);
    $query=$this->Categories_Model->delete_Category($id);
    if($query){
        $this->session->set_userdata('success_msg', 'Category has been deleted successfully.');
        redirect('admin/categories');
    }else{
        $data['error_msg'] = 'Some problems occurred, please try again.';
    }
}

public function articles()
{
    $data = array();
    $data['posts'] = $this->Article_Model->article_List();
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/articles' , $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function add_Article(){
    $postData = $this->input->post();
        //if add request is submitted
    if($postData){
        $config['upload_path']   = './site_assets/articles/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = $postData["title"];
        $config['overwrite'] = TRUE;
             //$config['max_size']      = 100; 
        $config['max_width']     = 1024;
        $config['max_height']    = 768;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $error = $this->upload->display_errors();
            $this->session->set_flashdata('error_msg', 'Image Upload: ' . $error);
            $this->session->set_flashdata('post', $postData);
                //print_r($error);
            redirect("/admin/add_article");
        }else { 
            $image = $this->upload->data();
            $postData["file"] = $image["file_name"];

                //insert post data
            $insert = $this->Article_Model->save_article($postData);
            if($insert){
                $this->session->set_flashdata('success_msg', 'Article has been added successfully.');
                redirect('admin/articles');
            }else{
                $this->session->set_flashdata('error_msg', 'Article has been failed to add.');
                redirect('admin/articles');
            }
        }     
    }

    $data['action'] = 'Add';
    $data['posts'] = $this->Categories_Model->getRows();

    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/add_Article', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function edit_Article($id=""){
    $postData = $this->input->post();


    if($postData){
        //print_r($_FILES['file']['name']);die();
        if( ! empty($_FILES['file']['name'])){
            $config['upload_path']   = './site_assets/articles/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $postData["title"];
            $config['overwrite'] = TRUE;
                 //$config['max_size']      = 100; 
            $config['max_width']     = 1024;
            $config['max_height']    = 768;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error_msg', 'Image Upload: ' . $error);
                $this->session->set_flashdata('post', $postData);
                    //print_r($error);
                redirect("/admin/add_article");
            }
            $image = $this->upload->data();
            $postData["file"] = $image["file_name"];  
        }
        
        $id = $postData['id'];
        unset($postData['id']);
        $update = $this->Article_Model->update_article($id, $postData);
        if($update){
            $this->session->set_flashdata('success_msg', 'Article has been edited successfully.');
            redirect('admin/articles');
        }else{
            $this->session->set_flashdata('error_msg', 'Article has been failed to edit.');
            redirect('admin/articles');
        }

    }

    $data['action'] = 'Edit';
    $data['post'] = $this->Article_Model->getRows($id);
    $data['posts'] = $this->Categories_Model->getRows();

        //load the add page view
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/add_Article', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function delete_Article($id=""){
    $query=$this->Article_Model->delete_article($id);
    if($query){
        $this->session->set_userdata('success_msg', 'articles has been deleted successfully.');
        redirect('admin/articles');
    }else{
        $this->session->set_userdata('error_msg', 'Some problems occurred, please try again.');
        redirect('admin/articles');
    }
}

public function blog_View()
{
        // $id= $_GET['id'];
    $data = array();
    $data['posts'] = $this->Article_Model->getblogs();
        //$data['cposts'] =$this->Categories_Model->getRows();
    $this->load->view('front_pages/front_header');
    $this->load->view('front_pages/blogView', $data);
    $this->load->view('front_pages/front_footer');
}


public function team_delete($id="")
{
    if($id){
        $delete = $this->user_model->delete_teams($id);
        if($delete){
            $this->session->set_flashdata('message', 'Delete operation has been successfully done!');
            redirect("/admin/teams");
        }else{
            $this->session->set_flashdata('message', 'Delete operation has been failed!');
            redirect("/admin/teams");
        }
    }
}

public function payment_method($parm = "")
{
    $this->load->view('front_pages/dashboard/dash_header');
        //print_r($this->session->userdata());die();
    if ($parm != "") {
        $pm = $this->store_model->get_pm($parm);
        if ($pm) {
            $data["pm"] = $pm;
            $this->load->view('front_pages/dashboard/' . $parm . "_configuration", $data);
        } else {
            $this->load->view('front_pages/dashboard/' . $parm . "_configuration");
        }
    } else {
        $data['pm_names'] = $this->store_model->check_pm();
        $this->load->view('front_pages/dashboard/payment_method', $data);
    }
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function create_pm($pm = "")
{
    if (!$pm == "") {
        if ($this->input->post()) {

                /*echo "<pre>";
                print_r($this->input->post());
                */
                $data["user_id"] = $this->session->userdata('user_id');
                $data["store_id"] = '0';//$this->session->userdata("store_id");
                $data["name"] = $pm;

                if (empty($this->input->post("status"))) {
                    $data["status"] = "0";
                } else {
                    $data["status"] = "1";
                }

                if (empty($this->input->post("default"))) {
                    $data["default"] = "0";
                } else {
                    $data["default"] = "1";
                }


                //prepare configration
                if (!empty($this->input->post("apiKey"))) {
                    $config["apiKey"] = $this->input->post("apiKey");
                }

                if (!empty($this->input->post("publishableApiKey"))) {
                    $config["publishableApiKey"] = $this->input->post("publishableApiKey");
                }

                if (!empty($this->input->post("country"))) {
                    $config["country"] = $this->input->post("country");
                }

                if (!empty($this->input->post("loginID"))) {
                    $config["loginID"] = $this->input->post("loginID");
                }

                if (!empty($this->input->post("transactionKey"))) {
                    $config["transactionKey"] = $this->input->post("transactionKey");
                }

                if (!empty($this->input->post("checkoutLabel"))) {
                    $config["checkoutLabel"] = $this->input->post("checkoutLabel");
                }

                if (!empty($this->input->post("visa"))) {
                    $config["visa"] = "1";
                } else {
                    $config["visa"] = "0";
                }

                if (!empty($this->input->post("mastercard"))) {
                    $config["mastercard"] = "1";
                } else {
                    $config["mastercard"] = "0";
                }

                if (!empty($this->input->post("ae"))) {
                    $config["americanExpress"] = "1";
                } else {
                    $config["americanExpress"] = "0";
                }

                if (!empty($this->input->post("dc"))) {
                    $config["dinerClub"] = "1";
                } else {
                    $config["dinerClub"] = "0";
                }

                if (!empty($this->input->post("paypalEmail"))) {
                    $config["paypalEmail"] = $this->input->post("paypalEmail");
                }

                if (!empty($this->input->post("paypalEmail"))) {
                    //$config["paypalEmail"] = $this->input->post("paypalEmail");
                }

                $data["configuration"] = json_encode($config);

                /*echo "<pre>";
                print_r($data);
                */

                if ($this->input->post('pm_id')) {
                    $result = $this->store_model->update_pm($this->input->post('pm_id'), $data);
                    if ($result) {
                        $msg = "Payment method has been updated successfully!";
                    } else {
                        $msg = "Payment method has been failed to update!";
                    }
                } else {
                    $result = $this->store_model->create_pm($data);
                    if ($result) {
                        $msg = "Payment method has been successfully added!";
                    } else {
                        $msg = "Payment method has been failed to create!";
                    }
                }
                $this->session->set_flashdata('create_pm', $msg);   
                redirect('/admin/payment_method/' . $pm);
            }
        }
    }

    public function history_for_admin()
    {
        $this->load->view('front_pages/dashboard/dash_header');
        $data['history'] = $this->user_model->billing_history_for_admin();
        $this->load->view('front_pages/dashboard/billing_history_for_admin', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function users(){
        $users = $this->db->get('dmd_users')->result_array();
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/users',['users' => $users]);
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    public function stores($user_id=""){
        $stores = [];
        $user = [];
        if($user_id != ""){
            $stores = $this->db->get_where('dmd_stores', ['user_id' => $user_id])->result_array();
            $user = $this->db->get_where('dmd_users', ['id' => $user_id], 1)->row_array();
        }
        
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/stores', ['stores' => $stores, 'user'=>$user]);
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    public function products($store_id=""){
        $products = [];
        $store = [];
        if($store_id != ""){
            $products = $this->db->get_where('dmd_store_products',['store_id' => $store_id])->result_array();
            $store = $this->db->get_where('dmd_stores',['id' => $store_id])->row_array();
        }
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/store_products',['products' => $products, 'store'=>$store]);
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function delete_single_product($id)
    {
        $store_id = $this->db->get_where('dmd_store_products', ['product_code'=>$id], 1)->row_array();
        $store_id = $store_id['store_id'];

        $qrystr = "SELECT * FROM dmd_store_products  WHERE product_code= " . $id;
        $qryresult = $this->db->query($qrystr);
        $product = $qryresult->row_array();
        if (isset($product['id']) && $product['id'] != '') {
            $delete = $this->db->delete("dmd_store_products", array('product_code' => $id));
            if ($delete) {
                    // delete old image path
                $old_path = getcwd() . '/site_assets/products/' . $product['pro_image'];
                unlink($old_path);
                $delete_files = $this->db->delete("dmd_delivery_for_pro", array('product_id' => $id));
                $delete_emails = $this->db->delete("dmd_email_delivery_pro", array('product_id' => $id));
                $path = "./site_assets/products/".$id;
                $this->removeDirectory($path);

                $this->session->set_flashdata('success_msg',
                    'your product successfully deleted!');
                redirect('admin/products/' . $store_id);
            } else {
                $this->session->set_flashdata('error_msg', 'try again something going wrong');
                redirect('admin/products/' . $store_id);
            }
        } else {
            $this->session->set_flashdata('error_msg',
                'try again this product not exists in our system');
            redirect('admin/products/' . $store_id);
        }
    }

    public function removeDirectory($path) {
      $files = glob($path . '/*');
      foreach ($files as $file) {
          is_dir($file) ? removeDirectory($file) : unlink($file);
      }
      rmdir($path);
      return;
  }

  public function delete_store($store_id="")
  {
    if($store_id != ""){
        $store = $this->db->get_where('dmd_stores',['id'=>$store_id],1)->row_array();
        $result = $this->user_model->delete_store($store['user_id'],$store_id);
        if($result){
            $this->session->set_flashdata('success_msg',
                'Store successfully deleted!');
            redirect('admin/stores/' . $store['user_id']);
        }else{
            $this->session->set_flashdata('error_msg',
                'try again this store not exists in our system');
            redirect('admin/stores/' . $store['user_id']);
        }
    }
}

public function block_user($user_id="")
{
    if($user_id != ""){
        $result = $this->db->update('dmd_users', ['status' => 0], ['id' => $user_id]);
        if($result){
            $this->db->update('dmd_stores', ['status' => 0], ['user_id' => $user_id]);
            $this->session->set_flashdata('success_msg',
                'User has been blocked successfully!');
            redirect('admin/users');
        }else{
            $this->session->set_flashdata('error_msg',
                'User has been failed to blocked!');
            redirect('admin/users');
        }
    }
}

public function active_user($user_id="")
{
    if($user_id != ""){
        $result = $this->db->update('dmd_users', ['status' => 1], ['id' => $user_id]);
        if($result){
            $this->db->update('dmd_stores', ['status' => 1], ['user_id' => $user_id]);
            $this->session->set_flashdata('success_msg',
                'User has been activated successfully!');
            redirect('admin/users');
        }else{
            $this->session->set_flashdata('error_msg',
                'User has been failed to activated!');
            redirect('admin/users');
        }
    }
}

public function block_store($store_id="")
{
    if($store_id != ""){
        $store = $this->db->get_where('dmd_stores',['id'=>$store_id],1)->row_array();
        $result = $this->db->update('dmd_stores', ['status' => 0], ['id' => $store_id]);
        if($result){
            $this->session->set_flashdata('success_msg',
                'Store has been blocked successfully!');
            redirect('admin/stores/' . $store['user_id']);
        }else{
            $this->session->set_flashdata('error_msg',
                'Store has been failed to blocked!');
            redirect('admin/stores/' . $store['user_id']);
        }
    }
}

public function active_store($store_id="")
{
    if($store_id != ""){
        $store = $this->db->get_where('dmd_stores',['id'=>$store_id],1)->row_array();
        $result = $this->db->update('dmd_stores', ['status' => 1], ['id' => $store_id]);
        if($result){
            $this->session->set_flashdata('success_msg',
                'Store has been activated successfully!');
            redirect('admin/stores/' . $store['user_id']);
        }else{
            $this->session->set_flashdata('error_msg',
                'Store has been failed to activated!');
            redirect('admin/stores/' . $store['user_id']);
        }
    }
}

public function tax(){
        //$users = $this->db->get('dmd_users')->result_array();
    $this->load->view('front_pages/dashboard/dash_header');
        //$this->load->view('front_pages/dashboard/users',['users' => $users]);
    $this->load->view('front_pages/dashboard/users');
    $this->load->view('front_pages/dashboard/dash_footer');
}


public function send_message(){
    if($this->input->post('all') == 1){
        $arr = $this->input->post('AllUsers');

    }else{
       $arr = $this->input->post('users');
   }
   // $arr = $this->input->post('users');
   $usersArr =  explode(",",$arr);
   $users    =  $this->User_model->getGroupUser($usersArr);
   $groups   =  round(count($users)/100);

   if($groups == 0){
    $groups = 1;
}
for($i = 0; $i < $groups; $i++){
    $emails   =  array();
    if($i == 0){
        if($groups == 1){
            $inc = count($users);
        }else{
            $inc =    $inc = $i+100;
        }
    }else{
     $inc = $i+100;
 }

 for($a =$i; $a < $inc; $a++){
      // array_push($emails, $users[$a]['email']);
    if($a < count($users)){
        array_push($emails, $users[$a]['email']);
    }
}

    // print_r("$this->input->post('all')");die;
$this->messages($emails, $this->input->post('message'));

}
redirect('admin/users');


}

public function email_mailgun($to,$message){

    $ci =& get_instance();

    $api_key= $ci->config->item('mailgun_api_key'); 

    $domain =$ci->config->item('domain'); 

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$api_key);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/'.$domain.'/messages');
    curl_setopt($ch, CURLOPT_POSTFIELDS, array(
     'from' => 'Suprime <eegames@gmail.com>',
     'to' => $to,
     'subject' => 'Digital Media Deliveries',
     'html' => $message
 ));
    $result = curl_exec($ch);

    curl_close($ch);
    return $result;

}


public function messages($emails, $message){
// print_r($emails);die;
    $subject = 'Digitalmediadeliveries';
    $this->callConfig();
    $email_message = $this->load->view('front_pages/emtemp/content_email', '', true);
    $email_message = str_replace("{content}", $message, $email_message);

    $to = $emails;
    $this->email->to($to);

    $this->email->subject($subject);
    $this->email->message($email_message);

    if($this->email->send()){
       $this->session->set_flashdata('success_msg', "Message has been delivered successfully. ");
       // $this->email->clear(TRUE);
   }else{
       $this->session->set_flashdata('error_msg', "Server is not  responding, Please try again. ");
   }


}


function delete(){
    $table = 'dmd_store_products';
    $user_id = $this->input->post('user');
    $stores = $this->store_model->get_stores($user_id );

    if($stores){
        foreach($stores as $store){
            $this->User_model->delete_store( $user_id , $store['id']);
        }
    }
    $this->User_model->delete_user($user_id);
    $this->session->set_flashdata('success_msg', "User has beeen deleted successfully.");
    redirect('admin/users');

}
// foreach ($users as $user) {
//     $user['email'] = "eegames@mailinator.com"; 
//     $to = $user['email'];
//     $this->email->to($to);
//     $this->email->subject($subject);
//     $this->email->message($email_message);

//     if($this->email->send()){
//        $this->session->set_flashdata('success_msg', "Message has been delivered successfully. ");
//    }else{
//        $this->session->set_flashdata('error_msg', "Server is not responding, Please try again. ");
//    }
//    $this->email->clear(TRUE);
// }



public function callConfig()
{
   $config['protocol'] = 'smtp';
   $config['smtp_crypto'] = 'ssl';
   $config['smtp_host'] = 'smtpout.secureserver.net';
   $config['smtp_user'] = 'do-not-reply@digitalmediadeliveries.com';
   $config['smtp_pass'] = 'WyWtAsHyHuHaDbAoTs2019';
   $config['smtp_port'] = 465;
   $config['smtp_timeout'] = 60;
   $config['newline'] = "\r\n";
   $config['mailtype'] = 'html';
   $config['charset'] = 'iso-8859-1';
   $this->load->library('email', $config);
   $this->email->initialize($config);
   $this->email->from('do-not-reply@digitalmediadeliveries.com', 'DMD');
}


public function metaTags(){

    $id = 0;
    $data['tagID'] = $id;
    $data['tag'] = $this->Resources_model->get_tag($id);
    $data['tags'] = $this->Resources_model->get_all_tags();
    // print_r($data);die;
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/admin_meta_tag',$data);
    $this->load->view('front_pages/dashboard/dash_footer');


}


public function addTag($id =0){
    $id = decode($id);
    $data['tag'] = $this->Resources_model->get_tag($id);
    if($this->input->post('submitted')== 'submit'){
        if($id > 0){
            $params['name'] = $this->input->post('name');
            $params['content'] = $this->input->post('content');

            $result = $this->Resources_model->update_tag($id, $params);
            $this->session->set_flashdata('success_msg', "Meta tag updated successfully.");
        }
        else{
          $params['name'] = $this->input->post('name');
          $params['content'] = $this->input->post('content');
          $result = $this->Resources_model->insert_tag($params);
          $this->session->set_flashdata('success_msg', "Meta tag created successfully.");
      }

      redirect('admin/metaTags');

  }else{


    $data['tagID'] = $id;
    $data['tags'] = $this->Resources_model->get_all_tags();
  // print_r($data);die;
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/admin_meta_tag',$data);
    $this->load->view('front_pages/dashboard/dash_footer');
}
}

function deleteTag($id){
    $id =decode($id);
    $tag = $this->Resources_model->get_tag($id);
    if($tag){
     $res =  $this->Resources_model->delete_tag($id);
 }

 $this->session->set_flashdata('success_msg', "tag has beeen deleted successfully.");
 redirect('admin/metaTags');

}



public function tracking(){

    $id = 0;
    $data['tagID'] = $id;
    $data['tag'] = $this->Resources_model->get_tracker($id);
    $data['tags'] = $this->Resources_model->get_all_trackers();
    // print_r($data);die;
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/admin_tracking',$data);
    $this->load->view('front_pages/dashboard/dash_footer');


}


public function addTracker($id =0){
    $id = decode($id);
    $data['tag'] = $this->Resources_model->get_tracker($id);
    if($this->input->post('submitted')== 'submit'){
        if($id > 0){
            $params['name'] = $this->input->post('name');
            $params['header_content'] = $this->input->post('headerContent');
            $params['body_content'] = $this->input->post('bodyContent');

            $result = $this->Resources_model->update_tracker($id, $params);
            $this->session->set_flashdata('success_msg', "Tracker code updated successfully.");
        }
        else{
            $params['name'] = $this->input->post('name');
            $params['header_content'] = $this->input->post('headerContent');
            $params['body_content'] = $this->input->post('bodyContent');

            $result = $this->Resources_model->insert_tracker($params);
            $this->session->set_flashdata('success_msg', "Traker code created successfully.");
        }

        redirect('admin/tracking');

    }else{


        $data['tagID'] = $id;
        $data['tags'] = $this->Resources_model->get_all_trackers();
  // print_r($data);die;
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/admin_tracking',$data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }
}

function deleteTracker($id){
    $id =decode($id);
    $tag = $this->Resources_model->get_tracker($id);
    if($tag){
        $res =  $this->Resources_model->delete_tracker($id);
    }

    $this->session->set_flashdata('success_msg', "Tracker has beeen deleted successfully.");
    redirect('admin/tracking');

}

function addUser(){
    $data['packages'] = $this->Package_model->getAll();
    if($this->input->post('user') == 'add'){
        $this->form_validation->set_rules('firstName', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lastName', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[dmd_users.email]');
        $this->form_validation->set_rules('package', 'package', 'trim|required');
        if ($this->form_validation->run()){
            $password = uniqid();
            $params = array(
                'f_name' => $_REQUEST['firstName'],
                'l_name' => $_REQUEST['lastName'],
                'display_name' => $_REQUEST['firstName'] . " " . $_REQUEST['lastName'],
                'email' => $_REQUEST['email'],
                'password' => md5($password),
                'verify' => 1,
                'is_admin_created' => 1,
            );
            $this->user_model->add_user($params,$_REQUEST['package']);
            $this->session->set_flashdata('success_msg', 'User created successfully, Credential has been sent on '.$params['email']);
            $email_message = $this->load->view('front_pages/emtemp/addUser', '', true);
            $email_message = str_replace("{name}", $params['display_name'], $email_message);
            $email_message = str_replace("{password}", $password, $email_message);
            $email_message = str_replace("{email}", $params['email'], $email_message);
            $this->callConfig();
            $subject = 'Welcome to digital Media Deliveries';
            $this->email->to($params['email']);
            $this->email->subject($subject);
            $this->email->message($email_message);
            $this->email->send();
            redirect('admin/users');
        }else{
            $this->session->set_flashdata('error_msg',"Email is already exist.");
        }
    }
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/add_user',$data);
    $this->load->view('front_pages/dashboard/dash_footer');
}


function managePackage($id = ''){
    if($id == ''){
        redirect('admin/users');
    }
    $data['user_id'] =  $id;
    $data['packages'] = $this->Package_model->getAll();
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/manage_user_package',$data);
    $this->load->view('front_pages/dashboard/dash_footer');
}






}// end controller
