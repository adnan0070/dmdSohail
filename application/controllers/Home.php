<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('store_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library("cart");

    }

    public function about()
    {
        $this->load->view('front_pages/front_header');
        $this->load->view('front_pages/about');
        $this->load->view('front_pages/front_footer');
    }
    public function getProrating($userid,$productid) {


      $id = $productid;
      // User rating
      $this->db->select('rating');
      $this->db->from('product_rating');
      //$this->db->where("customer_id", $userid);
      $this->db->where("product_id", $id);
      $userRatingquery = $this->db->get();

      $userpostResult = $userRatingquery->result_array();

      $userRating = 0;
      if(count($userpostResult)>0){
       $userRating = $userpostResult[0]['rating'];
   }

      // Average rating
   $this->db->select('ROUND(AVG(rating),1) as averageRating');
   $this->db->from('product_rating');
   $this->db->where("product_id", $id);
   $ratingquery = $this->db->get();

   $postResult = $ratingquery->result_array();

   $rating = $postResult[0]['averageRating'];

   if($rating == ''){
       $rating = 0;
   }

   $posts_arr = array("id"=>$id,"rating"=>$userRating,"averagerating"=>$rating);

   return $posts_arr;
}

public function index()
{
    $domain = $_SERVER['HTTP_HOST'];
    $domain_arr = $this->store_model->get_StoreRows('dmd_stores', 'store_url', $domain);
    //print_r($domain_arr);

    if (isset($domain_arr) && !empty($domain_arr)) {
        if($domain_arr['status'] == 0){
            show_error("This store has been blocked.", 404, "Blocked Store");
        }
        if($domain_arr['status'] == 2){
            show_error("This store has been deleted.", 404, "Deleted Store");
        }
        if($domain_arr['status'] == 1){
            $store = array();
            $store['storeid'] = $domain_arr['id'];
            $store['sub_store_name'] = $domain_arr['store_name'];
            $this->session->set_userdata('store_id', $domain_arr['id']);
            $this->session->set_userdata('store_name', $domain_arr['store_name']);
            $this->session->set_userdata('logo', $domain_arr['logo']);
            
            $store['controller']=$this;
            $store["total_store_product_count"] = $this->product_model->total_store_product_count();
            $store['payment_method'] = $this->product_model->store_payment_method_check();
            if($store['payment_method']){
                $get_products_arr = $this->product_model->get_StoreProducts($store['storeid']);
                if (isset($get_products_arr)) {
                    $store['products'] = $get_products_arr;
                    $store['countp'] = count($get_products_arr);
                }
            }   

            $store['store_data'] = $domain_arr;
            $store['feature_products'] = $this->product_model->getAllFeaturedProducts($domain_arr['id']);

            $this->load->view('front_pages/store/store_header', $store);
            $this->load->view('front_pages/store/index', $store);
            $this->load->view('front_pages/store/store_footer', $store);
        }
    } else {
        //echo $domain;
        $store = explode('.', $domain);
        if($store[0] == 'digitalmediadeliveries' || $store[1] == 'digitalmediadeliveries'){
        //if($store[0] == 'dmd'){
            $data['homepage'] = 'home';
            $this->load->view('front_pages/front_header', $data);
            $this->load->view('front_pages/home');
            $this->load->view('front_pages/front_footer');
        }else{
            show_error("Please enter a valid URL.", 404, "No store found!");
        }
    }
}


public function collection()
{
    $domain = $_SERVER['HTTP_HOST'];

    $domain_arr = $this->store_model->get_StoreRows('dmd_stores', 'store_url', $domain);
    if (isset($domain_arr) && !empty($domain_arr)) {

        if($domain_arr['status'] == 0 || $domain_arr['status'] == 2){
            redirect('http://' . $domain);
        } 

        $store = array();
        $store['storeid'] = $domain_arr['id'];
        $store['sub_store_name'] = $domain_arr['store_name'];
        $this->session->set_userdata('store_id', $domain_arr['id']);
        $this->session->set_userdata('store_name', $domain_arr['store_name']);
        $this->session->set_userdata('logo', $domain_arr['logo']);
        
        $store['controller']=$this;
        $store["total_store_product_count"] = $this->product_model->total_store_product_count();
        $store['payment_method'] = $this->product_model->store_payment_method_check();
        if($store['payment_method']){
            $get_products_arr = $this->product_model->get_StoreProducts($store['storeid']);
            if (isset($get_products_arr)) {
                $store['products'] = $get_products_arr;
                $store['countp'] = count($get_products_arr);
            }
        }   

        $this->load->view('front_pages/store/store_header', $store);
        $this->load->view('front_pages/store/home', $store);
        $this->load->view('front_pages/store/store_footer', $store);

    } else {
        //echo $domain;
        $store = explode('.', $domain);
        //if($store[0] == 'digitalmediadeliveries'){
        if($store[0] == 'dmd'){
            $this->load->view('front_pages/front_header');
            $this->load->view('front_pages/home');
            $this->load->view('front_pages/front_footer');
        }else{
            show_error("Please enter a valid URL.", 404, "No store found!");
        }
    }
}

public function showCart()
{

    $store = array();
    $store['cart'] = $this->cart->contents();

    $domain = $_SERVER['HTTP_HOST'];
    $domain_arr = $this->store_model->get_StoreRows('dmd_stores', 'store_url', $domain);
    if (isset($domain_arr) && !empty($domain_arr)) {
        if($domain_arr['status'] == 0 || $domain_arr['status'] == 2){
            redirect('http://' . $domain);
        } 

        $store_id = $domain_arr['id'];
        $getRand_products_arr = $this->product_model->get_RandProduct($store_id);
        $store['storeid'] = $domain_arr['id'];
        $store['sub_store_name'] = $domain_arr['store_name'];
    }

    if (isset($getRand_products_arr) && !empty($getRand_products_arr)) {
        $store['getRand_products_arr'] = $getRand_products_arr;
    } else {
        $store['getRand_products_arr'] = array();
    }
    $store_logo = $this->store_model->get_store_logo();
    if($store_logo){
        $store['store_logo'] = $store_logo['image'];
    }

    $this->load->view('front_pages/store/store_header', $store);
    $this->load->view('front_pages/store/cart', $store);
    $this->load->view('front_pages/store/store_footer');

}
public function showProduct()
{
    $cuid = $this->session->userdata('customer_details');
    $cuid = $cuid['customerid'];

    $store = array();
    $product_code =$this->uri->segment(3);
    $method = $this->uri->segment(4);
    $get_products_arr = $this->product_model->get_SingleProduct_Bycode($product_code);
    $store_id = $get_products_arr[0]->store_id;
    

        //get cross sell products
    $cross_sell_products = $get_products_arr[0]->cross_sell_product;
    $cross_sell_products_arr = json_decode($cross_sell_products, true);
        // $getRand_products_arr = $this->product_model->get_RandProduct($store_id,$product_code);
    if (!empty($cross_sell_products_arr)) {

        $getCross_products_arr = $this->product_model->get_CrossProduct($store_id, $product_code,
            $cross_sell_products_arr);
    } else {
        $getCross_products_arr = array();
    }


        //get up-sell products
    $up_sell_products = $get_products_arr[0]->upsel_product;
    $up_sell_products_arr = json_decode($up_sell_products, true);
    if (!empty($up_sell_products_arr)) {
        $get_up_sell_products_arr = $this->product_model->get_upsell_Product($store_id, $product_code, $up_sell_products_arr);
    } else {
        $get_up_sell_products_arr = array();
    }



    if (isset($get_products_arr)) {
        $store['product'] = $get_products_arr;
        $store['cross_sell_products'] = $getCross_products_arr;
        $store['up_sell_products'] = $get_up_sell_products_arr;
    }

    $domain = $_SERVER['HTTP_HOST'];
    $domain_arr = $this->store_model->get_StoreRows('dmd_stores', 'store_url', $domain);
    if (isset($domain_arr) && !empty($domain_arr)) {
        //print_r($domain_arr);die;
        if($domain_arr['status'] == 0 || $domain_arr['status'] == 2){
            redirect('http://' . $domain);
        } 
        $store['sub_store_name'] = $domain_arr['store_name'];
    }

    $store['login']=0;
    if($cuid!=""){
        $store['login']=1;
    }
    $store['already'] = "0";
    $chk_by_customer = $this->db->query('select id from comments where customer_id = "'.$cuid.'" and product_id="'.$product_code.'" ');
    if($chk_by_customer->num_rows()>0){
      $store['already'] = "1";   
  }
  $store['cu_id'] = $cuid;    
  $store['comments'] = array();
  $comments = $this->db->query('select cu.*,cust.* from comments as cu left join dmd_customers as cust on(cust.id=cu.customer_id)
    where product_id="' . $product_code . '"');
  if ($comments->num_rows() > 0) {
    $store['comments'] = $comments->result_array();
}

$this->load->view('front_pages/store/store_header', $store);
if($method =='webinar'){
    $this->load->view('front_pages/store/webinar', $store);
}else{
    $this->load->view('front_pages/store/single_product', $store);
}

$this->load->view('front_pages/store/store_footer');
}

public function loadProduct()
{
    if ($this->input->post()) {
        $data = $this->input->post();
            //print_r($data);die();

        if (!empty($data["search"])) {
            $search = $data["search"];
        } else {
            $search = "";
        }


            /*if (!empty($data["min"])) {
            $min = $data["min"];
            } else {
            $min = "";
            }

            if (!empty($data["max"])) {
            $max = $data["max"];
            } else {
            $max = "";
        }*/

        if (!empty($data["offset"])) {
            $offset = $data["offset"];
        } else {
            $offset = "0";
        }

        if (!empty($data["limit"])) {
            $limit = $data["limit"];
        } else {
            $limit = "9";
        }

        $products = $this->product_model->load_product($offset, $limit, $search);
        $products["total_store_product_count"] = $this->product_model->total_store_product_count();
        $products['controller']=$this;
        if (!empty($products["result"])) {
            $products['products'] = $products["result"];
            $products['countp'] = $products["count"];
            $products['search'] = 'search';
            $this->load->view("front_pages/store/home_products", $products);
        } else {
            return false;
        }
    }
}

public function range_product()
{
    if ($this->input->post()) {
        $data = $this->input->post();
        $products = $this->product_model->range_price_product($data);
        $products['products'] = $products;
        $products['controller'] = $this;

        $this->load->view("front_pages/store/home_products", $products);
    }
}
public function contact()

{

    $domain = $_SERVER['HTTP_HOST'];

    $domain_arr = $this->store_model->get_StoreRows('dmd_stores', 'store_url', $domain);
    if (isset($domain_arr) && !empty($domain_arr)) {
        if($domain_arr['status'] == 0 || $domain_arr['status'] == 2){
            redirect('http://' . $domain);
        } 

        $store = array();
        $store['storeid'] = $domain_arr['id'];
        $store['sub_store_name'] = $domain_arr['store_name'];
        $this->session->set_userdata('store_id', $domain_arr['id']);
        $this->session->set_userdata('store_name', $domain_arr['store_name']);

        $store['controller']=$this;
    }

    if($this->input->post()){
        $data = $this->input->post();
        //print_r($data);
        $empty_message = "";
        foreach ($data as $key => $val) {
            if ($val == "") {
                $empty_message .= $key . " is required.<br><br>";
            }
        }
        if ($empty_message != "") {
            $this->session->set_flashdata('error', $empty_message);
        }
        else{
            $to = 'do-not-reply@digitalmediadeliveries.com';
        //$to = 'thezainiji@gmail.com';
            $subject = $data["subject"];
            $message = $data["message"];
            $config['protocol'] = 'smtp';
            $config['smtp_crypto'] = 'ssl';
            $config['smtp_host'] = 'smtpout.secureserver.net';
            $config['smtp_user'] = 'do-not-reply@digitalmediadeliveries.com';
            $config['smtp_pass'] = 'WyWtAsHyHuHaDbAoTs2019';
            $config['smtp_port'] = 465;
            $config['smtp_timeout'] = 5;
            $config['newline'] = "\r\n";
            $config['mailtype'] = 'html';
            $config['charset'] = 'iso-8859-1';
            $this->load->library('email', $config);
            $this->email->initialize($config);

            $this->email->to($to);
            $this->email->from($data["email"], $data["name"]);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();

            $this->session->set_flashdata('success', 'Your message has been successfully send!');
        }
        //redirect('pages/front/support');
    } 

    $this->load->view('front_pages/store/store_header');
    $this->load->view('front_pages/store/contact', $store);
    $this->load->view('front_pages/store/store_footer');
}

}
