<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'cart'));
        $this->load->model('user_model');
        $this->load->model('Subadmin_model');
        $this->load->model('store_model');
        $this->load->model('package_model');
        $this->load->model('Resources_model');


        $integration = $this->store_model->get_integration_detail('mailchimp');
        if($integration){
            $this->load->library('mailchimp', $integration);
        }

        $integration = $this->store_model->get_integration_detail('getresponse');
        if($integration){
            $this->load->library('getresponse', $integration);
        }

        $this->callConfig();
    }

    function mypdf()
    {


        $data = array('order_number' => "123445");
        //load the view and saved it into $html variable
        $html = $this->load->view('welcome_message', $data, true);

        //this the the PDF filename that user will get to download
        $pdfFilePath = "output_pdf_name.pdf";

        //load mPDF library
        $this->load->library('m_pdf');

        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }
    public function callConfig()
    {
        $config['protocol'] = 'smtp';
        $config['smtp_crypto'] = 'ssl';
        $config['smtp_host'] = 'smtpout.secureserver.net';
        $config['smtp_user'] = 'do-not-reply@digitalmediadeliveries.com';
        $config['smtp_pass'] = 'WyWtAsHyHuHaDbAoTs2019';
        $config['smtp_port'] = 465;
        $config['smtp_timeout'] = 5;
        $config['newline'] = "\r\n";
        $config['crlf'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->from('do-not-reply@digitalmediadeliveries.com', 'Digital Media Deliveries');
    }
    public function knowl_base()
    {
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/knowl_base', array("base" => "update"));
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    public function new_updates()
    {
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/knowl_base', array("data" => "update"));
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    public function video()
    {
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/knowl_base', array("video" => "video"));
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    public function demo()
    {
        $this->load->view('front_pages/front_header');
        $this->load->view('front_pages/demo');
        $this->load->view('front_pages/front_footer');
    }
    public function pricing($period = 'Month')
    {
        $packages = $this->package_model->getByPeriod($period);
        for($i=0; $i<count($packages); $i++){
            $packages[$i]['pack_code'] = encode($packages[$i]['pack_code']);
        }
        $data['packages'] = $packages;
        $data['period']   = $period;
        $this->load->view('front_pages/front_header');
        $this->load->view('front_pages/pricing', $data);
        $this->load->view('front_pages/front_footer');
    }
    public function faq()
    {
        $this->load->view('front_pages/front_header');
        $this->load->view('front_pages/faq'); 
        $this->load->view('front_pages/front_footer');
    }
    public function resources()
    {
        $data["resources"] = $this->Resources_model->get_all(); 
        $this->load->view('front_pages/front_header' ,$data);
        $this->load->view('front_pages/resources', $data);
        $this->load->view('front_pages/front_footer');
    }
    
    public function visit(){
      $this->load->view('front_pages/front_header');
      $this->load->view('front_pages/public_page');
      $this->load->view('front_pages/front_footer');
  }

  public function sitemap()
  {
    $this->load->view('front_pages/front_header');
    $this->load->view('front_pages/sitemap');
    $this->load->view('front_pages/front_footer');
}
public function register($selected_package='')
{
    $data['selected_package'] = $selected_package;
    $this->load->view('front_pages/front_header');
    $this->load->view('front_pages/register',$data);
    $this->load->view('front_pages/front_footer');
}

public function login()
{
    $this->load->view('front_pages/front_header');
    $this->load->view('front_pages/login');
    $this->load->view('front_pages/front_footer');
}
public function subscription()
{
    $enable_pm = $this->store_model->enable_admin_pm();
    $subs['enable_pm'] = $enable_pm;
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/subscription',$subs);
    $this->load->view('front_pages/dashboard/dash_footer');
}
public function history()
{
    $data['history'] = $this->user_model->billing_history();
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/billing_history', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function repCard($CARD_NUM)
{
    return str_repeat('*', strlen($CARD_NUM) - 4) . substr($CARD_NUM, -4);
}

public function generateProCode($digits_needed = 10)
{


        $random_number = ''; // set up a blank string

        $count = 0;

        while ($count < $digits_needed) {
            $random_digit = mt_rand(0, 9);

            $random_number .= $random_digit;
            $count++;
        }
        return $random_number;
    }
    public function paypal_buy()
    {
        /*echo "paypal payment method in progress";
        die();*/
        if ($this->input->post()) {
            $postData = $this->input->post();

            // check fields validation
            $empty_message = "";
            foreach ($postData as $key => $val) {
                if ($val == "") {
                    $empty_message .= $key . " is required field<br>";
                }
            }
            if ($empty_message != "") {
                $this->session->set_flashdata('error_msg', $empty_message);
                if (isset($postData['subscription'])) {
                    redirect('user/subscription');
                }
                if (isset($postData['customer'])) {
                    redirect('cart/checkout');
                }
            }

            $userData = array(
                "f_name" => $postData['f_name'],
                "l_name" => $postData['l_name'],
                "email" => $postData['email'],
                "shipping_country" => $postData['country']);

            //check payment from store or customer
            $insert_id = '0';
            if (!isset($postData['subscription'])) {
                $user_id = $this->db->get_where('dmd_stores', array('id' => $postData["store_id"]));
                $user_id = $user_id->row_array();
                // check customer exists or not
                $query = $this->db->query("select * from dmd_customers where email='" . $postData['email'] .
                    "'");

                //if exists get customer data from db
                if ($query->num_rows() > 0) {
                    $userData = $query->row_array();
                } else {
                    // create customer

                    $userData['user_id'] = $user_id;
                    $userData['store_id'] = $postData["store_id"];

                    $insert = $this->db->insert('dmd_customers', $userData);
                    if ($insert) {
                        $userData['id'] = $this->db->insert_id();

                        $mail_data = array(
                            "f_name" => $postData['f_name'],
                            "l_name" => $postData['l_name'],
                            "email" => $postData['email']
                        );
                        
                        //MailChimp integration
                        $this->mailchimp_integration($mail_data);

                        //GetResponse integration
                        $this->getresponse_integration($mail_data);


                    } else {
                        $this->session->set_flashdata('error_msg',
                            'Customer profile creation has been failed!');
                        redirect('cart/checkout');
                    }
                }
                $insert_id = $userData['id'];
            }

            if (!isset($postData['subscription'])) {
               $store = $this->db->get_where('dmd_stores', array('id' => $postData["store_id"]));
               $store = $store->row_array();
               $keys = $this->user_model->get_admin_keys($store['user_id'], $name = 'paypal');
               $conf = json_decode(@$keys['configuration']);
               $buss_email = $conf->paypalEmail;
           //$conf->paypalEmail
           }else{
             $admin = $this->user_model->get_admin();
             $keys = $this->user_model->get_admin_keys($admin['id'], $name = 'paypal');
             $conf = json_decode(@$keys['configuration']);
             $buss_email = $conf->paypalEmail;
         }

         $parms = array(
            "sandbox" => "false",
            "business" => $buss_email,
            "paypal_lib_ipn_log" => true,
            "paypal_lib_ipn_log_file" => "site_assets/logs/paypal_ipn.log",
            "paypal_lib_button_path" => "site_assets/images/",
            "paypal_lib_currency_code" => "USD");
         $this->load->library('paypal_lib', $parms);
            // Set variables for paypal form
         $returnURL = base_url('user/paypal_success');
         $cancelURL = base_url('user/paypal_cancel');
         $notifyURL = base_url('user/paypal_ipn');


            // Get product data from the database
            //$product =  //$this->product->getRows($id);


            // Get current user ID from the session
         $userID = $this->session->userdata('user_id');
         $subscription = 'subscription';
         if (!isset($_REQUEST['subscription'])) {
                $userID = $userData['id']; //customer id
                $subscription = 'customer';
            }

            // Add fields to paypal form

            if (isset($_REQUEST["subscription"])) {


                $this->paypal_lib->add_field('txn_type', "recurring_payment");
                $this->paypal_lib->add_field('item_number', $postData['package']);
                $this->paypal_lib->add_field('item_name', $postData['pack_title']);
                $this->paypal_lib->add_field('cmd', "_xclick-subscriptions");
                $this->paypal_lib->add_field('a3', $postData['amount']);

                $postData["amount"] = $this->getAmount2($postData['package']);
                $this->paypal_lib->add_field('a1', $postData["amount"]);
                $this->paypal_lib->add_field('p1', 1);
                $this->paypal_lib->add_field('t1', 'M');

                //$this->paypal_lib->add_field('amount', $postData["amount"]);


                $this->paypal_lib->add_field('p3', 1);
                $this->paypal_lib->add_field('t3', 'M');
                $this->paypal_lib->add_field('src', 1);
                $this->paypal_lib->add_field('sra', 1);


            } else {
                $this->paypal_lib->add_field('cmd', "_xclick");
                $this->paypal_lib->add_field('item_number', '86768');
                $this->paypal_lib->add_field('amount', $this->cart->total());
                /*if ($this->cart->contents()) {
                $i = 0;
                foreach ($this->cart->contents() as $products) {
                $i++;
                $this->paypal_lib->add_field('item_number_' . $i, $products['id']);
                $this->paypal_lib->add_field('item_name_' . $i, $products['name']);
                $this->paypal_lib->add_field('quantity_' . $i, $products['qty']);
                $this->paypal_lib->add_field('amount_' . $i, $products['subtotal']);
                //$this->paypal_lib->add_field('invoice'.$i,       $products['subtotal'].'.0');
                }
            }*/
            $quantity = 0;
            foreach ($this->cart->contents() as $products) {
                $quantity = $quantity + $products['qty'];
            }
            $this->paypal_lib->add_field('quantity', $quantity);
        }

        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('custom', $userID . "|" . $subscription . "|" . $insert_id .
            "|" . $postData['store_id']);


        echo $this->paypal_lib->paypal_form();

            // Render paypal form
            //$this->paypal_lib->paypal_auto_form();
    }
}


public function stripe_payment()
{
        // get user id from store_id
    if ($this->input->post()) {
        $data = $this->input->post();

            //if resubmit stripe token
        if (isset($data['stripeToken'])) {
            $session_stripeToken = $this->session->userdata('stripeToken');
            if (isset($session_stripeToken) && ($session_stripeToken == $data['stripeToken'])) {
                $this->session->set_flashdata('error_msg',
                    'You have apparently resubmitted the request. Please do not do that and try again.');
                redirect('cart/checkout');
            } else {
                $this->session->set_userdata($data['stripeToken']);
            }
        } else {
            $this->session->set_flashdata('error_msg',
                'Your payment details cannot be processed. You have not been charged. Please confirm that you have JavaScript enabled and try again.');
            redirect('cart/checkout');
        }

        $user_id = $this->db->get_where('dmd_stores', array('id' => $data["store_id"]));
        $user_id = $user_id->row_array();
        $keys = $this->db->query("select * from payment_methods where user_id = '" . $user_id['user_id'] .
            "'and `name`='stripe'");
        if ($keys->num_rows() > 0) {
            $keys = $keys->row_array();
            $stkey = json_decode($keys['configuration'], 'array');
            $pubkey = $stkey['publishableApiKey'];
            $apikey = $stkey['apiKey'];

            $this->load->library('stripegateway', array("secret_key" => $apikey,
                "public_key" => $pubkey));


                $chk_flag = "customer"; //need

                /*$empty_message = "";
                foreach ($data as $key => $val) {
                    if ($val == "") {
                        if ($key == "f_name") {
                            $empty_message .= "first name is required.<br><br>";
                        } elseif ($key == "l_name") {
                            $empty_message .= "last name is required.<br><br>";
                        } else {
                            $empty_message .= $key . " is required.<br><br>";
                        }
                    }
                }
                if ($empty_message != "") {
                    $this->session->set_flashdata('error_msg', $empty_message);
                    if ($chk_flag == 'store') {
                        redirect('user/subscription');
                    }
                    if ($chk_flag == 'customer') {
                        redirect('cart/checkout');
                    }
                }*/

                if ($chk_flag == 'customer') {
                    // check customer exists or not
                    $query = $this->db->query("select * from dmd_customers where email='" . $data['email'] .
                        "'");

                    //if exists get customer data from db
                    $new_cus = false;
                    if ($query->num_rows() > 0) {
                        $customer = $query->row_array();
                    } else {
                        $new_cus = true;
                        // create customer

                        $user_pass = $this->generateProCode();
                        $customer = array(
                            "f_name" => $data['f_name'],
                            "l_name" => $data['l_name'],
                            "email" => $data['email'],
                            "shipping_country" => $data['country'],
                            "store_id" => $data['store_id'],
                            "password" => md5($user_pass)
                        );

                        $insert = $this->db->insert('dmd_customers', $customer);
                        if ($insert) {
                            $customer['id'] = $this->db->insert_id();

                            $mail_data = array(
                                "f_name" => $data['f_name'],
                                "l_name" => $data['l_name'],
                                "email" => $data['email'],
                            );

                            //MailChimp integration
                            $this->mailchimp_integration($mail_data);

                            //GetResponse integration
                            $this->getresponse_integration($mail_data);

                            //send password to new customer
                            $customer['password'] = $user_pass;
                            $this->new_cst_email($customer);

                        } else {
                            $this->session->set_flashdata('error_msg',
                                'Customer profile creation has been failed!');
                            redirect('cart/checkout');
                        }
                    }
                }


                $orderData = array(
                    //"transID" => ,
                    "order_number" => time(),
                    "customer_id" => $customer['id'],
                    "store_id" => $data["store_id"],
                    'user_id' => $user_id['user_id'],
                    "shipping_country" => $data['country'],

                    "total_amount" => $this->session->userdata('sub_total'),
                    "tax" => $this->session->userdata('tax_amount'),
                    "grand_total" => $this->session->userdata('cart_total'),
                    //"total_amount" => $this->session->userdata('cart_total'), //$this->cart->total(), //$postData['amount'],
                    //"grand_total" => $this->session->userdata('cart_total'), //$this->cart->total(), //$postData['amount'],
                    //"coupon_discount" => '10',
                    "status" => 'pending',
                    "created_date" => date('Y-m-d H:i:s'),
                    "deliver_status" => 'pending',
                    "Payment_method" => "stripe");

                //print_r($orderData);
                $insert_order = $this->db->insert('dmd_orders', $orderData);
                $order_id = $this->db->insert_id();

                $order_items = array();
                $products_attchment = array();
                // get cart products
                foreach ($this->cart->contents() as $products) {
                    // check coupon
                    /*$exp_date = date('Y-m-d');
                    $discount = 0;
                    $getCoupon = $this->db->query("SELECT pro.*,dmd_coupon.discount,dmd_coupon.max_use,dmd_coupon.expire_date FROM `dmd_store_products` as pro left join dmd_coupon on(dmd_coupon.id=pro.enable_coupon) WHERE pro.`product_code` = '" .
                    $products['id'] . "' and dmd_coupon.expire_date>='".$exp_date."' and dmd_coupon.max_use!='0'");
                    if ($getCoupon->num_rows() > 0) {
                    $getCoupon = $getCoupon->row_array();
                    $discount = $getCoupon['discount'];
                    }
                    if($discount=='0'){
                    $products['price']    = $products['Origprice']; 
                    $products['discount'] = $discount;  
                }*/
                if ($products['discount'] != "0") {
                    $getCoupon = $this->db->query("SELECT pro.*,dmd_coupon.discount,dmd_coupon.id as coid,dmd_coupon.max_use,dmd_coupon.expire_date FROM `dmd_store_products` as pro left join dmd_coupon on(dmd_coupon.id=pro.enable_coupon) WHERE pro.`product_code` = '" .
                        $products['id'] . "' and dmd_coupon.max_use!='0'");
                    if ($getCoupon->num_rows() > 0) {
                        $getCoupon = $getCoupon->row_array();
                        $discount_id = $getCoupon['coid'];
                        $max_us = $getCoupon['max_use'] - 1;
                        $this->db->where('id', $discount_id);
                        $this->db->update("dmd_coupon", array('max_use' => $max_us));
                    }
                }

                $order_items = array(
                    'product_id' => $products['id'],
                    'order_id' => $order_id,
                    'product_quantity' => $products['qty'],
                    'customer_id' => $customer['id'],
                    'product_price' => $products['price'],
                    'coupon_discount' => $products['discount'],
                    'total_price' => ($products['subtotal']));
                $insert_items = $this->db->insert('dmd_order_items', $order_items);
                $get_item_id = $this->db->insert_id();

                $getattch = $this->db->get_where('dmd_delivery_for_pro', array('product_id' => $products['id']));

                foreach ($getattch->result_array() as $file_attach) {
                    $order_files = array(
                        'product_id' => $products['id'],
                        'order_id' => $order_id,
                        'item_id' => $get_item_id,
                        'file_path' => $file_attach['upload_file'],
                        'file_id' => $file_attach['id']);
                    $insert_files = $this->db->insert('dmd_order_files', $order_files);
                        /*$file_attach['upload_file'] = base_url() . 'site_assets/products/' . $products['id'] .
                        '/' . $file_attach['upload_file'];
                        array_push($products_attchment, $file_attach['upload_file']);*/

                    }


                }

                $data["amount"] = (float)$this->session->userdata('cart_total') * 100;
                $data["currency"] = "usd";
                $data["customer_id"] = $customer['id'];
                $data["order_id"] = $order_id;
                $data["transId"] = "";

                $customer['order_number'] = $orderData['order_number'];
                $customer['store_name'] = $user_id['store_name'];
                $customer['store_person_name'] = $user_id['contact_person'];
                $customer['support_email'] = $user_id['support_email'];
                $customer['display_name'] = $customer['f_name'] . " " . $customer['l_name'];
                $customer['user_id'] = $user_id['user_id'];


                $data["transId"] = $this->stripegateway->charge_payment($data);
                if ($data["transId"] != "failed" && $data["transId"] != "") {
                    //print_r($data["transId"]);

                    if ($insert_order) {

                        foreach ($this->cart->contents() as $products) {
                            $this->product_email($products['id'], $data['email'],  $customer['display_name']);
                        }


                        $this->cart->destroy();

                        //update status and trans id to paid when payment successfully made!
                        $status = array('transID' => $data["transId"], 'status' => 'paid');
                        $this->db->where('id', $order_id);
                        $this->db->update('dmd_orders', $status);

                        //Payment Thank You Email
                        $EmailData = array('customer' => $customer, 'type' => "thankyou");

                        $this->final_email($EmailData);
                        $EmailData = array('customer' => $customer, 'type' => "purchase_thankyou");
                        $this->final_email($EmailData);

                        //purchase thank you email


                        $purchaseEmail = array(
                            'f_name' => $customer["f_name"],
                            'l_name' => $customer["l_name"],
                            'email' => $customer["email"],
                            //'attachment_list' => $products_attchment,
                            'purchase_id' => $order_id);


                        //$this->purchase_email($purchaseEmail);


                        $this->session->set_flashdata('success_msg',
                            'Thank you for purchasing. Your purchase is now completed. Please check your email for invoice detail.');

                        //print_r($this->session->userdata);die();

                        if ($this->session->userdata('customer_details')) {
                            redirect('customer/dashboard');
                        } else {
                            redirect('home/index');
                        }

                    } else {
                        $EmailData = array('customer' => $customer, 'type' => "purchase_pending");
                        $this->final_email($EmailData);
                        $this->session->set_flashdata('error_msg', 'Order is not created');
                        redirect('cart/checkout');
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Transaction faild');
                    //$return["data"] = $data;
                    $this->session->set_flashdata('data', $data);

                    $EmailData = array('customer' => $customer, 'type' => "purchase_pending");
                    $this->final_email($EmailData);

                    //print_r($data);die("sdsdsd");
                    redirect('cart/checkout');
                }
            }
        }
    }

    public function product_email($product_id, $cuemail, $customer_name='')
    {

        $email = $this->db->query('select * from dmd_email_delivery_pro where product_id="' .
            $product_id . '"'); //$this->user_model->get_email($data["type"]);
        if ($email->num_rows() > 0) {
            foreach ($email->result_array() as $email) {
              $this->callConfig();

              $message = $this->load->view('front_pages/emtemp/delivery_email', '', true);
              $message = str_replace("{content}", $email["email_desp"], $message);
              $message = str_replace("{name}", $customer_name, $message); 
              $subject = $email["subject"];
              $to = $cuemail;

              $this->email->to($to);
              $this->email->subject($subject);
              $this->email->message($message);
                //print($this->email);die();
              $this->email->send();
              $this->email->clear(TRUE);

          }
      }
  }

  public function final_email($data)
  {

    $this->callConfig();
    $customer = $data['customer'];

    $email = $this->user_model->get_email($data["type"], $customer['user_id']);

    if ($email) {


        $subject = $email["subject"];
            /*echo $email_message = '<style>
            .button {
                background-color: #4CAF50;
                border: none;
                color: white;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
            }
            </style>'; */
            $button_activate  =' <br><a href="' . base_url() . '/user/orderinvoice/' . $customer['order_number'] .
            '" class="home-form-btn " style="    font-size: 14px;
            color: #74a857;
            background-color: #fff;
            line-height: 1.3 !important;
            font-weight: 700;
            min-width: 150px;
            height: 30px;
            border: 2px solid #74a857;
            text-align: center;
            border-radius: 50px;
            text-decoration: none;
            text-transform: uppercase;
            padding: 10px 20px;"> Invoice</a><br>';

            $email_message = $this->load->view('front_pages/emtemp/content_email', '', true);
            /*$email_message .= $email["content"];*/

            /*$button_activate = '<a href="' . base_url() . '/user/orderinvoice/' . $customer['order_number'] .
            '" class="button">Invoice</a>';*/

            $email_message = str_replace("{content}", $email["content"], $email_message);
            
            $email_message = str_replace("{name}", $customer['display_name'], $email_message);
            $email_message = str_replace("{purchase_id}", $customer['order_number'], $email_message);
            $email_message = str_replace("{store_name}", $customer['store_name'], $email_message);
            $email_message = str_replace("{store_person_name}", $customer['store_person_name'], $email_message);
            $email_message = str_replace("{store_contact_email}", $customer['support_email'], $email_message);
            $email_message = str_replace("{invoice_button}", $button_activate, $email_message);
            /*$email_message .= $button_activate;*/


            $to = $customer['email'];
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($email_message);
        //print($this->email);die();
            $this->email->send();
        }

        //print_r($email);die();

        
        //$this->email->get_debugger_messages();die();
    }
    public function orderinvoice($orderid)
    {
        $data = array('order_number' => $orderid);
        //load the view and saved it into $html variable
        $html = $this->load->view('front_pages/emtemp/store_invoice', $data, true);

        //this the the PDF filename that user will get to download
        $pdfFilePath = "invoice.pdf";

        //load mPDF library
        $this->load->library('m_pdf');

        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }

    public function billing_invoice($bill_id)
    {
        //$data = array('order_number' => $orderid);
        $data['history'] = $this->user_model->billing_history($bill_id);
        //load the view and saved it into $html variable
        $html = $this->load->view('front_pages/emtemp/billing_invoice', $data, true);

        //print_r($html);

        //this the the PDF filename that user will get to download
        $pdfFilePath = "invoice.pdf";

        //load mPDF library
        $this->load->library('m_pdf');

        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }

    function authorize_net_payment()
    {
    // check fields validation
        if ($this->input->post()) {
            $postData = $this->input->post();
            //print_r($postData);die;

            $store_id = $postData['store_id'];
            $user_id = $this->db->get_where('dmd_stores', array('id' => $store_id));
            $user_id = $user_id->row_array();
        // get user bussines email
            $keys = $this->db->query("select * from payment_methods where store_id = '" . $store_id . "'and `name`='authorize'");
            if ($keys->num_rows() > 0) {
                $keys = $keys->row_array();
                $stkey = json_decode($keys['configuration'], 'array');

            //print_r($stkey);die();
                $dataapi = array(
                    'strAPILoginId' => $stkey['loginID'],
                    'strTransactionKey' => $stkey['transactionKey'],
                    // 'strValidationMode' => 'testMode');liveMode
                    'strValidationMode' => 'liveMode');
                $this->load->library('AuthorizeAPI', $dataapi);

            //print_r($dataapi);

                $chk_flag = "customer";
                if($this->input->post('subscription') == 'subscription'){
                    $chk_flag = "subscription";
                }

            //check fields validation
                $empty_message = "";
                foreach ($postData as $key => $val) {
                    if ($val == "") {
                        $empty_message .= $key . " is required field<br><br>";
                    }
                }
                if ($empty_message != "") {
                    $this->session->set_flashdata('error_msg', $empty_message);
                    if ($chk_flag == 'store') {
                        redirect('user/subscription');
                    }
                    if ($chk_flag == 'customer') {
                        redirect('cart/checkout');
                    }
                }

                $userData = array(
                    "f_name" => $postData['f_name'],
                    "shipping_country" => $postData['country'],
                    "l_name" => $postData['l_name'],
                    "email" => $postData['email']);


                if ($chk_flag == 'customer') {
                // check customer exists or not
                    $query = $this->db->query("select * from dmd_customers where email='" . $postData['email'] .
                        "'");

                //if exists get customer data from db
                    $new_cus = false;
                    if ($query->num_rows() > 0) {
                        $userData = $query->row_array();
                    } else {
                        $new_cus = true;
                    // create customer
                        $user_pass = $this->generateProCode();
                        $userData = array(
                            "f_name" => $postData['f_name'],
                            "l_name" => $postData['l_name'],
                            "email" => $postData['email'],
                            "shipping_country" => $postData['country'],
                            "store_id" => $postData['store_id'],
                            "password" => md5($user_pass)
                        );

                        $insert = $this->db->insert('dmd_customers', $userData);
                        if ($insert) {
                            $userData['id'] = $this->db->insert_id();
                            
                            $mail_data = array(
                                "f_name" => $postData['f_name'],
                                "l_name" => $postData['l_name'],
                                "email" => $postData['email']
                            );

                        //MailChimp integration
                            $this->mailchimp_integration($mail_data);

                        //GetResponse integration
                            $this->getresponse_integration($mail_data);

                        //send password to new customer
                            $userData['password'] = $user_pass;
                            $this->new_cst_email($userData);

                        } else {
                            $this->session->set_flashdata('error_msg',
                                'Customer profile creation has been failed!');
                            redirect('cart/checkout');
                        }
                    }

                }

                

                $get_name = $this->db->get_where('dmd_stores', array('id' => $store_id));
                $get_name = $get_name->row_array();
                $arrCustomerInfo = array();
                $arrCustomerInfo['firstname'] = $postData['f_name'];
                $arrCustomerInfo['lastname'] = $postData['l_name'];
                $arrCustomerInfo['company_name'] = $get_name['store_name'];
                $arrCustomerInfo['ad_street'] = $postData['street'];
                $arrCustomerInfo['ad_city'] = $postData['city'];
                $arrCustomerInfo['ad_state'] = $postData['state'];
                $arrCustomerInfo['ad_zip'] = $postData['zip'];
                $arrCustomerInfo['ad_country'] = $postData['country'];
                $arrCustomerInfo['ph_number'] = '1111';
                $arrCustomerInfo['em_email'] = $postData['email'];


                $this->authorizeapi->setCustomerAddress($arrCustomerInfo);

                $this->authorizeapi->setCreditCardParameters($postData['creditcard'], $postData['expiry'],
                    $postData['cvc']);

            /*if ($chk_flag == 'subscription') {
                $userData['id'] = $this->session->userdata('user_id');
            }*/


            $arrCustomerAddCCResponse = $this->authorizeapi->addCustomerPaymentProfile($userData['id'],
                'cc');


            $result = json_decode($arrCustomerAddCCResponse, 'array');

            //print_r($result);

            // if credit card number is invalid
            if($result["errorCode"] == "E00027"){
                $this->session->set_flashdata('error_msg', 'The credit card number is invalid.');
                redirect('cart/checkout');
            }

            if ($result["success"] == 0) {
                if (strpos($result["error"], 'duplicate') !== false) {
                    $id_err = $result["error"];
                    $id_err = explode("ID", $id_err);
                    $id_err = explode(" ", ltrim($id_err[1], " "));
                    $profile_id = $id_err[0];
                    $result["customerProfileId"] = $profile_id;

                    // add existing
                    $this->authorizeapi->setCreditCardParameters($postData['creditcard'], $postData['expiry'],
                        $postData['cvc']);

                    $arrCustomerAddCCResponse1 = $this->authorizeapi->addCustomerPaymentProfile($userData['id'],
                        'cc', true, $result["customerProfileId"]);
                    $arrCustomerAddCCResponse1 = json_decode($arrCustomerAddCCResponse1, 'array');

                    $flag = false;
                    if ($arrCustomerAddCCResponse1["error"] ==
                        'A duplicate customer payment profile already exists.') {

                        $check_card = $this->db->query('select customerPaymentProfileId from cst_payment_profiles 
                            where card_number="' . $this->repCard($postData["creditcard"]) .
                            '"');
                    $paymentProfile = $check_card->row_array();
                    $result["customerPaymentProfileId"] = $paymentProfile['customerPaymentProfileId'];
                }
                if (isset($arrCustomerAddCCResponse1['customerPaymentProfileId']) && $arrCustomerAddCCResponse1['customerPaymentProfileId'] !=
                    '') {
                        //echo "there";
                    $flag = true;
                $paymentProfile = array(
                    'customer_id' => $userData['id'],
                    'customerProfileId' => $result["customerProfileId"],
                    'customerPaymentProfileId' => $arrCustomerAddCCResponse1["customerPaymentProfileId"],
                    'card_number' => $this->repCard($postData["creditcard"]),
                    'card_expiry' => $postData["expiry"],
                    'card_cvc' => $postData["cvc"]);

                $insert = $this->db->insert('cst_payment_profiles', $paymentProfile);
                $result["customerPaymentProfileId"] = $arrCustomerAddCCResponse1['customerPaymentProfileId'];
            }

                    //die();
        }
    }
    if ($result["success"] == 1) {

        $this->db->select("*");
        $this->db->from("cst_payment_profiles");
        $this->db->where("card_number", $this->repCard($postData["creditcard"]));
        $query = $this->db->get();

        if ($query->num_rows() == 0) {

            $paymentProfile = array(
                'customer_id' => $userData['id'],
                'customerProfileId' => $result["customerProfileId"],
                'customerPaymentProfileId' => $result["customerPaymentProfileId"],
                'card_number' => $this->repCard($postData["creditcard"]),
                'card_expiry' => $postData["expiry"],
                'card_cvc' => $postData["cvc"]);

            $insert = $this->db->insert('cst_payment_profiles', $paymentProfile);
        }


    }
            //print_r($result);
    $arrChargeResponse = $this->authorizeapi->chargeCCeCheck($result["customerProfileId"],
        $result["customerPaymentProfileId"], $this->session->userdata('cart_total'));

    $arrChargeResponse = json_decode($arrChargeResponse, 'array');

    //print_r($arrChargeResponse);die;


    $orderData = array(
                //"transID" => ,
        "order_number" => time(),
        "customer_id" => $userData['id'],
        "store_id" => $store_id,
        'user_id' => $user_id['user_id'],
        "shipping_country" => $postData['country'],

        "total_amount" => $this->session->userdata('sub_total'),
        "tax" => $this->session->userdata('tax_amount'),
        "grand_total" => $this->session->userdata('cart_total'),
        //"total_amount" => $this->session->userdata('cart_total'), //$this->cart->total(), //$postData['amount'],
        //"grand_total" => $this->session->userdata('cart_total'), //$this->cart->total(), //$postData['amount'],
        //"coupon_discount" => '10',
        "status" => 'pending',
        "created_date" => date('Y-m-d H:i:s'),
        "deliver_status" => 'pending',
        "Payment_method" => "authorize");

            //print_r($orderData);
    $insert_order = $this->db->insert('dmd_orders', $orderData);
    $order_id = $this->db->insert_id();
    $order_items = array();

            // get cart products
    foreach ($this->cart->contents() as $products) {
                // check coupon

        if ($products['discount'] != "0") {
            $getCoupon = $this->db->query("SELECT pro.*,dmd_coupon.discount,dmd_coupon.id as coid,dmd_coupon.max_use,dmd_coupon.expire_date FROM `dmd_store_products` as pro left join dmd_coupon on(dmd_coupon.id=pro.enable_coupon) WHERE pro.`product_code` = '" .
                $products['id'] . "' and dmd_coupon.max_use!='0'");
            if ($getCoupon->num_rows() > 0) {
                $getCoupon = $getCoupon->row_array();
                $discount_id = $getCoupon['coid'];
                $max_us = $getCoupon['max_use'] - 1;
                $this->db->where('id', $discount_id);
                $this->db->update("dmd_coupon", array('max_use' => $max_us));
            }
        }

        $order_items = array(
            'product_id' => $products['id'],
            'order_id' => $order_id,
            'product_quantity' => $products['qty'],
            'customer_id' => $userData['id'],
            'product_price' => $products['price'],
            'coupon_discount' => $products['discount'],
            'total_price' => ($products['subtotal']));
        $insert_items = $this->db->insert('dmd_order_items', $order_items);
        $get_item_id = $this->db->insert_id();

        $getattch = $this->db->get_where('dmd_delivery_for_pro', array('product_id' => $products['id']));

        foreach ($getattch->result_array() as $file_attach) {
            $order_files = array(
                'product_id' => $products['id'],
                'order_id' => $order_id,
                'item_id' => $get_item_id,
                'file_path' => $file_attach['upload_file'],
                'file_id' => $file_attach['id']);
            $insert_files = $this->db->insert('dmd_order_files', $order_files);
                    /*$file_attach['upload_file'] = base_url() . 'site_assets/products/' . $products['id'] .
                    '/' . $file_attach['upload_file'];
                    array_push($products_attchment, $file_attach['upload_file']);*/

                }


            }


            if (isset($arrChargeResponse['transId']) && $arrChargeResponse['transId'] != '') {

                foreach ($this->cart->contents() as $products) {
                    $customer_name = $customer['f_name'] . " " . $customer['l_name'];
                    $this->product_email($products['id'], $data['email'],$customer_name);
                }
                $customer = array();
                $customer['order_number'] = $orderData['order_number'];
                $customer['store_name'] = $user_id['store_name'];
                $customer['store_person_name'] = $user_id['contact_person'];
                $customer['support_email'] = $user_id['support_email'];
                $customer['display_name'] = $customer['f_name'] . " " . $customer['l_name'];
                $customer['user_id'] = $user_id['user_id'];
                $customer['email'] = $userdata['email'];


                $this->cart->destroy();

                //update status and trans id to paid when payment successfully made!
                $status = array('transID' => $arrChargeResponse['transId'], 'status' => 'paid');
                $this->db->where('id', $order_id);
                $this->db->update('dmd_orders', $status);

                //Payment Thank You Email
                $EmailData = array('customer' => $customer, 'type' => "thankyou");

                $this->final_email($EmailData);
                $EmailData = array('customer' => $customer, 'type' => "purchase_thankyou");
                $this->final_email($EmailData);


                $this->session->set_flashdata('success_msg',
                    'Thank you for purchasing. Your purchase is now completed.');

                if ($this->session->userdata('customer_details')) {
                    redirect('customer/dashboard');
                } else {
                    redirect('home/index');
                }

            }else{
                $this->session->set_flashdata('success_msg',
                    $arrChargeResponse['error']);

                if ($this->session->userdata('customer_details')) {
                    redirect('customer/dashboard');
                } else {
                    redirect('home/index');
                } 
            }
        }
    }
}


    //function __construct($strAPILoginId, $strTransactionKey, $strValidationMode = 'testMode')





    /*    public function authorize_net()
    {
    if($this->input->post()){
    $postData = $this->input->post();
    $store_id = $postData['store_id'];
    // check fields validation

    print_r($postData);
    echo "<br/>";
    
    $empty_message = "";
    foreach ($postData as $key => $val) {
    if ($val == "") {
    $empty_message .= $key . " is required field<br>";
    }
    }
    if ($empty_message != "") {
    $this->session->set_flashdata('error_msg', $empty_message);
    redirect('user/auth_sample');
    }
    $userData = array(
    "f_name"            =>  $postData['f_name'],
    "shipping_country"  =>  $postData['country'],
    "l_name"            =>  $postData['l_name'],
    "email"             =>  $postData['email']);
    

    //check payment from store or customer
    if(isset($postData['subscription'])){
    //print_r($postData);
    $subcribe = $postData;
    unset($subcribe['subscription']);
    $subcribe['package'] = 'test';
    $subcribe['user_id'] = $this->session->userdata('user_id');
    $subcribe['store_id'] =$this->session->userdata('store_id');
    $subcribe['created_date'] = date('Y-m-d H:i:s');
    //print_r($subcribe);die();
    $insert = $this->db->insert('subscription', $subcribe);
    $insert_id = $this->db->insert_id();
    $userData['id'] = $this->session->userdata('user_id');

    }else{

    
    // check customer exists or not
    $query = $this->db->query("select * from dmd_customers where email='".$postData['email']."'");

    //if exists get customer data from db
    if($query->num_rows()>0){
    $userData = $query->row_array();
    }else{
    // create customer
    // $userData['user_id'] = $this->session->userdata('user_id');
    $userData['store_id'] = $this->session->userdata('store_id');

    print_r($userData);
    
    $insert = $this->db->insert('dmd_customers',$userData);
    if($insert){
    $userData['id'] = $this->db->insert_id();  
    }else{
    $this->session->set_flashdata('error_msg', 'Customer profile creation has been failed!');
    redirect('user/subscription');
    }
    }
    }




    $cardData = array(

    'firstName'         => $userData['f_name'],
    'lastName'          => $userData['l_name'],
    //'number'            => $postData['creditcard'],
    'number'            => '4111111111111111',
    'expiryMonth'       => date('m', strtotime($postData['expiry'])),
    'expiryYear'        => date('Y', strtotime($postData['expiry'])),
    //'startMonth'        => $postData['f_name'],
    //'startYear'         => $postData['f_name'],
    'cvv'               => $postData['cvc'],
    //'issueNumber'       => $postData['f_name'],
    'type'              => 'individual',
    'billingAddress1'   => (isset($postData['address'])) ? $postData['address'] : 'NA',
    //'billingAddress2'   => $postData['f_name'],
    'billingCity'       => (isset($postData['city'])) ? $postData['city'] : 'NA',
    //'billingPostcode'   => $postData['f_name'],
    //'billingState'      => $postData['f_name'],
    'billingCountry'    => $userData['shipping_country'],
    //'billingPhone'      => $postData['billingPhone'],
    'shippingAddress1'  => (isset($postData['address'])) ? $postData['address'] : 'NA',
    //'shippingAddress2'  => $postData['f_name'],
    'shippingCity'      => (isset($postData['city'])) ? $postData['city'] : 'NA',
    //'shippingPostcode'  => $postData['f_name'],
    //'shippingState'     => $postData['f_name'],
    'shippingCountry'   => $userData['shipping_country'],
    //'shippingPhone'     => $postData['shippingPhone'],
    //'company'           => $postData['company'],
    'email'             => $userData['email']);

    $customerProfile = array(
    'card'          => $cardData,
    'name'          => $userData['f_name'] . ' ' . $userData['l_name'],
    'email'         => $userData['email'],
    //'description'   => $postData['description'],
    'customerId'    => $userData['id']);

    // get authorizekeys by store_id
    
    if(!isset($_REQUEST['subscription'])){
    
    $keys = $this->db->query("select configuration from payment_methods where store_id = '".$store_id."' 
    and `default`= '1'and name='authorize'");
    $keys = $keys->row_array();
    $keys  = json_decode($keys['configuration'],'array');
    $appkey = $keys['loginID'];
    $transkey = $keys['transactionKey'];
    
    $this->authorizenet_model->setKeys($appkey,$transkey);
    }

    print_r($customerProfile);
    $result = $this->authorizenet_model->createCustomerProfile($customerProfile);
    $profile = json_decode($result);

    print_r($profile);

    //die();



    if(isset($profile->customerProfileId)){
    $data['authorizenet_profile'] = $profile->customerProfileId;
    if(!isset($_REQUEST['subscription'])){
    $this->db->where('id', $userData['id']);
    $update = $this->db->update('dmd_customers', $data);
    $cst_id = $this->db->insert_id();
    }
    }
    else if(strpos($result,'already exsists')!=='false'){

    $id_err = $result;
    $id_err = explode("ID",$id_err);
    $id_err = explode(" ",ltrim($id_err[1]," "));
    $profile_id = $id_err[0];
    $data['authorizenet_profile'] = $profile_id;
    
    }
    


    if(!isset($_REQUEST['subscription'])){
    $total_ammount = $this->cart->total();
    $postData['amount'] = $total_ammount;
    }
    
    if(isset($profile->customerPaymentProfileId)){
    $purchase = array(
    'amount' => $postData['amount'],
    'currency' => 'USD',
    'card' => $cardData,
    'transactionId' => time(),
    'clientIp' =>'',
    'returnUrl' => 'http//example.com');

    print_r($purchase);
    $payment = $this->authorizenet_model->chargePayment($purchase);
    //print_r($payment);

    $pay = json_decode($payment);

    if(!isset($_REQUEST['subscription'])){
    if(isset($cst_id)){
    $customer_id = $cst_id;
    }else{
    $q="SELECT";
    }
    }

    if(isset($pay->transId)){
    $orderData = array(
    "transID" => $pay->transId,
    "order_number" => time(),
    "customer_id" => $this->session->userdata('user_id'),
    "store_id" => $store_id,
    "shipping_country" => $userData['shipping_country'],
    "total_amount" => $postData['amount'],
    "grand_total" => $postData['amount'],
    "coupon_discount" => '20',
    "status" => 'paid',
    "created_date" => date('Y-m-d H:i:s'),
    "deliver_status" => 'pending');
    
    if(!isset($_REQUEST['subscription'])){
    $insert = $this->db->insert('dmd_orders', $orderData);    
    }elseif(isset($_REQUEST['subscription'])){
    $data = array('trans_id'=>$pay->transId,'amount'=>$postData['amount'],'status'=>'completed');   
    $this->db->where('id',$insert_id);// get from session $this->session->userdata('store_id')
    $this->db->update('subscription',$data);
    } 

    
    }else{
    $data = array('amount'=>$postData['amount'],'status'=>'pending');   
    $this->db->where('id',$insert_id);
    $this->db->update('subscription',$data);
    }

    $this->session->set_flashdata('error_msg', 'Your payment has been successfully made!');
    if(isset($_REQUEST['subscription'])){
    redirect('user/subscription');
    }else{
    //redirect('user/auth_sample');//need to be updated.
    }

    }else{
    print_r('payment profile not created');
    }
    }
}*/

public function subscribe()
{
    if ($this->input->post()) {
        $data = $this->input->post();
        $data['cardexpiry'] = date('Y-m-d', strtotime($data['cardexpiry']));
        $data['user_id'] = $this->session->userdata('user_id');

        $result = $this->user_model->add_subscription($data);
        if ($result) {
            $this->session->set_flashdata('subs_msg', 'Your information has been updated!');
        } else {
            $this->session->set_flashdata('subs_msg',
                'Your information has been failed to update!');
        }
        redirect('user/subscription');
    }
}
public function edit()
{

    $user = array(
        'f_name' => $_REQUEST['firstName'],
        'l_name' => $_REQUEST['lastName'],
        'display_name' => $_REQUEST['firstName'] . " " . $_REQUEST['lastName'],
        'email' => $_REQUEST['email'],
    );

    if (isset($_REQUEST['currentPassword'])) {
        $user['password'] = md5($_REQUEST['password']);
        $currentPassword = md5($_REQUEST['currentPassword']);

        $result = $this->user_model->password_check($this->session->userdata('user_id'),
            $currentPassword);
        if (!$result) {
            $this->session->set_flashdata('error_msg',
                'Current Password is not correct! Try again.');
            redirect('user/profile');
        }
    }


        // check fields validation
    $empty_message = "";
    foreach ($user as $key => $val) {
        if ($val == "") {
            $empty_message .= $key . " is required field<br>";
        }
    }
    if ($empty_message != "") {
        $this->session->set_flashdata('error_msg', $empty_message);
        redirect('user/profile');
    }


    $this->db->where('id', $this->session->userdata('user_id'));
    $update = $this->db->update("dmd_users", $user);
    if ($update) {
        $this->session->set_userdata('user_email', $user['email']);
        $this->session->set_userdata('user_name', $user['display_name']);
        $this->session->set_flashdata('success_msg',
            'Your information has been updated!');
        redirect('user/profile');
    } else {
        $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
        redirect('user/profile');
    }


}
public function profile()
{
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/profile');
    $this->load->view('front_pages/dashboard/dash_footer');
}
public function register_user($selected_package = '')
{
 $data['selected_package'] = $selected_package;
 $this->form_validation->set_rules('firstName', 'First name', 'trim|required',
    array('required' => 'First name is required'));
 $this->form_validation->set_rules('lastName', 'Last name', 'trim|required',
    array('required' => 'Last name is required'));
 $this->form_validation->set_rules('email', 'Email',
    'trim|required|valid_email|is_unique[dmd_users.email]', array(
        'required' => 'Email is required',
        'valid_email' => 'Enter in valid format',
        'is_unique' => 'Email already exist!'));
 $this->form_validation->set_rules('password', 'Password',
    'trim|required|min_length[6]|max_length[50]');
 $this->form_validation->set_rules('confPassword', 'Confirm Password',
    'trim|required|min_length[6]|max_length[50]');
 if ($this->form_validation->run()) {


    $user = array(
        'f_name' => $_REQUEST['firstName'],
        'l_name' => $_REQUEST['lastName'],
        'display_name' => $_REQUEST['firstName'] . " " . $_REQUEST['lastName'],
        'email' => $_REQUEST['email'],
        'password' => md5($_REQUEST['password']),
    );
            // check fields validation
            /*$empty_message = "";
            foreach ($_REQUEST as $key => $val) {
            if ($val == "") {
            $empty_message .= $key . " is required field. <br><br>";
            }
            }
            if ($empty_message != "") {
            $this->session->set_flashdata('error_msg', $empty_message);
            redirect('user/register');
            }
            */
            $email_check = $this->user_model->email_check($user['email']);

            /*  if ($email_check) {*/
                $email_verification_code = uniqid();
                $user['email_verification_code'] = $email_verification_code;
                $this->user_model->register_user($user, decode($data['selected_package']));
                $this->sendThankEmail($user['email']);

            } else {
                $this->load->view('front_pages/front_header');
                $this->load->view('front_pages/register',$data);
                $this->load->view('front_pages/front_footer');
            }

        /* } else {


        $this->session->set_flashdata('error_msg', 'You are already registered by this email!');
        redirect('user/register');



    }   */

}
public function deleteNotactive()
{
    $query = $this->db->query("delete from dmd_users where DATE_FORMAT(user_registered, '%Y-%m-%d') = DATE_SUB(CURDATE(), INTERVAL 7 DAY) and verify=0");
}
public function resendThankEmail()
{
    $email = $_REQUEST['email'];
    if ($email != '') {
        $this->sendThankEmail($email, "resend");

    }
}

public function sendThankEmail($email, $resend = "")
{
    $email_check = $this->user_model->email_check($email, "getuser");
    if (!isset($email_check['email'])) {
        $this->session->set_flashdata('error_msg',
            'This email not exists in our system.');
        redirect('user/register');
    } else {

        $subject = 'Confirm Your Email - Digital Media Deliveries';
        $email_message = '<style>
        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
        </style>';
        $email_message .= 'Hello <Name>,
        You created an account on Digital Media Delivery. Please click the button below to activate your account<br>
        <buttontext><br>
        Or, copy and paste this link into your browser:<br>
        <link_activation>
        Your username is: <email><br>
        Please activate your account within 7 days of this email.<br>
        Thanks,<br>
        Digital Media Delivery Team<br>
        www.digitalmediadelivery.com<br>
        This is an automated message, please do not reply.';
        $email_message = $this->load->view('front_pages/emtemp/register', '', true);
        $button_activate = '<a href="' . base_url() . '/user/emailVerify/' . $email_check['email_verification_code'] .
        '" class="button">Activate Your Account</a>';
        $link_activate = base_url() . '/user/emailVerify/' . $email_check['email_verification_code'];
        $email_message = str_replace("{name}", $email_check['display_name'], $email_message);
            //$email_message = str_replace("<buttontext>", $button_activate, $email_message);
        $email_message = str_replace("{link_activation}", $link_activate, $email_message);
        $email_message = str_replace("{email}", $email_check['email'], $email_message);


        $to = $email_check['email'];

            // send email to user

        $this->email->to($to);


        $this->email->subject($subject);
        $this->email->message($email_message);

        $this->email->send();

            //mail($to, $subject, $email_message, $headers);


        $this->session->set_flashdata('email', $email_check['email']);
        if ($resend != "") {
            $this->session->set_flashdata('success_msg',
                'Email has been resent, Please verify your Email
                now to start using platfrom.');
        } else {
            $this->session->set_flashdata('success_msg',
                'Congratulations on taking the step towards launching your products Digitaly, Please verify your Email
                now to start using platform.');
        }

        redirect('user/thankYou');
    }
}
public function resetPass($vcode = "")
{

    if (isset($_REQUEST['password'])) {
            // check fields validation
        $empty_message = "";

        if ($_REQUEST['password'] == "") {
            $empty_message .= "password is required field<br>";
        }
        if ($_REQUEST['email'] == "") {
            $empty_message .= "email is required field<br>";
        }

        if ($empty_message != "") {
            $this->session->set_flashdata('error_msg', $empty_message);
            redirect('user/resetpass/' . $vcode);
        }
        $passwordData['password'] = $_REQUEST['password'];
        $passwordData['email'] = $_REQUEST['email'];
        $passwordData['vcode'] = $_REQUEST['vcode'];

        $reset_pass = $this->user_model->resetPass($passwordData);

    }

    $email_check = $this->user_model->Verifycode($vcode);
        //print_r($email_check);
    if (isset($email_check['email'])) {
        $this->load->view('front_pages/front_header');
        $data['email'] = $email_check['email'];
        $data['vcode'] = $vcode;
        $this->load->view('front_pages/resetpass', $data);
        $this->load->view('front_pages/front_footer');

    } else {

        $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
        redirect('user/register');
    }
}
public function emailVerify($vcode)
{
    $email_check = $this->user_model->emailVerify($vcode);
    if ($email_check) {
        $this->session->set_flashdata('success_msg',
            'Your account has been successfully activated,
            Please proceed to the login screen to login now');
        $this->session->set_flashdata('email_verify', 'email_verify');
        redirect('user/thankYou');

    } else {

        $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
        redirect('user/register');


    }
}
public function thankYou()
{
    $this->load->view('front_pages/front_header');
    $this->load->view('front_pages/thankyou');
    $this->load->view('front_pages/front_footer');
}
public function ForgotPass()
{
    $this->load->view('front_pages/front_header');
    $this->load->view('front_pages/forgot');
    $this->load->view('front_pages/front_footer');
}

public function ForgotPassword()
{
    $email = $_REQUEST['email'];
    $data['email'] = $email;
    $findemail = $this->user_model->ForgotPassword($email);
    if ($findemail) {
        $this->user_model->sendpassword($findemail);
        $this->session->set_flashdata('success_msg', 'Email has been sent to ' . $email .
            ' to reset password');
        $this->load->view('front_pages/front_header');
        $this->load->view('front_pages/resetview',$data);
        $this->load->view('front_pages/front_footer');
            //redirect('user/login');
    } else {
        $this->session->set_flashdata('error_msg', $email .
            ' you are not able to reset password not active user or not found your email in this application');
        redirect('user/ForgotPass');
    }
}

public function login_user()
{
    $user_login = array('email' => $_REQUEST['email'], 'password' => md5($_REQUEST['password']));

    $data = $this->user_model->login_user($user_login['email'], $user_login['password']);
    $sub_admin = $this->Subadmin_model->get_subadmin($user_login['email'], $user_login['password']);
    
    if($sub_admin){
      if ($sub_admin['status'] == '0') {
        $this->session->set_flashdata('error_msg', 'Your  account has been blocked');
        redirect('user/login');
    }

    $user = $this->user_model->get_user($sub_admin['user_id']);
    $data = $this->user_model->login_user($user['email'], $user['password']);
}

if ($data) {
    if ($data['verify'] != '1') {
        if($sub_admin){
            $this->session->set_flashdata('error_msg',
                'Before you can login, you must active your subscriber account with the code sent to subscriber email ' .
                $data['email'] . ' If you did not receive this email, please check your junk/spam, Try again.');

        }else{
            $this->session->set_flashdata('error_msg',
                'Before you can login, you must active your account with the code sent to your email ' .
                $data['email'] . ' If you did not receive this email, please check your junk/spam, Try again.');

        }
        redirect('user/login');
    }
    if ($data['verify'] == '2') {
      if($sub_admin){
        $this->session->set_flashdata('error_msg', 'Your subscriber account has been expired so before you can login, you have must pay for subscription');
    }else{
        $this->session->set_flashdata('error_msg', 'Your account has been expired so before you can login, you have must pay for subscription');
    }
    redirect('user/login');
}
if ($data['user_status'] == 0) {
   if($sub_admin){
    $this->session->set_flashdata('error_msg', 'Your subscriber account has been blocked.');
}else{
    $this->session->set_flashdata('error_msg', 'Your account has been blocked.');
}

redirect('user/login');
}
if ($data['user_status'] == 2) {

   if($sub_admin){
     $this->session->set_flashdata('error_msg', 'Your subscriber account has been deleted.');
 }else{
  $this->session->set_flashdata('error_msg', 'Your account has been deleted.');
}



redirect('user/login');
}
if ($data['user_status'] == 1) {
    $this->session->set_userdata('user_id', $data['userid']);
    $this->session->set_userdata('user_email', $data['email']);
    $this->session->set_userdata('user_role', $data['user_role']);

    if($sub_admin){
        $this->session->set_userdata('user_name', $sub_admin['display_name']);
        $this->session->set_userdata('user_type', 'subadmin');
    }else{
        $this->session->set_userdata('user_name', $data['display_name']);
        $this->session->set_userdata('user_type', 'subscriber');
    }
    if ($data['user_role'] != '2') {
        $sub_admins = $this->Subadmin_model->get_all($data['userid']);
        $package = $this->Subadmin_model->get_package($data['userid']);
        $this->session->set_userdata('count_sub_admin', count($sub_admins));
        $this->session->set_userdata('pack_sub_admin', $package['sub_admins']);
    }
            //for super admin only
    if ($data['user_role'] == '2') {
        redirect('admin/dashboard');
    }

            // for every user except super admin
    $store_name = "";
    $store_id = "";
    $store_url = "";

    if($data['store_status'] == 1){
        $store_name = $data['store_name'];
        $store_id = $data['store_id'];
        $store_url = $data['store_url'];
    }else{
        $store = $this->db->get_where('dmd_stores',['user_id' => $data['userid'], 'status' => 1], 1)->row_array();
                //print_r($store);die;
        if($store){
            $store_name = $store['store_name'];
            $store_id = $store['id'];
            $store_url = $store['store_url'];
        }
    }

    $setting = $this->db->query('select * from settings');
    $setting = $setting->row_array();
    
    $this->session->set_userdata('store_name', $store_name);
    $this->session->set_userdata('store_id', $store_id);
    $this->session->set_userdata('store_url', $store_url);
    $this->session->set_userdata('tutorialfile', $setting['tutorialfile']);
    redirect('user/dashboard');

}
} else {
    $this->session->set_flashdata('error_msg',
        'You have entered an invalid email or password, Try again.');
    redirect('user/login');

}


}
/*public function check_memory($getsize="")
{
    $user_id = $this->session->userdata('user_id');

    //get package detail of current user
    $current_package = $this->db->get_where('store_package', array('user_id' => $user_id));
    $current_package = $current_package->row_array();
    $start_date = $current_package['start_date'];
    $end_date = $current_package['end_date'];
    //print_r($current_package);
        
    // get size of uploaded files for current user
    $size_query = "SELECT SUM(f.`memory_size`) AS file_memory 
        FROM (SELECT `memory_size` FROM `dmd_delivery_for_pro` 
            WHERE `user_id` = '" . $user_id . "'
            AND `media_date` BETWEEN '". $start_date ."' AND '". $end_date ."'
        UNION ALL
        SELECT `memory_size` FROM `dmd_store_products` 
            WHERE `user_id` = '" . $user_id . "'
            AND `media_date` BETWEEN '". $start_date ."' AND '". $end_date ."' 
        UNION ALL
        SELECT `memory_size` FROM `checkout_customization` 
            WHERE `user_id` = '" . $user_id . "'
            AND `media_date` BETWEEN '". $start_date ."' AND '". $end_date ."') f";
    
    $size = $this->db->query($size_query);
    $sizep = $size->row_array();
    $used_memory = $sizep['file_memory']; //KB
        
    // get allowed size of user
    $package_limit = $this->db->get_where('packages', array('pack_code' => $current_package['package_id']));
    $package_limit = $package_limit->row_array();
    $package_memory_limit_in_mb = $package_limit['memory_limit'];
    $package_memory_limit = $package_memory_limit_in_mb * 1024; //KB


    $package_memory_limit_in_mb = $current_package['memory_limit'];
    $package_memory_limit = (int)$package_memory_limit_in_mb * 1024; //KB
    
    if ($getsize == '') {
        //return storage memory is available or not
        ($used_memory < $package_memory_limit) ? TRUE : FALSE;
    }else if ($getsize == 'size') {
        //return available remaining storage size in MBs
        $remaining_storage_in_kb = ($package_memory_limit - $used_memory);
        $remaining_storage_in_mb = ($remaining_storage_in_kb / 1024);
        $remaining_storage_in_mb = round($remaining_storage_in_mb, 2);
        return $remaining_storage_in_mb;
    }else if($getsize == 'get'){
        //return storage used overview with available memory limit
        //$used_memory_in_mb = ($used_memory / 1024);
        //$used_memory_in_mb = round($used_memory_in_mb, 2);
        $used_memory_in_mb = $this->format_memory($used_memory);
        $package_memory_limit_in_mb = $this->format_memory($package_memory_limit);
        return $used_memory_in_mb . " / " . $package_memory_limit_in_mb;
    }else if($getsize == 'limit'){
        return $package_memory_limit_in_mb;
    }else if($getsize == 'expiry'){
        return $end_date;
    }
}*/

/*public function format_memory($size)
{
    if ($size < 1024) {
        $size = number_format($size, 2);
        $size .= ' KB';
    } else {
        if ($size / 1024 < 1024) {
            $size = number_format($size / 1024, 2);
            $size .= ' MB';
        } else if ($size / 1024 / 1024 < 1024) {
            $size = number_format($size / 1024 / 1024, 2);
            $size .= ' GB';
        }
    }
    return $size;
}*/

function dashboard()
{
    //print_r($_SESSION);

    $store_id = $this->session->userdata('store_id');
    $user_id = $this->session->userdata('user_id');
    
    // get total products
    $getq = $this->db->query("select count(id) as totpro from dmd_store_products where user_id='" . $user_id . "'");
    $getq = $getq->row_array();
    $overview['products'] = $getq['totpro'];

    // get default package detail
    /*$default_pack = $this->db->get_where('store_package', array('user_id' => $user_id));
    $default_pack = $default_pack->row_array();
    $depackage = $this->db->get_where('packages', array('pack_code' => $default_pack['package_id']));
    $depackage = $depackage->row_array();
    $overview['pack_product'] = $depackage['pack_products'];*/

    $current_package = $this->db->get_where('store_package', array('user_id' => $user_id));
    $current_package = $current_package->row_array();
    $overview['pack_product'] = $current_package['product_limit'];
    $overview['sub_admins'] = $current_package['sub_admins'];

    // get today sales
    $gettoday = $this->db->query("SELECT count(id) as tottoday FROM `dmd_orders` WHERE DATE(`created_date`) = CURDATE() and user_id='" . $user_id . "'");
    $gettoday = $gettoday->row_array();
    $overview['gettoday'] = $gettoday['tottoday'];

    // get current month sales
    $getmonth = $this->db->query("SELECT count(id) as totmonth FROM dmd_orders WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND YEAR(created_date) = YEAR(CURRENT_DATE()) and user_id='" . $user_id . "'");
    $getmonth = $getmonth->row_array();
    $overview['getmonth'] = $getmonth['totmonth'];

    // get lifetime sales
    $getotal = $this->db->query("SELECT count(id) as total_sales FROM dmd_orders WHERE  user_id='" . $user_id . "'");
    $getotal = $getotal->row_array();
    $overview['getotal'] = $getotal['total_sales'];

    // over all earning
    $earning = $this->db->query("SELECT sum(total_amount) as total_earn FROM dmd_orders WHERE  user_id='" . $user_id . "'");
    $earning = $earning->row_array();
    $overview['earning'] = $earning['total_earn'];
    $overview['check_memory'] = $this->user_model->check_memory("get");


    $package= $this->user_model->get_prev_package($user_id);

    if($package['affiliate'] == 1){
        $savedata['affiliate'] = "2";
        $this->db->where('user_id', $user_id);
        $this->db->update('store_package', $savedata);
        $overview['affiliate'] = 1; // free package , Affiliation required

        $overview['last_purchase']= $this->user_model->get_last_purchase($user_id);
// free package , no affiliation require 
        
    } else {
     $overview['affiliate']  =0; 
 }


 $this->load->view('front_pages/dashboard/dash_header');
 $this->load->view('front_pages/dashboard/dashboard', $overview);
 $this->load->view('front_pages/dashboard/dash_footer');

    //$overview['last_purchase']= ''; //$this->user_model->get_last_purchase($user_id);


}
public function logout()
{
    $this->session->sess_destroy();
    redirect('user/login', 'refresh');
}


function getAmount2($package_id)
{
        // check user days

    $default_pack = $this->db->get_where('store_package', array('user_id' => $this->
        session->userdata('user_id')));
    $default_pack = $default_pack->row_array();
        //echo $default_pack['start_date'];
    $date1 = date('Y-m-d', strtotime($default_pack['start_date']));
    $date2 = date('y-m-d');
    $date2 = date('Y-m-d', strtotime($date2));

    $date1 = date_create($date1);
    $date2 = date_create($date2);
    $diff = date_diff($date1, $date2);

    $used_day = $diff->format("%R%a");
    $used_day = str_replace("+", "", $used_day);
        //echo "<br>";
    $remain_day = 30 - $used_day;
        //echo "<br>";
    $depackage = $this->db->get_where('packages', array('pack_code' => $default_pack['package_id']));
    $depackage = $depackage->row_array();

        // get package detail
    $package = $this->db->get_where('packages', array('pack_code' => $package_id));
    $package = $package->row_array();


    $amount_dif = $depackage['pack_price'] / 30;
    $amount_dif = $amount_dif * $remain_day;
        //echo "<br>";
    return $amount = $package['pack_price'] - $amount_dif;


}
function getAmount($package_id)
{
        // get package detail
    $package = $this->db->get_where('packages', array('pack_code' => $package_id));
    $package = $package->row_array();
        // get uploaded products by user
    $products = $this->db->query('select count(*) as totpro from dmd_store_products where user_id="' .
        $this->session->userdata('user_id') . '"');
    $totpro = $products->row_array();
    $totpro = $totpro['totpro'];
        // get default package id
    $default_pack = $this->db->get_where('store_package', array('user_id' => $this->
        session->userdata('user_id')));
    $default_pack = $default_pack->row_array();

        // echo "<pre>";
    $depackage = $this->db->get_where('packages', array('pack_code' => $default_pack['package_id']));
    $depackage = $depackage->row_array();
        //print_r($depackage);

        // make formula for get ammount

    $amount_dif = $depackage['pack_price'] / $depackage['pack_products'];

    $amount_dif = $amount_dif * $totpro;

    echo $amount = $package['pack_price'] - $amount_dif;


}

function change_subscription_status($profile_id, $action)
{

    $api_request = 'USER=' . urlencode('api_username') . '&PWD=' . urlencode('api_password') .
    '&SIGNATURE=' . urlencode('api_signature') . '&VERSION=76.0' .
    '&METHOD=ManageRecurringPaymentsProfileStatus' . '&PROFILEID=' . urlencode($profile_id) .
    '&ACTION=' . urlencode($action) . '&NOTE=' . urlencode('Profile cancelled at store');

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp'); 
        // For live transactions, change to 'https://api-3t.paypal.com/nvp'
        // curl_setopt($ch, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
    curl_setopt($ch, CURLOPT_VERBOSE, 1);

        // Uncomment these to turn off server and peer verification
        // curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
        // curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);

        // Set the API parameters for this transaction
    curl_setopt($ch, CURLOPT_POSTFIELDS, $api_request);

        // Request response from PayPal
    $response = curl_exec($ch);

        // If no response was received from PayPal there is no point parsing the response
    if (!$response)
        die('Calling PayPal to change_subscription_status failed: ' . curl_error($ch) .
            '(' . curl_errno($ch) . ')');

    curl_close($ch);

        // An associative array is more usable than a parameter string
    parse_str($response, $parsed_response);

    print_r($parsed_response);die;
}


public function paypal_subscription()
{
    if ($this->input->post()) {
        $postData = $this->input->post();
            // print_r($postData);die();
        $user_id = $this->session->userdata('user_id');
        $store_id = $this->session->userdata('store_id');
        $pkg_detail = $this->db->get_where('packages', ['pack_code' => $postData['package']])->row_array();
            //save current user package to session and update new one
        $package = $this->db->get_where('store_package', ['user_id' => $user_id])->row_array();
        $this->session->set_userdata('paypal_pkg_subs', $package['package_id']);

              $prev_package = $this->user_model->get_prev_package($user_id);//get prev package
            // update package
              $new_package = $this->input->post('package');
              $handle_package = $this->handle_package($new_package);
              $store_package = [];
              $store_package['memory_limit'] = $handle_package['memory_limit'];
              $store_package['product_limit'] = $handle_package['product_limit'];
              $store_package['package_id'] = $new_package;
              $store_package['user_id'] = $user_id;
              $store_package['store_id'] = $store_id;
              $store_package['default'] = "1";
              $store_package['payment_status'] = 1;
              $store_package['sub_admins'] = $handle_package['sub_admins'];

              $days_next = $this->count_days($handle_package);
              $old_date = date('d-m-Y');
              $next_due_date = date('Y-m-d', strtotime($old_date . ' +'.$days_next.' days'));
              $store_package['end_date'] = $next_due_date;


              //affiliate
              if ($prev_package['affiliate'] == 0){
                 $store_package['affiliate'] = "1";
             }
             //affiliate


             $this->db->where('user_id', $user_id);
             $this->db->update('store_package', $store_package);

            //$store = $this->db->get_where('dmd_stores', ['id' => $postData["store_id"]])->row_array();
             $admin = $this->user_model->get_admin();
             $keys = $this->user_model->get_admin_keys($admin['id'], $name = 'paypal');
             $conf = json_decode(@$keys['configuration']);
           //$conf->paypalEmail
             $parms = array(
                "sandbox" => false,
                "business" => $conf->paypalEmail,
                "paypal_lib_ipn_log" => true,
                "paypal_lib_ipn_log_file" => "site_assets/logs/paypal_ipn.log",
                "paypal_lib_button_path" => "site_assets/images/",
                "paypal_lib_currency_code" => "USD");

             $this->load->library('paypal_lib', $parms);
             $returnURL = base_url('user/paypal_success_subscription');
             $cancelURL = base_url('user/paypal_cancel_subscription');
             $notifyURL = base_url('user/paypal_ipn_subscription');

             $this->paypal_lib->add_field('return', $returnURL);
             $this->paypal_lib->add_field('cancel_return', $cancelURL);
             $this->paypal_lib->add_field('notify_url', $notifyURL);

             $this->paypal_lib->add_field('txn_type', "recurring_payment");
             $this->paypal_lib->add_field('item_number', $postData['package']);
             $this->paypal_lib->add_field('item_name', $postData['pack_title']);
             $this->paypal_lib->add_field('cmd', "_xclick-subscriptions");
             $this->paypal_lib->add_field('a3', $postData['amount']);

           // $postData["amount"] = $this->getAmount2($postData['package']);
             $this->paypal_lib->add_field('a1', $postData["amount"]);


             $this->paypal_lib->add_field('p1', 1);
             if($pkg_detail['pack_time_period'] == 'Month'){
                $this->paypal_lib->add_field('t1', 'M');
            }else{
                $this->paypal_lib->add_field('t1', 'Y');
            }


            $this->paypal_lib->add_field('p3', 1);

            if($pkg_detail['pack_time_period'] == 'Month'){
               $this->paypal_lib->add_field('t3', 'M');
           }else{
               $this->paypal_lib->add_field('t3', 'Y');
           }

           // $this->paypal_lib->add_field('t3', 'M');
           $this->paypal_lib->add_field('src', 1);
           $this->paypal_lib->add_field('sra', 1);

           $this->paypal_lib->add_field('custom', $user_id . "|" . $store_id."|". $new_package);

           $this->paypal_lib->paypal_auto_form();
       }
   }

   public function paypal_ipn_subscription()
   {
        //$data = implode("|",$_REQUEST);
    $dataP = json_encode($_REQUEST, JSON_UNESCAPED_SLASHES);
    $this->db->insert('customer_card',['customer_id' => $dataP]);

        //$data = file_get_contents('php://input');
    $data = $_REQUEST;

    if(isset($data['txn_id'])){
            //$this->db->insert('customer_card',['customer_id' => 'ballay ballay']);

        $existed = $this->db->get_where('billing_history', ['trans_id' => $data['txn_id']])->result_array();
        if(count($existed) == 0){
            if ($data["payment_status"] == "Completed"){
                $custom = explode('|', $data["custom"]);
                $user_id = $custom[0];
                $store_id = $custom[1];
                    // insert billing history
                $this->db->insert('billing_history', array(
                    'user_id' => $user_id,
                    "store_id" => $store_id,
                    "trans_id" => $data['txn_id'],
                    "payment_response" => json_encode($data, JSON_UNESCAPED_SLASHES),
                    'amount' => $data['payment_gross']));

            }

            if ($data["payment_status"] != "Completed") {
                $this->db->where('user_id', $user_id);
                $this->db->update('store_package', array('payment_status' => 0));
            }
        }

        //Google analytics 
        $this->session->set_userdata('purchase_code', $custom[2]);
        $this->session->set_userdata('purchase_transaction', $data['txn_id']);          
        //Google analytics

    }
}

public function paypal_success_subscription()
{


    $this->session->set_flashdata('error_msg', 'Your package has been updated succesfully Thanks');
    redirect(base_url('user/subscription'));    
}

public function buypay()
{
        // get user id from store_id
    if ($this->input->post()) {
        $data = $this->input->post();

            //if resubmit stripe token

        $user_id = $this->db->get_where('dmd_stores', array('id' => $data["store_id"]));
        $user_id = $user_id->row_array();
            // get user bussines email
        $keys = $this->db->query("select * from payment_methods where user_id = '" . $user_id['user_id'] .
            "'and `name`='paypal'");
        if ($keys->num_rows() > 0) {
            $keys = $keys->row_array();
            $stkey = json_decode($keys['configuration'], 'array');
            $buss_email = $stkey['paypalEmail'];


                $chk_flag = "customer"; //need

                /*$empty_message = "";
                foreach ($data as $key => $val) {
                    if ($val == "") {
                        if ($key == "f_name") {
                            $empty_message .= "first name is required.<br><br>";
                        } elseif ($key == "l_name") {
                            $empty_message .= "last name is required.<br><br>";
                        } else {
                            $empty_message .= $key . " is required.<br><br>";
                        }
                    }
                }
                if ($empty_message != "") {
                    $this->session->set_flashdata('error_msg', $empty_message);
                    redirect('cart/checkout');

                }*/

                if ($chk_flag == 'customer') {
                    // check customer exists or not
                    $query = $this->db->query("select * from dmd_customers where email='" . $data['email'] .
                        "'");

                    //if exists get customer data from db
                    $new_cus = false;
                    if ($query->num_rows() > 0) {
                        $customer = $query->row_array();
                        $insert_id = $customer['id'];
                    } else {
                        $new_cus = true;
                        // create customer

                        $user_pass = $this->generateProCode();
                        $customer = array(
                            "f_name" => $data['f_name'],
                            "l_name" => $data['l_name'],
                            "email" => $data['email'],
                            "shipping_country" => $data['country'],
                            "store_id" => $data['store_id'],
                            "password" => md5($user_pass)
                        );

                        $insert = $this->db->insert('dmd_customers', $customer);
                        if ($insert) {
                            $customer['id'] = $this->db->insert_id();

                            // new customer username password email
                            $insert_id = $customer['id'];
                            $customer_email = $customer;
                            $customer['password'] = $user_pass;
                            $this->new_cst_email($customer);


                        } else {
                            $this->session->set_flashdata('error_msg',
                                'Customer profile creation has been failed!');
                            redirect('cart/checkout');
                        }
                    }
                }


                $orderData = array(
                    //"transID" => ,
                    "order_number" => time(),
                    "customer_id" => $customer['id'],
                    "store_id" => $data["store_id"],
                    'user_id' => $user_id['user_id'],
                    "shipping_country" => $data['country'],

                    "total_amount" => $this->session->userdata('sub_total'),
                    "tax" => $this->session->userdata('tax_amount'),
                    "grand_total" => $this->session->userdata('cart_total'),
                    //"coupon_discount" => '10',
                    "status" => 'pending',
                    "created_date" => date('Y-m-d H:i:s'),
                    "deliver_status" => 'pending',
                    "Payment_method" => "paypal");

                //print_r($orderData);
                $insert_order = $this->db->insert('dmd_orders', $orderData);
                $order_id = $this->db->insert_id();


                $order_items = array();
                $products_attchment = array();

                $parms = array(
                    "sandbox" => "true",
                    "business" => $buss_email,
                    "paypal_lib_ipn_log" => true,
                    "paypal_lib_ipn_log_file" => "site_assets/logs/paypal_ipn.log",
                    "paypal_lib_button_path" => "site_assets/images/",
                    "paypal_lib_currency_code" => "USD");

                $this->load->library('paypal_lib', $parms);
                // Set variables for paypal form
                $returnURL = base_url('user/paypal_success');
                $cancelURL = base_url('user/paypal_cancel');
                $notifyURL = base_url('user/paypal_ipn');

                $this->paypal_lib->add_field('return', $returnURL);
                $this->paypal_lib->add_field('cancel_return', $cancelURL);
                $this->paypal_lib->add_field('notify_url', $notifyURL);

                $this->paypal_lib->add_field('cmd', "_xclick");
                $this->paypal_lib->add_field('item_number', $orderData['order_number']);
                $this->paypal_lib->add_field('amount', $this->session->userdata('cart_total'));

                /*$quantity = 0;
                $i=0;
                foreach ($this->cart->contents() as $products) {
                    $i++;
                    $this->paypal_lib->add_field('quantity' . $i, $products['qty']);
                    $this->paypal_lib->add_field('amount' . $i, $this->cart->total());
                    //$quantity = $quantity + $products['qty'];
                }*/


                $proIds = "";
                $proDis = "";
                foreach ($this->cart->contents() as $products) {
                    if ($products['discount'] != "0") {
                        $proIds .= $products['id'] . ",";
                    }
                    $proDis .= $products['discount'] . ",";

                    $order_items = array(
                        'product_id' => $products['id'],
                        'order_id' => $order_id,
                        'product_quantity' => $products['qty'],
                        'customer_id' => $customer['id'],
                        'product_price' => $products['price'],
                        'coupon_discount' => $products['discount'],
                        'total_price' => ($products['subtotal']));
                    $insert_items = $this->db->insert('dmd_order_items', $order_items);
                    $get_item_id = $this->db->insert_id();

                    $getattch = $this->db->get_where('dmd_delivery_for_pro', array('product_id' => $products['id']));

                    foreach ($getattch->result_array() as $file_attach) {
                        $order_files = array(
                            'product_id' => $products['id'],
                            'order_id' => $order_id,
                            'item_id' => $get_item_id,
                            'file_path' => $file_attach['upload_file'],
                            'file_id' => $file_attach['id']);
                        $insert_files = $this->db->insert('dmd_order_files', $order_files);
                    }


                }
                $customer1 = array();
                $customer1['store_name'] = $user_id['store_name'];
                $customer1['store_person_name'] = $user_id['contact_person'];
                $customer1['support_email'] = $user_id['support_email'];
                $customer1['display_name'] = $customer['f_name'] . " " . $customer['l_name'];
                $customer1['email'] = $customer['email'];


                $cart_products = array('id' => rtrim($proIds, ","));
                $cart_products = json_encode($cart_products);
                $send_customer = json_encode($customer1);

                $data["amount"] = (float)$this->session->userdata('cart_total'); //$this->cart->total();
                $data["currency"] = "usd";
                $data["customer_id"] = $customer['id'];
                $data["order_id"] = $order_id;
                $data["transId"] = "";

                $customer['order_number'] = $orderData['order_number'];
                $customer['store_name'] = $user_id['store_name'];
                $customer['store_person_name'] = $user_id['contact_person'];
                $customer['support_email'] = $user_id['support_email'];
                $customer['display_name'] = $customer['f_name'] . " " . $customer['l_name'];
                $customer['user_id'] = $user_id['user_id'];
                $subscription = 'customer';

                $userID = $user_id['user_id'];
                
                $this->paypal_lib->add_field('custom', $userID . "|" . $subscription . "|" . $insert_id .
                    "|" . $data['store_id'] . "|" . $cart_products . '|' . $send_customer);


                //echo $this->paypal_lib->paypal_form();
                echo $this->paypal_lib->paypal_auto_form();

            }
        }
    }

    public function paypal_success()
    {
        $this->cart->destroy();
        $this->session->set_flashdata('success_msg',
            'Thank you for purchasing. Your purchase is now completed. Please check your email for invoice detail.');
        if ($this->session->userdata('customer_details')) {
            redirect('customer/dashboard');
        } else {
            redirect('home/index');
        }
        //redirect(base_url());
    }

    public function paypal_cancel()
    {
        redirect(base_url());
    }

    public function paypal_cancel_subscription()
    {
        // revert back package
        $this->session->set_userdata('purchase_code', '');
        $this->session->set_userdata('purchase_transaction', '');  

        $user_id = $this->session->userdata('user_id');
        $store_id = $this->session->userdata('store_id');

        $package = $this->db->get_where('store_package', ['user_id' => $user_id])->row_array();
        $limit = $package['memory_limit'];
        $products = $package['product_limit'];
        // get package from package table
        $pkg_detail = $this->db->get_where('packages', ['pack_code' => $package['package_id']])->row_array();
        if($pkg_detail){
            $limit = $pkg_detail['memory_limit'];
            $products = $pkg_detail['pack_products'];
        }
        $old_package = $this->session->userdata('paypal_pkg_subs');
        //$handle_package = $this->handle_package($package['package_id']);

        $used_memory = $this->user_model->check_memory('used');
        
        $getq = $this->db->query("select count(id) as totpro from dmd_store_products where user_id='" . $user_id . "'");
        $getq = $getq->row_array();
        $used_products = $getq['totpro'];

        $store_package = [];
        $store_package['memory_limit'] = (int)$used_memory - (int)$limit;
        $store_package['product_limit'] = (int)$used_products - (int)$products;
        $store_package['package_id'] = $old_package;
        /*$store_package['user_id'] = $user_id;
        $store_package['store_id'] = $store_id;
        $store_package['default'] = "1";
        $store_package['payment_status'] = 1;*/
        //$old_date = date('d-m-Y');
        //$next_due_date = date('Y-m-d', strtotime($old_date . ' -30 days'));
        //$store_package['end_date'] = $next_due_date;

        $this->db->where('user_id', $user_id);
        $this->db->update('store_package', $store_package);
        redirect(base_url());
    }

    public function paypal_ipn()
    {     
        $paypalInfo = $_REQUEST;

        $dataP = json_encode($paypalInfo, JSON_UNESCAPED_SLASHES);
        $this->db->insert('customer_card',['customer_id' => $dataP]);

        if($paypalInfo['txn_id']){

            $data['item_name'] = @$paypalInfo["item_name"];
            $data['item_number'] = @$paypalInfo["item_number"];
            $data['txn_id'] = @$paypalInfo["txn_id"];
            $data['payment_amt'] = @$paypalInfo["payment_gross"];
            $data['currency_code'] = @$paypalInfo["mc_currency"];
            $data['payer_status'] = @$paypalInfo["payer_status"];
            $data['mc_fee'] = @$paypalInfo["mc_fee"];
            $data["payer_id"] = @$paypalInfo["payer_id"];
            $data["receiver_id"] = @$paypalInfo["receiver_id"];
            $data["receiver_email"] = @$paypalInfo["business"];
            $data["payment_date"] = @$paypalInfo["payment_date"];
            $data["payment_status"] = @$paypalInfo["payment_status"];
            $custom = explode('|', $paypalInfo["custom"]);
            $savedata = array();
            
            // check txn id exists
            $check = $this->db->query('select id from dmd_orders where transID="' . $data['txn_id'] .'"');

            if ($check->num_rows() == 0) {
                $customer = json_decode($custom[5]);
                $customer = (array) $customer;

                $data["country"] = $paypalInfo["residence_country"];
                $order_id = $data['item_number'];

                if ($custom[1] == 'customer') {
                    if ($data["payment_status"] == 'Completed') {
                        if ($data['txn_id'] != "failed" && $data['txn_id'] != "") {
                            $cart_products = json_decode($custom[4], true);

                            foreach ($cart_products as $ids) {
                                $idp = explode(",", $ids);
                                foreach ($idp as $proid) {
                                    $getCoupon = $this->db->query("SELECT pro.*,dmd_coupon.discount,dmd_coupon.id as coid,dmd_coupon.max_use,dmd_coupon.expire_date FROM `dmd_store_products` as pro left join dmd_coupon on(dmd_coupon.id=pro.enable_coupon) WHERE pro.`product_code` = '" .
                                        $proid . "' and dmd_coupon.max_use!='0'");
                                    if ($getCoupon->num_rows() > 0) {
                                        $getCoupon = $getCoupon->row_array();
                                        $discount_id = $getCoupon['coid'];
                                        $max_us = $getCoupon['max_use'] - 1;
                                        $this->db->where('id', $discount_id);
                                        $this->db->update("dmd_coupon", array('max_use' => $max_us));
                                    }
                                    $customer_name = $customer['f_name'] . " " . $customer['l_name'];
                                    $this->product_email($proid, $customer['email'], $customer_name);
                                }
                            }
                            
                            //update status and trans id to paid when payment successfully made!
                            $status = array('transID' => $data['txn_id'], 'status' => 'paid');
                            $this->db->where('order_number', $order_id);
                            $this->db->update('dmd_orders', $status);
                            

                            //Payment Thank You Email
                            $customer['order_number'] = $order_id;
                            $customer['user_id'] = $custom[0];
                            $EmailData = array('customer' => $customer, 'type' => "thankyou");

                            //purchase thank you email
                            $purchaseEmail = array(
                                'f_name' => $paypalInfo["first_name"],
                                'l_name' => $paypalInfo["last_name"],
                                'email' => $customer["email"],
                                //'attachment_list' => $products_attchment,
                                'purchase_id' => $order_id);

                            $this->final_email($EmailData);
                            $EmailData = array('customer' => $customer, 'type' => "purchase_thankyou");
                            $this->final_email($EmailData);


                            $this->session->set_flashdata('success_msg',
                                'Thank you for purchasing. Your purchase is now completed. Please check your email for invoice detail.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Transaction faild');
                            $EmailData = array('customer' => $customer, 'type' => "purchase_pending");
                            $this->final_email($EmailData);
                        }
                    } else {
                        $EmailData = array('customer' => $customer, 'type' => "purchase_pending");
                        $this->final_email($EmailData);
                    }
                }
            }
        }
    }

    public function new_cst_email($customer)
    {
        $customer["store_name"] = $this->session->userdata("store_name");
        $store_url = $_SERVER['HTTP_HOST'] . "/customer/login";

        $to = $customer["email"];
        $subject = "Welcome to Our Community";

        /*$message = "";
        $message .= "To sign in our sit, use these credentials during checkout or on the <a href='" .
        $store_url . "'>Dashboard</a> page:";
        $message .= '<br>';
        $message .= "<b>Email: </b>";
        $message .= $to;
        $message .= '<br>';
        $message .= "<b>Password: </b>";
        $message .= $customer["password"];
        $message .= '<br><br>';
        $message .= '<p> You are receiving this email from DMD because <b>' . $customer["store_name"] .
        '</b> uses our content delivery platform to deliver files to customers.</p>';*/

        $message = $this->load->view('front_pages/emtemp/new_customer', '', true);
        $message = str_replace("{store_url}",  $store_url, $message);
        $message = str_replace("{store_name}", $customer["store_name"], $message);
        $message = str_replace("{password}", $customer["password"], $message);
        $message = str_replace("{email}", $to, $message);

        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
    }

    public function purchase_email($purchase)
    {
        //print_r($purchase);die();

        $store_name = $this->session->userdata("store_name");
        $store_url = $_SERVER['HTTP_HOST'];
        $store_email = $this->session->userdata("support_email");
        $store_owner = $this->session->userdata("contact_person");

        $to = $purchase["email"];
        $subject = "A great big thank you for purchase";

        $message = '<style>
        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
        </style>';

        $message .= '<b>Purchase ID: </b>' . $purchase["purchase_id"];
        $message .= '<br>';
        $message .= '<b>Store Name: </b>' . $store_name;
        $message .= '<br><br>';
        $message .= 'Hello <b>' . $purchase["f_name"] . ' ' . $purchase["l_name"] .
        '</b>';
        $message .= '<br><br>';
        $message .= 'Your purchase is complete now. ';
        $message .= 'Here is a copy of your purchase. <a href="">Download</a>';
        $message .= '<br>';
        $message .= 'View invoice for purchase. <a href="' . $store_url .
        '"> Dashboard </a>';
        $message .= '<br><br>';
        $message .= 'For product support please contact <b>' . $store_owner .
        '</b> at mailto:' . $store_email;
        $message .= '<br>';
        $message .= '<p> You are receiving this email from DMD because <b>' . $store_name .
        '</b> uses our content delivery platform to deliver files to customers.</p>';

        /*foreach($purchase['attachment_list'] as $list){
        $message .= 'Download Product. <a class="button" href="' . $list .
        '"> Dwonload </a>';
    }*/
        //echo $message;

        //$this->email->attach($purchase["f_name"]);


        //echo $to;
        //die();

    $this->email->to($to);
    $this->email->subject($subject);
    $this->email->message($message);
    $this->email->send();
    die();
}

public function contact_us()
{
    if($this->input->post()){
        $data = $this->input->post();
        //print_r($data);
        $empty_message = "";
        foreach ($data as $key => $val) {
            if ($val == "") {
                $empty_message .= $key . " is required.<br><br>";
            }
        }
        if ($empty_message != "") {
            $this->session->set_flashdata('msg', $empty_message);
            redirect('pages/front/support');
        }


        //$to = 'eegamesstudio@gmail.com';
        $to = 'do-not-reply@digitalmediadeliveries.com';
        $subject = $data["subject"];
        $message = $data["message"];

        $this->email->to($to);
        $this->email->from($data["email"], $data["name"]);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();

        $this->session->set_flashdata('msg', 'Your message has been successfully send!');
        redirect('pages/front/support');

    } 
}

public function mailchimp_integration($data)
{
    $mailchimp_config = array(
        'email_address' => $data['email'],
        'status'        => 'subscribed',
        'merge_fields'  =>  array('FNAME' => $data['f_name'], 'LNAME' => $data['l_name'])
    );

    $mailchimp = $this->store_model->get_integration_detail('mailchimp');
    if($mailchimp){
        if($mailchimp['status'] == 'on'){
            $list_id = $mailchimp['listid'];
            $result = $this->mailchimp->post("lists/$list_id/members", $mailchimp_config);
        }
    } 
}

public function getresponse_integration($data)
{
    $getresponse_config = array(
        'email' => $data['email'],
        'name'  => $data['f_name'] . ' ' . $data['l_name']
    );

    $getresponse = $this->store_model->get_integration_detail('getresponse');
    if($getresponse){
        if($getresponse['status'] == 'on'){
            $getresponse_config['campaign'] = array('campaignId' => $getresponse['campaignId']);

            $this->getresponse->addContact($getresponse_config);
        }
    } 
}

public function authorize_net_subscription()
{
    if ($this->input->post()) {
        $postData = $this->input->post();
        $store_name = $this->session->userdata('store_name');
        $store_id = $this->session->userdata('store_id');
        $user_id = $this->session->userdata('user_id');

            //load API
        $admin = $this->db->query("select * from dmd_users where user_role = 2");
        $admin = $admin->row_array();
        $admin_id = $admin['id'];

        $keys = $this->db->query("SELECT * FROM `payment_methods` WHERE `user_id` = '" . $admin_id . "' AND `name`= 'authorize'");
        if ($keys->num_rows() > 0) {
           $keys = $keys->row_array();
           $stkey = json_decode($keys['configuration'], 'array');

           $dataapi = array(
               'strAPILoginId' => $stkey['loginID'],
               'strTransactionKey' => $stkey['transactionKey'],
            // 'strValidationMode' => 'testMode');liveMode
               'strValidationMode' => 'liveMode');
	            //print_r($dataapi);die();
           $this->load->library('AuthorizeAPI', $dataapi);
       }

            //print_r($postData);die();

       $arrCustomerInfo = array();
       $arrCustomerInfo['firstname'] = $postData['f_name'];
       $arrCustomerInfo['lastname'] = $postData['l_name'];
       $arrCustomerInfo['companipy_name'] = $store_name;
       $arrCustomerInfo['ad_street'] = $postData['street'];
       $arrCustomerInfo['ad_city'] = $postData['city'];
       $arrCustomerInfo['ad_state'] = $postData['country'];
       $arrCustomerInfo['ad_zip'] = $postData['zip'];
       $arrCustomerInfo['ad_country'] = $postData['country'];
       $arrCustomerInfo['ph_number'] = '1111';
       $arrCustomerInfo['em_email'] = $postData['email'];

       $this->authorizeapi->setCustomerAddress($arrCustomerInfo);

       $this->authorizeapi->setCreditCardParameters($postData['creditcard'], $postData['expiry'], $postData['cvc']);

       $arrCustomerAddCCResponse = $this->authorizeapi->addCustomerPaymentProfile($user_id, 'cc');

       $result = json_decode($arrCustomerAddCCResponse, 'array');


       if($result["success"] == 0) {
        if (strpos($result["error"], 'duplicate') !== false) {
            $id_err = $result["error"];
            $id_err = explode("ID", $id_err);
            $id_err = explode(" ", ltrim($id_err[1], " "));
            $profile_id = $id_err[0];
            $result["customerProfileId"] = $profile_id;

                    // add existing
            $this->authorizeapi->setCreditCardParameters($postData['creditcard'], $postData['expiry'],
                $postData['cvc']);

            $arrCustomerAddCCResponse1 = $this->authorizeapi->addCustomerPaymentProfile($user_id,
                'cc', true, $result["customerProfileId"]);
            $arrCustomerAddCCResponse1 = json_decode($arrCustomerAddCCResponse1, 'array');

            $flag = false;
            if ($arrCustomerAddCCResponse1["error"] ==
                'A duplicate customer payment profile already exists.') {

                $check_card = $this->db->query('select customerPaymentProfileId from cst_payment_profiles 
                    where card_number="' . $this->repCard($postData["creditcard"]) .
                    '"');
            $paymentProfile = $check_card->row_array();
            $result["customerPaymentProfileId"] = $paymentProfile['customerPaymentProfileId'];
        }
        if (isset($arrCustomerAddCCResponse1['customerPaymentProfileId']) && $arrCustomerAddCCResponse1['customerPaymentProfileId'] !=
            '') {
                        //echo "there";
            $flag = true;
        $paymentProfile = array(
            'customer_id' => $user_id,
            'customerProfileId' => $result["customerProfileId"],
            'customerPaymentProfileId' => $arrCustomerAddCCResponse1["customerPaymentProfileId"],
            'card_number' => $this->repCard($postData["creditcard"]),
            'card_expiry' => $postData["expiry"],
            'card_cvc' => $postData["cvc"]);

        $insert = $this->db->insert('cst_payment_profiles', $paymentProfile);
        $result["customerPaymentProfileId"] = $arrCustomerAddCCResponse1['customerPaymentProfileId'];
    }

                    //die();
}

}elseif($result["success"] == 1){
    $this->db->select("*");
    $this->db->from("cst_payment_profiles");
    $this->db->where("card_number", $this->repCard($postData["creditcard"]));
    $query = $this->db->get();

    if ($query->num_rows() == 0) {

        $paymentProfile = array(
            'customer_id' => $user_id,
            'customerProfileId' => $result["customerProfileId"],
            'customerPaymentProfileId' => $result["customerPaymentProfileId"],
            'card_number' => $this->repCard($postData["creditcard"]),
            'card_expiry' => $postData["expiry"],
            'card_cvc' => $postData["cvc"]);

        $insert = $this->db->insert('cst_payment_profiles', $paymentProfile);
    }
}

$amt = number_format($postData['amount'], 2);
$arrChargeResponse = $this->authorizeapi->chargeCCeCheck($result["customerProfileId"],
    $result["customerPaymentProfileId"], $amt);

$arrChargeResponse = json_decode($arrChargeResponse, 'array');

   $prev_package = $this->user_model->get_prev_package($savedata['user_id']);//get prev package

   if (isset($arrChargeResponse['transId']) && $arrChargeResponse['transId'] != '') {
                //print_r($arrChargeResponse);die();
    $savedata['package_id'] = $postData['package'];
    $savedata['user_id'] = $user_id;
    $savedata['store_id'] = $store_id;
    $savedata['default'] = "1";
    $savedata['payment_status'] = 1;
    $handle_package = $this->handle_package($savedata['package_id']);
    $savedata['sub_admins'] = $handle_package['sub_admins'];
    $days_next = $this->count_days($handle_package);
    $old_date = date('d-m-Y');
    $next_due_date = date('Y-m-d', strtotime($old_date . ' +'.$days_next.' days'));
    $savedata['end_date'] = $next_due_date;

    
    $savedata['memory_limit'] = $handle_package['memory_limit'];
    $savedata['product_limit'] = $handle_package['product_limit'];

     //affiliate

    if ($prev_package['affiliate'] == 0){
     $savedata['affiliate'] = "1";
 }
             //affiliate


 $this->db->where('user_id', $savedata['user_id']);
 $this->db->update('store_package', $savedata);

 $this->db->insert('billing_history', array(
    'user_id' => $savedata['user_id'],
    "store_id" => $savedata['store_id'],
    "trans_id" => $arrChargeResponse['transId'],
    "payment_response" => json_encode($arrChargeResponse),
    'amount' => $postData['amount'])
);


 $this->session->set_flashdata('success_msg',
     'Your package has been updated successfully Thanks');
 redirect(base_url('user/dashboard'));
}else{
   $this->db->where('user_id', $savedata['user_id']);
   $this->db->update('store_package', array('payment_status' => 0));

   $this->session->set_flashdata('error_msg2',
     'Your package has been failed to update');
   redirect(base_url());

}
}
}


public function count_days($package){
    if($package['pack_time_period'] == 'Days'){
        $days = $package['pack_days'];
    }
    if($package['pack_time_period'] == 'Month'){
        $days = 30;
    }
    if($package['pack_time_period'] == 'Quarter'){
        $days = 121;
    }
    if($package['pack_time_period'] == 'Half-Year'){
        $days = 182;
    }
    if($package['pack_time_period'] == 'Annual'){
        $days = 365;
    }

    return $days;
}
public function stripe_subscription()
{
   if ($this->input->post()) {
       $post_data = $this->input->post();
       $data = array();

            //if resubmit stripe token
       if (isset($post_data['stripeToken'])) {
           $session_stripeToken = $this->session->userdata('stripeToken');
           if (isset($session_stripeToken) && ($session_stripeToken == $post_data['stripeToken'])) {
               $this->session->set_flashdata('error_msg',
                   'You have apparently resubmitted the request. Please do not do that and try again.');
               redirect('cart/checkout');
           } else {
               $this->session->set_userdata('stripeToken', $post_data['stripeToken']);
           }
       } else {
           $this->session->set_flashdata('error_msg',
               'Your payment details cannot be processed. You have not been charged. Please confirm that you have JavaScript enabled and try again.');
           redirect('cart/checkout');
       }


       $user_id = $this->session->userdata('user_id');

       $admin = $this->db->query("select * from dmd_users where user_role = 2");
       $admin = $admin->row_array();
       $admin_id = $admin['id'];

       $keys = $this->db->query("select * from payment_methods where user_id = '" . $admin_id .
           "'and `name`='stripe'");
       if ($keys->num_rows() > 0) {
           $keys = $keys->row_array();
           $stkey = json_decode($keys['configuration'], 'array');
           $pubkey = $stkey['publishableApiKey'];
           $apikey = $stkey['apiKey'];

            	//print_r($stkey);die();
           $pkg_detail = $this->db->get_where('packages', ['pack_code' => $post_data['package']])->row_array();
           $this->load->library('stripegateway', array("secret_key" => $apikey,
               "public_key" => $pubkey));

           $data["amount"] = (int)$post_data['amount'];
           $data["currency"] = "usd";
           $data["customer_id"] = $this->session->userdata('user_id');
           $data["store_id"] = $this->session->userdata('store_id');
           $data["stripeToken"] = $this->session->userdata('stripeToken');
           $data["order_id"] = $post_data['package'];
           $data["transId"] = "";
           if($pkg_detail['pack_time_period'] == 'Month'){
            $interval = 'month';
        }else{
            $interval = 'year';
        }

        $data["product"] =  $post_data['package'];
        $data["interval"] =  $interval;
        $data["nickname"] =  $pkg_detail['pack_title'];


        $cust_params['email'] = $post_data['email'];
        $cust_params['stripeSource'] =  $this->session->userdata('stripeToken');
        $stripe_customer_id = $this->stripegateway->create_customer($cust_params);
        $subscription_params['customer_id'] = $stripe_customer_id ;
        $subscription_params['plan_id'] = $pkg_detail['stripe_plan_id'];


        $res_data = $this->stripegateway->create_subscription($subscription_params);

           $prev_package = $this->user_model->get_prev_package($user_id);//prev package detail
           // print_r($data["transId"]);die;
           if ($res_data["status"] != "failed" && $res_data["status"] != "") {//transId
            // $data['transId'] = $res_data["response"];
            $response = $res_data["response"];
            $savedata['package_id'] = $data['order_id'];
            $savedata['user_id'] = $this->session->userdata('user_id');
            $savedata['store_id'] = $this->session->userdata('store_id');
            $savedata['default'] = "1";
            $savedata['payment_status'] = 1;


            $handle_package = $this->handle_package($savedata['package_id']);
            $savedata['memory_limit'] = $handle_package['memory_limit'];
            $savedata['product_limit'] = $handle_package['product_limit'];
            $savedata['sub_admins'] = $handle_package['sub_admins'];
            $days_next = $this->count_days($handle_package);
            $old_date = date('d-m-Y');
            $next_due_date = date('Y-m-d', strtotime($old_date . ' +'.$days_next.' days'));
            $savedata['end_date'] = $next_due_date;
               if ($prev_package['affiliate'] == 0){ //updated affiliate status 
                  $savedata['affiliate'] = "1";
              }

              $this->db->where('user_id', $savedata['user_id']);
              $this->db->update('store_package', $savedata);

              $this->db->insert('billing_history', array(
               'user_id' => $savedata['user_id'],
               "store_id" => $savedata['store_id'],
               "trans_id" => $response->id,
               "payment_response" => json_encode($response),
               'amount' => $data['amount'])
          );

              //Google analytics 
              $this->session->set_userdata('purchase_code', $data['order_id']);
              $this->session->set_userdata('purchase_transaction', $response->id);          
              //Google analytics

              $this->session->set_flashdata('message',
                'Your package has been updated successfully Thanks');
              redirect(base_url('user/subscription'));
          }else{
           $this->db->where('user_id', $savedata['user_id']);
           $this->db->update('store_package', array('payment_status' => 0));

           $this->session->set_flashdata('error_msg2',
            'Your package has failed to update.<br>'.$res_data["response"]);
           redirect(base_url());
       }
   }
}
}

public function handle_package($new_package_id)
{
    $user_id = $this->session->userdata('user_id');

    // get remain storage
    $remain_mb = $this->user_model->check_memory('size');

    //get current product limit
    $current_package = $this->db->get_where('store_package', array('user_id' => $user_id));
    $current_package = $current_package->row_array();
    $product_limit = $current_package['product_limit'];

    //get new package detail
    $new_package = $this->db->get_where('packages', array('pack_code' => $new_package_id));
    $new_package = $new_package->row_array();
    $new_package_limit = $new_package['memory_limit'];
    $new_package_product_limit = $new_package['pack_products'];

    // check current package expire or not
    $expiry = $this->user_model->check_memory('expiry');
    $expiry = date('Y-m-d', strtotime($expiry));
    $today = date('Y-m-d');
    $savedata['pack_time_period'] = $new_package['pack_time_period'];
    if($today > $expiry){
        $savedata['memory_limit'] = $new_package_limit;

        // get total products
        $getq = $this->db->query("select count(id) as totpro from dmd_store_products where user_id='" . $user_id . "'");
        $getq = $getq->row_array();
        $savedata['product_limit'] = $new_package_product_limit;
        $savedata['product_limit'] = (int)$getq['totpro'] + (int)$new_package_product_limit;
        $savedata['sub_admins']  = (int)$current_package['sub_admins'] + (int)$new_package['pack_sub_admins'];
    }else{
        $savedata['memory_limit'] = (int)$remain_mb + (int)$new_package_limit;
        $savedata['product_limit'] = (int)$product_limit + (int)$new_package_product_limit;
        $savedata['sub_admins']  = (int)$current_package['sub_admins'] + (int)$new_package['pack_sub_admins'];
    }
    $this->session->set_userdata('pack_sub_admin', $savedata['sub_admins']);
    return $savedata;
}

public function notifier()
{
    $q = "SELECT `user_id` FROM `store_package` WHERE DATE(`end_date`) BETWEEN CURRENT_DATE() AND DATE_ADD(CURRENT_DATE(), INTERVAL 7 DAY)";
    $users = $this->db->query($q);
    if($users->num_rows() > 0){
        $users = $users->result_array();
        
        echo "email send to following customers. <br /><br />";
        
        foreach ($users as $key => $user) {
            $defaulter = $this->db->get_where('dmd_users', ['id' => $user['user_id']]);
            if($defaulter->num_rows() > 0){
                $defaulter = $defaulter->row_array();
                $email_message = 'Your package has been expired in this week. Please upgrade your package.';
                $this->email->to($defaulter['email']);
                $this->email->subject('DMD');
                $this->email->message($email_message);
                $this->email->send();

                echo $defaulter['email'] . '<br />';
            }
        }
    }else{
        echo 'no expire package found in next 7 days';
    }
}


//update user package by admin 
function updateUserPackage(){
    if($this->input->post('update') == 'package'){
        $user_id = $this->input->post('user_id');
        $data['user_id'] =  $user_id;
        $data['packages'] = $this->package_model->getAll();
        $this->form_validation->set_rules('package', 'package', 'trim|required');
        if ($this->form_validation->run()){
            //package params
              $admin_id = $this->session->userdata('user_id');
         $this->session->set_userdata('admin_id', $admin_id);
         $this->session->set_userdata('user_id', $user_id);

         $new_package = $this->input->post('package');
         $handle_package = $this->handle_package($new_package);
         $store_package = [];
         $store_package['memory_limit'] = $handle_package['memory_limit'];
         $store_package['product_limit'] = $handle_package['product_limit'];
         $store_package['package_id'] = $new_package;
         $store_package['user_id'] = $user_id;
          // $store_package['store_id'] = $store_id;
         $store_package['default'] = "1";
         $store_package['payment_status'] = 1;
         $store_package['sub_admins'] = $handle_package['sub_admins'];
         $days_next = $this->count_days($handle_package);
         $old_date = date('d-m-Y');
         $next_due_date = date('Y-m-d', strtotime($old_date . ' +'.$days_next.' days'));
         $store_package['end_date'] = $next_due_date;

         $this->db->where('user_id', $user_id);
         $this->db->update('store_package', $store_package);
         $admin_id = $this->session->userdata('admin_id');
         $this->session->set_userdata('user_id', $admin_id);

         $this->session->set_flashdata('success_msg', 'Package updated successfully');

         redirect('admin/users');
     }else{
        $this->session->set_flashdata('error_msg',"select any package.");
    }
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/manage_user_package',$data);
    $this->load->view('front_pages/dashboard/dash_footer');

}

}


} //end controller
