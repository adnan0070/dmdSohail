<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tutorial extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('Tutorial_model');
		$this->load->model('Product_model');
		$this->load->model('User_model');
		$this->load->library('pagination');
	}

	function index()
	{
		$data["tutorials"] = $this->Tutorial_model->get_all();	
		$this->load->view('front_pages/dashboard/dash_header');
		$this->load->view('front_pages/dashboard/tutorial/index',$data);
		$this->load->view('front_pages/dashboard/dash_footer');
	}

	function add()
	{
		if($this->input->post('submitted') == 'add'){
			$this->form_validation->set_rules('title','Title','trim|required');
			$this->form_validation->set_rules('description','Description','trim|required');	
			if($this->form_validation->run())     
			{
				$params['title'] = $this->input->post('title');
				$params['description'] = $this->input->post('description');
				if(!empty($_FILES['mediaFile']['name']))
				{ 
					$config['upload_path']    = 'site_assets/tutorial/';
					$config['allowed_types']  = '*';
					$new_name = time()."_".str_replace(' ', '', $_FILES['mediaFile']['name']);
					$config['file_name'] = $new_name;
					$config['max_size'] = '0';
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if($this->upload->do_upload('mediaFile'))
					{
						$uploadData = $this->upload->data();
						$params['file'] = $uploadData['file_name'];
						$inser_id = $this->Tutorial_model->insert($params);
						$this->session->set_flashdata('success_msg', 'Tutorial has been added successfully.');
						unset($_POST);
						redirect('tutorial');
					}else{
						$this->session->set_flashdata('error_msg', $this->upload->display_errors());
					}
				}else{
					$this->session->set_flashdata('error_msg', 'Select Tutorial video file');
				}
			}
		}
		$this->load->view('front_pages/dashboard/dash_header');
		$this->load->view('front_pages/dashboard/tutorial/add');
		$this->load->view('front_pages/dashboard/dash_footer');
	}


	function edit()
	{
		$id = decode($this->uri->segment(3));
		$tutorial = $this->Tutorial_model->get($id);
		if($this->input->post('submitted') == 'edit'){

			$this->form_validation->set_rules('title','Title','trim|required');
			$this->form_validation->set_rules('description','Description','trim|required');	
			if($this->form_validation->run())     
			{
				$params['title'] = $this->input->post('title');
				$params['description'] = $this->input->post('description');

				if(!empty($_FILES['mediaFile']['name']))
				{ 
					$config['upload_path']    = 'site_assets/tutorial/';
					$config['allowed_types'] ='*';
					$new_name = time()."_".str_replace(' ', '', $_FILES['mediaFile']['name']);
					$config['file_name'] = $new_name;
					$config['max_size'] = '0';
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if($this->upload->do_upload('mediaFile'))
					{
						$uploadData = $this->upload->data();
						$params['file'] = $uploadData['file_name'];
						$result = $this->Tutorial_model->update($id, $params);
						$this->session->set_flashdata('success_msg', 'Tutorial   has been updated successfully.');
						unlink('site_assets/tutorial/' . $tutorial['file']);
						unset($_POST);
						redirect('tutorial');
					}else{
						$this->session->set_flashdata('error_msg', $this->upload->display_errors());
					}
				}else{
					$result = $this->Tutorial_model->update($id, $params);
					$this->session->set_flashdata('success_msg', 'Tutorial   has been updated successfully.');
					unset($_POST);
					redirect('tutorial');
				}

				

			}
		}
		$data['tutorial']  = $tutorial;
		$this->load->view('front_pages/dashboard/dash_header');
		$this->load->view('front_pages/dashboard/tutorial/edit', $data);
		$this->load->view('front_pages/dashboard/dash_footer');
	}
	public function delete(){

		$id = decode($this->uri->segment(3));
		$res  = $this->Tutorial_model->get($id);
		if($res)
		{
			$result = $this->Tutorial_model->delete($id);
			unlink('site_assets/tutorial/' . $res['file']);
			$this->session->set_flashdata('success_msg', 'Tutorial has been removed successfully.');
		}

		redirect('tutorial');
	}

	function add_webinar($code=''){

		$product = $this->Product_model->get_SingleProduct_Bycode($code);
		if($product){
			if(!empty($this->input->post('code'))){
				if(!empty($_FILES['webinarfile']['name'])){
					// if (!file_exists('./site_assets/products/' . $code)) {
					// 	mkdir('./site_assets/products/' . $code, 0777, true);
					// }

					$webinar_config['upload_path']    = './site_assets/products/';
					$webinar_config['allowed_types']  = '*';
					$new_name = time()."_".str_replace(' ', '', $_FILES['webinarfile']['name']);
					$webinar_config['file_name'] = $new_name;
					$webinar_config['max_size'] = (float)$this->User_model->check_memory("size") * 1024;
					$this->load->library('upload',$webinar_config);
					$this->upload->initialize($webinar_config);
					if($this->upload->do_upload('webinarfile'))
					{
						$webinar_data = $this->upload->data();
						$params['webinar_video'] = $webinar_data['file_name'];
						$params['webinar_memory_size'] = $webinar_data['file_size'];
						$params['is_webinar'] = '1';

						$hour = $this->input->post('hour');
						$min  =  $this->input->post('minute');
						$sec  = $this->input->post('second');
						if($hour > 0){
							$hour = $hour*60*60; 
						}
						if($min > 0){
							$min = $min*60;
						}
						$params['webinar_play_duration'] = $hour+$min+$sec;
						$this->Product_model->update($params, $code);
						$this->session->set_flashdata('success_msg', 'your product successfully updated!');
						redirect(base_url('Product/PurchaseAction/') . $code);

					}else{
						$this->session->set_flashdata('error_msg', $this->upload->display_errors());
					}
				}
				else{
					$this->session->set_flashdata('error_msg', 'Upload video');
				}

				redirect('Tutorial/add_webinar/'.$code);

			}else{
				$this->load->view('front_pages/dashboard/dash_header');
				$data['code'] = $code;
				$this->load->view('front_pages/dashboard/webinar', $data);
				$this->load->view('front_pages/dashboard/dash_footer');
			}
		}else{
			redirect('Home');
		}


	}

	
	

}

?>