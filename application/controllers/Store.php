<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Store extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('user_model');
        $this->load->model('store_model');
        $integration = $this->store_model->get_integration_detail('mailchimp');
        if($integration){
            $this->load->library('mailchimp', $integration);
        }

        $integration = $this->store_model->get_integration_detail('getresponse');
        if($integration){
            $this->load->library('getresponse', $integration);
        }

    }
    public function active($id){
        $store_check = $this->db->query('select id,store_name,store_url,status from dmd_stores where id="'.$id.'"');
        if($store_check->num_rows()>0){  
            $store_res = $store_check->row_array();
            if($store_res['status'] == 1){
                $this->session->set_userdata('store_id', $id);
                $this->session->set_userdata('store_name', $store_res['store_name']);
                $this->session->set_userdata('store_url', $store_res['store_url']);
                $this->session->set_flashdata('success_msg', $store_res['store_name'].' is activated now all changes will be apply to selected store');
                //redirect('user/dashboard');
            }else{
                $this->session->set_flashdata('error_msg', $store_res['store_name'].' is blocked.');
            }
            redirect('user/dashboard');
        }
    }
    public function dataready($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    public function add()
    {
        if (isset($_REQUEST['subdomain'])) {

            $userId = $this->session->userdata('user_id');
            $store = array(
                'store_name' => $_REQUEST['storeName'],
                'store_url' => $_REQUEST['subdomain'] . '.' . preg_replace('/^www\./i', '', $_SERVER['HTTP_HOST']),
                'contact_person' => $_REQUEST['contactPerson'],
                'support_email' => $_REQUEST['supportEmail'],
                'user_id' => $userId,
            );

            $store_check = $this->store_model->store_check($store['store_url']);
            $store_check_name = $this->store_model->store_check_name($_REQUEST['storeName']);

            if ($store_check&&$store_check_name) {
                $storeId = $this->store_model->create_store($store);
                $storeId = $this->session->userdata('store_id');
                
            //$delete  = $this->db->query('delete from email_templates where user_id="'.$userId.'"'); 
            //$delete  = $this->db->query('delete from payment_methods where user_id="'.$userId.'"');
            // create payment methods for new created store
                $methods = array(
                    array('name'=> 'stripe','store_id'=> $storeId,'user_id'=> $userId),
                    array('name'=> 'authorize','store_id'=> $storeId,'user_id'=> $userId),
                    array('name'=> 'paypal','store_id'=> $storeId,'user_id'=> $userId),

                );
                $chk_pay = $this->db->query('select id from payment_methods where user_id="'.$userId.'"');
                if($chk_pay->num_rows()==0){
                    foreach ($methods as $key => $value) {
                        $methods_id = $this->store_model->create_pm($value);
                    }
                }
                
                $thanks = 'Purchase ID: {purchase_id}&nbsp;<div>Store name: {store_name}&nbsp;</div><div>Greeting</div><div>&nbsp;Hello {name}: Thank you for purchasing from {store_name} .</div><div>&nbsp;Your purchase is now complete.&nbsp;</div><div>View invoice for purchase {invoice_button} Alternatively, download your product please login with your information that are sent by dmd to your email For product support please contact at You are receiving this email from DMD because uses our content delivery platform to deliver files to customers.</div>';
                $thanks_free = 'Purchase ID: {purchase_id}&nbsp;<div>Store name: {store_name}</div><div>Greeting</div><div>Hello{name} Body from customer or default content Default content: Thank you for purchasing  from {store_name} . Your purchase is now complete. View invoice for purchase {invoice_button} For product support please contact {store_person_name}  at {store_contact_email}  You are receiving this email from DMD because {store_name}  uses our content delivery platform to deliver files to customers.</div>';
                $pending = 'Purchase ID: {purchase_id}&nbsp;<div>Store name: {store_name}</div><div>Greeting&nbsp;</div><div>Hello {name} : Thank you for purchasing from {store_name} . Your purchase is pending . Once your purchase is complete, you will receive another email with the login information</div><div>If you have any questions about the product, please contact {store_person_name}  at {store_contact_email}</div><div>&nbsp;For product support please contact at You are receiving this email from DMD because uses our content delivery platform to deliver files to customers.</div>';
            // create email temlates for new created store
                $e_temp = array(
                    array('type' => 'thankyou', 'subject'=> 'Thank You','store_id'=> $storeId,'user_id'=> $userId, 'content' => $thanks),
                    array('type' => 'purchase_thankyou', 'subject'=> 'free purchase Thank you e-mail','store_id'=> $storeId,'user_id'=> $userId, 'content' => $thanks_free),
                    array('type' => 'purchase_pending', 'subject'=> 'purchase pending you e-mail','store_id'=> $storeId,'user_id'=> $userId, 'content' => $pending),
                //array('type' => 'shipping', 'subject'=> 'shipping e-mail','store_id'=> $storeId,'user_id'=> $userId, 'content' => 'content'),
                //array('type' => 'pin_request', 'subject'=> 'pin request e-mail','store_id'=> $storeId,'user_id'=> $userId, 'content' => 'content'),

                );
            // chk user email
                $chk_em = $this->db->query('select id from email_templates where user_id="'.$userId.'"');
                if($chk_em->num_rows()==0){
                 foreach ($e_temp as $key => $value) {
                    $this->store_model->create_email($value);
                }    
            }



            $this->session->set_flashdata('success_msg', 'Store created successfully.');
            redirect('user/dashboard');

        } else {

            $this->session->set_flashdata('error_msg', 'this store name already exists!');
            redirect('store/add');


        }
    }
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/addstore');
    $this->load->view('front_pages/dashboard/dash_footer');
}

public function update($id)
{
    $subdomain_id = $this->uri->segment(3);
    $data = array();
    $store_id = $this->session->userdata('store_id');
    $data['store'] = $this->store_model->get_StoreRows('dmd_stores', 'id', $store_id);

    $this->load->library('form_validation');

    if (isset($_REQUEST['update_store'])) {
            // echo '<pre>';
            // print_r($_REQUEST);
            // echo '</pre>';
            // die('asdasd');

        $rules = array(
            array(
                'field' => 'store[storeName]',
                'label' => 'Store Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'store[subdomain]',
                'label' => 'Sub Domain',
                'rules' => 'trim|alpha_dash|required',
            ),
            array(
                'field' => 'store[contactPerson]',
                'label' => 'Contact Person',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'store[supportEmail]',
                'label' => 'Support Email',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'store[address1]',
                'label' => 'Business Address #1',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'store[city]',
                'label' => 'City',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'store[country]',
                'label' => 'Country',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'store[sales_policy]',
                'label' => 'Return Policy',
                'rules' => 'trim|max_length[3000]|min_length[1]',
            ),
            array(
                'field' => 'store[privacy_policy]',
                'label' => 'Privacy Policy',
                'rules' => 'trim|max_length[3000]|min_length[1]',
            ),
            array(
                'field' => 'store[terms_services]',
                'label' => 'Terms of Services',
                'rules' => 'trim|max_length[3000]|min_length[1]',
            )
        );

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_msg', validation_errors());
            redirect('store/update/' . $store_id);
        }
        $userId = $this->session->userdata('user_id');
        $store = $_REQUEST['store'];

        $store_arr = array(
            'store_name' => $store['storeName'],
            'store_url' => $store['subdomain'] . '.' . $_SERVER['HTTP_HOST'],
            'contact_person' => $store['contactPerson'],
            'support_email' => $store['supportEmail'],
            'currency' => $store['currency'],
            'address1' => $store['address1'],
            'address2' => $store['address2'],
            'city' => $store['city'],
            'state' => $store['state'],
            'zip_code' => $store['zip_code'],
            'country' => $store['country'],
            'vat_id' => $store['vat_id'],
            'sales_policy' => $store['sales_policy'],
            'privacy_policy' => $store['privacy_policy'],
            'terms_services' => $store['terms_services'],
            'user_id' => $userId
        );
        $get_chk_row = $this->store_model->getRows('dmd_stores', $store_id);

            // print_r($get_chk_row['store_url']);
            // echo $store['store_url'];

        if ($store_arr['store_url'] != $get_chk_row['store_url']) {

            $store_check = $this->store_model->store_check($store_arr['store_url']);

        } else {

            $store_check = true;
        }


        if ($store_check) {

            $this->store_model->update_store($store_arr,$store_id);
            $this->session->set_flashdata('success_msg', 'Store Updated successfully.');
            redirect('user/dashboard');

        } else {

            $this->session->set_flashdata('error_msg', 'This store already exists!');
            redirect('store/update/' . $store_id);

        }
    }
    $data['id'] = $store_id;
    $this->load->view('front_pages/dashboard/dash_header');
    $this->load->view('front_pages/dashboard/editstore', $data);
    $this->load->view('front_pages/dashboard/dash_footer');
}
public function coupons($parm = "")
{
  $user_id = $this->session->userdata("user_id");
  $this->load->view('front_pages/dashboard/dash_header');
  if ($parm == "create") {
    $this->load->view('front_pages/dashboard/new_coupon');
}elseif($parm == "edit"){
    $id = $this->uri->segment(4,0);
    $data = array();
    $data['coupon'] = $this->store_model->getRows('dmd_coupon',$id);
    $this->load->view('front_pages/dashboard/new_coupon', $data);
}elseif($parm == "delete"){
    $id = $this->uri->segment(4,0);
    $data = array();
    $data['coupon'] = $this->store_model->deleteCoupon('dmd_coupon',$id);
        // $this->load->view('front_pages/dashboard/new_coupon', $data);

}
$data = array();
        //$data['coupons'] = $this->store_model->getRows('dmd_coupon');
$data['coupons'] = $this->store_model->get_coupons('dmd_coupon', 'user_id', $user_id);
$this->load->view('front_pages/dashboard/coupon_management', $data);
$this->load->view('front_pages/dashboard/dash_footer');

}
public function tax($parm = "")
{
    $user_id = $this->session->userdata("user_id");
    $this->load->view('front_pages/dashboard/dash_header');
    if ($parm == "create") {
        $this->load->view('front_pages/dashboard/new_tax');
    }elseif($parm == "edit"){
        $id = $this->uri->segment(4,0);
        $data = array();
        $data['coupon'] = $this->store_model->getRows('dmd_tax',$id);
        $this->load->view('front_pages/dashboard/new_tax', $data);
    } else {
        $data = array();
        //$data['coupons'] = $this->store_model->getRows('dmd_coupon');
        $data['coupons'] = $this->store_model->get_coupons('dmd_tax', 'user_id', $user_id);
        $this->load->view('front_pages/dashboard/tax_management', $data);
    }
    $this->load->view('front_pages/dashboard/dash_footer');

}

public function tax_create()
{
    if ($this->input->post()) {
            // check fields validation
        $empty_message = "";
        foreach ($this->input->post() as $key => $val) {
            if ($val == "") {
                $empty_message .= $key . " is required field<br>";
            }
        }
        if ($empty_message != "") {
            $this->session->set_flashdata('error_msg', $empty_message);
            redirect('store/tax/create');
        } else {

            $data = array();
            $data['country'] = $this->input->post('country');
            $data['name'] = $this->input->post('name');
            $data['price'] = $this->input->post('price');
            $data['user_id'] = $this->session->userdata('user_id');

                /*echo "<pre>";
                print_r($this->input->post());
                print_r($data);
                echo "</pre>";
                */

                $check_code = $this->store_model->check_tax($data['name']);
                if ($check_code) {
                    $create = $this->store_model->create_tax($data);
                    if ($create) {
                        $msg = "New tax has been created successfully!";
                        $this->session->set_flashdata('success_msg', $msg);
                    } else {
                        $msg = "New tax has not been created. Please try again!";
                        $this->session->set_flashdata('error_msg', $msg);
                    }
                } else {
                    $msg = "Tax name has already existed! Try with different tax name.";
                    $this->session->set_flashdata('error_msg', $msg);
                }

                redirect('store/tax');
            }
        }

    }

    public function tax_edit()
    {
        if($this->input->post()){
            $data = $this->input->post();
            $id = $data["id"];
            unset($data["id"]);

            $result = $this->store_model->edit_tax($id, $data);
            if($result){
                $msg = "Edited tax has been saved successfully!";
                $this->session->set_flashdata('success_msg', $msg);
            }else{
                $msg = "Edited coupon has not been saved. Please try again!";
                $this->session->set_flashdata('error_msg', $msg);
            }
            redirect('store/tax');
        }

    }

    public function get_tax()
    {
        if($this->input->post()){
            $store_id = $this->input->post('store');
            $store = $this->db->get_where('dmd_stores', ['id' => $store_id])->row_array();
            $country = $this->input->post('country');
            $tax = $this->db->get_where('dmd_tax', ['country' => $country, 'user_id' => $store['user_id']]);
            if($tax->num_rows() > 0){
                $taxx = $tax->row_array();
                echo json_encode($taxx);
                //print_r($taxx);
            }else{
                $taxx = ['country' => $country, 'price' => 0 ];
                echo json_encode($taxx);
            }
        }
    }

    public function update_tax_in_session()
    {
        if($this->input->post()){
            $this->session->set_userdata('cart_total', $this->input->post('cartTotal'));
            $this->session->set_userdata('tax_amount', $this->input->post('taxAmount'));
            $this->session->set_userdata('sub_total', $this->input->post('subTotal'));
            //print_r($this->session->userdata());
            echo 'true';
        }
    }


    public function coupon_create()
    {
        if ($this->input->post()) {
        //print_r($this->input->post());die();
            $empty_message = "";
            foreach ($this->input->post() as $key => $val) {
                if ($val == "") {
                    $empty_message .= $key . " is required field<br>";
                }
            }
            if ($empty_message != "") {
                $this->session->set_flashdata('error_msg', $empty_message);
                redirect('store/coupons');
            } else {

                $data = array();
                $data['status'] = $this->input->post('status');
                $data['name'] = $this->input->post('name');
                $data['code'] = $this->input->post('code');
                $data['discount'] = $this->input->post('discount');
                $data['max_use'] = $this->input->post('max_use');
                $data['active_date'] = date('Y-m-d', strtotime($this->input->post('active_date')));
                $data['expire_date'] = date('Y-m-d', strtotime($this->input->post('expire_date')));
                $data['store_id'] = 0;
                $data['user_id'] = $this->session->userdata('user_id');
                foreach ($data as $key => $val) {
                    if ($val == "") {
                        $data[$key] = "";
                    }
                }

                /*echo "<pre>";
                print_r($this->input->post());
                print_r($data);
                echo "</pre>";
                */

                $check_code = $this->store_model->check_coupon($data['code']);
                if ($check_code) {
                    $create = $this->store_model->create_coupon($data);
                    if ($create) {
                        $msg = "New coupon has been created successfully!";
                        $this->session->set_flashdata('success_msg', $msg);
                    } else {
                        $msg = "New coupon has not been created. Please try again!";
                        $this->session->set_flashdata('error_msg', $msg);
                    }
                } else {
                    $msg = "Coupon code has already existed! Try with different coupon code.";
                    $this->session->set_flashdata('error_msg', $msg);
                }

                redirect('store/coupons');
            }
        }

    }

    public function coupon_edit()
    {
        if($this->input->post()){
            $data = $this->input->post();
            $id = $data["id"];
            unset($data["id"]);
            //print_r($data);die();

            $result = $this->store_model->edit_coupon($id, $data);
            if($result){
                $msg = "Edited coupon has been saved successfully!";
                $this->session->set_flashdata('success_msg', $msg);
            }else{
                $msg = "Edited coupon has not been saved. Please try again!";
                $this->session->set_flashdata('error_msg', $msg);
            }
            redirect('store/coupons');
        }

    }

    public function paymentMethod($parm = "")
    { 
        $this->load->view('front_pages/dashboard/dash_header');
        if ($parm != "") {
            $pm = $this->store_model->get_pm($parm);
            if ($pm) {
                $data["pm"] = $pm;
                $this->load->view('front_pages/dashboard/' . $parm . "_configuration", $data);
            } else {
                $this->load->view('front_pages/dashboard/' . $parm . "_configuration");
            }
        } else {
            $data['pm_names'] = $this->store_model->check_pm();
            $this->load->view('front_pages/dashboard/payment_method', $data);
        }
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function integration($parm = "")
    {
        $this->load->view('front_pages/dashboard/dash_header');
        if ($parm != "") {
            //$pm = $this->store_model->get_pm($parm);
            $this->load->view('front_pages/dashboard/' . $parm . "_configuration");

        } else {
            $data['pm_names'] = $this->store_model->check_integration();
            $this->load->view('front_pages/dashboard/integration', $data);
        }
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    public function getresponseLists()
    {

       // 713e942af80e7666f9e888da47751fe8
       //$this->load->library('getresponse', array('apikey' => $_REQUEST['apiKey']));
       $lists = $this->getresponse->getCampaigns();
       //$lists = array(array('id' => "2", "name" => "test"));
       $list_html = '<select name="getresponse[campaignId]" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="select list">';
       $list_html .= '<option value="" class="form-input form-input-lg border-dark-1">select list</option>';
       foreach (@$lists as $listdata) {
        $selected = "";
        if ($_REQUEST['listid'] != '' && $_REQUEST['listid'] == $listdata->campaignId) {
            $selected = 'selected';
        }
        $list_html .= '<option '.$selected.' value="' . $listdata->campaignId .
        '" class="form-input form-input-lg border-dark-1">' . $listdata->name .
        '</option>';
    }
    echo $list_html .= '</select>';
        //var_dump($this->getresponse->getContacts());
}
public function saveresponseLists()
{
    $empty_message = "";
    $chimpPost = $_REQUEST['getresponse'];
    foreach ($chimpPost as $key => $val) {
        if ($val == "") {
            $empty_message .= $key . " is required field<br>";
        }
    }
    if ($empty_message != "") {
        $this->session->set_flashdata('error_msg', $empty_message);
        redirect('store/integration/getresponse');
    }
    if (isset($_REQUEST['enable'])) {
        $data['status'] = $_REQUEST['enable'];
    }
    $data['store_id'] = $this->session->userdata('store_id');
    $data['user_id'] = $this->session->userdata('user_id');
    $data['name'] = "getresponse";
    $data['configuration'] = json_encode($chimpPost);
    $this->db->query('delete from dmd_integrations where name="getresponse" and user_id="'.$this->session->userdata('user_id').'"');
    $create = $this->db->insert("dmd_integrations", $data);
    if ($create) {
        $msg = "Data has beed saved successfully!";
        $this->session->set_flashdata('success_msg', $msg);
        redirect('store/integration/getresponse');
    } else {
        $msg = "Please try again!";
        $this->session->set_flashdata('error_msg', $msg);
        redirect('store/integration/getresponse');
    }

}
public function saveiconnectLists()
{
    $empty_message = "";
    $iconnectPost = $_REQUEST['iconnectPost'];
    foreach ($iconnectPost as $key => $val) {
        if ($val == "") {
            $empty_message .= $key . " is required field<br>";
        }
    }
    if ($empty_message != "") {
        $this->session->set_flashdata('error_msg', $empty_message);
        redirect('store/integration/iconnect');
    }
    if (isset($_REQUEST['enable'])) {
        $data['status'] = $_REQUEST['enable'];
    }
    $data['store_id'] = $this->session->userdata('store_id');
    $data['user_id'] = $this->session->userdata('user_id');
    $data['name'] = "iconnect";
    $data['configuration'] = json_encode($iconnectPost);
    $this->db->query('delete from dmd_integrations where name="iconnect" and user_id="'.$this->session->userdata('user_id').'"');
    $create = $this->db->insert("dmd_integrations", $data);
    if ($create) {
        $msg = "Data has beed saved successfully!";
        $this->session->set_flashdata('success_msg', $msg);
        redirect('store/integration/iconnect');
    } else {
        $msg = "Please try again!";
        $this->session->set_flashdata('error_msg', $msg);
        redirect('store/integration/iconnect');
    }

}
public function geticontactLists()
{
    $iconnectPost = $_REQUEST;
    $this->load->library('icontact');
    
         /*$this->icontact->setConfig(array(
        'appId' => 'eb90ffc38a91f0ba8fb947e036a68947',
        'apiPassword' => 'FbeJEG48m6OqACnNc0ohSwQW',
        'apiUsername' => 'thezainiji@gmail.com'));*/

        $this->icontact->setConfig(array(
            'appId' => $iconnectPost['appId'],
            'apiUsername' => $iconnectPost['username'],
            'apiPassword' => $iconnectPost['password'])
    );
        
        
        $lists = $this->icontact->getLists();
        if(isset($lists[0]->listId)){
            $list_html = '<select name="iconnectPost[listid]" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="select list">';
            $list_html .= '<option value="" class="form-input form-input-lg border-dark-1">select list</option>';
            foreach ($lists as $listdata) {
                $selected = "";
                if ($_REQUEST['listid'] != '' && $_REQUEST['listid'] == $listdata->listId) {
                    $selected = 'selected';
                }
                $list_html .= '<option ' . $selected . '  value="' . $listdata->listId .
                '" class="form-input form-input-lg border-dark-1">' . $listdata->name .'</option>';
            }
            echo $list_html .= '</select>';
        }
        else{
            echo 'no records found';
        }
        
    }

    public function savemailchimpLists()
    {
        $empty_message = "";
        $chimpPost = $_REQUEST['chimpPost'];
        foreach ($chimpPost as $key => $val) {
            if ($val == "") {
                $empty_message .= $key . " is required field<br>";
            }
        }
        if ($empty_message != "") {
            $this->session->set_flashdata('error_msg', $empty_message);
            redirect('store/integration/mailchimp');
        }
        if (isset($_REQUEST['enable'])) {
            $data['status'] = $_REQUEST['enable'];
        }

        //authenticate mail chimp api key
        if (strpos($chimpPost['apiKey'], '-') === false) {
            $this->session->set_flashdata('error_msg', "Invalid MailChimp API key supplied.");
            redirect('store/integration/mailchimp');
        }

        $this->load->library('mailchimp', $chimpPost);
        $result = $this->mailchimp->get('lists');
        if(isset($result['status'])){
            //echo $result['detail'];die();
            $this->session->set_flashdata('error_msg', $result['detail']);
            redirect('store/integration/mailchimp');
        }


        $data['store_id'] = $this->session->userdata('store_id');
        $data['user_id'] = $this->session->userdata('user_id');
        $data['name'] = "mailchimp";
        $data['configuration'] = json_encode($chimpPost);
        $this->db->query('delete from dmd_integrations where name="mailchimp" and user_id="'.$this->session->userdata('user_id').'"');
        $create = $this->db->insert("dmd_integrations", $data);
        if ($create) {
            $msg = "Data has beed saved successfully!";
            $this->session->set_flashdata('success_msg', $msg);
            redirect('store/integration/mailchimp');
        } else {
            $msg = "Please try again!";
            $this->session->set_flashdata('error_msg', $msg);
            redirect('store/integration/mailchimp');
        }

    }
    public function getmailchimpLists()
    {
        //$params = array('apikey' => '70e71970347b8e9e1eac1637eaf6e9e6-us19');
        // dummy list
        /*$lists = array(array('id' => "2", "name" => "test"));
        $list_html = '<select name="chimpPost[listid]" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="select list">';
        $list_html .= '<option value="" class="form-input form-input-lg border-dark-1">select list</option>';
        foreach ($lists as $listdata) {

            $selected = "";
            if ($_REQUEST['listid'] != '' && $_REQUEST['listid'] == $listdata['id']) {
                $selected = 'selected';
            }
            $list_html .= '<option ' . $selected . '  value="' . $listdata['id'] .
                '" class="form-input form-input-lg border-dark-1">' . $listdata['name'] .
                '</option>';
        }
        echo $list_html .= '</select>';
        die();*/
        
        $lists = $this->mailchimp->get('lists');
        /*echo '<pre>';
        print_r($lists);
        echo '</pre>';*/
        //die();
        if (isset($lists)) {
            $list_html = '<select name="chimpPost[listid]" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="select list">';
            $list_html .= '<option value="" class="form-input form-input-lg border-dark-1">select list</option>';
            foreach ($lists['lists'] as $listdata) {
                $selected = "";
                if ($_REQUEST['listid'] != '' && $_REQUEST['listid'] == $listdata['id']) {
                    $selected = 'selected';
                }
                $list_html .= '<option ' . $selected . '  value="' . $listdata['id'] .
                '" class="form-input form-input-lg border-dark-1">' . $listdata['name'] .
                '</option>';
            }
            echo $list_html .= '</select>';


        }
        if (isset($lists['error'])) {
            echo $lists['error'];
        }
    }

    public function checkoutPage()
    {

        $this->load->view('front_pages/dashboard/dash_header');
        $store_id = $this->session->userdata('store_id');
        $this->db->select("*");
        $this->db->from('checkout_customization');
        $this->db->where('store_id', $store_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['data'] = $query->row_array();
        } else {
            $data['data'] = array();
        }

        $this->load->view('front_pages/dashboard/checkout_customization', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function customize()
    {

        $this->load->view('front_pages/dashboard/dash_header');
        $store_id = $this->session->userdata('store_id');

        $data['store_customize'] = $this->store_model->get_store_customization($store_id);

        $this->load->view('front_pages/dashboard/customize_store_home', $data);
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function update_customization()
    {
        $store_customize = $_REQUEST['store'];
        // check fields validation
        if ($this->user_model->check_memory()) {
            $empty_message = "";
            /*
            if ($_REQUEST['old_file'] == "") {
                if ($_FILES['userfile']['name'] == '') {
                    $empty_message .= "File is required field<br>";
                }

            }*/


            /*
            if ($empty_message != "") {
                $this->session->set_flashdata('error_msg', $empty_message);
                redirect('store/customize/');
            } */
            // upload file
            if($_FILES['logo']['name']){
                if ($_REQUEST['old_file'] == "") {
                    // $name_parts = pathinfo($_FILES['logo']['name']);

                    // $name_full = preg_replace('/\s+/', '', $name_parts['logo']);

                    // $file_ext = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
                    // $file_name = uniqid() . '.' . $file_ext;

                //$file_name = uniqid() . '-' . $name_full;
                    $config = array(
                        'upload_path' => './site_assets/stores',
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'file_name' => time().(preg_replace('/[^A-Za-z0-9]/', "", $_FILES["logo"]['name'])),
                        'max_size' => ($this->user_model->check_memory("size")*1024),
                        'overwrite' => false,
                    );

                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('logo')) {
                        $upload_data = $this->upload->data();
                        $data_ary = array(
                            'title' => $upload_data['client_name'],
                            'file' => $upload_data['file_name'],
                            'width' => $upload_data['image_width'],
                            'height' => $upload_data['image_height'],
                            'type' => $upload_data['image_type'],
                            'size' => $upload_data['file_size'],
                            'path' => $upload_data['full_path'],
                            'date' => time(),
                        );

                        $data = array('upload_data' => $data_ary);
                        $old_path = getcwd() . '/site_assets/stores/' . $_REQUEST['old_image_path'];
                        unlink($old_path);

                        $store_customize['banner_image'] = $data_ary['file'];
                    } else {
                    //print_r($this->upload->display_errors());
                        $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                        redirect('store/customize/');

                    }
                } else {
                    $store_customize['image'] = $_REQUEST['old_file'];
                } }

        //$store_customize['display'] = $_REQUEST['display'];

                $store_id = $this->session->userdata('store_id');
      //  $store_customize['store_id'] = $store_id;
                $this->store_model->update_store_customization($store_customize, $store_id);
                $this->session->set_flashdata('success_msg', 'successfully updated!');
                redirect('store/customize/');
            } else {
                $this->session->set_flashdata('error_msg_limit',
                    'Your memory limit exceeded please upgrade your package Thanks');

                redirect('user/subscription');

            }


        }
        public function emailTemplates($id = "")
        {
            $this->load->view('front_pages/dashboard/dash_header');
            if ($id != "") {
                $data["edit_email"] = $this->store_model->email_templete_main($id);
                $this->load->view('front_pages/dashboard/edit_email_template', $data);
            } else {
                $data["emails"] = $this->store_model->email_templete_main();
                $this->load->view('front_pages/dashboard/email_template', $data);
            }
            $this->load->view('front_pages/dashboard/dash_footer');
        }

        public function coupon_search()
        {
            if ($this->input->post()) {
                $c_name = $this->input->post('cName');
                $c_status = $this->input->post('cStatus');

                $data = $this->store_model->searchCoupon('dmd_coupon', $c_name, $c_status);
            //print_r($data);
                $string = "";
                if ($data) {
                    foreach ($data as $key => $value) {

                        $string .= "<tr>";
                        $string .= "<td>" . $value['code'] . "</td>";
                        $string .= "<td>" . $value['discount'] . "</td>";
                        $string .= "<td>" . $value['name'] . "</td>";
                        $string .= "<td>" . $value['active_date'] . "</td>";
                        $string .= "<td>" . $value['expire_date'] . "</td>";
                        $string .= "<td>" . $value['max_use'] . "</td>";
                        $string .= "<td><a href='" . base_url('store/coupons/edit') . '/' . $value['id'] . "'>EDIT</a></td>";
                        $string .= "</tr>";

                    }
                    print_r($string);
                } else {
                    print_r("No coupon found");
                }
            }

        }

        public function couponShowAll()
        {
            if ($this->input->post("d")) {
                $user_id = $this->session->userdata("user_id");
                $data = $this->store_model->get_coupons('dmd_coupon', 'user_id', $user_id);
            //print_r($data);
                $string = "";
                if ($data) {
                    foreach ($data as $key => $value) {

                        $string .= "<tr>";
                        $string .= "<td>" . $value['code'] . "</td>";
                        $string .= "<td>" . $value['discount'] . "</td>";
                        $string .= "<td>" . $value['name'] . "</td>";
                        $string .= "<td>" . $value['active_date'] . "</td>";
                        $string .= "<td>" . $value['expire_date'] . "</td>";
                        $string .= "<td>" . $value['max_use'] . "</td>";
                        $string .= "<td><a href='" . base_url('store/coupons/edit') . '/' . $value['id'] . "'>EDIT</a></td>";
                        $string .= "</tr>";

                    }
                    print_r($string);
                } else {
                    print_r("No coupon found");
                }
            }
        }

        public function createPM($pm = "")
        {
            if (!$pm == "") {
                if ($this->input->post()) {

                /*echo "<pre>";
                print_r($this->input->post());
                */
                $data["user_id"] = $this->session->userdata('user_id');
                $data["store_id"] = $this->session->userdata("store_id");
                $data["name"] = $pm;

                if (empty($this->input->post("status"))) {
                    $data["status"] = "0";
                } else {
                    $data["status"] = "1";
                }

                if (empty($this->input->post("default"))) {
                    $data["default"] = "0";
                } else {
                    $data["default"] = "1";
                }


                //prepare configration
                if (!empty($this->input->post("apiKey"))) {
                    $config["apiKey"] = $this->input->post("apiKey");
                }

                if (!empty($this->input->post("publishableApiKey"))) {
                    $config["publishableApiKey"] = $this->input->post("publishableApiKey");
                }

                if (!empty($this->input->post("country"))) {
                    $config["country"] = $this->input->post("country");
                }

                if (!empty($this->input->post("loginID"))) {
                    $config["loginID"] = $this->input->post("loginID");
                }

                if (!empty($this->input->post("transactionKey"))) {
                    $config["transactionKey"] = $this->input->post("transactionKey");
                }

                if (!empty($this->input->post("checkoutLabel"))) {
                    $config["checkoutLabel"] = $this->input->post("checkoutLabel");
                }

                if (!empty($this->input->post("visa"))) {
                    $config["visa"] = "1";
                } else {
                    $config["visa"] = "0";
                }

                if (!empty($this->input->post("mastercard"))) {
                    $config["mastercard"] = "1";
                } else {
                    $config["mastercard"] = "0";
                }

                if (!empty($this->input->post("ae"))) {
                    $config["americanExpress"] = "1";
                } else {
                    $config["americanExpress"] = "0";
                }

                if (!empty($this->input->post("dc"))) {
                    $config["dinerClub"] = "1";
                } else {
                    $config["dinerClub"] = "0";
                }

                if (!empty($this->input->post("paypalEmail"))) {
                    $config["paypalEmail"] = $this->input->post("paypalEmail");
                }

                if (!empty($this->input->post("paypalEmail"))) {
                    //$config["paypalEmail"] = $this->input->post("paypalEmail");
                }

                $data["configuration"] = json_encode($config);

                /*echo "<pre>";
                print_r($data);
                */

                if ($this->input->post('pm_id')) {
                    $result = $this->store_model->update_pm($this->input->post('pm_id'), $data);
                    if ($result) {
                        $msg = "Payment method has been updated successfully!";
                    } else {
                        $msg = "Payment method has been failed to update!";
                    }
                } else {
                    $result = $this->store_model->create_pm($data);
                    if ($result) {
                        $msg = "Payment method has been successfully added!";
                    } else {
                        $msg = "Payment method has been failed to create!";
                    }
                }
                $this->session->set_flashdata('create_pm', $msg);
                redirect('/store/paymentMethod/' . $pm);
            }
        }
    }

    public function editEmailTemplete($id = "")
    {
        if (!empty($id)) {
            if ($this->input->post()) {
                $data = $this->input->post();

                $result = $this->store_model->update_email($id, $data);

                if ($result) {
                    $this->session->set_flashdata("emailMsg", "Email has been updated.");
                } else {
                    $this->session->set_flashdata("emailMsg", "Email has been failed to update.");
                }

                redirect("/store/emailTemplates/" . $id);
                //print_r($data);
            }
        }
    }

   /* public function createNewEmail()
    {
        
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/new_email_templete');
        $this->load->view('front_pages/dashboard/dash_footer');
    }*/
    public function mailchimp()
    {
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/mailchimp_configuration');
        $this->load->view('front_pages/dashboard/dash_footer');
    }
    public function iconnect()
    {
        $this->load->view('front_pages/dashboard/dash_header');
        $this->load->view('front_pages/dashboard/iconnect_configuration');
        $this->load->view('front_pages/dashboard/dash_footer');
    }

    public function NewEmailTemplete()
    {
        if ($this->input->post()) {
            $data = $this->input->post();

            $result = $this->store_model->create_email($data);

            if ($result) {
                $this->session->set_flashdata("emailMsg", "New email template has been created.");
            } else {
                $this->session->set_flashdata("emailMsg",
                    "New email template has been failed to create.");
            }

            redirect("/store/emailTemplates");
            //print_r($data);
        }
    }

    public function delete_email($id = "")
    {
        if (!empty($id)) {
            $result = $this->store_model->delete_email($id);
            if ($result) {
                $this->session->set_flashdata('emailMsg', 'Email template has been deleted.');
            } else {
                $this->session->set_flashdata('emailMsg',
                    'Email template has been failed to delete.');
            }
            redirect("/store/emailTemplates");
        }
    }

    public function check_subdomain()
    {
        if($this->input->post('subdomain')){
            $subdomain = $this->input->post('subdomain');
            $subdomain = $subdomain . "." . $_SERVER["HTTP_HOST"];
            $idv = $this->input->post('thisid');
            $store_check = $this->store_model->store_check($subdomain,$idv);
            if($store_check){
                echo "<span class='store-status-text-success'>Available</span>";
            }else{
                //if store exist
               echo "<span class='store-status-text-danger'>Not Available</span>";
           }
       }
   }

   public function delete_store()
   {
    $user_id = $this->session->userdata('user_id');
    $store_id = $this->session->userdata('store_id');
    $data = $this->input->post('delete');

    if($data == 'delete'){
        $result = $this->user_model->delete_store($user_id,$store_id);
        if($result){
            $next_store = $this->db->get_where('dmd_stores', ['user_id' => $user_id], 1)->row_array();
            $store_check = $this->db->query('select id,store_name,store_url from dmd_stores where id="'.$next_store['id'].'"');
            if($store_check->num_rows()>0){  
                $store_res = $store_check->row_array();
                $this->session->set_userdata('store_id', $store_res['id']);
                $this->session->set_userdata('store_name', $store_res['store_name']);
                $this->session->set_userdata('store_url', $store_res['store_url']);
                $this->session->set_flashdata('success_msg', 'Store has been deleted successfully!');
            }
            echo 'deleted';
        }
    }
}

}
