<!DOCTYPE html>
<html lang="en">
<head>
	<title>DMD</title>
	<meta charset="utf-8">	
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>site_assets/images/favicon.png"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- CSS ////////////////////////////////////////////////// -->
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap-grid.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap-reboot.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/home.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/barchart.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/editor.css" type="text/css" rel="stylesheet"/>

	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- font-family: 'Montserrat', sans-serif; -->

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- JS ///////////////////////////////////////////////////-->
	
	<script src="<?php echo base_url(); ?>site_assets/js/jquery.min.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/editor.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/custom.js"></script>

</head>
<?php
// get order detail
                $order = $this->db->query('select ord.*,dmd_order_items.*,dmd_order_items.coupon_discount as pdiscount,dmd_store_products.product_name,dmd_store_products.pro_image,dmd_stores.store_name from dmd_orders as ord left join dmd_order_items on(dmd_order_items.order_id=ord.id)
                left join dmd_store_products on(dmd_order_items.product_id=dmd_store_products.product_code)
                left join dmd_stores on(dmd_stores.id=ord.store_id) where ord.order_number="'.$order_number.'"');
                $order = $order->result_array();
                
?>
<body>  
	<div class="container store">
		<div class="row header">
			<div class ="col-md-4 text-left store-head text-center">
				<h4 class="heading-lg-red"><?php echo $order[0]['store_name']; ?></h4>
			</div>

			
		</div>

		<div class="row store-body mx-auto">
			
			<div class="col-md-12 store-inner-heading">	
				<h4>Your Order</h4>

			</div>

			<div class="col-md-12">
				<table class="table store-table">
                <tbody>
                <?php
                foreach($order as $ord){
                    if($ord['pdiscount']==""){
                        $ord['pdiscount'] = 0;
                    }
                   ?>
                     <tr>
							<td class="store-order-image"><img src="<?php echo base_url(); ?>site_assets/products/<?php echo $ord['pro_image'];?>" /></td>
							<td class="store-order-product-name td-info"><?php echo $ord['product_name'];?></td>
							<td class="store-order-product-discount td-info"><?php echo $ord['pdiscount']."%";?></td>
                            <td class="store-order-product-price td-info"><?php echo $ord['product_price'];?></td>
							<td class="store-order-product-quantity td-info"><?php echo $ord['product_quantity'];?></td>
                            
                            <td class="store-order-product-total td-info">$<?php echo $ord['total_price'];?></td>
				    </tr>
                   <?php 
                }
                
                ?>
					</tbody>
				</table>
			</div>

			<div class="col-md-7 text-left">	
				
			</div>
			<div class="col-md-5 text-right store-cart text-right">
				<div class="list">
				<table class="table store-cart-table">
					<tbody>
						<tr class="cart-data">
							<td>Subtotal:</td>
							<td>$<?php echo $order[0]['total_amount']; ?></td>
						</tr>
						<!--<tr class="cart-data">
							<td>Tax (19%):</td>
							<td>$23.5</td>
						</tr>
						<tr class="cart-data">
							<td>Coupon (10%):</td>
							<td>$-240</td>
						</tr>-->
						<tr class="cart-footer">
							<td>Total:</td>
							<td>$<?php echo $order[0]['grand_total']; ?></td>
						</tr>
						
					</tbody>
				</table>
				</div>
			</div>

		</div>
	</div>
	<div class="container-fluid store-footer">
		<div class="row store-footer-row">
			<div class="col-md-6  footer-left max-auto store-footer-left">
				<div class="nauk-info-connections text-left">
					<p class="store-footer-links"><a href="#"  class="store-footer-links"> Refund Policy</a> | <a href="#"  class="store-footer-links"> Privacy Policy</a></p>
					<p  class="store-footer-links">Copyright (c) 2018 ((<?php echo $order[0]['store_name']; ?>))</p>
				</div>
			</div>

			<div class="col-md-6  footer-right max-auto store-footer-right">
				<div class="nauk-info-connections text-left">
					<p class="store-footer-links">Store powered by Digital Media Publishing</p>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
