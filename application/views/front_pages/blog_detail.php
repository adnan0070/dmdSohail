<div class="row main-body blogs-detail-page mx-auto ">
	<div class="col-md-12 blog-head text-left">
		<h3 class="heading-lg-green"> Our Latest Blogs</h3>
	</div>

	<div class="col-md-9 blog-body">
		<div class="row blog-detail">
			<div class="col-md-12 blog-detail-image">
				<div class="nauk-info-connections blog-image text-center">
					<img src="<?php echo base_url('site_assets/articles/').$blog['file'];?>">
				</div>
			</div>
			<div class="col-md-12">
				<div class="nauk-info-connections text-left">
					<p class="heading-lg-green"><?php echo $blog['title']; ?></p>
				</div>
			</div>
			<div class="col-md-12 blog-detail-description">
				<?php echo $blog['content']; ?>
			</div>
		</div>
	</div>

	<div class="col-md-3 blog-left">
		<div class="row blog-categories">
			<div class="col-md-12"  style="padding:0px;">
				<p class="heading-md-blue">Categories</p>
			</div>
			<div class="col-md-12"  style="padding-left:25px;">
				<ul>
					<li class="">
						<a  class="hvr-bounce-to-right" href="<?php echo base_url('blog/blogs_with_categories') ?>">
							All
						</a>
					</li>
					<?php foreach($categories as $category): ?>
						<li class="<?php if ($blog['Category_id'] == $category['id']){ echo "active"; } ?>"> 
							<a  class="hvr-bounce-to-right" href="<?php echo base_url('blog/blogs_with_categories/'.$category['id']) ?>">
								<?php echo $category['title']; ?>

							</a>
						</li>
					<?php endforeach;?>
				</ul>
			</div>
		</div>
	</div>
</div>