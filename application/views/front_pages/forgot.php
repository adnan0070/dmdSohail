         <div class="row text-center section-login-signup border-dark-1">
          <div class="col-md-12">
            <?php
            $success_msg= $this->session->flashdata('success_msg');
            $error_msg= $this->session->flashdata('error_msg');

            if($success_msg){
             ?>
             <div class="alert alert-success">
              <?php echo $success_msg; ?>
            </div>
            <?php
          }
          if($error_msg){
            ?>
            <div class="alert alert-danger">
              <?php echo $error_msg; ?>
            </div>
            <?php
          }
          ?>
        </div>      
        <div class="col-md-12">
          <h2 class="heading-lg-green">Please enter your email to receive instructions on resetting the password.</h2>
          <br> 
        </div>

        <div class="col-md-12">
          <form role="form" method="post" action="<?php echo base_url('user/ForgotPassword'); ?>">

           <div class="col-md-12">
            <div class="form-group input-effects">
              <input type="email"  name = "email" class = "home-input" value="" id="email" placeholder=""/>
             <label>E-mail address</label>
             <span class="focus-border"></span>
             <!--<span class="text-danger">Validation error</span>-->
           </div>
         </div> 


         <div class="form-footer">
          <input class="btn-lg-default btn" type="submit" id="forgot" value="Send password reset link" name="forgot" >
        </div>
      </form>
    </div>
  </div>