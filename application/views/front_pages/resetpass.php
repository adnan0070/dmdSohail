<div class="row text-center section-login-signup border-dark-1">
	<div class="col-md-12">
		<?php
		$error_msg = $this->session->flashdata('error_msg');
		if ($error_msg) {
			echo $error_msg;
		}
		?>
	</div>
	<div class="col-md-12">
		<h2 class="heading-lg-green">want to launch your  </h2>
		<h2 class="heading-lg-green"> Digital Media?</h2>
		<br>
	</div>
	<div class="col-md-12">  
		<form method="post" action="<?php echo base_url('user/resetPass/'); ?>">
			<div class="col-md-12">
				<div class="form-group input-effects">
					<input type="hidden" name="email" value="<?php echo $email; ?>" />
					<input type="hidden" name="vcode" value="<?php echo $vcode; ?>" />
					<input type="password" 
					name ="password" 
					class = "home-input" 
					id="password"  
					required 
					placeholder=""
					pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
					title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
					/>
					<label>password</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div> 
			<div class="col-md-12">
				<div class="form-group input-effects">
					<input type="password" 
					onkeyup='check();' 
					class = "home-input" 
					id="conf-password" 
					required 
					placeholder=""
					pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
					title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
					/>
					<label>re-type password</label>
					<span class="focus-border"></span>
					<span class="" id='pass_message'></span>
				</div>
			</div> 

			

			<div class="form-footer">
				<input class="btn-lg-default btn" type="submit" id="register" value="Reset Password" name="reset_pass" >
			</div>
		</form>
	</div>
</div>
<script>
	var check = function() {
		if (document.getElementById('password').value ==
			document.getElementById('conf-password').value) {
			document.getElementById('pass_message').style.color = 'green';
		document.getElementById('pass_message').innerHTML = 'matching';
	} else {
		document.getElementById('pass_message').style.color = 'red';
		document.getElementById('pass_message').innerHTML = 'not matching';
	}
}
</script>       