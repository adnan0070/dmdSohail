<div class="row text-center section-login-signup border-dark-1">
  <?php
  $email_verify = $this->session->flashdata('email_verify');
  if($email_verify=='email_verify'){
    ?>
    <div class="col-md-12">
      <h2 class="heading-lg-green">Thank you for activating your account,</h2>
    </div>
    <div class="col-md-12"> 
      <?php
      $success_msg = $this->session->flashdata('success_msg');
      if ($success_msg) {
        echo $success_msg;
      }
      ?> 
      <div class="form-footer">
        <a href="/user/login" class="btn-lg-default btn">Login</a>

      </div>

    </div>
    <div class="col-md-12 text-center">
     <p class="heading-sm-grey" >Do not have an Account?
      <a class="heading-sm-grey" href ="<?php echo base_url('user/register'); ?>"> Create one here</a>
    </p>
  </div> 
  <?php
}
else{
  ?>
  <div class="col-md-12">
    <h2 class="heading-lg-green">Thank you for signing up!</h2>
  </div>
  <div class="col-md-12"> 
    <?php
    $success_msg = $this->session->flashdata('success_msg');
    if ($success_msg) {
      echo $success_msg;
    }
    ?> 
    <div class="form-footer">
     <form method="post" action="<?php echo base_url('user/resendThankEmail/'); ?>">
      <input type="hidden" name="email" value="<?php echo $this->session->flashdata('email'); ?>" />
      <input type="submit" value="Resend E-mail" class="btn-lg-default btn"/>
    </form>
  </div>
  <center><b>Already registered ?</b>&nbsp; &nbsp;<a href="<?php echo
  base_url('user/login'); ?>">Login here</a></center>
  <br><br>
</div>
<?php  
}
?>

</div>
<script>
 var check = function() {
  if (document.getElementById('password').value ==
    document.getElementById('conf-password').value) {
    document.getElementById('pass_message').style.color = 'green';
  document.getElementById('pass_message').innerHTML = 'matching';
} else {
  document.getElementById('pass_message').style.color = 'red';
  document.getElementById('pass_message').innerHTML = 'not matching';
}
}
</script>       