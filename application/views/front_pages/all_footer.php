<div class="nauk-info-connections text-left">
	<h4> About Digital Media Deliveries</h4>
	<p> 
		<a  href="<?php echo base_url('pages/front/about_us');?>">About us</a>&nbsp;/&nbsp;
		<a  href="<?php echo base_url('pages/front/support');?>">Support</a>&nbsp;/&nbsp;
		<a  href="<?php echo base_url('pages/show_teams');?>">Our Team</a>&nbsp;/&nbsp;
		<a  href="<?php echo base_url('user/sitemap');?>">Site Map</a>&nbsp;&nbsp;
		<!--	<a  href="<?php echo base_url('pages/front/affiliate_program');?>">Affiliate Program</a>-->
	</p>

	<h4>Social Media</h4>
	<p> 
		<a target="_blank" href="https://www.facebook.com/DigitalMediaDeliveries">Facebook</a>&nbsp;/&nbsp;
		<a target="_blank" href="">Linkedin</a>&nbsp;/&nbsp;
		<a target="_blank" href="https://www.twitter.com/digimeddelivery">Twitter</a>&nbsp;/&nbsp;
		<a target="_blank" href="https://www.instagram.com/digitalmediadeliveries/">Instagram</a>
	</p>

	<h4>Legal Links</h4>
	<p> 
		<a href="<?php echo base_url('pages/front/legal_notice');?>">Legal Notice</a>&nbsp;/
		<a  href="<?php echo base_url('user/faq');?>">FAQs</a>&nbsp;/&nbsp;
		<a  href="<?php echo base_url('pages/front/terms_service');?>">Terms of Service</a>
		<!-- <a  href="<?php echo base_url('pages/front/dmca_notice');?>">DMCA</a>&nbsp;/&nbsp; -->

	</p>

</div>

<script type="text/javascript">
	function ozfunction(e){
		var base = '<?php echo base_url();?>';
		var method = "pages/front";
		var d = {"id":e};
		if(e == "show_teams"){
			method = "pages/show_teams";
			d = {"showTeam":e};
		}
	//console.log(d);
	$.ajax({
		type: "POST",
		url: base+method,
		data:d,
		success: (function(response){
			//console.log(response);
			$('.static-content').html(response);
			//$('.static-content').fadeIn(5000);
		})   
	});
}
</script>