
</style>

<?php if($this->session->flashdata('error_msg')){ ?>
	<div class="row section-login-signup">
		<div class="col-md-12">
			<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error! </strong><?php echo $this->session->flashdata('error_msg'); ?>
			</div>
		</div>
	</div>
<?php } ?>

<?php echo form_open('user/register_user/'.$selected_package); ?>
<div class="row text-center section-login-signup border-dark-1">

	<div class="col-md-12">
		<h2 class="heading-lg-green">Ready to launch your </h2>
		<h2 class="heading-lg-green"> Digital Media?</h2>
		<br>
	</div> 

	<div class="col-md-12">
		<div class="form-group input-effects">
			<input type="text"  name = "firstName" value="<?php echo $this->input->post('firstName'); ?>" class = "home-input" id="first-name" placeholder=""/>
			<label>First name</label>
			<span class="focus-border"></span>
			<span class="text-danger"><?php echo form_error('firstName');?></span>
		</div>
	</div> 
	<div class="col-md-12">
		<div class="form-group input-effects">
			<input type="text"  name = "lastName"  value="<?php echo $this->input->post('lastName'); ?>" class = "home-input" id="last-name" placeholder=""/>
			<label>Last name</label>
			<span class="focus-border"></span>
			<span class="text-danger"><?php echo form_error('lastName');?></span>
		</div>
	</div> 
	<div class="col-md-12">
		<div class="form-group input-effects">
			<input type="email"  name = "email"  value="<?php echo $this->input->post('email'); ?>"  class = "home-input" id="email" placeholder=" "/>
			<label>E-mail address</label>
			<span class="focus-border"></span>
			<span class="text-danger"><?php echo form_error('email');?></span>
		</div>
	</div> 

	<div class="col-md-12">
		<p class="paragraph-text-xs-grey">Password must be at least 8 characters and include at least 1 uppercase letter, 1 lowercase letter and 1 number.</p>
		<div class="form-group input-effects">
			<input type="password"  
			name="password" 
			class = "home-input" 
			id="password" 
			placeholder=""
			pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
			title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
			/>
			<label>password</label>
			<span class="focus-border"></span>
			<span class="text-danger"><?php echo form_error('password');?></span>
		</div>
	</div> 
	<div class="col-md-12">
		<div class="form-group input-effects">
			<input type="password"  
			name = "confPassword"  
			class = "home-input" 
			id="conf-password"
			placeholder=""
			pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
			title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
			/>
			<label>re-type password</label>
			<span class="focus-border"></span>
			<span class="" id='pass_message'></span>
			<span class="text-danger"><?php echo form_error('confPassword');?></span>
		</div>
	</div> 

	<div class="col-md-12 text-center">
		<p class="heading-sm-grey">By clicking create account, You agree to </p>
		<p class="heading-sm-grey">the <a class="heading-sm-grey" href="<?php echo base_url('static_views/termsOfService');?>" >Term of Services</a>  of Digital Media Deliveries</p>
	</div> 

	<div class="col-md-12 text-center form-footer">
		<input class="btn-lg-default btn" type="submit" id="register" value="Create Account" name="register" >

		<br>
		<b>Already registered ?</b>&nbsp; &nbsp;<a class="paragraph-text-md-black-blod" href="<?php echo base_url('user/login'); ?>">Login Here</a>

	</div>

</div>
<?php echo form_close(); ?>
<script>

	$("#conf-password").on("keyup", function(e){
		var pass = $("#password").val();
		var msg = $("#pass_message");

		if (pass) {
			if($(this).val() == pass){
				msg.html("matching").css("color","green");
			}else if ($(this).val() == ""){
				msg.html("");
			}else{
				msg.html("not matching").css("color","red");
			}
		}else{	
			msg.html("write password first").css("color","red");
		}

	});

</script>       