
<div class="row main-body home-features packages-row mx-auto ">
	<div class="col-md-12 text-center">
		
		<h3 class="heading-xl-green">Frequently Asked Questions</h3>
		<br>
	</div>
</div>

<div class="row  packages-row">


	<div class="pull-left col-12col-xs-12 col-sm-12 col-md-12 col-sm-12 col-xs-12">

		<div class="faq-section">
			<button class="faq-accordion">Am I tied into a Contract?</button>
			<div class="faq-panel">
				<p class="paragraph-text-md-grey">you can cancel your Subscription at any time.</p>
			</div>
			<button class="faq-accordion">Can I Upgrade /Downgrade my Subscription?</button>
			<div class="faq-panel">
				<p class="paragraph-text-md-grey">You can most certainly upgrade at any time by paying the upgrade fee to the new Subscription level you need. Downgrades will take effect at your next monthly renewal. </p>
			</div>

			<button class="faq-accordion">Can I get a refund if I cancel?</button>
			<div class="faq-panel">
				<p class="paragraph-text-md-grey">Unfortunately not, as it costs us in administration and also any storage space used. </p>
			</div>

			<button class="faq-accordion">Can I upload and sell anything I want?</button>
			<div class="faq-panel">
				<p class="paragraph-text-md-grey"> Our platform is a great place for any kind of Digital Media, however, please do not upload any content that is illegal, of an adult nature (pornographic), slanderous, libellous, offensive to anyone whatsoever or generally distasteful as it will be removed from our system.  </p>
				<p class="paragraph-text-md-grey">Also, only upload and sell Media that you have the right to sell. Whether it is your own Media or licenced – illegal and improper Media will be removed if it infringes Copyright or Intellectual Property laws.  </p>
			</div>

			<button class="faq-accordion">Can I promote Digital Media Deliveries to earn Commissions?</button>
			<div class="faq-panel">
				<p class="paragraph-text-md-grey">Absolutely! Sign up to our Affiliate Programme Here – and you can earn 25% commission for 6 months of the life of any new Subscribers you bring to Digital Media Deliveries. Please note, commissions will only be paid for either 6 months or, until the Subscriber cancels their Subscription, whichever is sooner. </p>
			</div>

		</div>
	</div>
</div>
<script>
	var acc = document.getElementsByClassName("faq-accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", function() {
			this.classList.toggle("faq-active");
			var panel = this.nextElementSibling;
			if (panel.style.maxHeight){
				panel.style.maxHeight = null;
			} else {
				panel.style.maxHeight = panel.scrollHeight + "px";
			} 
		});
	}
</script>
<br> <br>
<br>
<br>