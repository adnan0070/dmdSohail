		<div class="container home-container">
			<div class="row main-body home-features features-row-1 mx-auto ">

				<div class="col-md-12 text-center">
					<br>
					<br>
					<h3 class="heading-xl-green">Join Our Platform Today</h3>

					<p class="paragraph-text-lg-blue text-center">
						Our software makes it extremely simple for you to launch your digital products immediately… 
					</p>
					<br>
					<br>
					<br>
				</div>

<!-- <?php echo base_url('site_assets/images/5c18188e0a7a3.mp4');?> -->
				<div class="col-md-12 text-center">
					<video class="img-responsive" src="<?php echo base_url('site_assets/video/5d5ecd63496e5.mp4'); ?>"  controlsList="nodownload" controls width="100%" style="max-width:850px; width:100%;" id="active-video"></video> 
				</div>

				<br>
				<div class="col-md-12 text-center">
					<a href="<?php echo base_url('blog/features');?>" class="btn-lg-default btn">Click To Find Out More</a> 
				</div>
				

			</div>

		</div>



		

		<script src="https://cdnjs.cloudflare.com/ajax/libs/vissense/0.10.0/vissense.min.js"></script>
		<script>
			var myVideo = document.getElementById('active-video');

			var videoElementArea = VisSense(myVideo);

			var monitorBuilder = VisSense.VisMon.Builder(videoElementArea);

			monitorBuilder.on('fullyvisible', function() {
				myVideo.play();  
			});
			monitorBuilder.on('hidden', function() {
				myVideo.pause();  
			});
			var videoVisibilityMonitor = monitorBuilder.build();
			videoVisibilityMonitor.start();


		</script>