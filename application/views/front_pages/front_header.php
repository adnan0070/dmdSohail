<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$user_id  = $this->session->userdata('user_id');
$store_id = $this->session->userdata('store_id');
$logo_url = base_url();
if(isset($user_id)&&$user_id!=""){
	$flag = "true";
	$logo_url = "user/dashboard";
	
}
$display_name = $this->session->userdata('user_name');

$user_role = $this->session->userdata('user_role');
$dash_link = '/user/dashboard';
if ($user_role == '2') {
	$controller = "admin";
	$dash_link = '/admin/dashboard';
	$logo_url = "/admin/dashboard";
}



?><!DOCTYPE html>
<html lang="en">
<head>
	<script>
		

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-147349522-1', 'auto');

		// ga("require", "ec");
		
		ga('require', 'ecommerce');


	</script>

	<title>Digital Media Deliveries</title>
	<meta charset="utf-8">	
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>site_assets/images/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- metatags -->
	<?php $metaTags = getMetatags(); 
	foreach($metaTags as $row){
		echo $row['content'];
	}
	?>
	<!-- metatags --> 

	<!-- CSS ////////////////////////////////////////////////// -->
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap-grid.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap-reboot.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/home.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/style.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/responsive.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/datepicker/css/bootstrap-datepicker.standalone.css" rel="stylesheet">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>site_assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>site_assets/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!--<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">-->
	<!-- font-family: 'Montserrat', sans-serif; -->

	<!-- JS ///////////////////////////////////////////////////-->

	<link rel="stylesheet" href="https://use.typekit.net/cpc3oyl.css"> 

	<script src="<?php echo base_url(); ?>site_assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/topbutton.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/events.js?v=2"></script>

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/sl-1.2.6/datatables.min.css"/>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/sl-1.2.6/datatables.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/owl.carousel.min.js"></script>
	<!-- Tapfiliation-->
	<script src="https://script.tapfiliate.com/tapfiliate.js" type="text/javascript" async></script>
	<script type="text/javascript">
		(function(t,a,p){t.TapfiliateObject=a;t[a]=t[a]||function(){ (t[a].q=t[a].q||[]).push(arguments)}})(window,'tap');

		tap('create', '9351-9a1558');

	</script>


	<!-- Tapfiliation-->

	<!-- tracking -->
	<?php $trackers = getTracking(); 
	foreach($trackers as $row){
		echo $row['header_content'];
	}
	?>
	<!-- tracking --> 

</head>

<body>  
	<!-- tracking --> 
	<?php  
	foreach($trackers as $row){
		echo $row['body_content'];
	}
	?>
	<!-- tracking --> 
	<!--<div class="se-pre-con"></div>-->
	<script>
		/*
		$(window).load(function() {
			$(".se-pre-con").fadeOut(1000);
		});
		*/
		$(document).ready(function(){
			$.topbutton({
				html : "<i>Top</i>",      
                css : "width:50px; height:50px; background:#468fb1; border:none; font-size:20px;",  //String
                scrollAndShow : true,   
                scrollSpeed : 1000        
            });

		});
		//tap affiliate
		tap('click');
	</script>

	<?php 
	if(isset($homepage)){
		$header_main_style= " ";
		$header_row_style= " home-header-2 ";
	}else{
		$header_main_style= " bg-header-pricing ";
		$header_row_style= " row-stretched ";
	} 
	?>

	<!-- Start of HubSpot Embed Code -->
	<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/6277315.js"></script>
	<!-- End of HubSpot Embed Code -->




	<div class="container-fluid home-container bg-header-section <?php echo $header_main_style; ?>">
		<div class="toggle-menu">
			<span class="toggle-btn" onclick="menuOpen()">
				<i class="menu-icon fa fa-bars"></i>
			</span> 
		</div>
		<div class="row home-header <?php echo $header_row_style; ?>">
			<div class="pull-left col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2  col-space text-center home-logo">
				<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>site_assets/images/logo.png"></a>
			</div>
			
			<div class ="pull-left col-10 col-xs-10 col-sm-10 col-md-10 col-lg-10 col-space text-right home-menu">
				<div id="menu-option">
					<ul class="list-inline">
						<div class="toggle-menu">
							<span class="toggle-btn" onclick="menuClose()">
								<i class="menu-icon  fa fa-times"></i>
							</span>
						</div>
						<li class="list-inline-item item-link"><a class="" href="<?php echo base_url(); ?>">Home</a></li>
						<li class="list-inline-item item-link"><a href="<?php echo base_url('pages/front/about_us');?>">about</a></li>
						<li class="list-inline-item item-link"><a href="<?php echo base_url('blog/features');?>">features</a></li>
						<li class="list-inline-item item-link"><a href="<?php echo base_url('user/pricing');?>">pricing</a></li>
						<li class="list-inline-item item-link"><a href="<?php echo base_url('user/demo');?>">demo</a></li>
						<li class="list-inline-item item-link"><a href="<?php echo base_url('blog/blogs_with_categories') ?>">blog</a></li>
						<li class="list-inline-item item-link"><a href="<?php echo base_url('user/resources');?>">Resources</a></li>
						<?php
						if(!isset($flag)){
							?>
							<li class="list-inline-item item-link"><a href="/user/login">login</a></li>
							<li class="list-inline-item"><a class="btn-sm-default btn pricing-btn text-uppercase" href="/user/register">get started</a></li>
							<?php    
						}
						if(@$flag){
							?>
							
							<li class="list-inline-item item-link"><a href="<?php echo $dash_link; ?>">dashboard</a></li>
						<?php } ?>
					</ul>

				</div>
				
			</div>
			
		</div>


	<!-- 	<div class="row home-header">
			<div class ="col-md-2 text-center home-logo logo">
				
				<a href="<?php echo $logo_url; ?>"><img src="<?php echo base_url(); ?>site_assets/images/logo.png"></a>
			</div>
			
			<div class ="col-md-10 text-right home-menu">
				<ul class="list-inline">
					<li class="list-inline-item item-link"><a class="" href="<?php echo base_url(); ?>">Home</a></li>
					<li class="list-inline-item item-link"><a href="<?php echo base_url('pages/front/about_us');?>">about</a></li>
					<li class="list-inline-item item-link"><a href="<?php echo base_url('blog/features');?>">features</a></li>
					<li class="list-inline-item item-link"><a href="<?php echo base_url('user/pricing');?>">pricing</a></li>
					<li class="list-inline-item item-link"><a href="<?php echo base_url('user/demo');?>">demo</a></li>
					<li class="list-inline-item item-link"><a href="<?php echo base_url('blog/blogs_with_categories') ?>">blog</a></li>
					<?php
					if(!isset($flag)){
						?>
						<li class="list-inline-item item-link"><a href="/user/login">login</a></li>
						<li class="list-inline-item item-link-dark"><a href="/user/register">get started</a></li>
						<?php    
					}
					if(@$flag){
						?>
						<li class="list-inline-item item-link"><a href="<?php echo base_url('user/resources');?>">Resources</a></li>
						<li class="list-inline-item item-link"><a href="<?php echo $dash_link; ?>">dashboard</a></li>
 -->						<!--
						<li class="list-inline-item menu-item dropdown">
							<a href="<?php echo base_url('user/dashboard'); ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $display_name;?> <b class="caret"></b></a>
							<ul class="dropdown-menu border-dark-1">
								<li class="menu-dropdown-item"><a href="<?php echo base_url('user/profile'); ?>">profile</a></li>
								<?php
								if(isset($store_id)&&$store_id!=""){
									?>
									<li class="menu-dropdown-item"><a href="<?php echo base_url('user/subscription'); ?>">manage subscription</a></li>
									<li class="menu-dropdown-item"><a href="<?php echo base_url('user/history'); ?>">history</a></li>
									
									<?php    
								}
								?>
								<li class="menu-dropdown-item"><a href="<?php echo base_url('user/logout'); ?>">logout</a></li>
							</ul>
						</li>-->
					<!-- 	working copy
						<li class="list-inline-item menu-item item-link dropdown">
							<a href="javascript:void(0)" class="dropdown-toggle header-dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<?php echo $display_name; ?>
								<span class=" fa fa-angle-down" style="font-size:12px;"></span>
							</a>
							<ul class="dropdown-menu animated fadeInUp home-dropdown header-dropdown">
								<li>
									<a href="<?php echo base_url('user/profile'); ?>" class="hvr-bounce-to-right"> profile</a>
								</li>
								<?php
								if (isset($store_id) && $store_id != "") {
									?>
									<li>
										<a href="<?php echo
										base_url('user/subscription'); ?>" class="hvr-bounce-to-right">  manage subscription</a>
									</li>
									<li>
										<a href="<?php echo base_url('user/history'); ?>" class="hvr-bounce-to-right">history</a>
									</li>

									<?php
								} ?>

								<li>
									<a href="<?php echo base_url('user/logout'); ?>" class="hvr-bounce-to-right"> logout</a>
								</li>
							</ul>
						</li> working copy-->
					<!-- 	<?php
					}
					?>
					
				</ul>
			</div>
		</div> -->
	</div>
	<?php
	$error_msg = $this->session->flashdata('error_msg2');
	if ($error_msg) {
		?>
		<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
			</button>
			<?php echo $error_msg; ?>
		</div>
		<?php
	}
	?>
	<script>
		function menuOpen(){

			document.getElementById("menu-option").style.width="250px";

		}
		function menuClose(){
			document.getElementById("menu-option").style.width="0px";
		}


	</script>