		<div class="container-fluid main-container pricing-section">
			<div class="row row-fixed">
				<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
					<div class="section-content-block text-center">
						<div class="head"	>
							<h1 class="heading-level-1">Our Resources</h1>
						</div>
						<div class="content">
							<p class="text-level-1">
								We are a platform to help digital nomads and entrepeneurs to launch digital products globally to the world. 
							</p>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>


		<div class="container-fluid">
			<div class="row row-stretched">
				<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 resource-block">
					<div class="resource-img-block text-center">
						<div class="img-div">
							<img style="max-width:300px !important;" src="<?php echo base_url();?>site_assets/images/logo.png">
						</div>
						<div class="aaaa resource-content-block">
							<div class="head text-left">
								<h3 class="resource-title">Digital Media Deliveries</h3>
							</div>
							<div class="content text-left">
								<p class="resource-text">
									Would you like to earn money helping us grow our company?
									Sign up today and start making commissions.
								</p>
							</div>
							<div class="block-btn">
								<a href="/user/register" class="btn-empty-md btn">Sign Up Now</a>
							</div>
						</div>
					</div>
				</div>

				<?php if (count($resources) > 0): 
					foreach($resources as $res): 
						if($res['file_type'] == 1):
							?>
							<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 resource-block">
								<div class="resource-img-block text-center">
									<div class="img-div">
										<img src="<?php echo base_url();?>site_assets/resources/<?php echo $res['file'];?>">
									</div>
									<div class="aaaa resource-content-block">
										<div class="head text-left">
											<h3 class="resource-title"><?php echo $res['title'];?></h3>
										</div>
										<div class="content text-left">
											<p class="resource-text">
												<?php echo $res['description'];?>
											</p>
										</div>
										<div class="block-btn">
											<a target="_blank" href="<?php echo $res['url']; ?>" class="btn-empty-md btn"><?php echo $res['label'];?></a>
										</div>
									</div>
								</div>
							</div>
							<?php else: ?>

								<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 resource-block video-field">
									<div class="resource-img-block text-center">
										<div class="video-div">
											<video controls>
												<source  src="<?php echo base_url();?>site_assets/resources/<?php echo $res['file'];?>" type="video/mp4">
												</video>
											</div>
											<div class="aaaa resource-content-block">
												<div class="head text-left">
													<h3 class="resource-title"><?php echo $res['title'];?></h3>
												</div>
												<div class="content text-left">
													<p class="resource-text">
														<?php echo $res['description'];?>
													</p>
												</div>
												<div class="block-btn">
													<a target="_blank" href="<?php echo $res['url']; ?>" class="btn-empty-md btn"><?php echo $res['label'];?></a>
												</div>
											</div>
										</div>
									</div>

								<?php endif;  endforeach; endif; ?>


								<div class="clearfix"></div>
							</div>
						</div>


