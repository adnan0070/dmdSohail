<div class="row margin-none">
	<div class="col-md-6 mx-auto section-login-signup border-dark-1 text-center demo-section">

		<h4 class="heading-lg-green">Administration</h4>
		<div class="demo-block">
			<br><br><br>
			<img  src="<?php echo base_url('site_assets/demo/') . 'demo_user.gif'; ?>" />
		</div>
		<p class="paragraph-text-sm-grey  text-left">Login with<br> Username: demo@dmd.com <br> Password: Demodmd123</p>
		<a href="http://digitalmediadeliveries.com/user/login" target="_blank" class="static-content-btn btn"> Run Demo </a>
	</div>

	


	<div class="col-md-6 mx-auto section-login-signup border-dark-1 text-center demo-section">
		<h4 class="heading-lg-green">Store Front</h4>
		<div class="demo-block">
			<img src="<?php echo base_url('site_assets/demo/') . 'demo_store.png'; ?>" />
		</div>
		<a href="http://demo.digitalmediadeliveries.com" target="_blank" class="static-content-btn btn"> Run Demo </a>
	</div>
	
	
</div>

<!-- Username: demo@dmd.com -->
<!-- Password: Demodmd123 -->