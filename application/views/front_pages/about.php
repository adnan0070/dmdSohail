<style type="text/css">
	.form-input{
		font-size: 14px;
		color: rgb(69, 85, 97);
		text-transform: none !important;
		line-height: 1.2;
		font-weight:500;
		text-align: center;
		margin-top:5px;
		margin-bottom:5px;
		border:1px solid rgb(69, 85, 97);
		height: 40px;
}
</style>

<?php if($this->session->flashdata('error_msg')){ ?>
	<div class="row section-login-signup">
		<div class="col-md-12">
				<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
			  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  		<strong>Error! </strong><?php echo $this->session->flashdata('error_msg'); ?>
				</div>
		</div>
	</div>
<?php } ?>

<div class="row main-body mx-auto about">
			<div class="col-md-12 about-head">
				<h4 class="heading-lg-red"> About us</h4>
			</div>
			<div class="col-md-12 about-paragraph">
				<p class="paragraph-text-sm-grey"> 
					Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum.
				</p>
			</div>

			<div class="col-md-12 about-paragraph">
				<p class="paragraph-text-sm-grey"> 
					Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum.
				</p>
			</div>

			<div class="col-md-12 about-paragraph">
				<p class="paragraph-text-sm-grey"> 
					Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum.
				</p>
			</div>

			<div class="col-md-12 about-paragraph">
				<p class="paragraph-text-sm-grey"> 
					Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum.
				</p>
			</div>

			<div class="col-md-12 about-paragraph">
				<p class="paragraph-text-sm-grey"> 
					Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum Lorem ipsum dolor sit amet, augue litora, elementum animi nibh aliquam porttitor, pede officia rhoncus ac mi in. Risus ac mauris amet faucibus, suscipit mattis feugiat vel fermentum.
				</p>
			</div>
		</div><!-- inner-body-end-->

 <script>
 var check = function() {
  if (document.getElementById('password').value ==
    document.getElementById('conf-password').value) {
    document.getElementById('pass_message').style.color = 'green';
    document.getElementById('pass_message').innerHTML = 'matching';
  } else {
    document.getElementById('pass_message').style.color = 'red';
    document.getElementById('pass_message').innerHTML = 'not matching';
  }
}
 </script>       