<div class="row section-login-signup border-dark-1">
	<div class="col-md-12 text-center ">
		<h4 class="heading-lg-green">Write, What you want?</h4>
		<form method="post" action="#">

			<div class="form-group input-effects">
				<input type="text"  name="title" class="home-input" placeholder="">
				<label>Title</label>
				<span class="focus-border"></span>
			</div>


			<div class="form-group input-effects">
				<input type="email"  name="email" class="home-input" placeholder="">
				<label>E-mail</label>
				<span class="focus-border"></span>
			</div>


			<div class="form-group input-effects">
				<input type="text"  name="phone" class="home-input" placeholder="">
				<label>Phone</label>
				<span class="focus-border"></span>
			</div>




			<textarea placeholder="Message" name="message" maxlength="1000" rows="7" class="form-control form-textarea border-dark-1"></textarea> 
			<br>
			<input type="submit" class="static-content-btn btn" />
			<br> <br>
		</form>
	</div>
</div>