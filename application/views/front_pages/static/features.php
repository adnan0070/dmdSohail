		<?php 
		$get_lowest = $this->db->query("SELECT *
			FROM packages
			WHERE pack_price =  ( SELECT MIN(pack_price) FROM packages ) ");
		$default_package = $get_lowest->row_array();

		?>
		<div class="container home-container">
			<div class="row main-body home-features features-row-1 mx-auto ">

				<div class="col-md-12 text-center">
					<h3 class="heading-xl-green">Features</h3>
					<br>
					<p class="paragraph-text-lg-blue text-center">
						We are a platform to help digital nomads and entrepeneurs to launch digital products globally to the world. 
					</p>
				</div>

				<div class="col-md-12 text-center">
					<a href="/user/register" class="btn-lg-default btn"><?php echo $default_package['pack_days']; ?> Day FREE Trial – Click Here</a> 
				</div>

				<div class="col-md-4 col-xs-12 col-sm-6 feature text-center">
					<div class="nauk-info-connections text-center feature-block" >
						<img src="<?php echo base_url(); ?>site_assets/images/webinar.png">

						<p style="background:none; font-weight:600;" class="heading-md-blue text-center"> New Feature: Host Your Webinars And Sales Videos</p>
						<p class="paragraph-text-sm-grey text-center">
							Use our Platform to host your Webinars and Sales Videos, simply link directly with your registration pages. Add buy now buttons to appear at the time you make your offer
						</p>
					</div>
				</div>
				
				<div class="col-md-4 col-xs-12 col-sm-6 feature text-center">
					<div class="nauk-info-connections text-center feature-block" >
						<img src="<?php echo base_url(); ?>site_assets/images/feature-1.png">
						<p style="background:none; font-weight:600;" class="heading-md-blue text-center"> Multiple payment methods<br><br></p>
						<p class="paragraph-text-sm-grey text-center">
							We have integrated multiple payment methods for ease of purchasing your product.
						</p>
					</div>
				</div>

				<div class="col-md-4 col-xs-12 col-sm-6 feature text-center">
					<div class="nauk-info-connections text-center feature-block" >
						<img src="<?php echo base_url(); ?>site_assets/images/feature-2.png">

						<p  style="background:none; font-weight:600;" class="heading-md-blue text-center"> Hosting your product<br><br></p>
						<p class="paragraph-text-sm-grey text-center">
							We use latest server technology to host your products. 
						</p>
					</div>
				</div>

				<div class="col-md-4 col-xs-12 col-sm-6 feature text-center">
					<div class="nauk-info-connections text-center feature-block"  >
						<img src="<?php echo base_url(); ?>site_assets/images/feature-3.png">

						<p style="background:none; font-weight:600;" class="heading-md-blue text-center"> Secure data delivery</p>
						<p class="paragraph-text-sm-grey text-center">
							We make sure all your files are protected for delivery.
						</p>
					</div>
				</div>

				<div class="col-md-4 col-xs-12 col-sm-6 feature text-center">
					<div class="nauk-info-connections text-center feature-block"  >
						<img src="<?php echo base_url(); ?>site_assets/images/feature-4.png">

						<p style="background:none; font-weight:600;" class="heading-md-blue text-center"> Multiple store management</p>
						<p class="paragraph-text-sm-grey text-center">
							Create as many stores as you need and manage them all from one place in your Dashboard.
						</p>
					</div>
				</div>

				<div class="col-md-4 col-xs-12 col-sm-6 feature text-center">
					<div class="nauk-info-connections text-center feature-block"  >
						<img src="<?php echo base_url(); ?>site_assets/images/feature-5.png">

						<p style="background:none; font-weight:600;" class="heading-md-blue text-center"> Responsive stores</p>
						<p class="paragraph-text-sm-grey text-center">
							All stores work perfectly on all devices because of the device readiness.
						</p>
					</div>
				</div>

				<div class="col-md-4 col-xs-12 col-sm-6 feature text-center">
					<div class="nauk-info-connections text-center feature-block"  >
						<img src="<?php echo base_url(); ?>site_assets/images/feature-6.png">

						<p style="background:none; font-weight:600;" class="heading-md-blue text-center"> Manage upsells-crosssells</p>
						<p class="paragraph-text-sm-grey text-center">
							Easily manage upsells that compliment your products, or cross-sell similar ones
						</p>
					</div>
				</div>

				<div class="col-md-4 col-xs-12 col-sm-6 feature text-center">
					<div class="nauk-info-connections text-center feature-block"  >

						<img src="<?php echo base_url(); ?>site_assets/images/feature-7.png">

						<p style="background:none; font-weight:600;" class="heading-md-blue text-center"> Powerful analytics</p>
						<p class="paragraph-text-sm-grey text-center">
							Generate powerful reports to help you improve your business and increase sales.
						</p>
					</div>
				</div>

				<div class="col-md-4 col-xs-12 col-sm-6 feature text-center">
					<div class="nauk-info-connections text-center feature-block" >
						<img src="<?php echo base_url(); ?>site_assets/images/feature-8.png">

						<p style="background:none; font-weight:600;" class="heading-md-blue text-center">Video Vault</p>
						<p class="paragraph-text-sm-grey text-center">
							Easily set up videos your customers can view online only, in a secure environment, perfect for training courses or series
						</p>
					</div>
				</div>

				<div class="col-md-4 col-xs-12 col-sm-6 feature text-center">
					<div class="nauk-info-connections text-center feature-block" >
						<img src="<?php echo base_url(); ?>site_assets/images/feature-9.png">

						<p style="background:none; font-weight:600;" class="heading-md-blue text-center">Customise Emails</p>
						<p class="paragraph-text-sm-grey text-center">
							Link your account seamlessly with your Autoresponder and create customised emails for your customers.
						</p>
					</div>
				</div>

				<div class="col-md-12 text center">
					<br>
					<p class="paragraph-text-lg-blue text-center"> And many more...</p>
				</div>

				<div class="col-md-12 text center">
					<p class="paragraph-text-lg-blue text-center">
						Still not convinced? Ask us for a demo or sign up for a free trial to explore the platform.
					</p>
				</div>


			</div>
			<div class="row home-feature features-row-2 mx-auto ">

				<div class="col-md-6 text-center">
					<a href="<?php echo base_url('pages/front/support');?>" class="btn-lg-default btn" >Talk to Us</a> 
				</div>
				<div class="col-md-6 text-center">
					<a href="/user/register" class="btn-lg-default btn" ><?php echo $default_package['pack_days']; ?> Day FREE Trial – Click Here</a> 
				</div>
			</div>

		</div>