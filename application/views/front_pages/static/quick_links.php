<div class="container home-container">
	<div class="row main-body static-pages mx-auto ">
		<div class="static-tabs">

			

			<?php if(isset($slug)) { ?>
				<input type="radio" id="tab1" name="tab-control"
				<?php if ($slug == "about_us") {
					echo "checked";
				}?> >
				<input type="radio" id="tab2" name="tab-control"
				<?php if ($slug == "support") {
					echo "checked";
				}?> >
				<input type="radio" id="tab3" name="tab-control" 
				
				<?php if ($slug == "show_teams") {
					echo "checked";
				}?> >
				<input type="radio" id="tab4" name="tab-control" 
				<?php if ($slug == "affiliate_program") {
					echo "checked";
				}?> >
			<?php } ?>
			<ul>
				<li title="Features"><label onclick="ozfunction($(this).attr('id'));" id="about_us" for="tab1" role="button"><br><span id="about">About Us</span></label></li>
				<li title="Delivery Contents"><label onclick="ozfunction($(this).attr('id'));" id="support" for="tab2" role="button"><br><span>Support</span></label></li>
				<li title="Shipping"><label onclick="ozfunction($(this).attr('id'));" id="show_teams" for="tab3" role="button"><br><span>Our Teams</span></label></li>    
				<!--<li title="Returns"><label onclick="ozfunction($(this).attr('id'));" id="affiliate_program" for="tab4" role="button"><br><span id="affiliates">Affiliate Program</span></label></li>-->
			</ul>

			
			<div class="static-slider"><div class="indicator"></div></div>
			<div class="static-content">
				
				<?php if ($this->session->flashdata('msg')) { ?>
					<div class="alert alert-info" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<?php echo str_replace("_", " ", $this->session->flashdata('msg')); ?>
					</div>
				<?php } ?>


				<?php if(isset($view_items) && !empty($view_items)) {?>
					<?php echo $view_items[0]->content; ?>
				<?php } ?>

				<?php if(isset($teams) && !empty($teams)) {
					echo '<div class="row">';
					echo '<div class="col-md-12 text-center static-our-team">
					<p class="static-team-info">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam nemo ducimus eius, magnam error quisquam sunt voluptate labore, excepturi numquam! Alias libero optio sed harum debitis! Veniam, quia in eum.
					</p>
					</div>';

					foreach ($teams as $key => $value) {

						?>
						
						<div class="col-md-3 text-center static-our-team">
							<img src="<?php echo base_url('site_assets/team/') . $value["image"]; ?>" class="static-team-photo">
						</div>

						<div class="col-md-9 static-our-team">
							<p class="static-team-name"><?php echo $value["name"]; ?></p>
							<p class="static-team-job"><?php echo $value["designation"]; ?></p>
							<p class="static-team-info"><?php echo $value["description"]; ?></p>
						</div>


					<?php }
					echo '</div>';
				} ?>




				<section><p></p></section>

				<section>

					

				<!-- <div class="row">
					<div class="col-md-12 text-center static-contact-us">
						<h4>Contact Us test</h4>
						<form method="post" action="user/contact_us">
							<input type="text"  name="name" class="form-control form-input border-dark-1" placeholder="Name">
							<input type="text"  name="email" class="form-control form-input border-dark-1" placeholder="E-mail">
							<input type="text"  name="subject" class="form-control form-input border-dark-1" placeholder="Subject">
							<textarea name="message" maxlength="1000" rows="7" class="form-control form-textarea border-dark-1"></textarea> 
							<input type="submit" class="static-content-btn btn" />
						</form>
					</div>
				</div> -->
				
				<!-- <span class="text-danger">Validation error</span> -->
				


			</section>

			<section>



				<div class="row">
					<div class="col-md-12 text-center static-our-team">
						<p class="static-team-info">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam nemo ducimus eius, magnam error quisquam sunt voluptate labore, excepturi numquam! Alias libero optio sed harum debitis! Veniam, quia in eum.
						</p>
					</div>

					<div class="col-md-3 text-center static-our-team">
						<img src="assets/images/user-photo.jpg" class="static-team-photo">
					</div>

					<div class="col-md-9 static-our-team">
						<p class="static-team-name"> Lorem ipsum</p>
						<p class="static-team-job"> Lorem ipsum dolor</p>
						<p class="static-team-info">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam nemo ducimus eius, magnam error quisquam sunt voluptate labore, excepturi numquam! Alias libero optio sed harum debitis! Veniam, quia in eum.
						</p>
					</div>

					<div class="col-md-3 text-center static-our-team">
						<img src="assets/images/user-photo.jpg" class="static-team-photo">
					</div>

					<div class="col-md-9 static-our-team">
						<p class="static-team-name"> Lorem ipsum</p>
						<p class="static-team-job"> Lorem ipsum dolor</p>
						<p class="static-team-info">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam nemo ducimus eius, magnam error quisquam sunt voluptate labore, excepturi numquam! Alias libero optio sed harum debitis! Veniam, quia in eum.
						</p>
					</div>

					<div class="col-md-3 text-center static-our-team">
						<img src="assets/images/user-photo.jpg" class="static-team-photo">
					</div>

					<div class="col-md-9 static-our-team">
						<p class="static-team-name"> Lorem ipsum</p>
						<p class="static-team-job"> Lorem ipsum dolor</p>
						<p class="static-team-info">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam nemo ducimus eius, magnam error quisquam sunt voluptate labore, excepturi numquam! Alias libero optio sed harum debitis! Veniam, quia in eum.
						</p>
					</div>

					<div class="col-md-3 text-center static-our-team">
						<img src="assets/images/user-photo.jpg" class="static-team-photo">
					</div>

					<div class="col-md-9 static-our-team">
						<p class="static-team-name"> Lorem ipsum</p>
						<p class="static-team-job"> Lorem ipsum dolor</p>
						<p class="static-team-info">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam nemo ducimus eius, magnam error quisquam sunt voluptate labore, excepturi numquam! Alias libero optio sed harum debitis! Veniam, quia in eum.
						</p>
					</div>



				</div>

			</section>

			<section><p></p></section>

		</div>
	</div>


</div>
</div>

