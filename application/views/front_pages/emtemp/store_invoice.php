<!DOCTYPE html>
<html lang="en">
<head>
    <title>DMD</title>
    <meta charset="utf-8">  
</head>
<?php
// get order detail
$order = $this->db->query('select ord.*,dmd_order_items.*,dmd_order_items.coupon_discount as pdiscount,dmd_store_products.product_name,dmd_store_products.pro_image,dmd_stores.store_name from dmd_orders as ord left join dmd_order_items on(dmd_order_items.order_id=ord.id)
    left join dmd_store_products on(dmd_order_items.product_id=dmd_store_products.product_code)
    left join dmd_stores on(dmd_stores.id=ord.store_id) where ord.order_number="'.$order_number.'"');
$order = $order->result_array();

?>
<body style="font-family: 'Montserrat', sans-serif"> 
    <div class="container" style="max-width:960px;
    width:100%;
    background:#fff;
    margin: auto;">
    <div class=" row header-right col" style="display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px; position: relative;
    width: 100%;
    min-height: 1px;
    text-align:center;">
    <img src="http://digitalmediadeliveries.com/site_assets/images/logo.png" style="height:100px; margin:0px auto;float:center;text-align:center;
    padding-top: 15px;">
</div>

<div class="row header name-heading"
style="
font-size: 30px;
margin: auto;display: -webkit-box;
display: -ms-flexbox;
display: flex;
-ms-flex-wrap: wrap;
flex-wrap: wrap;
margin-right: -15px;
margin-left: -15px;
min-height: 100px;
padding: 10px 0px;
border-bottom: 1px solid rgb(240, 240, 240);
margin-bottom: 30px;
color: #468fb1;
">
<h3 style="margin: auto;font-size: 30px;text-align:center;"><?php echo $order[0]['store_name']; ?></h3>
</div>




<div class="row heading" style="    width:100%;
min-width:960px;
color: #74a857;">
<h1 style="
font-size: 27px;color: #74a857;
">Order invoice</h1>
</div>      


<div class="row">
    <table class="tbl-large" style="
    width: 100%;
    ">
    <tr>
        <th></th>
        <th style="font-size: 16px;">Name</th>
        <th style="font-size: 16px;">Discount</th>
        <th style="font-size: 16px;">Price</th>
        <th style="font-size: 16px;">Quantity</th>
        <th style="font-size: 16px;">Total price</th>
    </tr>
    <?php
    foreach($order as $ord){
        if($ord['pdiscount']==""){
            $ord['pdiscount'] = 0;
        }
        ?>
        <tr>
            <td class="tbl-img" style="
            color: rgb(69, 85, 97);
            margin-top: 30px;
            display: block;
            ">
            <img src="<?php echo base_url(); ?>site_assets/products/<?php echo $ord['pro_image'];?>" style="max-height: 80px;
            max-width: 80px;text-align: center;"></td>
            <td style="color: rgb(69, 85, 97); font-size: 14px;
            text-align: center;"><?php echo $ord['product_name'];?></td>
            <td style="color: rgb(69, 85, 97); font-size: 14px;
            text-align: center;"><?php echo $ord['pdiscount']."%";?></td>
            <td style="color: rgb(69, 85, 97); font-size: 14px;
            text-align: center;"><?php echo $ord['product_price'];?></td>
            <td style=" color: rgb(69, 85, 97); font-size: 14px;
            text-align: center;"><?php echo $ord['product_quantity'];?></td>
            <td style="color: rgb(69, 85, 97); font-size: 14px;
            text-align: center;">$<?php echo $ord['total_price'];?></td>
        </tr>
        <?php 
    }

    ?>
</table>
</div>      

<div class="row" style="
background-color: rgb(247, 247, 247);
float: right;
width: 328px;
padding-top: 15px;
padding-bottom: 20px;
margin-top: 15px;
margin-bottom: 100px;
">
<table class="tbl-small">
    <tbody>
        <tr class="cart-data tbl-small-heading">
            <td 
            style="
            font-size: 14px;
            width: 150px;
            padding: 10px 30px;
            ">Subtotal:</td>
            <td class="price-filed"
            style="
            text-align: right;
            font-size: 14px;
            width: 150px;
            padding: 10px 30px;
            ">$<?php echo $order[0]['total_amount']; ?></td>
        </tr>

        <tr class="cart-data tbl-small-heading">
            <td 
            style="
            font-size: 14px;
            width: 150px;
            padding: 10px 30px;
            ">Tax:</td>
            <td class="price-filed"
            style="
            text-align: right;
            font-size: 14px;
            width: 150px;
            padding: 10px 30px;
            ">$<?php echo $order[0]['tax']; ?></td>
        </tr>
        
        <tr class="cart-footer">
            <td
            style="
            color: rgb(69, 85, 97);
            border-bottom: dashed 1px;
            border-top: dashed 1px;
            font-size: 14px;
            width: 150px;
            padding: 10px 30px;
            ">Total:</td>
            <td class="price-filed"
            style="
            color: rgb(69, 85, 97);
            border-bottom: dashed 1px;
            border-top: dashed 1px;
            text-align: right;
            font-size: 14px;
            width: 150px;
            padding: 10px 30px;
            ">$<?php echo $order[0]['grand_total']; ?></td>
        </tr>
    </tbody>
</table>
</div>      


</div>
</body>
</html>

