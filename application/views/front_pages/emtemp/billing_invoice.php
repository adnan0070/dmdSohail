<!DOCTYPE html>
<html lang="en">
<head>
    <title>DMD</title>
    <meta charset="utf-8">  
</head>
<body style="font-family: 'Montserrat', sans-serif"> 
    <div class="container" style="max-width:960px;
    width:100%;
    background:#fff;
    margin: auto;">
    <div class=" row header-right col" style="display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px; position: relative;
    width: 100%;
    min-height: 1px;">
    <img src="http://digitalmediadeliveries.com/site_assets/images/logo.png" style="height:100px;
    padding-top: 15px;">
</div>

<div class="row header name-heading"
style="
font-size: 36px;
margin: auto;display: -webkit-box;
display: -ms-flexbox;
display: flex;
-ms-flex-wrap: wrap;
flex-wrap: wrap;
margin-right: -15px;
margin-left: -15px;
min-height: 100px;
padding: 10px 0px;
border-bottom: 1px solid rgb(240, 240, 240);
margin-bottom: 30px;
color: #468fb1;
">
<h3 style="margin: auto;font-size: 36px;">Billing Invoice</h3>
</div>




<div class="row heading" style="    width:100%;
min-width:960px;
color: rgb(11,102,35);">
<h1 style="
font-size: 30px;color: rgb(11,102,35);
"></h1>
</div>   

<div class="row">
    <table class="tbl-large" style="
    width: 100%;
    ">
    <tr>
        <th style="font-size: 18px;">Name</th>
        <th style="font-size: 18px;">Email</th>
        <th style="font-size: 18px;">Billing Date</th>
        <th style="font-size: 18px;">Amount</th>
        <th style="font-size: 18px;">Transaction ID</th>
    </tr>

<?php $history = $history[0]; ?>
<tr>
    <td style="color: rgb(69, 85, 97); font-size: 18px; text-align: center;">
        <?php echo $this->session->userdata('user_name'); ?>
    </td>
    
    <td style="color: rgb(69, 85, 97); font-size: 18px; text-align: center;">
        <?php echo $this->session->userdata('user_email'); ?>
    </td>
    
    <td style="color: rgb(69, 85, 97); font-size: 18px; text-align: center;">
        <?php echo $history['created_date']; ?>
    </td>
    
    <td style="color: rgb(69, 85, 97); font-size: 18px; text-align: center;">
        $<?php echo $history['amount']; ?>
    </td>

    <td style="color: rgb(69, 85, 97); font-size: 18px; text-align: center;">
        <?php echo $history['trans_id']; ?>
    </td>
</tr>

</table>
</div>      


</div>
</body>
</html>

