<!DOCTYPE html>
<html lang="en">
<head>
  <title>Email</title>
  <meta charset="utf-8">  
  <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- font-family: 'Lato', sans-serif; -->

</head>
<style>
body{
  font-family: Arial, Helvetica, sans-serif !important; 
}

</style>
<body>  
  <div class="container " style="padding: 50px 0px;">

    <div class="text-center " style="
    width:100%;
    max-width: 600px !important;
    margin: 20px auto !important;
    text-align: center;">
    <img class="" style="width: 210px; height:80px;" src="http://digitalmediadeliveries.com/site_assets/images/logo.png">
  </div>

  <div class="row  text-center" style="
  padding: 20px 10px !important;
  background: rgb(253,253,253) !important;
  width:100%;
  max-width: 600px !important;
  margin: 1px auto !important;
  text-align: center;
  border: 1px solid #eee;">

  <div class="col-md-12 ">
    <h4 class="" style="
    color: #468fb1;
    font-size: 25px;
    line-height: 1.167;
    font-weight: 600;
    ">Welcome to Our Community</h4>
    <h4 class="" style="
    color: #74a857;
    font-size: 30px;
    line-height: 1.167;
    font-weight: 600;
    ">Digital Media Deliveries</h4>
  </div>



  <div class="col-md-12">
    <div>
      <p style="
      font-size: 17px;
      color: #000;
      line-height: 1.2;
      font-weight: 500;">Hello, {name}</p>
      <p style="
      font-size: 14px;
      color: #848484;
      line-height: 1.2;
      letter-spacing: 0.03rem;">Your purchase from <b>{storename}</b> is complete. View your purchase</p>



      <div class="  login-form-footer" style="margin-top:30px;
      margin-bottom:30px;">
      <a href="{link}" class="home-form-btn " style="    font-size: 15px;
      color: #74a857 !important;
      background-color: #fff;
      line-height: 1.5 !important;
      font-weight: 700;
      min-width: 250px;
      height: 40px;
      border: 2px solid #74a857;
      text-align: center;
      border-radius: 50px;
      text-decoration: none;
      text-transform: uppercase;
      padding: 10px 20px;"> {storename}</a><br><br>

    </div>
    <div>
      <p style="
      font-size: 14px;
      color: #000;
      line-height: 1.2;">For questions contact storesupport@store.com</p>
      <p style="
      font-size: 14px;
      color: #848484;
      line-height: 1.2;
      letter-spacing: 0.04rem; ">You received this email from DMD because <b>{storename}</b> uses DMD as a<br>
    content delivery partner. This is not a marketing email.</p>
    <p style="
    font-size: 14px;
    color: #848484;
    line-height: 1.2;">Thanks</p>
    <p style="
    font-size: 16px;
    color: #468fb1;
    line-height: 1.6;
    font-weight: 600;
    letter-spacing: .02rem;">Digital Media Deliveries Team</p>

    <p style="
    font-size: 14px;
    color: #848484;
    line-height: 1.2;">This is an automated message, please do not reply.</p>
  </div>
  <div>
        <a target="_blank" href="https://digitalmediadeliveries.com" style="
        color: #74a857;
        font-size: 14px;
        line-height: 0.267;
        cursor: pointer;
        ">www.digitalmediadeliveries.com</a>
        <p style="
        font-size: 16px;
        color: #000;
        line-height: 1;">Stay Connected</p>
      </div>
      <div>
      <ul style="padding: 0;">
          <li style="
          display:inline-block; 
          height: 25px; 
          width: 25px; 
          padding: 3px;
          border-radius: 50px;
          background: linear-gradient(to right top, #F6BA45, #7F2AB8);
          background: -webkit-linear-gradient(to right top, #F6BA45, #7F2AB8);

          background: -webkit-gradient(to right top, #F6BA45, #7F2AB8);
          background: -moz-linear-gradient(to right top, #F6BA45, #7F2AB8);
          background: -o-linear-gradient(to right top, #F6BA45, #7F2AB8);
          ">

          <a target="_blank" href="https://www.instagram.com/digitalmediadeliveries/"><img style="
          height: 15px;
          width: 15px;
          padding: 5px 0px;" src="https://wwww.digitalmediadeliveries.com/site_assets/images/icon-instagram.png"></a></li>
          <li style="
          display:inline-block; 
          height: 25px; 
          width: 25px; 
          padding: 3px;
          border-radius: 50px;
          background: #3b5998;">

          <a target="_blank" href="https://www.facebook.com/DigitalMediaDeliveries"><img style="
          height: 15px;
          width: 15px;
          padding: 5px 0px;" src="https://wwww.digitalmediadeliveries.com/site_assets/images/icon-facebook.png"></a></li>
          <li style="
          display:inline-block; 
          height: 25px; 
          width: 25px; 
          padding: 3px;
          border-radius: 50px;
          background: #0077b5;">

          <a target="_blank" href="https://www.linkedin.com/"><img style="
          height: 15px;
          width: 15px;
          padding: 5px 0px;" src="https://wwww.digitalmediadeliveries.com/site_assets/images/icon-linkedin.png"></a></li>
          <li style="
          display:inline-block; 
          height: 25px; 
          width: 25px; 
          padding: 3px;
          border-radius: 50px;
          background: #1da1f2;">

          <a target="_blank" href="https://www.twitter.com/digimeddelivery"><img style="
          height: 15px;
          width: 15px;
          padding: 5px 0px;" src="https://wwww.digitalmediadeliveries.com/site_assets/images/icon-twitter.png"></a></li>

        </ul>
      </div>
</div>




</div>

</div>
</div>











</div>
</body>
</html>
