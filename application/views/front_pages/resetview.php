<div class="row text-center section-login-signup border-dark-1">
    <div class="col-md-12">
      <h2 class="heading-lg-green">Email sent</h2>
    </div>
    <div class="col-md-12"> 
      <?php
      $success_msg = $this->session->flashdata('success_msg');
      if ($success_msg) {
        echo $success_msg;
      }
      ?> 
      <div class="form-footer">
        <form method="post" action="<?php echo base_url('user/ForgotPassword/'); ?>">
          <input type="hidden" name="email" value="<?php echo $email; ?>" />
          <input type="submit" value="Resend E-mail" class="btn-lg-default btn"/>
        </form>
      </div>

    </div>
    <div class="col-md-12 text-center">
     <p class="heading-sm-grey">Do not have an account?
      <a class="heading-sm-grey" href ="<?php echo base_url('user/register'); ?>"> Create on here</a>
    </p>
  </div> 
</div>

<script>
 var check = function() {
  if (document.getElementById('password').value ==
    document.getElementById('conf-password').value) {
    document.getElementById('pass_message').style.color = 'green';
  document.getElementById('pass_message').innerHTML = 'matching';
} else {
  document.getElementById('pass_message').style.color = 'red';
  document.getElementById('pass_message').innerHTML = 'not matching';
}
}
</script>       