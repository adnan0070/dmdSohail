                  <form role="form" method="post" action="<?php echo base_url('customer/login_user'); ?>">
                   <div class="row text-center section-login-signup border-dark-1">
                    <div class="col-md-12">
                      <?php
                      $success_msg= $this->session->flashdata('success_msg');
                      $error_msg= $this->session->flashdata('error_msg');

                      if($success_msg){
                       ?>
                       <div class="alert alert-success">
                        <?php echo $success_msg; ?>
                      </div>
                      <?php
                    }
                    if($error_msg){
                      ?>
                      <div class="alert alert-danger">
                        <?php echo $error_msg; ?>
                      </div>
                      <?php
                    }
                    ?>
                  </div>      
                  <div class="col-md-12">
                    <div class="form-group input-effects">
                      <input type="email"  name = "email" class = "home-input" autocomplete="off" id="email" value="" placeholder=""/>
                      <label>E-mail address</label>
                      <span class="focus-border"></span>
                      <!--<span class="text-danger">Validation error</span>-->
                    </div>
                  </div> 

                  <div class="col-md-12">
                    <div class="form-group input-effects">
                     <input type="password" name = "password" class = "home-input" value="" autocomplete=”off” id="password" placeholder=""/>
                     <label>Password</label>
                     <span class="focus-border"></span>
                     <!--<span class="text-danger">Validation error</span>-->
                   </div>
                 </div> 

                 <div class="col-md-12 form-footer text-center">
                  <div class="form-group"> 
                    <input class="btn-lg-default btn" type="submit" id="login" value="login" name="login" >
                  </div>

                  <div class="form-group">
                    <a class="heading-sm-grey" href ="<?php echo base_url('user/ForgotPass'); ?>">Forgot password?</a>
                  </div> 
                </div>
              </div>


            </form>
            <script type="text/javascript">
              $('#searchCart').css('display','none');
            </script>