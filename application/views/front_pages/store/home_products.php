<?php 
function calculate_dicounted_price($price, $coupon){
	$discount_price = ($coupon/100) * $price;
	$discount_price = $price - $discount_price;
	return $discount_price;
}

$cuid = $this->session->userdata('customer_details');
$cuid = @$cuid['customerid'];

if(isset($products)){
	foreach($products as $product){
		$rate  = $controller->getProrating(@$cuid,$product['product_code']);
		$rating = $rate['rating'];
		$averagerating = $rate['averagerating'];

		?>
		<script>
			var obj = <?php echo (json_encode($product));?> ;
			var store = <?php echo (json_encode($this->session->userdata('store_name'))); ?>;
		</script>

		<div class="col-md-4 col-sm-12 col-sm-12 store-product">
			<div class="product-cover">
				<div class="nauk-info-connections">
					<div class="box14">
						<img src="<?php echo base_url('site_assets/products/') . $product['pro_image']; ?>" alt="">
						<div class="box-content">
							<h3 class="title"><?php echo stripslashes($product['product_name']); ?></h3>
							<span class="post"><?php echo $product['product_short_description']; ?></span>
							<ul class="icon">
								<li><a onclick="myFunction(<?php echo $product['id']; ?>,obj);" ><i style="padding-right:3px;" class="fa fa-cart-plus"></i></a></li>
								<li><a onclick="addProductClick(obj,store); return !ga.loaded;" href="<?php echo base_url('home/showProduct/') . $product['product_code']; ?>"><i class="fa fa-info"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="nauk-info-connections detail">
					<p class="store-product-name"><?php echo stripslashes($product['product_name']); ?></p>
					<?php 
					//print_r($product);
					if ((@$product['co_discount'] != '') && 
						(@$product['co_discount'] > 0 ) && 
						(@$product['max_use'] > 0 ) && 
						(@$product['expire_date'] >= date('Y-m-d') ) &&
						(@$product['active_date'] <= date('Y-m-d') ) &&
						(@$product['expire_date'] != '' ) &&
						(@$product['active_date'] != '' )
					) 

					{ ?>
						<div class="page-header">
							<div class="pull-left"><p class="store-product-price product-price-strike margin-bottom-none">$<?php echo $product['product_price'];?></p></div>
							<div class="pull-right">
								<p class="store-product-price margin-bottom-none">$<?php echo calculate_dicounted_price($product['product_price'],@$product['co_discount']);?>
							</p>
						</div>
						<div class="clearfix"></div>
						<p class="store-product-detail">Coupon <?php echo @$product['co_discount']; ?>% </p>
					</div>
				<?php } else { ?> 

					<div class="page-header">
						<div class="pull-left"><p class="store-product-price margin-bottom-none">$<?php echo $product['product_price'];?></p></div>

						<div class="clearfix"></div>
						<p class="store-product-detail"></p>
					</div>


				<?php }

				$comments = $this->db->query('select id from comments 
					where product_id="'.$product['product_code'].'"');
				$comments = $comments->num_rows();   
				?>
			</div>

			<div class="nauk-info-connections store-product-rating product-rating-comment">
				<div class="page-header">
					<div class="pull-left">
						<?php
						$rating_value = round($averagerating);

						?>
						<span onclick="rating(1)"  class="fa fa-star <?php if($rating_value>=1){echo ' start-checked';}else{echo 'start-unchecked';} ?>"></span>
						<span onclick="rating(2)"  class="fa fa-star <?php if($rating_value>=2){echo ' start-checked';}else{echo 'start-unchecked';} ?>"></span>
						<span onclick="rating(3)"  class="fa fa-star <?php if($rating_value>=3){echo ' start-checked';}else{echo 'start-unchecked';} ?>"></span>
						<span onclick="rating(4)"  class="fa fa-star <?php if($rating_value>=4){echo ' start-checked';}else{echo 'start-unchecked';} ?>"></span>
						<span onclick="rating(5)"  class="fa fa-star <?php if($rating_value>=5){echo ' start-checked';}else{echo 'start-unchecked';} ?>"></span>
						<span class="product-rating"><?= $averagerating ?></span>
					</div>
					<div class="pull-right">
						<a rel="external" href="<?php echo base_url('home/showProduct/'.$product['product_code']); ?>">
							<span class="product-comment"><?php echo $comments; ?></span>
							<span class="fa fa-comment product-comment"></span>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" cust="<?php echo @$countp; ?>" value="<?php echo @$countp; ?>" class="getcount" />	
	<input type="hidden" value="<?php echo @$total_store_product_count; ?>" id="total_store_product_count" />
<?php }} ?>

<!-- GA: EC -->
<?php 
$enable = 0;
if(isset($search) && isset($products)) {
	if(count($products) > 0){
		$enable = 1;
	}
}
?>

<script>
	$(document).ready(function(){
		var enable = <?php echo (json_encode($enable)); ?>;

		if(enable != '0' ){
			var store = <?php echo (json_encode($this->session->userdata('store_name'))); ?>;
			var obj = <?php echo (json_encode($products)); ?>;
			addImpressionProductAll(obj,store);
		}
	});

</script>