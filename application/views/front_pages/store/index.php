</div>
<style>
.search{
	display: none;
}
</style>

<?php 
$banner = $store_data['banner_image'];
if ($banner){
	$banner_path ='site_assets/stores/'.$banner; 
}
else{
	$banner_path ='site_assets/images/back1.jpg'; 
}


if($store_data['banner_text']) { ?>
	<div class="container-fluid home-container-full row-background" style='background: url(<?php echo base_url($banner_path);?>) no-repeat center; margin-top: -30px;'>
		<br>

		<div  id="site_about" class="row home-row-1 home-row">
			<div class="col-md-12 col-overlay col1 text-left">
				<p class="paragraph-text-sm-white text-left">
					<?php echo $store_data['banner_text'];?>
				</p>
			</div>

			<div class="col-md-12 text-left col2" >
				<a style="opacity:0;height:0px;" href="#" class="btn-lg-default btn"></a>
			</div>
		</div>

	</div>
<?php } ?>
<div class="container">
	<div class="row store-body mx-auto">

		<div class="col-md-12 store-inner-body text-left">
			<h2 class="heading-lg-green"><?php echo $store_data['welcome_head'];?></h2>
			<p class="paragraph-text-md-black-blod" style="font-weight:400; font-size:16px">
				<?php echo $store_data['welcome_text'];?>
			</p>
		</div>
	</div>
</div>

<?php if (count($feature_products) > 0){  ?>
	<div class="container"  style="max-width:960px; background: rgb(250,250,250); border-radius: 10px; padding: 20px;">
		<?php $align = 0; foreach($feature_products as $product){ ?>
			<script>
				var obj = <?php echo (json_encode($product));?> ;
				var store = <?php echo (json_encode($this->session->userdata('store_name'))); ?>;
			</script>

			<?php if ($align == 0) { ?>
				<div class="row store-product-list">
					<div class="col-md-8 col-sm-12 col-sm-12 store-product" style="padding-top:50px">
						<p class="paragraph-text-sm-grey text-center" style="font-weight:400;font-size:15px;">
							<?php echo $product['feature_description'];?>
						</p>
					</div>
					<div class="col-md-4 col-sm-12 col-sm-12 store-product">
						<div class="product-cover" style="background:#fff; border-radius:10px;">
							<div class="nauk-info-connections text-center store-front-product-block">	
								<img class="store-front-product-image" src="<?php echo base_url();?>site_assets/products/<?php echo $product['feature_image'];?>">
							</div>
							<div class="nauk-info-connections load-more detail text-center" style="margin-bottom:20px">
								<a onclick="addProductClick(obj,store); return !ga.loaded;" href="<?php echo base_url('home/showProduct/') . $product['product_code']; ?>" class="load-more-btn btn">More info</a>
							</div>
						</div>
					</div>
				</div>

				<?php $align = 1; } else { ?>

					<div class="row store-product-list">
						<div class="col-md-4 col-sm-12 col-sm-12 store-product">
							<div class="product-cover"  style="background:#fff; border-radius:10px;">
								<div class="nauk-info-connections text-center store-front-product-block">
									<img class="store-front-product-image" src="<?php echo base_url();?>site_assets/products/<?php echo $product['feature_image'];?>">
								</div>
								<div class="nauk-info-connections load-more detail text-center" style="margin-bottom:20px">
									<a onclick="addProductClick(obj,store); return !ga.loaded;" href="<?php echo base_url('home/showProduct/') . $product['product_code']; ?>" class="load-more-btn btn">More info</a>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-sm-12 col-sm-12 store-product" style="padding-top:50px">
							<p class="paragraph-text-sm-grey text-center" style="font-weight:400;font-size:15px;">
								<?php echo $product['feature_description'];?>
							</p>
						</div>
					</div>

					<?php $align = 0; }  } ?>
					<div class="row">
						<div class="col-md-12 text-center" >

						</div>
					</div>

				</div>

			<?php }  ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" >

						<a href="<?php echo base_url('home/collection');?>" class="btn-lg-default btn">Click Here to See all our Products</a> 
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row " >
					<div class="col-md-12 text-left" style="padding:50px 15px;" >
						<h2><?php echo $store_data['about_head'];?></h2>
						<p class="paragraph-text-md-green-blod" style="font-size:18px;font-weight:400;">
							<?php echo $store_data['about_text'];?>
						</p>
					</div>
				</div>
			</div>
			<div>