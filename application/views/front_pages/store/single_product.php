
<style type="text/css">

.product-class-text{
	font-size: 19px;
	font-weight: bold; background: linear-gradient(to right, #74a857 10%, #468fb1 70%);
	-webkit-background-clip: text;
	-webkit-text-fill-color: transparent;
}

</style>
<?php 
$success_msg = $this->session->flashdata('success_cart');
if(isset($success_msg)){ ?>
	<div class="row">
		<div class="text-center col-md-4 col-md-offset-4"><div id="alert-sml" class="col-md-12 alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <?php echo $success_msg; ?></div></div>
	</div>

<?php } ?> 



	<!--<div class="container-fluid row col-md-5">
		<img style="max-height: 300px;max-width: 300px;" class="img-thumbnail" src="<?php echo base_url(); ?>site_assets/products/<?php echo $product[0]->pro_image;?>" title="Product">
		<br/>
	</div>		
	<div class="container-fluid row col-md-7 ">

		<label class="col-md-12 s-product-name"><?php echo $product[0]->product_name;?></label>
		<div class="col-md-6"> <?php echo ($product[0]->product_visibility == 'live' ? 'Available' : 'Not Available') ?></div>
		<div class="col-md-6">$<?php echo $product[0]->product_price;?></div>
		<div class="col-md-4">
			<input type="number" id="<?php echo $product[0]->product_code;?>" placeholder="Enter Quantity" name="qty" class="form-control qty">
			<span class="text-danger" id ="number-error" style="display:none">Please Enter Quantity</span>
		</div>
		<div class="col-md-8">
			<a style="width: 200px;" href="#" class="btn btn-success add_cart" data-productname="<?php echo stripslashes($product[0]->product_name); ?>" data-price="<?php echo $product[0]->product_price;?>" data-productid="<?php echo $product[0]->product_code;?>">Add to Cart</a>

		</div>

		<label class="col-md-12">Description:</label>
		<p class="col-md-12"><?php echo $product[0]->product_short_description;?> </p>


		<br/>
	</div>	
</div>-->
<div class="row store-body mx-auto store-single-product">

	<div class="col-md-6 store-single-product-image text-center">	
		<img src="<?php echo base_url(); ?>site_assets/products/<?php echo $product[0]->pro_image;?>">
		<br><br>
	</div>
	<div class="col-md-6 store-inner-heading store-single-product-image">	
		<h4><?php echo $product[0]->product_name;?></h4>

		<div class="page-header">
			<div class="pull-left">
				<p class="status"><?php echo ($product[0]->product_visibility == 'live' ? 'Available' : 'Not Available') ?></p>
			</div>

			<div class="pull-right">
				<p class="price">$<?php echo $product[0]->product_price;?></p>
			</div>

			<div class="clearfix"></div>

		</div>
		<div class="page-header">
			<div class="pull-left">
				<input  type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="<?php echo $product[0]->product_code;?>" placeholder="Enter Quantity" name="qty" class="form-control qty  store-input">
				<span class="text-danger" id ="number-error" style="display:none">Please Enter Quantity</span>
			</div>

			<div class="pull-right">
				<a href="#" class="btn-green-dark btn add_cart" data-productname="<?php echo stripslashes($product[0]->product_name); ?>" data-price="<?php echo $product[0]->product_price;?>" data-productid="<?php echo $product[0]->product_code;?>">Add to Cart
				</a>

			</div>

			<div class="clearfix"></div>

		</div>

		<h5 class="store-heading">Description</h5>
		<p class="store-paragraph">
			<?php echo $product[0]->product_short_description;?> 
		</p>
	</div>

</div>


<!-- //product_long_description -->
<div class="row store-body mx-auto store-single-product store-product-detail">
	<div class="col-md-12">	
		<h5 class="paragraph-text-md-grey-blod">Long description</h5>
		<p class="store-paragraph">
			<?php echo $product[0]->product_long_description;?>
		</p>
	</div>
</div>

<!--<div class="container-fluid" style="margin: 30px 0 0 0;">
	
	<div class="row">
		<h5 class="col-md-12">Long description:</h5>
		<hr>
		<p class="col-md-12"><?php echo $product[0]->product_long_description;?> </p>

	</div>
</div>-->


<div class="row store-relevant-product mx-auto store-single-product store-product-detail">
	<?php  if(isset($cross_sell_products) && !empty($cross_sell_products)){ ?>
		<div class="col-md-12">
			<h5 class="paragraph-text-md-grey-blod">Relevant products</h5>
		</div>
		<?php 
		foreach ($cross_sell_products as $key => $product_rand) {

			?>
			<div class="col-md-3 text-center">
				<div class="relevant-product-detail text-center">
					<div class="image-section">
						<img src="<?php echo base_url(); ?>site_assets/products/<?php echo $product_rand->pro_image;?>">
					</div>
					<div class="info-section">
						<h5>
							<a class="product-class-text"
							href="<?php  echo base_url();?>home/showProduct/<?php echo $product_rand->product_code; ?>"><?php echo stripslashes($product_rand->product_name);?></a>
						</h5>
						<h5 class="store-heading"  style="font-weight:500;">$<?php echo $product_rand->product_price;?></h5>
					</div>
				</div>
			</div>
			<div class="col-md-9 store-inner-heading store-single-product-image">	
				<h5 class="store-heading">Description of product</h5>
				<p class="store-paragraph">
					<?php echo stripslashes($product_rand->product_long_description);?> 

				</p>
			</div>
			<?php 
		}
	} ?>
</div>


<div class="row store-relevant-product mx-auto store-single-product store-product-detail">
	<?php  if(isset($up_sell_products) && !empty($up_sell_products)){ ?>
		<div class="col-md-12">
			<h5 class="paragraph-text-md-grey-blod">Up-Sell Products</h5>
		</div>
		<?php 
		foreach ($up_sell_products as $key => $product_rand) {

			?>
			<div class="col-md-3 text-center">
				<div class="relevant-product-detail text-center">
					<div class="image-section">
						<img src="<?php echo base_url(); ?>site_assets/products/<?php echo $product_rand->pro_image;?>">
					</div>
					<div class="info-section">
						<h5><a class="product-class-text" href="<?php  echo base_url();?>home/showProduct/<?php echo $product_rand->product_code; ?>"><?php echo stripslashes($product_rand->product_name);?></a></h5>
						<h5 class="store-heading"  style="font-weight:500;" >$<?php echo $product_rand->product_price;?></h5>
					</div>
				</div>
			</div>
			<div class="col-md-9 store-inner-heading store-single-product-image">	
				<h5 class="store-heading">Description of product</h5>
				<p class="store-paragraph">
					<?php echo stripslashes($product_rand->product_long_description); ?>

				</p>
			</div>
			<?php 
		}
	} ?>
</div>


<!--
<div class="container-fluid" style="margin: 30px 0 0 0;">
	<?php  if(isset($cross_sell_products) && !empty($cross_sell_products)){ ?>
		<div class="row">
			<h5 class="col-md-12">Check these products:</h5>
			<?php 
			foreach ($cross_sell_products as $key => $product_rand) {

				?>
				<div class="row col-md-12">
					<div class="col-md-3 text-center">
						<img style="max-height: 200px;max-width: 200px;" class="img-thumbnail" src="<?php echo base_url(); ?>site_assets/products/<?php echo $product_rand->pro_image;?>" title="Product"><br>
						<label class="text-center"><a class=" heading-simple-grey" href="<?php  echo base_url();?>home/showProduct/<?php echo $product_rand->product_code; ?>"><?php echo stripslashes($product_rand->product_name);?></a>
						</label><br>
						<label class="text-center">$<?php echo $product_rand->product_price;?>
					</label><br>
				</div>
				<div class="col-md-9">
					<h4>Description of Product</h4>
					<?php echo stripslashes($product_rand->product_long_description);?> 
				</div>
			</div>

		</div>
		<?php 
	}
} ?>
</div>
-->
<div class="row store-relevant-product mx-auto store-single-product store-product-detail">
	<div class="col-md-12">	
		<h5 class="paragraph-text-md-grey-blod">End user agreement</h5>
		<p class="store-paragraph" style="font-weight:500;">
			<?php echo $product[0]->license_editor; ?>
		</p>
	</div>
</div>
<!--
<div class="container-fluid" style="margin: 30px 0 0 0;">
	<div class="row">
		<h5 class="col-md-12">End user agreement</h5>
		<p class="col-md-12"> 
			<?php echo $product[0]->license_editor; ?>
		</p>

	</div>
</div>
-->

<div class="row store-body mx-auto single-product-comments">
	<div class="col-md-12">	

		<div class="nauk-info-connections text-left">
			<h5 class="comments-heading">Comments/Questions</h5>
		</div>
		<?php
		if(@$already=='0' && @$login=='1'){
			?>

			<form method="post" action="<?php echo base_url('customer/comments') ?>">
				<input type="hidden" name="product_id" value="<?php echo @$product[0]->product_code; ?>" />
				<input type="hidden" name="customer_id" value="<?php echo @$cu_id; ?>" />
				<input type="hidden" name="front" value="<?php echo @$cu_id; ?>" />

				
				<div class="nauk-info-connections post-area">

					<div class="form-group">
						<textarea name = "comment_text"  max-length="1000" class = "form-control" id="comment-area" rows="5" placeholder="Post new comment"></textarea>
						<span id="comment-error"></span>
					</div>

				</div>

				<div class="nauk-info-connections text-left">
					<input type="submit" class="btn" value="submit" id="post-btn"/>
				</div>



				

			</form>
			<?php
		}
		?>

		<div class="nauk-info-connections" >

			<div class="row comments">
				<?php
				if(count(@$comments)>0){
					foreach(@$comments as $comment){
						?>
						<div class="col-md-12 comment-info">
							<div class="nauk-info-connections text-left">
								<h2 class="comment-user-name"><?php echo @$comment['f_name']." ".@$comment['l_name']; ?></h2>
								<p class="comment-text"> <?php echo @$comment['content']; ?></p>
								<p class="comment-time"><?php echo timeAgo(@$comment['created_date']); ?></p>
							</div>	
						</div>
						<?php 
					}
				}
				?>




			</div>

		</div>


	</div>

</div>
<?php
function timeAgo($time_ago)
{
	$time_ago = strtotime($time_ago);
	$cur_time   = time();
	$time_elapsed   = $cur_time - $time_ago;
	$seconds    = $time_elapsed ;
	$minutes    = round($time_elapsed / 60 );
	$hours      = round($time_elapsed / 3600);
	$days       = round($time_elapsed / 86400 );
	$weeks      = round($time_elapsed / 604800);
	$months     = round($time_elapsed / 2600640 );
	$years      = round($time_elapsed / 31207680 );
    // Seconds
	if($seconds <= 60){
		return "just now";
	}
    //Minutes
	else if($minutes <=60){
		if($minutes==1){
			return "one minute ago";
		}
		else{
			return "$minutes minutes ago";
		}
	}
    //Hours
	else if($hours <=24){
		if($hours==1){
			return "an hour ago";
		}else{
			return "$hours hrs ago";
		}
	}
    //Days
	else if($days <= 7){
		if($days==1){
			return "yesterday";
		}else{
			return "$days days ago";
		}
	}
    //Weeks
	else if($weeks <= 4.3){
		if($weeks==1){
			return "a week ago";
		}else{
			return "$weeks weeks ago";
		}
	}
    //Months
	else if($months <=12){
		if($months==1){
			return "a month ago";
		}else{
			return "$months months ago";
		}
	}
    //Years
	else{
		if($years==1){
			return "one year ago";
		}else{
			return "$years years ago";
		}
	}
}
?>

<script type="text/javascript">

	$(document).ready(function(){

		$('.alert').css('display','none');
		$('.add_cart').click(function(){
			var product_id = $(this).data("productid");
			var product_name = $(this).data("productname");
			var product_price = $(this).data("price");
			var quantity = $('#' + product_id).val();
			if(quantity != '' && quantity > 0)
			{

				$.ajax({
					url:"<?php echo base_url(); ?>Cart/add_to_cart",
					method:"POST",
					data:{product_id:product_id, product_name:product_name, product_price:product_price, quantity:quantity},
					success:function(data)
					{

						$('#' + product_id).val('');

						window.setTimeout(function() {
							window.location.href = '<?php echo base_url(); ?>Home/showCart';
						}, 1000);
				    // window.location.href = '<?php //echo base_url(); ?>Home/showCart';
				}
			});
			}
			else
			{
				$("#number-error").css({ display: "block" });
			}
		});
	});
</script>
