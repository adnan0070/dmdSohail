<!-- left-menu-start-->
<div class="col-md-2 store-left-menu">
	<ul><span class="menu-name"> Filter</span><br><br>
		<li  class="menu-item">Price</li>
		
		<div id="slider" data-role="rangeslider" class="price-range-slider">
			<input type="range" name="price-min" class="price-range-min" id="amount_min" value="1"	 min="1" max="1000">
			<label class="ui-range-slider-label">to</label>
			<input type="range" name="price-max" class="price-range-max" id="amount_max" value="1000" min="1" max="1000">
		</div>
		
	</ul>
	<br><br>
	<button id="apply">Apply</button>

	<link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
	<script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
</div><!-- left-menu-end-->
