<div class="box-body">
	<div class="row clearfix">

		<div class="col-md-4 modal-product-image text-center">
			<img src="<?php echo base_url('site_assets/products/') . $product[0]->pro_image; ?>">
		</div>
		<div class="col-md-8 modal-product-detail">
			<h4 class="modal-cart-head"> Add <?php echo $product[0]->product_name;?> to cart</h4>
			<div class="page-header">
				<div class="pull-left">
					<p class="modal-cart-price">$<?php echo $product[0]->product_price;?></p>
				</div>

				<div class="pull-right">
					<p class="modal-cart-status"><?php echo $product[0]->product_visibility; ?></p>
				</div>

				<div class="clearfix"></div>

			</div>
			<input type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="<?php echo $product[0]->product_code;?>" placeholder="Quantity" name="qty" class="form-control modal-cart-input store-input qty">
			<div id="msg_err" class="text-danger"></div>
			<!-- <input type="text" placeholder="Quantity" class="form-control store-input modal-cart-input"> -->
			<p class="modal-cart-total">Total $<?php echo $product[0]->product_price;?></p>
			<!-- <a class="btn-green-dark btn" href="#">add to cart</a> -->
			<a href="#" id="exitModal" class="btn-green-dark btn add_cart" data-productname="<?php echo stripslashes($product[0]->product_name); ?>" data-price="<?php echo $product[0]->product_price;?>" data-productid="<?php echo $product[0]->product_code;?>">Add to Cart</a>
			<br><br>
			
			<div id="msg" style="color: green"></div>
		</div>

	</div>
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('.alert').css('display','none');
		$('.add_cart').click(function(){
			var product_id = $(this).data("productid");
			var product_name = $(this).data("productname");
			var product_price = $(this).data("price");
			var quantity = $('#' + product_id).val();
			if(quantity != '' && quantity > 0){
				$.ajax({
					url:"<?php echo base_url(); ?>Cart/add_to_cart",
					method:"POST",
					data:{product_id:product_id, product_name:product_name, product_price:product_price, quantity:quantity},
					success:function(data){
						$("#msg_err").text("");
						$('#' + product_id).val('');
						$("#msg").html("Item has been successfully added to cart!");
						$(".qty").focus();
						window.setTimeout(function(){
							$('#myModal').modal('toggle');
							location.reload();
						},50);
						
					}
				});
			}else{
				$("#msg_err").text("Please Enter Quantity");
				//alert("");
			}
		});

		$(".qty").on("keyup", function(){
			$("#msg").html("");
		});
	});
</script>