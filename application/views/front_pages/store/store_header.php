<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script>

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-147349522-1', 'auto');

		ga("require", "ec");


	</script>

	<title>DMD</title>
	<meta charset="utf-8">	
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>site_assets/images/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--   Meta Tags -->
	<?php $metaTags = getMetatags(); 
	foreach($metaTags as $row){
		echo $row['content'];
	}
	?>
	<!--  Meta Tags -->

	<!-- CSS ////////////////////////////////////////////////// -->
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap-grid.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap-reboot.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/home.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/responsive.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/datepicker/css/bootstrap-datepicker.standalone.css" rel="stylesheet">
	<!--FontAwesome Font Style -->
	<link href="<?php echo base_url(); ?>site_assets/css/font-awesome.min.css" rel="stylesheet">
	<!-- <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
	<!--<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">-->
	<!-- font-family: 'Montserrat', sans-serif; -->

	<link href="<?php echo base_url(); ?>site_assets/countrySelect/dist/css/bootstrap-formhelpers.min.css" rel="stylesheet">


	<!-- JS ///////////////////////////////////////////////////-->

	<script src="<?php echo base_url(); ?>site_assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/topbutton.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/events.js"></script>

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/sl-1.2.6/datatables.min.css"/>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/sl-1.2.6/datatables.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>site_assets/countrySelect/dist/js/bootstrap-formhelpers.min.js"></script>


	<!-- tracking -->
	<?php $trackers = getTracking(); 
	foreach($trackers as $row){
		echo $row['header_content'];
	}
	?>
	<!-- tracking --> 
	
</head>

<style>
.badge{
	background-color: #74a857;
	color: white;
	height: 16px;
	width: 16px;
	margin-top: -20px;
	font-weight: 600;
	border-radius: 50%;
	position: relative;
	top: -8px;
}
</style>
<?php
$cart_count = count($this->cart->contents());
$badge = "";
if(@$cart_count!=0){
	
	$badge = '<span class="badge">'.$cart_count.'</span>';  
}

if( ! isset($sub_store_name)){
	$sub_store_name = $this->session->userdata('store_name');
}


?>

<body>  
	<!-- tracking --> 
	<?php  
	foreach($trackers as $row){
		echo $row['body_content'];
	}
	?>
	<!-- tracking --> 

	<div class="se-pre-con"></div>
	<script>
	 	/*
	 	$(window).load(function() {
			$(".se-pre-con").fadeOut(1000);
		});*/
		$(document).ready(function(){
			$.topbutton({
				html : "<i>Top</i>",      
                css : "width:50px; height:50px; background:#468fb1; border:none; font-size:20px;",  //String
                scrollAndShow : true,   
                scrollSpeed : 1000        
            });

		});
	</script>
	<div class="container store">
		<div class="row header">
			<div class ="col-md-2 logo text-left text-center">
				
				<?php
				$store_logo = $this->session->userdata('logo');
				if(!empty($store_logo)){ ?>
					
					<img onerror="this.onerror=null; this.src='<?php echo base_url(); ?>site_assets/images/logo.png'" src="<?php echo base_url('site_assets/stores/') . $store_logo; ?>" style="max-width:70px !important;" />
				<?php  } else { ?>
					<img src="<?php echo base_url(); ?>site_assets/images/logo.png">
				<?php } ?>
			</div>
			<div class ="col-md-2 text-left store-head text-center">
				<!--<?php
				if(isset($store_logo)){
					$method = $this->router->fetch_method();
					if($method == 'checkout' || $method == 'showCart'){
						?>
						<img src="<?php echo base_url('site_assets/products/') . $store_logo; ?>" height="84" width="84" />
					<?php }else{ ?>
						<h4 class="heading-lg-red"><?php echo (!empty($sub_store_name) ? $sub_store_name : 'Store'); ?></h4>
					<?php } 
				}else{ ?>
					<h4 class="heading-lg-red"><?php echo (!empty($sub_store_name) ? $sub_store_name : 'Store'); ?></h4>
					<?php } ?> -->
					<h4 class="heading-lg-red"><?php echo (!empty($sub_store_name) ? $sub_store_name : 'Store'); ?></h4>
				</div>

				<div class ="col-md-8 mx-auto store-top-menu text-center">

					<div class="page-header">
						<!-- Search Box -->
						
						<div class="pull-right">
							<ul class="list-inline">
								<li class="list-inline-item item-link"><a rel="external" href="<?php echo base_url(); ?>">Home</a></li>
								<li class="list-inline-item item-link"><a rel="external" href="<?php echo base_url('home/collection'); ?>">Products</a></li>
								<li class="list-inline-item item-link"><a rel="external" href="<?php echo base_url('home/showCart'); ?>">Cart<?php echo $badge; ?></a></li>
								<li class="list-inline-item item-link"><a rel="external" href="<?php echo base_url();?>">About us</a></li>
								<li class="list-inline-item item-link"><a rel="external" href="<?php echo base_url('home/contact'); ?>">Contact</a></li>

								<?php if($this->session->userdata('customer_details')){  ?>
									<li class="list-inline-item item-link"><a rel="external" href="<?php echo base_url('customer/dashboard'); ?>">Dashboard</a></li>
									<li class="list-inline-item item-link"><a rel="external" href="<?php echo base_url('customer/purchase'); ?>">Purchases</a></li>
									<li class="list-inline-item item-link"><a rel="external" href="<?php echo base_url('customer/profile'); ?>">Profile</a></li>
									<li class="list-inline-item item-link"><a rel="external" href="<?php echo base_url('customer/logout'); ?>">Logout</a></li>
								<?php }else{	?>
									<li class="list-inline-item item-link"><a rel="external" href="<?php echo base_url('customer/login'); ?>">Login</a></li>
								<?php } ?>
							</ul>
						</div>

						<div class="clearfix"></div>
					</div>

				</div>
			</div>
			<div class="row search store-header-search" style="padding-right: 10px;">
				<?php
				$method = $this->router->fetch_method();
				$class = $this->router->fetch_class();
				$customer_details = $this->session->userdata('customer_details');

					//$class = $this->router->fetch_class();
					//print_r($method);
					//if ($method != 'showCart') {
					//	if($class!='customer'){
				if(isset($payment_method) && !empty($payment_method)){
					if($method != 'showCart' && $class != 'customer'){
						?>
						<div class="col-md-12 text-right">
							<div class="page-header">	
								<div class="pull-left">
								</div>
								<div class="pull-right">
									<div class="input-group ">
										<input type="text" id="searchCart" class="form-control" placeholder="Search Text">
										<div class="input-group-append">
											<button class="btn btn-secondary" type="button">
												<i class="fa fa-search"></i>
											</button>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<?php
					}
				}

				?>
			</div>
