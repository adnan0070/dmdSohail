<style type="text/css">
  .section-login-signup .form-input{
    max-width: 100% !important;
  }
</style>
<div class="section-login-signup container-fluid "> 
			<div class="col-md-12 "><!-- inner-body-start-->

				<div class="row">

					<div class="col-md-12 "><!-- full block start-->
						<div class="nauk-info-connections">
							<div class="page-header">
								<div class="pull-center">
									<h2 class="heading-lg-green">Profile</h2>
									<p class="paragraph-text-sm-grey">Edit your profile and information here</p>
								</div>
								
								<div class="clearfix"></div>
							</div>
						</div>
					</div><!-- block end-->
				</div>
        <div class="row">
           <div class="col-md-12">
            <?php
              $success_msg= $this->session->flashdata('success_msg');
              $error_msg= $this->session->flashdata('error_msg');
              $user_name = $this->session->userdata('user_name');
              $user_name = explode(" ",$user_name);
 
                  if($success_msg){
             ?>
                    
                   <div id="alert-sml" class="col-md-12 alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <?php echo $success_msg; ?></div>
                  <?php
                  }
                  if($error_msg){
                    ?>
              
                     <div id="alert-sml" class="col-md-12 alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <?php echo $error_msg; ?></div>
                    <?php
                  }
                  ?>
            </div> 
        </div>
                

            
  <div class="col-md-12"> 

    <form class="row" method="post" action="<?php echo base_url('customer/edit'); ?>" >
           

            <div class="col-md-12">
                    <div class="form-group input-effects">
                      <input type="text" value="<?php echo $customer_data['f_name']; ?>"  name = "firstName" class = "home-input" id="first-name" required  placeholder=""/>
                       <label>first name</label>
                     <span class="focus-border"></span>
                      <!--<span class="text-danger">Validation error</span>-->
                    </div>
                  </div> 
                  <div class="col-md-12">
                    <div class="form-group input-effects">
                      <input type="text" value="<?php echo $customer_data['l_name']; ?>"  name = "lastName" class = "home-input" id="last-name" required  placeholder=""/>
                       <label>last name</label>
                     <span class="focus-border"></span>
                      <!--<span class="text-danger">Validation error</span>-->
                    </div>
                  </div> 


                  <div class="col-md-12 mx-auto">
                    <div class="nauk-info-connections text-center">
                      <button type="button" class= "btn-sm-blue btn" id="cPass">change password</button>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group input-effects">
                      <input type="password"  
                        name = "currentPassword"
                       
                        class = "home-input"
                        id="current-password"
                         
                        placeholder=""
                        style="display:none;"
                      />
                       <label>current password</label>
                     <span class="focus-border"></span>
                    </div>
                  </div> 
            
                  <div class="col-md-12">
                    <div class="form-group input-effects">
                      <input type="password"  
                        name = "password"
                        class = "home-input" 
                        id="password"
                        placeholder=""
                        pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
                        title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                        style="display:none;"
                      />
                       <label>new password</label>
                     <span class="focus-border"></span>
                    </div>
                  </div> 

                  <div class="col-md-12">
                    <div class="form-group input-effects">
                      <input type="password"
                        name = "ConfPassword" 
                        onkeyup='check();' 
                        class = "home-input" 
                        id="conf-password"
                         
                        placeholder=""
                        pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
                        title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                        style="display:none;"
                      />
                       <label>re-type new password</label>
                     <span class="focus-border"></span>
                      <span class="" id='pass_message'></span>
                    </div>
                  </div> 
            <div class="col-md-12 mx-auto">
              <div class="nauk-info-connections text-center">
                <input class="btn-sm-blue btn" type="submit" id="save" value="save" name="save" >
                                
              </div>
            </div>  
 
        </form>

  </div>
				
			</div>
		</div>

  <script>
 var check = function() {
  if (document.getElementById('password').value ==
    document.getElementById('conf-password').value) {
    document.getElementById('pass_message').style.color = 'green';
    document.getElementById('pass_message').innerHTML = 'matching';
  } else {
    document.getElementById('pass_message').style.color = 'red';
    document.getElementById('pass_message').innerHTML = 'not matching';
  }
}

  $("#cPass").on("click", function(){
    $("#password, #conf-password, #current-password").show();
    $("#password, #conf-password, #current-password").attr('required','required');
    $(this).hide();
    
  });
 </script> 