<style type="text/css">
.totals .totals-item {
	float: left;
	clear: both;
	width: 100%;
	/*margin-bottom: 10px;*/
}
.totals .totals-item label {
	float: left;
	clear: both;
	width: 79%;
	text-align: left;
}
.totals .totals-item .totals-value {
	float: left;
	width: 21%;
	text-align: left;
}
</style>
<div id="loading">
	<img id="loading-image" src="<?php echo base_url(); ?>site_assets/images/loader-image.gif" alt="Loading..." />
</div>
<div class="row store-body mx-auto">
	<?php //$this->load->view('front_pages/store/store_left'); ?>
	<?php $data = $this->session->flashdata('data'); ?>

	<?php //if(isset($enable_pm)){print_r($enable_pm);} ?>


	<div class="col-md-12 store-inner-heading">
		<div id="showError"></div>

		<?php if($this->session->flashdata('error_msg')){ ?>
			<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error! </strong><br><br><?php echo $this->session->flashdata('error_msg'); ?>
			</div>
		<?php } ?>

		<?php if($this->session->flashdata('success_msg')){ ?>
			<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Success! </strong><?php echo $this->session->flashdata('success_msg'); ?>
			</div>
		<?php } ?>


		<h4>Customer information</h4>
	</div>


	<div class="col-md-5 text-right store-cart text-right">
		<div class="list table-block table-responsive">
			<table class="table store-cart-table">
				<tbody>
					<tr class="cart-data">
						<td>Subtotal:</td>
						<td id="cart-subtotal">$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
					</tr>
					
					<tr class="cart-data">
						<td id="sTax">Tax (0%):</td>
						<td id="fTax">$0.00</td>
					</tr>
					<!-- <tr class="cart-data">
						<td>Coupon (10%):</td>
						<td>$-240</td>
					</tr> -->
					<tr class="cart-footer">
						<td>Total:</td>
						<td id="cart-total">$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>
			<!--<div class="col-md-4">
				<div class="totals" style="margin-top: 30%">
					<div class="totals-item">
  						<label>Subtotal:</label>
  						<div class="totals-value" id="cart-subtotal">$<?php //echo $this->cart->format_number($this->cart->total()); ?></div>
					    
  					</div>-->
					<!-- <div class="totals-item">
					  						<label>Tex(19%):</label>
					  						<div class="totals-value" id="cart-tex">$<?php //echo "200"; ?></div>
					</div>
					<div class="totals-item">
					  						<label>Coupon(10%):</label>
					  						<div class="totals-value" id="cart-coupon">$<?php //echo "300"; ?></div>
					  					</div> -->
					<!--<div class="totals-item">
						<label style="width: 100%"></label>
					</div>
					<div class="totals-item totals-item-total">
	  					<label>Total:</label>
	  					<div class="totals-value" id="cart-total">$<?php //echo $this->cart->format_number($this->cart->total()); ?></div>
					</div>
					<div class="totals-item">
						<label style="width: 100%"></label>
					</div>
				</div>
			</div>-->

			<div class="col-md-5 text-left store-customer-information">	
				<?php
				$stripe = false;
                
                // get user id by store id
				$user = $this->db->query('select user_id from dmd_stores where id="'.$this->session->userdata('store_id').'"');
				$user = $user->row_array();

                $keys_stripe = $this->db->query("select * from payment_methods where user_id = '".$user['user_id']."' 
					and `name`='stripe'");
                $keys_stripe = $keys_stripe->row_array();
	
				$stkey = json_decode($keys_stripe['configuration'],'array');
				$pubkey = $stkey['publishableApiKey'];
				?>

				 <input type="hidden" id="getpub" value="<?php echo $pubkey; ?>">
				
				<?php
				$keys = $this->db->query("select * from payment_methods where user_id = '".$user['user_id']."' 
					and `default`='1'");
				$keys = $keys->row_array();
                //print_r($keys);

				$suburl = "";
				if($keys['name']=='authorize'){
					$suburl = "authorize_net_payment";
				}
				if($keys['name']=='stripe'){
					$suburl = "stripe_payment";
					$stripe = true;
					$stkey = json_decode($keys['configuration'],'array');
					$pubkey = $stkey['publishableApiKey'];
				}
				if($keys['name']=='paypal'){
					$suburl = "buypay";
				}

				?>

				<form id="payment" method="post">

					<p class="heading-sm-grey" style="margin:0px 0px 5px 0px;">Personal information</p>	

					<div class="form-group">
						<input type="text" 
						name="f_name" 
						placeholder="First Name" 
						class="form-control store-input validThis" 
						value="<?php if(isset($data['f_name'])){echo $data['f_name'];} ?>"/>
					</div>
					<input type="hidden" 
					name="store_id" 
					value="<?php echo $this->session->userdata('store_id'); ?>"/>

					<div class="form-group">
						<input type="text" 
						name="l_name" 
						placeholder="Last Name" 
						class="form-control store-input validThis" 
						value="<?php if(isset($data['l_name'])){echo $data['l_name'];} ?>"/>
					</div>

					<div class="form-group">
						<input type="text" 
						name="city" 
						placeholder="City" 
						class="form-control store-input validThis" 
						value="<?php if(isset($data['city'])){echo $data['city'];} ?>"/>
					</div>

					<div class="form-group">
						<input type="text" 
						name="state" 
						placeholder="State" 
						class="form-control store-input validThis" 
						value="<?php if(isset($data['state'])){echo $data['state'];} ?>"/>
					</div>
					<input type="hidden" name="customer" value="customer" />

					<!-- <div class="form-group">
						<input type="text" 
						name="country" 
						placeholder="Country" 
						class="form-control store-input validThis" 
						value="<?php if(isset($data['country'])){echo $data['country'];} ?>"/>
					</div> -->

					<div class="form-group">
						<select name="country" id="cList" class="bfh-countries store-input validThis" data-country="US"></select>
					</div>

					<div class="form-group">
						<input type="text" 
						name="email" 
						placeholder="Email Address" 
						class="form-control store-input validThis" 
						value="<?php if(isset($data['email'])){echo $data['email'];} ?>"/>
					</div>

					<p class="heading-sm-grey" style="margin:10px 0px 5px 0px;">Payment methods</p>
					<div class="form-group">

						<select type="text" onchange="changePayment()" id="changePM" placeholder="Payment methods" class="form-control store-input">
							<?php

								foreach ($enable_pm as $value) {
									$selected="";
									if($value["default"] == 1){
										$selected = "selected";
									}
									echo '<option '.$selected.' value="'.$value["name"] .'">'.$value["name"].'</option>';
								
								}
							?>
							
						</select>
					</div>

						<div class="form-group stripe_payment" style="display:none;">
							<!-- <label for="card-element">Credit or debit card</label> -->
							<div id="card-element"></div>
							<div id="card-errors" role="alert"></div>
						</div>

					      <div class="authorize_show" style="display:none;">
							<div class="form-group">
								<input type="text" name="street" placeholder="Street" class="form-control store-input" />
							</div>

							<div class="form-group">
								<input type="text" name="zip" placeholder="Zip" class="form-control store-input" />
							</div>

							<div class="form-group">
								<input type="text" name="creditcard" placeholder="Credit Card Number" class="form-control store-input" />
							</div>

							<div class="form-group">
								<input type="text" name="expiry" placeholder="Expiry (MM/YY)" class="monthpicker form-control store-input" />
							</div>

							<div class="form-group">
								<input type="text" name="cvc" placeholder="CVC" class="form-control form-input store-input" />
							</div>

						</div>	
                    

						<div class="form-footer">
							<?php
								if($suburl!=""){ ?>
									<button type="submit" id="paySubmit" rel="external" class="btn btn-sm-blue" >Submit</button>
								<?php
								}else{
									echo '<p>No payment method setup</p>';
								}
							?>
						</div>
					</form>

				</div>


			</div>

<script type="text/javascript">
$(document).ready(function(){
	changePayment();
});
	  
function changePayment(){
	var method = $("#changePM").val();
	var action = "";
	if(method == "stripe"){
	    action =  'stripe_payment';
		$(".stripe_payment").show();
		$(".authorize_show").hide();
		$(".pay_authorize").show();
	}
	if(method == "paypal"){
		action =  'buypay';
		$(".stripe_payment").hide();
		$(".authorize_show").hide();
		$(".pay_authorize").show();
	}
	if(method == "authorize"){
		action =  'authorize_net_payment';
		$(".authorize_show").show();
		$(".pay_authorize").show();
		$(".stripe_payment").hide();
	}

	$("#payment").attr("action","<?php echo base_url('user/');?>"+action);
}



</script>

<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">

	//var stripe = Stripe('<?php echo $pubkey; ?>');
	var getpub = $("#getpub").val();
    var stripe = Stripe(getpub);

	// Create an instance of Elements.
	var elements = stripe.elements();

	var style = {
		base: {
			color: '#32325d',
			lineHeight: '18px',
			fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
			fontSmoothing: 'antialiased',
			fontSize: '16px',
			'::placeholder': {
				color: '#aab7c4'
			}
		},
		invalid: {
			color: '#fa755a',
			iconColor: '#fa755a'
		}
	};

  		var card = elements.create('card', {style: style});
		card.mount('#card-element');
		card.addEventListener('change', function(event) {
			var displayError = document.getElementById('card-errors');
			if (event.error) {
				displayError.textContent = event.error.message;
			} else {
				displayError.textContent = '';
			}
		});

		var form = document.getElementById('payment');
		form.addEventListener('submit', function(event) {
			event.preventDefault();

			//$("#loading").css("display","block");
			var method = $("#changePM").val();
			if(method != "stripe"){
				formValidate('payment');
				//form.submit();
			}else{
				stripe.createToken(card).then(function(result) {
					if (result.error) {
			      		// Inform the user if there was an error.
			      		var errorElement = document.getElementById('card-errors');
			      		errorElement.textContent = result.error.message;
			      		//console.log(result.error.message);
			      	} else {
			      		// Send the token to your server.
			      		stripeTokenHandler(result.token);
			      		//console.log(result.token);
			      	}
			    });
			}
		});

		function stripeTokenHandler(token) {
			var form = document.getElementById('payment');
			var hiddenInput = document.createElement('input');
			hiddenInput.setAttribute('type', 'hidden');
			hiddenInput.setAttribute('name', 'stripeToken');
			hiddenInput.setAttribute('value', token.id);
			form.appendChild(hiddenInput);
			//form.submit();
			formValidate('payment');
		}
</script>

<script type="text/javascript">
	$('#cList').on('change', function(event){
		event.preventDefault();
		var country = $(this).val();
		var store = "<?= $this->session->userdata('store_id'); ?>";

		$.ajax({
			url: "<?= base_url('store/get_tax') ?>",
			method: "POST",
			data: { 'country' : country, 'store' : store },
			success: function(data){
				console.log(data);
				var tax = JSON.parse(data);
				console.log(tax);
				if(tax){
					$('#sTax').html('Tax (' + tax.price + '%):');

					var total = '<?= $this->cart->total(); ?>';
					var perc = (total / 100) * tax.price;
					$('#fTax').html('$' + perc);

					var gTotal = Number(total) + Number(perc);
					$('#cart-total').html('$' + gTotal);
					//console.log(gTotal);

					$.ajax({
						url: "<?= base_url('store/update_tax_in_session'); ?>",
						method: "POST",
						data: {'cartTotal' : gTotal, 'taxAmount' : perc, 'subTotal': total},
						success: function(msg){
							console.log(msg);
						}
					});
				}

			}
		});
	});
</script>
