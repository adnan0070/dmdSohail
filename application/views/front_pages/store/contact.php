<div class="row">
	<div class="col-md-6 text-center mx-auto">
		<?php if($this->session->flashdata('success'))  {?>
			<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('success_msg'); ?>
			</div>
		<?php } if ($this->session->flashdata('error')) {  $error = true; ?>
			<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('error_msg'); ?>
			</div>
		<?php } ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12 text-center static-contact-us">
		<h4>Contact Us</h4>
		<form method="post" action="<?php echo base_url('home/contact')?>">

			<div class="form-group input-effects">
				<input type="text"  name="name" value="" class="home-input" placeholder="">
				<label>Name</label>
				<span class="focus-border"></span>
			</div>


			<div class="form-group input-effects">
				<input type="email"  name="email" value="" class="home-input" placeholder="">
				<label>E-mail</label>
				<span class="focus-border"></span>
			</div>

			<div class="form-group input-effects">
				<input type="text"  name="subject" value="" class="home-input" placeholder="">
				<label>Subject</label>
				<span class="focus-border"></span>
			</div>


			<textarea placeholder="description" name="message" maxlength="1000" rows="7" class="form-control form-textarea border-dark-1"></textarea> 
			<br>
			<input type="submit" class="static-content-btn btn" />
		</form>
	</div>
</div>