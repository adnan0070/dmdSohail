<!-- Font awesome -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href='https://makitweb.com/demo/codeigniter_rating/assets/bootstrap-star-rating/css/star-rating.min.css' type='text/css' rel='stylesheet'>

<script src='https://makitweb.com/demo/codeigniter_rating/assets/bootstrap-star-rating/js/star-rating.min.js' type='text/javascript'></script>

<?php
$base = base_url()."site_assets/products/";
?>
<div class="col-md-12">
  <?php if($this->session->flashdata('success_msg')){ ?>
    <div class="alert alert-success alert-dismissible" style="margin-top: 50px">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Success! </strong><?php echo $this->session->flashdata('success_msg'); ?>
    </div>
  <?php } ?>

  <?php if($this->session->flashdata('error_msg')){ ?>
    <div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Error! </strong><?php echo str_replace("_"," ",$this->session->flashdata('error_msg')); ?>
    </div>
  <?php } ?>
</div>
<div class="row video-body pb-row mx-auto">

	<div class="col-md-8 pb-video-preview pb-video">

   <?php
    //print_r($files);die();
   if(count($files)>0){    
     foreach($files as $file){


      $base = base_url()."site_assets/products/".$file['proid'].'/';
      ?>
      <?php
      if(strpos(@$file['upload_type'],"image")!==false){
        ?>
        <img class="pb-video-frame" width="100%" src="<?php echo $base.@$file['upload_file'];  ?>" />
        <?php  
      }
      if(strpos(@$file['upload_type'],"video")!==false){
        ?>
        <!--<embed src="<?php //echo $base.$file['upload_file']; ?>" autostart="false" height="30" width="144" />-->
          <video class="pb-video-frame" width="100%" class="img-responsive" src="<?php echo $base.@$file['upload_file']; ?>" type="<?php echo @$file['upload_type'] ?>" controls  > </video>
          <?php  
        }
        if(strpos(@$file['upload_type'],"audio")!==false){
          ?>
          <audio controls="" class="pb-video-frame" width="100%">
            <source src="<?php echo $base.@$file['upload_file']; ?>" type="<?php echo @$file['upload_type'] ?>"/>

              Your browser does not support the audio element.
            </audio>
            <?php  
          }
          if(strpos(@$file['upload_type'],"application")!==false){
            ?>
            <iframe class="pb-video-frame" width="100%" src='https://docs.google.com/viewer?url=<?php echo $base.@$file['upload_file']; ?>&embedded=true' frameborder='0'></iframe>
            <?php
          }
          ?>
          <!--<a  href="<?php //echo base_url('static_views/productVideoDetail'); ?>" class="form-control video-label text-center">Title</a>-->
          <?php
          break;
        }
      }
      else{ 
        ?>
        <img class="pb-video-frame" width="100%" src="<?php echo $base.@$order_items['pro_image'];  ?>" />
        <?php
      }     
      ?>

      <!--<img class="pb-video-frame" width="100%" height="400" src="<?php echo $base.@$order_items['pro_image'];  ?>" />-->
      <!--<iframe  width="100%" height="400" src="https://www.youtube.com/embed/wjT2JVlUFY4?list=RDzuAcaBkcYGE?ecver=1" frameborder="0" allowfullscreen></iframe>-->
      <label class="form-control label-warning text-xs-center"><?php echo @$order_items['product_name'];  ?></label>
    </div>

    <div class="col-md-4 pb-video-detail">
      <h2 class="pb-video-heading"> <?php echo @$order_items['product_name'];  ?></h2>
      <h4 class="pb-video-sub-heading"> <?php echo @$order_items['product_short_description'];  ?> </h4>
      <p class="pb-video-description"> 
       <?php echo @$order_items['product_long_description'];  ?> 


     </p>
     <div class="nauk-info-connections product-rating-comment">
       <h5 class="store-heading">Rate to product</h5>
       <div class="page-header">

        <div class="pull-left post-rating" >
          <div class="post-action">	
            <?php
            $rating_value = ceil($averagerating);

            ?>
            <span id="star_1" onclick="rating(1,this)"  class="fa fa-star <?php if($rating_value>=1){echo ' start-checked';}else{echo 'start-unchecked';} ?>"></span>
            <span id="star_2" onclick="rating(2,this)"  class="fa fa-star <?php if($rating_value>=2){echo ' start-checked';}else{echo 'start-unchecked';} ?>"></span>
            <span id="star_3" onclick="rating(3,this)"  class="fa fa-star <?php if($rating_value>=3){echo ' start-checked';}else{echo 'start-unchecked';} ?>"></span>
            <span id="star_4" onclick="rating(4,this)"  class="fa fa-star <?php if($rating_value>=4){echo ' start-checked';}else{echo 'start-unchecked';} ?>"></span>
            <span id="star_5" onclick="rating(5,this)"  class="fa fa-star <?php if($rating_value>=5){echo ' start-checked';}else{echo 'start-unchecked';} ?>"></span>
            <span class="product-rating"><?= $averagerating ?></span> 


            <!-- Rating Bar -->
                            <!-- <input id="post_<?= $order_items['product_id']; ?>" value='<?= $rating ?>' class="rating-loading ratingbar" data-min="0" data-max="5" data-step="1">
                            
                             <div >Average Rating: <span id='averagerating_<?= $order_items['product_id']; ?>'><?= $averagerating ?></span></div>-->
                           </div>
                            <!--<input type="radio" id="star5" onclick="rating(this.value)" name="rating" value="5" /><label class = "full" for="star5" ></label>
                   
							<input type="radio" id="star4" onclick="rating(this.value)"  name="rating" value="4" /><label class = "full" for="star4" ></label>

							<input type="radio" id="star3" onclick="rating(this.value)"  name="rating" value="3" /><label class = "full" for="star3"></label>

							<input type="radio" id="star2" onclick="rating(this.value)"  name="rating" value="2" /><label class = "full" for="star2" ></label>

							<input type="radio" id="star1" onclick="rating(this.value)"  name="rating" value="1" /><label class = "full" for="star1" ></label>
              <input type="hidden" name="whatever1" class="rating-value" value="0">-->
            </div>

            <div class="clearfix"></div>
          </div>
        </div>

      </div>
    </div>
    <div class="row video-body pb-row  related-videos mx-auto">

     <?php
    //print_r($files);die();
     if(count($files)>0){    
       foreach($files as $file){


        $base = base_url()."site_assets/products/".$file['proid'].'/';
        ?>
        <div class="col-md-3 pb-video mx-auto">
          <?php
          if(strpos(@$file['upload_type'],"image")!==false){
            ?>
            <img width="100%" height="230" src="<?php echo $base.@$file['upload_file'];  ?>" />
            <?php  
          }
          if(strpos(@$file['upload_type'],"video")!==false){
            ?>
            <!--<embed src="<?php //echo $base.$file['upload_file']; ?>" autostart="false" height="30" width="144" />-->
              <video width="100%" height="230" class="img-responsive" src="<?php echo $base.@$file['upload_file']; ?>" type="<?php echo @$file['upload_type'] ?>" controls  > </video>
              <?php  
            }
            if(strpos(@$file['upload_type'],"audio")!==false){
              ?>
              <audio controls="" style="width: 100%;">
                <source src="<?php echo $base.@$file['upload_file']; ?>" type="<?php echo @$file['upload_type'] ?>"/>

                  Your browser does not support the audio element.
                </audio>
                <?php  
              }
              if(strpos(@$file['upload_type'],"application")!==false){
                ?>
                <iframe style="width: 100%;" src='https://docs.google.com/viewer?url=<?php echo $base.@$file['upload_file']; ?>&embedded=true' frameborder='0'></iframe>
                <?php
              }
              ?>
              <!--<a  href="<?php echo base_url('static_views/productVideoDetail'); ?>" class="form-control video-label text-center">Title</a>-->
            </div>
            <?php
          }
        }     
        ?>
        <br>
        <br>
      </div>
      <div class="row store-body mx-auto single-product-comments">
       <div class="col-md-12">	

        <div class="nauk-info-connections text-left">
         <h5 class="comments-heading">Post your Comment</h5>
       </div>
       <form method="post" action="<?php echo base_url('customer/comments') ?>">
        <input type="hidden" name="product_id" value="<?php echo @$order_items['product_id']; ?>" />
        <input type="hidden" name="customer_id" value="<?php echo @$order_items['customer_id']; ?>" />
        <input type="hidden" name="item_id" value="<?php echo @$order_items['itemid']; ?>" />

        <div class="nauk-info-connections post-area">

         <div class="form-group">
          <textarea name = "comment_text"  max-length="1000" class = "form-control" id="comment-area" rows="5" placeholder="Post new comment"></textarea>
          <span id="comment-error"></span>
        </div>

      </div>
      <?php
      if($already=='0'){
        ?>
        <div class="nauk-info-connections text-left">
         <input type="submit" class="btn" value="submit" id="post-btn"/>
       </div>
       <?php
     }
     ?>



   </form>

   <div class="nauk-info-connections" >

     <div class="row comments">
       <?php
       if(count($comments)>0){
         foreach($comments as $comment){
           ?>
           <div class="col-md-12 comment-info">
             <div class="nauk-info-connections text-left">
              <h2 class="comment-user-name"><?php echo $comment['f_name']." ".$comment['l_name']; ?></h2>
              <p class="comment-text"> <?php echo $comment['content']; ?></p>
              <p class="comment-time"><?php echo timeAgo($comment['created_date']); ?></p>
            </div>	
          </div>
          <?php 
        }
      }

      ?>




    </div>

  </div>


</div>

</div>

</div>
<?php

function timeAgo($time_ago)
{
  $time_ago = strtotime($time_ago);
  $cur_time   = time();
  $time_elapsed   = $cur_time - $time_ago;
  $seconds    = $time_elapsed ;
  $minutes    = round($time_elapsed / 60 );
  $hours      = round($time_elapsed / 3600);
  $days       = round($time_elapsed / 86400 );
  $weeks      = round($time_elapsed / 604800);
  $months     = round($time_elapsed / 2600640 );
  $years      = round($time_elapsed / 31207680 );
    // Seconds
  if($seconds <= 60){
    return "just now";
  }
    //Minutes
  else if($minutes <=60){
    if($minutes==1){
      return "one minute ago";
    }
    else{
      return "$minutes minutes ago";
    }
  }
    //Hours
  else if($hours <=24){
    if($hours==1){
      return "an hour ago";
    }else{
      return "$hours hrs ago";
    }
  }
    //Days
  else if($days <= 7){
    if($days==1){
      return "yesterday";
    }else{
      return "$days days ago";
    }
  }
    //Weeks
  else if($weeks <= 4.3){
    if($weeks==1){
      return "a week ago";
    }else{
      return "$weeks weeks ago";
    }
  }
    //Months
  else if($months <=12){
    if($months==1){
      return "a month ago";
    }else{
      return "$months months ago";
    }
  }
    //Years
  else{
    if($years==1){
      return "one year ago";
    }else{
      return "$years years ago";
    }
  }
}
?>
<!-- Script -->
<script type='text/javascript'>
  function rating (valp,obj1){
    var postid = '<?= $order_items['product_id']; ?>';
    var cstid = '<?= $order_items['customer_id']; ?>';

    var stars = Array.apply(null, Array(valp)).map(function (_, i) {return i +1;});
    var starSelecter = "#star_" + stars.join(",#star_");

    $.ajax({
      url: "<?= base_url('customer/checkRating') ?>",
      type: "post",
      data: {product_id: postid, customer_id: cstid},
      success: function(res){
        if(!res){
          $.ajax({
            url: '<?= base_url() ?>/customer/updateRating',
            type: 'post',
            data: {postid: postid, rating: valp},
            success: function(response){
                 //$('#averagerating_'+postid).text(response);
                 $('.product-rating').text(response);
                 $("#star_1,#star_2,#star_3,#star_4,#star_5").addClass("start-unchecked");
                 $(starSelecter).removeClass("start-unchecked");
                 $(starSelecter).addClass("start-checked");
                 
               }
             });
        }
      }
    }); 
  }



  $(document).ready(function(){

      // Initialize
      $('.ratingbar').rating({
        showCaption:false,
        showClear: false,
        size: 'sm'
      });

      // Rating change
      $('.ratingbar').on('rating:change', function(event, value, caption) {
        var id = this.id;
        var splitid = id.split('_');
        var postid = splitid[1];

        $.ajax({
          url: '<?= base_url() ?>/customer/updateRating',
          type: 'post',
          data: {postid: postid, rating: value},
          success: function(response){
           $('#averagerating_'+postid).text(response);
           console.log(response);
         }
       });
      });
    });

  </script>