<div class="row store-body mx-auto">
	<?php 
	if(isset($payment_method) && !empty($payment_method)){
		$this->load->view('front_pages/store/store_left'); 
	}
	?>
	
	<div class="col-md-10 store-inner-body"> 

		<?php if($this->session->flashdata('error_msg')){ ?>
			<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error! </strong><br><br><?php echo $this->session->flashdata('error_msg'); ?>
			</div>
		<?php } ?>

		<?php if($this->session->flashdata('success_msg')){ ?>
			<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Success! </strong><?php echo $this->session->flashdata('success_msg'); ?>
			</div>
		<?php } ?>


		<div class="row store-product-list" id="putProducts">
			<?php $this->load->view("front_pages/store/home_products"); ?>
		</div>

		<div class="row store-product-list">
			<div class="col-md-12 load-more text-center" id="divload">
				<a id="loadmore" onclick = "load_m()" class="load-more-btn btn">load more</a>
				<input type="hidden" id="offset" name="offset" value="9" />
				<input type="hidden" id="limit" name="limit" value="9" />
			</div>
		</div>
	</div>
</div>




<!-- Modal -->
<div class="modal fade" id="myModal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body modal-add-to-cart oz-model">
				<!-- adding body through ajax from popup_product.php -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		$("#loadmore").hide();
		var total_store_product_count = $("#total_store_product_count").val();
		if(total_store_product_count > 9){
			$("#loadmore").show();
		}
	});

	function myFunction(id,obj) {
		jQuery.ajax({
			url: "<?php echo base_url(); ?>Cart/showProduct",
			type: "POST",
			data: {  product_id: id  },
			success: function (data) {
			// var response = jQuery.parseJSON(data);
			$('#loading').css('display','none');
	            //console.log(data);
	            $('.modal-body').html(data);  
	          // $('#myModal').show();
	          $('#myModal').modal('show');
	          var store = 'Store Name';
	          productAddToCart(obj,store)
	      }
	  }); 
	}

	function load_m(){
		var data = {
			offset: $("#offset").val(),
			limit: $("#limit").val(),
		};
	//console.log(data);

	$(".getcount").remove();

	$.ajax({
		url: "<?php echo base_url('home/loadProduct'); ?>",
		type: "POST",
		data: data,
		success: function (data) {
			if(data){
				$("#putProducts").append(data);
				var getcount = $(".getcount").attr("cust");
				var total_store_product_count = $("#total_store_product_count").val();
				
				var pl = $("#offset").val();
				pl = Number(pl) + 9;
				$("#offset").val(pl);

				if(getcount < 9){
					$("#loadmore").hide();
				}

				if(pl >= total_store_product_count){
					$("#loadmore").hide();
				}

			}else{
				$("#divload").html("No more products.");
			}
		}
	}); 
}

//Search product by range price filter
$("#apply").on("click", function(e){
	e.preventDefault();
	var min_price = $("#amount_min").val();
	var max_price = $("#amount_max").val();
	//console.log("min: " + min_price + " max: " + max_price);
	var data = {min: min_price, max: max_price};

	$.ajax({
		url: "<?php echo base_url('home/range_product'); ?>",
		method: "post",
		data: data,
		success: function(msg){
			//console.log(msg);
			$("#putProducts").html(msg);
			$("#loadmore").hide();
		}
	});
});

</script>