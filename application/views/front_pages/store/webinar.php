

<div class="row store-body mx-auto store-single-product">

	


	<div class="container home-container">
		<div class="row main-body home-features features-row-1 mx-auto ">

			<div class="col-md-12 text-center">
				<br>
				<br>
				<h3 class="heading-xl-green"><?php echo $product[0]->product_name;?></h3>

				<p class="paragraph-text-lg-blue text-center">
					<?php echo $product[0]->product_short_description;?> 
				</p>
				<br>
				<br>
				<br>
			</div>

			<!-- <?php echo base_url('site_assets/images/5c18188e0a7a3.mp4');?> -->
			<div class="col-md-12 text-center">
				<video class="img-responsive" src="<?php echo base_url('site_assets/products/'. @$product[0]->webinar_video); ?>"  controlsList="nodownload" controls width="100%" style="max-width:850px; width:100%;" id="active-video"></video> 
			</div>

			<br>
			<div class="col-md-12 text-center">
				<a style="display:none;" id= "buy-btn" href="<?php  echo base_url();?>home/showProduct/<?php echo $product[0]->product_code; ?>" class="btn-lg-default btn">Click Here Now</a> 
			</div>
			<input type="hidden" value="<?php echo $product[0]->webinar_play_duration;?>" id="buy-time">


		</div>
		<br>
		<br>
		<br><br>
		<br>
		<br>

	</div>





	<script src="https://cdnjs.cloudflare.com/ajax/libs/vissense/0.10.0/vissense.min.js"></script>
	<script>
		var myVideo = document.getElementById('active-video');

		var videoElementArea = VisSense(myVideo);

		var monitorBuilder = VisSense.VisMon.Builder(videoElementArea);

		monitorBuilder.on('fullyvisible', function() {
			myVideo.play();  
			var buyTime = Number($("#buy-time").val());

			if(buyTime > 0){

				$("#active-video").on(
					"timeupdate", 
					function(event){
						onTrackedVideoFrame(this.currentTime, this.duration);
					});

				function onTrackedVideoFrame(currentTime, duration){
					if(currentTime >= buyTime){
						$("#buy-btn").show();
					}else{
						$("#buy-btn").hide();
					}
					console.log(currentTime);
					console.log(duration);

				}
			}



		});
		monitorBuilder.on('hidden', function() {
			myVideo.pause();  
		});
		var videoVisibilityMonitor = monitorBuilder.build();
		videoVisibilityMonitor.start();


	</script>



</div> 