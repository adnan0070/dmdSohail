


<style>
.pb-video-container {
	padding-top: 20px;
	background: #bdc3c7;
	font-family: Lato;
}
.search{
    display: none;
}

.pb-video {
	border: 1px solid #e6e6e6;
	padding: 5px;
}

.pb-video:hover {
	background: #eee;
	border-radius:5px;
}
.pb-video-frame {
	transition: width 2s, height 2s;
}

.pb-video-frame:hover {
	height: 300px;
	z-index:1000;
}

.pb-row {
	margin-bottom: 10px;
}
</style>

<div class="row vido-bodypb-row  mx-auto">

    <?php
    //print_r($files);die();
     foreach($files as $file){
      //print_r($file);

        $base = base_url()."site_assets/products/".$file['product_id'].'/';
        ?>
          <div class="col-md-3 pb-video">
            <?php
             if(strpos($file['upload_type'],"image")!==false){
              ?>
                <img width="100%" height="230" src="<?php echo $base.$file['upload_file'];  ?>" />
              <?php  
             }
             if(strpos($file['upload_type'],"video")!==false){
              ?>
                <!--<embed src="<?php //echo $base.$file['upload_file']; ?>" autostart="false" height="30" width="144" />-->
                <video width="100%" height="230" class="img-responsive" src="<?php echo $base.$file['upload_file']; ?>" type="<?php echo $file['upload_type'] ?>" controls  > </video>
              <?php  
             }
             if(strpos($file['upload_type'],"audio")!==false){
              ?>
              <audio controls="" style="width: 100%;">
                  <source src="<?php echo $base.$file['upload_file']; ?>" type="<?php echo $file['upload_type'] ?>"/>
                  
                Your browser does not support the audio element.
              </audio>
              <?php  
             }
             if(strpos($file['upload_type'],"application")!==false){
                ?>
                  <iframe style="width: 100%;" src='https://docs.google.com/viewer?url=<?php echo $base.$file['upload_file']; ?>&embedded=true' frameborder='0'></iframe>
                <?php
             }
            ?>
        <!-- <a  href="<?php //echo base_url('customer/productDetail/').$file['product_id'].'/'.$file['id']; ?>" class="form-control video-label text-center">zain</a> -->
    		<?php if($is_download){ ?>
          <a href="<?= $base.$file['upload_file']; ?>" target="_blank" class="form-control video-label text-center"><?php echo $file['button_label']; ?></a>
        <?php }else{ ?>
          <span class="form-control video-label text-center"><?php echo $file['button_label']; ?></span>
        <?php } ?>
    	</div>
        <?php
     }
    ?>

	<!-- <div class="col-md-3 pb-video">
    <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/wjT2JVlUFY4?list=RDzuAcaBkcYGE?ecver=1" frameborder="0" allowfullscreen></iframe>
    <a href="<?php //echo base_url('static_views/productVideoDetail'); ?>" class="form-control video-label text-center">Title</a>
  </div> -->

	<br>
	<br>
</div>
</div>
