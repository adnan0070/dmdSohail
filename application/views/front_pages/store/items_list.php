<div class="col-md-12 table-block table-responsive">
  <table class="table  table-hover table-striped table-bordered cart-table">
    <thead>
      <tr>
        <th class="store-order-product-name cart-th">Product Price</th>
        <th class="store-order-product-name cart-th">Quantity</th>
        <th class="store-order-product-name cart-th">Price</th>
        <th class="store-order-product-name cart-th">Files</th>
      </tr>
    </thead>
    <tbody>
      <?php if(isset($order_items)){
        //print_r($order_items)
        foreach ($order_items as $key => $value) {

          ?>
          <tr>
            <td class="store-order-product-name"><?php echo $value['product_price']; ?></td>
            <td class="store-order-product-name"><?php echo $value['product_quantity']; ?></td>
            <td class="store-order-product-name"><?php echo $value['total_price']; ?></td>
            <td class="store-order-product-name">
              <a href="<?php echo base_url('product/purchaseFile/').$value['product_id']; ?>" class="btn btn-sm-blue">View/download Files</a>
            </td>
          </tr>
        <?php }}?>
      </tbody>
    </table>
  </div>