


<style>
.pb-video-container {
	padding-top: 20px;
	background: #bdc3c7;
	font-family: Lato;
}
.search{
  display: none;
}

.pb-video {
	/*border: 1px solid #e6e6e6;*/
	padding: 10px;
}

.pb-video:hover {
	background: #eee;
	border-radius:5px;
}
.pb-video-frame {
	transition: width 2s, height 2s;
}

.pb-video-frame:hover {
	height: 300px;
	z-index:1000;
}

.pb-row {
	margin-bottom: 10px;
}
</style>

<div class="row vido-body pb-row  mx-auto">

  <?php
  foreach($order_items as $items){

    if( (isset($items['product_code'])) && ( ! empty($items['product_code'])) ){

      $image = "";
      if( (isset($items['pro_image'])) && ( ! empty($items['pro_image'])) ){
        $image = "./site_assets/products/" . $items['pro_image'];
      }

      ?>
      <div class="col-md-3 pb-video">



        <?php if(file_exists($image)){ ?>
         <div class="fix-media text-center">
          <img src="<?= base_url('site_assets/products/') . $items['pro_image']; ?>" />
        </div>
      <?php } ?>

      <?php if( (isset($items['itemid'])) && ( ! empty($items['itemid'])) ){ ?>
        <a href="<?php echo base_url('customer/productDetail/'.$items['product_code'].'/'.$items['itemid']); ?>" class="form-control video-label text-center"><?= $items['product_name']; ?></a>
      <?php } ?>

    </div>

    <?php
  }
}
?>

</div>

