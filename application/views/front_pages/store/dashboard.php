<style>
.search{
  display: none;
}
</style>
<div class="row text-center" style="padding: 0;">
	<div class="col-md-12">

		<?php if($this->session->flashdata('error_msg')){ ?>
      <div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error! </strong><br><br><?php echo $this->session->flashdata('error_msg'); ?>
      </div>
    <?php } ?>

    <?php if($this->session->flashdata('success_msg')){ ?>
      <div class="alert alert-success alert-dismissible" style="margin-top: 50px">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success! </strong><?php echo $this->session->flashdata('success_msg'); ?>
      </div>
    <?php } ?>

	</div>    
	<div class="col-md-12">
		<h3 class="cart-sub-heading">Orders Detail</h3>
		<table id="orders" class="table table-bordered table-striped" style="width:100%">
			<thead>
				<tr>
					<th class="store-order-product-name cart-th">Order No</th>
					<th class="store-order-product-name cart-th">Total</th>
					<th class="store-order-product-name cart-th">Status</th>
					<th class="store-order-product-name cart-th" style="display: none;">Delivery Status</th>
          <th class="store-order-product-name cart-th">Transection ID</th>
					<th class="store-order-product-name cart-th">Date</th>
          <th class="store-order-product-name cart-th">Invoice</th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($customer_orders)){
         foreach ($customer_orders as $key => $value) {

          ?>
          <tr>
            <th class="store-order-product-name"><a  class="store-order-product-name cart-th" onclick="ozFunction(<?php echo $value['id']; ?>);" href="#"> <?php echo $value['order_number']; ?></a> </th>
            <th class="store-order-product-name"><?php echo $value['grand_total']; ?></th>
            <th class="store-order-product-name"><?php echo $value['status']; ?></th>
            <th  class="store-order-product-name" style="display: none;"><?php echo $value['deliver_status']; ?></th>
            <th class="store-order-product-name"><?php echo $value['transID']; ?></th>
            <th class="store-order-product-name"><?php echo $value['created_date']; ?></th>
            <th class="store-order-product-name"><a href="<?php echo base_url('user/orderinvoice/'.$value['order_number']) ?>"><button class="btn btn-sm-blue">Download Invoice</button></a></th>
          </tr>
        <?php }} ?>
      </tbody>
    </table>
  </div>
</div>

<!-- Modal -->
<div id="OzModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title modal-cart-sub-heading">Orders Detail</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body oz-model-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
  $('#searchCart').css('display','none');




  var url = '<?php echo base_url();?>';
  function ozFunction(id){


   $.ajax({
    url: url + 'customer/order_items_details',
    type: 'POST',
    data: {order_id: id},
  }).then(function(data) {
    var objData = jQuery.parseJSON(data);
    console.log(objData.msg);
    if (objData.status == 'success') {
            // $('.imgload').css('display','none');

            $(".oz-model-body").html(objData.table);
            $('#OzModal').modal('show');
            // window.location.href = url+objData.redirect;

          } else {
            // $('.imgload').css('display','none');

            $(".model-body").html(objData.msg);
          }
        });

}



$(".button_info_search").on('click', function(e) { 
  var formData = $('#search_list_form').serialize();
  $('.imgload').css('display','inherit');
  $.ajax({
    url: url + 'dashboard/search',
    type: 'POST',
    data: formData,
  }).then(function(data) {
    var objData = jQuery.parseJSON(data);
    console.log(objData.msg);
    if (objData.status == 'success') {
      $('.imgload').css('display','none');
      $('.msg-alerts').css('display', 'block');
      $(".msg-alerts").html('<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close" style="position:relative;font-size: 21px;margin: 0;top:-2px;right:-21px;color: inherit;">&times;</a><strong>Redirecting! </strong> ' + objData.msg + '</div>');
      window.location.href = url+objData.redirect;

    } else {
      $('.imgload').css('display','none');
      $('.msg-alerts').css('display', 'block');
      $(".msg-alerts").html('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close" style="position:relative;font-size: 21px;margin: 0;top:-2px;right:-21px;color: inherit;">&times;</a><strong>Error! </strong> ' + objData.msg + '</div>');
    }
  });

});

$(document).ready(function(){
    $('#orders').DataTable({
        "pagingType": "full_numbers",
        "ordering": false,
        "searching": false,
        "bLengthChange": false,
        //"bInfo": false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">'
    });
});
</script>