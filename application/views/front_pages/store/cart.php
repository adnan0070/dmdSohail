<form action="" method="post" id="cart_form">
	<div class="row store-body mx-auto">
		<div class="col-md-12 store-inner-heading">	
			<h4>Your Order &nbsp;<span class="text-danger"> </span> <span class="text-success"></span></h4>
		</div>

		<div class="col-md-12 table-block table-responsive">
			<table class="table store-table cart-table">

				<thead>
					<tr>
						<td class="store-order-product-name cart-th">Image</td>
						<td class="store-order-product-name cart-th">Name</td>
						<td class="store-order-product-name cart-th">Discount</td>
						<td class="store-order-product-name cart-th">Actual Price</td>
						<td class="store-order-product-name cart-th">Discount Price</td>
						<td class="store-order-product-name cart-th">Quantity</td>
						<td class="store-order-product-name cart-th">Subtotal</td>
						<td class="store-order-product-name cart-th">Action</td>

					</tr>
				</thead>

				<tbody> 
					<?php 
					foreach ($this->cart->contents() as $items) {  ?>

						<input type="hidden" name="cart[<?php echo $items['id']; ?>][id]" value="<?php echo $items['id']; ?>" />
						<input type="hidden" name="cart[<?php echo $items['id']; ?>][rowid]" value="<?php echo $items['rowid']; ?>" />
						<input type="hidden" name="cart[<?php echo $items['id']; ?>][name]" value="<?php echo $items['name']; ?>" />
						<input type="hidden" name="cart[<?php echo $items['id']; ?>][price]" value="<?php echo $items['price']; ?>" />


						<tr>
							<td  class="store-order-image" data-th="Product">
								<?php 
								$ci =& get_instance();
								$ci->load->model('product_model');
								if(isset($items['id']) && !empty($items['id'])){
									$product_arr = $ci->product_model->get_SingleProduct_Bycode($items['id']);
								}
								?>
								<img src="<?php if(isset($product_arr[0]->pro_image)){ echo base_url().'site_assets/products/'.$product_arr[0]->pro_image;}else{ echo 'http://placehold.it/100x100'; }  ?>" alt="Item Image" />

							</td>
							<td  class="store-order-product-name td-info"><?php echo $items['name']; ?></td>
							<td  class="store-order-product-price td-info" data-th="Discount"><?php echo ($items['discount']); ?>%</td>
							<td  class="store-order-product-price td-info" data-th="Actual Price">$<?php echo $this->cart->format_number($items['Origprice']); ?></td>
							<td  class="store-order-product-price td-info" data-th="Discount Price">$<?php echo $this->cart->format_number($items['price']); ?></td>
							<td  class="store-order-product-quantity td-info" data-th="Quantity">
								<input type="text" 
								id="quantity" 
								name="cart[<?php echo $items['id']; ?>][qty]" 
								class="store-input" 
								value="<?php echo $this->cart->format_number($items['qty']); ?>" 
								oninput="this.value=this.value.replace(/[^0-9]/g,'');"
								/>
							</td>
							<td data-th="Subtotal" class="store-order-product-price align-middle">$<?php echo $this->cart->format_number($items['subtotal']); ?></td>
							<td data-th="remove" class="text-center align-middle"><a style="color: #fff;" class="btn btn-sm btn-danger btn-rounded" href="<?php echo base_url();?>Cart/remove/<?php echo $items['rowid']; ?>"><i class="fa fa-remove"></i> Remove</a></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>

		<div class="col-md-7 text-left">	
			<!-- <input type="text" placeholder="Enter coupon" class="form-control store-input"> -->
		</div>
		<div class="col-md-5 text-right store-cart">
			<div class="list">
				<table class="table store-cart-table">
					<tbody>
						<tr class="cart-data">
							<td>Subtotal:</td>
							<td>$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
						</tr>
					<!-- <tr class="cart-data">
						<td>Tax (19%):</td>
						<td>$23.5</td>
					</tr>
					<tr class="cart-data">
						<td>Coupon (10%):</td>
						<td>$-240</td>
					</tr> -->
					<tr class="cart-footer">
						<td>Total:</td>
						<td>$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
					</tr>

				</tbody>
			</table>
		</div>
		
		<?php if(count($this->cart->contents()) > 0){ ?>
			<a class="btn-sm-blue btn update_cart" href="#">update</a>
			<a class="btn-sm-blue btn checkout_cart" id="checkoutBtn" href="<?php echo base_url('cart/checkout'); ?>">checkout</a>
		<?php } ?>

	</div>


</div>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$('#quantity input').blur(function(){
			if(this.value) {
				$(this).css('border-color','#a94442');
			}
		});

		$('.update_cart').click(function(){
			var quantity = $('#quantity').val();
			$.ajax({
				url:"<?php echo base_url(); ?>Cart/update_cart",
				method:"POST",
				data: $('#cart_form').serialize(),
				success:function(data){
					window.setTimeout(function() {
						window.location.href = '<?php echo base_url(); ?>Home/showCart';
					}, 1000);
				}
			}); 
		});

	});

</script>