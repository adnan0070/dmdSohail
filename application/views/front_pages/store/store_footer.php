	
</div><!-- end Body Container -->
<!-- Footer -->
<div class="container-fluid store-footer">
	<div class="row store-footer-row">
		<div class="col-md-6  footer-left max-auto store-footer-left">
			<div class="nauk-info-connections text-text-center">
				<?php if($this->session->userdata('store_name')){
					$store_name = $this->session->userdata('store_name');
				}else{
					$store_name = "Store";
				} ?>
				<p class="store-footer-links"><?php echo $store_name; ?> &copy; 2019, All Right Reserved.</p>
				<p class="store-footer-links"><?php echo $store_name; ?> powered by Digital Media Deliveries</p>
			</div>
		</div>

		<div class="col-md-6  footer-right max-auto store-footer-right">
			<div class="nauk-info-connections text-left">
				
				<a class="store-footer-links" href="<?php echo base_url();?>">Home</a><br>
				<a class="store-footer-links" href="<?php echo base_url();?>">About us</a><br>
				<a class="store-footer-links" href="<?php echo base_url('home/contact'); ?>">Contact</a><br>
				<a class="store-footer-links"  rel="external"  href="<?php echo base_url('pages/front/storeTermsServices');?>">Terms and Services</a><br>
				<a class="store-footer-links" rel="external"  href="<?php echo base_url('pages/front/storePrivacyPolicy');?>">Privacy Policy</a><br>
				<a class="store-footer-links" rel="external"  href="<?php echo base_url('pages/front/storeRefundPolicy');?>">Return Policy</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function ozfunction(e){
		var base = '<?php echo base_url();?>';
		var method = "pages/front";
		var d = {"id":e};
		if(e == "show_teams"){
			method = "pages/show_teams";
			d = {"showTeam":e};
		}
	//console.log(d);
	$.ajax({
		type: "POST",
		url: base+method,
		data:d,
		success: (function(response){
			//console.log(response);
			$('.static-content').html(response);
			//$('.static-content').fadeIn(5000);
		})   
	});
}
</script>


<script type="text/javascript">
	function formValidate(FormId){
		var validation = false;
		var f = $("#" + FormId)[0];
		var e;
		var errorMsg;

		if (f.checkValidity()) {
			$(".validThis").each(function(){
				e = $(this);
				if(($.trim(e.val()) == "") || (e.val() == null) || (typeof e.val() === 'undefined')){
		        	//console.log("run");
		        	if(FormId == 'payment'){
		        		errorMsg = e.attr("placeholder").replace(/_|-/g, " ") + " is required.";
		        	}else{
		        		errorMsg = e.attr("name").replace(/_|-/g, " ").toUpperCase() + " is required.";
		        	}
		        	e.focus();
		        	$('#showError').html('<div style="word-wrap:break-word;" class="alert alert-danger alert-dismissable text-justify"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + errorMsg + '</div>');
		        	validation = false;
		        	return false;
		        }else{
		        	validation = true;
		        }
		    });
			if (validation == true) {
				f.submit();
			}

		}else{
			$('showError').html('<div style="word-wrap:break-word;" class="alert alert-danger alert-dismissable text-justify"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error: </strong>All enable fields are required! Please check again all of your input.</div>');
		}
	}
</script>

<script type="text/javascript">
	function searchCart(){
		var v = $('#searchCart').val();
		var d;
		if(v.length > 2) {
			d = {'search': v}
		}else if(v.length <= 2){
			d = {'search': ""}
		}
				//console.log(v);
				$.ajax({
					data: d,
					type: "POST",
					url: "<?php echo base_url('home/loadProduct'); ?>",
					cache: false,
					success: function(msg){
						console.log(msg);
						$("#putProducts").html(msg);
						var getcount = $(".getcount").attr("cust");
						//console.log(getcount);
						if(getcount < 3){
							$("#loadmore").hide();
						}else{
							$("#loadmore").show();
						}

						if( ! getcount){
							$("#loadmore").hide();
						}
					}
				});
			}

			$('#searchCart').on({
				keyup: searchCart,
				click: searchCart,
				select: searchCart
			});
		</script>

	</body>
	</html>