
<?php $store = $this->db->query('select * from settings');
$store = $store->row_array(); 

$get_lowest = $this->db->query("SELECT *
	FROM packages
	WHERE pack_price =  ( SELECT MIN(pack_price) FROM packages ) ");
$default_package = $get_lowest->row_array();

?>


<!-- home section 1 -->
<div class="container-fluid bg-home-section ">

	<!-- <div class="row row-stretched">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space text-right">
		</div>
		
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space text-center">
			<div style="background-color:rgba(255,255,255,0.9); border-radius:5px; padding:5px;">
				<p class="text-level-2" style="margin:0px;"> 
					<span><i class="fa fa-phone" style="color:#468fb1;"></i> +44 (0) 203 488 2610 &nbsp;</span>
					<a class="text-level-2" href="mailto:info@digitalmediadeliveries.com" target="_top"><i class="fa fa-envelope" style="color:#468fb1;"></i> info@digitalmediadeliveries.com</a>
				</p>
			</div>
			<br>
		</div>
	</div> -->

	<div class="row home-header-2">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space contact-info-block">
			<div class="text-right">
				<ul class="list-inline">
					<li class="contact-phone"><span class="icon-circle"><i class="fa fa-phone"></i></span><p href="#" class="btn-empty btn ">+44 (0) 203 488 2610</p></li>
					<li><span class="icon-circle"><i class="fa fa-envelope"></i></span><a href="mailto:info@digitalmediadeliveries.com" target="_top"" class="btn-empty btn ">info@digitalmediadeliveries.com</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row row-stretched">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
			<div class="section-content-block home-content-section">
				<div class="head" data-aos="fade-down" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
					<h1 class="heading-level-1">3…2…1…Lift-Off</h1>
				</div>
				<div class="content">
					<p class="text-level-1 text-black">Your Digital Media products are ready for launch. Let Digital Media Deliveries help you get them off the ground and into the world with brilliant management and sales tools.</p>
				</div>
				<div class="head" data-aos="fade-down" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
					<h1 class="heading-level-2" style="background: linear-gradient(to right, #74a857 10%, #468fb1 70%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;">New Feature: Host Your Webinars And Sales Videos</h1>
				</div>
				
				<!-- <div class="row block-email"  data-aos="fade-left">
					<div class="pull-left col-7 col-xs-7 col-sm-6 col-md-8 col-lg-8 col-pad email-input">
						<input type="email" name="" placeholder="Email">
					</div>
					<div class="pull-left col-5 col-xs-5 col-sm-6 col-md-4 col-lg-4 notify-btn text-right">
						<a href="#" class="btn-sm-default btn pricing-btn">Notify <i class="fa fa-paper-plane"></i></a>
					</div>
					<div class="clearfix"></div>
				</div> -->
			</div>
		</div>
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space col-padding-video">
			<div class="block-video text-center video-home-session">
				<!-- <?php echo base_url('site_assets/images/5c18188e0a7a3.mp4');?> -->
				<!-- <?php echo base_url('site_assets/video/') . $store['homefile']; ?> -->
				<!-- <?php echo base_url('site_assets/images/video-1.mp4'); ?> -->
				<video class="img-responsive video-hover" src="<?php echo base_url('site_assets/video/') . $store['homefile']; ?>" id="vid"  controlsList="nodownload" controls width="100%"> </video>
				<img  src="<?php echo base_url(); ?>site_assets/images/video.png" class="img-show" onclick="Play()" id="vid-play" >
			</div>

			<input style="opacity:0;" type="text" value="<?php echo base_url('user/visit');?>" id="myInput">

			<div class="block-btn text-center">
				<div class="tooltipp">
					<button href="#" class="btn-sm-default btn btn-lg" onmouseout="outFunc()" onclick="myFunction()" ><span class="tooltiptext" id="myTooltip"></span> SHARE NOW</button>
				</div>
			</div>

		</div>
		<div class="clearfix"></div>
	</div>
</div>



<script>
	function myFunction() {
		var copyText = document.getElementById("myInput");
		copyText.select();
		document.execCommand("copy");

		var tooltip = document.getElementById("myTooltip");
		tooltip.innerHTML = "COPIED: ";
	}

	function outFunc() {
		var tooltip = document.getElementById("myTooltip");
		tooltip.innerHTML = "COPY TO ";
	}
</script>
<!-- end home section 1 -->

<!-- home section 2 -->
<div class="container-fluid bg-home-section-2 main-container">
	<div class="row  row-stretched">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
			<div class="head text-left">
				<h1 class="heading-level-2">The Ultimate Digital Media Product Delivery Portal</h1>
			</div>
			<div class="content text-left">
				<p class="text-level-1">DMD is an all-in-one platform that empowers you to share your digital products with a global audience. From e-books to imagery, audio, videos, and everything in-between, publish it all through DMD’s secure, automated delivery portal. Set up unlimited websites and storefronts unique to your niches, create unique URLs for each of your new websites, and more. Everything is fully customizable!</p>
			</div>
		</div>
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
			<div class="img-block text-right">
				<img  src="<?php echo base_url(); ?>site_assets/images/img-home-right.png" style="max-width: 450px;">
			</div>
		</div>
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">

			<div class="block-btn text-center">
				<a href="/user/register" class="btn-sm-default btn btn-lg">Click Here To Try For Free</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- home section 2 -->

<!-- home section 2 -->
<div class="container-fluid bg-home-section-2 main-container">
	<div class="row  row-stretched">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
			<div class="img-block text-left">
				<img  src="<?php echo base_url(); ?>site_assets/images/img-home-left.png"  style="max-width: 365px;">
			</div>
		</div>
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
			<div class="head text-left">
				<h1 class="heading-level-2">Any Digital File. Anytime. Anywhere.</h1>
			</div>
			<div class="content text-left">
				<p class="text-level-1">You don’t need to be an online seller to use DMD. Use our secure portal to safely send media to your clients or partners in just a few clicks. Attach, upload, send, and breathe. Have some coffee. It only takes a few minutes, with support from intuitive software that sends the right media to the right place, at the right time.</p>
			</div>

		</div>

		<div class="clearfix"></div>
	</div>
</div>
<!-- home section 2 -->

<!-- home section 3 -->
<div class="container-fluid main-container">
	<div class="row row-stretched">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
			<div class="row row-max-width">
				<div class="pull-left col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 img-block-1 padding-left-0">
					<div class="sm-img-block" id="sm-img-1" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="400" data-aos-offset="0">
							<!-- <div class="svg-icon-block">
								
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="167.886" height="159.902" viewBox="0 0 167.886 159.902">
									<defs>
										<filter id="Path_43" x="0" y="0" width="167.886" height="159.902" filterUnits="userSpaceOnUse">
											<feOffset input="SourceAlpha"/>
											<feGaussianBlur stdDeviation="5" result="blur"/>
											<feFlood flood-opacity="0.161"/>
											<feComposite operator="in" in2="blur"/>
											<feComposite in="SourceGraphic"/>
										</filter>
										<linearGradient id="linear-gradient" x1="0.151" y1="-0.087" x2="1.217" y2="1.086" gradientUnits="objectBoundingBox">
											<stop offset="0" stop-color="#458fb4"/>
											<stop offset="1" stop-color="#78ab4f"/>
										</linearGradient>
									</defs>
									<g  id="Group_73" data-name="Group 73" transform="translate(-176.075 -1230.412)">
										<g transform="matrix(1, 0, 0, 1, 176.08, 1230.41)" filter="url(#Path_43)">
											<path  class="svg-img-icon" id="Path_43-2" data-name="Path 43" d="M8889.631-75.2c.014,0-91.156-105.425-6.614-120.191.88.421,129.032-20.809,96.739,47.26C8979.661-148.435,8927.47-40.423,8889.631-75.2Z" transform="translate(-8832.09 213.2)" fill="#fff"/>
										</g>
										<g id="file" transform="translate(230.039 1272.049)">
											<path class="inner-icon" id="Path_93" data-name="Path 93" d="M44.958,25H20a1,1,0,0,0,0,2H44.958a1,1,0,0,0,0-2Z" transform="translate(-0.021 -0.041)" fill="url(#linear-gradient)"/>
											<path class="inner-icon" id="Path_94" data-name="Path 94" d="M20,19h9.984a1,1,0,0,0,0-2H20a1,1,0,1,0,0,2Z" transform="translate(-0.021 -0.028)" fill="url(#linear-gradient)"/>
											<path class="inner-icon" id="Path_95" data-name="Path 95" d="M44.958,33H20a1,1,0,1,0,0,2H44.958a1,1,0,0,0,0-2Z" transform="translate(-0.021 -0.054)" fill="url(#linear-gradient)"/>
											<path class="inner-icon" id="Path_96" data-name="Path 96" d="M44.958,41H20a1,1,0,1,0,0,2H44.958a1,1,0,0,0,0-2Z" transform="translate(-0.021 -0.067)" fill="url(#linear-gradient)"/>
											<path class="inner-icon" id="Path_97" data-name="Path 97" d="M44.958,49H20a1,1,0,1,0,0,2H44.958a1,1,0,0,0,0-2Z" transform="translate(-0.021 -0.08)" fill="url(#linear-gradient)"/>
											<path class="inner-icon" id="Path_98" data-name="Path 98" d="M48.93,14.562V0H6V54.91h4.992V59.9h42.93V19.554ZM39.945,8.4l8.985,8.985,1.583,1.583H39.945ZM8,52.914V2H46.933V12.566L39.36,4.992H10.992V52.914Zm4.992,4.992V6.989H37.948V20.966H51.925v36.94Z" fill="url(#linear-gradient)"/>
										</g>
									</g>
								</svg>

							</div> -->
							<img class="img-white"  src="<?php echo base_url(); ?>site_assets/images/img-triangle-1.png">
							<img class="img-color" src="<?php echo base_url(); ?>site_assets/images/img-triangle-1-bg.png">
						</div>
						<div class="sm-img-block" id="sm-img-2" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
							<img class="img-white" src="<?php echo base_url(); ?>site_assets/images/img-triangle-2.png">
							<img class="img-color" src="<?php echo base_url(); ?>site_assets/images/img-triangle-2-bg.png">
						</div>
					</div>
					<div class="pull-left col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 img-block-2 padding-left-0">
						<div class="sm-img-block" id="sm-img-3" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="100" data-aos-offset="0">
							<img class="img-white" src="<?php echo base_url(); ?>site_assets/images/img-triangle-3.png">
							<img class="img-color" src="<?php echo base_url(); ?>site_assets/images/img-triangle-3-bg.png">
						</div>
						<div class="sm-img-block" id="sm-img-4" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
							<img class="img-white" src="<?php echo base_url(); ?>site_assets/images/img-triangle-4.png">
							<img class="img-color" src="<?php echo base_url(); ?>site_assets/images/img-triangle-4-bg.png">
						</div>
					</div>
					<div class="pull-left col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 img-block-3 padding-left-0">

						<div class="sm-img-block" id="sm-img-5" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="500" data-aos-offset="0">
							<img class="img-white" src="<?php echo base_url(); ?>site_assets/images/img-triangle-5.png">
							<img class="img-color" src="<?php echo base_url(); ?>site_assets/images/img-triangle-5-bg.png">
						</div>
					</div>
				</div>
			</div>
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
				<div class="section-content-block">
					<div class="head">
						<!-- <div class="divider" data-aos="fade-left"></div> -->
						<h1 class="heading-level-2">Our Clients Love Our Easy Process</h1>
					</div>
					<div class="content">
						<ul>
							<li class="text-level-1">
								Pick your package based on your number of products and storage needs.
							</li>
							<li class="text-level-1">
								Configure your store(s) and profile – don’t have the time or patience? No problem! Our in-house team can do it all for you, simply submit a request…
							</li>
							<li class="text-level-1">
								Link your payment processor, like PayPal or Stripe and Autoresponder, such as Aweber or Mailchimp.
							</li>
							<li class="text-level-1" >
								Upload your Digital Media.
							</li>
							<li class="text-level-1" >
								Copy and paste the links to your website (or create a unique URL with us to start selling – directly through our Platform).
							</li>
							<li class="text-level-1" >
								Immediately following a sale, DMD will automatically deliver orders to your customers. Relax - you’ve earned it.
							</li>
						</ul>
					</div>
					<div class="block-btn text-center">
						<a href="/user/register" class="btn-sm-default btn btn-lg">Click Now For <?php echo $default_package['pack_days']; ?> Day Free Trial</a>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- end home section 3 -->

	<!-- home section 4 -->
	<div class="container-fluid main-container bg-home-section-4">
		<div class="row row-stretched">

			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
				<div class="section-content-block">
					<div class="text-img-block text-left">
						<img  src="<?php echo base_url(); ?>site_assets/images/img-12.gif">
					</div>
				<!-- 	<div class="content">
						<p class="text-level-1">Simply upload your products > configure your store and profile > copy and paste the links to your own website OR, use our website feature with your own unique URL to sell your products and services > as soon as your customers buy, Digital Media Deliveries will automatically deliver the products to your customers...
						</p>
					</div> -->
				</div>
			</div>
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
				<div class="img-block text-right">
					<img  src="<?php echo base_url(); ?>site_assets/images/img-13.gif">
				</div>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>
	<!-- end home section 4 -->

	<!-- home section 5 -->
	<div class="container-fluid main-container">
		<div class="row row-stretched">
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
				<div class="section-content-block content-section-5">
					<div class="head">
						<!-- <div class="divider" data-aos="fade-right"></div> -->
						<h1 class="heading-level-2">The DMD Video Vault</h1>
					</div>
					<div class="content">
						<p class="text-level-1">Not all Digital Media needs to be downloaded. Sometimes, you might have an important video you need to share with clients, partners, or customers. That’s where our Video Vault comes in! Simply upload your video(s), like training material or a video sales letter, and send the link to the people that matter. It’s easy.
						</p>
					</div>
				</div>
			</div>
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
				<div class="img-block text-right">
					<img  src="<?php echo base_url(); ?>site_assets/images/img-14.png">
				</div>
			</div>
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
				<div class="block-btn text-center">
					<a href="/user/register" class="btn-sm-default btn btn-lg">Sign Up Now</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- end home section 5 -->


	<!-- home section 6 -->
	<div class="container-fluid main-container">
		<div class="row row-fixed-width">
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
				<div class="head text-center">
					<h1 class="heading-level-2">Digital Media Deliveries is 100% automated, giving you the time and freedom to focus on more pressing matters - like running a business.</h1>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="row row-stretched">
			<div class="pull-left col-12 col-xs-12 col-sm-6 col-md-3 col-lg-3 col-space">
				<div class="right-border"></div>
				<div class="info-block" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
					<div class="icon-block text-center">
						<svg class="svg-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 489.4 489.4" style="enable-background:new 0 0 489.4 489.4;" xml:space="preserve">

						<path d="M347.7,263.75h-66.5c-18.2,0-33,14.8-33,33v51c0,18.2,14.8,33,33,33h66.5c18.2,0,33-14.8,33-33v-51
						C380.7,278.55,365.9,263.75,347.7,263.75z M356.7,347.75c0,5-4.1,9-9,9h-66.5c-5,0-9-4.1-9-9v-51c0-5,4.1-9,9-9h66.5
						c5,0,9,4.1,9,9V347.75z"/>
						<path d="M489.4,171.05c0-2.1-0.5-4.1-1.6-5.9l-72.8-128c-2.1-3.7-6.1-6.1-10.4-6.1H84.7c-4.3,0-8.3,2.3-10.4,6.1l-72.7,128
						c-1,1.8-1.6,3.8-1.6,5.9c0,28.7,17.3,53.3,42,64.2v211.1c0,6.6,5.4,12,12,12h66.3c0.1,0,0.2,0,0.3,0h93c0.1,0,0.2,0,0.3,0h221.4
						c6.6,0,12-5.4,12-12v-209.6c0-0.5,0-0.9-0.1-1.3C472,224.55,489.4,199.85,489.4,171.05z M91.7,55.15h305.9l56.9,100.1H34.9
						L91.7,55.15z M348.3,179.15c-3.8,21.6-22.7,38-45.4,38c-22.7,0-41.6-16.4-45.4-38H348.3z M232,179.15c-3.8,21.6-22.7,38-45.4,38
						s-41.6-16.4-45.5-38H232z M24.8,179.15h90.9c-3.8,21.6-22.8,38-45.5,38C47.5,217.25,28.6,200.75,24.8,179.15z M201.6,434.35h-69
						v-129.5c0-9.4,7.6-17.1,17.1-17.1h34.9c9.4,0,17.1,7.6,17.1,17.1v129.5H201.6z M423.3,434.35H225.6v-129.5
						c0-22.6-18.4-41.1-41.1-41.1h-34.9c-22.6,0-41.1,18.4-41.1,41.1v129.6H66v-193.3c1.4,0.1,2.8,0.1,4.2,0.1
						c24.2,0,45.6-12.3,58.2-31c12.6,18.7,34,31,58.2,31s45.5-12.3,58.2-31c12.6,18.7,34,31,58.1,31c24.2,0,45.5-12.3,58.1-31
						c12.6,18.7,34,31,58.2,31c1.4,0,2.7-0.1,4.1-0.1L423.3,434.35L423.3,434.35z M419.2,217.25c-22.7,0-41.6-16.4-45.4-38h90.9
						C460.8,200.75,441.9,217.25,419.2,217.25z"/></svg>
					</div>
					<div class="head text-center">
						<h3 class="heading-level-3">Unlimited stores</h3>
					</div>
					<div class="vertical-diviver"></div>
					<div class="content text-center">
						<p class="text-level-2">One for this product, one for that product, and everything in-between. With DMD, there’s no cap on the number of stores you can launch online. Each will have a unique URL, so you can laser-target specific niches.</p>
					</div>
				</div>
			</div>
			<div class="pull-left col-12 col-xs-12 col-sm-6 col-md-3 col-lg-3 col-space">
				<div class="right-border"></div>
				<div class="info-block info-block-display" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="400" data-aos-offset="0">
					<div class="icon-block text-center">
						<svg class="svg-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 480.006 480.006" style="enable-background:new 0 0 480.006 480.006;" xml:space="preserve">
						<g>
							<g>
								<rect x="40.003" y="48.006" width="104" height="16"/>
							</g>
						</g>
						<g>
							<g>
								<rect x="40.003" y="80.006" width="104" height="16"/>
							</g>
						</g>
						<g>
							<g>
								<rect x="40.003" y="112.006" width="72" height="16"/>
							</g>
						</g>
						<g>
							<g>
								<rect x="40.003" y="144.006" width="72" height="16"/>
							</g>
						</g>
						<g>
							<g>
								<rect x="0.003" y="464.006" width="480" height="16"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M463.163,68.43l-32-64c-1.973-3.954-6.776-5.56-10.73-3.587c-0.002,0.001-0.004,0.002-0.006,0.003l-64,32
								c-3.954,1.975-5.559,6.782-3.584,10.736s6.782,5.559,10.736,3.584l46.352-23.16l-12.648,36.112
								C341.69,218.446,198.501,329.522,31.323,344.006l1.36,16C206.16,344.978,354.737,229.701,412.395,65.398l12.912-36.896
								l23.536,47.08c1.975,3.954,6.782,5.559,10.736,3.584C463.534,77.191,465.138,72.385,463.163,68.43z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M96.003,384.006h-56c-4.418,0-8,3.582-8,8v48c0,4.418,3.582,8,8,8h56c4.418,0,8-3.582,8-8v-48
								C104.003,387.588,100.421,384.006,96.003,384.006z M88.003,432.006h-40v-32h40V432.006z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M184.003,360.006h-56c-4.418,0-8,3.582-8,8v72c0,4.418,3.582,8,8,8h56c4.418,0,8-3.582,8-8v-72
								C192.003,363.588,188.421,360.006,184.003,360.006z M176.003,432.006h-40v-56h40V432.006z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M272.003,320.006h-56c-4.418,0-8,3.582-8,8v112c0,4.418,3.582,8,8,8h56c4.418,0,8-3.582,8-8v-112
								C280.003,323.588,276.421,320.006,272.003,320.006z M264.003,432.006h-40v-96h40V432.006z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M360.003,264.006h-56c-4.418,0-8,3.582-8,8v168c0,4.418,3.582,8,8,8h56c4.418,0,8-3.582,8-8v-168
								C368.003,267.588,364.421,264.006,360.003,264.006z M352.003,432.006h-40v-152h40V432.006z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M448.003,152.006h-56c-4.418,0-8,3.582-8,8v280c0,4.418,3.582,8,8,8h56c4.418,0,8-3.582,8-8v-280
								C456.003,155.588,452.421,152.006,448.003,152.006z M440.003,432.006h-40v-264h40V432.006z"/>
							</g>
						</g>
					</svg>


				</div>
				<div class="head text-center">
					<h3 class="heading-level-3">Upsells and Cross Sells</h3>
				</div>
				<div class="vertical-diviver"></div>
				<div class="content text-center">
					<p class="text-level-2">Quickly and easily set up your Upsells and Cross Sells with the click of a button to supercharge your sales potential!</p>
				</div>
			</div>
		</div>
		<div class="pull-left col-12 col-xs-12 col-sm-6 col-md-3 col-lg-3 col-space">
			<div class="right-border"></div>
			<div class="info-block info-block-display" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="400" data-aos-offset="0">
				<div class="icon-block text-center">
					<svg class="svg-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
					<path d="M438.942,451.898c-5.518,0-9.997,4.479-9.997,9.997s4.479,9.997,9.997,9.997s9.997-4.479,9.997-9.997
					S444.46,451.898,438.942,451.898z"/>
					<path d="M375.962,451.898c-5.518,0-9.997,4.479-9.997,9.997s4.479,9.997,9.997,9.997s9.997-4.479,9.997-9.997
					S381.48,451.898,375.962,451.898z"/>
					<path d="M315.981,451.898c-5.518,0-9.997,4.479-9.997,9.997s4.479,9.997,9.997,9.997s9.997-4.479,9.997-9.997
					S321.499,451.898,315.981,451.898z"/>
					<path d="M250.002,451.898c-5.518,0-9.997,4.479-9.997,9.997s4.479,9.997,9.997,9.997s9.997-4.479,9.997-9.997
					S255.52,451.898,250.002,451.898z"/>
					<path d="M190.021,451.898c-5.518,0-9.997,4.479-9.997,9.997s4.479,9.997,9.997,9.997s9.997-4.479,9.997-9.997
					S195.539,451.898,190.021,451.898z"/>
					<path d="M130.04,451.898c-5.518,0-9.997,4.479-9.997,9.997s4.479,9.997,9.997,9.997s9.997-4.479,9.997-9.997
					S135.558,451.898,130.04,451.898z"/>
					<path d="M70.059,451.898c-5.518,0-9.997,4.479-9.997,9.997s4.479,9.997,9.997,9.997s9.997-4.479,9.997-9.997
					S75.577,451.898,70.059,451.898z"/>
					<path d="M461.935,411.83h-12.996V309.943c0-5.521-4.476-9.997-9.997-9.997h-82.974c-5.521,0-9.997,4.476-9.997,9.997V411.83
					h-85.973V279.952c0-17.576-5.593-34.255-16.176-48.233c-9.723-12.843-23.341-22.586-38.547-27.651l-65.612-62.422l204.269-24.848
					c3.594,4.004,7.617,7.559,12.035,10.634v32.558h-9.997c-3.787,0-7.248,2.139-8.941,5.526l-19.994,39.987
					c-1.924,3.849-1.17,8.497,1.872,11.539l39.987,39.987c3.904,3.904,10.234,3.904,14.139,0c3.904-3.904,3.904-10.234,0-14.139
					l-34.893-34.892l14.009-28.016h87.612l14.009,28.016l-34.893,34.892c-3.904,3.904-3.904,10.234,0,14.139
					c1.952,1.952,4.511,2.928,7.069,2.928c2.558,0,5.117-0.976,7.069-2.928l39.987-39.987c3.042-3.043,3.797-7.691,1.872-11.539
					l-19.994-39.987c-1.693-3.387-5.155-5.526-8.941-5.526h-9.997v-32.554c18.63-13.008,29.99-34.497,29.99-57.418
					c0-38.586-31.392-69.978-69.978-69.978c-25.977,0-49.492,14.25-61.618,36.801L90.729,42.03c-0.877,0.019-1.719,0.161-2.524,0.39
					c-5.905-1.579-11.988-2.392-18.146-2.392c-38.586,0-69.978,31.392-69.978,69.978c0,37.28,29.305,67.839,66.088,69.864
					l45.451,58.657c-7.583,12.456-11.57,26.627-11.57,41.425v131.877H50.065c-13.368,0-25.939,5.209-35.397,14.668
					C5.209,435.956,0,448.526,0,461.895c0,27.606,22.459,50.065,50.065,50.065h411.87c13.368,0,25.939-5.209,35.397-14.668
					c9.459-9.458,14.668-22.029,14.668-35.397C512,434.289,489.541,411.83,461.935,411.83z M415.949,159.99h-39.987v-22.91
					c6.457,1.921,13.194,2.916,19.994,2.916c6.8,0,13.537-0.995,19.994-2.916V159.99z M349.882,50.598
					c0-0.001,0.001-0.002,0.001-0.003c7.839-18.565,25.923-30.56,46.072-30.56c27.561,0,49.984,22.423,49.984,49.984
					c0,17.79-9.579,34.382-25.001,43.301c-7.555,4.373-16.194,6.683-24.983,6.683c-8.789,0-17.429-2.311-24.985-6.684
					c-5.817-3.365-10.82-7.784-14.874-13.143c-6.623-8.732-10.124-19.161-10.124-30.157C345.973,63.271,347.288,56.737,349.882,50.598
					z M365.965,319.94h62.98v91.971h-62.98V319.94z M197.158,222.46c0.006,0.002,0.013,0.004,0.02,0.006
					c25.216,7.501,42.827,31.14,42.827,57.487c0,33.074-26.907,59.981-59.981,59.981s-59.981-26.908-59.981-59.981
					c0-13.049,4.117-25.449,11.907-35.864c11.45-15.327,28.973-24.117,48.074-24.117C185.884,219.971,191.648,220.808,197.158,222.46z
					M240.005,332.767v79.144H120.043v-79.144c14.666,16.636,36.112,27.16,59.981,27.16S225.339,349.402,240.005,332.767z
					M327.201,56.991c-0.803,4.264-1.223,8.612-1.223,13.027c0,9.831,2.009,19.336,5.87,28.109l-192.779,23.451
					c0.632-3.798,0.968-7.659,0.968-11.572c0-18.577-7.315-35.853-19.619-48.611L327.201,56.991z M68.035,159.938
					c-26.626-1.066-47.961-23.05-47.961-49.932c0-27.561,22.423-49.984,49.984-49.984c5.483,0,10.882,0.883,16.046,2.624
					c20.3,6.876,33.938,25.908,33.938,47.36c0,6.748-1.316,13.282-3.913,19.426c-0.683,1.617-1.479,3.251-2.354,4.834
					c-8.258,14.844-23.587,24.476-40.49,25.594C71.573,159.424,69.761,159.441,68.035,159.938z M89.422,177.238
					c13.526-3.916,25.662-11.866,34.681-22.8l48.266,45.919c-17.976,1.753-34.932,9.62-47.902,22.108L89.422,177.238z M483.079,483.04
					c-5.651,5.651-13.161,8.764-21.144,8.764H50.065c-16.493,0-29.909-13.417-29.909-29.91c0-7.983,3.113-15.493,8.765-21.145
					c5.651-5.651,13.161-8.764,21.144-8.764h59.981c0.414,0,0.82-0.032,1.221-0.081h137.515c0.401,0.049,0.807,0.081,1.221,0.081
					h105.966c0.414,0,0.82-0.032,1.221-0.081h80.534c0.401,0.049,0.807,0.081,1.221,0.081h22.993c16.492,0,29.909,13.417,29.909,29.91
					C491.844,469.878,488.731,477.388,483.079,483.04z"/>
					<path d="M395.956,40.028c-16.537,0-29.991,13.454-29.991,29.991c0,16.537,13.454,29.99,29.991,29.99
					c16.537,0,29.99-13.454,29.99-29.99C425.946,53.482,412.492,40.028,395.956,40.028z M395.956,80.016
					c-5.512,0-9.997-4.485-9.997-9.997s4.485-9.997,9.997-9.997s9.997,4.485,9.997,9.997S401.468,80.016,395.956,80.016z"/>
					<path d="M70.059,80.016c-16.537,0-29.991,13.454-29.991,29.991c0,16.537,13.454,29.99,29.991,29.99s29.991-13.454,29.991-29.99
					C100.049,93.469,86.596,80.016,70.059,80.016z M70.059,120.003c-5.512,0-9.997-4.485-9.997-9.997c0-5.512,4.485-9.997,9.997-9.997
					s9.997,4.485,9.997,9.997C80.056,115.518,75.571,120.003,70.059,120.003z"/>
					<path d="M180.024,239.965c-22.049,0-39.987,17.937-39.987,39.987c0,22.049,17.938,39.987,39.987,39.987
					s39.987-17.938,39.987-39.987C220.011,257.903,202.073,239.965,180.024,239.965z M180.024,299.946
					c-11.025,0-19.994-8.969-19.994-19.994c0-11.024,8.969-19.994,19.994-19.994c11.024,0,19.994,8.969,19.994,19.994
					C200.018,290.977,191.049,299.946,180.024,299.946z"/>
				</svg>

			</div>
			<div class="head text-center">
				<h3 class="heading-level-3">Dedicated Support</h3>
			</div>
			<div class="vertical-diviver"></div>
			<div class="content text-center">
				<p class="text-level-2">We’re here to do all the hard work, so you don’t have to. After integrating your links onto your existing website, we’ll take care of the rest, instantly and automatically delivering your Digital Media products every time a customer places an order (or anyone requests viewing permission). With DMD, you don’t have to worry about a thing. Your sensitive content is safe with us.</p>
			</div>
		</div>
	</div>
	<div class="pull-left col-12 col-xs-12 col-sm-6 col-md-3 col-lg-3 col-space">
		<div class="info-block" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="600" data-aos-offset="0">
			<div class="icon-block text-center">
				<svg class="svg-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 470 470" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 470 470">
					<path d="m202.5,405.883v-120.208c0-4.143-3.357-7.5-7.5-7.5h-50c-4.143,0-7.5,3.357-7.5,7.5s3.357,7.5 7.5,7.5h42.5v112.708c-14.315,3.396-25,16.278-25,31.617 0,17.921 14.579,32.5 32.5,32.5s32.5-14.579 32.5-32.5c0-15.339-10.685-28.221-25-31.617zm-7.5,49.117c-9.649,0-17.5-7.851-17.5-17.5s7.851-17.5 17.5-17.5 17.5,7.851 17.5,17.5-7.851,17.5-17.5,17.5z"/>
					<path d="m282.5,405.883v-120.208c0-4.143-3.357-7.5-7.5-7.5h-49.999c-4.143,0-7.5,3.357-7.5,7.5s3.357,7.5 7.5,7.5h42.499v112.708c-14.315,3.396-25,16.278-25,31.617 0,17.921 14.579,32.5 32.5,32.5s32.5-14.579 32.5-32.5c0-15.339-10.685-28.221-25-31.617zm-7.5,49.117c-9.649,0-17.5-7.851-17.5-17.5s7.851-17.5 17.5-17.5 17.5,7.851 17.5,17.5-7.851,17.5-17.5,17.5z"/>
					<path d="m410,355c-15.339,0-28.221,10.685-31.617,25h-15.883v-94.325c0-4.143-3.357-7.5-7.5-7.5h-50c-4.143,0-7.5,3.357-7.5,7.5s3.357,7.5 7.5,7.5h42.5v94.325c0,4.143 3.357,7.5 7.5,7.5h23.383c3.396,14.315 16.278,25 31.617,25 17.921,0 32.5-14.579 32.5-32.5s-14.579-32.5-32.5-32.5zm0,50c-9.649,0-17.5-7.851-17.5-17.5s7.851-17.5 17.5-17.5 17.5,7.851 17.5,17.5-7.851,17.5-17.5,17.5z"/>
					<path d="M452.141,161.557c-10.108-12.55-23.837-21.848-39.113-26.592c-1.72-35.503-16.45-68.628-41.84-93.806   C344.422,14.617,308.922,0,271.226,0c-49.501,0-95.306,25.812-121.066,67.787c-6.264-1.975-12.775-2.972-19.434-2.972   c-30.637,0-56.983,21.626-63.336,51.105C28.044,126.127,0,161.988,0,203.101c0,49.667,40.406,90.074,90.073,90.074H107.5V380   H91.617C88.221,365.685,75.339,355,60,355c-17.921,0-32.5,14.579-32.5,32.5S42.079,420,60,420c15.339,0,28.221-10.685,31.617-25   H115c4.143,0,7.5-3.357,7.5-7.5V285.675c0-4.143-3.357-7.5-7.5-7.5H90.073C48.678,278.175,15,244.497,15,203.101   c0-35.723,25.395-66.688,60.383-73.627c3.155-0.625,5.561-3.193,5.978-6.383c3.229-24.671,24.451-43.275,49.365-43.275   c6.854,0,13.501,1.373,19.756,4.08c3.546,1.535,7.675,0.125,9.543-3.254C182.409,40.152,225.019,15,271.226,15   c69.384,0,126.34,56.438,126.966,125.812c0.031,3.466,2.433,6.458,5.809,7.239c30.028,6.949,51,33.351,51,64.203   c0,36.349-29.571,65.921-65.92,65.921H385c-4.143,0-7.5,3.357-7.5,7.5s3.357,7.5,7.5,7.5h4.08c44.619,0,80.92-36.301,80.92-80.921   C470,193.859,463.657,175.854,452.141,161.557z M60,405c-9.649,0-17.5-7.851-17.5-17.5S50.351,370,60,370s17.5,7.851,17.5,17.5   S69.649,405,60,405z"/>
					<path d="m365.692,148.605h0.069c4.142-0.038 7.469-3.426 7.432-7.568-0.112-12.354-2.444-24.443-6.931-35.931-1.508-3.858-5.856-5.765-9.715-4.258s-5.765,5.856-4.258,9.715c3.822,9.784 5.808,20.083 5.903,30.61 0.039,4.12 3.389,7.432 7.5,7.432z"/>
					<path d="m338.278,86.756c1.483,1.796 3.626,2.724 5.786,2.724 1.683,0 3.375-0.563 4.772-1.718 3.193-2.638 3.644-7.365 1.006-10.559-19.53-23.643-48.185-37.203-78.615-37.203-4.143,0-7.5,3.357-7.5,7.5s3.357,7.5 7.5,7.5c25.94,0 50.379,11.574 67.051,31.756z"/>
				</svg>
			</div>
			<div class="head text-center">
				<h3 class="heading-level-3">Product Hosting</h3>
			</div>
			<div class="vertical-diviver"></div>
			<div class="content text-center">
				<p class="text-level-2">Why pay to host your product, when we’ll do it free of charge or hassles? We’ll feature your Digital Media product(s) on our high-end dedicated US-based Amazon S3 servers, where your Digital Media will receive the royal treatment 24/7, 365. This guarantees an excellent experience for your clients or partners. We handle all backups and media delivery too, with full PCI compliance.</p>
			</div>
		</div>
	</div>
<!-- 	<div class="pull-left col-12 col-xs-12 col-sm-6 col-md-3 col-lg-3 col-space mx-auto">
		<div class="right-border"></div>
		<div class="left-border"></div>
		<div class="info-block last-info-block" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="800" data-aos-offset="0">
			<div class="icon-block text-center">
				<svg class="svg-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
				<path d="M453.034,361.322c-2.808-1.164-6.04-0.521-8.19,1.628l-82.126,82.126c-2.149,2.15-2.792,5.382-1.628,8.19
				c1.162,2.809,3.904,4.639,6.943,4.639h82.126c4.151,0,7.515-3.364,7.515-7.515v-82.126
				C457.673,365.225,455.842,362.485,453.034,361.322z M386.173,442.876l56.469-56.469v56.469H386.173z"/>
				<path d="M507.361,240.867c-2.808-1.164-6.04-0.521-8.19,1.628l-22.507,22.507V111.971c0-4.151-3.365-7.515-7.515-7.515h-57.741
				l20.96-20.96c0.003-0.003,0.006-0.007,0.009-0.01l20.196-20.196c4.327-4.327,6.71-10.083,6.71-16.203s-2.383-11.874-6.71-16.203
				L429.219,7.53c-4.327-4.327-10.083-6.71-16.203-6.71s-11.875,2.383-16.203,6.71l-20.206,20.206l-24.416,24.416l-52.303,52.303
				H66.62V30.11c0-4.151-3.365-7.515-7.515-7.515C26.515,22.597,0,49.11,0,81.702v319.186c0,0.555,0.064,1.095,0.178,1.615
				c1.038,13.829,7.021,26.678,17.02,36.432C27.886,449.359,41.995,455.1,56.927,455.1h229.638l-43.249,43.249
				c-2.149,2.15-2.792,5.381-1.629,8.19c1.163,2.808,3.904,4.639,6.943,4.639h255.855c4.15,0,7.515-3.364,7.515-7.515V247.81
				C512,244.771,510.168,242.03,507.361,240.867z M407.443,18.159c3.072-3.074,8.077-3.074,11.149,0l23.355,23.355
				c3.074,3.073,3.074,8.076,0,11.15l-14.891,14.892L392.55,33.051L407.443,18.159z M381.922,43.679l34.505,34.504l-13.789,13.789
				l-34.503-34.504L381.922,43.679z M496.972,332.252h-13.865c-4.151,0-7.515,3.364-7.515,7.515c0,4.151,3.364,7.515,7.515,7.515
				h13.865v22.646h-13.865c-4.151,0-7.515,3.364-7.515,7.515s3.364,7.515,7.515,7.515h13.865v22.645h-13.865
				c-4.151,0-7.515,3.364-7.515,7.515c0,4.151,3.364,7.515,7.515,7.515h13.865v22.646h-13.865c-4.151,0-7.515,3.365-7.515,7.515
				c0,4.151,3.364,7.515,7.515,7.515h13.865v35.842h-230.2l75.908-75.907c2.935-2.936,2.935-7.693,0-10.628
				c-0.337-0.337-0.699-0.629-1.078-0.889c-2.926-2.004-6.952-1.709-9.551,0.889l-30.456,30.456H56.928
				c-22.792,0-41.14-17.794-41.862-40.545c0.043-1.428,0.154-2.844,0.332-4.244c0.001-0.008,0.002-0.016,0.003-0.024
				c0.084-0.665,0.185-1.327,0.3-1.985c0.013-0.075,0.027-0.15,0.04-0.225c0.105-0.587,0.223-1.171,0.352-1.753
				c0.036-0.162,0.074-0.324,0.112-0.485c0.114-0.492,0.237-0.981,0.369-1.468c0.072-0.264,0.147-0.525,0.224-0.788
				c0.11-0.382,0.224-0.763,0.345-1.14c0.121-0.377,0.25-0.75,0.381-1.123c0.091-0.261,0.181-0.522,0.279-0.781
				c0.183-0.492,0.377-0.979,0.577-1.462c0.057-0.139,0.113-0.28,0.172-0.419c0.257-0.6,0.524-1.193,0.806-1.779
				c0.012-0.025,0.023-0.05,0.035-0.075c5.878-12.164,17.183-21.233,30.75-24.047c0.038-0.008,0.076-0.017,0.114-0.025
				c0.622-0.127,1.25-0.236,1.882-0.337c0.105-0.017,0.21-0.038,0.317-0.054c0.571-0.087,1.148-0.155,1.725-0.22
				c0.162-0.018,0.323-0.043,0.486-0.059c0.557-0.056,1.119-0.093,1.681-0.128c0.178-0.011,0.356-0.03,0.534-0.039
				c0.736-0.037,1.478-0.056,2.223-0.056c4.15,0,7.515-3.364,7.515-7.515v-54.489c0-4.151-3.365-7.515-7.515-7.515
				c-4.15,0-7.515,3.364-7.515,7.515v47.447c-12.69,1.605-24.469,7.265-33.757,16.335c-0.977,0.954-1.903,1.946-2.805,2.954V81.702
				c0-21.741,15.826-39.858,36.561-43.436v73.705v152.777v0.001c0,4.15,3.365,7.515,7.515,7.515c4.15,0,7.515-3.365,7.515-7.515
				v-0.001V119.486h218.24l-41.544,41.544c-2.935,2.936-2.935,7.693,0,10.628c2.935,2.934,7.693,2.934,10.627,0l54.373-54.373
				c0.002-0.002,0.003-0.004,0.005-0.005l49.185-49.185l34.503,34.505l-4.048,4.048c-0.003,0.003-0.007,0.006-0.01,0.009
				l-151.528,151.53l-34.504-34.504l29.868-29.868c2.935-2.936,2.935-7.693,0-10.628c-2.935-2.934-7.693-2.934-10.627,0
				l-35.183,35.182c-0.188,0.188-0.364,0.387-0.529,0.591c-0.055,0.068-0.104,0.141-0.156,0.212c-0.106,0.141-0.21,0.283-0.307,0.431
				c-0.055,0.086-0.105,0.174-0.157,0.263c-0.083,0.14-0.163,0.282-0.236,0.427c-0.048,0.094-0.091,0.189-0.135,0.287
				c-0.067,0.147-0.129,0.297-0.186,0.447c-0.02,0.053-0.046,0.103-0.065,0.156l-14.641,41.475l-9.982,28.279
				c-0.132,0.376-0.227,0.758-0.298,1.143h-24.992c-4.15,0-7.515,3.364-7.515,7.515c0,4.151,3.365,7.515,7.515,7.515h208.114
				c4.151,0,7.515-3.364,7.515-7.515c0-4.151-3.364-7.515-7.515-7.515H193.093l45.83-16.178c0.038-0.014,0.074-0.033,0.112-0.047
				c0.178-0.066,0.353-0.14,0.526-0.219c0.069-0.032,0.138-0.062,0.205-0.095c0.18-0.09,0.357-0.189,0.531-0.295
				c0.056-0.034,0.114-0.065,0.169-0.1c0.181-0.116,0.357-0.243,0.53-0.377c0.043-0.033,0.088-0.062,0.13-0.096
				c0.209-0.168,0.414-0.349,0.608-0.543l154.644-154.643h65.255v160.546L354.137,387.53c-2.935,2.936-2.935,7.693,0,10.628
				c0.183,0.183,0.374,0.355,0.571,0.516c1.378,1.123,3.061,1.685,4.743,1.685s3.366-0.562,4.743-1.685
				c0.197-0.16,0.388-0.333,0.571-0.516c0.003-0.003,0,0,0.003-0.003L496.972,265.95V332.252z M222.677,265.696l-21.526,7.599
				l-14.342-14.342l7.599-21.526L222.677,265.696z"/></svg>

			</div>
			<div class="head text-center">
				<h3 class="heading-level-3">Experienced Team</h3>
			</div>
			<div class="vertical-diviver"></div>
			<div class="content text-center">
				<p class="text-level-2">You’ve got enough on your plate. Technological upgrades and jargon shouldn’t add to it. That’s for us to worry about, and you to forget about. We’ll take care of the tech, so you can focus on growing your empire!</p>
			</div>
		</div>
	</div> -->
	<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
		<div class="block-btn text-center">
			<a href="<?php echo base_url('blog/features');?>" class="btn-sm-default btn btn-lg">Explore Amazing Features</a>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
</div>
<!-- end home section 6 -->


<!-- home section 7 -->
<div class="container-fluid bg-home-section-7 main-container">
	<div class="row row-720">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space" data-aos="zoom-in" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
			<div class="head text-center">
				<h1 class="heading-level-2">Any Digital Media Product. Anytime. Anywhere.</h1>
			</div>
			<div class="content text-center">
				<p class="text-level-1">From tiny MP3s to 1000-page PDF files, videos, templates, training courses, plug-ins - you name it - DMD has a range of options available. There’s a package for every business and a price for every budget. With a 7-day no-obligation free trial, what’s there to lose? Exactly.</p>
			</div>
			<div class="block-btn text-center">
				<a href="/user/register" class="btn-sm-default btn btn-lg">Sign Up Free Now</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- home section 7 -->

<!-- home section 8 -->
<div class="container-fluid main-container section-padding">
	<div class="row row-fixed-width">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
			<div class="head text-center">
				<h1 class="heading-level-2">How are your stores performing? DMD has more answers than questions. View live statistics for every store. Quickly identify best-sellers, popular products, units sold, and more. Education is power, and DMD puts the power in your hands.</h1>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row row-fixed-width">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
			<div class="block-graph">
				<img src="<?php echo base_url(); ?>site_assets/images/img-15.png">
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- end home section 8 -->



<!-- home section 9 -->
<div class="container-fluid main-container">
	<div class="row row-stretched">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space mx-auto text-center">
			<div class="section-content-block">
				<div class="head">
					<!-- <div class="divider" data-aos="fade-left"></div> -->
					<h1 class="heading-level-2"></h1>
				</div>
				<div class="content">
					<p class="text-level-1">There’s nothing to lose by putting Digital Media Deliveries to the test for 7 days, at no cost to you. You’ll gain exclusive access to our platform’s full functionality. Love it? You’re welcome to stick around. Not so keen? There are no obligations to stick around, and there won’t be any hard feelings. However, we’re confident you’ll love DMD just as much as our other clients do. We’re sure your only regret will be that you didn’t sign up yesterday!
					</p>
				</div>
				<div class="block-btn text-center">
					<a  data-aos="fade-right" href="/user/register"class="btn-sm-default btn btn-lg">START FREE TRIAL</a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- end home section 9 -->


<!-- home section 9 -->
<div class="container-fluid main-container">
	<div class="row row-stretched">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
			<div class="section-content-block">
				<div class="head">
					<!-- <div class="divider" data-aos="fade-right"></div> -->
					<h1 class="heading-level-2">337% Sales Increase? Yes Please.</h1>
				</div>
				<div class="content">
					<p class="text-level-1">Who said you needed to hire a pro video maker to boost sales? We’re ready to give you a FREE copy of the market’s #1 Doodle Software, proven to send sales soaring. Create instant, professional, fully interactive sketch videos in minutes, with drag-and-drop functionality on PC and Mac. Did you know that the human mind processes visual content 60,000 times faster than text? This isn’t a stat you want to ignore. Download now and start making magic with us!
					</p>
				</div>
				<div class="block-btn text-left">
					<a href="/user/register"class="btn-sm-default btn btn-lg">SIGN ME UP!</a>
				</div>
			</div>
		</div>
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-space">
			<div class="block-video block-video-2 text-right">
				<!-- <?php echo base_url('site_assets/video/') . $store['userfile']; ?> -->
				<video class="img-responsive" src="<?php echo base_url('site_assets/images/video-2.mp4'); ?>"  controlsList="nodownload" width="100%" style="max-width:850px;" id="active-video"></video> 
				<img  src="<?php echo base_url(); ?>site_assets/images/video.png" class="img-show" onclick="Play()" id="vid-play2" >
			</div>
			<div class="block-btn text-center" style="margin-top:-40px;">
				<a href="https://onlinesuccessexpert.com/easy-sketch/?id=7029"  class="btn-sm-default btn btn-lg">DOWNLOAD NOW</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- end home section 9 -->


<!-- home section 10 -->
<div class="container-fluid main-container home-container bg-home-section-10">
	<div class="row home-row-4 row-normal row-integrations row-normal mx-auto home-row  margin-default">
		<div class="col-md-12 text-center"> 
			<h4 class="heading-level-2"> Integrations</h4>
		</div>
		<div class="col-md-12 text-center methods col2">
			<ul class="list-inline">
				<li class="list-inline-item payment-method-img" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
					<img src="<?php echo base_url(); ?>site_assets/images/Authorize-net-logo.png">
				</li>

				<li class="list-inline-item payment-method-img" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="400" data-aos-offset="0">
					<img src="<?php echo base_url(); ?>site_assets/images/paypal-784404_960_720.png">
				</li>

			<!-- 	<li class="list-inline-item payment-method-img" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="600" data-aos-offset="0">
					<img src="<?php echo base_url(); ?>site_assets/images/sage_payment.png">
				</li> -->

				<li class="list-inline-item payment-method-img" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="800" data-aos-offset="0">
					<img src="<?php echo base_url(); ?>site_assets/images/Stripe_logo,_revised_2016.png">
				</li>

			<!-- 	<li class="list-inline-item payment-method-img" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="1000" data-aos-offset="0">
					<img src="<?php echo base_url(); ?>site_assets/images/2checkout-logo-vector.png">
				</li> -->

				<li class="list-inline-item payment-method-img" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="1200" data-aos-offset="0">
					<img src="<?php echo base_url(); ?>site_assets/images/MailChimp-Logo.png">
				</li>

				<li class="list-inline-item payment-method-img" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="1400" data-aos-offset="0">
					<img src="<?php echo base_url(); ?>site_assets/images/aweber_integration_liveagent.png">
				</li>

				<li class="list-inline-item payment-method-img" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="1600" data-aos-offset="0">
					<img src="<?php echo base_url(); ?>site_assets/images/getresponse_logotype.png">
				</li>

			</ul>
		</div>
	</div>
</div>
<!-- end home section 10 -->

<script>
	function menuOpen(){

		document.getElementById("menu-option").style.width="250px";

	}
	function menuClose(){
		document.getElementById("menu-option").style.width="0px";
	}
	var   video =document.getElementById("vid");
	var   butn =document.getElementById("vid-play, vid-play2");


	function Play(){
		if (video.paused) {
			video.play();




		}    
		else
		{

			video.pause();

		}



	}


</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/vissense/0.10.0/vissense.min.js"></script>
<script>
	var myVideo = document.getElementById('active-video');

	var videoElementArea = VisSense(myVideo);

	var monitorBuilder = VisSense.VisMon.Builder(videoElementArea);

	monitorBuilder.on('fullyvisible', function() {
		myVideo.play();  
	});
	monitorBuilder.on('hidden', function() {
		myVideo.pause();  
	});
	var videoVisibilityMonitor = monitorBuilder.build();
	videoVisibilityMonitor.start();


</script>