<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				
				<?php
				$action = "page_insert";
				if(isset($view_items)){
					$view_items = $view_items[0];
					$action = "update";
				}

				$message = $this->session->flashdata('message');
				if ($message) { ?>
					<div class="alert alert-info" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
						<?php echo $message; ?>
					</div>
				<?php } ?>

				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green"><?php if(isset($view_items)){echo $view_items->title;} ?></h2>
							<!-- <p class="paragraph-text-sm-grey">Add New Pages and contents</p> -->
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-12">
					<div id="content"><?php if(isset($view_items)){echo $view_items->content;} ?></div>
				</div>
			</div>
		</div>
	</div>
</div>

