<?php
$user_role =(string)$this->session->userdata('user_role');
if($user_role != '0' and $user_role != '1'){
  //echo $user_role;
  redirect(base_url());
}
?>


<div class="col-md-3 left-menu"><!-- left-menu-start-->
 <?php
 $store_id = $this->session->userdata('store_id');
 $class = "disab";
 $store_result = array();
 $user_ID = $this->session->userdata('user_id');
 $query = $this->db->query("select * from dmd_stores where user_id='".$user_ID."'");
 if($query->num_rows()>0){
   $store_result = $query->result_array();   
   $store_id = $query->row_array();
   $store_id = $store_id['id'];

   if($query->num_rows()==1){
    $class = "";
    $this->session->set_userdata('store_id', $store_id);  
  }              
}


//print_r($store_result);
$store_list = '';
$selected = 'Select a store';
$selected_url = '';
if(count($store_result) > 0){
  $active_store_id =0;
  foreach ($store_result as $key => $value) {
    if($value['id'] == $this->session->userdata('store_id')){
      $selected = $value['store_name'];
      $selected_url = $value['store_url'];
      $active_store_id = $value['id'];
      $store_list .= '<a href="' . base_url('store/active/'.$value['id']) . '" class="w3-bar-item w3-button actve-content">' . $value['store_name'] . '</a>';
    }
    if ($active_store_id != $value['id']){
      $store_list .= '<a href="' . base_url('store/active/'.$value['id']) . '" class="w3-bar-item w3-button">' . $value['store_name'] . '</a>';
    }
  }

  ?>
  <ul class="custom-dropdown">
    <div class="w3-dropdown-hover">
      <button class="menu-name w3-button"><?php echo $selected; ?></button> <i class="fa fa-angle-down"></i>
      <!--<a class="link" target="blank" href="#"> link</a>-->
      <div class="w3-dropdown-content w3-bar-block w3-card-4">
        <?php echo $store_list; ?>
      </div>
    </div>
  </ul>

<?php } ?>

<?php if($selected_url){ ?>
  <ul><span class="menu-name"><a style="color:#458fb4;" target="_blank" href="<?php echo 'https://' . $selected_url; ?>"> Link to store</a></span></ul>
<?php } ?>

<ul><li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('store/add'); ?>">+ Add New Store</a></li></ul>

<!--
<?php
if($this->session->userdata('store_id')!=''){
  $class = "";
}
if(count($store_result)>1){

  ?>
  <ul>
    <?php foreach($store_result as $res){ ?>
      <li  class="menu-item"><a target="_blank" href="<?php echo 'http://' . $res['store_url']; ?>"><?php echo $res['store_url']; ?></a></li>
    <?php
    }
  }
  ?>
</ul> -->
<style>
.disab
{
  pointer-events: none;

  /* for "disabled" effect */
  opacity: 0.5;
  background: #CCC;
}
</style>

<div class="<?php echo $class ?>">
  <ul > <span class="menu-name">product tools</span>
   <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('product'); ?>">products</a></li>
   <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('product/add'); ?>">new product</a></li>
 </ul>

 <ul> <span class="menu-name">Sales</span>
   <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('product/purchase'); ?>">purchases</a></li>
   <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('customer'); ?>">customers</a></li>
   <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('reports'); ?>">Reports</a></li>
   <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('customer/import'); ?>">Import customers</a></li>
 </ul>

 <ul> <span class="menu-name">Store Setup</span>
   <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('store/update/'.$store_id); ?>">preferences</a></li>
   <!--<li  class="menu-item"><a href="sales_policies.html">Policies</a></li>-->
   <!-- <li  class="menu-item"><a href="#">Affiliate options</a></li> -->
   <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('store/coupons'); ?>">Coupons</a></li>
   <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('store/tax'); ?>">Tax Rules</a></li>
   <?php if($this->session->userdata('user_type') == 'subscriber'): ?>
     <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('store/paymentMethod'); ?>">Payment Methods</a></li>
   <?php endif; ?>
   <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('store/integration'); ?>">Integrations</a></li>
 </ul>

 <ul> <span class="menu-name">Look and Feel</span>
  <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('store/customize'); ?>">Store Customization</a></li>
  <li class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('product/featured'); ?>">Featured Products</a></li>
  <li class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('store/checkoutPage'); ?>">logo Setup</a></li>
  <li style="display: none;"  class="menu-item"><a href="#">Theme</a></li>
  <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('customization/packages'); ?>">Request to customize</a></li>
  <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('store/emailTemplates'); ?>">Email Templates</a></li>

</ul>

<ul style="display: none;"> <span class="menu-name">Affiliates</span>
 <li  class="menu-item hvr-bounce-to-right"><a href="#">Programe Manager</a></li>
 <li  class="menu-item hvr-bounce-to-right"><a href="#">DPD Referrals</a></li>
</ul>
</div>

			</div><!-- left-menu-end-->