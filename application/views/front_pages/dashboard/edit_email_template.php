<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>



	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<form action="/store/editEmailTemplete/<?php echo $edit_email['id']; ?>" method="post" id="frmEdit">
			<div class="row">
				<div id="showError"></div>

				<?php 
					/*echo "<pre>";
					print_r($edit_email);
					echo "</pre>";*/

					if($this->session->flashdata('emailMsg')){
						?> 
						<div id="emailMsg" class="alert alert-info alert-dismissible" style="margin-top: 50px">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Info!</strong> <?php echo $this->session->flashdata('emailMsg'); ?>
						</div>
					<?php } ?>

					<div class="col-md-12 inner-body-head"><!-- full block start-->
						<div class="nauk-info-connections">
							<div class="page-header">
								<div class="pull-left">
									<h2 class="heading-lg-green">Edit email template</h2>
									<p class="paragraph-text-sm-grey">Edit email template</p>								
								</div>
							</div>
						</div>

					</div>

					

					<div class="col-md-12">
						<div class="form-group">
							<input type="text"  
							name="subject" 
							id="subject"  
							placeholder="subject"
							class = "form-control form-input border-dark-1 validThis" 
							value="<?php if(isset($edit_email['subject'])) {echo $edit_email['subject'];} ?>"
							/>
							<!-- <span class="text-danger">Validation error</span> -->
						</div>
					</div> 

					<div class="col-md-12 editor">
						<label for="email-editor" class=" heading-md-grey">Email Content</label>
						<div class="form-group">
							<input name="content" 
							id="edit-email-content-editor" 
							class="form-control form-input border-dark-1 validThis"
							/>
						</div>
					</div> 



					<div class="col-md-12 inner-body-block text-center">
						<a href="/store/emailTemplates" class="btn btn-sm-blue">cancel</a> &nbsp; &nbsp;
						<button id="sbmtt" class="btn-sm-blue">update</button>

					</div>

					
							<!-- <div class="col-md-4 inner-body-block">
								<div class="nauk-info-connections text-center">
									<a href="/store/delete_email/<?php //echo $edit_email['id']; ?>" class="btn btn-sm-blue btn-top-margin-more">delete</a>
								</div>
							</div> -->	




							<script>
								$(document).ready(function() {
									var editor = $("#edit-email-content-editor");
									var s = "<?php if(isset($edit_email['content'])) {echo addslashes($edit_email['content']);} ?>";
									editor.Editor();
									editor.Editor("setText", s);

									$("#sbmtt").on("click", function(e){
										e.preventDefault();

										$("#edit-email-content-editor").val($("#edit-email-content-editor").Editor("getText"));

							//$("#frmEditor").submit();
							formValidate('frmEdit');
						});

								});
							</script>
						</div>
					</form>
				</div>


			</div>