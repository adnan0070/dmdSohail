<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<?php 

		if($this->session->flashdata('error_msg')){
			?>
			<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
			</div>

			<?php
		}

		if($this->session->flashdata('success_msg')){
			?>

			<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
			</div>


		<?php } ?>
		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Featured Products</h2>
							<p class="paragraph-text-sm-grey">Manage all your Featured products for your store front.</p>
						</div>
						<div class="pull-right">
							<a  href="<?php echo base_url().'product/addFeatured'; ?>" class="btn default-btn-green"><i class="fa fa-plus"></i> Add new</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->

		</div>
		<div class="row">
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				
				<table id="tbl_articles" class="table table-bordered table-striped" style="width:100%">
					<thead>
						<tr>
							<th>Product ID</th>
							<!--<th>label</th>-->
							<th>Image</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php  foreach($products as $product): ?>
							<tr>
								<td><?php echo $product['product_code']; ?></td>
								<!--
								<td>
									<?php $style ='color:'.$product['label_color'].';border-radius:50px;background:#fff;padding:2px 10px;border:1px solid '.$product['label_color'].';';?>
									<a href="#" target="_blank" class="btn btn-success" style="<?php echo $style; ?>:">
										<?php echo $product['label']; ?>
									</a>
								</td>-->
								<td><img width="60" height="60" src="<?php echo base_url('site_assets/products/') . $product['feature_image']; ?>" /></td>
								<td class="actions"><a href="<?php echo base_url('product/editFeature/') . $product['product_code']; ?>" class="btn default-btn-blue" ><i class="fa fa-pencil"></i> Edit</a>
									<a href="<?php echo base_url('product/RemoveFeature/') . $product['product_code']; ?>" class="btn default-btn-red"  onclick="return confirm('Are you sure to Remove?')"><i class="fa fa-trash"></i> Remove</a></td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#tbl_articles').DataTable({
				"pagingType": "full_numbers",
				"ordering": false,
				"searching": false,
				"bLengthChange": false,
	        //"bInfo": false,
	        "dom": '<"top"i>rt<"bottom"flp><"clear">'
	    });
		});
	</script>