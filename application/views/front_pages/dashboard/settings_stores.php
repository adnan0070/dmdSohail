<div class="row main-body mx-auto ">

	<?php $this->load->view('front_pages/dashboard/admin_dash_left');
	$store = $this->db->query('select * from settings');
	$store = $store->row_array();
	?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">


			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<?php
				$success_msg = $this->session->flashdata('success_msg');
				$error_msg   = $this->session->flashdata('error_msg');

				if ($success_msg) {
					?>

					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $success_msg; ?>
					</div>

					<?php
				}
				if ($error_msg) {
					?>


					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $error_msg; ?>
					</div>

					<?php
				}
				?>
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Settings</h2>
							<p class="paragraph-text-sm-grey">Modify Pages contents</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>



		</div>

		<form method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/updateSetting/'); ?>">
			<div class="row clearfix form">
				<div class="col-md-12">
					<label class="heading-md-grey">Name</label>
					<div class="form-group">
						<input type="hidden" name="old_image_path" id="old_image_path" value="<?php echo @$store['userfile']; ?>" />
						<input type="hidden" name="old_image" id="old_image" value="<?php echo @$store['userfile']; ?>" />

						<input type="hidden" name="old_image_path_home" id="old_image_path_home" value="<?php echo @$store['homefile']; ?>" />
						<input type="hidden" name="old_image_home" id="old_image_home" value="<?php echo @$store['homefile']; ?>" />

						<!-- <input type="hidden" name="old_tutorial_video_path" id="old_tutorial_video_path" value="<?php echo @$store['tutorialfile']; ?>" />
							<input type="hidden" name="old_tutorial_video" id="old_tutorial_video" value="<?php echo @$store['tutorialfile']; ?>" /> -->


							<input type="text"  value="<?php echo @$store['footer_left_heading']; ?>" name = "store[footer_left_heading]" class = "form-control form-input border-dark-1"  placeholder="footer left heading"/>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>
					


					<div class="col-md-12 editor">
						<label for="footer-left-text" class="heading-md-grey">Footer Left Text</label>
						<div class="form-group">
							<textarea name="store[footer_left_text]" rows="10" id="footer-left-text" class="form-control form-textarea border-dark-1"><?php echo @$store['footer_left_text']; ?></textarea>

							<!-- <span class="text-danger">Validation error</span> -->
						</div>
					</div>
					<div class="col-md-12 editor">
						<label for="home-text" class="heading-md-grey">Home Page Ready To Launch Text</label>
						<div class="form-group">
							<textarea name="store[home_header_text]" rows="10" id="home-text" class="form-control form-textarea border-dark-1"><?php echo @$store['home_header_text']; ?></textarea>

							<!-- <span class="text-danger">Validation error</span> -->
						</div>
					</div>

					<div class="col-md-12 editor">
						<label for="privacy-policy-editor" class="heading-md-grey">Dashboard New Feature Text</label>
						<div class="form-group">
							<textarea  id="feature-policy-editor" class="form-control form-textarea border-dark-1"><?php echo @$store['dash_feature_text']; ?></textarea>
							<textarea style="display: none;" name="store[dash_feature_text]" id="put_feature_desp"></textarea>
							<!-- <span class="text-danger">Validation error</span> -->
						</div>
					</div>

					<div class="col-md-12 editor">
						<label for="privacy-policy-editor" class="heading-md-grey">Knowledge base</label>
						<div class="form-group">
							<textarea  id="knowl-policy-editor" class="form-control form-textarea border-dark-1"><?php echo @$store['knowl_base']; ?></textarea>
							<textarea style="display: none;" name="store[knowl_base]" id="put_knowl_desp"></textarea>
							<!-- <span class="text-danger">Validation error</span> -->
						</div>
					</div>

					<div class="col-md-12 text-center">

						<video width="400" height="200" controls>
							<source src="<?php echo base_url('site_assets/video/'.@$store['homefile']); ?>" type="<?php echo $store['file_type']; ?>"/>
								<!--<source src="mov_bbb.ogg" type="video/ogg">-->
								</video>
							</div>
							<div class="col-md-12 text-center">

								<div class="nauk-info-connections block-file-upload block-border-dotted text-center">
									
									<h3 class="heading-sm-grey text-center">UPLOAD  video 1 for home page</h3>
									<div class="form-group">
										
										<input name="homefile"  type="file" id="fileInputhome" class="file-input"  placeholder="Select from computer">
										<!--<img id="output_image"/>-->
										<img class="btn-upload-file" src="<?php echo base_url(); ?>site_assets/images/btn-upload-file.png">
									</div>
									<div style="" class="form-group">
										<span class="paragraph-sm-grey" id="putfvalhome"></span>

									</div>

									<div style="display: none;" class="form-group">
										
										<input type="checkbox" class="border-dark-1" name="enable"> 
										<span class="paragraph-sm-grey">You can also drag and drop files.</span>

									</div>
									<!--<span class="text-danger">Validation error</span>-->
								</div> 
							</div> 



							<div class="col-md-12 text-center">
								<video width="400" height="200" controls>
									<source src="<?php echo base_url('site_assets/video/'.$store['userfile']); ?>" type="<?php echo $store['file_type']; ?>">
										<!--<source src="mov_bbb.ogg" type="video/ogg">-->
										</video>
									</div>
									<div class="col-md-12 text-center">

										<div class="nauk-info-connections block-file-upload block-border-dotted text-center">

											<h3 class="heading-sm-grey text-center">UPLOAD  video 2 for home page</h3>
											<div class="form-group">

												<input name="userfile"  type="file" id="fileInput" class="file-input"  placeholder="Select from computer">
												<!--<img id="output_image"/>-->
												<img class="btn-upload-file" src="<?php echo base_url(); ?>site_assets/images/btn-upload-file.png">
											</div>
											<div style="" class="form-group">
												<span class="paragraph-sm-grey" id="putfval"></span>

											</div>

											<div style="display: none;" class="form-group">

												<input type="checkbox" class="border-dark-1" name="enable"> 
												<span class="paragraph-sm-grey">You can also drag and drop files.</span>

											</div>
											<!--<span class="text-danger">Validation error</span>-->
										</div> 
									</div> 

								<!-- <div class="col-md-12 text-center">
									<video width="400" height="200" controls>
										<source src="<?php echo base_url('site_assets/video/'.$store['tutorialfile']); ?>" type="<?php echo $store['file_type']; ?>">
										</video>
									</div> -->
								<!-- 	<div class="col-md-12 text-center">

										<div class="nauk-info-connections block-file-upload block-border-dotted text-center">

											<h3 class="heading-sm-grey text-center">UPLOAD video for Tutorial</h3>
											<div class="form-group">

												<input name="tutorialfile" type="file" id="tutorialVideoInput" class="file-input"  placeholder="Select from computer">
												<img class="btn-upload-file" src="<?php echo base_url(); ?>site_assets/images/btn-upload-file.png">
											</div>
											<div style="" class="form-group">
												<span class="paragraph-sm-grey" id="putfvaltut"></span>

											</div>

											<div style="display: none;" class="form-group">

												<input type="checkbox" class="border-dark-1" name="enable"> 
												<span class="paragraph-sm-grey">You can also drag and drop files.</span>

											</div>
									 
										</div> 
									</div> 
								-->

								<div class="form-footer">
									
									<input style="display: none;" value="save" style="" id="submit_pro" type="submit" name="update_store" class="btn-sm-blue btn" />
									<input class="default-btn-green btn" onclick="get_descrip()" type="submit" id="update_store" value="save" name="update_store" >

								</div>


							</div>

						</form>

					</div>

				</div>
				<script>
					document.getElementById('fileInput').onchange = function () {
						$("#putfval").text(this.value);
						$("#old_image").val("");
					}

					document.getElementById('fileInputhome').onchange = function () {
						$("#putfvalhome").text(this.value);
						$("#old_image_home").val("");
					}

					document.getElementById('tutorialVideoInput').onchange = function () {
						$("#putfvaltut").text(this.value);
						$("#old_tutorial_video").val("");
					}

					$(document).ready(function(){
						$("#knowl-policy-editor").Editor();
						var license_editor = "<?php echo addslashes(@$store['knowl_base']); ?>";
						$("#knowl-policy-editor").Editor("setText", license_editor);

						$("#feature-policy-editor").Editor();
						var license_editor = "<?php echo addslashes(@$store['dash_feature_text']); ?>";
						$("#feature-policy-editor").Editor("setText", license_editor);

					})
					function get_descrip(){

						$("#put_knowl_desp").html($("#knowl-policy-editor").Editor("getText"));
						$("#put_feature_desp").html($("#feature-policy-editor").Editor("getText"));
						$("#submit_pro").click();
					}
				</script>