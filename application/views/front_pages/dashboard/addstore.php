<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left');?>
	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Create New Store</h2>
							<p class="paragraph-text-sm-grey">Enter Information About Your New Store</p>
						</div>

						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
		</div>
		<div class="col-md-12">
			<div id="showError"></div>
			<?php
			$success_msg = $this->session->flashdata('success_msg');
			$error_msg   = $this->session->flashdata('error_msg');

			if ($success_msg) {
				?>

				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $success_msg; ?>
				</div>

				<?php
			}
			if ($error_msg) {
				?>


				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $error_msg; ?>
				</div>

				<?php
			}
			?>
		</div>

		<form id="frmAddStore" method="post" action="<?php echo base_url('store/add'); ?>">
			<div class="row form">

				<div class="col-md-12">
					<div class="form-group input-effects">
						<input type="text"  name="storeName" class="validThis home-input" id="store-name"  placeholder=""/>
						<label>store name</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group input-group input-effects">
						<input type="text" name = "subdomain" class = "validThis form-control form-input border-dark-1" id="subdomain" placeholder="store url">
						<!-- <div class="input-group-append">
							<span class="input-group-text form-input input-prepend">digitalmediadeliveries.com</span>
						</div> -->
						<p style="display: none;" class="paragraph-text-xs-grey">This is the address you can give to your customers. It can include numbers, alphabets and dashes stc.
						</p>
						
					</div>
					<p id="subdomail-error" class="text-center"></p>
				</div>

				<div class="col-md-12">
					<div class="form-group input-effects">
						<input type="text"  name = "contactPerson" class = "validThis  home-input" id="contact-person"  placeholder=""/>
						<label>contact person on store</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group input-effects">
						<input type="email"  name = "supportEmail" class = "validThis  home-input" id="support-email"  placeholder=""/>
						<label>support email for store</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>
				<div class="col-md-12 mx-auto">
					<div class="nauk-info-connections text-center">
						<button class="btn-form btn" type="submit" id="create_store">create store</button>
						<!--<a class="btn-form btn" type="submit" id="create_store" ><span class="tick">create store </span></a>-->

					</div>
				</div>


			</div>
		</form>
	</div>
</div>


<script type="text/javascript">
	function subdomainCheck(){
		var error = $("#subdomail-error");
		var value = $(this).val();
		//console.log(value);

		if(value){
			$.ajax({
				type: 'post',
				url: '<?php echo base_url("store/check_subdomain") ?>',
				data: 'subdomain=' + value,
				success: function(msg){
					console.log(msg);
					error.html(msg);
				}
			});
		}else{
			error.html('');
		}
	}

	$('#subdomain').on({
		keyup: subdomainCheck,
		click: subdomainCheck,
		select: subdomainCheck
	});

	$('#subdomain').keypress(function (e) {
		var allowedChars = new RegExp("^[a-zA-Z0-9\-\b]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (allowedChars.test(str)) {
			return true;
		}
		e.preventDefault();
		return false;
	}).keyup(function() {
	    // the addition, which whill check the value after a keyup (triggered by Ctrl+V)
	    // We take the same regex as for allowedChars, but we add ^ after the first bracket : it means "all character BUT these"
	    var forbiddenChars = new RegExp("[^a-zA-Z0-9\-\b]", 'g');
	    if (forbiddenChars.test($(this).val())) {
	    	$(this).val($(this).val().replace(forbiddenChars, ''));
	    }
	});

</script>

<script type="text/javascript">
	$("#create_store").on("click", function (e) {
		e.preventDefault();
		formValidate('frmAddStore');
	});
</script>