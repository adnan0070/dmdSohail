<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left');
	$success_msg = $this->session->flashdata('success_msg');
	$error_msg = $this->session->flashdata('error_msg');

	?>


	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<?php
			if ($success_msg) {
				?>

				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>

				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
					</button>
					<?php echo str_replace("_", " ", $error_msg); ?>
				</div>
				<?php
			}

			?>
		</div>

		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">send product for download</h2>
							<p class="paragraph-text-sm-grey">Product will be sent to the person for download.</p>
						</div>
						
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
		</div>
		<form method="post" action="<?php echo base_url("product/sendProduct"); ?>">
			<div class="row clearfix form">						
				<div class="col-md-12">
					<div class="form-group input-effects">
						<input type="text"  name = "customer[f_name]" class = "home-input" id="first-name"  placeholder=""/>
						<label>first name</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div> 

				<div class="col-md-12">
					<div class="form-group input-effects">
						<input type="text"  name = "customer[l_name]" class = "home-input" id="last-name"  placeholder=""/>
						<label>Last Name</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div> 

				<div class="col-md-12">
					<div class="form-group input-effects">
						<input type="email"  name = "customer[email]" class = "home-input" id="email"  placeholder=""/>
						<label>e-mail</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
					<input type="hidden" name="product_id" value="<?php echo
					$data_id; ?>" />
				</div>  

				<div class="col-md-12">
					<div class="item form-group"> 
						<label class="control-label " for="title">Message for Customer</label>
						<textarea   id="title" name="customer[message]" rows="10" class="form-control form-textarea"><?php echo $this->input->post('customer[message]')?></textarea>
					</div>
				</div>
				

				<div class="col-md-12">
					<ul class="list-inline ">
						<li class="list-inline-item form-checkbox">
							<input type="checkbox" class="border-dark-1" name="default" checked> <span class="paragraph-dark-md-capital"> Use product email already set up for your customers</span>
						</li>

					</ul>
				</div> 


				<div class="col-md-12 text-center">

					<input class="btn-form btn" type="submit" value="send download" />


				</div> 

			</div>
		</form>


	</div>

</div>