<div class="row main-body mx-auto ">
 <?php $this->load->view('front_pages/dashboard/dash_left'); ?>

            <div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

				<div class="row">

					<div class="col-md-12">
                      <?php
$success_msg = $this->session->flashdata('success_msg');
$error_msg = $this->session->flashdata('error_msg');


if ($success_msg) {
?>
                   <div class="alert alert-success" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
                      </button>
                        <?php echo $success_msg; ?>
                    </div>
                  <?php
}
if ($error_msg) {
?>
                       <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
                      </button>
                        <?php echo $error_msg; ?>
                    </div>
                    <?php
}
?>
          </div>
          <?php
                 $store_id = $this->session->userdata('store_id');
                 $user_ID = $this->session->userdata('user_id');
                 $query = $this->db->query("select configuration,status from dmd_integrations where user_id='".$this->session->userdata('user_id')."' and name='getresponse'");
                 $configuration = array();
                 if($query->num_rows()>0){
                 $configuration = $query->row_array();
                 $status = @$configuration['status'];
                 $configuration = json_decode($configuration['configuration'],true);
                 
                 
                 }
                ?>

					<div class="col-md-12 inner-body-head"><!-- full block start-->
						<div class="nauk-info-connections">
							<div class="page-header">
								<div class="pull-left">
									<h2 class="heading-lg-green">getresponse configuration</h2>
									<p class="paragraph-text-sm-grey">Integrate for better configuration</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div><!-- block end-->

				</div><!-- inner-body-end-->

				<form action="/store/saveresponseLists" method="post">
					<div class="row clearfix form">						
						<div class="col-md-12">
							<div class="form-group">
								<input id="chimpkey" value="<?php echo @$configuration['apiKey']; ?>" type="text" oninput="change_set(this.value)" name="getresponse[apiKey]" class="form-control form-input form-input-lg border-dark-1" placeholder="api Key">
								<!--<span class="text-danger">Validation error</span>-->
							</div>
						</div> 

						<div class="col-md-12">
							<div class="form-group" id="chimp_list">
								
								<!--<span class="text-danger">Validation error</span>-->
							</div>
						</div>
                        
                        
                        <div class="col-md-12">
							<ul class="list-inline ">

								<li class="list-inline-item form-checkbox"> 
										<input type="checkbox" <?php if (@$status !="" && @$status == "on") {echo "checked";
} ?> class="border-dark-1" name="enable"/> <span class="paragraph-dark-md-capital">Enabled</span>
								</li>

								
							</ul>
						</div>
                        
                         


						<div class="form-footer">
                        <button type="submit" class="btn-form btn ">save</button>
							<!--
                            <?php if(isset($pm)){ ?>
								<input type="hidden" name="pm_id" value="<?php echo $pm['id']; ?>" />
								<button type="submit" class="btn-sm-blue btn">update</button>
							<?php }else{ ?>
								<button type="submit" class="btn-sm-blue btn ">save</button>
							<?php } ?>
                            -->
						</div>

					</div>
				</form>
			</div>
		
	</div>
    <script>
    $(document).ready(function(){
        var getval = $("#chimpkey").val();
        change_set(getval);
    });
    function change_set(tval){
        if(tval.length>=20){
           $.post('<?php echo base_url().'store/getresponseLists';?>',{"apiKey":tval,"listid":'<?php echo  @$configuration['campaignId']; ?>'},function(res){
            $("#chimp_list").html(res);
           }); 
        }
    }
    </script>