<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>




	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">
			<div id="showError"></div>
<?php //print_r($coupon); ?>

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">
								<?php 
								if(isset($coupon)){
									echo "edit coupon";
								}else{
									echo "create new coupon";
								}
								?>
							</h2>
							<p class="paragraph-text-sm-grey">Coupon price will be set in percentage</p>
						</div>
						<div class="pull-right">
							<!--<a class="btn-sm-blue btn" href="#">create new</a>-->
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
		</div><!-- inner-body-end-->

		<?php
		$action = "";
		if(isset($coupon)){
			$action = "coupon_edit";
		}else{
			$action = "coupon_create";
		}
		?>

		<form method="post" id="newCoupon" action="<?php echo base_url() . 'store/' . $action; ?> " >
			<div class="row clearfix form">
				<div class="col-md-6">
					<div class="form-group input-effects">
						<select name="status" class="home-input validThis" placeholder="">
							<?php if(isset($coupon)){
								if($coupon["status"] == "live"){
									?>
									<option value="live" class="form-input form-input-lg border-dark-1" selected>Live</option>
									<option value="expired" class="form-input form-input-lg border-dark-1">Expired</option>
									<?php
								}elseif($coupon["status"] == "expired"){
									?>
									<option value="live" class="form-input form-input-lg border-dark-1">Live</option>
									<option value="expired" class="form-input form-input-lg border-dark-1" selected>Expired</option>
									<?php
								}
							}else{
								?>
								<option value="" class="form-input form-input-lg border-dark-1" selected disabled>Status</option>
								<option value="live" class="form-input form-input-lg border-dark-1">Live</option>
								<option value="expired" class="form-input form-input-lg border-dark-1">Expired</option>
								<?php
							} ?>
						</select>
						<label>Status</label>
						<span class="focus-border"></span>
					</div>
				</div> 

				<?php
				if(isset($coupon)){
					$id = $coupon["id"];
					echo '<input type="hidden" name="id" value="' . $id . '" />';
				}
				?>

				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="text"  
						name="name"
						class="home-input validThis"
						id="name"
						placeholder=""
						value="<?php if(isset($coupon)){echo $coupon['name'];}?>"/>
						<label>name</label>
						<span class="focus-border"></span>
					</div>
				</div> 

				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="text"  
						name="code"
						class="home-input validThis"
						id="code"
						placeholder=""
						value="<?php if(isset($coupon)){echo $coupon['code'];}?>"/>
						<label>code</label>
						<span class="focus-border"></span>
					</div>
				</div> 

				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="number" 
						min="0" 
						max="100" 
						step="0.1" 
						name="discount" 
						class="home-input validThis" 
						id="coupon-discount" 
						placeholder=""
						value="<?php if(isset($coupon)){echo $coupon['discount'];}?>"/>
						<label>coupon discount</label>
						<span class="focus-border"></span>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="number" 
						min="1" 
						name="max_use" 
						class="home-input validThis" 
						id="maximum-uses" 
						placeholder="" 
						onkeydown="return false"
						value="<?php if(isset($coupon)){echo $coupon['max_use'];}?>"/>
						<label>maximum uses</label>
						<span class="focus-border"></span>
					</div>
				</div> 

				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="text" 
						name="active_date" 
						id="active_date" 
						class="home-input datepicker validThis"  
						placeholder="" 
						onkeydown="return false"
						value="<?php if(isset($coupon)){echo date('m/d/Y', strtotime($coupon['active_date']));}?>"/>
						<label>activation date</label>
						<span class="focus-border"></span>
					</div>
				</div> 

				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="text" 
						name="expire_date" 
						id="expire_date" 
						class="home-input datepicker validThis" 
						placeholder="" 
						onkeydown="return false"
						value="<?php if(isset($coupon)){echo date('m/d/Y', strtotime($coupon['expire_date']));}?>"/>	
						<label>expiration date</label>
						<span class="focus-border"></span>
					</div> 
				</div> 

				<br>

			</div>

		</form>

		<div class="form-footer">
			<div id="todateError" class="text-danger"></div>
			<button id="sbmtCoupon" class="btn-form btn">save</button>
		</div>

	</div>



</div>

<script type="text/javascript">
	$("#sbmtCoupon").on("click", function(event){
		event.preventDefault();
		//alert("meeeee");
		var from = Date.parse($("#active_date").val());
		var to = Date.parse($("#expire_date").val());
		//console.log(from + ", " + to);
		if(from > to){
			$("#expire_date").focus();
			$("#todateError").html("Expire Date must be greater than Activation Date.");
			return false;
		}
		formValidate('newCoupon');
	});
</script>

