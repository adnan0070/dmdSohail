<div class="row main-body mx-auto ">

	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<div class="col-md-12">
				<?php
				$success_msg = $this->session->flashdata('success_msg');
				$error_msg = $this->session->flashdata('error_msg');

				if ($success_msg) {
					?>
					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $success_msg; ?>
					</div>
					<?php
				}
				if ($error_msg) {
					?>

					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $error_msg;?>
					</div>
					<?php
				}
				?>
				<div id="showError"></div>
			</div>
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Update user Package</h2>
							<p class="paragraph-text-sm-grey">Select any package.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</br></br></br>
	<form method="post" id="pkg" enctype="multipart/form-data" action="<?php echo
	base_url('User/updateUserPackage'); ?>">
	<div class="row form">
		<div class="col-md-6">
			<div class="form-group input-effects">
				<select required id="time-period" name = "package" class="home-input validThis">
					<?php foreach ($packages as $package) {
						echo "<option   value='".$package['pack_code']."'>".$package['pack_title']."</option>";
					}
					?>
				</select>
				<label>Package</label>
				<span class="focus-border"></span>
				<span class="text-danger"></span>
			</div>
		</div>
		<div class="col-md-12 mx-auto">
			<div class="nauk-info-connections text-center">
				<input type="hidden" name="update" value="package" />
				<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
				<button 
				type="submit" 
				class="btn-form btn" >Save</button>
			</div>
		</div>

	</form>
</div>	
</div>	
</div>	

