<div class="row main-body mx-auto ">
	<?php 
	$this->load->view('front_pages/dashboard/admin_dash_left'); 
	//print_r($users);
	?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<?php
			if($this->session->flashdata('error_msg')){
				?>
				<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
				</div>

				<?php
			}
			if($this->session->flashdata('success_msg')){
				?>
				<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
				</div>
			<?php } ?>

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Customization request</h2>
							<p class="paragraph-text-sm-grey">Manage all your users customization packages.</p>
						</div>
						<div class="pull-right">
							<a  id ="message-btn" href="#"  class="btn-sm-default btn pricing-btn"><i class="fa fa-envelope"></i> Message</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
		</div>
		<div class="row">
			
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<!-- <div class="input-group">
					<input type="text" class="form-control" placeholder="Search by user, store name here ...." >
					<div class="input-group-append">
						<button class="btn btn-outline-secondary btn-search-list" type="button">Search</button>
					</div>
				</div> -->
				<table id="cats" class="table table-bordered table-striped" style="width:100%">
					<thead>
						<tr>
							<th></th>
							<th>Store</th>
							<th>Email</th>
							<th>Package</th>
							<!-- <th>Comments</th> -->
							<th>Status</th>
							<th>Date</th>
							<th>Action</th>

						</tr>
					</thead>
					<tbody>
						<?php 
						if ($requests) {
							foreach ($requests as $user) {
								?>
								<tr>
									<td><input class="user-check" type="checkbox" name="usersID" value="<?php echo $user['user_id']; ?>"> </td>
									<td><a target="_blank" href="<?php echo $user['url']; ?>"><?php echo $user['store']; ?></a></td>
									<td><?php echo $user['email']; ?></td>
									<td><?php echo $user['package']; ?></td>
									<td>
										<?php 
										if($user['status'] == 1){
											echo 'Updated';
										}else{
											echo 'New';
										}
										?>
									</td>
									
									<td><?php echo date('Y-m-d', strtotime($user['created_date'])); ?></td>
									
									<td>
										<a href="<?php echo base_url('Customization/requestData/'.encode($user['id'])); ?>" class="btn default-btn-grey">Data</a>
									</td>
								</tr>

								<?php
							}
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>



<div class="modal fade " id="message-user">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Enter  message</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form method="post" enctype="multipart" action="<?php echo
			base_url('customization/send_message'); ?>">
			<input type="hidden" value="" id="selected-users" name="users">
			<div class="modal-body">

				<div class="row">
					<div class="col-md-12 editor ">
						<label for="short-description-editor" class=" heading-md-grey"></label>
						<div class="form-group">
							<textarea name="message"  maxlength="5000" rows="20" class="form-control form-textarea "></textarea> 
						</div>
					</div>
				</div> 
			</div>
			<div class="modal-footer text-right">
				<!-- <a class="btn default-btn-grey">Yes</a> -->
				<input type="submit" class="default-btn-grey" value="Send Email">
			</div>
		</form>
	</div>
</div>
</div>



<script type="text/javascript">
	$(document).ready(function(){
		$("#message-btn").click(function(){
			var len = $("input[name='usersID']:checked").length;
			if(len > 0){
				var favorite = [];
				$.each($("input[name='usersID']:checked"), function(){            
					favorite.push($(this).val());
					console.log($(this).val());
				});
				console.log(favorite.length);
				$("#selected-users").val(favorite);
				$("#message-user").modal('toggle');
			}
		});
	});
</script>


<script type="text/javascript">
	$(document).ready(function(){
		$('#cats').DataTable({
			//"pagingType": "full_numbers",
			"ordering": false,
			//"searching": true,
			"bLengthChange": false,
			"bInfo": false,
	        //"dom": '<"top"i>rt<"bottom"flp><"clear">'
	    });
	});
</script>

