<div class="row main-body mx-auto ">
			<?php
$user_role = $this->session->userdata('user_role');
if ($user_role == '2') {
    $this->load->view('front_pages/dashboard/admin_dash_left');
} else {
    $this->load->view('front_pages/dashboard/dash_left');
}
?>
			<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

				<div class="row">

					<div class="col-md-12 inner-body-head"><!-- full block start-->
						<div class="nauk-info-connections">
							<div class="page-header">
								<div class="pull-left">
									<h2 class="heading-lg-green">Profile</h2>
									<p class="paragraph-text-sm-grey">Edit your profile and information here</p>
								</div>
								
								<div class="clearfix"></div>
							</div>
						</div>
					</div><!-- block end-->
				</div>
                <div class="col-md-12">
            <?php
                $success_msg = $this->session->flashdata('success_msg');
                $error_msg = $this->session->flashdata('error_msg');
                $user_name = $this->session->userdata('user_name');
                $user_name = explode(" ", $user_name);

                if ($success_msg) {
                ?>
                    <div class="alert alert-success">
                      <?php echo $success_msg; ?>
                    </div>
                <?php }
                
                if ($error_msg) {
                ?>
                    <div class="alert alert-danger">
                      <?php echo $error_msg; ?>
                    </div>
                    <?php
                    }
                ?>
            </div>  

            

				<form method="post" action="<?php echo base_url('user/edit'); ?>">
					<div class="row form">

						<div class="col-md-12">
            				<div class="form-group input-effects">
            					<input type="text" value="<?php echo $user_name[0]; ?>"  name = "firstName" class = "home-input" id="first-name" required  placeholder=""/>
                                <label>first name</label>
                                <span class="focus-border"></span>
            					<!--<span class="text-danger">Validation error</span>-->
            				</div>
            			</div> 
            			<div class="col-md-12">
            				<div class="form-group input-effects">
            					<input type="text" value="<?php echo $user_name[1]; ?>"  name = "lastName" class = "home-input" id="last-name" required  placeholder=""/>
                                <label>last name</label>
                                <span class="focus-border"></span>
            					<!--<span class="text-danger">Validation error</span>-->
            				</div>
            			</div> 
            			<div class="col-md-12">
            				<div class="form-group">
            					<input type="hidden" 
                                value="<?php echo $this->session->userdata('user_email'); ?>"  
                                name = "email" 
                                class = "form-control" id="email"   
                                placeholder=""/>
            				</div>
            			</div>

                <div class="col-md-12"><div class="form-group input-effects" id="current-password"></div></div> 
            
                <div class="col-md-12"><div class="form-group input-effects" id="password1"></div></div> 

                <div class="col-md-12"><div class="form-group input-effects" id="conf-password1"></div>
                    <span class="" id='pass_message'></span>
                </div> 

                  <div class="col-md-12 mx-auto">
                    <div class="nauk-info-connections text-center">
                      <button class = "btn-form btn" id="cPass">change password</button>
                    </div>
                  </div><br>

				<div class="col-md-12 mx-auto">
					<div class="nauk-info-connections text-center">
						<input class="btn-form btn" type="submit" id="save" value="save" name="save" >
					</div>
				</div>	


					</div>
				</form>
			</div>
		</div>
  <script>
 var check = function() {
  if (document.getElementById('password').value ==
    document.getElementById('conf-password').value) {
    document.getElementById('pass_message').style.color = 'green';
    document.getElementById('pass_message').innerHTML = 'matching';
  } else {
    document.getElementById('pass_message').style.color = 'red';
    document.getElementById('pass_message').innerHTML = 'not matching';
  }
}

  $("#cPass").on("click", function(e){
    e.preventDefault();
    
    $("#current-password").html('<input type="password" name="currentPassword" onkeyup="check();" class="home-input" placeholder="" /><label>current password</label><span class="focus-border"></span>');

    $("#password1").html('<input id="password" type="password" name = "password" onkeyup="check();" class = "home-input" placeholder="" pattern="(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" /><label>new password</label><span class="focus-border"></span>');

    $("#conf-password1").html('<input id="conf-password" type="password" name="ConfPassword" onkeyup="check();" class="home-input" placeholder="" pattern="(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" /><label>re-type new password</label><span class="focus-border"></span>');
    //var pattern = '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
    //$("#password, #conf-password, #current-password").show();

    $(this).hide();
    
  });
 </script> 