<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<?php
	if($this->session->flashdata('error_msg')){
		?>
		<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
		</div>

		<?php
	}
	if($this->session->flashdata('success_msg')){
		?>
		<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
		</div>
	<?php } ?>
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Manage Stores <?php if($user){ echo 'of ' . $user['display_name'];} ?>&nbsp; 
								<span class="paragraph-text-sm-grey">
									<?php 
									if($user){ 
										if($user['status'] == 1){ 
											echo 'Active';
										}elseif($user['status'] == 0){
											echo 'Blocked';
										}
									}
									?>	
								</span>
							</h2>
							<p class="paragraph-text-sm-grey">Manage stores.</p>

						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->

		</div>
		<div class="row">
			

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<!-- <div class="input-group">
					<input type="text" class="form-control" placeholder="Search by store, product here ...." >
					<div class="input-group-append">
						<button class="btn btn-outline-secondary btn-search-list" type="button">Search</button>
					</div>
				</div> -->
				<table id="cats" class="table table-bordered table-striped" style="width:100%">
					<thead>
						<tr>
							<th>Name</th>
							<th>Store Since</th>
							<th>Store Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if($stores){
							foreach ($stores as $key => $store) {
								?>
									<tr>
										<td><a target="_blank" href="<?php echo 'http://' . $store['store_url']; ?>"><?php echo $store['store_name']; ?></a></td>
										<td><?php echo date('Y-m-d', strtotime($store['store_created'])); ?></td>
										<td>
											<?php 
												if($store['status'] == 1){
													echo 'Active';
												}elseif($store['status'] == 0){
													echo 'Blocked';
												}
												?>
										</td>
										<td>
											<?php 
											if(isset($user)){ 
												if($user['status'] == 1){
													?>
													<a data-status="<?php echo $store['status']; ?>" 
														data-store="<?php echo $store['id']; ?>"
														data-target="#block-store" 
														class="btn default-btn-grey blkStore">
														<?php 
														if($store['status'] == 0){
															echo 'Active';
														}elseif($store['status'] == 1){
															echo 'Block';
														}
														?>
													</a>
													<?php
												}
											}
											?>
											<!-- <a data-toggle="modal" data-target="#delete-store" class="btn default-btn-grey"> Delete</a> -->
											<a data-target="#delete-store" 
												data-store="<?php echo $store['id']; ?>" 
												class="btn default-btn-grey delStore"> Delete</a>
											<a href="<?php echo site_url('admin/products/') . $store['id']; ?>" class="btn default-btn-grey">Products</a>
										</td>
									</tr>
								<?php
							}
						}
						?>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade delete" id="delete-store">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Delete Store</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				Are you sure you want to delete this store ?
				<input type="hidden" id="storeID" name="storeID" value="">
			</div>

			<!-- Modal footer -->
			<div class="modal-footer text-right">
				<a class="btn default-btn-grey" id="deleteStore">Yes</a>
				<a class="btn default-btn-grey" data-dismiss="modal">No</a>
			</div>

		</div>
	</div>
</div>

<div class="modal fade block" id="block-store">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Store Management</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<p id="blkTxt"></p>
				<input type="hidden" name="blkStoreID" id="blkStoreID" value="">
				<input type="hidden" name="blkStatus" id="blkStatus" value="">
			</div>

			<!-- Modal footer -->
			<div class="modal-footer text-right">
				<a class="btn default-btn-grey" id="blockStore">Yes</a>
				<a class="btn default-btn-grey" data-dismiss="modal">No</a>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#cats').DataTable({
			//"pagingType": "full_numbers",
			"ordering": false,
			//"searching": false,
			"bLengthChange": false,
	        "bInfo": false,
	        //"dom": '<"top"i>rt<"bottom"flp><"clear">'
	    });
	});
</script>

<script type="text/javascript">
	$('.delStore').on('click', function(event){
		event.preventDefault();
		var storeID = $(this).data('store');
		$('.modal-body #storeID').val(storeID);
		$('.delete').modal('show');
		//console.log(storeID);
	});
	$('#deleteStore').on('click', function(event){
		event.preventDefault();
		var storeID = $('#storeID').val();
		//console.log(storeID);
		window.location.replace('<?php echo base_url('admin/delete_store/'); ?>' + storeID);
	});

	$('.blkStore').on('click', function(event){
		event.preventDefault();
		var storeID = $(this).data('store');
		var storeStatus = $(this).data('status');
		
		if(storeStatus == 1){
			$('#blkTxt').text('Are you sure to block this store ?');
		}else if(storeStatus == 0){
			$('#blkTxt').text('Are you sure to active this store ?');
		}

		$('.modal-body #storeID').val(storeID);
		$('.modal-body #blkStatus').val(storeStatus);

		$('.block').modal('show');
		//console.log(storeID);
		//console.log(storeStatus);
	});

	$('#blockStore').on('click', function(event){
		event.preventDefault();

		var storeID = $('#storeID').val();
		var storeStatus = $('#blkStatus').val();
		//console.log(storeID);
		//console.log(storeStatus);
		if(storeStatus == 1){
			window.location.replace('<?php echo base_url('admin/block_store/'); ?>' + storeID);
		}else if(storeStatus == 0){
			window.location.replace('<?php echo base_url('admin/active_store/'); ?>' + storeID);
		}
	});
</script>