<?php
$user_role = $this->session->userdata('user_role');
if($user_role != '2'){
    redirect(base_url());
}
?>

<div class="col-md-3 left-menu"><!-- left-menu-start-->
    <div>
        <br> <br>
        <ul>
            <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('admin/users'); ?>">Users</a></li>
            <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('admin/setting'); ?>">settings</a></li>
            <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('package'); ?>">Packages</a></li>
            <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('pages/page_list'); ?>">Pages</a></li>
            <!-- <li  class="menu-item hvr-bounce-to-right"><a href="<?php //echo base_url('pages/add_new_pages'); ?>">Add Page</a></li> -->
            <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('admin/teams'); ?>">Team Management</a></li>
            <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('/reports'); ?>">Reports</a></li>
            <!--<li  class="menu-item hvr-bounce-to-right"><a href="<?php //echo base_url('admin/blog'); ?>">Add Blog</a></li>
                <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('admin/blog'); ?>">Blog Management</a></li>-->
                <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('admin/categories'); ?>">Categories</a></li>
                <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('admin/articles'); ?>">Articles</a></li>
                <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('admin/payment_method'); ?>">Payment Methods</a></li>
                <!-- <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('admin/history_for_admin'); ?>">User Payments</a></li> -->
                <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('Customization/store_customization_packages'); ?>">Customization Packages</a></li>
                <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('Customization/requests'); ?>">Customization Request</a></li>
                <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('resources'); ?>">Resources</a></li>
                <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('admin/metaTags'); ?>">Manage Meta Tags</a></li>
                <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('admin/tracking'); ?>">Manage Tracking</a></li>
                <li  class="menu-item hvr-bounce-to-right"><a href="<?php echo base_url('tutorial'); ?>">Tutorials</a></li>

            </ul>
        </div>
</div><!-- left-menu-end-->