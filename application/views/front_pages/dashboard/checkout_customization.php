<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<?php
			$success_msg = $this->session->flashdata('success_msg');
			$error_msg = $this->session->flashdata('error_msg');


			if ($success_msg) {
				?>
				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
					</button>
					<?php echo $error_msg; ?>
				</div>
				<?php
			}


			$action = "savecheckout";
			$subject = "";
			$upload_file = "";
			$fid  = "";
			$desp = "";
			if (isset($data['id'])) {

				$action = "updatecheckout";
				$display = $data["display"];
				$upload_file = $data["image"];
			}

			?>
		</div>
		<div class="row">

			<div class="col-sm-8 col-md-8 col-lg-8 col-xl-8 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">checkout customization</h2>
							<p class="paragraph-text-sm-grey">Choose your image to represent your storefront.This image will be displayed on cart,checkouts and delivery pages.This image must be a JPG and PNG.</p>
							<p class="paragraph-text-sm-grey">For better look and feel, logo should be in equal height and width, also use minimum dimension of (500 x 500)</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-right">
							<a  href="<?php echo base_url('customization/packages'); ?>" class="btn default-btn-green">Get a request</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

		</div>


		<form method="post" enctype="multipart/form-data" action="<?php echo
		base_url('product/' . $action); ?>">
		<div class="row clearfix form">	

			<input type="hidden" name="old_image_path" id="old_image_path" value="<?php echo $upload_file; ?>" />
			<input type="hidden" name="old_file" id="old_file" value="<?php echo $upload_file; ?>" />

			<div class="col-md-12 text-center">

				<div id="imagePreview" style="background-image: url('<?php echo base_url() .
				"/site_assets/stores/" . @$upload_file; ?>');background-repeat: no-repeat;background-size: cover;" class="nauk-info-connections block-file-upload block-border-dotted text-center">

				<h3 class="heading-sm-grey text-center" style="display: none;">UPLOAD FILE</h3>
				<div class="form-group">

					<input style="display: none;" id="imfile" type="file" value="" name="userfile" class="file-input-product" onchange="preview_image(this)" placeholder="Select from computer">

				</div>
				<div class="form-group" style="margin-bottom:0px;">
					<input type="button" onclick="upload_img()" value="Upload Image" class="btn-sm-blue btn" />
				</div>

				<div class="form-group" style="display: none;">

					<input type="checkbox" class="border-dark-1" name="enable"> 
					<span class="paragraph-sm-grey">You can also drag and drop files.</span>

				</div>
				<!--<span class="text-danger">Validation error</span>-->
			</div> 
		</div> 





		<!--
		<div class="col-md-12">
			<div class="form-group">

				<label>
					<input <?php if (@$display !=
						"" && @$display == "on") {
						echo "checked";
					} ?>  type="checkbox" class="border-dark-1" name="display"> <span class="paragraph-dark-md-capital">Display logo on payment page</span>
				</label>

			</div>
		</div>
	-->


	<div class="form-footer">
		<input type="submit" class="btn-form btn" value="save"/>
	</div>


</div>

</form>


</div>

</div>
<script>
	function upload_img(){
		$('#imfile').click();
	}
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#imagePreview').css('background-image', 'url('+e.target.result +')');
				$('#imagePreview').css('background-repeat', 'no-repeat');
				$('#imagePreview').css('background-size', 'cover');
                    //$('#imagePreview').attr('src', e.target.result);
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
                $("#old_file").val("");
            }
        }

        function preview_image(eventt){
        	readURL(eventt);
        }
    </script>