
<div id="loading">
	<img id="loading-image" src="<?php echo base_url(); ?>site_assets/images/loader-image.gif" alt="Loading..." />
</div>
<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>



	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<?php
			$success_msg = $this->session->flashdata('success_msg');
			$error_msg = $this->session->flashdata('error_msg');


			if ($success_msg) {
				?>
				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $error_msg = str_replace("_"," ",$error_msg); ; ?>
				</div>
				<?php
			}


			$action = "savefile";
			$button_label = "";
			$upload_file = "";
			$fid = "";
			if (isset($file_id)) {

				$action = "updatefile";
				$fid = $file_id;
				$button_label = $file[0]["button_label"];

				$upload_file = $file[0]["upload_file"];
			}

			?>
		</div>



		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">add file for delivery</h2>
							<p class="paragraph-text-sm-grey">Add a new file for your product to the customers.</p>
							<p class="paragraph-text-sm-grey">Allowed File Types.<br><span style="color: lightgrey; word-break: break-word;">gif|jpg|png|jpeg|txt|pdf|mp4|mp3|ogg|doc|docx|xls|xlsx|ppt|pptx|csv|wav|wmv|avi|mov|mpg|rar|zip|xml|tif|tiff|html|js|ods</span></p>
						</div>

						<div style="" class="pull-right">
							<h4  class="heading-md-grey"><?php echo @$check_memory; ?></h4>
							<p class="heading-sm-grey">space available</p>
						</div>
					</div>
				</div>
			</div>

		</div>


		<form method="post" enctype="multipart/form-data" onsubmit = "awaiting()" action="<?php echo base_url('product/' . $action); ?>">
			<div class="row clearfix form">	


				<div class="col-md-12">
					<div class="form-group input-effects">
						<input type="text" value="<?php echo $button_label; ?>"  name = "product[button_label]" class = "home-input" id="button_label"  placeholder=""/>
						<input type="hidden" name="product[product_id]" value="<?php echo $id; ?>" />
						<input type="hidden" name="file_id" value="<?php echo $fid; ?>" />
						<input type="hidden" name="old_image_path" id="old_image_path" value="<?php echo $upload_file; ?>" />
						<input type="hidden" name="old_file" id="old_file" value="<?php echo $upload_file; ?>" />
						<label>Download button label</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div> 

				<div class="col-md-12 text-center">

					<div class="nauk-info-connections block-file-upload block-border-dotted text-center">

						<h3 class="heading-sm-grey text-center">UPLOAD FILE</h3>
						<div class="form-group">

							<input name="userfile"  type="file" id="fileInput" class="file-input"  placeholder="Select from computer">
							<!--<img id="output_image"/>-->
							<img class="btn-upload-file" src="<?php echo base_url(); ?>site_assets/images/btn-upload-file.png">
						</div>
						<div style="" class="form-group">
							<span class="paragraph-sm-grey" id="putfval"><?php echo $upload_file; ?></span>

						</div>

						<div style="display: none;" class="form-group">

							<input type="checkbox" class="border-dark-1" name="enable"> 
							<span class="paragraph-sm-grey">You can also drag and drop files.</span>

						</div>
						<!--<span class="text-danger">Validation error</span>-->
					</div> 
				</div> 




				<div class="form-footer">
					<input value="Save"  type="submit" class="btn-sm-blue btn" />
				</div>


			</div>

		</form>



	</div>
</div>
<script>
	function awaiting(){
		window.setTimeout(function(){
			$("#loading").css("display","block");   
		}, 100);
		
		return true;
	}
	document.getElementById('fileInput').onchange = function () {
		$("#putfval").text(this.value);
		$("#old_file").val("");
          //alert('Selected file: ' + this.value);
      };
  </script>