			<?php $store = $this->db->query('select * from settings');
			$store = $store->row_array(); 

			$sales = $this->db->query('SELECT
				DATE(`dmd_orders`.`created_date`) AS `date`,
				COUNT(`dmd_orders`.`id`) AS `count`
				FROM `dmd_orders`
				WHERE `dmd_orders`.`created_date` BETWEEN CURDATE()-INTERVAL 1 WEEK AND CURDATE()+1 and dmd_orders.user_id="'.$this->session->userdata('user_id').'"
				and dmd_orders.store_id="'.$this->session->userdata('store_id').'" GROUP BY `date`
				ORDER BY `date`');

			$sales = $sales->result_array();
			$dates_array = array();
			$start  = date('d', strtotime('-7 days'));
			$till   = date('d');
			for($i=$start;$start<=$till;$start++){
				$get_date = date('Y-m'); 
				array_push($dates_array,date('Y-m-d',strtotime($get_date.'-'.$start))); 
			}
			function getLastNDays($days, $format = 'Y-m-d'){
				$m = date("m"); $de= date("d"); $y= date("Y");
				$dateArray = array();
				for($i=0; $i<=$days-1; $i++){
					$dateArray[] = '' . date($format, mktime(0,0,0,$m,($de-$i),$y)) . ''; 
				}
				return array_reverse($dateArray);
			}
			$dates_array = getLastNDays(7);
			// print_r($dates_array);
			// die;
			
			$use_array = "";
			foreach($sales as $sale){
  //$use_array[$sale['date']] = $sale['count']; 
  //[[1450569600,0],[1450656000,0],[1450742400,34],[1450828800,75],[1450915200,59],[1451001600,120]]
				if (in_array($sale['date'], $dates_array)){
					$sale['date'] = date('m-d',strtotime($sale['date']));
					$use_array .= '["'.$sale['date'].'",'.$sale['count'].']'.',';
					$key = array_search($sale['date'], $dates_array);
					unset($dates_array[$key]);
				}
				
			}
			foreach($dates_array as $nodate){
				$count = 0; 
				$nodate = date('m-d',strtotime($nodate));
				$use_array .= '["'.$nodate.'",'.$count.']'.',';  
			}
            //print_r($use_array);
			

			function js_str($s)
			{
				return '"' . addcslashes($s, "\0..\37\"\\") . '"';
			}

			function js_array($array)
			{
				$temp = array_map('js_str', $array);
				return '[' . implode(',', $temp) . ']';
			}


			?>
			<div class="row main-body mx-auto ">
				<?php $this->load->view('front_pages/dashboard/dash_left');?>


				<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

					<div class="row">


						<div class="col-md-12 inner-body-head"><!-- full block start-->
							<div class="nauk-info-connections">
								<div class="page-header">


									<?php
									$success_msg = $this->session->flashdata('success_msg');
									$error_msg   = $this->session->flashdata('error_msg');

									if ($success_msg) {
										?>

										<div class="alert alert-success" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
											</button>
											<?php echo $success_msg; ?>
										</div>

										<?php
									}
									if ($error_msg) {
										?>

										<div class="alert alert-danger" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
											</button>
											<?php echo $error_msg; ?>
										</div>

										<?php
									}
									?>
									<div class="pull-left">

										<h2 class="heading-lg-green">Dashboard</h2>
										<p class="paragraph-text-sm-grey">Your store/product performance overview</p>
									</div>
									<?php 
									$store_id_check = $this->session->userdata('store_id');
									if($store_id_check){ ?>
										<div class="pull-right">
											<a class="btn-sm-blue btn" href="<?php echo  base_url('/product/add') ?>">New Product</a>
										</div>
									<?php } ?>
									<div class="clearfix"></div>
								</div>
							</div>
						</div><!-- block end-->

						<div class="col-md-6 inner-body-block "><!-- full block start-->
							<div class="nauk-info-connections">
								<h3 class="heading-md-blue">Sales and Transactions</h3>
							</div>
							<div class="nauk-info-connections border-dark-1" style="padding:10px">
								<div class="img-responsive" id="chart"></div>
								<!--<div class=" graph" id="bars"> </div>-->
								<a class="btn-sm-blue btn" href="<?php echo  base_url('/reports'); ?>">View reports</a>
							</div>

						</div><!-- block end-->



						<div class="col-md-6 inner-body-block"><!-- block start-->
							<div class="nauk-info-connections">
								<h3 class="heading-md-blue">My Overview</h3>
							</div>
							<div class="nauk-info-connections  border-dark-1" style="padding:10px;">
								<div class="page-header">
									<div class="pull-left">
										<p class="paragraph-text-sm-grey">Product</p>
									</div>
									<div class="pull-right">
										<p class="paragraph-text-sm-grey"><?php echo $products; ?>
										<?php if ($pack_product == '' || $pack_product <= 0){ 
											echo  $pack_product;
										} else { ?>
											/
											<?php echo $pack_product; }?>
										</p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div style="" class="page-header">
									<div class="pull-left">
										<p class="paragraph-text-sm-grey">Storage Used</p>
									</div>
									<div class="pull-right">
										<p class="paragraph-text-sm-grey"><?php echo @$check_memory; ?></p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="page-header">
									<div class="pull-left">
										<p class="paragraph-text-sm-grey">Sub Admins</p>
									</div>
									<div class="pull-right">
										<p class="paragraph-text-sm-grey">

											<?php if($this->session->userdata('pack_sub_admin') > 0){
												echo $this->session->userdata('count_sub_admin').'/'.$this->session->userdata('pack_sub_admin');
											}else{
												echo $sub_admins; 
											}
											?>
										</p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="page-header">
									<div class="pull-left">
										<h4 class="paragraph-text-sm-grey grey-sm-bold">Sales Overview</h4>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="page-header">
									<div class="pull-left">
										<p class="paragraph-text-sm-grey">Today Sales</p>
									</div>
									<div class="pull-right">
										<p class="paragraph-text-sm-grey"><?php echo $gettoday; ?></p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="page-header">
									<div class="pull-left">
										<p class="paragraph-text-sm-grey">Month to date</p>
									</div>
									<div class="pull-right">
										<p class="paragraph-text-sm-grey"><?php echo $getmonth; ?></p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="page-header">
									<div class="pull-left">
										<p class="paragraph-text-sm-grey">Lifetime sales</p>
									</div>
									<div class="pull-right">
										<p class="paragraph-text-sm-grey"><?php echo $getotal; ?></p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="page-header">
									<div class="pull-left">
										<p class="paragraph-text-sm-grey">Overall earnings</p>
									</div>
									<div class="pull-right">
										<p class="paragraph-text-sm-grey">
											<?php if($earning){ echo '$' . number_format($earning); }else{echo '0';} ?>
										</p>
									</div>
									<div class="clearfix"></div>
								</div>
								
							</div>
						</div><!-- block end-->
						<div class="col-md-12 mx-auto inner-body-block"><!-- full block start-->
							<div class="nauk-info-connections text-center">
								
							</div>
						</div>

						<div class="col-md-6 inner-body-block"><!-- block start-->
							<div class="nauk-info-connections">
								<h3 class="heading-md-blue">What can you do next?</h3>
								
							</div>
							<div class="nauk-info-connections border-dark-1" style="padding:10px;">

								<!-- <video class="img-responsive" src="<?php echo base_url(); ?>site_assets/video/<?php echo $store['userfile']; ?>" controls  controlsList="nodownload" width="100%"> </video> -->

								<p class="paragraph-text-sm-grey">Create your first product on Digital Media Deliveries</p>
								<a class="btn-sm-blue btn" href="<?php echo base_url("tutorial"); ?>">View Tutorial Videos</a>

							</div>
						</div><!-- block end-->

						<div class="col-md-6 inner-body-block"><!-- block start-->
							<div class="nauk-info-connections">
								<h3 class="heading-md-blue">new features</h3>
							</div>
							<div class="nauk-info-connections border-dark-1" style="padding:10px;">
								<p class="paragraph-text-sm-grey"> 
									<?php echo $store['dash_feature_text']; ?>
								</p>
								
								<a class="btn-sm-blue btn" href="<?php echo base_url("user/new_updates"); ?>">View All</a>
							</div>
							
						</div><!-- block end-->

					</div>

				</div><!-- inner-body-end-->
			</div>
			<!-- <script src="<?php //echo base_url(); ?>site_assets/js/barchart.jquery.js"></script> -->
			<script>
				/*$('#chart').barChart({

					height : 200,
					bars : [
					{ 
						name : 'Dataset 1',
						values:[<?php echo $use_array; ?>]
					//values : [[1450569600,0],[1450656000,0],[1450742400,34],[1450828800,75],[1450915200,59],[1451001600,120]] 
				}
				
				]
			});*/
		</script>

		<script type="text/javascript">
			$(document).ready(function(){
				$.ajax({
					type: "post",
					url: "/reports/filter",
					data: {product: 'all', period: '7'},
					success: function(msg){
      //console.log(msg);
      if(msg){
      	buildChart(msg);
      }else{
          //console.log('no data');
          $("#chart").html('<p class="text-info ">No sales has been found in last 7 days.</p>');
      }
      }//end success function
    });//end ajax
			});

			function buildChart(ChartData){
				var data = JSON.parse(ChartData);
  //$('#chart').highcharts({
  	var chart = new Highcharts.Chart({
  		chart: {
  			renderTo: 'chart',
  			height: 204,
  			type: 'column'
  		},

  		title: {
  			text: ''
  		},

  		xAxis: {
  			categories: data.xAxis
          /*tickInterval: 1,
          labels: {
              enabled: true,
              formatter: function() { return seriesData[this.value][0];},
          }*/
      },

      yAxis: {
      	title: false
      },

      series: [{
      	name: 'Sales',
      	data: data.yAxis     
      }],

      plotOptions: {
      	line: {
      		dataLabels: {
      			enabled: true
      		},
      		enableMouseTracking: true
      	}
      },

      legend: {
      	enabled: false
      },

      navigation: {
      	buttonOptions: {
      		enabled: false
      	}
      },

      responsive: {
      	rules: [{
      		condition: {
      			maxWidth: 500
      		},
      		chartOptions: {
      			legend: {
      				layout: 'horizontal',
      				align: 'center',
      				verticalAlign: 'bottom'
      			}
      		}
      	}]
      }


    });//end chart
  	$(".highcharts-credits").hide();
  }
</script>
<script>
	$(document).ready(function(){
		var affiliate = "<?php echo $affiliate ;?>";
		if(affiliate == 1){
			var trans_id = '<?php echo $last_purchase['trans_id'] ;?>';
			var amount = '<?php echo $last_purchase['amount'];?>';
			

			tap('conversion', trans_id, amount);

		}
	});
</script> 

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


