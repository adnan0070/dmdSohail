<div class="row main-body mx-auto ">
	<?php 

	$this->load->view('front_pages/dashboard/dash_left');
	
	$products = $products[0];
	$checkoutComplete = $checkoutComplete[0];

	//embedded code
	$store_url = $this->session->userdata('store_url');
	$embedded_url = 'https://' . $store_url . '/home/showProduct/' . $products['product_code'];
	$embedded_btn = '<a href="'.$embedded_url.'" target="_blank"><button class="btn btn-success">Buy Now</button></a>';
	$webinar_url = 'https://' . $store_url . '/home/showProduct/' . $products['product_code'].'/webinar';
	
	?>


	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green"><?php echo stripslashes($products['product_name']);  ?>-Detail</h2>
							<p class="paragraph-text-sm-grey">Product ID:<?php echo $products['product_code'];  ?> | Status: <?php echo $products['product_visibility'];  ?></p>
						</div>

						<div class="clearfix"></div>
					</div>
				</div>
				<ul class="list-inline">
					<li class="list-inline-item item-link"><a style="color:#fff; border:1px solid #458fb4; text-align:center;" class="btn-sm-blue btn" href="<?php echo base_url('product/sendToDownload/'.$products['product_code']); ?>">Send Downloads</a></li>
					<li class="list-inline-item item-link"><a class="btn-sm-blue btn"  href="<?php echo base_url('product/purchase/'.$products['product_code']); ?>">Purchases</a></li>
					<li class="list-inline-item item-link"><button id="showEmbedded" class="btn-sm-blue btn">Embedded Code</button></li>
					<?php if($products['webinar_video'] != '' && $products['is_webinar'] == '1'): ?>
						<li class="list-inline-item item-link"><button id="showWebinar" class="btn-sm-blue btn">Webinar Link</button></li>
					<?php endif; ?>
					<li class="list-inline-item menu-item dropdown  ">
						<a style="padding:5px 40px; color:#fff; border:1px solid #458fb4; text-align:center;" class="dropdown-toggle btn-sm-blue btn" data-toggle="dropdown">Edit &nbsp;&nbsp;&nbsp;<b class="caret"></b></a>
						<ul class="dropdown-menu border-dark-1">
							<li class="menu-dropdown-item"><a href="<?php echo base_url('product/'.$products['product_code']); ?>">Edit Product</a></li>
							<li class="menu-dropdown-item"><a href="javascript:void(0)" onclick="create_duplicate('<?php echo $products['product_code']; ?>')">Duplicate Product</a></li>
							<li class="menu-dropdown-item"><a href="<?php echo base_url('product/archived/'.$products['product_code']); ?>">Archive Product</a></li>
							<li class="menu-dropdown-item"><a href="<?php echo base_url('product/delete/'.$products['product_code']); ?>">Delete Product</a></li>
						</ul>
					</li>
				</ul>
				<textarea id="embedded_code" style="width:100%; padding:15px;margin-bottom:5px; color:#aaa; border-radius:5px;"><?php echo $embedded_btn; ?></textarea>
				<textarea id="webinar_link"  style="width:100%; padding:15px;margin-bottom:5px; color:#aaa; border-radius:5px;"><?php echo $webinar_url; ?></textarea>
			</div><!-- block end-->
			
			<div class="col-md-6 sub-block "><!-- block start-->
				<div class="nauk-info-connections">
					<h3 class="heading-md-blue">Statistics</h3>
				</div>
				<div class="nauk-info-connections  border-dark-1" style="padding:10px;">
					<table class="table block-table">
						<tr>
							<td> <p class="paragraph-text-sm-grey">Begin Checkout</p></td>
							<td> <p class="paragraph-text-sm-grey">
								<?php if(isset($checkoutBegin)){echo $checkoutBegin; } ?>
							</p></td>
						</tr>
						<tr>
							<td> <p class="paragraph-text-sm-grey">Checkout Completes</p></td>
							<td> <p class="paragraph-text-sm-grey">
								<?php if(isset($checkoutComplete["checkout_qty"])){echo $checkoutComplete["checkout_qty"]; } ?>
							</p>
						</td>
					</tr>
					<tr>
						<td> <p class="paragraph-text-sm-grey">Lifetime Revenue</p></td>
						<td> <p class="paragraph-text-sm-grey">
							<?php if(isset($checkoutComplete["checkout_price"])){echo "$" . $checkoutComplete["checkout_price"]; } ?>
						</p>
					</td>
				</tr>

			</table>

		</div>
	</div>

	<div class="col-md-6 block sub-block"><!-- block start-->
		<div class="nauk-info-connections">
			<h3 class="heading-md-blue">Purchase Actions</h3>
		</div>
		<div class="nauk-info-connections  border-dark-1" style="padding:10px;">
			<div class="page-header">
				<div class="pull-left">
					<p class="paragraph-text-sm-grey">&#10158; Delivery File(s).</p>
				</div>

				<div class="clearfix"></div>
			</div>
			<div class="page-header">
				<div class="pull-left">
					<p class="paragraph-text-sm-grey">&#10158; Email Setup for Product.</p>
				</div>

				<div class="clearfix"></div>
			</div>
			<div class="footer-action">
				<br>
				<a style=" color:#fff; border:1px solid #458fb4; text-align:center;"  class="btn-sm-blue btn" href="<?php echo base_url('product/PurchaseAction/'.$products['product_code']); ?>">Add/Edit Actions</a>
			</div>

		</div>


	</div>
</div>
</div><!-- block end-->
</div>
<script>
	$(document).ready(function(){
		$("#embedded_code").hide();
		$("#webinar_link").hide();
		$("#showEmbedded").on("click", function(){
			$("#embedded_code").toggle();
			$("#webinar_link").hide();
		});
		$("#showWebinar").on("click", function(){
			$("#webinar_link").toggle();
			$("#embedded_code").hide();
		});
	});

	function create_duplicate(id){

		var r = confirm("Are you sure want to duplicate product");
		if (r == true) {
			window.location.href = '<?php echo base_url()."product/duplicate/"; ?>'+id; 
		} 
	}
</script>