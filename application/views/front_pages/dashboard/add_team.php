<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<div id="showError"></div>
			<?php

			$message = $this->session->flashdata('message');
			if ($message) { ?>
				<div class="alert alert-info" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<?php echo $message; ?>
				</div>
			<?php } ?>

			<?php
			if($this->session->flashdata('teams') !== ''){
				$teams = $this->session->flashdata('teams');
			}

			$action = "save_team";
			$name = "";
			$designation = "";
			$description = "";
			$upload_file = "";

			if (isset($teams)) {
				//$teams = $teams[0];
				$action = "save_team";
				$name = $teams["name"];
				$designation = $teams["designation"];
				$description = $teams["description"];
				$upload_file = @$teams["image"];
			}

			if (isset($teams["id"])) {
				$teams = $teams[0];
				$action = "update_team/" . $teams["id"];
				$name = $teams["name"];
				$designation = $teams["designation"];
				$description = $teams["description"];
				$upload_file = $teams["image"];
			}

			?>
		</div>



		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Team Member</h2>
							<p class="paragraph-text-sm-grey">Add a new / edit team member</p>
						</div>
					</div>
				</div>
			</div>

		</div>


		<form method="post" id="frmteam" enctype="multipart/form-data" action="<?php echo base_url('admin/' . $action); ?>">
			<div class="row clearfix form">	

				<div class="col-md-12">
					<div class="form-group">
						<input type="text" 
						name="name" 
						id="name"  
						maxlength="50"
						class="form-control form-input border-dark-1 validThis" 
						placeholder="Name"
						value="<?php echo $name; ?>"/>

						<input type="text" 
						name="designation" 
						id="designation"  
						maxlength="100"
						class="form-control form-input border-dark-1 validThis" 
						placeholder="Designation"
						value="<?php echo $designation; ?>"/>

						<input type="text" 
						name="description" 
						id="description" 
						maxlength="200" 
						class="form-control form-input border-dark-1 validThis" 
						placeholder="Description"
						value="<?php echo $description; ?>"/>

					</div>
				</div>

				<div class="col-md-12 text-center">

					<div class="nauk-info-connections block-file-upload block-border-dotted text-center">

						<h3 class="heading-sm-grey text-center">UPLOAD FILE</h3>
						<div class="form-group">

							<input name="userfile" 
							type="file" 
							id="fileInput" 
							class="file-input validThis" 
							placeholder="Select from computer" 
							value="<?php echo $upload_file; ?>"/>

							<!--<img id="output_image"/>-->
							<img class="btn-upload-file" src="<?php echo base_url(); ?>site_assets/images/btn-upload-file.png">
						</div>
						<div style="" class="form-group">
							<span class="paragraph-sm-grey" id="putfval"><?php echo $upload_file; ?></span>

						</div>

						<div style="display: none;" class="form-group">

							<input type="checkbox" class="border-dark-1" name="enable"> 
							<span class="paragraph-sm-grey">You can also drag and drop files.</span>

						</div>
						<!--<span class="text-danger">Validation error</span>-->
					</div> 
				</div>




				<div class="form-footer">
					<input value="Save" id="btnsbt"  type="submit" class="default-btn-green btn" />
				</div>


			</div>

		</form>



	</div>
</div>
<script>
	document.getElementById('fileInput').onchange = function () {
		$("#putfval").text(this.value);
    //alert('Selected file: ' + this.value);
};

$("#btnsbt").on("click", function(e){
	e.preventDefault();
	formValidate("frmteam");
});
</script>