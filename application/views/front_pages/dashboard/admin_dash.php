<?php 

$store = $this->db->query('select * from settings');
$store = $store->row_array(); 

function getLastNDays($days, $format = 'Y-m-d'){
	$m = date("m"); $de= date("d"); $y= date("Y");
	$dateArray = array();
	for($i=0; $i<=$days-1; $i++){
		$dateArray[] = '' . date($format, mktime(0,0,0,$m,($de-$i),$y)) . ''; 
	}
	return array_reverse($dateArray);
}

$sales = $this->db->query('SELECT
	DATE(`billing_history`.`created_date`) AS `date`,
	COUNT(`billing_history`.`id`) AS `count`
	FROM `billing_history`
	WHERE `billing_history`.`created_date` BETWEEN CURDATE()-INTERVAL 1 WEEK AND CURDATE()+1
	GROUP BY `date`
	ORDER BY `date`');

$sales = $sales->result_array();
$dates_array = array();
$start  = date('d', strtotime('-7 days'));
$till   = date('d');
if($till=='01'){
	$till = 31;
}

for($i=$start;$start<=$till;$start++){
	$get_date = date('Y-m'); 
	array_push($dates_array,date('Y-m-d',strtotime($get_date.'-'.$start))); 
}
//print_r($dates_array);
$dates_array = getLastNDays(7);

$use_array = "";
foreach($sales as $sale){

	if (in_array($sale['date'], $dates_array)){
		$sale['date'] = date('m-d',strtotime($sale['date']));    
		$use_array .= '["'.$sale['date'].'",'.$sale['count'].']'.',';
		$key = array_search($sale['date'], $dates_array);
		unset($dates_array[$key]); 
	}

}

foreach($dates_array as $nodate){
	$count = 0; 
	$nodate = date('m-d',strtotime($nodate));
	$use_array .= '["'.$nodate.'",'.$count.']'.',';  
}
//print_r($use_array);


function js_str($s) { return '"' . addcslashes($s, "\0..\37\"\\") . '"'; }

function js_array($array){
	$temp = array_map('js_str', $array);
	return '[' . implode(',', $temp) . ']';
}

//today sale
$this->db->select('COUNT(amount) AS today_sale');
$this->db->from('billing_history');
$this->db->where('DATE(created_date) = CURRENT_DATE()');
$query = $this->db->get();
//echo $this->db->last_query();
$today_sale = $query->row_array();
//print_r($today_sale);

//month to date sale
$this->db->select('COUNT(amount) AS month_sale');
$this->db->from('billing_history');
$this->db->where('MONTH(created_date) = MONTH(CURRENT_DATE())');
$this->db->where('YEAR(created_date) = YEAR(CURRENT_DATE())');
$query = $this->db->get();
//echo $this->db->last_query();
$month_sale = $query->row_array();
//print_r($month_sale);

//year to date sale
$this->db->select('COUNT(amount) AS year_sale');
$this->db->from('billing_history');
$this->db->where('YEAR(created_date) = YEAR(CURRENT_DATE())');
$query = $this->db->get();
//echo $this->db->last_query();
$year_sale = $query->row_array();

//life time sale
$this->db->select('COUNT(amount) AS life_sale');
$this->db->from('billing_history');
$query = $this->db->get();
//echo $this->db->last_query();
$lifetime_sale = $query->row_array();

//overall sale
$this->db->select('SUM(amount) AS overall');
$this->db->from('billing_history');
$query = $this->db->get();
//echo $this->db->last_query();
$overall_sale = $query->row_array();

$adminStats = getStatistic();

?>
<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left');?>


	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">


			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">


						<?php
						$success_msg = $this->session->flashdata('success_msg');
						$error_msg   = $this->session->flashdata('error_msg');

						if ($success_msg) {
							?>

							<div class="alert alert-success" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<?php echo $success_msg; ?>
							</div>

							<?php
						}
						if ($error_msg) {
							?>

							<div class="alert alert-danger" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
								</button>
								<?php echo $error_msg; ?>
							</div>

							<?php
						}
						?>
						<div class="pull-left">

							<h2 class="heading-lg-green">Dashboard</h2>
							<p class="paragraph-text-sm-grey">Your performance overview</p>
						</div>
						<div style="display: none;" class="pull-right">
							<a class="btn-sm-blue btn" href="<?php echo  base_url('/product/add') ?>">New Product</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
			<div class="col-md-6 inner-body-block"><!-- full block start-->
				<div class="nauk-info-connections">
					<h3 class="heading-md-blue">Overall Status</h3>
					<div class="row icon-main-row ">
						<div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 dashboard-icon-field">
							<div class="title">
								<h4>Stores</h4>
							</div>
							<div class="icon-block text-center">

								<svg class="svg-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								viewBox="0 0 489.4 489.4" style="enable-background:new 0 0 489.4 489.4;" xml:space="preserve">
								<path d="M347.7,263.75h-66.5c-18.2,0-33,14.8-33,33v51c0,18.2,14.8,33,33,33h66.5c18.2,0,33-14.8,33-33v-51
								C380.7,278.55,365.9,263.75,347.7,263.75z M356.7,347.75c0,5-4.1,9-9,9h-66.5c-5,0-9-4.1-9-9v-51c0-5,4.1-9,9-9h66.5
								c5,0,9,4.1,9,9V347.75z"/>
								<path d="M489.4,171.05c0-2.1-0.5-4.1-1.6-5.9l-72.8-128c-2.1-3.7-6.1-6.1-10.4-6.1H84.7c-4.3,0-8.3,2.3-10.4,6.1l-72.7,128
								c-1,1.8-1.6,3.8-1.6,5.9c0,28.7,17.3,53.3,42,64.2v211.1c0,6.6,5.4,12,12,12h66.3c0.1,0,0.2,0,0.3,0h93c0.1,0,0.2,0,0.3,0h221.4
								c6.6,0,12-5.4,12-12v-209.6c0-0.5,0-0.9-0.1-1.3C472,224.55,489.4,199.85,489.4,171.05z M91.7,55.15h305.9l56.9,100.1H34.9
								L91.7,55.15z M348.3,179.15c-3.8,21.6-22.7,38-45.4,38c-22.7,0-41.6-16.4-45.4-38H348.3z M232,179.15c-3.8,21.6-22.7,38-45.4,38
								s-41.6-16.4-45.5-38H232z M24.8,179.15h90.9c-3.8,21.6-22.8,38-45.5,38C47.5,217.25,28.6,200.75,24.8,179.15z M201.6,434.35h-69
								v-129.5c0-9.4,7.6-17.1,17.1-17.1h34.9c9.4,0,17.1,7.6,17.1,17.1v129.5H201.6z M423.3,434.35H225.6v-129.5
								c0-22.6-18.4-41.1-41.1-41.1h-34.9c-22.6,0-41.1,18.4-41.1,41.1v129.6H66v-193.3c1.4,0.1,2.8,0.1,4.2,0.1
								c24.2,0,45.6-12.3,58.2-31c12.6,18.7,34,31,58.2,31s45.5-12.3,58.2-31c12.6,18.7,34,31,58.1,31c24.2,0,45.5-12.3,58.1-31
								c12.6,18.7,34,31,58.2,31c1.4,0,2.7-0.1,4.1-0.1L423.3,434.35L423.3,434.35z M419.2,217.25c-22.7,0-41.6-16.4-45.4-38h90.9
								C460.8,200.75,441.9,217.25,419.2,217.25z"/></svg>
								<h5 class=""><?php echo $adminStats['allStoreCount']; ?></h5>
							</div>
						</div>
						<div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 dashboard-icon-field">
							<div class="title">
								<h4>Products</h4>
							</div>
							<div class="icon-block text-center">

								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 512 512"><title>Commercial delivery </title><path d="M472.916,224H448.007a24.534,24.534,0,0,0-23.417-18H398V140.976a6.86,6.86,0,0,0-3.346-6.062L207.077,26.572a6.927,6.927,0,0,0-6.962,0L12.48,134.914A6.981,6.981,0,0,0,9,140.976V357.661a7,7,0,0,0,3.5,6.062L200.154,472.065a7,7,0,0,0,3.5.938,7.361,7.361,0,0,0,3.6-.938L306,415.108v41.174A29.642,29.642,0,0,0,335.891,486H472.916A29.807,29.807,0,0,0,503,456.282v-202.1A30.2,30.2,0,0,0,472.916,224Zm-48.077-4A10.161,10.161,0,0,1,435,230.161v.678A10.161,10.161,0,0,1,424.839,241H384.161A10.161,10.161,0,0,1,374,230.839v-.678A10.161,10.161,0,0,1,384.161,220ZM203.654,40.717l77.974,45.018L107.986,185.987,30.013,140.969ZM197,453.878,23,353.619V153.085L197,253.344Zm6.654-212.658-81.668-47.151L295.628,93.818,377.3,140.969ZM306,254.182V398.943l-95,54.935V253.344L384,153.085V206h.217A24.533,24.533,0,0,0,360.8,224H335.891A30.037,30.037,0,0,0,306,254.182Zm183,202.1A15.793,15.793,0,0,1,472.916,472H335.891A15.628,15.628,0,0,1,320,456.282v-202.1A16.022,16.022,0,0,1,335.891,238h25.182a23.944,23.944,0,0,0,23.144,17H424.59a23.942,23.942,0,0,0,23.143-17h25.183A16.186,16.186,0,0,1,489,254.182Z"/><path d="M343.949,325h7.327a7,7,0,1,0,0-14H351V292h19.307a6.739,6.739,0,0,0,6.655,4.727A7.019,7.019,0,0,0,384,289.743v-4.71A7.093,7.093,0,0,0,376.924,278H343.949A6.985,6.985,0,0,0,337,285.033v32.975A6.95,6.95,0,0,0,343.949,325Z"/><path d="M344,389h33a7,7,0,0,0,7-7V349a7,7,0,0,0-7-7H344a7,7,0,0,0-7,7v33A7,7,0,0,0,344,389Zm7-33h19v19H351Z"/><path d="M351.277,439H351V420h18.929a7.037,7.037,0,0,0,14.071.014v-6.745A7.3,7.3,0,0,0,376.924,406H343.949A7.191,7.191,0,0,0,337,413.269v32.975A6.752,6.752,0,0,0,343.949,453h7.328a7,7,0,1,0,0-14Z"/><path d="M393.041,286.592l-20.5,20.5-6.236-6.237a7,7,0,1,0-9.9,9.9l11.187,11.186a7,7,0,0,0,9.9,0l25.452-25.452a7,7,0,0,0-9.9-9.9Z"/><path d="M393.041,415.841l-20.5,20.5-6.236-6.237a7,7,0,1,0-9.9,9.9l11.187,11.186a7,7,0,0,0,9.9,0l25.452-25.452a7,7,0,0,0-9.9-9.9Z"/><path d="M464.857,295H420.891a7,7,0,0,0,0,14h43.966a7,7,0,0,0,0-14Z"/><path d="M464.857,359H420.891a7,7,0,0,0,0,14h43.966a7,7,0,0,0,0-14Z"/><path d="M464.857,423H420.891a7,7,0,0,0,0,14h43.966a7,7,0,0,0,0-14Z"/></svg>

								<h5 class=""><?php echo $adminStats['allProductCount']; ?></h5>
							</div>
						</div>

						<div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 dashboard-icon-field">
							<div class="title">
								<h4>Sales</h4>
							</div>
							<div class="icon-block text-center">

								<svg class="svg-icon"  viewBox="0 0 512.0001 512" xmlns="http://www.w3.org/2000/svg"><path d="m468.101562 130.023438-68.453124-113.457032c-1.8125-3-5.058594-4.832031-8.5625-4.832031-3.503907 0-6.753907 1.832031-8.5625 4.832031l-68.453126 113.457032c-1.863281 3.085937-1.917968 6.941406-.148437 10.082031 1.773437 3.140625 5.101563 5.085937 8.710937 5.085937h17.726563v54.015625h-76.605469c-5.523437 0-10 4.476563-10 10v35h-76.5625c-5.523437 0-10 4.476563-10 10v35h-76.566406c-5.523438 0-10 4.476563-10 10v135.40625c0 5.523438 4.476562 10 10 10h341.183594c5.523437 0 10-4.476562 10-10v-289.421875h17.730468c3.605469 0 6.933594-1.945312 8.707032-5.085937 1.773437-3.140625 1.71875-6.992188-.144532-10.082031zm-367.476562 179.183593h66.566406v115.40625h-66.566406zm86.566406-45h66.5625v160.40625h-66.5625zm86.5625-45h66.609375v205.40625h-66.609375zm158.054688-94.015625c-5.519532 0-10 4.476563-10 10v289.421875h-61.445313v-279.421875h18.039063c5.523437 0 10-4.480468 10-10 0-5.523437-4.476563-10-10-10h-38.058594l50.742188-84.097656 50.742187 84.097656zm0 0"/><path d="m267.992188 468.773438h-224.765626v-428.902344l5.246094 6.953125c1.964844 2.605469 4.960938 3.980469 7.992188 3.980469 2.09375 0 4.207031-.65625 6.011718-2.015626 4.410157-3.324218 5.289063-9.597656 1.964844-14.007812l-23.230468-30.800781c-1.886719-2.507813-4.84375-3.980469-7.984376-3.980469-3.136718 0-6.09375 1.472656-7.984374 3.980469l-23.226563 30.804687c-3.324219 4.410156-2.445313 10.679688 1.964844 14.003906 4.410156 3.324219 10.679687 2.445313 14.003906-1.964843l5.242187-6.953125v428.902344h-11.601562c-5.523438 0-10 4.476562-10 10 0 5.523437 4.476562 10 10 10h11.601562v11.601562c0 5.523438 4.476563 10 10 10 5.523438 0 10-4.476562 10-10v-11.601562h224.765626c5.519531 0 10-4.476563 10-10 0-5.523438-4.480469-10-10-10zm0 0"/><path d="m316.289062 474.949219c-.25-.609375-.558593-1.191407-.917968-1.730469-.359375-.550781-.78125-1.058594-1.242188-1.519531-.457031-.460938-.96875-.878907-1.519531-1.238281-.539063-.363282-1.117187-.671876-1.730469-.921876-.597656-.25-1.226562-.441406-1.867187-.570312-1.292969-.257812-2.621094-.257812-3.910157 0-.632812.128906-1.261718.320312-1.871093.570312-.601563.25-1.179688.558594-1.71875.921876-.550781.359374-1.0625.777343-1.519531 1.238281-.460938.460937-.882813.96875-1.25 1.519531-.363282.539062-.671876 1.121094-.921876 1.730469-.25.601562-.441406 1.230469-.570312 1.871093-.128906.640626-.191406 1.296876-.191406 1.949219 0 .648438.0625 1.308594.191406 1.960938.128906.628906.320312 1.257812.570312 1.867187.25.601563.558594 1.179688.921876 1.722656.367187.546876.789062 1.058594 1.25 1.519532.457031.460937.96875.878906 1.519531 1.25.539062.359375 1.117187.667968 1.71875.917968.609375.25 1.238281.441407 1.871093.570313.648438.132813 1.308594.191406 1.957032.191406.652344 0 1.3125-.058593 1.953125-.191406.640625-.128906 1.269531-.320313 1.867187-.570313.613282-.25 1.191406-.558593 1.730469-.917968.550781-.371094 1.058594-.789063 1.519531-1.25.460938-.460938.882813-.972656 1.242188-1.519532.359375-.542968.667968-1.121093.917968-1.722656.25-.609375.441407-1.238281.570313-1.867187.132813-.652344.199219-1.3125.199219-1.960938 0-.652343-.066406-1.308593-.199219-1.949219-.128906-.640624-.320313-1.269531-.570313-1.871093zm0 0"/><path d="m508.019531 470.789062-30.804687-23.230468c-4.40625-3.324219-10.679688-2.445313-14.003906 1.964844-3.324219 4.410156-2.445313 10.679687 1.964843 14.003906l6.953125 5.242187h-126.496094c-5.519531 0-10 4.480469-10 10 0 5.523438 4.480469 10 10 10h126.496094l-6.953125 5.246094c-4.410156 3.324219-5.289062 9.59375-1.964843 14.003906 1.964843 2.605469 4.960937 3.980469 7.992187 3.980469 2.09375 0 4.210937-.65625 6.011719-2.015625l30.804687-23.230469c2.507813-1.890625 3.980469-4.84375 3.980469-7.984375 0-3.136719-1.472656-6.09375-3.980469-7.980469zm0 0"/><path d="m283.53125 84.207031c-.667969-3.0625-2.738281-5.640625-5.589844-6.949219-2.847656-1.3125-6.148437-1.210937-8.917968.273438l-31.0625 16.710938c-4.863282 2.613281-6.683594 8.679687-4.070313 13.542968 2.617187 4.863282 8.679687 6.6875 13.546875 4.070313l6.238281-3.359375c-16.894531 36.304687-59.746093 74.445312-114.191406 101.132812-18.414063 9.023438-37.214844 16.277344-55.875 21.554688-5.3125 1.503906-8.402344 7.027344-6.902344 12.34375 1.246094 4.402344 5.257813 7.28125 9.617188 7.28125.902343 0 1.820312-.125 2.730469-.382813 19.8125-5.601562 39.742187-13.285156 59.234374-22.839843 57.199219-28.035157 102.832032-68.703126 122.410157-108.378907l.8125 3.726563c1.019531 4.675781 5.160156 7.871094 9.761719 7.871094.707031 0 1.421874-.078126 2.140624-.234376 5.394532-1.175781 8.816407-6.503906 7.640626-11.902343zm0 0"/></svg>
								<h5 class=""><?php echo '$'.number_format($adminStats['allSaleSum']); ?></h5>
							</div>
						</div>
					</div>
				</div>
			</div><!-- block end-->
			<div class="col-md-6 inner-body-block"><!-- full block start-->
				<div class="nauk-info-connections">
					<h3 class="heading-md-blue">Today Status</h3>
					<div class="row icon-main-row ">
						<div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 dashboard-icon-field">
							<div class="title">
								<h4>Stores</h4>
							</div>
							<div class="icon-block text-center">

								<svg class="svg-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								viewBox="0 0 489.4 489.4" style="enable-background:new 0 0 489.4 489.4;" xml:space="preserve">
								<path d="M347.7,263.75h-66.5c-18.2,0-33,14.8-33,33v51c0,18.2,14.8,33,33,33h66.5c18.2,0,33-14.8,33-33v-51
								C380.7,278.55,365.9,263.75,347.7,263.75z M356.7,347.75c0,5-4.1,9-9,9h-66.5c-5,0-9-4.1-9-9v-51c0-5,4.1-9,9-9h66.5
								c5,0,9,4.1,9,9V347.75z"/>
								<path d="M489.4,171.05c0-2.1-0.5-4.1-1.6-5.9l-72.8-128c-2.1-3.7-6.1-6.1-10.4-6.1H84.7c-4.3,0-8.3,2.3-10.4,6.1l-72.7,128
								c-1,1.8-1.6,3.8-1.6,5.9c0,28.7,17.3,53.3,42,64.2v211.1c0,6.6,5.4,12,12,12h66.3c0.1,0,0.2,0,0.3,0h93c0.1,0,0.2,0,0.3,0h221.4
								c6.6,0,12-5.4,12-12v-209.6c0-0.5,0-0.9-0.1-1.3C472,224.55,489.4,199.85,489.4,171.05z M91.7,55.15h305.9l56.9,100.1H34.9
								L91.7,55.15z M348.3,179.15c-3.8,21.6-22.7,38-45.4,38c-22.7,0-41.6-16.4-45.4-38H348.3z M232,179.15c-3.8,21.6-22.7,38-45.4,38
								s-41.6-16.4-45.5-38H232z M24.8,179.15h90.9c-3.8,21.6-22.8,38-45.5,38C47.5,217.25,28.6,200.75,24.8,179.15z M201.6,434.35h-69
								v-129.5c0-9.4,7.6-17.1,17.1-17.1h34.9c9.4,0,17.1,7.6,17.1,17.1v129.5H201.6z M423.3,434.35H225.6v-129.5
								c0-22.6-18.4-41.1-41.1-41.1h-34.9c-22.6,0-41.1,18.4-41.1,41.1v129.6H66v-193.3c1.4,0.1,2.8,0.1,4.2,0.1
								c24.2,0,45.6-12.3,58.2-31c12.6,18.7,34,31,58.2,31s45.5-12.3,58.2-31c12.6,18.7,34,31,58.1,31c24.2,0,45.5-12.3,58.1-31
								c12.6,18.7,34,31,58.2,31c1.4,0,2.7-0.1,4.1-0.1L423.3,434.35L423.3,434.35z M419.2,217.25c-22.7,0-41.6-16.4-45.4-38h90.9
								C460.8,200.75,441.9,217.25,419.2,217.25z"/></svg>
								<h5 class=""><?php echo $adminStats['todayStoreCount']; ?></h5>
							</div>
						</div>
						<div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 dashboard-icon-field">
							<div class="title">
								<h4>Products</h4>
							</div>
							<div class="icon-block text-center">

								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 512 512"><title>Commercial delivery </title><path d="M472.916,224H448.007a24.534,24.534,0,0,0-23.417-18H398V140.976a6.86,6.86,0,0,0-3.346-6.062L207.077,26.572a6.927,6.927,0,0,0-6.962,0L12.48,134.914A6.981,6.981,0,0,0,9,140.976V357.661a7,7,0,0,0,3.5,6.062L200.154,472.065a7,7,0,0,0,3.5.938,7.361,7.361,0,0,0,3.6-.938L306,415.108v41.174A29.642,29.642,0,0,0,335.891,486H472.916A29.807,29.807,0,0,0,503,456.282v-202.1A30.2,30.2,0,0,0,472.916,224Zm-48.077-4A10.161,10.161,0,0,1,435,230.161v.678A10.161,10.161,0,0,1,424.839,241H384.161A10.161,10.161,0,0,1,374,230.839v-.678A10.161,10.161,0,0,1,384.161,220ZM203.654,40.717l77.974,45.018L107.986,185.987,30.013,140.969ZM197,453.878,23,353.619V153.085L197,253.344Zm6.654-212.658-81.668-47.151L295.628,93.818,377.3,140.969ZM306,254.182V398.943l-95,54.935V253.344L384,153.085V206h.217A24.533,24.533,0,0,0,360.8,224H335.891A30.037,30.037,0,0,0,306,254.182Zm183,202.1A15.793,15.793,0,0,1,472.916,472H335.891A15.628,15.628,0,0,1,320,456.282v-202.1A16.022,16.022,0,0,1,335.891,238h25.182a23.944,23.944,0,0,0,23.144,17H424.59a23.942,23.942,0,0,0,23.143-17h25.183A16.186,16.186,0,0,1,489,254.182Z"/><path d="M343.949,325h7.327a7,7,0,1,0,0-14H351V292h19.307a6.739,6.739,0,0,0,6.655,4.727A7.019,7.019,0,0,0,384,289.743v-4.71A7.093,7.093,0,0,0,376.924,278H343.949A6.985,6.985,0,0,0,337,285.033v32.975A6.95,6.95,0,0,0,343.949,325Z"/><path d="M344,389h33a7,7,0,0,0,7-7V349a7,7,0,0,0-7-7H344a7,7,0,0,0-7,7v33A7,7,0,0,0,344,389Zm7-33h19v19H351Z"/><path d="M351.277,439H351V420h18.929a7.037,7.037,0,0,0,14.071.014v-6.745A7.3,7.3,0,0,0,376.924,406H343.949A7.191,7.191,0,0,0,337,413.269v32.975A6.752,6.752,0,0,0,343.949,453h7.328a7,7,0,1,0,0-14Z"/><path d="M393.041,286.592l-20.5,20.5-6.236-6.237a7,7,0,1,0-9.9,9.9l11.187,11.186a7,7,0,0,0,9.9,0l25.452-25.452a7,7,0,0,0-9.9-9.9Z"/><path d="M393.041,415.841l-20.5,20.5-6.236-6.237a7,7,0,1,0-9.9,9.9l11.187,11.186a7,7,0,0,0,9.9,0l25.452-25.452a7,7,0,0,0-9.9-9.9Z"/><path d="M464.857,295H420.891a7,7,0,0,0,0,14h43.966a7,7,0,0,0,0-14Z"/><path d="M464.857,359H420.891a7,7,0,0,0,0,14h43.966a7,7,0,0,0,0-14Z"/><path d="M464.857,423H420.891a7,7,0,0,0,0,14h43.966a7,7,0,0,0,0-14Z"/></svg>

								<h5 class=""><?php echo $adminStats['todayProductCount']; ?></h5>
							</div>
						</div>
						<div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 dashboard-icon-field">
							<div class="title">
								<h4>Sales</h4>
							</div>
							<div class="icon-block text-center">

								<svg class="svg-icon"  viewBox="0 0 512.0001 512" xmlns="http://www.w3.org/2000/svg"><path d="m468.101562 130.023438-68.453124-113.457032c-1.8125-3-5.058594-4.832031-8.5625-4.832031-3.503907 0-6.753907 1.832031-8.5625 4.832031l-68.453126 113.457032c-1.863281 3.085937-1.917968 6.941406-.148437 10.082031 1.773437 3.140625 5.101563 5.085937 8.710937 5.085937h17.726563v54.015625h-76.605469c-5.523437 0-10 4.476563-10 10v35h-76.5625c-5.523437 0-10 4.476563-10 10v35h-76.566406c-5.523438 0-10 4.476563-10 10v135.40625c0 5.523438 4.476562 10 10 10h341.183594c5.523437 0 10-4.476562 10-10v-289.421875h17.730468c3.605469 0 6.933594-1.945312 8.707032-5.085937 1.773437-3.140625 1.71875-6.992188-.144532-10.082031zm-367.476562 179.183593h66.566406v115.40625h-66.566406zm86.566406-45h66.5625v160.40625h-66.5625zm86.5625-45h66.609375v205.40625h-66.609375zm158.054688-94.015625c-5.519532 0-10 4.476563-10 10v289.421875h-61.445313v-279.421875h18.039063c5.523437 0 10-4.480468 10-10 0-5.523437-4.476563-10-10-10h-38.058594l50.742188-84.097656 50.742187 84.097656zm0 0"/><path d="m267.992188 468.773438h-224.765626v-428.902344l5.246094 6.953125c1.964844 2.605469 4.960938 3.980469 7.992188 3.980469 2.09375 0 4.207031-.65625 6.011718-2.015626 4.410157-3.324218 5.289063-9.597656 1.964844-14.007812l-23.230468-30.800781c-1.886719-2.507813-4.84375-3.980469-7.984376-3.980469-3.136718 0-6.09375 1.472656-7.984374 3.980469l-23.226563 30.804687c-3.324219 4.410156-2.445313 10.679688 1.964844 14.003906 4.410156 3.324219 10.679687 2.445313 14.003906-1.964843l5.242187-6.953125v428.902344h-11.601562c-5.523438 0-10 4.476562-10 10 0 5.523437 4.476562 10 10 10h11.601562v11.601562c0 5.523438 4.476563 10 10 10 5.523438 0 10-4.476562 10-10v-11.601562h224.765626c5.519531 0 10-4.476563 10-10 0-5.523438-4.480469-10-10-10zm0 0"/><path d="m316.289062 474.949219c-.25-.609375-.558593-1.191407-.917968-1.730469-.359375-.550781-.78125-1.058594-1.242188-1.519531-.457031-.460938-.96875-.878907-1.519531-1.238281-.539063-.363282-1.117187-.671876-1.730469-.921876-.597656-.25-1.226562-.441406-1.867187-.570312-1.292969-.257812-2.621094-.257812-3.910157 0-.632812.128906-1.261718.320312-1.871093.570312-.601563.25-1.179688.558594-1.71875.921876-.550781.359374-1.0625.777343-1.519531 1.238281-.460938.460937-.882813.96875-1.25 1.519531-.363282.539062-.671876 1.121094-.921876 1.730469-.25.601562-.441406 1.230469-.570312 1.871093-.128906.640626-.191406 1.296876-.191406 1.949219 0 .648438.0625 1.308594.191406 1.960938.128906.628906.320312 1.257812.570312 1.867187.25.601563.558594 1.179688.921876 1.722656.367187.546876.789062 1.058594 1.25 1.519532.457031.460937.96875.878906 1.519531 1.25.539062.359375 1.117187.667968 1.71875.917968.609375.25 1.238281.441407 1.871093.570313.648438.132813 1.308594.191406 1.957032.191406.652344 0 1.3125-.058593 1.953125-.191406.640625-.128906 1.269531-.320313 1.867187-.570313.613282-.25 1.191406-.558593 1.730469-.917968.550781-.371094 1.058594-.789063 1.519531-1.25.460938-.460938.882813-.972656 1.242188-1.519532.359375-.542968.667968-1.121093.917968-1.722656.25-.609375.441407-1.238281.570313-1.867187.132813-.652344.199219-1.3125.199219-1.960938 0-.652343-.066406-1.308593-.199219-1.949219-.128906-.640624-.320313-1.269531-.570313-1.871093zm0 0"/><path d="m508.019531 470.789062-30.804687-23.230468c-4.40625-3.324219-10.679688-2.445313-14.003906 1.964844-3.324219 4.410156-2.445313 10.679687 1.964843 14.003906l6.953125 5.242187h-126.496094c-5.519531 0-10 4.480469-10 10 0 5.523438 4.480469 10 10 10h126.496094l-6.953125 5.246094c-4.410156 3.324219-5.289062 9.59375-1.964843 14.003906 1.964843 2.605469 4.960937 3.980469 7.992187 3.980469 2.09375 0 4.210937-.65625 6.011719-2.015625l30.804687-23.230469c2.507813-1.890625 3.980469-4.84375 3.980469-7.984375 0-3.136719-1.472656-6.09375-3.980469-7.980469zm0 0"/><path d="m283.53125 84.207031c-.667969-3.0625-2.738281-5.640625-5.589844-6.949219-2.847656-1.3125-6.148437-1.210937-8.917968.273438l-31.0625 16.710938c-4.863282 2.613281-6.683594 8.679687-4.070313 13.542968 2.617187 4.863282 8.679687 6.6875 13.546875 4.070313l6.238281-3.359375c-16.894531 36.304687-59.746093 74.445312-114.191406 101.132812-18.414063 9.023438-37.214844 16.277344-55.875 21.554688-5.3125 1.503906-8.402344 7.027344-6.902344 12.34375 1.246094 4.402344 5.257813 7.28125 9.617188 7.28125.902343 0 1.820312-.125 2.730469-.382813 19.8125-5.601562 39.742187-13.285156 59.234374-22.839843 57.199219-28.035157 102.832032-68.703126 122.410157-108.378907l.8125 3.726563c1.019531 4.675781 5.160156 7.871094 9.761719 7.871094.707031 0 1.421874-.078126 2.140624-.234376 5.394532-1.175781 8.816407-6.503906 7.640626-11.902343zm0 0"/></svg>
								<h5 class=""><?php echo '$'.number_format($adminStats['todaySaleSum']); ?></h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- new block end-->

			<div class="col-md-6 inner-body-block"><!-- full block start-->
				<div class="nauk-info-connections">
					<h3 class="heading-md-blue">Sales and Transactions</h3>
					<div class="img-responsive" id="chart"> </div>
					<!--<div class=" graph" id="bars"> </div>-->
				</div>

				<div class="nauk-info-connections text-right">
					<a class="btn-sm-blue btn" href="<?php echo  base_url('/reports'); ?>">View reports</a>
				</div>
			</div><!-- block end-->



			<div class="col-md-6 inner-body-block"><!-- block start-->
				<div class="nauk-info-connections">
					<h3 class="heading-md-blue">My Overview</h3>
				</div>
				<div class="nauk-info-connections  border-dark-1" style="padding:10px;">
					<div style="display: none;" class="page-header">
						<div class="pull-left">
							<p class="paragraph-text-sm-grey">Product</p>
						</div>
						<div class="pull-right">
							<p class="paragraph-text-sm-grey"><?php echo $products; ?>

							<?php if ($pack_product == ' ' || $pack_product <= 0){ 
								echo  $pack_product;
							} else { 
								echo  $pack_product; 
							}
							?> 
						</p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div style="display: none;" class="page-header">
					<div class="pull-left">
						<p class="paragraph-text-sm-grey">Storage Used</p>
					</div>
					<div class="pull-right">
						<p class="paragraph-text-sm-grey">1.3GB / 15GB</p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="page-header">
					<div class="pull-left">
						<h4 class="paragraph-text-sm-grey grey-sm-bold">Package Sales Overview</h4>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="page-header">
					<div class="pull-left">
						<p class="paragraph-text-sm-grey">Today Sales</p>
					</div>
					<div class="pull-right">
						<p class="paragraph-text-sm-grey"><?php echo $today_sale['today_sale']; ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="page-header">
					<div class="pull-left">
						<p class="paragraph-text-sm-grey">Month to date</p>
					</div>
					<div class="pull-right">
						<p class="paragraph-text-sm-grey"><?php echo $month_sale['month_sale']; ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="page-header">
					<div class="pull-left">
						<p class="paragraph-text-sm-grey">Year to date</p>
					</div>
					<div class="pull-right">
						<p class="paragraph-text-sm-grey"><?php echo $year_sale['year_sale']; ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="page-header">
					<div class="pull-left">
						<p class="paragraph-text-sm-grey">Lifetime sales</p>
					</div>
					<div class="pull-right">
						<p class="paragraph-text-sm-grey"><?php echo $lifetime_sale['life_sale']; ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="page-header">
					<div class="pull-left">
						<p class="paragraph-text-sm-grey">Overall earnings</p>
					</div>
					<div class="pull-right">
						<!-- <p class="paragraph-text-sm-grey">$<?php echo number_format($overall_sale['overall']); ?> -->
						<?php if($overall_sale['overall']){ echo '$' . number_format($overall_sale['overall']); }else{echo '0';} ?>
						
					</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div><!-- block end-->
	<div class="col-md-12 mx-auto inner-body-block"><!-- full block start-->
		<div class="nauk-info-connections text-center">

		</div>
	</div>



	<div class="col-md-6 inner-body-block"><!-- block start-->
		<div class="nauk-info-connections">
			<h3 class="heading-md-blue">What can you do next?</h3>

			<video class="img-responsive" src="<?php echo base_url(); ?>site_assets/video/<?php echo $store['userfile']; ?>" controls  width="100%"> </video>

			<p class="paragraph-text-sm-grey">Create first product on Digital Media Deliveries</p>
			<a class="btn-sm-blue btn" href="#">View help</a>

		</div>
	</div><!-- block end-->

	
	<div class="col-md-6 inner-body-block"><!-- block start-->
		<div class="nauk-info-connections">
			<h3 class="heading-md-blue">new features</h3>
		</div>
		<div class="nauk-info-connections border-dark-1" style="padding:10px;">
			<p class="paragraph-text-sm-grey"> 
				<?php echo $store['dash_feature_text']; ?>
			</p>

			<a class="btn-sm-blue btn" href="<?php echo base_url("user/new_updates"); ?>">View All</a>
		</div>
		
	</div><!-- block end-->

</div>

</div><!-- inner-body-end-->
</div>
<!-- <script src="<?php echo base_url(); ?>site_assets/js/barchart.jquery.js"></script> -->
<script>
				/*$('#chart').barChart({

					height : 200,
					bars : [
					{ 
						name : 'Dataset 1',
						values:[<?php echo $use_array; ?>]
					//values : [[1450569600,0],[1450656000,0],[1450742400,34],[1450828800,75],[1450915200,59],[1451001600,120]] 
				}
				
				]
			});*/
		</script>


		<script type="text/javascript">
			$(document).ready(function(){
				$.ajax({
					type: "post",
					url: "/reports/billing",
					data: {period: '7'},
					success: function(msg){
      //console.log(msg);
      if(msg){
      	buildChart(msg);
      }else{
      	$("#chart").html('<p class="text-info ">No package sale has been found in last 7 days.</p>');
          //console.log('no data');
      }
      }//end success function
    });//end ajax
			});

			function buildChart(ChartData){
				var data = JSON.parse(ChartData);
  //$('#chart').highcharts({
  	var chart = new Highcharts.Chart({
  		chart: {
  			renderTo: 'chart',
  			height: 204,
  			type: 'column'
  		},

  		title: {
  			text: ''
  		},

  		xAxis: {
  			categories: data.xAxis
          /*tickInterval: 1,
          labels: {
              enabled: true,
              formatter: function() { return seriesData[this.value][0];},
          }*/
      },

      yAxis: {
      	title: false
      },

      series: [{
      	name: 'Sales',
      	data: data.yAxis     
      }],

      plotOptions: {
      	line: {
      		dataLabels: {
      			enabled: true
      		},
      		enableMouseTracking: true
      	}
      },

      legend: {
      	enabled: false
      },

      navigation: {
      	buttonOptions: {
      		enabled: false
      	}
      },

      responsive: {
      	rules: [{
      		condition: {
      			maxWidth: 500
      		},
      		chartOptions: {
      			legend: {
      				layout: 'horizontal',
      				align: 'center',
      				verticalAlign: 'bottom'
      			}
      		}
      	}]
      }


    });//end chart
  	$(".highcharts-credits").hide();
  }
</script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>



