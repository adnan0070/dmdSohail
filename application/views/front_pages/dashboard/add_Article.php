<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->


    <div class="row">

      <div class="col-md-12 inner-body-head"><!-- full block start-->
        <div class="nauk-info-connections">
          <div class="page-header">
            <div class="pull-left">
              <div id="showError"></div>
              <h2 class="heading-lg-green"><?php echo $action; ?> Article </h2>
              <p class="paragraph-text-sm-grey">Manage all your blog articles.</p>
            </div>

            <div class="clearfix"></div>
          </div>
        </div>
      </div><!-- block end-->

    </div>

    <?php 
    if(!isset($post)){
      $post = $this->session->flashdata('post');
    }

    if(!isset($post)){
      $post = array();
    }


    $form_action = '';
    if($action=='Edit'){ 
      $form_action = 'edit_Article';
    } else { 
      $form_action = 'add_Article';
    } 

    ?>
    <form method="post" id="art" action="<?php echo base_url('admin/') . $form_action; ?>" enctype="multipart/form-data" >
     <?php if($action=='Edit'){ ?>
       <!-- <input type="hidden" name="id" value="<?php if(isset($_REQUEST['id'])) { echo $_REQUEST['id']; }?>"> -->
       <input type="hidden" name="id" value="<?php echo !empty($post['id'])?$post['id']:''; ?>">
     <?php } ?>
     <div class="row">



      <?php 

      if($this->session->flashdata('error_msg')){
        ?>
        <div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
        </div>

        <?php
      }

      if($this->session->flashdata('success_msg')){
        ?>

        <div class="alert alert-success alert-dismissible" style="margin-top: 50px">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
        </div>


      <?php } 

      ?>



     <!-- <div class="col-md-12 inner-body-head">
        <div class="panel-heading"><a href="<?php echo site_url('admin/add_Article'); ?>" class="glyphicon glyphicon-arrow-left pull-right"></a></div>

      </div> -->



      <div class="col-md-6">
        <div class="item form-group">   
         <label class="control-label " for="title">Title<span style="color: red;" class="required">*</span>
         </label>
         <input type="text"  id="title" name="title" value="<?php echo !empty($post['title'])?$post['title']:''; ?>" class="form-control form-input  validThis">
       </div>
     </div>

     <div class="col-md-6">
       <div class="item form-group">
        <label class="control-label " for="title">Meta Keywords<span style="color: red;" class="required">*</span></label>
           <input type="text" id="title" name="meta_keywords" value="<?php echo !empty($post['meta_keywords'])?$post['meta_keywords']:''; ?>" class="form-control form-input validThis">
         </div>
       </div>

       <div class="col-md-12">
        <div class="item form-group">
          <label class="control-label" for="Category">Category<span style="color: red;" class="required">*</span></label>
          <select name="Category_id" class="form-control form-input validThis">
            <?php foreach ($posts as $category) { ?>
              <option <?php if(@$post['Category_id'] == $category['id'] ){ echo 'selected';} ?> value="<?php echo $category['id']; ?>"><?php echo $category['title']; ?></option>  
            <?php } ?>
          </select>
        </div>
      </div>

      <div class="col-md-12">
        <div class="item form-group"> 
         <label class="control-label " for="title">Meta Description<span style="color: red;" class="required">*</span>
         </label>
         <textarea type="text"  id="title" name="meta_description" value="" rows="6" class="form-control form-textarea validThis"><?php echo !empty($post['meta_description'])?$post['meta_description']:''; ?></textarea>
       </div>
     </div>


                <!-- <div class="item form-group">    
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label " for="title">Content<span style="color: red;" class="required">*</span>
                        </label>
                        <textarea type="text"  id="title" required="required" name="content" value="" class="form-control col-md-7 col-xs-12"><?php //echo !empty($post['content'])?$post['content']:''; ?></textarea>
                    </div>
                  </div> -->

                  <div class="col-md-12 editor">
                    <label for="email-editor" class="heading-md-grey">Content</label>
                    <div class="form-group">
                      <textarea name="content" 
                      id="edit-email-content-editor"
                      class="form-control border-dark-1 validThis"></textarea>
                    </div>
                  </div> 



                  <div class="col-md-12 text-center">
                    <div class="item form-group">
                      <div class="nauk-info-connections block-file-upload block-border-dotted text-center">

                        <h3 class="heading-sm-grey text-center">UPLOAD FILE</h3>
                        <div class="form-group">

                          <input name="file" 
                          type="file" 
                          id="fileInput" 
                          class="file-input" 
                          placeholder="Select from computer" 
                          value=""/>

                          <!--<img id="output_image"/>-->
                          <img class="btn-upload-file" src="<?php echo base_url(); ?>site_assets/images/btn-upload-file.png">
                        </div>
                        <div style="" class="form-group">
                          <span class="paragraph-sm-grey" id="putfval"><?= isset($post['file']) ? $post['file']  : ''; ?></span>

                        </div>

                        <div style="display: none;" class="form-group">

                          <input type="checkbox" class="border-dark-1" name="enable"> 
                          <span class="paragraph-sm-grey">You can also drag and drop files.</span>

                        </div>
                        <!--<span class="text-danger">Validation error</span>-->
                      </div> 
                    </div> 
                  </div>

                  <div class="ln_solid col-md-12"></div>

                  <div class="col-md-12 text-center">
                    <div class="form-group">
                      <button type="submit" id="sbmtt" class="btn default-btn-green">submit</button>&nbsp;
                      <a href="<?php echo base_url().'admin/articles'; ?>" class="btn default-btn-blue" >Cancel</a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <script>
            $(document).ready(function() {
              var editor = $("#edit-email-content-editor");
              editor.Editor();
              var content = '<?php echo !empty($post["content"])?addslashes($post["content"]):""; ?>';
//content = "zain";
editor.Editor("setText", content);
//console.log(content);
});

            $("#sbmtt").on("click", function(e){
              e.preventDefault();
              $("#edit-email-content-editor").val($("#edit-email-content-editor").Editor("getText"));
              formValidate("art");
            });

            document.getElementById('fileInput').onchange = function () {
              $("#putfval").text(this.value);
            };

/*var form = document.getElementById('frmEditor1');
form.addEventListener('submit', function(event) {
event.preventDefault();
});*/
</script>