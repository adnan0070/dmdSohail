</div>
<div class="container-fluid home-container-full">
	<div class="row footer footer-main">
		<div class="col-md-6  footer-left max-auto">
			<div class="nauk-info-connections text-left">
				<h4> About Digital Media Deliveries</h4>
				<p>Digital Media Deliveries (DMD) is an automated digital media and product delivery portal serving business worldwide. From e-books to videos to training material and beyond, our platform allows businesses to instantly, automatically, and securely upload, organize, and share digital products with the world. We also offer additional resources to boost sales and foster measurable success for years to come.
				</p>
				<p>Copyright &copy; 2018 – Present Digital Media Deliveries. All Rights Reserved.</p>
			</div>
		</div>

		<div class="col-md-6  footer-right max-auto">
			<?php $this->load->view('front_pages/all_footer'); ?>
		<!-- <div class="nauk-info-connections text-left">
			<h4> About Digital Media Deliveries</h4>
			<p> <a>About us</a>&nbsp;/&nbsp;<a>Support</a>&nbsp;/&nbsp;<a>Our Team</a>&nbsp;/&nbsp;<a>Site Status</a>&nbsp;/&nbsp;<a>Affiliate Program</a></p>
		
			<h4>Social Media</h4>
			<p> <a>Facebook</a>&nbsp;/&nbsp;<a>Linkedin</a>&nbsp;/&nbsp;<a>Twitter</a></p>
		
			<h4>Legal Links</h4>
			<p> <a>Legal Notice</a>&nbsp;/&nbsp;<a>DMCA Notice</a>&nbsp;/&nbsp;<a>Terms of Service</a></p>
		
		
		</div> -->
	</div>
</div>
</div>





<script>
		//coupon search
		function searchCoupon () {
			var v = $('#cName').val();
			if(v.length > 2) {
				console.log(v);
				var status = $('#cStatus').val();
				if(!status){ status = 'live' }

					var d = {'cName': v, 'cStatus': status}
				
				$.ajax({
					data: d,
					type: "POST",
					url: '<?php echo base_url(); ?>' + 'store/coupon_search',
					cache: false,
					success: function(msg){
						//console.log(msg);
						$("#cBody").html(msg);
					}
				});

			}else if(v.length <= 2){
				$.ajax({
					data: "d=dmd_coupon",
					type: "POST",
					url: '<?php echo base_url(); ?>' + 'store/couponShowAll',
					cache: false,
					success: function(msg){
						//console.log(msg);
						$("#cBody").html(msg);
					}
				});
			}
		}

		$('#cName').on({
			keyup: searchCoupon,
			click: searchCoupon,
			select: searchCoupon
		});
		$('#cStatus').on({
			select: searchCoupon
		});

		

		$(document).ready(function(){

			$('.datepicker').datepicker({
				autoclose: true,
				clearBtn: true,
				enableOnReadonly: true,
				todayBtn: true,
				todayHighlight: true
			});

			$(".monthpicker").datepicker( {
				format: "yyyy-mm",
				startView: "months",
				minViewMode: "months",
				autoclose: true,
				clearBtn: true,
				enableOnReadonly: true
			});

			


		});

	</script>
<!-- 	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-147349522-1']);
		_gaq.push(['_setDomainName', 'jqueryscript.net']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	</script> -->

	<script type="text/javascript">
		function formValidate(FormId){
			var validation = false;
			var f = $("#" + FormId)[0];
			var e;
			var errorMsg;
			

			if (f.checkValidity()) {
				$(".validThis").each(function(){
					e = $(this);
					if(($.trim(e.val()) == "") || (e.val() == null) || (typeof e.val() === 'undefined')){
				        	//console.log("run");
				        	errorMsg = e.attr("name").replace(/_|-/g, " ").toUpperCase() + " is required.";
				        	if(e.attr("name") == 'userfile' || e.attr("name") == 'file'){
				        		errorMsg = 'File or image uploading error! Please choose a right file or image';
				        	}
				        	e.focus();
				        	$('#showError').html('<div style="word-wrap:break-word;" class="alert alert-danger alert-dismissable text-justify"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + errorMsg + '</div>');
				        	validation = false;
				        	return false;
				        }else{
				        	validation = true;
				        }
				    });
				if (validation == true) {
				    	//console.log(f);
				    	f.submit();
				    }

				}else{
					$('#showError').html('<div style="word-wrap:break-word;" class="alert alert-danger alert-dismissable text-justify"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error: </strong>All enable fields are required! Please check again all of your input.</div>');
				}
			}
		</script>

	</body>
	</html>

