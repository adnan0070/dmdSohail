<div class="row main-body mx-auto ">
 <?php $this->load->view('front_pages/dashboard/dash_left'); ?>
  <div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
    <div class="row">

      <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-info alert-dismissible" style="margin-top: 50px">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Info!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
      <?php } ?>

      <div class="col-md-12 inner-body-head"><!-- full block start-->
        <div class="nauk-info-connections">
          <div class="page-header">
            <div class="pull-left">
              <h2 class="heading-lg-green">Billing History</h2>
              <p class="paragraph-text-sm-grey">View history of your billing</p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div><!-- block end-->
    </div>
    
    <table class="table purchase-table">
      <thead class="back-border">
        <tr>
          <th scope="col">Month</th>
          <th scope="col">Amount Paid</th>
          <th scope="col">Download Invoice</th>
        </tr>
      </thead>
      <tbody id="billing">
        <?php 
          if($history){
            //print_r($history);
          foreach ($history as $key => $value) { ?>
                  <tr>
                    <td><?php echo date('F-Y', strtotime($value["created_date"])); ?></td>
                    <td><?php echo "$" . $value["amount"]; ?></td>
                    <td><a href="<?php echo base_url('user/billing_invoice/') . $value['id']; ?>" class="btn btn-sm-blue">Download</a></td>
                  </tr>
              <?php }
              } ?>
      </tbody>
    </table>
    
  </div>
</div>



<!-- <form action="/charge" method="post" id="payment-form">
  <div class="form-row">
    <label for="card-element">
      Credit or debit card
    </label>
    <div id="card-element">
      A Stripe Element will be inserted here.
    </div>

    Used to display form errors.
    <div id="card-errors" role="alert"></div>
  </div>

  <button>Submit Payment</button>
</form> -->


<!-- <script src="https://js.stripe.com/v3/"></script> -->
<script type="text/javascript">
  // Create a Stripe client.
/*var stripe = Stripe('pk_test_AYdBqSQMdtU12wnyIsOJTAoO');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});*/
</script>