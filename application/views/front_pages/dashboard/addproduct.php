<div class="row main-body mx-auto ">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>			
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<div class="col-md-12">
				<?php
				$success_msg = $this->session->flashdata('success_msg');
				$error_msg = $this->session->flashdata('error_msg');
				$fields_val = array();
				$fields_val['upsel_product'] = array();
				$fields_val['cross_sell_product'] = array();
				$duration = 0;
				$fields_val = $this->session->flashdata('fields');
				$save_id = "";
				if (isset($fields_val['save_id'])) {
					$save_id = $fields_val['save_id'];
				}
				$method = "add";

				if (isset($products[0]['id']) && $products[0]['id'] != '') {
					$fields_val = $products[0];
					$save_id = $products[0]['product_code'];
					$method = "update";
					$fields_val['upsel_product'] = json_decode($fields_val['upsel_product']);
					$fields_val['cross_sell_product'] = json_decode($fields_val['cross_sell_product']);
					if(isset($fields_val['webinar_play_duration'])){
						$duration = $fields_val['webinar_play_duration'];
					}
				}
				if (isset($fields_val['save_id']) && $fields_val['save_id'] != "") {
					$method = "update";
				}
				$hour = 0; $minute = 0; $second = 0;
				if($duration > 0){
					$h = $duration/3600;

					if($h >= 1){
						$hour = floor($h);
						$m = $duration - ($hour*3600);
						if(($m/60) >= 1){
							$minute = floor($m/60);
							$second = $duration - ($minute*60+$hour*3600);
						}else{
						$minute = $m;//minutes
						$second = floor($m/60);
					}
				}
				else{
					if(($duration/60) >= 1){
						$minute = floor($duration/60);
						$m = $duration - ($minute*60);
						$second = floor($m/60);

					}else{
						$second = $duration;
					}
				}
			}



			if ($success_msg) {
				?>

				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>

				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo str_replace("_"," ",$error_msg); ?>
				</div>
				<?php
			}
			?>
		</div>
		<div class="col-md-12 inner-body-head"><!-- full block start-->
			<div class="nauk-info-connections">
				<div class="page-header">
					<div class="pull-left">

						<?php if ($method == "add"){ ?>
							<h2 class="heading-lg-green">Create new product</h2>
							<p class="paragraph-text-sm-grey">Enter information about your new product.</p>

						<?php } else { ?>

							<h2 class="heading-lg-green">Edit product</h2>
							<p class="paragraph-text-sm-grey">Update information about your product.</p>

						<?php } ?>
							<p class="paragraph-text-sm-grey">For better look and feel, product image should be in equal height and width, also use minimum dimension of (500 x 500)</p>

					</div>
				</div>
			</div>

		</div>
	</div>
	<?php

	?>
	<form method="post" enctype="multipart/form-data" action="<?php echo
	base_url('product/' . $method); ?>">

	<div class="row form">

		<div class="col-md-6">

			<div id="imagePreview" style="background-image: url('<?php echo base_url() .
			"site_assets/products/" . @$fields_val['pro_image']; ?>');background-repeat: no-repeat;background-size: contain; background-position:center;" class="nauk-info-connections block-file-upload block-border-dotted text-center">

			<!--<h3 class="heading-sm-grey text-center">UPLOAD FILE</h3>-->
			<div class="form-group">
				<input hidden="" name="save_id" value="<?php echo @$save_id; ?>" />
				<input hidden="" name="slug_name" value="<?php echo
				@$fields_val['product_slug']; ?>" />
				<input hidden="" name="old_image_path"  value="<?php echo
				@$fields_val['pro_image']; ?>" />
				<input hidden="" name="old_image" id="old_image" value="<?php echo
				@$fields_val['pro_image']; ?>" />
				<input hidden="" name="old_image_thumb" id="old_image_thumb" value="<?php echo
				@$fields_val['pro_image_thumb']; ?>" />
				<input style="display: none;" id="imfile" type="file" value="" name="userfile" class="file-input-product" onchange="preview_image(this)" placeholder="Select from computer">

			</div>

		</div> 
		<div class="form-group text-center" >
			<span id ="image-error" class="text-danger"> </span>
			<input type="button" onclick="upload_img()" value="Upload Image" class="btn-sm-blue btn" />
		</div>


		<?php
		$download_checked = '';
		if(isset($fields_val['is_download'])){
			if($fields_val['is_download'] == 'on' && $fields_val['is_download'] != ''){
				$download_checked = 'checked';
			}
		}
		?>

		<div class="form-group">
			<label>
				<input <?php echo $download_checked; ?> type="checkbox" name="is_download" class="border-dark-1">
				<span class="paragraph-dark-md-capital"> Is downloadable</span>
			</label>
		</div>





		<!-- Webinar Video-->
		<?php if(isset($fields_val['is_webinar'])){  ?>
			<div  class="nauk-info-connections block-file-upload block-border-dotted text-center" style="padding:0px;  max-height:163px;">

				<div class="form-group ">
					<?php  if($fields_val['is_webinar'] == '1' && $fields_val['is_webinar'] != ''){ ?>
						<video id="webinar-video" controls style=" width:100%;max-height:163px !important;  ">
							<source src="<?php echo base_url('site_assets/products/'.@$fields_val['webinar_video']); ?>" type=""/>
							</video>

						<?php } else { ?>
							<video id="webinar-video" controls style="display:none; width:100%;max-height:163px !important;  ">
								<source src="" type=""/>
							</video>
						<?php }  
						?>
						

						<input type="hidden" name="old_webinar_path"  value="<?php echo
						@$fields_val['webinar_video']; ?>"/>
						<input type="hidden" name="old_webinar" id="old_webinar" value="<?php echo
						@$fields_val['webinar_video']; ?>" />
						<input style="display: none;" id="webinarfile" type="file" value="" name="webinarfile" class="file-input-product" onchange="preview_webinar(this)">
					</div>

				</div> 
				<div class="form-group text-center" >
					<span id ="webinar-error" class="text-danger"> </span>
					<input type="button" onclick="upload_webinar()" value="Upload Webinar" class="btn-sm-blue btn" />
				</div>


				<?php
				$webinar_checked = '';
				if(isset($fields_val['is_webinar'])){
					if($fields_val['is_webinar'] == '1' && $fields_val['is_webinar'] != ''){
						$webinar_checked = 'checked';
					}
				}
				?>

				<div class="form-group">
					<label>
						<input <?php echo $webinar_checked; ?> type="checkbox" name="is_webinar" class="border-dark-1">
						<span class="paragraph-dark-md-capital"> Is Webinar</span>
					</label>
				</div>

				<!-- end Webinar video -->
				<div class="row" id='duration-row'>
					<div class="col-md-4">
						<div class="form-group input-effects"  id="hour-block">
							<input type="number" min='0' max='2' value="<?php echo $hour;?>"  name="hour" class = "home-input" id="hour"  placeholder=""/>
							<label>Hours</label>
							<span class="focus-border"></span>
						</div>
					</div> 
					<div class="col-md-4">
						<div class="form-group input-effects"  id="minute-block">
							<input type="number" min='0' max='60' value="<?php echo $minute;?>"  name = "minute" class = "home-input" id="minute"  placeholder=""/>
							<label>Minutes</label>
							<span class="focus-border"></span>

						</div>
					</div> 
					<div class="col-md-4">
						<div class="form-group input-effects"  id="second-block">
							<input type="number" min='0' max='60' value="<?php echo $second;?>"  name = "second" class = "home-input" id="second"  placeholder=""/>
							<label>Seconds</label>
							<span class="focus-border"></span>

						</div>
					</div> 
				</div>
			<?php }  ?>

		</div> 

		<script>
				// var myVideos = [];

				// window.URL = window.URL || window.webkitURL;

				// document.getElementById('webinarfile').onchange = setFileInfo;

				// function setFileInfo() {
				// 	var files = this.files;
				// 	myVideos.push(files[0]);
				// 	var video = document.createElement('video');
				// 	video.preload = 'metadata';

				// 	video.onloadedmetadata = function() {
				// 		window.URL.revokeObjectURL(video.src);
				// 		var duration = video.duration;
				// 		myVideos[myVideos.length - 1].duration = duration;
				// 		updateInfos();
				// 	}

				// 	video.src = URL.createObjectURL(files[0]);
				// }


				// function updateInfos() { 
				// 	var infos = document.getElementById('webinar-duration');
				// 	infos.textContent = "";
				// 	for (var i = 0; i < myVideos.length; i++) {
				// 		infos.textContent += " duration: " + myVideos[i].duration + '\n';
				// 	}
				// }
			</script>






			<div class="col-md-6">
				<div class="form-group input-effects">
					<input type="text" value="<?php echo stripslashes($fields_val['product_name']); ?>"  name = "product[product_name]" class = " home-input" id="name"  placeholder=""/>
					<label>product name</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
				<div class="form-group input-effects">
					<input  type="text" 
					name="product[product_price]" 
					value="<?php echo $fields_val['product_price']; ?>" 
					class = " home-input"
					oninput="this.value=this.value.replace(/[^0-9]/g,'');"
					id="price" 
					placeholder="Price">
					<label>Price (USD)</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			<!-- 	<div class="form-group input-group">
					<input type="text" 
					name="product[product_price]" 
					value="<?php echo $fields_val['product_price']; ?>" 
					class = "form-control form-input"
					oninput="this.value=this.value.replace(/[^0-9]/g,'');"
					id="price" 
					placeholder="Price">
					<div class="input-group-append">
						<span class="input-group-text form-input input-prepend">USD</span>
					</div>
				 
				</div> -->

				<?php $exp_date = date('Y-m-d'); ?>
				<div class="form-group">
					<label>
						<input <?php if (@$fields_val['save_enable'] != "" && @$fields_val['save_enable'] == 1) {
							echo "checked";
						} ?> type="checkbox" id="chk_chk" name="save_enable" class="border-dark-1" name="enable"> <span class="paragraph-dark-md-capital">allow coupons</span>
					</label>

				</div>
				<div class="form-group input-effects" id="coupon_management">
					<select name="product[enable_coupon]" id="cou" class="home-input" placeholder="">
						<option value="">Choose Coupon</option>
						<?php


						$coupons = $this->db->query('select * from dmd_coupon where user_id="'.$this->session->userdata('user_id').'" and expire_date>="'.$exp_date.'" and status="live" ');
						if($coupons->num_rows()>0){
							foreach($coupons->result_array() as $coupon){
								?>
								<option <?php if (@$fields_val['enable_coupon'] !=
									"" && @$fields_val['enable_coupon'] == $coupon['id']) {
									echo "selected";
								} ?> value="<?php echo $coupon['id']; ?>"><?php echo $coupon['name']; ?></option>
								<?php
							}
						}
						?>

					</select>
					<label>Choose Coupon</label>
					<span class="focus-border"></span>


				</div>

				<div class="form-group input-effects">

					<select name="product[product_visibility]" id="visibility" class="home-input" placeholder="Product Visibility">
						<option <?php if ($fields_val['product_visibility'] != "" && $fields_val['product_visibility'] ==
							"none") {
							echo "selected";
						} ?> value="none" class="form-input form-input-lg border-dark-1">product visibility</option>
						<option <?php if ($fields_val['product_visibility'] != "" && $fields_val['product_visibility'] ==
							"live") {
							echo "selected";
						} ?> value="live" class="form-input form-input-lg border-dark-1">Live</option>
						<option <?php if ($fields_val['product_visibility'] != "" && $fields_val['product_visibility'] ==
							"draft") {
							echo "selected";
						} ?> value="draft" class="form-input form-input-lg border-dark-1">Draft</option>
						<option <?php if ($fields_val['product_visibility'] != "" && $fields_val['product_visibility'] ==
							"archived") {
							echo "selected";
						} ?> value="archived" class="form-input form-input-lg border-dark-1">Archived</option>
					</select>
					<label>Product Visibility</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>


			</div> 
			<div class="col-md-12 editor ">
				<label for="short-description-editor" class=" heading-md-grey">Short description</label>
				<div class="form-group">
					<textarea name="product[product_short_description]"  maxlength="160" rows="10" class="form-control form-textarea "><?php echo $fields_val['product_short_description']; ?></textarea> 

					<!--<textarea style="display: none;"  id="put_short_desp"></textarea>-->
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div> 

			<div class="col-md-12 editor ">
				<label for="long-description-editor" class=" heading-md-grey">Long description</label>
				<div class="form-group">
					<textarea id="long-description-editor" class="form-control form-input border-dark-1"><?php echo
					$fields_val['product_long_description']; ?></textarea> 
					<textarea style="display: none;" name="product[product_long_description]" id="put_long_desp"></textarea>
					<div class="text-danger" id="error1"></div>
				</div>
			</div> 
			<div class="col-md-12">
				<h4 class="heading-md-grey"> Upsell<h4>
					<label for="upsell-product" class=" heading-md-grey">Select product for upsell</label>
					<div class="form-group">
						<select class="js-example-basic-multiple" style="width: 100%" name="upsel_product[]" multiple="multiple" id="upsell-product" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="product">
							<option value="" disabled class="form-input form-input-lg border-dark-1">Select product</option>
							<?php foreach ($product_name as $product) {
								?>
								<option <?php if (@in_array($product['id'], @$fields_val['upsel_product'])){echo "selected";} ?>  value="<?php echo $product['id']; ?>" class="form-input form-input-lg border-dark-1"><?php echo
								$product['product_name']; ?></option>
								<?php
							} ?>
						</select>
					</div>
				</div> 

				<div id="upselEditor" class="col-md-12 editor">
					<label for="upsell-editor" class="heading-md-grey">Up-sell Description</label>
					<div class="form-group">
						<textarea id="upsell-editor" class="form-control form-input border-dark-1"><?php echo
						$fields_val['upsel_description']; ?></textarea> 
						<textarea style="display: none;" name="product[upsel_description]" id="put_upsel_desp"></textarea>
						<div class="text-danger" id="error2"></div>
					</div>
				</div>

				<div class="col-md-12">
					<h4 class="heading-md-grey">Cross-Sell<h4>
						<label for="cross-sell-product" class=" heading-md-grey">Select product for Cross-sell</label>
						<div class="form-group ">
							<select class="js-example-basic-multiple" style="width: 100%" name="cross_sell_product[]" multiple="multiple" id="cross-sell-product" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="select product">
								<option value="" disabled class="form-input form-input-lg border-dark-1">Select product</option>
								<?php foreach ($product_name as $product) {
									?>
									<option <?php if (@in_array($product['id'], $fields_val['cross_sell_product'])){echo "selected";} ?>  value="<?php echo $product['id']; ?>" value="<?php echo $product['id']; ?>" class="form-input form-input-lg border-dark-1"><?php echo
									$product['product_name']; ?></option>
									<?php
								} ?>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div> 

					<div id="crosssellEditor" class="col-md-12 editor ">
						<label for="cross-sell-editor" class="heading-md-grey">Cross-sell Description</label>
						<div class="form-group ">
							<textarea id="cross-sell-editor" class="form-control form-input border-dark-1"><?php echo
							$fields_val['cross_sell_description']; ?></textarea> 
							<textarea style="display: none;" name="product[cross_sell_description]" id="put_cross_desp"></textarea>
							<div class="text-danger" id="error3"></div>
						</div>
					</div>

					<div class="col-md-12 editor ">
						<label for="license" class="heading-md-grey">End User license Agreement</label>
						<div class="form-group">
							<textarea id="license-editor" class="form-control form-input border-dark-1"><?php echo
							$fields_val['license_editor']; ?></textarea>
							<textarea style="display: none;" name="product[license_editor]" id="put_license_desp"></textarea> 
							<div class="text-danger" id="error4"></div>
						</div>
					</div>



					<div class="col-md-12 mx-auto text-center">
									<!-- <p class="paragraph-text-sm-grey"> Available Memory <?= (float)$this->user_model->check_memory("size") * 1024; ?> Mbs</p>
										<div class="nauk-info-connections text-center"> -->
											<input value="save" style="display: none;" id="submit_pro" type="submit" name="create_product" class="btn-sm-blue btn" />
											<!--<input value="Save" onclick="get_descrip()" type="button" class="btn-sm-blue btn" />-->
											<a class="btn-form btn" onclick="get_descrip()"><span class="tick">Save</span></a>	
										</div>
									</div>	

								</form>
							</div>	
						</div>	
					</div>	

					<script>
						$(document).ready(function() {
							$('#duration-row').attr('disabled', 'disabled');
							if($("#chk_chk").is(':checked')){
								$('#cou').removeAttr('disabled');
								$("#chk_chk").val(1);
								$("#coupon_management").css("background-color", "rgb(255,255,255)");
							}else{
								$('#cou').attr('disabled', 'disabled');
								$("#chk_chk").val(0);
								$("#coupon_management").css("background-color", "rgb(240,240,240)");
							}

							$("#chk_chk").click(function(){
								if($("#chk_chk").is(':checked')){
									$('#cou').removeAttr('disabled');
									$("#chk_chk").val(1);
									$("#coupon_management").css("background-color", "rgb(255,255,255)");
								}else{
									$('#cou').attr('disabled', 'disabled');
									$("#chk_chk").val(0);
									$("#coupon_management").css("background-color", "rgb(240,240,240)");
								}
							});

								//$("#short-description-editor").Editor();
								$("#long-description-editor").Editor();
								$("#upsell-editor").Editor();
								$("#cross-sell-editor").Editor();
								$("#license-editor").Editor();

								$(".Editor-editor").on("keypress", function(e){
									var chars = $("#long-description-editor").Editor("getCharCount");
									
									if(chars >= 2000){
										if(e.keyCode != 8){
											$("#long-description-editor").Editor("showMessage","error1","chars more than 2000 are not allowed");
											return false;
										}
									}

									chars = $("#upsell-editor").Editor("getCharCount");
									
									if(chars >= 2000){
										if(e.keyCode != 8){
											$("#long-description-editor").Editor("showMessage","error2","chars more than 2000 are not allowed");
											return false;
										}
									}

									chars = $("#cross-sell-editor").Editor("getCharCount");
									
									if(chars >= 2000){
										if(e.keyCode != 8){
											$("#long-description-editor").Editor("showMessage","error3","chars more than 2000 are not allowed");
											return false;
										}
									}

									chars = $("#license-editor").Editor("getCharCount");
									
									if(chars >= 2000){
										if(e.keyCode != 8){
											$("#long-description-editor").Editor("showMessage","error4","chars more than 2000 are not allowed");
											return false;
										}
									}
								});



								var product_long_description = "<?php echo
								addslashes($fields_val['product_long_description']); ?>";
								$("#long-description-editor").Editor("setText", product_long_description);
								var upsel_description = "<?php echo addslashes(@$fields_val['upsel_description']); ?>";
								$("#upsell-editor").Editor("setText", upsel_description);
								var cross_sell_description = "<?php echo
								addslashes(@$fields_val['cross_sell_description']); ?>";
								$("#cross-sell-editor").Editor("setText", cross_sell_description);
								var license_editor = "<?php echo addslashes($fields_val['license_editor']); ?>";
								$("#license-editor").Editor("setText", license_editor);

							});
						function get_descrip(){
                              //$("#put_short_desp").html($("#short-description-editor").Editor("getText"));

                              $("#put_long_desp").html($("#long-description-editor").Editor("getText"));
                              $("#put_upsel_desp").html($("#upsell-editor").Editor("getText"));
                              $("#put_cross_desp").html($("#cross-sell-editor").Editor("getText"));
                              $("#put_license_desp").html($("#license-editor").Editor("getText"));
                              $("#submit_pro").click();
                          }
                      </script>
                      <script type='text/javascript'>
                      	function upload_img(){
                      		$('#imfile').click();
                      	}
                      	function readURL(input) {
                      		//if (input.files && input.files[0]) {
                      			var reader = new FileReader();
                      			reader.onload = function(e) {
                      				$('#imagePreview').css('background-image', 'url('+e.target.result +')');
                      				$('#imagePreview').css('background-repeat', 'no-repeat');
                      				$('#imagePreview').css('background-size', 'cover');
                      				$("#put_long_desp").html($("#long-description-editor").Editor("getText"));
                      				$("#put_upsel_desp").html($("#upsell-editor").Editor("getText"));
                      				$("#put_cross_desp").html($("#cross-sell-editor").Editor("getText"));
                      				$("#put_license_desp").html($("#license-editor").Editor("getText"));
                      				$("#submit_pro").click();
                      			}
                      		}
                      	</script>
                      	<script type='text/javascript'>
                      		function upload_img(){
                      			$('#imfile').click();
                      		}
                      		function readURL(input) {
                      			if (input.files && input.files[0]) {
                      				var reader = new FileReader();
                      				reader.onload = function(e) {
                      					$('#imagePreview').css('background-image', 'url('+e.target.result +')');
                      					$('#imagePreview').css('background-repeat', 'no-repeat');
                      					$('#imagePreview').css('background-size', 'contain');
                      					$('#imagePreview').css('background-position', 'center');
                                        //$('#imagePreview').attr('src', e.target.result);
                                        $('#imagePreview').hide();
                                        $('#imagePreview').fadeIn(650);

                                        //
                                        var imageSrc = document
                                        .getElementById('imagePreview').style.backgroundImage.replace(/url\((['"])?(.*?)\1\)/gi, '$2');                                        
                                        var image = new Image();
                                        image.src = imageSrc;
                                        var width = image.width;
                                        height = image.height;
                                        // if ((height < 300) || (width < 300))
                                        // {
                                        // 	$('#imfile').val('');
                                        // 	$('#imagePreview').css('background-image', 'url()');
                                        // 	$('#image-error').text('Height and Width should not be greater than 300px');
                                        // }
                                        // else{
                                        // 	$('#image-error').text('');
                                        // }
                                    }
                                    reader.readAsDataURL(input.files[0]);
                                    $("#old_image").val("");
                                }
                            }
                            //$("#imageUpload").change(function() {
//                                readURL(this);
//                            });
function preview_image(eventt){
	readURL(eventt);
}

$(document).ready(function() {
	$('#put_upsel_desp').attr('disabled','disabled');
	$('#put_cross_desp').attr('disabled','disabled');
	var upsellEditor = $("#upselEditor");
	upsellEditor.hide();
	
	var upSell = $('#upsell-product');
	upSell.select2();
	upSell.on("change",function(){
		//console.log($(this).val());
		if($(this).val()){
			upsellEditor.show();
			$('#put_upsel_desp').removeAttr('disabled','disabled');
		}else{
			upsellEditor.hide();
			$('#put_upsel_desp').attr('disabled','disabled');
		}
	});

	var crosssellEditor = $("#crosssellEditor");
	crosssellEditor.hide();
	var crossSell = $('#cross-sell-product');
	crossSell.select2();
	
	crossSell.on("change",function(){
		//console.log($(this).val());
		if($(this).val()){
			crosssellEditor.show();
			$('#put_cross_desp').removeAttr('disabled','disabled');
		}else{
			crosssellEditor.hide();
			$('#put_cross_desp').attr('disabled','disabled');
		}
	});
});

</script>

<script type='text/javascript'>
	function upload_webinar(){
		$('#webinarfile').click();
	}
	function readWebinarURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#webinar-video').hide();
				$('#webinar-video').fadeIn(650);

				$('#webinar-video').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			$("#old_webinar").val("");

			$('#duration-row').removeAttr('disabled');
			



		}
	}

	function preview_webinar(eventt){
		readWebinarURL(eventt);
	}

</script>
