<div class="row main-body mx-auto ">
  <?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>
  <div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

    <div class="row">

      <div class="col-md-12 inner-body-head"><!-- full block start-->
        <div class="nauk-info-connections">
          <div class="page-header">
            <div class="pull-left">
              <h2 class="heading-lg-green">Pages</h2>
              <p class="paragraph-text-sm-grey">Manage pages content.</p>
            </div>
            <div class="pull-right">
             <!-- <a href="<?php //echo base_url('pages/add_new_pages') ?>" class="btn default-btn-green"><i class="fa fa-plus"></i> Add page</a> -->
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div><!-- block end-->

    </div>
    <div class="row">
      <div class="col-md-12 inner-body-head"><!-- full block start-->

        <?php
        $message = $this->session->flashdata('message');
        if ($message) { ?>
          <div class="alert alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?php echo $message; ?>
          </div>
        <?php } ?>

        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Pages</th>
              <th>Slug</th>
              <th>Status</th>
              <th class="nosort">Action</th>

            </tr>
          </thead>
          <tbody>
           <?php  
           foreach($menu_items as $item):
            if (!(empty($item->title))){
              if(!($item->slug == "support")){
              ?>
              <tr id="row-<?php echo $item->id; ?>">
                <td><?php echo $item->title; ?></td>
                <td><?php echo $item->slug; ?></td>
                <td><?php if ($item->status == 1) {
                  echo "Active";
                } if ($item->status == 0) {
                  echo "Inactive";
                }?></td>
                <td>
                  <a href="<?php echo base_url('pages/edit/') . $item->id; ?>" class="btn default-btn-grey"><i class="fa fa-pencil"></i> Edit </a>
                  <!-- <a href="<?php //echo base_url('pages/delete/') . $item->id; ?>" class="btn default-btn-red"><i class="fa fa-trash"></i> Delete</a> -->
                </td>
              </tr>
            <?php }
                }
          endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>


<script type="text/javascript">

  // remove action sorting
  /*$('#datatable-responsive').dataTable({ 
    columnDefs: [{ orderable: false, targets: 'nosort'}]
  });*/

</script> 