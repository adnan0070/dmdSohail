<div class="row main-body mx-auto ">

	<?php $this->load->view('front_pages/dashboard/admin_dash_left');
	$store = $this->db->query('select * from settings');
	$store = $store->row_array();
	?>

	<div class="col-md-9 inner-body dashboard"> 

		<div class="row">


			<div class="col-md-12 inner-body-head">
				<?php
				$success_msg = $this->session->flashdata('success_msg');
				$error_msg   = $this->session->flashdata('error_msg');

				if ($success_msg) {
					?>

					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $success_msg; ?>
					</div>

					<?php
				}
				if ($error_msg) {
					?>


					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $error_msg; ?>
					</div>

					<?php
				}
				?>
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Tutorials</h2>
							<p class="paragraph-text-sm-grey">Manage tutorial videos and its content for subscribers.</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>



		</div>

		<form method="post" enctype="multipart/form-data" action="<?php echo base_url('tutorial/add'); ?>">
			<div class="row clearfix form">
				<div class="col-md-12">
					<label class="heading-md-grey">Title (max 50 character)</label>
					<div class="form-group">
						<input type="text"  maxlength="50" value="<?php echo $this->input->post('title'); ?>" name = "title" required class = "form-control form-input border-dark-1"  placeholder="Title of tutorial"/>
					</div>
				</div>
				


				<div class="col-md-12 editor">
					<label for="footer-left-text" class="heading-md-grey">Description (max 160 character)</label>
					<div class="form-group">
						<textarea maxlength="160" name="description" rows="10"  class="form-control form-textarea border-dark-1" placeholder ="Write something about tutorial..." required><?php echo $this->input->post('description'); ?></textarea>
					</div>
				</div>






				<div class="col-md-12 text-center">

					<div class="nauk-info-connections block-file-upload block-border-dotted text-center">

						<h3 class="heading-sm-grey text-center">Upload  video</h3>
						<div class="form-group">

							<input accept="video/mp4,video/x-m4v,video/*" name="mediaFile" required type="file" id="fileInputhome" class="file-input"  placeholder="Select from computer">

							<img class="btn-upload-file" src="<?php echo base_url(); ?>site_assets/images/btn-upload-file.png">
						</div>
						<div style="" class="form-group">
							<span class="paragraph-sm-grey" id="putfvalhome"></span>

						</div>

						<div style="display: none;" class="form-group">

							<input type="checkbox" class="border-dark-1" name="enable"> 
							<span class="paragraph-sm-grey">You can also drag and drop files.</span>

						</div>
					</div> 
				</div> 
				<input name="submitted" type="hidden" value="add">
				<div class="col-md-12 text-center">
					<div class="form-footer">

						<input class="default-btn-green btn"  value="Save" type="submit">

					</div>
				</div>
			</div>
		</form>

	</div>

</div>
<script>
	document.getElementById('fileInputhome').onchange = function () {
		$("#putfvalhome").text(this.value);
		$("#old_image_home").val("");
	}

</script>