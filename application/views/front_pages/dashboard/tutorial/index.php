	
<div class="row main-body mx-auto ">
	<?php 
	if ($this->session->userdata('user_role') == 2){
		$this->load->view('front_pages/dashboard/admin_dash_left'); 
	}else{
		$this->load->view('front_pages/dashboard/dash_left'); 
	}
	?>

	<div class="col-md-9 inner-body dashboard"> 

		<?php if ($this->session->userdata('user_role') == 2): ?>
			<div class="container-fluid main-container pricing-section">
				<div class="row row-fixed">
					<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
						<div class="section-content-block text-center">
							<div class="head"	>
								<h1 class="heading-level-1">Tutorials for subscribers </h1>
							</div>
							<div class="content">
								<p class="text-level-1">
									Guides and step-by-step tutorials to help subscribers get the most out of Digital Media Deliveries.

								</p>
							</div>
						</div>
					</div>
					<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center">
						<div class="block-btn">
							<a href="<?php echo base_url('tutorial/add'); ?>" class="btn-form btn">Add Tutorials</a>
						</div>
					</div>
					<div class="clearfix"></div>

				</div>
			</div>

			<?php else: ?>
				<div class="container-fluid main-container pricing-section">
					<div class="row row-fixed">
						<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
							<div class="section-content-block text-center">
								<div class="head"	>
									<h1 class="heading-level-1">Popular guides and tutorials </h1>
								</div>
								<div class="content">
									<p class="text-level-1">
										Know More, Do More with Digital Media Deliveries, Guides and step-by-step tutorials to help you get the most out of Digital Media Deliveries.

									</p>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>

					</div>
				</div>
			<?php endif; ?>


			<?php
			if($this->session->flashdata('error_msg')){
				?>
				<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
				</div>

				<?php
			}
			if($this->session->flashdata('success_msg')){
				?>
				<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
				</div>
			<?php } ?>

			<div class="container-fluid">
				<div class="row row-stretched">

					<?php if (count($tutorials) > 0): 
						foreach($tutorials as $res):  ?>
							<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 resource-block"  style="margin-bottom:100px; ">
								<div class="resource-img-block text-center" style="    max-width: 500px !important;">
									<div class="video-div">
										<video controls style="    height: 275px !important;">
											<source  src="<?php echo base_url();?>site_assets/tutorial/<?php echo $res['file'];?>" type="video/mp4">
											</video>
										</div>
										<div class="aaaa resource-content-block">
											<div class="head text-left">
												<h3 class="resource-title"><?php echo $res['title'];?></h3>
											</div>
											<div class="content text-left" style="min-height:90px !important;">
												<p class="resource-text" style="font-weight: 400;">
													<?php echo $res['description'];?>
												</p>
											</div>
											<?php if ($this->session->userdata('user_role') == 2): ?>
												<div class="block-btn">
													<a href="<?php echo base_url('tutorial/edit/'.encode($res['id'])); ?>" class="btn-empty-md btn">Edit</a>
													<a href="<?php echo base_url('tutorial/delete/'.encode($res['id'])); ?>" class="btn-empty-md btn">Delete</a>
												</div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							<?php endforeach; endif; ?>


							<div class="clearfix"></div>
						</div>
					</div>


				</div>
			</div>
