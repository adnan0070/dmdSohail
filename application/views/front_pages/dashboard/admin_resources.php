<div class="row main-body mx-auto ">
	<?php 
	$this->load->view('front_pages/dashboard/admin_dash_left'); 
	//print_r($users);
	?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<?php
			if($this->session->flashdata('error_msg')){
				?>
				<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
				</div>

				<?php
			}
			if($this->session->flashdata('success_msg')){
				?>
				<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
				</div>
			<?php } ?>

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Resources</h2>
							<p class="paragraph-text-sm-grey">Manage all your resources content.</p>
						</div>
						<div class="pull-right">
							<a class="btn-sm-default btn pricing-btn"  href="<?php echo base_url('resources/add'); ?>"><i class="fa fa-plus"></i> Add</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
		</div>
		<div class="row">
			
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<!-- <div class="input-group">
					<input type="text" class="form-control" placeholder="Search by user, store name here ...." >
					<div class="input-group-append">
						<button class="btn btn-outline-secondary btn-search-list" type="button">Search</button>
					</div>
				</div> -->
				<table id="cats" class="table table-bordered table-striped" style="width:100%">
					<thead>
						<tr>
							<th>Name</th>
							<th>Title</th>
							<th>Media</th>
							<th>label</th>
							<th>Url</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						if ($resources) {
							foreach ($resources as $res) {
								?>
								<tr>
									<td><?php echo $res['name']; ?></td>
									<td><?php echo $res['title']; ?></td>
									<td>
										<?php 
										if($res['file_type'] == 1){
											echo 'Image';
										}else{
											echo 'Video';
										}
										?>
									</td>
									<td><?php echo $res['label']; ?></td>
									<td><a target="_blank" href="<?php echo $res['url']; ?>" class="btn default-btn-grey">Visit</a></td>
									<td>
										<a href="<?php echo base_url('resources/edit/'.encode($res['id'])); ?>" class="btn default-btn-grey">Edit</a>
										<a href="<?php echo base_url('resources/delete/'.encode($res['id'])); ?>" class="btn default-btn-grey">Delete</a>
									</td>
								</tr>

								<?php
							}
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>





<script type="text/javascript">
	$(document).ready(function(){
		$('#cats').DataTable({
			//"pagingType": "full_numbers",
			"ordering": false,
			//"searching": true,
			"bLengthChange": false,
			"bInfo": false,
	        //"dom": '<"top"i>rt<"bottom"flp><"clear">'
	    });
	});
</script>

