<div class="row main-body mx-auto ">

	<?php
	$left = 'front_pages/dashboard/dash_left';
	$action = '/store/createPM/authorize';

	$user_role = $this->session->userdata('user_role');
	if($user_role == 2){
		$left = 'front_pages/dashboard/admin_dash_left';
		$action = '/admin/create_pm/authorize';
	}
	$this->load->view($left); 

	?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">
			<div id="showError"></div>

			<?php 
			if($this->session->flashdata('create_pm')){
				?> 
				<div class="alert alert-info alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Info!</strong> <?php echo $this->session->flashdata('create_pm'); ?>
				</div>
			<?php } ?>

			<?php 
			if(isset($pm)){
				$config = json_decode($pm["configuration"]);
							/*echo '<pre>';
							print_r($pm);
							echo '</pre>';*/
						}
						?>

						<div class="col-md-12 inner-body-head"><!-- full block start-->
							<div class="nauk-info-connections">
								<div class="page-header">
									<div class="pull-left">
										<h2 class="heading-lg-green">authorize.net configuration</h2>
										<p class="paragraph-text-sm-grey">Integrate payment methods for getting paid.</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div><!-- block end-->
					</div>

					<form action="<?php echo $action; ?>" id="confAuth" method="post">

						<div class="row clearfix">						
							<div class="col-md-12">
								<div class="form-group input-effects">
									<input type="text"
									name = "loginID"
									class = "home-input validThis"
									id="login-id"
									placeholder=""
									value="<?php if(isset($config->loginID)){echo $config->loginID;} ?>"
									/>
									<label>login id</label>
									<span class="focus-border"></span>
									<!--<span class="text-danger">Validation error</span>-->
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group input-effects">
									<input type="text"
									name = "transactionKey"
									class = "home-input validThis"
									id="transaction-key"
									placeholder=""
									value="<?php if(isset($config->transactionKey)){echo $config->transactionKey;} ?>"
									/>
									<label>transaction key</label>
									<span class="focus-border"></span>
									<!--<span class="text-danger">Validation error</span>-->
								</div>
							</div> 

							<div class="col-md-12">
								<div class="form-group input-effects">
									<input type="text"
									name="checkoutLabel"
									class = "home-input validThis"
									id="checkout-label"
									placeholder=""
									value="<?php if(isset($config->checkoutLabel)){echo $config->checkoutLabel;} ?>"
									/>
									<label>checkout label</label>
									<span class="focus-border"></span>
									<!--<span class="text-danger">Validation error</span>-->
								</div>
							</div> 



							<div class="col-md-12">
								<p class="paragraph-text-sm-grey"> Your MailChimp API key is available
									at Account, API key & Authorized apps in your MialChimp account. 
								</p>
							</div> 
							<div class="col-md-12">
								<ul class="list-inline ">
									<li class="list-inline-item form-checkbox">
										<input type="checkbox" class="border-dark-1" name="visa" <?php if(isset($config->visa)){echo "checked";} ?>> <span class="paragraph-dark-md-capital">visa</span>
									</li>
									<li class="list-inline-item form-checkbox">
										<input type="checkbox" class="border-dark-1" name="mastercard" <?php if(isset($config->mastercard)){echo "checked";} ?>> <span class="paragraph-dark-md-capital">Mastercard</span>
									</li>

									<li class="list-inline-item form-checkbox">
										<input type="checkbox" class="border-dark-1" name="ae" <?php if(isset($config->americanExpress)){echo "checked";} ?>> <span class="paragraph-dark-md-capital">American express</span>
									</li>

									<li class="list-inline-item form-checkbox">
										<input type="checkbox" class="border-dark-1" name="dc" <?php if(isset($config->dinerClub)){echo "checked";} ?>> <span class="paragraph-dark-md-capital">diner's club</span>
									</li>

									<li class="list-inline-item form-checkbox"> 
										<input type="checkbox" class="border-dark-1" name="status" <?php if(isset($pm) && $pm['status']){echo "checked";} ?>> <span class="paragraph-dark-md-capital">Enabled</span>
									</li>

									<li class="list-inline-item form-checkbox">
										<input type="checkbox" class="border-dark-1" name="default" <?php if(isset($pm) && $pm['default']){echo "checked";} ?>> <span class="paragraph-dark-md-capital">Default</span>
									</li>
								</ul>
							</div> 

							<div class="form-footer">
								<?php if(isset($pm)){ ?>
									<input type="hidden" name="pm_id" value="<?php echo $pm['id']; ?>" />
								<?php } ?>
								<button id="sbtAuth" class="btn-form btn">update</button>
							</div>

						</div>

					</form>



				</div><!-- inner-body-end-->

			</div>

		</div>

		<script type="text/javascript">
			$("#sbtAuth").on("click", function (e) {
				e.preventDefault();
				formValidate('confAuth');
			});
		</script>