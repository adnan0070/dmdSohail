<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

    <?php 

    if($this->session->flashdata('error_msg')){
      ?>
      <div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
      </div>

      <?php
    }

    if($this->session->flashdata('success_msg')){
      ?>

      <div class="alert alert-success alert-dismissible" style="margin-top: 50px">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
      </div>


    <?php } ?>




    <div class="row">

      <div class="col-md-12 inner-body-head"><!-- full block start-->
        <div class="nauk-info-connections">
          <div class="page-header">
            <div class="pull-left">
              <h2 class="heading-lg-green"><?php echo $action; ?> Categories</h2>
              <p class="paragraph-text-sm-grey">Manage all your blog categories.</p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div><!-- block end-->

    </div>


    <div class="row">
     <div class="col-md-12 inner-body-head"><!-- full block start-->
      <div class="panel-heading"><a href="<?php echo site_url('admin/add'); ?>" class="glyphicon glyphicon-arrow-right pull-right"></a></div>
      <form method="post" novalidate="" id="" action="<?php if($action=='Edit'){ echo 'edit_Category/'; } else { echo 'add/'; } ?>" enctype="multipart/form-data" class="form-horizontal form-label-left">
       <?php 
               // $action = 'Add';
       if($action='Edit'){
         ?>
         <input type="hidden" name="id" value="<?php if(isset($_GET['id'])) { echo $_GET['id']; }?>">
       <?php } ?>

       <div class="item form-group">    
        <div class="col-md-6 col-sm-6 col-xs-12">
        </label>
        <input type="text"  id="title" required="required" name="title" placeholder="title" value="<?php echo !empty($post['title'])?$post['title']:''; ?>" class="form-control form-input col-md-7 col-xs-12">
      </div>
    </div>

    <div class="ln_solid"></div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <input type="submit" href="" name="postSubmit" class="btn default-btn-blue" value="Submit"/>
        <a href="<?php echo base_url().'admin/categories'; ?>" class="btn default-btn-grey" type="button">Cancel</a>
      </div>
    </div>

  </form>
</div>
</div>
</div>
</div>