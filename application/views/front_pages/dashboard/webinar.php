<div class="row main-body mx-auto ">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>			
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<div class="col-md-12">
				<?php

				$error_msg = $this->session->flashdata('error_msg');
				if ($error_msg) {
					?>

					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo str_replace("_"," ",$error_msg); ?>
					</div>
					<?php
				}
				?>
			</div>
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">

							<h2 class="heading-lg-green">Add Webinar video</h2>
							<p class="paragraph-text-sm-grey">Send webinar link to your customer, that will redirect this to your product with button linked to your store.</p>


						</div>
					</div>
				</div>

			</div>
		</div>
		<?php

		?>
		<form method="post" enctype="multipart/form-data" action="<?php echo
		base_url('Tutorial/add_webinar/'.$code); ?>">
		<div class="row form">
			<div class="col-md-2">
			</div>
			<div class="col-md-8">


				<div  class="nauk-info-connections block-file-upload block-border-dotted text-center" style="padding:0px;  max-height:163px;">

					<div class="form-group ">
						<video id="webinar-video" controls style="display:none; width:100%;max-height:163px !important;  ">
							<source src="" type=""/>
						</video>
						<input accept="video/mp4,video/x-m4v,video/*" style="display: none;"  id="webinarfile" type="file" value="" name="webinarfile" class="file-input-product" onchange="preview_webinar(this)">
					</div>
				</div> 
				<div class="form-group text-center" >
					<span id ="webinar-error" class="text-danger"> </span>
					<input type="button" onclick="upload_webinar()" value="Upload Webinar" class="btn-sm-blue btn" />
				</div>
				<input type="hidden" value="<?php echo $code;?>" name="code">
				<div class="row" >
					<div class="col-md-12 text-center">
						<div class="form-group">
							<div class="page-header">
								<div class="">
									<p class="paragraph-text-sm-grey">Time for purchase /sign up button to appear required.</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row" id='duration-row'>
					<div class="col-md-4">
						<div class="form-group input-effects"  id="hour-block">
							<input type="number" min='0' max='2' value="0"  name="hour" class = "home-input" id="hour"  placeholder=""/>
							<label>Hours</label>
							<span class="focus-border"></span>
						</div>
					</div> 
					<div class="col-md-4">
						<div class="form-group input-effects"  id="minute-block">
							<input type="number" min='0' max='60' value="0"  name = "minute" class = "home-input" id="minute"  placeholder=""/>
							<label>Minutes</label>
							<span class="focus-border"></span>

						</div>
					</div> 
					<div class="col-md-4">
						<div class="form-group input-effects"  id="second-block">
							<input type="number" min='0' max='60' value="0"  name = "second" class = "home-input" id="second"  placeholder=""/>
							<label>Seconds</label>
							<span class="focus-border"></span>

						</div>
					</div> 
				</div>


			</div> 
			<div class="col-md-2">
			</div>

			<div class="col-md-12 mx-auto text-center">

				<input value="save" style="display: none;" id="submit_pro" type="submit" name="create_product" class="btn-sm-blue btn"/>
				<a class="btn-form btn" onclick="get_descrip()"><span class="tick">Save</span></a>	

				<a class="btn-form btn" href="<?php echo base_url('Product/PurchaseAction/'.$code); ?>"><span class="tick">Skip</span></a>
			</div>
		</div>	

	</form>
</div>	





<script type='text/javascript'>

	function get_descrip(){
		
		$("#submit_pro").click();
	}

	function upload_webinar(){
		$('#webinarfile').click();
	}
	function readWebinarURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#webinar-video').hide();
				$('#webinar-video').fadeIn(650);

				$('#webinar-video').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			$("#old_webinar").val("");

			$('#duration-row').removeAttr('disabled');
		}
	}

	function preview_webinar(eventt){
		readWebinarURL(eventt);
	}

</script>
