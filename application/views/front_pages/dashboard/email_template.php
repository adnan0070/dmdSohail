<div class="row main-body mx-auto ">
 <?php $this->load->view('front_pages/dashboard/dash_left'); ?>

            
	<div class="col-md-9 inner-body dashboard email-template"><!-- inner-body-start-->
		<div class="row">

			<?php 

			 if($this->session->flashdata('emailMsg')){
			 	?> 
				<div class="alert alert-info alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Info!</strong> <?php echo $this->session->flashdata('emailMsg'); ?>
				</div>
			 <?php } ?>

			<!-- full block start-->
			<div class="col-md-12 inner-body-head">
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">E-mail template</h2>
							<p class="paragraph-text-sm-grey">Customize e-mail templates you created.</p>
						</div>
						<div class="pull-right" style="display: none;">
							<a class="btn-form btn" href="<?php echo base_url('store/createNewEmail');?>">Create New</a>
						</div>

						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
			
			<?php
			if($emails){
				foreach ($emails as $key => $value) {		
			?>
			<div class="col-md-12 email-template-block "><!-- full block start-->
				<div class="nauk-info-connections border-dark-1">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-md-grey"><?php echo $value["subject"]; ?></h2>
						</div>
						<div class="pull-right">
							<a class="btn-sm-blue btn" href="<?php echo base_url('store/emailTemplates/') . $value['id']; ?>">Settings</a>
						</div>
						<div class="clearfix"></div>

						<div class="pull-left">
							<p class="paragraph-text-sm-grey">
								<?php echo $value["content"]; ?>
							</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->

			<?php 
				}
			} 
			?>
			
		</div>
	</div><!-- inner-body-end-->
</div>