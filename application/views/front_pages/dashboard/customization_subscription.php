<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); 

	//get user data
	$user = $this->db->query('select dmd_users.*,store_package.* from dmd_users left join store_package on (dmd_users.id=store_package.user_id) where dmd_users.id="'.$this->session->userdata('user_id').'"');
	$user = $user->row_array();
	
	//get payment methods configrations of admin
	$admin = $this->db->query("select * from dmd_users where user_role = 2");
	$admin = $admin->row_array();
	$admin_id = $admin['id'];

	$keys_stripe = $this->db->query("select * from payment_methods where user_id = '".$admin_id."' and `name`='stripe'");
	
	$keys_stripe = $keys_stripe->row_array();

	$stkey = json_decode($keys_stripe['configuration'],'array');
	$pubkey = $stkey['publishableApiKey'];

	//store publish key in input
	
	
	/*$keys = $this->db->query("select * from payment_methods where user_id = '".$user['user_id']."' 
		and `default`='1'");
		$keys = $keys->row_array();*/
    //print_r($keys);

	/*$suburl = "";
	if($keys['name']=='authorize'){
		$suburl = "authorize_net_payment";
	}
	if($keys['name']=='stripe'){
		$suburl = "stripe_payment";
		$stripe = true;
		$stkey = json_decode($keys['configuration'],'array');
		$pubkey = $stkey['publishableApiKey'];
	}
	if($keys['name']=='paypal'){
		$suburl = "buypay";
	}*/

	?>

	<input type="hidden" id="getpub" value="<?php echo $pubkey; ?>">
	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			
			
			<?php if($this->session->flashdata('error_msg')){ ?>
				<div class="alert alert-info alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Info! </strong><?php echo $this->session->flashdata('error_msg'); ?>
				</div>
			<?php } ?>

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							
							<h2 class="heading-lg-green">Customization Packages</h2>
							<p class="paragraph-text-sm-grey">Purchase Package and send request for store customizations.</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
		</div><!-- inner-body-end-->

		<form id="payment" method="post">
			<div class="row clearfix form">
				<!--<div class="col-md-12">
			        <div class="form-group">
			        	<input type="text" name="creditcard" placeholder="Credit Card Number" class="form-control form-input border-dark-1" />
			        </div>
			    </div>

		        <div class="col-md-12">
		          	<div class="form-group">
		            	<input type="text" name="expiry" placeholder="expiry" class="monthpicker form-control form-input border-dark-1" />
		          	</div>
		        </div>

		        <div class="col-md-12">
		          	<div class="form-group">
		            	<input type="text" name="cvc" placeholder="cvc" class="form-control form-input border-dark-1" />
		          	</div>
		          </div>-->
<!-- 
		          <div class="col-md-12" style="text-align: center">
		          	<p class="heading-md-blue">Billing Information</p>
		          </div>



		          <div class="col-md-12">
		          	<div class="form-group input-effects">
		          		<input type="text" value="<?php echo $user['f_name']; ?>" name="f_name" placeholder="" class="validThis home-input" />
		          		<input type="hidden" value="subscription" name="subscription">
		          		<label>First Name</label>
		          		<span class="focus-border"></span>
		          		<span class="text-danger"></span>
		          	</div>
		          </div>

		          <div class="col-md-12">
		          	<div class="form-group input-effects">
		          		<input type="text" name="l_name" value="<?php echo $user['l_name']; ?>" placeholder="" class="validThis home-input" />
		          		<label>Last Name</label>
		          		<span class="focus-border"></span>
		          		<span class="text-danger"></span>
		          	</div>
		          </div> -->
		          <div class="col-md-6">
		          	<div class="form-group input-effects">
		          		<input type="hidden" value="" name="package" id="pack_add" /> 
		          		<input type="hidden" id="chk_def" value="<?php echo $user['package_id']; ?>" />  

		          		<select id="getam" onchange="change_pack(this.value)" class="validThis home-input">
		          			<?php
		          			$getAll = $this->db->query('select * from customization_packages');

		          			foreach($getAll->result_array() as $packages){
		          				?>
		          				<option value="<?php echo $packages['id']."|".$packages['pack_price']."|".$packages['pack_title']; ?>"><?php echo $packages['pack_title']; ?></option>	
		          				<?php    
		          			}
		          			?>

		          		</select>
		          		<label>Packages</label>
		          		<span class="focus-border"></span>
		          		<span class="text-danger"></span>
		          	</div>
		          </div>

		          <!-- <div class="col-md-12">
		          	<div class="form-group input-effects">
		          		<input type="text" name="company" class="validThis home-input" />
		          		<label>Company</label>
		          		<span class="focus-border"></span>
		          		<span class="text-danger"></span>
		          	</div>
		          </div>

		          <div class="col-md-12">
		          	<div class="form-group input-effects">
		          		<input type="text" name="address" placeholder="" class="validThis home-input" />
		          		<label>Street Address</label>
		          		<span class="focus-border"></span>
		          		<span class="text-danger"></span>
		          	</div>
		          </div>

		          <div class="col-md-12">
		          	<div class="form-group input-effects">
		          		<input type="text" name="city" placeholder="" class="validThis home-input" />
		          		<label>City</label>
		          		<span class="focus-border"></span>
		          		<span class="text-danger"></span>
		          	</div>
		          </div>

		          <div class="col-md-12">
		          	<div class="form-group input-effects">
		          		<input type="text" name="country" value="" placeholder="" class="validThis home-input" />
		          		<label>Country</label>
		          		<span class="focus-border"></span>
		          		<span class="text-danger"></span>
		          	</div>
		          </div>

		          <div class="col-md-12">
		          	<div class="form-group input-effects">
		          		<input type="hidden" name="amount" placeholder="amount" id="amount_add" />
		          		<input type="hidden" name="pack_title" placeholder="amount" id="name_add" />
		          		<input type="hidden" name="store_id" value="<?php echo $this->session->userdata('store_id'); ?>" placeholder="amount" />
		          		<input type="text" name="email" value="<?php echo $this->session->userdata('user_email'); ?>" class="validThis home-input" />
		          		<label>e-mail address</label>
		          		<span class="focus-border"></span>
		          		<span class="text-danger"></span>
		          	</div>
		          </div>
		      -->



		        <!--   <div class="col-md-12">
		          	<div class="form-group">
		          		<p class="heading-md-blue" style="margin:10px 0px 5px 0px;">Payment </p>
		          	</div>
		          </div> -->

		          <?php //echo '<pre>'; print_r($enable_pm); echo '</pre>'; ?>
		          <div class="col-md-6">
		          	<div class="form-group input-effects">
		          		<select type="text" onchange="changePayment()" id="changePM" placeholder="Payment methods" class="validThis home-input">
		          			<?php

		          			foreach ($enable_pm as $value) {
		          				$selected="";
		          				if($value["default"] == 1){
		          					$selected = "selected";
		          				}
		          				echo '<option '.$selected.' value="'.$value["name"] .'">'.$value["name"].'</option>';

		          			}
		          			?>

		          		</select>
		          		<label>methods</label>
		          		<span class="focus-border"></span>
		          		<span class="text-danger"></span>
		          	</div>
		          </div>

		          <div class="col-md-6">
		          	<div class="form-group stripe_payment" style="display:none;">
		          		<!-- <label for="card-element">Credit or debit card</label> -->
		          		<div id="card-element"></div>
		          		<div id="card-errors" role="alert"></div>
		          	</div>
		          </div>

		          <div class="authorize_show row" style="display:none;">
		          	<div class="col-md-6">
		          		<div class="form-group input-effects">
		          			<input type="text" name="street" placeholder="" class="home-input" />
		          			<label>Street</label>
		          			<span class="focus-border"></span>
		          			<span class="text-danger"></span>
		          		</div>
		          	</div>

		          	<div class="col-md-6">
		          		<div class="form-group input-effects">
		          			<input type="text" name="zip" placeholder="" class="home-input" />
		          			<label>Zip</label>
		          			<span class="focus-border"></span>
		          			<span class="text-danger"></span>
		          		</div>
		          	</div>

		          	<div class="col-md-6">
		          		<div class="form-group input-effects">
		          			<input type="text" name="creditcard" placeholder="" class="home-input" />
		          			<label>Credit Card Number</label>
		          			<span class="focus-border"></span>
		          			<span class="text-danger"></span>
		          		</div>
		          	</div>


		          	<div class="col-md-6">
		          		<div class="form-group input-effects">
		          			<input type="text" name="expiry" placeholder="" class="monthpicker  home-input" />
		          			<label>Expiry (MM/YY)</label>
		          			<span class="focus-border"></span>
		          			<span class="text-danger"></span>
		          		</div>
		          	</div>

		          	<div class="col-md-6">
		          		<div class="form-group input-effects">
		          			<input type="text" name="cvc" placeholder="" class="home-input" />
		          			<label>CVC</label>
		          			<span class="focus-border"></span>
		          			<span class="text-danger"></span>
		          		</div>
		          	</div>

		          </div>	


		          <div class="col-md-12 text-center">
		          	<div class="form-footer">
		          		<p class="text-danger" id="sub_error"></p>
		          		<div id="showError"></div>
		          		<?php
		          		if($enable_pm){ ?>
		          			<button type="submit" id="paySubmit" rel="external" class="btn btn-form" >Submit</button>
		          			<?php
		          		}else{
		          			echo '<p class="text-danger">No payment method setup by Admin</p>';
		          		}
		          		?>
		          	</div>
		          </div>

		      </div>
		  </form>
		  <input type="hidden" value="0" id="chk_load" />
		</div>
	</div>



	<script>
		$(document).ready(function(){
			change_pack();
			changePayment();
		});

		function change_pack(){
			var valpack = $("#getam").val();
			var valp = valpack.split("|");
			var pack = valp[0];
			var amountt = valp[1];
			$("#amount_add").val(amountt);
			$("#pack_add").val(pack);
			$("#name_add").val(valp[2]);
			$("#change_ammount").text("$"+amountt);
			$("#sub_error").html('');
		}

		function changePayment(){
			var method = $("#changePM").val();
			var action = "";
			if(method == "stripe"){
				action =  'stripe_subscription';
				$(".stripe_payment").show();
				$(".authorize_show").hide();
				$(".pay_authorize").show();
			}
			if(method == "paypal"){
				action =  'paypal_subscription';
				$(".stripe_payment").hide();
				$(".authorize_show").hide();
				$(".pay_authorize").show();
			}
			if(method == "authorize"){
				action =  'authorize_net_subscription';
				$(".authorize_show").show();
				$(".pay_authorize").show();
				$(".stripe_payment").hide();
			}

			$("#payment").attr("action","<?php echo base_url('customization/');?>"+action);
		}

	</script>

	<script src="https://js.stripe.com/v3/"></script>
	<script type="text/javascript">

	//var stripe = Stripe('<?php echo $pubkey; ?>');
	var getpub = $("#getpub").val();
	var stripe = Stripe(getpub);

	// Create an instance of Elements.
	var elements = stripe.elements();

	var style = {
		base: {
			color: '#32325d',
			lineHeight: '18px',
			fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
			fontSmoothing: 'antialiased',
			fontSize: '16px',
			'::placeholder': {
				color: '#aab7c4'
			}
		},
		invalid: {
			color: '#fa755a',
			iconColor: '#fa755a'
		}
	};

	var card = elements.create('card', {style: style});
	card.mount('#card-element');
	card.addEventListener('change', function(event) {
		var displayError = document.getElementById('card-errors');
		if (event.error) {
			displayError.textContent = event.error.message;
		} else {
			displayError.textContent = '';
		}
	});

	var form = document.getElementById('payment');
	form.addEventListener('submit', function(event) {
		event.preventDefault();

		var packageDetail = $("#getam").val();
		var package = packageDetail.split("|");
		
		if(package[0] == '12345'){
			$("#sub_error").html("Please select any package except default package.");
			return false;
		}

		    /*if($("#chk_def").val()==valp[0]){
		    	$("#sub_error").html("You Have Already Subscribed This Package Please Select Other Package");
				return false;
			}*/

			//$("#loading").css("display","block");
			var method = $("#changePM").val();
			if(method != "stripe"){
				//alert('not stripe');
				formValidate('payment');
				//form.submit();
			}else{
				stripe.createToken(card).then(function(result) {
					if (result.error) {
			      		// Inform the user if there was an error.
			      		var errorElement = document.getElementById('card-errors');
			      		errorElement.textContent = result.error.message;
			      		//console.log(result.error.message);
			      	} else {
			      		// Send the token to your server.
			      		stripeTokenHandler(result.token);
			      		//console.log(result.token);
			      	}
			      });
			}
		});

	function stripeTokenHandler(token) {
		var form = document.getElementById('payment');
		var hiddenInput = document.createElement('input');
		hiddenInput.setAttribute('type', 'hidden');
		hiddenInput.setAttribute('name', 'stripeToken');
		hiddenInput.setAttribute('value', token.id);
		form.appendChild(hiddenInput);
			//form.submit();
			formValidate('payment');
		}


	</script>