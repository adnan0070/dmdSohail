<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Purchases</h2>
							<p class="paragraph-text-sm-grey">Generate reports for all purchases and their status.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php //echo count($purchases['result']); ?>

		<form id="frm_filter">
			<div class="row form border-dark-1 filter-block" style="margin:20px 0px;">

				<div class="col-md-12 mx-auto">
					<div class="nauk-info-connections text-center">
						<label for="long-description-editor" class=" heading-md-grey">filter</label>
					</div>
				</div>

				<div class="col-md-6">

					<div class="form-group input-effects">
						<select  id="status" name="dmd_orders%status" class="home-input" placeholder="status">
							<option value="" class="form-input form-input-lg border-dark-1">status </option>
							<option value="paid" class="form-input form-input-lg border-dark-1">Paid </option>
							<option value="declined" class="form-input form-input-lg border-dark-1">Declined </option>
						</select>
						<label>Status</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>

				<div class="col-md-6">

					<div class="form-group input-effects">
						<select  id="product" name="dmd_order_items%product_id" class="home-input" placeholder="">
							<option value="" id="nochose" class="form-input form-input-lg border-dark-1"> Choose product</option>
							<?php
							foreach($purchases['products'] as $product){
								$selected = "";
								if($purchases['pid']!=""){
									$selected = 'selected';
									?>
									<script>
										$("#nochose").hide();
									</script>
									<?php  
								}
								?>
								<option <?php echo $selected; ?> value="<?php echo $product['product_code'];?>" class="home-input"><?php echo $product['product_name'];?></option>
								<?php
							}
							?>
						</select>
						<label>Choose product</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>



				<div class="col-md-6">
					<div class="form-group input-effects">
						<select  name="dmd_customers%email" class="home-input" placeholder="">
							<option class="form-input form-input-lg border-dark-1" value="" selected>Choose Email</option>
							<?php
							if($purchases['result']){
								foreach ($purchases['result'] as $key => $value) {
									$emails[] = $value["email"];
								}

								$email_list = array_unique($emails);

								foreach ($email_list as $cst_email) {
									?>
									<option class="form-input form-input-lg border-dark-1" value="<?php echo $cst_email; ?>">
										<?php echo $cst_email; ?>
									</option>
									<?php
								}
							}

							?>

						</select>
						<label>Choose Email</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>

						<!--<div class="col-md-6">
							<div class="form-group">
								<input type="text"  name = "purchaseID" class = "form-control form-input border-dark-1" id="purchase-id"  placeholder="purchase id"/>
								
							</div>
						</div> -->
						<div class="col-md-6">
							<div class="form-group input-effects">
								<input type="text"  name = "dmd_customers%f_name" class = "home-input"  placeholder=""/>
								<label>first name</label>
								<span class="focus-border"></span>
								<!--<span class="text-danger">Validation error</span>-->
							</div>
						</div> 
						<div class="col-md-6">
							<div class="form-group input-effects">
								<input type="text"  name = "dmd_customers%l_name" class = "home-input" id="last-name"  placeholder=""/>
								<label>last name</label>
								<span class="focus-border"></span>
								<!--<span class="text-danger">Validation error</span>-->
							</div>
						</div> 

						<div class="col-md-6">

							<div class="form-group input-effects">
								<select name="dmd_customers%shipping_country" id="shipping-country" class="home-input" placeholder="">
									<option value="" class="form-input form-input-lg border-dark-1">shipping country</option>
									<?php
									if($purchases['result']){
										foreach ($purchases['result'] as $key => $value) {
											$countries[] = $value["purchase_country"];
										}
										
										$country_list = array_unique($countries);

										foreach ($country_list as $cst_country) {
											?>
											<option class="form-input form-input-lg border-dark-1" value="<?php echo $cst_country; ?>">
												<?php echo $cst_country; ?>
											</option>
											<?php
										}
									}

									?>
								</select>
								<label>Shipping country</label>
								<span class="focus-border"></span>
								<?php //print_r($country_list); ?>
								<!--<span class="text-danger">Validation error</span>-->
							</div>
						</div>

						<div class="col-md-2  input-dates-small-lg">

							<div class="form-group input-effects">
								<input type="text" id="fromdate" name="from" class="datepicker home-input" placeholder="" />
								<label style="text-transform:lowercase;">mm/dd/yyyy</label>
								<span class="focus-border"></span>

								<!--<span class="text-danger">Validation error</span>-->
							</div>
						</div>

						<div class="col-md-2 text-center input-dates-small-sm input-effects">
							<label for="long-description-editor" class=" paragraph-text-md-grey" style="margin-top:10px;">To</label>
						</div> 
						<div class="col-md-2  input-dates-small-lg">

							<div class="form-group input-effects">
								<input type="text" name="to" id="todate" class="datepicker home-input" placeholder="" />
								<label style="text-transform:lowercase;">mm/dd/yyyy</label>
								<span class="focus-border"></span>
								
							</div>
						</div>


						<div class="col-md-12 mx-auto">
							<span id="todateError" class="text-danger"></span>
							<div class="nauk-info-connections text-center">
								<button type="submit" id="filter" class="btn-sm-blue btn-fix-style" >Apply filters</button>
								<a href="<?php echo base_url('product/purchase'); ?>" class="btn btn-sm-blue">clear filters</a>
							</div>
						</div>	


					</div>
				</form>


				<div class="row purchases">

					<div class="col-md-12 block"><!-- full block start-->
						<div class="nauk-info-connections border-green">
							<!-- Default form register -->
							
							<div class="page-header">
								<div class="pull-left">
									<p class="paragraph-text-sm-grey" style="padding-left:10px">
										<span id="purchase_tot"><?php if(isset($purchases['result'])){ echo count($purchases['result']) . ' Purchases'; } ?></span><span id="grand_tot"><?php if(isset($purchases['grand_sum']['gtotal']) && $purchases['grand_sum']['gtotal'] != 'undefined'){ echo '-Total Paid $' . $purchases['grand_sum']['gtotal'];} ?></span></p>
									</div>
									<div class="pull-right" style="padding-right:10px;">

										<a id="export" class="btn-sm-blue btn" href="#">Export</a>
									</div>
									<div class="clearfix"></div>
								</div>
								<table id="purchases" class="table table-bordered table-striped" style="width:100%">
									<thead class="back-border">
										<tr>
											<th scope="col"><p class="text-70 paragraph-text-sm-grey-bold">Order ID</p></th>
											<th scope="col"><p class="text-70 paragraph-text-sm-grey-bold">Date</p></th>
											<th scope="col"><p class="text-70 paragraph-text-sm-grey-bold">Amount</p></th>
											<th scope="col"><p class="text-70 paragraph-text-sm-grey-bold">Status</p></th>
											<th scope="col"><p class="text-70 paragraph-text-sm-grey-bold">Customer</p></th>
											<th scope="col"><p class="text-70 paragraph-text-sm-grey-bold">Email</p></th>
											<th scope="col"><p class="text-70 paragraph-text-sm-grey-bold">Country</p></th>
											<!-- <th scope="col">Delivery Status</th> -->
										</tr>
									</thead>
									<tbody id="tb">
										<?php
										$sum = 0;
										if(!isset($purchases['result'])){
											$purchases['result'] = array();
										}
										foreach($purchases['result'] as $purchase){
											$sum+=$purchase['grand_total'];
											?>
											<tr>
												<td class="paragraph-text-xs-grey"><p class="text-70"><?php echo $purchase['order_number']; ?></p></td>
												<td class="paragraph-text-xs-grey"><p class="text-70"><?php echo date('Y-m-d', strtotime($purchase['order_date'])); ?></p></td>
												<td class="paragraph-text-xs-grey"><p class="text-70">$<?php echo $purchase['grand_total']; ?></p></td>
												<td class="paragraph-text-xs-grey"><p class="text-70"><?php echo $purchase['status']; ?></p></td>
												<td class="paragraph-text-xs-grey "><p class="text-70 width-120 overflow-hidden"><?php echo $purchase['f_name'] . ' ' . $purchase['l_name']; ?></p></td>
												<td class="paragraph-text-xs-grey "><p class="width-120 overflow-hidden text-70"><?php echo $purchase['email']; ?></p></td>
												<td class="paragraph-text-xs-grey"><p class="text-70 width-120 overflow-hidden"><?php echo $purchase['purchase_country']; ?></p></td>
												<!-- <td class="paragraph-text-sm-grey"><?php //echo $purchase['deliver_status']; ?></td> -->
											</tr>
											<?php
										}
										?>

									</tbody>
								</table>


								<?php if(isset($links)){echo $links;} ?>


							</div>

						</div>

					</div>
				</div>

			</div>
			<script>
				$("#filter").on("click", function(e){
					e.preventDefault();

					var from = Date.parse($("#fromdate").val());
					var to = Date.parse($("#todate").val());
				//console.log(from + ", " + to);
				if(from > to){
					$("#todate").focus();
					$("#todateError").html("To date must be greater than From date.");
					return false;
				}

				$("#purchase_tot").html("");
				$("#grand_tot").html("");
				var dt = $("#frm_filter").serialize();
				//console.log(dt);
				$.ajax({
					type: "post",
					url: "/product/filter",
					data: dt,
					success: function(msg){
						console.log(msg);
						$("#tb").html(msg);
						if(msg != 'No records found!!'){
							var tot_price = $("#getto").val();
							if (tot_price > 0){
								// $("#grand_tot").html("-Total Paid $" + $("#getto").val());
								$("#grand_tot").html(" / Total Paid $" + tot_price);
							}else{
								$("#grand_tot").html(" / Total Paid $0");
							}

							var count = $("#purchase_count").val();
							if (count > 0){
								$("#purchase_tot").html(count + ' Purchases');
							}
							else{
								$("#purchase_tot").html('0 Purchases');
							}
						}


						$('div#purchases_paginate').hide();


					}
				});
			});

				function exportTableToCSV($table, filename) {
					var $rows = $table.find('tr:has(td)'),

	            tmpColDelim = String.fromCharCode(11), // vertical tab character
	            tmpRowDelim = String.fromCharCode(0), // null character

	            // actual delimiter characters for CSV format
	            colDelim = '","',
	            rowDelim = '"\r\n"',

	            // Grab text from table into CSV formatted string
	            csv = '"' + $rows.map(function (i, row) {
	            	var $row = $(row),
	            	$cols = $row.find('td');

	            	return $cols.map(function (j, col) {
	            		var $col = $(col),
	            		text = $col.text();
		                    		return text.replace(/"/g, '""'); // escape double quotes
		                    	}).get().join(tmpColDelim);
	            }).get().join(tmpRowDelim)
	            .split(tmpRowDelim).join(rowDelim)
	            .split(tmpColDelim).join(colDelim) + '"';

	            console.log(csv);

				// Deliberate 'false', see comment below
				if (false && window.navigator.msSaveBlob) {
					var blob = new Blob([decodeURIComponent(csv)], {
						type: 'text/csv;charset=utf8'
					});

		            // Crashes in IE 10, IE 11 and Microsoft Edge
		            window.navigator.msSaveBlob(blob, filename);

		        } else if (window.Blob && window.URL) {
					// HTML5 Blob
					var blob = new Blob([csv], { type: 'text/csv;charset=utf8' });
					var csvUrl = URL.createObjectURL(blob);

					$(this).attr({
						'download': filename,
						'href': csvUrl
					});
				} else {
        			// Data URI
        			var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        			$(this).attr({
        				'download': filename,
        				'href': csvData,
        				'target': '_blank'
        			});
        		}
        	}

        	$("#export").on("click", function(){
        		var args = [$('#purchases'), 'purchases.csv'];
        		exportTableToCSV.apply(this, args);
        	});
        	
        	$(document).ready(function(){
        		$('#purchases').DataTable({
        			//"pagingType": "full_numbers",
        			"ordering": false,
        			"searching": false,
        			"bLengthChange": false,
        			"bInfo": false,
        			"scrollY": true,
        			"scrollX": true,
        			"dom": '<"top"i>rt<"bottom"flp><"clear">'
					/*"fnDrawCallback": function(oSettings) {
				        if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
				            $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
				        }
				    }*/
				});

        		<?php if(count($purchases['result']) < 10 ): ?>
        			$('div#purchases_paginate').hide();
        		<?php endif; ?>
        	});
        </script>
        