<div class="row main-body mx-auto ">
  <?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>
  <div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

    <div class="row">

      <div class="col-md-12 inner-body-head"><!-- full block start-->
        <div class="nauk-info-connections">
          <div class="page-header">
            <div class="pull-left">
              <h2 class="heading-lg-green">Team Members</h2>
              <p class="paragraph-text-sm-grey">Manage team members.</p>
            </div>
            <div class="pull-right">
              <a href="<?php echo base_url('admin/add_team') ?>" class="btn default-btn-green"><i class="fa fa-plus"></i> Add Member</a>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div><!-- block end-->

    </div>

    <div class="row">
      <div class="col-md-12 inner-body-head"><!-- full block start-->

        <?php
        $message = $this->session->flashdata('message');
        if ($message) { ?>
          <div class="alert alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?php echo $message; ?>
          </div>
        <?php } ?>

      
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Designation</th>
              <th>Description</th>
              <th>Action</th>

            </tr>
          </thead>
          <tbody>
           <?php  
           if($teams){
            foreach($teams as $team){
              ?>
              <tr>
                <td>
                  <img src="<?php echo base_url('site_assets/team/') . $team['image']; ?>" width="50" height="50">
                  
                </td>
                <td><?php echo $team['name']; ?></td>
                <td><?php echo $team['designation']; ?></td>
                <td><p style="overflow: hidden; height: 50px; max-width: 200px; width: 100%;">
                  <?php echo $team['description']; ?></p>
                </td>
                <td>
                  <a href="<?php echo base_url('admin/add_team/') . $team['id']; ?>" class="btn default-btn-grey"><i class="fa fa-pencil"></i> Edit</a>
                  <a href="<?php echo base_url('admin/team_delete/') . $team['id']; ?>" class="btn default-btn-red"><i class="fa fa-trash"></i> delete</a>
                </td>
              </tr>
            <?php }
          } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>


<script type="text/javascript">

  // remove action sorting
  /*$('#datatable-responsive').dataTable({ 
    columnDefs: [{ orderable: false, targets: 'nosort'}]
  });*/

</script> 