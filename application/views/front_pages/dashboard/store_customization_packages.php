<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<!-- end pricing section 1 -->
		<div class="container-fluid main-container pricing-section">
			<div class="row">
				<div class="col-md-12 inner-body-head"><!-- full block start-->
					<div class="nauk-info-connections">
						<div class="page-header">
							<div class="pull-left">
								<h2 class="heading-lg-green">Customization offers</h2>
								<p class="paragraph-text-sm-grey">Lorem ipsum sit amet</p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div><!-- block end-->

			</div>

			<div class="row">
				<div class="col-md-12 inner-body-head"><!-- full block start-->
					<div id="showError"></div>
					<?php
					$message = $this->session->flashdata('message');
					if ($message) { ?>
						<div class="alert alert-info" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo $message; ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- pricing section 2 -->
		<div class="container-fluid ">
			<div class="row slider-row row-1100">
				<?php  foreach($packages as $package): ?>
					<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
						<div class="item resource-block">
							<div class="slider-block">
								<div class="slider-head">
									<h4 class="heading-level-4"><?php echo $package['pack_title']; ?></h4>
								</div>
								<div class="slider-price">
									<h1 class="heading-level-1"><span class="price-text-sm">$</span><?php echo $package['pack_price']; ?></h1>
								</div>
								<div class="slider-detail">
									<ul class="list-inline text-left">
										<?php $labels = explode("*",$package['pack_description']);

										for($i =0; $i < count($labels); $i++)
										{
											?>

											<li class="text-level-1">
												<?php echo $labels[$i]; ?>
											</li>

										<?php } ?>
									</ul>
								</div>
								<div class="block-btn">
									<a href="<?php echo base_url('customization/buy/'.encode($package['id'])); ?>" class="btn-sm-default btn pricing-btn">Purchase Now</a>
								</div>
							</div>
						</div>
					</div> 
				<?php endforeach;?>

				<div class="clearfix"></div>
			</div>
		</div>


	</div>
</div>