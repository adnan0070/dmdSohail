<div class="row main-body mx-auto ">
 <?php $this->load->view('front_pages/dashboard/dash_left'); ?>



			<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

				<div class="row">

					<?php 

					 if($this->session->flashdata('import_csv')){
					 	?> 
						<div class="alert alert-info alert-dismissible" style="margin-top: 50px">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Info!</strong> <?php echo $this->session->flashdata('import_csv'); ?>
						</div>
					 <?php } ?>

					<div class="col-md-12 inner-body-head"><!-- full block start-->
						<div class="nauk-info-connections">
							<div class="page-header">
								<div class="pull-left">
									<h2 class="heading-lg-green">import customers</h2>
									<p class="paragraph-text-sm-grey">Add multiple customers at a single time to your products. They will receive the file after the import is successful.</p>
									<br>
									<p class="paragraph-text-sm-grey"><a class="paragraph-text-sm-grey" href="<?php echo base_url('site_assets/samples/Import_Customers.xlsx'); ?>"><u><strong>Download</strong></u> </a>our template to import multiple customers at one time.</p>
								</div>

							</div>
						</div>
					</div>

				</div>
				
				<?php echo form_open_multipart('customer/import_file'); ?>

				<form action="" method="">
					<div class="row clearfix form">

						<div class="col-md-12 text-center">

							<div class="nauk-info-connections block-file-upload block-border-dotted text-center">
								
								<h3 class="heading-sm-grey text-center">Upload File</h3>
								<div class="form-group">
									
									<input type="file" id="customerfile" name="customerfile" class="file-input" placeholder="Select from computer">
									<!--<img id="output_image"/>-->
									<img class="btn-upload-file" src="<?php echo base_url(); ?>site_assets/images/btn-upload-file.png">
								</div>

								<div class="form-group">
									
									<!-- <input type="checkbox" class="border-dark-1" name="enable"> --> 
									<span id="label" class="paragraph-sm-grey"><?php if(isset($file[0]["button_label"])){ echo $file[0]["button_label"];} ?></span>

								</div>
								<!--<span class="text-danger">Validation error</span>-->
							</div> 
						</div> 




						<div class="form-footer">
							<button type="submit" class="btn-form btn">import</button>
							<!-- <a href="/customer/import_file" class="btn-sm-blue btn">import</a> -->
						</div>


					</div>

				</form>

			</div>
		
		<script type='text/javascript'>
		function preview_image(event)
		{
			var reader = new FileReader();
			reader.onload = function()
			{
				var output = document.getElementById('output_image');
				output.src = reader.result;
			}
			reader.readAsDataURL(event.target.files[0]);
		}

		document.getElementById('customerfile').onchange = function () {
          $("#label").text(this.value);
          //$("#old_file").val("");
          //alert('Selected file: ' + this.value);
        };
		</script>
		
		
		</div>