<div class="row main-body mx-auto ">

	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<div class="col-md-12">
				<?php
				$success_msg = $this->session->flashdata('success_msg');
				$error_msg = $this->session->flashdata('error_msg');
				$fields_val = array();

				$fields_val = $this->session->flashdata('fields');
				$save_id = "";
				if (isset($fields_val['save_id'])) {
					$save_id = $fields_val['save_id'];
				}
				$method = "add";

				if (isset($products[0]['id']) && $products[0]['id'] != '') {
					$fields_val = $products[0];
					$save_id = $products[0]['pack_code'];
					$method = "update";


				}
				if (isset($fields_val['save_id']) && $fields_val['save_id'] != "") {
					$method = "update";
				}


				if ($success_msg) {
					?>

					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $success_msg; ?>
					</div>
					<?php
				}
				if ($error_msg) {
					?>

					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $error_msg; ?>
					</div>
					<?php
				}
				?>
				<div id="showError"></div>
			</div>
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Manage packages</h2>
							<p class="paragraph-text-sm-grey">Enter information about your new package.</p>
							<p class="paragraph-text-sm-grey margin-none"><i class="fa fa-info" style="color:#468fb1;"></i> Default package price should be zero.</p>
							<p class="paragraph-text-sm-grey margin-none"><i class="fa fa-info" style="color:#468fb1;"></i> In case of discount, should enter package orignal price in orignal price input field.
							</p>
							<p class="paragraph-text-sm-grey margin-none"><i class="fa fa-info" style="color:#468fb1;"></i> In case of discount, Package name should be same as the normal package.
							</p>
							<p class="paragraph-text-sm-grey margin-none"><i class="fa fa-info" style="color:#468fb1;"></i> By default number of sub-admins will be zero in each package.
							</p>
							<p class="paragraph-text-sm-grey margin-none"><i class="fa fa-info" style="color:#468fb1;"></i> In case of Time period is in days, Number of days input should be greater then zero, otherwise select others time periods.
							</p>
						</div>
					</div>
				</div>

			</div>
		</div>
		<?php

		?>
		<form method="post" id="pkg" enctype="multipart/form-data" action="<?php echo
		base_url('package/' . $method); ?>">

		<div class="row form">






			<div class="col-md-6">
				<div class="form-group input-effects">
					<input type="text" 
					name = "product[pack_title]" 
					id="name"
					class="home-input validThis"
					value="<?php echo stripslashes($fields_val['pack_title']); ?>" 
					placeholder="Package name"/>
					<label>Package name</label>
					<span class="focus-border"></span>
					<span class="text-danger"></span>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group input-effects">
					<input type="text" 
					value="<?php echo $fields_val['memory_limit']; ?>"  
					name = "product[memory_limit]" 
					oninput="this.value=this.value.replace(/[^0-9]/g,'');"
					class="home-input validThis"
					placeholder="Memory Limit"/>
					<label>Memory Limit (Mbs)</label>
					<span class="focus-border"></span>
					<span class="text-danger"></span>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group input-effects">
					<input type="text" 
					value="<?php echo stripslashes($fields_val['pack_products']); ?>"  
					name = "product[pack_products]" 
					oninput="this.value=this.value.replace(/[^0-9]/g,'');"
					class="home-input validThis" 
					placeholder="Number of products"/>
					<label>Number of products</label>
					<span class="focus-border"></span>
					<span class="text-danger"></span>					
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group input-effects">
					<input type="text" 
					value="<?php echo stripslashes($fields_val['pack_sub_admins']); ?>"  
					name = "product[pack_sub_admins]" 
					oninput="this.value=this.value.replace(/[^0-9]/g,'');"
					class="home-input "
					placeholder="Number of Sub-Admins"/>
					<label>Sub-Admins</label>
					<span class="focus-border"></span>
					<span class="text-danger"></span>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group input-effects">
					<input type="text" 
					name = "product[pack_price]" 
					id="price" 
					oninput="this.value=this.value.replace(/[^0-9]/g,'');"
					value="<?php echo $fields_val['pack_price']; ?>" 
					class="home-input validThis"
					placeholder="Price">
					<!-- <div class="input-group-append">
						<span class="input-group-text form-input input-prepend">USD</span>
					</div> -->
					<label>Price (USD)</label>
					<span class="focus-border"></span>
					<span class="text-danger"></span>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group input-effects">
					<input type="text" 
					value="<?php echo stripslashes($fields_val['pack_discount']); ?>"  
					name = "product[pack_discount]" 
					oninput="this.value=this.value.replace(/[^0-9]/g,'');"
					class="home-input"
					placeholder="Orignal price"/>
					<label>Orignal price in case of Discount (USD)</label>
					<span class="focus-border"></span>
					<span class="text-danger"></span>
				</div>
			</div>
			<?php 
			$month = "selected='selected'";
			$quarter = " ";
			$halfYear = " ";
			$annual = " ";
			$days = " ";
			// if(isset($fields_val['pack_time_period']) && !empty($fields_val['pack_days'])){
			if(isset($fields_val['pack_time_period'])){
				if($fields_val['pack_time_period'] == 'Days'){
					$days = "selected='selected'";
					$month = " ";
				}
				if($fields_val['pack_time_period'] == 'Month'){
					$month = "selected='selected'";
				}
				if($fields_val['pack_time_period'] == 'Quarter'){
					$quarter = "selected='selected'";
					$month = " ";
				}
				if($fields_val['pack_time_period'] == 'Half-Year'){
					$halfYear = "selected='selected'";
					$month = " ";
				}
				if($fields_val['pack_time_period'] == 'Annual'){
					$annual = "selected='selected'";
					$month = " ";
				}
			}
			?>
			<div class="col-md-6">
				<div class="form-group input-effects">
					<select id="time-period" onchange="change_time_period(this.value)" name = "product[pack_time_period]"  class="home-input validThis">
						<option <?php echo $days; ?>  value="Days">Days</option>
						<option <?php echo $month; ?> value="Month">Monthly</option>
						<option <?php echo $quarter; ?> value="Quarter">Quarterly</option>
						<option <?php echo $halfYear; ?> value="Half-Year">Half-Yearly</option>
						<option <?php echo $annual; ?> value="Annual">Annually</option>
					</select>
					<label>Select Time Period</label>
					<span class="focus-border"></span>
					<span class="text-danger"></span>
				</div>
			</div>
			<?php if(isset($fields_val['pack_days']) && !empty($fields_val['pack_days'])){
				$daysDisplay = "style='display:block;'";
			}
			else{
				$daysDisplay = "style='display:none;'";
			} ?>
			<div class="col-md-6" <?php echo $daysDisplay; ?> id="days-input-main">
				<div class="form-group input-effects">
					<input type="text" 
					id="pack_days" 
					value="<?php echo stripslashes($fields_val['pack_days']); ?>"  
					name = "product[pack_days]" 
					oninput="this.value=this.value.replace(/[^0-9]/g,'');"
					class="home-input "
					placeholder="Number of Days"/>
					<label>Days</label>
					<span class="focus-border"></span>
					<span class="text-danger"></span>
				</div>
			</div>
			<input type="hidden" name="save_id" value="<?php echo @$save_id; ?>" />
			<input type="hidden" name="slug_name" value="<?php echo@$fields_val['pack_slug']; ?>" />

<!-- 			<div class="col-md-6 text-center">

				<div id="imagePreview" style="background-image: url('<?php echo base_url() .
				"/site_assets/package/" . @$fields_val['pack_image']; ?>');background-repeat: no-repeat;background-size: cover;" class="nauk-info-connections block-file-upload block-border-dotted text-center">

				<h3 class="heading-sm-grey text-center">UPLOAD FILE</h3>
				<div class="form-group">
					<input hidden="" name="save_id" value="<?php echo @$save_id; ?>" />
					<input hidden="" name="slug_name" value="<?php echo
					@$fields_val['pack_slug']; ?>" />
					<input hidden="" name="old_image_path"  value="<?php echo
					@$fields_val['pack_image']; ?>" />
					<input hidden="" name="old_image" id="old_image" value="<?php echo
					@$fields_val['pack_image']; ?>" />
					<input hidden="" name="old_image_thumb" id="old_image_thumb" value="<?php echo
					@$fields_val['pack_image_thumb']; ?>" />
					<input style="display: none;" id="imfile" type="file" value="" name="userfile" class="file-input-product" onchange="preview_image(this)" placeholder="Select from computer">

				</div>
				<div class="form-group text-center">
					<input type="button" onclick="upload_img()" value="Upload Image" class="default-btn-blue btn" />
				</div>
			</div> 
		</div>  -->



		<div class="col-md-12 editor">
			<label for="long-description-editor" class=" heading-md-grey">Package description</label>
			<div class="form-group">
				<textarea id="long-description-editor" class="form-control form-textarea border-dark-1"><?php echo
				$fields_val['pack_description']; ?></textarea> 
				<textarea placeholder="Description" style="display: none;" name="product[pack_description]" id="put_long_desp" class="validThis"></textarea>
				<!--	<span class="text-danger">Validation error</span>-->
			</div>
		</div> 




		<div class="col-md-12 mx-auto">
			<div class="nauk-info-connections text-center">
				<!-- <input value="save" style="display: none;" id="submit_pro" type="submit" name="create_product" class="btn-sm-blue btn" /> -->
				<input type="hidden" name="create" value="create" />
				<button id="pkgSave" 
				type="button" 
				class="btn-form btn" >Save</button>
			</div>
		</div>

	</form>
</div>	
</div>	
</div>	

<script>
	$("#pkgSave").on("click", function(event){
		event.preventDefault();

		$("#put_long_desp").html($("#long-description-editor").Editor("getText"));
							//console.log($('#pkg').serializeArray() );
							formValidatePack('pkg');
						});

	$(document).ready(function() {
								//$("#short-description-editor").Editor();
								$("#long-description-editor").Editor();
								var product_long_description = "<?php echo
								addslashes($fields_val['pack_description']); ?>";

								$("#long-description-editor").Editor("setText", product_long_description);


							});
						/*function get_descrip(){
                              //$("#put_short_desp").html($("#short-description-editor").Editor("getText"));
                              $("#put_long_desp").html($("#long-description-editor").Editor("getText"));

                              $("#submit_pro").click();
                          }*/
                      </script>
                      <script type='text/javascript'>
                      	function upload_img(){
                      		$('#imfile').click();
                      	}
                      	function readURL(input) {
                      		if (input.files && input.files[0]) {
                      			var reader = new FileReader();
                      			reader.onload = function(e) {
                      				$('#imagePreview').css('background-image', 'url('+e.target.result +')');
                      				$('#imagePreview').css('background-repeat', 'no-repeat');
                      				$('#imagePreview').css('background-size', 'cover');
                                        //$('#imagePreview').attr('src', e.target.result);
                                        $('#imagePreview').hide();
                                        $('#imagePreview').fadeIn(650);
                                    }
                                    reader.readAsDataURL(input.files[0]);
                                    $("#old_image").val("");
                                }
                            }
                            //$("#imageUpload").change(function() {
//                                readURL(this);
//                            });
function preview_image(eventt){
	readURL(eventt);
}

</script>
<script>
	function formValidatePack(FormId){
		var validation = false;
		var f = $("#" + FormId)[0];
		var e;
		var errorMsg;


		if (f.checkValidity()) {
			$(".validThis").each(function(){
				e = $(this);
				if(($.trim(e.val()) == "") || (e.val() == null) || (typeof e.val() === 'undefined')){
				        	//console.log("run");
				        	errorMsg = e.attr("placeholder").replace(/_|-/g, " ").toUpperCase() + " is required.";
				        	e.focus();
				        	$('#showError').html('<div style="word-wrap:break-word;" class="alert alert-danger alert-dismissable text-justify"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + errorMsg + '</div>');
				        	validation = false;
				        	return false;
				        }else{
				        	validation = true;
				        }
				    });
			if (validation == true) {
				    	//console.log(f);
				    	f.submit();
				    }

				}else{
					$('#showError').html('<div style="word-wrap:break-word;" class="alert alert-danger alert-dismissable text-justify"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error: </strong>All enable fields are required! Please check again all of your input.</div>');
				}
			}




			function change_time_period(value){
				if(value == 'Days' ){
					$("#days-input-main").show();
				}else{
					$("#days-input-main").hide();
					$("#pack_days").val('');

				}
			}
		</script>
