<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>



	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<?php
			$success_msg = $this->session->flashdata('success_msg');
			$error_msg = $this->session->flashdata('error_msg');


			if ($success_msg) {
				?>
				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $error_msg; ?>
				</div>
				<?php
			}
			?>
		</div>

		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">All my Packages</h2>
							<p class="paragraph-text-sm-grey">Manage all your package.</p>
							<p class="paragraph-text-sm-blue">For free package Price should be zero.</p>
						</div>
						<div class="pull-right">
							<a class="btn-form btn" href="<?php echo base_url('package/add'); ?>"><i class="fa fa-plus"></i> Create Package</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->

		</div>

		<div class="row">

			<?php foreach($products as $product){ ?>
				<div class="col-md-4 product" >
					<div class="nauk-info-connections text-left product-view border-dark-1">
						<img class="product-image" onerror="this.onerror=null; this.src='https://digitalmediadeliveries.com/site_assets/package/12345-5d10cab2841d4.png'" src="<?php echo base_url(); ?>site_assets/package/<?php echo $product['pack_image'];  ?>">
						<p class="paragraph-text-sm-blue"><?php echo stripslashes($product['pack_title']);  ?></p>
						<p class="paragraph-text-sm-grey">Price: $<?php echo $product['pack_price'];  ?></p>
					</div>
					<div class="nauk-info-connections text-center product-detail  border-dark-1">
						<p class="paragraph-text-sm-blue"><?php echo stripslashes($product['pack_title']); 
						 if($product['pack_discount'] != ''){ echo ' (Disc)';}?></p>
						<p class="product-detail-text-red"><a  href="<?php echo base_url('package/'.$product['pack_code']); ?>"> Edit</a></p>
						<p class="product-detail-text-red"><a  href="<?php echo base_url('package/delete/'.$product['pack_code']); ?>"> Delete</a></p>
						<!--<p class="product-detail-text-red"><a  href="<?php echo base_url('package/purchase/'.$product['pack_code']); ?>"> PURCHASES</a></p>-->
					</div>
				</div>
			<?php } ?>

		</div>
	</div>
</div>