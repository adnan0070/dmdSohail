<div class="row main-body mx-auto ">

  <?php
  $user_role = $this->session->userdata('user_role');
  if($user_role!='2'){
    $this->load->view('front_pages/dashboard/dash_left'); 
  }else{
    $this->load->view('front_pages/dashboard/admin_dash_left');
  }

  ?>


  <div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
    <div class="row">
      <div class="col-md-12 inner-body-head"><!-- full block start-->
        <div class="nauk-info-connections">
          <div class="page-header">
            <div class="pull-left">
              <h2 class="heading-lg-green">Sales Reports</h2>
              <p class="paragraph-text-sm-grey">Generate reports for all sales for all your products or, filter out individual products and period ranges.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <form id="frm_filter">
      <div class="row form border-green filter-block-sales" style="margin:20px 0px; padding:10px">
       <div class="col-md-12 mx-auto">
        <div class="nauk-info-connections text-center">
         <label for="long-description-editor" class=" heading-md-grey">filter</label>
       </div>
     </div>
     <div class="col-md-6">
      <div class="form-group input-effects">
        <select name="period"  class="home-input" placeholder="">
          <option value="" class="form-input form-input-lg border-dark-1">select shipping period</option>
          <option value="7">Last 7 Days</option>
          <option value="30">Last 30 Days</option>
          <option value="90">Last 90 Days</option>
        </select>
        <label>select Period</label>
        <span class="focus-border"></span>
        <!--<span class="text-danger">Validation error</span>-->
      </div>
    </div>
    
    <div class="col-md-6">
      <div class="form-group input-effects">
        <select  id="product" name="product" class="home-input" placeholder="">
          <option class="form-input form-input-lg border-dark-1" value="all" selected="selected"> all products </option>
          <?php foreach ($products as $product) { ?>
            <option  value="<?php echo $product['product_code']; ?>" class="form-input form-input-lg border-dark-1">
              <?php echo $product['product_name']; ?>
            </option>
          <?php } ?>
        </select>
        <label>Report for</label>
        <span class="focus-border"></span>
        <!--<span class="text-danger">Validation error</span>-->
      </div>
    </div>

    <div class="col-md-2 input-dates-small-lg">
      <div class="form-group input-effects">
        <input type="text" name="from" id="fromdate" class="datepicker home-input" placeholder="" />
        <label style="text-transform:lowercase;">mm/dd/yyyy</label>
        <span class="focus-border"></span>
        <!--<span class="text-danger">Validation error</span>-->
      </div>
    </div>

    <div class="col-md-2 text-center input-dates-small-sm input-effects">
      <label for="long-description-editor" class=" paragraph-text-md-grey" style="margin-top:10px;">TO</label>
    </div>

    <div class="col-md-2 input-dates-small-lg">
      <div class="form-group input-effects">
        <input type="text" name="to" id="todate" class="datepicker home-input" placeholder="" />
        <label style="text-transform:lowercase;">mm/dd/yyyy</label>
        <span class="focus-border"></span>
        <!--<span class="text-danger">Validation error</span>-->
      </div>
    </div>

    <div class="col-md-12 mx-auto">
      <div class="nauk-info-connections text-center"><div id="todateError" class="text-danger"></div>
      <button type="button" id="filter"class="btn btn-sm-blue">Apply Filters</button>
      <a class="btn btn-sm-blue" href="<?php echo base_url('reports'); ?>">clear filters</a>
    </div>
  </div>	
</div>
</form>


<div class="row form border-green filter-block-sales" style="margin:20px 0px;">
  <div class="col-md-6">
    <div class="form-group input-effects">
      <select  onchange="getval(this.value);" id="graph-list" class="home-input" placeholder="chart type">
        <option class="form-input form-input-lg border-dark-1" value="0">chart type</option>
        <option class="form-input form-input-lg border-dark-1" value="column">Bar chart</option>
        <option style="" class="form-input form-input-lg border-dark-1" value="line">Line chart</option>
      </select>
      <!--<span class="text-danger">Validation error</span>-->
    </div>
  </div>

  <div class="col-md-12 mx-auto">
    <div class="nauk-info-connections text-center">
      <label for="long-description-editor" class=" heading-md-grey">sales</label>
    </div>
  </div>	

  <div class="col-md-12">
    <div class="nauk-info-connections text-center" >
      <div id="chart"></div>
    </div>
  </div>
</div>

</div>

</div>

<script type="text/javascript">
  $("#filter").on("click", function(e){
    e.preventDefault();

    var from = Date.parse($("#fromdate").val());
    var to = Date.parse($("#todate").val());
  //console.log(from + ", " + to);

  if(from && ! to){
    $("#todate").focus();
    $("#todateError").html("To date must be selected after From date.");
    return false;
  }

  if(to && ! from){
    $("#fromdate").focus();
    $("#todateError").html("From date must be selected before To date.");
    return false;
  }

  if(from > to){
    $("#todate").focus();
    $("#todateError").html("To date must be greater than From date.");
    return false;
  }

  var dt = $("#frm_filter").serializeArray();
    //console.log(dt);
    $.ajax({
      type: "post",
      url: "/reports/filter",
      data: dt,
      success: function(msg){
        //console.log(msg);
        if(msg){
          $("#todateError").html("");
          buildChart(msg);
        }else{
            //console.log('no data');
            $("#todateError").html("No record found for selected filters.");
          }
        }//end success function
      });//end ajax
    });// end function

  $('#graph-list').on('change', function(event){
    event.preventDefault();

    var type = this.value;
    if(type !== '0') {
      var cdh = $('#chart').highcharts();
      $(cdh.series).each(function(){
        this.update({
          type: type 
        }, false);
      });
      cdh.redraw();
    }
  });

  $(document).ready(function(){
    $.ajax({
      type: "post",
      url: "/reports/filter",
      data: {product: 'all'},
      success: function(msg){
        console.log(msg);
        if(msg){
          buildChart(msg);
        }else{
          console.log('no data');
        }
      }//end success function
    });//end ajax
  });

  function buildChart(ChartData){
    var data = JSON.parse(ChartData);
  //$('#chart').highcharts({
    var chart = new Highcharts.Chart({
      chart: {
        renderTo: 'chart',
        type: 'line'
      },

      title: {
        text: ''
      },
      
      xAxis: {
        categories: data.xAxis
          /*tickInterval: 1,
          labels: {
              enabled: true,
              formatter: function() { return seriesData[this.value][0];},
            }*/
          },

          yAxis: {
            title: {
              text: 'Number of Sales'
            }
          },

          series: [{
            name: 'Sales',
            data: data.yAxis     
          }],

          plotOptions: {
            line: {
              dataLabels: {
                enabled: true
              },
              enableMouseTracking: true
            }
          },

          legend: {
            enabled: false
          },

          navigation: {
            buttonOptions: {
              enabled: false
            }
          },

          responsive: {
            rules: [{
              condition: {
                maxWidth: 500
              },
              chartOptions: {
                legend: {
                  layout: 'horizontal',
                  align: 'center',
                  verticalAlign: 'bottom'
                }
              }
            }]
          }


    });//end chart
    $(".highcharts-credits").hide();
  }
</script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>