<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<?php 

		if($this->session->flashdata('error_msg')){
			?>
			<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
			</div>

			<?php
		}

		if($this->session->flashdata('success_msg')){
			?>

			<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
			</div>


		<?php } ?>
		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Blog Articles</h2>
							<p class="paragraph-text-sm-grey">Manage all your blog articles.</p>
						</div>
						<div class="pull-right">
							<a  href="<?php echo base_url().'admin/add_Article'; ?>" class="btn default-btn-green"><i class="fa fa-plus"></i> Add new</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->

		</div>
		<div class="row">
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<h3>Articles</h3>
				
				<table id="tbl_articles" class="table table-bordered table-striped" style="width:100%">
					<thead>
					<tr>
						<th>#</th>
						<th>Image</th>
						<th>Title</th>
						<th>Meta Description</th>
						<th>Meta Keywords</th>
						<th>Actions</th>
					</tr>
					</thead>
					<tbody>
					<?php $i=0; if(!empty($posts)): foreach($posts as $post): $i++; ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><img width="60" height="60" src="<?php echo base_url('site_assets/articles/') . $post['file']; ?>" /></td>
						<td><?php echo $post['title']; ?></td>
						<td><?php echo $post['meta_description']; ?></td>
						<td><?php echo $post['meta_keywords']; ?></td>
						<td class="actions"><a href="<?php echo base_url('admin/edit_Article/') . $post['id']; ?>" class="btn default-btn-blue" ><i class="fa fa-pencil"></i> Edit</a>
							<a href="<?php echo base_url('admin/delete_Article/') . $post['id']; ?>" class="btn default-btn-red"  onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash"></i> Delete</a></td>
						</tr>
					<?php endforeach; endif;?>
					</tbody>
			</table>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
	    $('#tbl_articles').DataTable({
	        "pagingType": "full_numbers",
	        "ordering": false,
	        "searching": false,
	        "bLengthChange": false,
	        //"bInfo": false,
	        "dom": '<"top"i>rt<"bottom"flp><"clear">'
	    });
	});
</script>