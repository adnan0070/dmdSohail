<div class="row main-body mx-auto ">
 <?php $this->load->view('front_pages/dashboard/dash_left'); ?>
 <?php
$success_msg = $this->session->flashdata('sales');
if (isset($success_msg)) {
    $sales = $success_msg;
}



$month = array(
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec');
$total = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0);
$forline_total = $total;
array_unshift($forline_total, "");
unset($forline_total[0]);
foreach ($month as $mont) {
    foreach ($sales as $sale) {
        $mon = substr($sale['month'], 0, 3);
        if ($mont == $mon) {
            $date = date_parse($mont);
            $forline_total[$date['month']] = (int)$sale['total'];
        }
    }
}
//print_r($forline_total);
$repo_arr = array_combine($month, $total);

foreach ($sales as $sale) {
    $mon = substr($sale['month'], 0, 3);
    $repo_arr[$mon] = (int)$sale['total'];
}
foreach ($repo_arr as $key => $val) {
    if ($val <= 1) {
        unset($repo_arr[$key]);
    }
}


function js_str($s)
{
    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
}

function js_array($array)
{
    $temp = array_map('js_str', $array);
    return '[' . implode(',', $temp) . ']';
}






?>
            <div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

				<div class="row">

					<div class="col-md-12 inner-body-head"><!-- full block start-->
						<div class="nauk-info-connections">
							<div class="page-header">
								<div class="pull-left">
									<h2 class="heading-lg-green">SALES REPORTS</h2>
									<p class="paragraph-text-sm-grey">Generate report for many sales are generated.</p>
								</div>
							</div>
						</div>
					</div>
				</div>



<form id="frm_filter" method="post" action="<?php echo base_url() .
"reports/filter"; ?>">
				
				<div class="row form border-green filter-block-sales" style="margin:20px 0px; padding:10px">
                	<div class="col-md-12 mx-auto">
						<div class="nauk-info-connections text-center">
							<label for="long-description-editor" class=" heading-md-grey">filter</label>
						</div>
					</div>	

                     
					   
                     

					<div class="col-md-6">
						<div class="form-group">
							<select name="period"  class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="select shipping period">
								<option value="" class="form-input form-input-lg border-dark-1">select shipping period</option>
								<option value="7">Last 7 Days</option>
                                <option value="30">Last Month</option>
                                <option value="90">Last 90 Days</option>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>

					<div class="col-md-6">

						<div class="form-group">
							<select  id="product" name="product" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="report for">
								<option class="form-input form-input-lg border-dark-1" value=""> Report for </option>
							    	<?php
foreach ($products as $product) {

?>
                                          <option  value="<?php echo $product['product_code']; ?>" class="form-input form-input-lg border-dark-1"><?php echo
    $product['product_name']; ?></option>
                                        <?php
}
?>
                                    
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>
                    <div class="col-md-2 input-dates-small-lg">

							<div class="form-group">
								<input type="text" name="from" class="datepicker form-control form-input select-text-center form-input-lg border-dark-1" placeholder="MM/DD/YYYY" />
								<!--<span class="text-danger">Validation error</span>-->
							</div>
						</div>

						<div class="col-md-2 text-center input-dates-small-sm">
							<label for="long-description-editor" class=" paragraph-text-md-grey" style="margin-top:10px;">TO</label>
						</div> 
						<div class="col-md-2 input-dates-small-lg">

							<div class="form-group">
								<input type="text" name="to" id="todate" class="datepicker form-control form-input select-text-center form-input-lg border-dark-1" placeholder="MM/DD/YYYY" />
								<!--<span class="text-danger">Validation error</span>-->
							</div>
						</div>
                    <div class="col-md-12 mx-auto">
						<div class="nauk-info-connections text-center">
                        <input type="submit" value="apply filters" class="btn-sm-blue" />
							<!--<a class="btn-sm-blue" href="javascript:void(0)" id="filter">apply filters</a>-->
						</div>
					</div>	
                 
				</div>
            </form>
                 

				<div class="row form border-green filter-block-sales" style="margin:20px 0px;">

					<div class="col-md-6" style="display: none;">
						<div class="form-group">
							<select  id="shipping-country" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="select report">
								<option class="form-input form-input-lg border-dark-1"> select report</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<select  onchange="getval(this);" id="graph-list" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="chart type">
								<option class="form-input form-input-lg border-dark-1" value="0">chart type</option>
								<option class="form-input form-input-lg border-dark-1" value="1">Bar chart</option>
								<option style="display: none;" class="form-input form-input-lg border-dark-1" value="2">Line chart</option>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>

					<div class="col-md-12 mx-auto">
						<div class="nauk-info-connections text-center">
							<label for="long-description-editor" class=" heading-md-grey">sales</label>
						</div>
					</div>	


					

					<div class="col-md-12">
						<div class="nauk-info-connections text-center" > 
							<div class=" graph" id="bars"> </div>
						</div>
					</div>
					<script src="<?php echo base_url(); ?>site_assets/js/graphite.js"></script>
				<!--	<script src="<?php echo base_url(); ?>site_assets/js/main.js"></script>-->

					<div class="col-md-12">
						<div class="nauk-info-connections text-center" id="line-chart"> 
							<canvas id="graph" width="500" height="400" align="center"></canvas>
                            <script>
                            $("#filter").on("click", function(e){
                                		e.preventDefault();
                        				var dt = $("#frm_filter").serialize();
                        				//console.log(dt);
                        				$.ajax({
                        					type: "post",
                        					url: "/reports/filter",
                        					data: dt,
                        					success: function(msg){
                        					    /*$("#tb").html(msg);
                       						    $("#purchase_tot").html($("#purchase_count").val());
                                                $("#grand_tot").html($("#getto").val());*/
                                                var barsData = msg.barda;
                                                var barsOptions = {
                                                      'height': 300,
                                                      'title': '',
                                                      'width': 400,
                                                      'fixPadding': 10,
                                                      'barFont': [0, 12, "bold"],
                                                      'labelFont': [0, 10, 0]
                                                    };
                                                graphite(barsData, barsOptions, bars);
                                                    
                                                
                                                
                        					}
                        				});
                    		 });
                            var barsData = <?php echo json_encode($repo_arr); ?>;
                            
                            if(Object.keys(barsData).length>0){
                            
                                var barsOptions = {
                                  'height': 300,
                                  'title': '',
                                  'width': 400,
                                  'fixPadding': 10,
                                  'barFont': [0, 12, "bold"],
                                  'labelFont': [0, 10, 0]
                                };
                                graphite(barsData, barsOptions, bars);
                                }
                            </script>
							<script>		
							$( document ).ready(function() {
							 var total_arr   = <?php echo js_array($forline_total); ?>;
                             var total_month = <?php echo js_array($month); ?>;
                             
                             window.setTimeout(function(){
                              $("#bars").show();  
                             },500);
                             
                             
								var chartData = {
									node: "graph",
									dataset: total_arr,//[55, 99, 101, 80, 26, 55, 55, 99, 101, 80, 26, 55],
									labels: total_month,//["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
									pathcolor: "#288ed4",
									fillcolor: "#8e8e8e",
									xPadding: 0,
									yPadding: 0,
									ybreakperiod: 50
								};
								drawlineChart(chartData);
                                
                                
                                
                                               
							});
							</script>
							<script src="<?php echo base_url(); ?>site_assets/js/topup.js"></script>

						</div>
					</div>

				</div>
			</div>
		
	</div>