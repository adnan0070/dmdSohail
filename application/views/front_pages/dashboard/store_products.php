<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green"><?php if($store){echo $store['store_name'];} ?> Products</h2>
							<p class="paragraph-text-sm-grey">Manage all store products.</p>
						</div>
						
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
		<!-- </div> -->




			<?php 
			if($products){
				foreach ($products as $key => $product) {
					?>
						<div class="col-md-4 product" >
							<div class="nauk-info-connections text-left product-view border-dark-1">
								<img class="product-image"  src="<?php echo base_url('site_assets/products/') . $product['pro_image'] ; ?>">
								<p class="paragraph-text-sm-blue"><?php echo $product['product_name']; ?></p>
								<p class="paragraph-text-sm-grey">$<?php echo $product['product_price']; ?></p>
							</div>
							<div class="nauk-info-connections text-center product-detail  border-dark-1">
								<p class="paragraph-text-sm-blue"><?php echo $product['product_name']; ?></p>
								<p class="paragraph-text-sm-blue">Code: <?php echo $product['product_code']; ?></p>
								<p class="product-detail-text-red"><a target="_blank" href="<?php if($store){echo 'http://' . $store['store_url'] . '/home/showProduct/' . $product['product_code'];} ?>">View</a></p>
								<!-- <p class="product-detail-text-red"><a id="delProduct" data-product="<?php echo $product['product_code']; ?>" data-toggle="modal" data-target="#delete-product" href="#">Delete</a></p> -->
								<p class="product-detail-text-red"><button type="button" class="delProduct" data-product="<?php echo $product['product_code']; ?>">Delete</button></p>
							</div>
						</div>
						
					<?php
				}
			}
			?>
		</div>
	</div>
</div>
<div class="modal fade " id="delete-product">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Delete Product</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				Are you sure you want to delete this product ?
				<input type="hidden" name="productID" id="productID" value="">
			</div>

			<!-- Modal footer -->
			<div class="modal-footer text-right">
				<button type="button" class="btn default-btn-grey" id="deleteProduct">Yes</button>
				<a class="btn default-btn-grey" data-dismiss="modal">No</a>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$('.delProduct').on('click', function(event){
		event.preventDefault();
		var productID = $(this).data('product');
		$('.modal-body #productID').val(productID);
		$('.modal').modal('show');
		//console.log(productID);
	});
	$('#deleteProduct').on('click', function(event){
		event.preventDefault();
		var productID = $('#productID').val();
		//console.log(productID);
		window.location.replace('<?php echo base_url('admin/delete_single_product/'); ?>' + productID);
	});
</script>