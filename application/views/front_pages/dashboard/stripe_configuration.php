<div class="row main-body mx-auto ">

	<?php
	$left = 'front_pages/dashboard/dash_left';
	$action = '/store/createPM/stripe';

	$user_role = $this->session->userdata('user_role');
	if($user_role == 2){
		$left = 'front_pages/dashboard/admin_dash_left';
		$action = '/admin/create_pm/stripe';
	}
	$this->load->view($left); 

	?>




	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">
			<div id="showError"></div>

			<?php 
			if($this->session->flashdata('create_pm')){
				?> 
				<div class="alert alert-info alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Info!</strong> <?php echo $this->session->flashdata('create_pm'); ?>
				</div>
			<?php } ?>

			<?php 
			if(isset($pm)){
				$config = json_decode($pm["configuration"]);
							/*echo '<pre>';
							print_r($pm);
							echo '</pre>';*/
						}
						?>

						<div class="col-md-12 inner-body-head"><!-- full block start-->
							<div class="nauk-info-connections">
								<div class="page-header">
									<div class="pull-left">
										<h2 class="heading-lg-green">stripe configuration</h2>
										<p class="paragraph-text-sm-grey">Integrate payment methods for getting paid.</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div><!-- block end-->

					</div><!-- inner-body-end-->

					<form action="<?php echo $action; ?>" id="configStripe" method="post">

						<div class="row clearfix form">					
							<div class="col-md-12">
								<div class="form-group input-effects">
									<input type="text"
									name = "apiKey" 
									class = "home-input validThis" 
									id="api-key"  
									placeholder=""
									value="<?php if(isset($config->apiKey) && $config->apiKey){echo $config->apiKey;} ?>"
									/>
									<label>api key</label>
									<span class="focus-border"></span>
									<!--<span class="text-danger">Validation error</span>-->
								</div>
							</div> 

							<div class="col-md-12">
								<div class="form-group input-effects">
									<input type="text"  
									name = "publishableApiKey" 
									class = "home-input validThis" 
									id="publishable-api-key"  
									placeholder=""
									value="<?php if(isset($config->publishableApiKey) && $config->publishableApiKey){echo $config->publishableApiKey;} ?>"
									/>
									<label>publishable Api Key</label>
									<span class="focus-border"></span>
									<!--<span class="text-danger">Validation error</span>-->
								</div>
							</div> 

							<div class="col-md-12">
								<div class="form-group input-effects">
									<input type="text"
									name = "country"
									class = "home-input validThis"
									id="country"
									placeholder=""
									value="<?php if(isset($config->country) && $config->country){echo $config->country;} ?>"
									/>
									<label>country</label>
									<span class="focus-border"></span>
									<!--<span class="text-danger">Validation error</span>-->
								</div>
							</div> 

							<div class="col-md-12">
								<ul class="list-inline">

									<li class="list-inline-item form-checkbox"> 
										<input type="checkbox" 
										class="border-dark-1" 
										name="status" 
										value="1"
										<?php if(isset($pm) && $pm['status']){echo "checked";} ?>
										><span class="paragraph-dark-md-capital">Enabled</span>
									</li>

									<li class="list-inline-item form-checkbox">
										<input type="checkbox" 
										class="border-dark-1" 
										name="default" 
										value="1"
										<?php if(isset($pm) && $pm['default']){echo "checked";} ?>
										><span class="paragraph-dark-md-capital">Default</span>
									</li>
								</ul>
							</div> 
						</form>


						<div class="form-footer">
							<?php if(isset($pm)){ ?>
								<input type="hidden" name="pm_id" value="<?php echo $pm['id']; ?>" />
							<?php }?>
							<button id="confStripe" class="btn-form btn">update</button>
						</div>



					</div>
				</div>


			</div>

			<script type="text/javascript">
				$("#confStripe").on("click", function (e) {
					e.preventDefault();
					formValidate('configStripe');
				});
			</script>