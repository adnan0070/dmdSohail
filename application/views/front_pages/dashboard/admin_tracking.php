<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<?php
			$success_msg = $this->session->flashdata('success_msg');
			$error_msg = $this->session->flashdata('error_msg');


			if ($success_msg) {
				?>
				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $error_msg; ?>
				</div>
				<?php
			}

			?>
		</div>
		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Manage Tracking content in your platform</h2>
							<p class="paragraph-text-sm-grey">Add/update tracker codes.</p>
						</div>
						<div class="pull-right">
							<a class="btn-sm-default btn pricing-btn"  href="<?php echo base_url('admin/addTracker/'.encode($tagID)); ?>"><i class="fa fa-plus"></i> Add</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

		</div>


		<form method="post" enctype="multipart/form-data" action="<?php echo
		base_url('Admin/addTracker/'.encode($tagID)); ?>">
		<div class="row clearfix">

			<div class="col-md-6">
				<div class="item form-group">
					<label class="control-label" for="title">Name</label>
					<input  required type="text" id="name" name="name" value="<?php echo $tag['name']; ?>" class="form-control form-input">
					<span class="text-danger"><?php echo form_error('name');?></span>
				</div>
			</div>

			<div class="col-md-12 editor ">
				<label for="short-description-editor" class=" heading-md-grey">Header Content</label>
				<div class="form-group">
					<textarea  required name="headerContent"  maxlength="800" rows="20" class="form-control form-textarea "><?php echo $tag['header_content']; ?></textarea>
					<span class="text-danger"><?php echo form_error('content');?></span>
				</div>
			</div> 


			<div class="col-md-12 editor ">
				<label for="short-description-editor" class=" heading-md-grey">Body Content</label>
				<div class="form-group">
					<textarea  required name="bodyContent"  maxlength="800" rows="20" class="form-control form-textarea "><?php echo $tag['body_content']; ?></textarea>
					<span class="text-danger"><?php echo form_error('content');?></span>
				</div>
			</div> 
			<input name="submitted" type="hidden" value="submit">
			<div class="form-footer">
				<input type="submit" class="btn-form btn" value="save">
			</div>


		</div>

	</form>


	<div class="row">

		<div class="col-md-12 inner-body-head"><!-- full block start-->
			<table id="cats" class="table table-bordered table-striped" style="width:100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if ($tags) {
						foreach ($tags as $res) {
							?>
							<tr>
								<td><?php echo $res['name']; ?></td>
								<td>
									<a href="<?php echo base_url('admin/addTracker/'.encode($res['id'])); ?>" class="btn default-btn-grey">Edit</a>
									<a href="<?php echo base_url('admin/deleteTracker/'.encode($res['id'])); ?>" class="btn default-btn-grey">Delete</a>
								</td>
							</tr>

							<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>

</div>

</div>