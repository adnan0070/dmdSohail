<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">New email template</h2>
							<p class="paragraph-text-sm-grey">New email template</p>								
						</div>
					</div>
				</div>

			</div>

			<form action="/store/newEmailTemplete" method="post" id="frmEditor">

				<div class="col-md-12">
					<div class="form-group input-effects">
						<input type="text"  
						name="subject" 
						id="subject"  
						placeholder=""
						class = "home-input"
						/>
						<label>subject</label>
						<span class="focus-border"></span>
						<input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id'); ?>" />
					</div>
				</div> 

				<div class="col-md-12 editor">
					<label for="email-editor" class=" heading-md-grey">Email Content</label>
					<div class="form-group">
						<textarea name="content" 
						id="edit-email-content-editor" 
						class="form-control form-input border-dark-1"
						></textarea> 
					</div>
				</div> 

				<div class="col-md-12 mx-auto inner-body-block">
					<div class="nauk-info-connections text-center">
						<button type="submit" id="sbmt" class="btn-form btn-top-margin-more">save</button>
					</div>
				</div>	

			</form>

			
			<script>
				$(document).ready(function() {
					var editor = $("#edit-email-content-editor");
					editor.Editor();

					$("#sbmt").on("click", function(e){
						e.preventDefault();

						$("#edit-email-content-editor").val($("#edit-email-content-editor").Editor("getText"));
							//$("#frmEditor").submit();
							SubmitForm('frmEditor');
						});

				});
			</script>
		</div>
	</div>
	
	
</div>