<div class="row main-body mx-auto ">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>			
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<div class="col-md-12">
				<?php
				$success_msg = $this->session->flashdata('success_msg');
				$error_msg = $this->session->flashdata('error_msg');
				 
				 
				 


				if ($success_msg) {
					?>

					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $success_msg; ?>
					</div>
					<?php
				}
				if ($error_msg) {
					?>

					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo str_replace("_"," ",$error_msg); ?>
					</div>
					<?php
				}
				?>
			</div>
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							
						 
								<h2 class="heading-lg-green">Create new product</h2>
								<p class="paragraph-text-sm-grey">Enter information about your new product.</p>

							 
							 

						</div>
					</div>
				</div>

			</div>
		</div>
		<?php

		?>
		<form method="post" enctype="multipart/form-data" action="<?php echo
		base_url('product/add_product'); ?>">

		<div class="row form">

			<div class="col-md-6">

				<div>
					<div class="form-group">

						<input  type="file" value="" name="image"   placeholder="Select from computer">

					</div>

				</div> 






			</div> 


			<div class="col-md-6">
				<div class="form-group input-effects">
					<input type="text" value=" "  name = "name" class = " home-input" id="name"  placeholder=""/>
					<label>product name</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>







			</div> 






			<div class="col-md-12 mx-auto text-center">

				<input value="save"   id="submit_pro" type="submit" name="create_product" class="btn-sm-blue btn" />

			</div>
		</div>	

	</form>
</div>	
</div>	
</div>	
