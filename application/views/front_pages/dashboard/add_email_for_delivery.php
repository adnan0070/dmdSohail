<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>



	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<?php
			$success_msg = $this->session->flashdata('success_msg');
			$error_msg = $this->session->flashdata('error_msg');


			if ($success_msg) {
				?>
				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $error_msg; ?>
				</div>
				<?php
			}


			$action = "savemail";
			$subject = "";
			$upload_file = "";
			$fid  = "";
			$desp = "<div>Hello, {name} </div><div><br></div><div>--- Enter your Message for Customer ---</div>";
			if (isset($file_id)) {

				$action = "updatemail";
				$fid = $file_id;
				$subject = $file[0]["subject"];
				$desp = $file[0]["email_desp"];
			}

			?>
		</div>



		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">add e-mail for delivery</h2>
							<p class="paragraph-text-sm-grey">Add a new e-mail to your product for customers</p>
						</div>


					</div>
				</div>
			</div>

		</div>


		<form method="post" enctype="multipart/form-data" action="<?php echo
		base_url('product/' . $action); ?>">
		<div class="row clearfix form">	


			<div class="col-md-12">
				<div class="form-group input-effects">
					<input type="text" value="<?php echo $subject; ?>"  name = "product[subject]" class = "home-input" id="subject"  placeholder=""/>
					<input type="hidden" name="product[product_id]" value="<?php echo $id; ?>" />
					<input type="hidden" name="file_id" value="<?php echo $fid; ?>" />
					<label>Subject</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div> 

			<div class="col-md-12 editor">
				<label for="privacy-policy-editor" class=" heading-md-grey">content</label>
				<div class="form-group">
					<textarea id="subject-editor" class="form-control form-input border-dark-1"></textarea> 
					<textarea style="display: none;" id="email_desp" name="product[email_desp]" class="form-control form-input border-dark-1"></textarea>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>




			<div class="form-footer">
				<input value="Save" style="display: none;" id="submit_pro"  type="submit" class="btn-sm-blue btn" />
				<input value="Save"  type="button" onclick="get_descrip()" class="btn-sm-blue btn" />
			</div>


		</div>

	</form>



</div>
</div>
<script>
	$(document).ready(function() {
		$("#subject-editor").Editor();
		var product_long_description = "<?php echo addslashes($desp); ?>";
		$("#subject-editor").Editor("setText", product_long_description);
	});
	function get_descrip(){
          //$("#put_short_desp").html($("#short-description-editor").Editor("getText"));
          $("#email_desp").html($("#subject-editor").Editor("getText"));
          $("#submit_pro").click();
      }
  </script>