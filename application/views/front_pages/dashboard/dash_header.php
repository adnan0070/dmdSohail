<?php
$user_id = $this->session->userdata('user_id');
$user_role = $this->session->userdata('user_role');
$display_name = $this->session->userdata('user_name');
$store_id = $this->session->userdata('store_id');
$store_name = $this->session->userdata('store_name');
$store_url = 'https://' . $this->session->userdata('store_url');
$error_msg_limit   = $this->session->flashdata('error_msg_limit');
$tutorialfile = $this->session->userdata('tutorialfile');

if (!$user_id) { redirect('user/login'); }
$dash_link = '/user/dashboard';
if ($user_role == 2) {
	$controller = "admin";
	$dash_link = '/admin/dashboard';
}


$method = $this->router->fetch_method();
$class = $this->router->fetch_class();
if ($user_role != '2') {
	if ($store_id == "" && ($method != 'dashboard') && ($method != 'subscription')) {
		if ($method != 'add' && $class != 'store') {
			if($method != 'profile' && $method != 'knowl_base' && $method != 'new_updates'){
				redirect('user/dashboard');
			}	
		}
	}
	// check product excceded or not, get uploaded products by user
	$products = $this->db->query('select count(*) as totpro from dmd_store_products where user_id="' .
		$this->session->userdata('user_id') . '"');
	$totpro = $products->row_array();
	$totpro = $totpro['totpro'];
	
	// get default package id
	$default_pack = $this->db->get_where('store_package', array('user_id' => $this->session->userdata('user_id')));
	$default_pack = $default_pack->row_array();
	$depackage = $this->db->get_where('packages', array('pack_code' => $default_pack['package_id']));
	$depackage = $depackage->row_array();

	if($default_pack['payment_status'] != '2'){
		if ($totpro >= $depackage['pack_products']) {
			$this->session->set_flashdata('error_msg_limit',
				'Your product limit exceeded please upgrade your package.');
			if ($method == 'add' && $class == 'product') {
				redirect('user/subscription');
			}
		}
	}
	else{
		$this->session->set_flashdata('error_msg_limit',
			'Please first buy your selected subscription to explore Digital Media Deliveries more.');
		if($method != 'subscription'){
			redirect('user/subscription');
		}
	}


    // get expire date
	$today = date('y-m-d');
	$today = date('Y-m-d', strtotime($today));
	$end_date = date('Y-m-d', strtotime($default_pack['end_date']));
	$final_date = date('jS F Y', strtotime($end_date));
	$alert_date = date('Y-m-d', strtotime('-1 day', strtotime($default_pack['end_date'])));

	if ($default_pack['payment_status'] == 0 && ($today >= $alert_date)) {
		$this->session->set_flashdata('error_msg_limit',
			'Your package has been expired on <strong>' . $final_date . '</strong>. Please upgrade your package.');
		if ($method != 'subscription') {
			redirect('user/subscription');
		}
	}
}

if(isset($view_items)){
	$view_items = $view_items[0];
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<script>

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-147349522-1', 'auto');

		ga("require", "ecommerce");

	</script>



	<title>Digital Media Deliveries</title>
	<meta charset="utf-8">	
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>site_assets/images/favicon.png"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="description" content="<?php if(isset($view_items)){ echo $view_items->meta_description;} ?>">
	<meta name="keywords" content="<?php if(isset($view_items)){ echo $view_items->meta_keyword;} ?>">
	<?php $metaTags = getMetatags(); 
	foreach($metaTags as $row){
		echo $row['content'];
	}
	?>
	<!-- CSS ////////////////////////////////////////////////// -->
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap-grid.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/bootstrap-reboot.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/home.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/style.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/responsive.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/barchart.css?v=1" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/css/editor.css?v=1" type="text/css" rel="stylesheet"/>
	<link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>site_assets/datepicker/css/bootstrap-datepicker.standalone.css" rel="stylesheet">

	<link href="<?php echo base_url(); ?>site_assets/countrySelect/dist/css/bootstrap-formhelpers.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>site_assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>site_assets/css/owl.theme.default.min.css">
	<!--<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">-->
	<!-- font-family: 'Montserrat', sans-serif; -->

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- JS ///////////////////////////////////////////////////-->

	<script src="<?php echo base_url(); ?>site_assets/js/jquery.min.js"></script>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/editor.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/custom.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/topbutton.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/events.js"></script>	

	<script src="<?php echo base_url(); ?>site_assets/datepicker/js/bootstrap-datepicker.min.js"></script>

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/sl-1.2.6/datatables.min.css"/>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/sl-1.2.6/datatables.min.js"></script>
	<script src="<?php echo base_url(); ?>site_assets/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>site_assets/countrySelect/dist/js/bootstrap-formhelpers.min.js"></script>

	<!-- Tapfiliation-->
	<script src="https://script.tapfiliate.com/tapfiliate.js" type="text/javascript" async></script>
	<script type="text/javascript">
		(function(t,a,p){t.TapfiliateObject=a;t[a]=t[a]||function(){ (t[a].q=t[a].q||[]).push(arguments)}})(window,'tap');

		tap('create', '9351-9a1558');

	</script>
	<!-- Tapfiliation-->

	<!-- tracking -->
	<?php $trackers = getTracking(); 
	foreach($trackers as $row){
		echo $row['header_content'];
	}
	?>
	<!-- tracking --> 
<!-- <script>

dataLayer.push({
  'ecommerce': {
    'purchase': {
      'actionField': {
        'id': 'T12345',                        
        'affiliation': 'Online Store',
        'revenue': '35.43',                    
        'tax':'4.90',
        'shipping': '5.99',
        'coupon': 'SUMMER_SALE',
        'currency': 'USD' 
      },
      'products': [{                            
        'name': 'Triblend Android T-Shirt',    
        'id': '12345',
        'price': '15.25',
        'brand': 'Google',
        'category': 'Apparel',
        'variant': 'Gray',
        'quantity': 1,
        'coupon': '' ,
        'currency': 'USD'                           
       },
       {
        'name': 'Donut Friday Scented T-Shirt',
        'id': '67890',
        'price': '33.75',
        'brand': 'Google',
        'category': 'Apparel',
        'variant': 'Black',
        'quantity': 1,
        'currency': 'USD' 
       }]
    }
  }
});
</script> -->
</head>

<body>  
	<?php  
	foreach($trackers as $row){
		echo $row['body_content'];
	}
	?>
	
	<div class="se-pre-con"></div>
	<script>
		/*
		$(window).load(function() {
			$(".se-pre-con").fadeOut(1000);;
		});*/
		$(document).ready(function(){
			$.topbutton({
				html : "<i>Top</i>",      
				css : "width:50px; height:50px; background:#468fb1; border:none; font-size:20px;",  
				scrollAndShow : true,   
				scrollSpeed : 1000        
			});

		});
	</script>
	<div class="container main">
		<div class="row header">
			<div class ="col-md-3 text-left logo">
				<a href="<?php echo $dash_link; ?>">
					<img src="<?php echo base_url(); ?>site_assets/images/logo.png">
				</a>
			</div>

			<div class ="col-md-9 mx-auto main-menu">		
				<div class="nauk-info-connections text-right">
					<nav class="navbar" role="navigation">


						<ul class="nav navbar-right list-inline ">
							<li class="list-inline-item menu-item item-link"><a target="_blank" href="<?php if($store_url){echo $store_url;} ?>"><?php if($store_name && ($user_role != '2')){echo $store_name;} ?></a></li>
							<li class="list-inline-item menu-item item-link"><a href="<?php echo $dash_link; ?>">dashboard</a></li>
							<li class="list-inline-item menu-item item-link"><a href="<?php echo base_url('user/resources');?>">Resources</a></li>
							<!--<?php if (!$user_role == '2') {
								?>
								<li class="list-inline-item menu-item dropdown ">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">help <b class="caret"></b></a>
									<ul class="dropdown-menu border-dark-1">
										<li class="menu-dropdown-item"><a href="<?php echo base_url("user/knowl_base"); ?>">Knowledge base</a></li>
										<li class="menu-dropdown-item"><a href="#">tutorial videos</a></li>
										<li class="menu-dropdown-item"><a href="<?php echo base_url("user/new_updates"); ?>">updates</a></li>
									</ul>
								</li>
								<?php
							}
							?>
						-->
						<?php if (!$user_role == '2') {
							?>
							<li class="list-inline-item menu-item item-link dropdown">
								<a href="javascript:void(0)" class="dropdown-toggle header-dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									help
									<span class=" fa fa-angle-down" style="font-size:12px;"></span>
								</a>
								<ul class="dropdown-menu animated fadeInUp header-dropdown">
									<li>
										<a href="<?php echo base_url("user/knowl_base"); ?>" class="hvr-bounce-to-right"> Knowledge base</a>
									</li>

									<li>
										<a href="<?php echo base_url("tutorial"); ?>" class="hvr-bounce-to-right"> tutorial videos</a>
									</li>

									<li>
										<a href="<?php echo base_url("user/new_updates"); ?>" class="hvr-bounce-to-right">updates</a>
									</li>
								</ul>
							</li>
							<?php
						}
						?>
<!--
							<li class="list-inline-item menu-item dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $display_name; ?> <b class="caret"></b></a>
								<ul class="dropdown-menu border-dark-1">
									<li class="menu-dropdown-item"><a href="<?php echo base_url('user/profile'); ?>">profile</a></li>
									<?php
									if (isset($store_id) && $store_id != "") {
										?>
										<li class="menu-dropdown-item"><a href="<?php echo
										base_url('user/subscription'); ?>">manage subscription</a></li>
										<li class="menu-dropdown-item"><a href="<?php echo base_url('user/history'); ?>">history</a></li>
										
										<?php
									}
									?><li class="menu-dropdown-item"><a href="<?php echo base_url('user/logout'); ?>">logout</a></li>
								</ul>
							</li>
						-->

						<li class="list-inline-item menu-item item-link dropdown">
							<a href="javascript:void(0)" class="dropdown-toggle header-dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<?php echo $display_name; ?>
								<span class=" fa fa-angle-down" style="font-size:12px;"></span>
							</a>
							<ul class="dropdown-menu animated fadeInUp header-dropdown">
								<?php if($this->session->userdata('user_type') == 'subscriber'): ?>
									<li>
										<a href="<?php echo base_url('user/profile'); ?>" class="hvr-bounce-to-right"> profile</a>
									</li>
								<?php endif; ?>

								<?php
								if (isset($store_id) && $store_id != "") {
									?>
									<?php if($this->session->userdata('user_type') == 'subscriber'): ?>
										<li>
											<a href="<?php echo
											base_url('user/subscription'); ?>" class="hvr-bounce-to-right">  manage subscription</a>
										</li>

										<?php if($this->session->userdata('pack_sub_admin') > 0): ?>
											<li>
												<a href="<?php echo
												base_url('subadmin'); ?>" class="hvr-bounce-to-right">Sub Admins</a>
											</li>
										<?php endif; ?>

									<?php endif; ?>
									<li>
										<a href="<?php echo base_url('user/history'); ?>" class="hvr-bounce-to-right">history</a>
									</li>

									<?php
								} ?>

								<li>
									<a href="<?php echo base_url('user/logout'); ?>" class="hvr-bounce-to-right"> logout</a>
								</li>
							</ul>
						</li>




					</ul>

					<!-- navbar-collapse -->
				</nav>
			</div>

		</div>
		<?php if($error_msg_limit) { ?>
			<div class="alert alert-info alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Info!</strong> <?php echo $error_msg_limit; ?>
			</div>
		<?php } ?>
		<?php if ($user_role != '2') {
			?>
			<div class="modal fade " id="tutorial-video">
				<div class="modal-dialog modal-lg">
					<div class="modal-content tutorial-modal-content">
						<div class="modal-header tutorial-modal-header">
							<button onclick="closeTutorial()" type="button" class="close" data-dismiss="modal">&times;</button>
						</div>

						<div class="modal-body tutorial-modal-body">
							<video id="tutorial-video" class="img-responsive" src="<?php echo base_url('site_assets/video/') . $tutorialfile; ?>" controls controlsList="nodownload" width="100%" style=""> </video>
						</div>

					</div>
				</div>
			</div>
		<?php } ?>
		<script>
			function closeTutorial(){
				$('video').trigger('pause');
			}
		</script>
	</div>
	<div class="loader-cover" id="main-loader"><img src="<?php echo base_url(); ?>site_assets/images/loader-dash.gif"></div>  