<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>



	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<?php
			$success_msg = $this->session->flashdata('success_msg');
			$error_msg = $this->session->flashdata('error_msg');


			if ($success_msg) {
				?>
				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $error_msg; ?>
				</div>
				<?php
			}
			?>
		</div>

		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">manage action purchases</h2>
							<p class="paragraph-text-sm-grey">Manage what the users will receive after they purchase your product. Manage all your products here and what information your customer will receive after purchasing.</p>
						</div>

						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->

			<div class="col-md-6 manage-purshase-block "><!--block start-->
				<div class="nauk-info-connections">
					<h4 class="heading-md-blue">Active</h4>
				</div>
				<div class="nauk-info-connections  border-dark-1" style="padding:10px;">

					<?php
    //print_r($files);
					if(count($files)>0){    
						foreach($files as $file){

							$file_name = explode($id."-",$file['upload_file']);
							$file_name = $file_name[1];

							$base = base_url()."site_assets/products/".$file['product_id'].'/';
							?>
							<div class="pb-video">
								<?php if(strpos(@$file['upload_type'],"image")!==false){ ?>
									<img width="100%" height="230" src="<?php echo $base.@$file['upload_file'];  ?>" />
								<?php } ?>

								<?php if(strpos(@$file['upload_type'],"video")!==false){ ?>
									<!--<embed src="<?php //echo $base.$file['upload_file']; ?>" autostart="false" height="30" width="144" />-->
									<video width="100%" height="230" class="img-responsive" src="<?php echo $base.@$file['upload_file']; ?>" type="<?php echo @$file['upload_type'] ?>" controls  > </video>
								<?php } ?>

								<?php if(strpos(@$file['upload_type'],"audio")!==false){ ?>
									<audio controls="" style="width: 100%;">
										<source src="<?php echo $base.@$file['upload_file']; ?>" type="<?php echo @$file['upload_type'] ?>"/>
											Your browser does not support the audio element.
									</audio>
								<?php } ?>

								<?php if(strpos(@$file['upload_type'],"application")!==false){ ?>
									<iframe style="width: 100%;" src='https://docs.google.com/viewer?url=<?php echo $base.@$file['upload_file']; ?>&embedded=true' frameborder='0'></iframe>
								<?php } ?>

								<?php if(strpos(@$file['upload_type'],"text")!==false){ ?>
									<iframe style="width: 100%;" src='https://docs.google.com/viewer?url=<?php echo $base.@$file['upload_file']; ?>&embedded=true' frameborder='0'></iframe>
								<?php } ?>
								<!--<a  href="<?php //echo base_url('static_views/productVideoDetail'); ?>" class="form-control video-label text-center">Title</a>-->
							</div>



									<div class="page-header">
										<div class="pull-left">
											<a class="paragraph-text-sm-grey-bold" href="<?php echo base_url('site_assets/products/'.$id.'/'. $file['upload_file']); ?>">Deliver File - <?php echo $file_name; ?></a>
										</div>
										<div class="pull-right">
											<a class="paragraph-text-sm-grey-bold text-capital" href="<?php echo base_url('product/editFile/' .$id.'/'.$file['id']); ?>">Edit</a>
										</div>

										<div class="pull-left">
											<p class="paragraph-text-sm-grey">
												We will deliver you file to your customer over a secure link.

											</p>
										</div>
										<div class="clearfix"></div>
									</div>




									<?php
								}
							}     
							?>

							<?php
							foreach ($mails as $mail) {
								$file_name = $mail['subject'];
								?>
								<div class="page-header">
									<div class="pull-left">
										<a class="paragraph-text-sm-grey-bold">Deliver Email - <?php echo $file_name; ?></a>
									</div>
									<div class="pull-right">
										<a class="paragraph-text-sm-grey-bold text-capital" href="<?php echo
										base_url('product/editMail/' .$id.'/'.$mail['id']); ?>">Edit</a>
									</div>

									<div class="clearfix"></div>
								</div>
								<?php
							}
							?>




						</div>
					</div><!-- block end-->

					<div class="col-md-6 manage-purshase-block "><!--block start-->
						<div class="">
							<div class="nauk-info-connections">
								<h4 class="heading-md-blue">Add new</h4>
							</div>

							<div class="nauk-info-connections  border-dark-1" style="padding:10px;">
								<h2 class="paragraph-text-sm-grey-bold">Add New File</h2>
								<p class="paragraph-text-sm-grey">
									We will provide a secure download button to your customer to download the file.

								</p>
								<div class="text-center mx-auto">
									<a class="btn btn-sm-blue" href="<?php echo
									base_url('product/addFile/' . $id); ?>">add file</a>

								</div>
								<br>

								<h2 class="paragraph-text-sm-grey-bold">Add New Email</h2>
								<p class="paragraph-text-sm-grey">
									Sends an email to the user with information that you enter here. This could include information or contact support.

								</p>
								<div class="text-center mx-auto">
									<a class="btn btn-sm-blue" href="<?php echo
									base_url('product/addMail/' . $id); ?>">add e-mail </a>
								</div>
							</div>


						
						</div>
					</div>
				</div>

			</div>
		</div>