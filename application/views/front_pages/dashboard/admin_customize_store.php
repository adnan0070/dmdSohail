<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<?php
			$success_msg = $this->session->flashdata('success_msg');
			$error_msg = $this->session->flashdata('error_msg');


			if ($success_msg) {
				?>
				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $error_msg; ?>
				</div>
				<?php
			}

			?>
		</div>
		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Store home customization form</h2>
							<p class="paragraph-text-sm-grey">Add your store home page content.</p>
						</div>
					</div>
				</div>
			</div>

		</div>


		<form method="post" enctype="multipart/form-data" action="<?php echo
		base_url('customization/customize_store'); ?>">
		<input type="hidden" name="storeID" value="<?php echo $storeID;?>">
		<input type="hidden" name="requestID" value="<?php echo $requestID;?>">
		<div class="row clearfix form">	
			<div class="col-md-12 text-center">
				<label for="short-description-editor" class="heading-md-grey">Logo</label>
				<div id="imagePreview" style="background-repeat: no-repeat;background-size: contain; background-position:center;" class="nauk-info-connections block-file-upload block-border-dotted text-center">

					<h3 class="heading-sm-grey text-center" style="display: none;">Upload Logo</h3>
					<div class="form-group">

						<input style="display: none;" id="imfile" type="file" value="" name="logo" class="file-input-product" onchange="preview_image(this)" placeholder="Select from computer">

					</div>
					<div class="form-group" style="margin-bottom:0px;">
						<input type="button" onclick="upload_img()" value="Upload Image" class="btn-sm-blue btn" />
					</div>

					<div class="form-group" style="display: none;">

						<input type="checkbox" class="border-dark-1" name="enable"> 
						<span class="paragraph-sm-grey">You can also drag and drop files.</span>

					</div>
					<!--<span class="text-danger">Validation error</span>-->
				</div> 
			</div> 


			<div class="col-md-12 text-center">
				<label for="short-description-editor" class=" heading-md-grey">Banner Image</label>
				<div id="bannerPreview" style="background-repeat: no-repeat;background-size: cover;" class="nauk-info-connections block-file-upload block-border-dotted text-center">

					<h3 class="heading-sm-grey text-center" style="display: none;">Upload Banner</h3>
					<div class="form-group">

						<input style="display: none;" id="bannerfile" type="file" value="" name="banner" class="file-input-product" onchange="preview_image_banner(this)" placeholder="Select from computer">

					</div>
					<div class="form-group" style="margin-bottom:0px;">
						<input type="button" onclick="upload_banner()" value="Upload Image" class="btn-sm-blue btn" />
					</div>

					<div class="form-group" style="display: none;">

						<input type="checkbox" class="border-dark-1" name="enable"> 
						<span class="paragraph-sm-grey">You can also drag and drop files.</span>

					</div>

				</div> 
			</div> 

			<div class="col-md-12 editor ">
				<label for="short-description-editor" class=" heading-md-grey">Banner Description</label>
				<div class="form-group">
					<textarea name="bannerText"  maxlength="3000" rows="20" class="form-control form-textarea "></textarea>
				</div>
			</div> 


			<div class="col-md-12 editor ">
				<label for="short-description-editor" class=" heading-md-grey">Welcome Description</label>
				<div class="form-group">
					<textarea name="welcome"  maxlength="3000" rows="20" class="form-control form-textarea"></textarea> 
				</div>
			</div>  

			<div class="col-md-12 editor ">
				<label for="short-description-editor" class=" heading-md-grey">About us Description</label>
				<div class="form-group">
					<textarea name="about"  maxlength="3000" rows="20" class="form-control form-textarea "></textarea> 
				</div>
			</div> 


			<div class="form-footer">
				<input type="submit" class="btn-form btn" value="save"/>
			</div>


		</div>

	</form>

</div>

</div>
<script>
	function upload_img(){
		$('#imfile').click();
	}
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#imagePreview').css('background-image', 'url('+e.target.result +')');
				$('#imagePreview').css('background-repeat', 'no-repeat');
				$('#imagePreview').css('background-size', 'cover');
                    //$('#imagePreview').attr('src', e.target.result);
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
                $("#old_file").val("");
            }
        }

        function preview_image(eventt){
        	readURL(eventt);
        }
    </script>

    <script>
    	function upload_banner(){
    		$('#bannerfile').click();
    	}
    	function readURLBanner(input) {
    		if (input.files && input.files[0]) {
    			var reader = new FileReader();
    			reader.onload = function(e) {
    				$('#bannerPreview').css('background-image', 'url('+e.target.result +')');
    				$('#bannerPreview').css('background-repeat', 'no-repeat');
    				$('#bannerPreview').css('background-size', 'cover');
                    //$('#imagePreview').attr('src', e.target.result);
                    $('#bannerPreview').hide();
                    $('#bannerPreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
                $("#old_file").val("");
            }
        }

        function preview_image_banner(eventt){
        	readURLBanner(eventt);
        }
    </script>