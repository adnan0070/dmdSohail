<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<?php 

		if($this->session->flashdata('error_msg')){
			?>
			<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
			</div>

			<?php
		}

		if($this->session->flashdata('success_msg')){
			?>

			<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
			</div>


		<?php } ?>
		<form method="post" enctype="multipart" action="<?php echo
		base_url('customization/request_featured_products'); ?>">

		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Featured Products</h2>
							<p class="paragraph-text-sm-grey">Select your store front products.</p>
						</div>
						<div class="pull-right">
							<input type="submit" class="btn-form btn" value="save"/>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
			<input  type="hidden" name="requestID" value="<?php echo $requestID;?>">
			<input  type="hidden" name="submitted" value="featured">
		</div><br><br>
		<div class="row">
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				
				<table id="tbl_articles" class="table table-bordered table-striped" style="width:100%">
					<thead>
						<tr>
							<th>Code</th>
							<th>Product</th>
							<th>Price</th>
							<!--<th>label</th>-->
							<th>Image</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php  foreach($products as $product): ?>
							<tr>
								<td><?php echo $product['product_code']; ?></td>
								<td><?php echo $product['product_name']; ?></td>
								<td><?php echo $product['product_price']; ?></td>
								<!--
								<td>
									<?php $style ='color:'.$product['label_color'].';border-radius:50px;background:#fff;padding:2px 10px;border:1px solid '.$product['label_color'].';';?>
									<a href="#" target="_blank" class="btn btn-success" style="<?php echo $style; ?>:">
										<?php echo $product['label']; ?>
									</a>
								</td>-->
								<td><img width="60" height="60" src="<?php echo base_url('site_assets/products/') . $product['feature_image']; ?>" /></td>
								<td class="actions"><input type="checkbox" name="productIDs[]" value="<?php echo $product['id']; ?>"> </td>
							</tr>
						<?php endforeach;?>
						
					</tbody>
				</table>
			</div>
		</div>
	</form>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#tbl_articles').DataTable({
				//"pagingType": "full_numbers",
				"ordering": false,
			//"searching": true,
			"bLengthChange": false,
			"bInfo": false,
	        //"dom": '<"top"i>rt<"bottom"flp><"clear">'
	    });
	});
</script>