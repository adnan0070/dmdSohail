<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">
			<?php 

			if(form_error('email')){
				?>
				<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					
					<strong>Error!</strong> <?php echo form_error('email');?>
				</div>
				<?php 
			} 

			if($this->session->flashdata('error_msg')){
				?>
				<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

					<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
				</div>

				<?php
			}

			if($this->session->flashdata('success_msg')){
				?>

				<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
				</div>


			<?php } ?>

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">
								Manage Sub admins
							</h2>

						</div>
						<div class="pull-right">
							<!--<a class="btn-sm-blue btn" href="#">create new</a>-->
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
		</div><!-- inner-body-end-->

		<form method="post"  action="<?php echo base_url('subadmin/add')?>" >
			<div class="row clearfix form">	

				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="text"  
						name="first_name"
						class="home-input"
						id="name"
						placeholder="First Name"
						value="<?php echo $this->input->post('first_name') ;?>">
						<label>Enter first name</label>
						<span class="focus-border"></span>
					</div>
				</div> 

				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="text"  
						name="last_name"
						class="home-input"
						placeholder="Last Name"
						value="<?php echo $this->input->post('last_name') ;?>">
						<label>Enter last name</label>
						<span class="focus-border"></span>
					</div>
				</div> 

				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="email"  
						name="email"
						class="home-input"
						placeholder="Email Address"
						value="<?php echo $this->input->post('email') ;?>">
						<label>Enter email</label>
						<span class="focus-border"></span>
					</div>
				</div> 





				<br>

				<div class="col-md-12 text-center">
					<div class="form-footer">
						<button type="submit" class="btn-form btn">save</button>
					</div>
				</div>


				<input type="hidden" name="sub_admin" value="add">
			</div>

		</form>



	</div>



</div>
