<div class="row main-body mx-auto ">

	<?php $this->load->view('front_pages/dashboard/dash_left');?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">


			<div class="col-md-12 inner-body-head"><!-- full block start-->
			<!--	<?php
				$success_msg = $this->session->flashdata('success_msg');
				$error_msg   = $this->session->flashdata('error_msg');

				if ($success_msg) {
					?>

					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $success_msg; ?>
					</div>

					<?php
				}
				?>
			-->
<!--
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
				</div>
			-->

			<div class="nauk-info-connections">
				<div class="page-header">
					<div class="pull-left">
						<?php
						$success_msg = $this->session->flashdata('success_msg');
						$error_msg   = $this->session->flashdata('error_msg');

						if ($success_msg) {
							?>

							<p class="text-success"><?php echo $success_msg; ?></p>

							<?php
						}
						?>

						<?php if ($error_msg) { ?>
							<p class="text-danger"><?php echo $error_msg; ?></p>

						<?php } ?>
						<h2 class="heading-lg-green">Edit Store Preferences</h2>
						<p class="paragraph-text-sm-grey">Modify your store settings and address </p>
					</div>
					<div class="pull-right">
						<a data-toggle="modal" data-target="#delete-store" href="#" class="btn btn-danger btn-sm btn-rounded" style="color:#fff;padding:5px 15px;">Delete</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>



	</div>

	<form method="post" enctype="multipart/form-data" action="<?php echo base_url('store/update/').$id; ?>">
		<div class="row clearfix form">

			<div class="col-md-12">
				<p class="heading-md-blue">store information</p> 
				<div class="form-group input-effects">
					<input type="hidden" id="idv" value="<?php echo $id; ?>" />
					<input type="text"  value="<?php echo ((isset($store['store_name'])) ? $store['store_name'] : ''); ?>"  name = "store[storeName]" class = "home-input" id="storeName"  placeholder=""/>
					<label>store name</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group input-group input-effects">
					<input type="text" value="<?php
					if (isset($store['store_url'])) {
						$store_url = explode(".", $store['store_url']);
						echo $store_url[0];
					}?>"  name = "store[subdomain]" class = "form-control form-input border-dark-1" id="subdomain" placeholder="store url">
					<!-- <div class="input-group-append">
						<span class="input-group-text form-input input-prepend"><?php echo $store_url[1]; ?></span>
					</div> -->


					<!--<span class="text-danger">Validation error</span>-->
				</div>
				<p id="subdomail-error" class="text-center"></p>
			</div>
			<div class="col-md-12">
				<div class="form-group input-group">
					<label><a class="paragraph-text-xs-grey" style="border-bottom:1px solid rgb(100,100,100); font-weight:600;" href="<?php echo "http://".@$store['store_url']; ?>" target="_blank"><?php echo @$store['store_url']; ?></a></label>  
					<!--<input class = "form-control form-input border-dark-1" type="text" disabled="" value="<?php echo @$store['store_url']; ?>" />-->

				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group input-effects">
					<select name="store[currency]" class="home-input" placeholder="">
						<option class="form-input form-input-lg border-dark-1">currency</option>
						<option value="USD">USD</option>
					</select>
					<label>Select Currency</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group input-effects">
					<input type="text"  value="<?php echo ((isset($store['contact_person'])) ? $store['contact_person'] : ''); ?>"  name = "store[contactPerson]" class = " home-input" id="contactPerson"  placeholder=""/>
					<label>contact name</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group input-effects">
					<input type="email" value="<?php echo ((isset($store['support_email'])) ? $store['support_email'] : ''); ?>"  name = "store[supportEmail]" class = " home-input" id="supportEmail"  placeholder=""/>
					<label>contact e-mail</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>


			<div class="col-md-12">
				<p class="heading-md-blue">business information</p> 
				<div class="form-group input-effects">
					<input type="text"  name = "store[address1]" class = " home-input" id="address1" value="<?php echo ((isset($store['address1'])) ? $store['address1'] : ''); ?>"  placeholder=""/>
					<label>business address # 1</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group input-effects">
					<input type="text"  name = "store[address2]" class = " home-input"  value="<?php echo ((isset($store['address2'])) ? $store['address2'] : ''); ?>"  placeholder=""/>
					<label>business address # 2</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group input-effects">
					<input type="text"  value="<?php echo ((isset($store['city'])) ? $store['city'] : ''); ?>"  name = "store[city]" class = " home-input"  placeholder=""/>
					<label>city</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group input-effects">
					<input type="text"  value="<?php echo ((isset($store['state'])) ? $store['state'] : ''); ?>" name = "store[state]" class = " home-input"  placeholder=""/>
					<label>state/county</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group input-effects">
					<input type="text"  value="<?php echo ((isset($store['zip_code'])) ? $store['zip_code'] : ''); ?>"  name = "store[zip_code]" class = " home-input"  placeholder=""/>
					<label>zip code/postal code</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group input-effects">
					<input type="text"  value="<?php echo ((isset($store['country'])) ? $store['country'] : ''); ?>"  name = "store[country]" class = " home-input"  placeholder=""/>
					<label>country</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group  input-effects">
					<input type="text"    value="<?php echo ((isset($store['vat_id'])) ? $store['vat_id'] : ''); ?>"  name = "store[vat_id]" class = " home-input"  placeholder=""/>
					<label>valid id</label>
					<span class="focus-border"></span>
					<!--<span class="text-danger">Validation error</span>-->
				</div>
			</div>

			<div class="col-md-12 editor">
				<label for="return-policy-editor" class=" heading-md-grey">return policy</label>
				<div class="form-group">
					<textarea name="store[sales_policy]" id="return-policy-editor" class="form-control form-input border-dark-1"><?php echo ((isset($store['sales_policy'])) ? $store['sales_policy'] : ''); ?></textarea>
					<textarea style="display: none;" name="store[sales_policy]" id="put_sales_policy"></textarea>
					<!-- <span class="text-danger">Validation error</span> -->
				</div>
			</div>

			<div class="col-md-12 editor">
				<label for="privacy-policy-editor" class=" heading-md-grey">privacy policy</label>
				<div class="form-group">
					<textarea id="privacy-policy-editor" class="form-control form-input border-dark-1"><?php echo ((isset($store['privacy_policy'])) ? $store['privacy_policy'] : ''); ?></textarea>
					<textarea style="display: none;" name="store[privacy_policy]" id="put_privacy_policy"></textarea>
					<!-- <span class="text-danger">Validation error</span> -->
				</div>
			</div>

			<div class="col-md-12 editor">
				<label for="privacy-policy-editor" class=" heading-md-grey">Terms of services</label>
				<div class="form-group">
					<textarea id="terms-services-editor" class="form-control form-input border-dark-1"><?php echo ((isset($store['terms_services'])) ? $store['terms_services'] : ''); ?></textarea>
					<textarea style="display: none;" name="store[terms_services]" id="put_terms_services"></textarea>
					<!-- <span class="text-danger">Validation error</span> -->
				</div>
			</div>

			<br>

			<div class="form-footer">
				<!-- <a class="btn-sm-blue btn" href="#">save</a> -->
				<input value="save" style="display: none;" id="submit_pro" type="submit" name="update_store" class="btn-sm-blue btn" />
				<!-- <input class="btn-sm-blue btn" onclick="get_descrip()" type="submit" id="update_store" value="save" name="update_store" > -->
				<input value="Save" onclick="get_descrip()" type="button" class="btn-form btn" />
			</div>


		</div>

	</form>

	<div class="modal fade " id="delete-store">
		<div class="modal-dialog">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Delete Store</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					Are you sure you want to delete this store? 
				</div>

				<!-- Modal footer -->
				<div class="modal-footer text-right">
					<a class="btn default-btn-grey" id="deleteStore">Yes</a>
					<!-- <a class="btn default-btn-grey">No</a> -->
				</div>

			</div>
		</div>
	</div>
	<script>
		$('#deleteStore').on('click', function(event){
			event.preventDefault();
			//console.log('run');

			$.ajax({
				type: "post",
				url: '<?php echo base_url("store/delete_store") ?>',
				data: {delete: 'delete'},
				success: function(data){
					console.log(data);
					if(data == 'deleted'){
						$('#delete-store').modal('hide');
						setTimeout(function(){ 
							window.location.replace('<?php echo base_url('user/dashboard'); ?>');
						}, 500);
					}
				}
			});
		});


		$(document).ready(function() {
			$("#return-policy-editor").Editor();
			$("#privacy-policy-editor").Editor();
			$("#terms-services-editor").Editor();
			var store_return_policy = "<?php echo
			addslashes($store['sales_policy']); ?>";
			$("#return-policy-editor").Editor("setText", store_return_policy);

			var store_privacy_policy = "<?php echo
			addslashes($store['privacy_policy']); ?>";
			$("#privacy-policy-editor").Editor("setText", store_privacy_policy);
			var store_terms_services = "<?php echo addslashes(json_encode($store['terms_services'])); ?>";
			$("#terms-services-editor").Editor("setText", store_terms_services);
		});
		function get_descrip(){
                     // $("#put_short_desp").html($("#short-description-editor").Editor("getText"));
                     $("#put_sales_policy").html($("#return-policy-editor").Editor("getText"));
                     $("#put_privacy_policy").html($("#privacy-policy-editor").Editor("getText"));
                     $("#put_terms_services").html($("#terms-services-editor").Editor("getText"));
                     $("#submit_pro").click();
                 }
             </script>
             <script type="text/javascript">
             	function subdomainCheck(){
             		var error = $("#subdomail-error");
             		var value = $(this).val();
             		var idv = $("#idv").val();
             		console.log(value);

             		if(value){
             			$.ajax({
             				type: 'post',
             				url: '<?php echo base_url("store/check_subdomain") ?>',
             				data: 'subdomain=' + value+'&thisid=' + idv,
             				success: function(msg){
             					console.log(msg);
             					error.html(msg);
             				}
             			});
             		}else{
             			error.html('');
             		}
             	}

             	$('#subdomain').on({
             		keyup: subdomainCheck,
             		click: subdomainCheck,
             		select: subdomainCheck
             	});

             	$('#subdomain').keypress(function (e) {
             		var allowedChars = new RegExp("^[a-zA-Z0-9\-\b]+$");
             		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
             		if (allowedChars.test(str)) {
             			return true;
             		}
             		e.preventDefault();
             		return false;
             	}).keyup(function() {
	    // the addition, which whill check the value after a keyup (triggered by Ctrl+V)
	    // We take the same regex as for allowedChars, but we add ^ after the first bracket : it means "all character BUT these"
	    var forbiddenChars = new RegExp("[^a-zA-Z0-9\-\b]", 'g');
	    if (forbiddenChars.test($(this).val())) {
	    	$(this).val($(this).val().replace(forbiddenChars, ''));
	    }
	});

</script>
</div>

</div>