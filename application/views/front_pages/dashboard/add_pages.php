<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div id="showError"></div>
				<?php
				$action = "page_insert";
				if(isset($page_items)){
					$page_items = $page_items[0];
					$action = "update";
				}

				$message = $this->session->flashdata('message');
				if ($message) { ?>
					<div class="alert alert-info" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
						<?php echo $message; ?>
					</div>
				<?php } ?>

				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Edit Page</h2>
							<p class="paragraph-text-sm-grey">Edit Pages and contents</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>

		<form method="post" id="frmEditor" action="<?php echo base_url('pages/') . $action; ?>" enctype="multipart/form-data">
			<div class="row clearfix form">

				<div class="col-md-12">
					<div class="form-group">
						<input type="text"  
						name="title"
						class="form-control form-input border-dark-1 validThis"
						id="title"
						placeholder="Page Title"
						value="<?php if(isset($page_items)){echo $page_items->title;} ?>"
						/>
						
					</div>
				</div>
				
				<?php if(isset($page_items)){ 
					$content = $page_items->content;
					?>
					<input type="hidden" name="id" value="<?php echo $page_items->id; ?>" />
				<?php } ?>

				<div class="col-md-12">
					<div class="form-group">
						<select name="slug" id="slug" class="form-control form-input select-text-center form-input-lg border-dark-1 validThis" placeholder="Select a slug">
							
							<option value="" class="form-input form-input-lg border-dark-1" selected disabled>Select a slug</option>
							
							<option value="about_us" class="form-input form-input-lg border-dark-1">About Us</option>
							<!-- <option value="support" class="form-input form-input-lg border-dark-1">Support</option> -->
							<option value="affiliate_program" class="form-input form-input-lg border-dark-1">Affiliate Program</option>
							
							<option value="legal_notice" class="form-input form-input-lg border-dark-1">Legal Notice</option>
							<option value="dmca_notice" class="form-input form-input-lg border-dark-1">DMCA Notice</option>
							<option value="terms_service" class="form-input form-input-lg border-dark-1">Terms of Service</option>
							
							<option value="storeRefundPolicy" class="form-input form-input-lg border-dark-1">Refund Policy</option>
							<option value="storePrivacyPolicy" class="form-input form-input-lg border-dark-1">Privacy Policy</option>
							<option value="storeTermsServices" class="form-input form-input-lg border-dark-1">Store Terms of Service</option>

						</select>
						<script type="text/javascript">
							<?php 
							if(isset($page_items->slug)){
								?>

								$("#slug").val("<?php echo $page_items->slug; ?>");

								<?php
							}
							?>
						</script>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<input type="text"  
						name="meta_keyword"
						class="form-control form-input border-dark-1 validThis"
						id="slmeta_keywordug"
						placeholder="Meta Keyword"
						value="<?php if(isset($page_items)){echo $page_items->meta_keyword;} ?>"
						/>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<input type="text"
						name="meta_description"
						class="form-control form-input border-dark-1 validThis"
						id="meta_description"
						placeholder="Meta Description"
						value="<?php if(isset($page_items)){echo $page_items->meta_description;} ?>"
						/>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<select name="status" id="status" class="validThis form-control form-input select-text-center form-input-lg border-dark-1" placeholder="Status">
							<option value="" class="form-input form-input-lg border-dark-1" selected disabled>Status</option>
							<option value="1" class="form-input form-input-lg border-dark-1">Active</option>
							<option value="0" class="form-input form-input-lg border-dark-1">Deactive</option>
						</select>
					</div>
				</div> 

				<script type="text/javascript">
					<?php 
					if(isset($page_items->status)){
						?>

						$("#status").val("<?php echo $page_items->status; ?>");

						<?php
					}
					?>
				</script>

				<div class="col-md-12 editor">
					<label for="email-editor" class=" heading-md-grey">Page Content</label>
					<div class="form-group">
						<textarea name="content" 
						id="content"
						class="form-control form-input border-dark-1"
						></textarea> 
					</div>
				</div> 


				<div class="form-footer">
					<input type="submit" id="add_page" value="save" class="default-btn-green btn" />
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	$(document).ready(function() {
		var editor = $("#content");
		editor.Editor();
		var s = "<?php echo addslashes($content); ?>";
		//console.log(s);
		if(s){
			editor.Editor("setText", s);
		}

		$("#add_page").on("click", function(e){
			e.preventDefault();

			$("#content").val($("#content").Editor("getText"));
			//$("#frmEditor").submit();
			formValidate("frmEditor");
		});

	});
</script>