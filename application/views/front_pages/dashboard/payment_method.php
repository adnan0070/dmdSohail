<div class="row main-body mx-auto ">
<?php
$left = 'front_pages/dashboard/dash_left';

$user_role = $this->session->userdata('user_role');
if($user_role == 2){
    $left = 'front_pages/dashboard/admin_dash_left';
}
$this->load->view($left);

?>

            
				<div class="col-md-9 inner-body payment-methods"><!-- inner-body-start-->

					<div class="row">
						<?php 
                        $pmName = array();
						if(@$pm_names){

							$pmName = array();
							foreach ($pm_names as $key => $value) {
								$pmName[] = $value['name'];
							}

							/*echo '<pre>';
							print_r($pmName);
							echo '</pre>';*/

						}
						?>

						<div class="col-md-12 inner-body-head"><!-- full block start-->
							<div class="nauk-info-connections">
								<div class="page-header">
									<div class="pull-left">
										<h2 class="heading-lg-green">Payment methods</h2>
										<p class="paragraph-text-sm-grey">Integrate payment methods for getting paid.</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div><!-- block end-->

						<div class="col-md-4 method-block"><!-- full block start-->
							<div class="nauk-info-connections text-center border-dark-1">
								<img src="<?php echo base_url(); ?>site_assets/images/stripe_payment.png">
								<?php 
								
								$url = base_url('store/paymentMethod/stripe');
								$txt = "add";

								if($user_role == 2){
									$url = base_url('admin/payment_method/stripe');
								}


								if($pmName){
									if(in_array("stripe", $pmName)){
										$url = base_url('store/paymentMethod/stripe');
										$txt = "setings";
										if($user_role == 2){
											$url = base_url('admin/payment_method/stripe');
										}
									}
								}
								?>
								<a class="btn-sm-blue btn" href="<?php echo $url; ?>"><?php echo $txt; ?></a>
							</div>
						</div>

						<div class="col-md-4 method-block"><!-- full block start-->
							<div class="nauk-info-connections text-center border-dark-1">
								<img src="<?php echo base_url(); ?>site_assets/images/authorize_payment.png">
								<?php 
								
								$url = base_url('store/paymentMethod/authorize');
								$txt = "add";

								if($user_role == 2){
									$url = base_url('admin/payment_method/authorize');
								}

								if($pmName){
									if(in_array("authorize", $pmName)){
										$url = base_url('store/paymentMethod/authorize');
										$txt = "setings";

										if($user_role == 2){
											$url = base_url('admin/payment_method/authorize');
										}
									}
								}
								?>
								<a class="btn-sm-blue btn" href="<?php echo $url; ?>"><?php echo $txt; ?></a>
							</div>
						</div>

						<div class="col-md-4 method-block"><!-- full block start-->
							<div class="nauk-info-connections text-center border-dark-1">
								<img src="<?php echo base_url(); ?>site_assets/images/paypal_payment.png">
								<?php 
								
								$url = base_url('store/paymentMethod/paypal');
								$txt = "add";

								if($user_role == 2){
									$url = base_url('admin/payment_method/paypal');
								}

								if($pmName){
									if(in_array("paypal", $pmName)){
										$url = base_url('store/paymentMethod/paypal');
										$txt = "setings";
										if($user_role == 2){
											$url = base_url('admin/payment_method/paypal');
										}
									}
								}
								?>
								<a class="btn-sm-blue btn" href="<?php echo $url; ?>"><?php echo $txt; ?></a>
							</div>
						</div>

						<div style="display: none;" class="col-md-4 method-block"><!-- full block start-->
							<div class="nauk-info-connections text-center border-dark-1">
								<img src="<?php echo base_url(); ?>site_assets/images/sage_payment.png">
								<?php 
								
								$url = base_url('store/paymentMethod/sage');
								$txt = "add";

								if($user_role == 2){
									$url = base_url('admin/payment_method/sage');
								}

								if($pmName){
									if(in_array("sage", $pmName)){
										$url = base_url('store/paymentMethod/sage');
										$txt = "setings";
										if($user_role == 2){
											$url = base_url('admin/payment_method/sage');
										}
									}
								}
								?>
								<a class="btn-sm-blue btn" href="<?php echo $url; ?>"><?php echo $txt; ?></a>
							</div>
						</div>



					</div><!-- inner-body-end-->
				</div>
			
		
		
		
	</div>