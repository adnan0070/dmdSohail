<div class="row main-body mx-auto ">
 <?php $this->load->view('front_pages/dashboard/dash_left'); ?>



			<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
                      <div class="col-md-12">
                      <?php
$success_msg = $this->session->flashdata('success_msg');
$error_msg = $this->session->flashdata('error_msg');


if ($success_msg) {
?>
                   <div class="alert alert-success" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $success_msg; ?>
                    </div>
                  <?php
}
if ($error_msg) {
?>
                       <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $error_msg; ?>
                    </div>
                    <?php
}
?>
          </div>

				<div class="row">

					<div class="col-md-12 inner-body-head"><!-- full block start-->
						<div class="nauk-info-connections">
							<div class="page-header">
								<div class="pull-left">
									<h2 class="heading-lg-green">All My Products</h2>
									<p class="paragraph-text-sm-grey">Manage all your products.</p>
								</div>
								<div class="pull-right">
									<a class="btn-form btn" href="<?php echo base_url('product/add'); ?>">create Product</a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div><!-- block end-->
					<div class="col-md-12 inner-body-head">
                    <?php 
                    $user_id = $this->session->userdata('user_id');
                    $store_id = $this->session->userdata('store_id');
                    $live_pro = $this->db->query('select count(id) as live from dmd_store_products where store_id="'.$store_id.'"
                    and product_visibility="live"');
                    $live_pro = $live_pro->row_array();
                    $arch_pro = $this->db->query('select count(id) as arch_pro from dmd_store_products where store_id="'.$store_id.'"
                    and product_visibility="archived"');
                    $arch_pro = $arch_pro->row_array();
                    
                    
                    ?>
						<p> <span class="paragraph-text-sm-green">LIVE PRODUCTS&nbsp;(<?php echo $live_pro['live']; ?>)&nbsp;</span><span class="paragraph-text-sm-grey">|&nbsp;ARCHIVED PRODUCTS&nbsp;(<?php echo $arch_pro['arch_pro']; ?>)</span></p>
					</div>
				</div>

				<div class="row">
                
                <?php foreach($products as $product){ ?>
                 	<div class="col-md-4 product" >
						<div class="nauk-info-connections text-left product-view border-dark-1">
							<img class="product-image"  src="<?php echo base_url(); ?>site_assets/products/<?php echo $product['pro_image'];  ?>">
							<p class="paragraph-text-sm-blue"><?php echo stripslashes($product['product_name']);  ?></p>
							<p class="paragraph-text-sm-grey">Status: <?php echo $product['product_visibility'];  ?><span> </span></p>
							<p class="paragraph-text-sm-grey">Product ID: <?php echo $product['product_code'];  ?></p>
							<p class="paragraph-text-sm-grey">Price: $<?php echo $product['product_price'];  ?></p>
						</div>
						<div class="nauk-info-connections text-center product-detail  border-dark-1">
							<p class="paragraph-text-sm-blue"><?php echo stripslashes($product['product_name']);  ?></p>
							<p class="paragraph-text-sm-blue">Project ID: <?php echo $product['product_code'];  ?></p>
							<p class="product-detail-text-red"><a  href="<?php echo base_url('product/detail/'.$product['product_code']); ?>"> View Detail</a></p>
							<p class="product-detail-text-red"><a  href="<?php echo base_url('product/'.$product['product_code']); ?>"> Edit</a></p>
							<p class="product-detail-text-red"><a  href="<?php echo base_url('product/PurchaseAction/'.$product['product_code']); ?>"> Actions</a></p>
							<p style="display: none;" class="product-detail-text-red"><a  href="<?php echo base_url('product/sendToDownload/'.$product['product_code']); ?>">Send Products</a></p>
							<p class="product-detail-text-red"><a  href="<?php echo base_url('product/purchase/'.$product['product_code']); ?>"> Purchases</a></p>
						</div>
					</div>
                    <?php } ?>

				</div>
			</div>
		</div>