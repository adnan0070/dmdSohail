<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>




	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">
			<?php 

			if($this->session->flashdata('error_msg')){
				?>
				<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
				</div>

				<?php
			}

			if($this->session->flashdata('success_msg')){
				?>

				<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
				</div>


			<?php } ?>

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">customers</h2>
							<p class="paragraph-text-sm-grey">Generate report for customers, and their purchases</p>
						</div>
					</div>
				</div>
			</div>

		</div>

		<form id="frm_filter">
			<div class="row form border-dark-1 filter-block" style="margin:20px 0px;">

				<div class="col-md-12 mx-auto">
					<div class="nauk-info-connections text-center">
						<label for="long-description-editor" class="heading-md-grey">filter</label>
					</div>
				</div>	

				<div class="col-md-6">

					<div class="form-group input-effects">
						<select  name="email" class="home-input" placeholder="">
							<option class="form-input form-input-lg border-dark-1" value="" selected>Choose Email</option>
							<?php
							if($customers){
								foreach ($customers as $key => $value) {
									$emails[] = $value["email"];
								}

								$email_list = array_unique($emails);
								for ($i=0; $i < count($email_list) ; $i++) {
									?>
									<option class="form-input form-input-lg border-dark-1" value="<?php echo $email_list[$i]; ?>">
										<?php echo $email_list[$i]; ?>
									</option>
									<?php
								}
							}

							?>

						</select>
						<label>Choose Email</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>

				<div class="col-md-6">

					<div class="form-group input-effects">
						<select  name="product" class="home-input" placeholder="product">
							<option class="form-input form-input-lg border-dark-1" value="" selected>Choose Product</option>
							<?php
							if($products){
								foreach ($products as $key => $value) { ?>
									<option class="form-input form-input-lg border-dark-1" value="<?php echo $value['product_code']; ?>">
										<?php echo $value['product_name']; ?>
									</option>
									<?php
								}
							}
							?>
						</select>
						<label>Choose Product</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>




				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="text"  name = "f_name" class = "home-input" id="first-name"  placeholder=""/>
						<label>first name</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div> 
				<div class="col-md-6">
					<div class="form-group input-effects">
						<input type="text"  name = "l_name" class = "home-input" id="last-name"  placeholder=""/>
						<label>last name</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div> 

				<div class="col-md-6">

					<div class="form-group input-effects">
						<select name="shipping_country" id="shipping-country" class="home-input" placeholder="">
							<option class="form-input form-input-lg border-dark-1" value="" selected>Select Country</option>
							<?php
							if($customers){
								foreach ($customers as $key => $value) {
									$countries[] = $value["shipping_country"];
								}

								$country_list = array_unique($countries);

								for ($i=0; $i < count($country_list); $i++) { 

									?>
									<option class="form-input form-input-lg border-dark-1" value="<?php echo $country_list[$i]; ?>">
										<?php echo $country_list[$i]; ?>
									</option>
									<?php
								}
							}

							?>
						</select>
						<label>Select Country</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>

				<div class="col-md-2 input-dates-small-lg">

					<div class="form-group input-effects">
						<input type="text" id="fromdate" name="from" class="datepicker home-input" placeholder="" />
						<label style="text-transform:lowercase;">mm/dd/yyyy</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>

				<div class="col-md-2 text-center input-dates-small-sm input-effects">
					<label for="long-description-editor" class=" paragraph-text-md-grey" style="margin-top:10px;">To</label>
				</div> 
				<div class="col-md-2 input-dates-small-lg">

					<div class="form-group input-effects">
						<input type="text" id="todate" name="to" class="datepicker home-input" placeholder="" />
						<label style="text-transform:lowercase;">mm/dd/yyyy</label>
						<span class="focus-border"></span>
					</div>
				</div>


				<div class="col-md-12 mx-auto">
					<span id="todateError" class="text-danger"></span>
					<div class="nauk-info-connections text-center">
						<button type="submit" id="filter" class="btn-sm-blue btn">apply filters</button>
						<a href="/customer" id="clear" class="btn-sm-blue btn">clear filters</a>
					</div>
				</div>	


			</div>
		</form>


		<div class="row purchases">

			<div class="col-md-12 block"><!-- full block start-->
				<div class="nauk-info-connections border-green">
					<!-- Default form register -->

					<div class="page-header">
						<div class="pull-left">
							<p id="cst_count" class="paragraph-text-sm-grey" style="padding-left:10px">
								<?php
								/*if($customers){
									echo count($customers) . " CUSTOMERS";
								}*/
								?>
							</p>
						</div>
						<div class="pull-right" style="padding-right:10px;">
							<button id="export" class="btn-sm-blue btn">Export</button>
						</div>
						<div class="pull-right" style="padding-right:10px;">
							<a href= "<?php echo base_url('customer/import'); ?>" class="btn-sm-blue btn">Import</a>
						</div>
						<div class="clearfix"></div>
					</div>

					<table id="cst" class="table table-bordered table-striped" style="width:100%">
						<thead class="back-border">
							<tr>
								<th scope="col">Name</th>
								<th scope="col">Email</th>
								<th scope="col"># of Sales</th>
								<th scope="col">Total Sales ($)</th>

							</tr>
						</thead>
						<tbody id="tb">
							<?php if($customers){
								foreach ($customers as $key => $value) { ?>
									<tr>
										<td><?php echo $value["f_name"] . " " . $value["l_name"]; ?></td>
										<td><?php echo $value["email"]; ?></td>
										<td>
											<?php 
											if(isset($value["count_sale"])){
												echo $value["count_sale"]; 
											}else{
												echo "0";
											}
											?>
										</td>
										<td>
											<?php 
											if(isset($value["total_sale"])){
												echo $value["total_sale"]; 
											}else{
												echo "0";
											}
											?>
										</td>
									</tr>
								<?php }
							} ?>
						</tbody>
					</table>




				</div>

			</div>

		</div>
	</div>


</div>

<script>

$(document).ready(function(){
    $('#cst').DataTable({
        "pagingType": "full_numbers",
        "ordering": false,
        "searching": false,
        "bLengthChange": false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">'
    });
});
//$(document).ready(function() {
	$("#filter").on("click", function(e){
		e.preventDefault();

		var from = Date.parse($("#fromdate").val());
		var to = Date.parse($("#todate").val());
		//console.log(from + ", " + to);
		if(from > to){
			$("#todate").focus();
			$("#todateError").html("To date must be greater than From date.");
			return false;
		}

		var dt = $("#frm_filter").serialize();
		//console.log(dt);
		$.ajax({
			type: "post",
			url: "/customer/filter",
			data: dt,
			dataType: "json",
			success: function(msg){
				$("#tb").html(msg.table);
				$("#cst_count").html(msg.count);
				$('div#cst_paginate').hide();
				//console.log(msg);
			}
		});
	});

	$("#export").on("click", function(){
		var form = $("#frm_filter");
		var dt = form.serialize();

		form.submit(function (){
			var action = '/customer/export_file';
			$(this).attr('action', action);
			$(this).attr('method', 'post');
		});

		form.submit();
	});
//});
</script>