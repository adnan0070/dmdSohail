<div class="row main-body mx-auto ">
	<?php 
	$this->load->view('front_pages/dashboard/admin_dash_left'); 
	//print_r($users);
	?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<?php
			if($this->session->flashdata('error_msg')){
				?>
				<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
				</div>

				<?php
			}
			if($this->session->flashdata('success_msg')){
				?>
				<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
				</div>
			<?php } ?>

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">User Management</h2>
							<p class="paragraph-text-sm-grey">Manage all your users.</p>
						</div>
						<div class="pull-right">
							<a href="<?php echo base_url('Admin/addUser')?>"  class="btn-sm-default btn pricing-btn"><i class="fa fa-plus"></i> New</a>
							<a  id ="message-btn" href="#"  class="btn-sm-default btn pricing-btn"><i class="fa fa-envelope"></i> Message</a>

						</div>
						<!-- data-toggle="modal" data-target="#message-user" -->
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->
		</div><br><br>
		<div class="row">
			<div class="col-md-12">
				&nbsp;<input class="user-check" type="checkbox" id="all-user-checkbox" value="">  Message to All users in platform 
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<!-- <div class="input-group">
					<input type="text" class="form-control" placeholder="Search by user, store name here ...." >
					<div class="input-group-append">
						<button class="btn btn-outline-secondary btn-search-list" type="button">Search</button>
					</div>
				</div> -->
				
				<table id="cats" class="table table-bordered table-striped" style="width:100%">
					<thead>
						<tr>
							<th><input id="check-all" type="checkbox" name="checkAll" value=""></th>
							<th>Name</th>
							<th>Email</th>
							<th>Since</th>
							<th>Status</th>
							<th>Stores</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						if ($users) {
							foreach ($users as $key => $user) {
								if($user['user_role'] != 2){
									?>
									<tr>
										<td><input class="user-check" type="checkbox" name="usersID" value="<?php echo $user['id']; ?>"> </td>
										<td><?php echo $user['display_name']; 
										if($user['is_admin_created'] == 1){ ?>

											<?php ?>
											<a href="<?php echo site_url('admin/managePackage/') . $user['id']; ?>" class="btn default-btn-grey">Manage package</a>
										<?php } ?>
									</td>
									<td><?php echo $user['email']; ?></td>
									<td><?php echo date('Y-m-d', strtotime($user['user_registered'])); ?></td>
									<td>
										<?php 
										if($user['status'] == 1){
											echo 'Active';
										}elseif($user['status'] == 0){
											echo 'Blocked';
										}
										?>
									</td>
									<td><a href="<?php echo site_url('admin/stores/') . $user['id']; ?>" class="btn default-btn-grey">Stores</a></td>
									<td>
										<!-- <a data-toggle="modal" data-target="#block-user" href="#" class="btn default-btn-grey">Block</a> -->
										<a data-status="<?php echo $user['status']; ?>" data-user="<?php echo $user['id']; ?>" data-target="#block-user" href="#" class="btn default-btn-grey blockUser">
											<?php 
											if($user['status'] == 0){
												echo 'Active';
											}elseif($user['status'] == 1){
												echo 'Block';
											}
											?>
										</a>

										<a id="<?php echo $user['id']; ?>" class="btn delete-user-btn default-btn-grey">Delete</a>											
									</td>

								</tr>

								<?php
							}
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>

<div class="modal action-user-modal fade " id="block-user">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">User Management</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<p id="txt"></p>
				<input type="hidden" id="userID" name="userID" value="">
				<input type="hidden" id="userStatus" name="userStatus" value="">
			</div>

			<!-- Modal footer -->
			<div class="modal-footer text-right">
				<a class="btn default-btn-grey" id="blockUsers">Yes</a>
				<a class="btn default-btn-grey" data-dismiss="modal">No</a>
			</div>

		</div>
	</div>
</div>

<div class="modal fade " id="message-user">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Enter  message</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form method="post" enctype="multipart" action="<?php echo
			base_url('admin/send_message'); ?>">
			<input type="hidden" value="" id="selected-users" name="users">
			<?php if ($users) {
				$arr = array();
				foreach ($users as $key => $user) {
					array_push($arr, $user['id']);
				}
				$arr = implode(',',$arr);
			} ?>
			<input type="hidden" value="<?php echo $arr; ?>" id="all-users" name="AllUsers">
			<input type="hidden" value="0" id="selected-all" name="all">
			<div class="modal-body">

				<div class="row">
					<div class="col-md-12 editor ">
						<label for="short-description-editor" class=" heading-md-grey"></label>
						<div class="form-group">
							<textarea name="message"  maxlength="5000" rows="20" class="form-control form-textarea "></textarea> 
						</div>
					</div>
				</div> 
			</div>
			<div class="modal-footer text-right">
				<!-- <a class="btn default-btn-grey">Yes</a> -->
				<input type="submit" class="default-btn-grey" onclick="loading()" value="Send Email">
			</div>
		</form>
	</div>
</div>
</div>



<div class="modal fade " id="delete-user">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Manage users</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form method="post" enctype="multipart" action="<?php echo
			base_url('admin/delete'); ?>">
			<input type="hidden" value="" id="delete-user-id" name="user">
			<div class="modal-body">

				<div class="row">
					<div class="col-md-12 editor ">
						<div class="form-group">
							<p class="paragraph-text-sm-grey">Are you sure you want to delete this user.</p> 
						</div>
					</div>
				</div> 
			</div>
			<div class="modal-footer text-right">
				<a class="btn default-btn-grey" data-dismiss="modal">No</a>
				<input type="submit" class="default-btn-grey" onclick="loading()" value="Yes">
			</div>
		</form>
	</div>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#message-btn").click(function(){
			if($("#selected-all").val() == 0){
				var len = $("input[name='usersID']:checked").length;
				if(len > 0){
					var favorite = [];
					$.each($("input[name='usersID']:checked"), function(){            
						favorite.push($(this).val());
						console.log($(this).val());
					});

					console.log(favorite.length);
					$("#selected-users").val(favorite);
					console.log($("#selected-users").val());
					$("#message-user").modal('toggle');
				}
			}
			if ( ($("#selected-all").val() == 1)) {}{
				$("#message-user").modal('toggle');
			}
		});

		$("#check-all").click(function(){
			if($("input[name='checkAll']").prop("checked") == true){
				$("input[name='usersID']").prop('checked',true);	
			}else{
				$("input[name='usersID']").prop('checked', false);	
			}$("#selected-all").val('0');

		});

		$("#all-user-checkbox").click(function(){
			if($("#all-user-checkbox").prop("checked") == true){
				$("#selected-all").val('1');	
			}else{
				$("#selected-all").val('0');
			}
		});
		$(".delete-user-btn").click(function(){
			$("#delete-user-id").val($(this).attr('id'));
			$("#delete-user").modal('toggle');
		});

	});

	$(document).ready(function(){
		$('#cats').DataTable({
			//"pagingType": "full_numbers",
			"ordering": false,
			//"searching": true,
			"bLengthChange": false,
			"bInfo": false,
	        //"dom": '<"top"i>rt<"bottom"flp><"clear">'
	    });
	});
</script>

<script type="text/javascript">
	$('.blockUser').on('click', function(event){
		event.preventDefault();
		var userID = $(this).data('user');

		var userStatus = $(this).data('status');

		if(userStatus == 1){
			$('#txt').text('Are you sure to block this user ?');
		}else if(userStatus == 0){
			$('#txt').text('Are you sure to active this user ?');
		}

		$('.modal-body #userID').val(userID);
		$('.modal-body #userStatus').val(userStatus);
		$('.action-user-modal').modal('show');
		console.log(userStatus);
	});
	$('#blockUsers').on('click', function(event){
		event.preventDefault();
		var userID = $('#userID').val();
		var userStatus = $('#userStatus').val();
		console.log(userStatus);
		if(userStatus == 1){
			window.location.replace('<?php echo base_url('admin/block_user/'); ?>' + userID);
		}else if(userStatus == 0){
			window.location.replace('<?php echo base_url('admin/active_user/'); ?>' + userID);
		}
		//console.log('<?php echo base_url('admin/active_user/'); ?>' + userID);
	});

	function loading(){
		$("#main-loader").show();
	}
</script>