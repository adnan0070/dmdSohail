<div class="row main-body mx-auto ">
 <?php $this->load->view('front_pages/dashboard/dash_left'); ?>

            <div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

				<div class="row">

					<div class="col-md-12 inner-body-head"><!-- full block start-->
						<div class="nauk-info-connections">
							<div class="page-header">
								<div class="pull-left">
									<h2 class="heading-lg-green">SALES REPORTS</h2>
									<p class="paragraph-text-sm-grey">Generate report for many sales are generated.</p>
								</div>
							</div>
						</div>
					</div>
				</div>




				<div class="row form border-green filter-block-sales" style="margin:20px 0px; padding:10px">

					<div class="col-md-12 mx-auto">
						<div class="nauk-info-connections text-center">
							<label for="long-description-editor" class=" heading-md-grey">filter</label>
						</div>
					</div>	


					<div class="col-md-2">

						<div class="form-group">
							<select  id="from-date" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="MM.DD.YY">
								<option class="form-input form-input-lg border-dark-1"> MM.DD.YY</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>

					<div class="col-md-2 text-center">
						<label for="long-description-editor" class=" paragraph-text-md-grey" style="margin-top:10px;">TO</label>
					</div> 
					<div class="col-md-2">

						<div class="form-group">
							<select  id="to-date" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="MM.DD.YY">
								<option class="form-input form-input-lg border-dark-1"> MM.DD.YY</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>


					<div class="col-md-6">
						<div class="form-group">
							<select  class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="select shipping period">
								<option class="form-input form-input-lg border-dark-1">select shipping period</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>

					<div class="col-md-6">

						<div class="form-group">
							<select  id="product" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="report for">
								<option class="form-input form-input-lg border-dark-1"> report for</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<select  id="product" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="graph level detail">
								<option class="form-input form-input-lg border-dark-1"> report for</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>


					<div class="col-md-12 mx-auto">
						<div class="nauk-info-connections text-center">
							<a class="btn-sm-blue" href="#">apply filters</a>
						</div>
					</div>	

				</div>

				<div class="row form border-green filter-block-sales" style="margin:20px 0px;">

					<div class="col-md-6">
						<div class="form-group">
							<select  id="shipping-country" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="select report">
								<option class="form-input form-input-lg border-dark-1"> select report</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
								<option class="form-input form-input-lg border-dark-1">lorem ipsum</option>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<select  onchange="getval(this);" id="graph-list" class="form-control form-input select-text-center form-input-lg border-dark-1" placeholder="chart type">
								<option class="form-input form-input-lg border-dark-1" value="0">chart type</option>
								<option class="form-input form-input-lg border-dark-1" value="1">Bar chart</option>
								<option class="form-input form-input-lg border-dark-1" value="2">Line chart</option>
							</select>
							<!--<span class="text-danger">Validation error</span>-->
						</div>
					</div>

					<div class="col-md-12 mx-auto">
						<div class="nauk-info-connections text-center">
							<label for="long-description-editor" class=" heading-md-grey">sales</label>
						</div>
					</div>	


					

					<div class="col-md-12">
						<div class="nauk-info-connections text-center" > 
							<div class=" graph" id="bars"> </div>
						</div>
					</div>
					<script src="<?php echo base_url(); ?>site_assets/js/graphite.js"></script>
					<script src="<?php echo base_url(); ?>site_assets/js/main.js"></script>

					<div class="col-md-12">
						<div class="nauk-info-connections text-center" id="line-chart"> 
							<canvas id="graph" width="500" height="400" align="center"></canvas>
							<script>		
							$( document ).ready(function() {
								var chartData = {
									node: "graph",
									dataset: [55, 99, 101, 80, 26, 55, 55, 99, 101, 80, 26, 55],
									labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
									pathcolor: "#288ed4",
									fillcolor: "#8e8e8e",
									xPadding: 0,
									yPadding: 0,
									ybreakperiod: 50
								};
								drawlineChart(chartData);
							});
							</script>
							<script src="<?php echo base_url(); ?>site_assets/js/topup.js"></script>

						</div>
					</div>

				</div>
			</div>
		
	</div>