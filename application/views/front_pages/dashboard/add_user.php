<div class="row main-body mx-auto ">

	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="row">
			<div class="col-md-12">
				<?php
				$success_msg = $this->session->flashdata('success_msg');
				$error_msg = $this->session->flashdata('error_msg');

				if ($success_msg) {
					?>
					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $success_msg; ?>
					</div>
					<?php
				}
				if ($error_msg) {
					?>

					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?php echo $error_msg;?>
					</div>
					<?php
				}
				?>
				<div id="showError"></div>
			</div>
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Add new user</h2>
							<p class="paragraph-text-sm-grey">Enter user information.</p>
							<p class="paragraph-text-sm-grey margin-none">
								<i class="fa fa-info" style="color:#468fb1;"></i> 
								Email will be send to user email address with password.
							</p>
							<p class="paragraph-text-sm-grey margin-none">
								<i class="fa fa-info" style="color:#468fb1;"></i> 
								Can assign package to user without any payment.
							</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</br></br></br>
	<form method="post" id="pkg" enctype="multipart/form-data" action="<?php echo
	base_url('Admin/addUser'); ?>">

	<div class="row form">
		<div class="col-md-6">
			<div class="form-group input-effects">
				<input type="text" 
				name = "firstName" 
				id="name"
				class="home-input validThis"
				value="<?php echo $this->input->post('firstName'); ?>" 
				required
				placeholder="First Name"/>
				<label>First Name</label>
				<span class="focus-border"></span>
				<span class="text-danger"></span>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group input-effects">
				<input type="text" 
				value="<?php echo $this->input->post('lastName'); ?>"  
				name = "lastName" 
				required
				class="home-input validThis"
				placeholder="Last Name"/>
				<label>Last Name</label>
				<span class="focus-border"></span>
				<span class="text-danger"></span>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group input-effects">
				<input type="email" 
				value="<?php echo $this->input->post('email'); ?>"  
				name = "email"
				required
				class="home-input validThis" 
				placeholder="Email address"/>
				<label>Email address</label>
				<span class="focus-border"></span>
				<span class="text-danger"></span>					
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group input-effects">
				<select required id="time-period" name = "package" class="home-input validThis">
					<?php foreach ($packages as $package) {
						echo "<option   value='".$package['pack_code']."'>".$package['pack_title']."</option>";
					}
					?>
				</select>
				<label>Package</label>
				<span class="focus-border"></span>
				<span class="text-danger"></span>
			</div>
		</div>
		<div class="col-md-12 mx-auto">
			<div class="nauk-info-connections text-center">
				<input type="hidden" name="user" value="add" />
				<button 
				type="submit" 
				class="btn-form btn" >Save</button>
			</div>
		</div>

	</form>
</div>	
</div>	
</div>	

