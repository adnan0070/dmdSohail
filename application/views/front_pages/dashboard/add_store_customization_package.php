<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->


		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Add new customization Package</h2>
							<p class="paragraph-text-sm-grey">Manage all your package.</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->

		</div>


		<div class="row">
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div id="showError"></div>
				<?php
				$message = $this->session->flashdata('message');
				if ($message) { ?>
					<div class="alert alert-info" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
						<?php echo $message; ?>
					</div>
				<?php } ?>
			</div>
		</div>

		<form method="post" action="<?php echo base_url('customization/add'); ?>" enctype="multipart/form-data">
			<div class="row clearfix">

				<div class="col-md-6">
					<div class="form-group">
						<input type="text"  
						name="title"
						class="form-control form-input border-dark-1"
						id="title"
						placeholder="Package Title"
						value="<?php echo $this->input->post("title"); ?>"
						/>
						
					</div>
				</div>


				<div class="col-md-6">
					<div class="form-group">
						<input type="text"
						oninput="this.value=this.value.replace(/[^0-9]/g,'');" 
						name="price"
						class="form-control form-input border-dark-1"
						id="price"
						placeholder="Price"
						value="<?php echo $this->input->post("price"); ?>"
						/>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>
						<input type="checkbox" name="enable_form" class="border-dark-1">
						<span class="paragraph-dark-md-capital">Enable form</span>
					</label>
				</div>
			</div>

			<div class="row label-row"  id="parent-hrps3">
				<div class="col-10 col-xs-10 col-sm-9 col-md-8 col-lg-8 col-xl-8">
					<div class="form-group">
						<input type="text"
						name="label[]"
						class="form-control form-input border-dark-1"
						placeholder="Package label"
						value=""
						/>
					</div>
				</div>

				<div class="col-2 col-xs-2 col-sm-3 col-md-4 col-lg-4 col-xl-4">
					<div class="form-group">
						<a href="#" class="remove-label-btn" id="hrps3"><i class="fa fa-times"></i> Remove</a>
					</div>
				</div>
			</div>

			<div class="row label-row " id="add-label-row">
				<div class="form-footer col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-left">
					<a href="#" id ="add-label-btn" class="default-btn-green btn">Add</a>
				</div>

				<div class="form-footer col-md-12">
					<button type="submit" class="btn-form btn">Save</button>
				</div>
			</div>
			<input type="hidden" name="submitted" value="add">
		</form>
	</div>
</div>


<script>
	// $(".remove-label-btn").click(function(){
		$(document).on('click', '.remove-label-btn', function (event) {

			var id = $(this).attr('id');
			$("#parent-"+id).remove();

		});


		$("#add-label-btn").click(function(event){
			id = Math.random().toString(36).substring(7);
			var html = "";
			html += "<div class='row label-row'  id='parent-"+id+"'>";
			html += "<div class='col-10 col-xs-10 col-sm-9 col-md-8 col-lg-8 col-xl-8'>";
			html += "<div class='form-group'>";
			html += "<input type='text'";
			html += "name='label[]'";
			html += "class='form-control form-input border-dark-1' placeholder='Package label' value=''>"; 
			html += "</div>";
			html += "</div>";
			html += "<div class='col-2 col-xs-2 col-sm-3 col-md-4 col-lg-4 col-xl-4'>";
			html += "<div class='form-group'>";
			html += "<a href='#' class='remove-label-btn' id='"+id+"'><i class='fa fa-times'></i> Remove</a>";
			html += "</div>";
			html += "</div>";
			html += "</div>";
			$(html).insertBefore("#add-label-row");
		});
	</script>