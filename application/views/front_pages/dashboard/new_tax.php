<div class="row main-body mx-auto ">
 <?php $this->load->view('front_pages/dashboard/dash_left'); ?>

			<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

				<div class="row">
					<div id="showError"></div>

					<?php 

					 	if($this->session->flashdata('error_msg')){
					 		?>
							<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
						  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  		<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
							</div>

					 		<?php
					 	}

					 	if($this->session->flashdata('success_msg')){
					 		?>
							
							<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
						  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  		<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
							</div>


					 <?php } ?>

					<div class="col-md-12 inner-body-head"><!-- full block start-->
						<div class="nauk-info-connections">
							<div class="page-header">
								<div class="pull-left">
									<h2 class="heading-lg-green">
										<?php 
										if(isset($coupon)){
											echo "edit tax";
										}else{
											echo "create new tax";
										}
										?>
									</h2>
									
								</div>
								<div class="pull-right">
									<!--<a class="btn-sm-blue btn" href="#">create new</a>-->
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div><!-- block end-->
				</div><!-- inner-body-end-->

				<?php
					$action = "";
					if(isset($coupon)){
						$action = "tax_edit";
					}else{
						$action = "tax_create";
					}
				?>

				<form method="post" id="frmtax" action="<?php echo base_url() . 'store/' . $action; ?> " >
					<div class="row clearfix form">	
						<div class="col-md-12">
							<div class="form-group input-effects">
								<!-- <select name="country" class="home-input validThis" placeholder="">
									<option value="" class="form-input form-input-lg border-dark-1" selected disabled>Select Country</option>
									<option value="pk" class="form-input form-input-lg border-dark-1">Pakistan</option>
									<option value="usa" class="form-input form-input-lg border-dark-1">United States</option>
								</select> -->
								<select name="country" class="bfh-countries home-input validThis" data-country="<?= (isset($coupon)) ? $coupon['country'] : 'US'; ?>"></select>
								<label>Select Country</label>
								<span class="focus-border"></span>
							</div>
						</div> 

						<?php
							if(isset($coupon)){
								//print_r($coupon);
								$id = $coupon["id"];
								echo '<input type="hidden" name="id" value="' . $id . '" />';
							}
						?>

						<div class="col-md-6">
							<div class="form-group input-effects">
								<input type="text"  
									name="name"
									class="home-input validThis"
									id="name"
									placeholder=""
									value="<?php if(isset($coupon)){echo $coupon['name'];}?>"/>
									
									<label>name</label>
									<span class="focus-border"></span>
							</div>
						</div> 

						<div class="col-md-6">
							<div class="form-group input-effects">
								<input type="text"  
									name="price"
									class="home-input validThis"
									id="price"
									placeholder=""
									value="<?php if(isset($coupon)){echo $coupon['price'];}?>"/>
									
									<label>tax percentage</label>
									<span class="focus-border"></span>
							</div>
						</div> 

						

						
						
						<br>




					</div>

				</form>
				
				<div class="form-footer">
					<button onclick="formValidate('frmtax')" class="btn-form btn">save</a>
				</div>

			</div>
		
		
		
	</div>
