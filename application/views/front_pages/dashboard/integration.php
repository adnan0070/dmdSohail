<div class="row main-body mx-auto ">
 <?php $this->load->view('front_pages/dashboard/dash_left'); ?>

            
				<div class="col-md-9 inner-body payment-methods"><!-- inner-body-start-->

					<div class="row">
                    <div class="col-md-12">
                      <?php
$success_msg = $this->session->flashdata('success_msg');
$error_msg = $this->session->flashdata('error_msg');


if ($success_msg) {
?>
                   <div class="alert alert-success" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
                      </button>
                        <?php echo $success_msg; ?>
                    </div>
                  <?php
}
if ($error_msg) {
?>
                       <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
                      </button>
                        <?php echo $error_msg; ?>
                    </div>
                    <?php
}
?>
          </div>
                    
						<?php 
						if($pm_names){

							$pmName = array();
							foreach ($pm_names as $key => $value) {
								$pmName[] = $value['name'];
							}

							/*echo '<pre>';
							print_r($pmName);
							echo '</pre>';*/

						}
						?>

						<div class="col-md-12 inner-body-head"><!-- full block start-->
							<div class="nauk-info-connections">
								<div class="page-header">
									<div class="pull-left">
										<h2 class="heading-lg-green">Integrations</h2>
										<p class="paragraph-text-sm-grey">Integrate third party applications to enhance your bussiness performance.</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div><!-- block end-->

						<div class="col-md-4 method-block"><!-- full block start-->
							<div class="nauk-info-connections text-center border-dark-1">
								<img  src="<?php echo base_url(); ?>site_assets/images/payment-method-9.png">
								<?php 
								
								$url = base_url('store/integration/mailchimp');
								$txt = "add";

								if(@$pmName){
									if(in_array("mailchimp", $pmName)){
										$url = base_url('store/integration/mailchimp');
										$txt = "settings";
									}
								}
								?>
								<a class="btn-sm-blue btn" href="<?php echo @$url; ?>"><?php echo @$txt; ?></a>
							</div>
						</div>

						<div class="col-md-4 method-block"><!-- full block start-->
							<div class="nauk-info-connections text-center border-dark-1">
								<img style="width: 190px;" src="<?php echo base_url(); ?>site_assets/images/payment-method-10.png">
								<?php 
								
								$url = base_url('store/integration/getresponse');
								$txt = "add";

								if(@$pmName){
									if(in_array("getresponse", $pmName)){
										$url = base_url('store/integration/getresponse');
										$txt = "setings";
									}
								}
								?>
								<a class="btn-sm-blue btn" href="<?php echo @$url; ?>"><?php echo @$txt; ?></a>
							</div>
						</div>
<!-- 
						<div class="col-md-4 method-block">
							<div class="nauk-info-connections text-center border-dark-1"><br>
								<img src="<?php echo base_url(); ?>site_assets/images/payment-method-7.png">
								<?php 
								
								$url = base_url('store/integration/iconnect');
								$txt = "add";

								if(@$pmName){
									if(in_array("iconnect", $pmName)){
										$url = base_url('store/integration/iconnect');
										$txt = "setings";
									}
								}
								?>
								<a class="btn-sm-blue btn" href="<?php echo @$url; ?>"><?php echo @$txt; ?></a>
							</div>
						</div> -->

					<!-- 	<div class="col-md-4 method-block">
							<div class="nauk-info-connections text-center border-dark-1">
								<img  src="<?php echo base_url(); ?>site_assets/images/payment-method-8.png">
								<?php 
								
								$url = base_url('store/integration/authorize');
								$txt = "add";

								if(@$pmName){
									if(in_array("authorize", $pmName)){
										$url = base_url('store/integration/authorize');
										$txt = "setings";
									}
								}
								?>
								<a class="btn-sm-blue btn" href="<?php echo @$url; ?>"><?php echo @$txt; ?></a>
							</div>
						</div> -->

					</div><!-- inner-body-end-->
				</div>
			
		
		
		
	</div>