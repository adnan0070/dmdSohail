<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>



	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">

			<?php
			if($this->session->flashdata('error_msg')){
				?>
				<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
				</div>

				<?php
			}
			if($this->session->flashdata('success_msg')){
				?>
				<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
				</div>
			<?php } ?>

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Coupon Management</h2>
							<p class="paragraph-text-sm-grey">Manage all your coupon for your products.</p>
						</div>
						<div class="pull-right">
							<a class="btn-form btn" href="<?php echo base_url('store/coupons/create'); ?>">Create new</a>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form>
			<div class="row form ">

				<div class="col-md-6 padding-right-none">
					<div class="form-group input-effects">
						<input type="text"  name="name" id="cName" class="home-input" id="first-name"  placeholder=""/>
						<label>coupon name</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div> 

				<div class="col-md-6 padding-right-none">

					<div class="form-group padding-right-none input-effects">
						<select  id="cStatus" class="home-input" placeholder=" ">
							<option value="" class="form-input form-input-lg border-dark-1" selected disabled>Status</option>
							<option value="live" class="form-input form-input-lg border-dark-1">Live</option>
							<option value="expired" class="form-input form-input-lg border-dark-1">Expired</option>
						</select>
						<label>coupon status</label>
						<span class="focus-border"></span>
						<!--<span class="text-danger">Validation error</span>-->
					</div>
				</div>

			</div>
		</form>


		<div class="row purchases">

			<div class="col-md-12 block padding-right-none"><!-- full block start-->
				<div class="nauk-info-connections border-green">
					<table id="coupon" class="table table-bordered table-striped" style="width:100%">
						<thead class="back-border">
							<tr>
								<th scope="col">Code</th>
								<th scope="col">Discount</th>
								<th scope="col">Name</th>
								<th scope="col">Active</th>
								<th scope="col">Expires on</th>
								<th scope="col">Uses Remaining</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody id="cBody">
							<?php
									 //echo "<pre>";
									 //print_r($data);
									 //echo "</pre>";
							foreach ($coupons as $key => $value) { ?>
								<tr>
									<td class="paragraph-text-sm-grey"><?php echo $value['code'];?></td>		 	
									<td class="paragraph-text-sm-grey"><?php echo $value['discount'];?></td>		 	
									<td class="paragraph-text-sm-grey"><?php echo $value['name'];?></td>
									<td class="paragraph-text-sm-grey"><?php echo $value['active_date'];?></td>
									<td class="paragraph-text-sm-grey"><?php echo $value['expire_date'];?></td>
									<td class="paragraph-text-sm-grey"><?php echo $value['max_use'];?></td>
									<td class="paragraph-text-sm-grey">
										<a class="btn btn-xs" href="<?php echo base_url('store/coupons/edit') . '/' . $value['id']; ?>">Edit</a>
										<a class="btn btn-xs" href="<?php echo base_url('store/coupons/delete') . '/' . $value['id']; ?>">Delete</a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>

			</div>

		</div>
	</div>		
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#coupon').DataTable({
			"pagingType": "full_numbers",
			"ordering": false,
			"searching": false,
			"bLengthChange": false,
        //"bInfo": false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">'
    });
	});
</script>

