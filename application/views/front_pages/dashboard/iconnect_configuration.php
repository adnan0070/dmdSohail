<div class="row main-body mx-auto ">
 <?php $this->load->view('front_pages/dashboard/dash_left'); ?>

            <div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

				<div class="row">

					<div class="col-md-12">
                      <?php
$success_msg = $this->session->flashdata('success_msg');
$error_msg = $this->session->flashdata('error_msg');


if ($success_msg) {
?>
                   <div class="alert alert-success" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
                      </button>
                        <?php echo $success_msg; ?>
                    </div>
                  <?php
}
if ($error_msg) {
?>
                       <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
                      </button>
                        <?php echo $error_msg; ?>
                    </div>
                    <?php
}
?>
<style>
.text-danger{
    display:none;
}
</style>
          </div>
              <?php
                 $store_id = $this->session->userdata('store_id');
                 $user_ID = $this->session->userdata('user_id');
                 $query = $this->db->query("select configuration,status from dmd_integrations where store_id='".$store_id."' and name='iconnect'");
                 $configuration = array();
                 if($query->num_rows()>0){
                 $configuration = $query->row_array();
                 $status = $configuration['status'];
                 $configuration = json_decode($configuration['configuration'],true);
                 
                 
                 }
                ?>

					<div class="col-md-12 inner-body-head"><!-- full block start-->
						<div class="nauk-info-connections">
							<div class="page-header">
								<div class="pull-left">
									<h2 class="heading-lg-green">Icontact configuration</h2>
									<p class="paragraph-text-sm-grey">Integrate for better configuration</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div><!-- block end-->

				</div><!-- inner-body-end-->

				<form action="/store/saveiconnectLists" method="post">
					<div class="row clearfix form">						
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" id="getkey" value="<?php echo @$configuration['appId'];  ?>" onblur="change_set(this)" name="iconnectPost[appId]" class="form-control form-input form-input-lg border-dark-1" placeholder="App Id">
								<span class="text-danger">Required AppId</span>
							</div>
						</div> 
                        <div class="col-md-12">
							<div class="form-group">
								<input type="text" id="getu" value="<?php echo @$configuration['username'];  ?>" onblur="change_set(this)" name="iconnectPost[username]" class="form-control form-input form-input-lg border-dark-1" placeholder="Api Username">
								<span class="text-danger">Required Api Username</span>
							</div>
						</div>
                        <div class="col-md-12">
							<div class="form-group">
								<input type="text" id="getp" value="<?php echo @$configuration['password'];  ?>" onblur="change_set(this)" name="iconnectPost[password]" class="form-control form-input form-input-lg border-dark-1" placeholder="Api Password">
								<span class="text-danger">Required Api Password</span>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group" id="icon_list">
								
								<!--<span class="text-danger">Validation error</span>-->
							</div>
						</div>
                        <div class="col-md-12">
							<ul class="list-inline ">

								<li class="list-inline-item form-checkbox"> 
									<input <?php if (@$status !="" && @$status == "on") {echo "checked";
} ?> type="checkbox" class="border-dark-1" name="enable"> <span class="paragraph-dark-md-capital">Enabled</span>
								</li>

								
							</ul>
						</div>
                        <div class="col-md-12 inner-body-head form"><!-- full block start-->
							<div class="nauk-info-connections">



								<p class="paragraph-text-sm-grey form-paragraph">iConatct APP Authentication
								</p>

								<p class="paragraph-text-sm-grey form-paragraph">IMPORTANT: iContact requires that you authorize DPD to add subscribers to your account. Please do the following:
								</p>

								<p class="paragraph-text-sm-grey form-paragraph">Visit <a class="paragraph-text-sm-grey form-paragraph" href="http://api.icontact.com/icp/core/externallogin" target="_blank"> http://api.icontact.com/icp/core/externallogin</a>.
								</p>

								<p class="paragraph-text-sm-grey form-paragraph">Enter your iContact username and password to log in to iContact. 
								</p>

								<p class="paragraph-text-sm-grey form-paragraph">Enter "  " for the API-APPid (without quotes).

								</p>

								<p class="paragraph-text-sm-grey form-paragraph">Enter a password . Note: For security reasons , we recommend that this password be different than your iContact password.
								</p>

								<p class="paragraph-text-sm-grey form-paragraph">Click Save.
								</p>
								<br>
							</div>
						</div>
                        
                         


						<div class="form-footer">
                        <button type="submit" class="btn-form btn ">save</button>
							<!--
                            <?php if(isset($pm)){ ?>
								<input type="hidden" name="pm_id" value="<?php echo $pm['id']; ?>" />
								<button type="submit" class="btn-sm-blue btn">update</button>
							<?php }else{ ?>
								<button type="submit" class="btn-sm-blue btn ">save</button>
							<?php } ?>
                            -->
						</div>

					</div>
				</form>
			</div>
		
	</div>

<script>
    $(document).ready(function(){
        $('.text-danger').hide();
        change_set("justhit");
    });

    function change_set(tval){
        var getval = $(tval).val();
        var flag = true;
        if(getval=="justhit"){
			flag = false;
        }
        if(flag){
           	if(getval==""){
            	$(tval).parent().find('.text-danger').show();
           	}else{
            	$(tval).parent().find('.text-danger').hide();
        	}
        }

		var getkey = $("#getkey").val(); 
		var getu = $("#getu").val();
		var getp = $("#getp").val(); 
		if(getkey!=""&&getu!=""&&getp!=""){
			$("#icon_list").html('<img>');
			$.post('<?php echo base_url().'store/geticontactLists';?>',
					{"appId":getkey,"username":getu,"password":getp,"listid":"<?php echo  @$configuration['listid']; ?>"},
					function(res){
						$("#icon_list").html(res);
					});
		}    
    }
</script>
<img src="" />