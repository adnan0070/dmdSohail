<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>



	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<?php
			$success_msg = $this->session->flashdata('success_msg');
			$error_msg = $this->session->flashdata('error_msg');


			if ($success_msg) {
				?>
				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $error_msg; ?>
				</div>
				<?php
			}


			?>
		</div>



		<div class="row">
			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">New Featured product</h2>
							<p class="paragraph-text-sm-grey">Customize your store products as a feature products for your store front.</p>
						</div>
					</div>
				</div>
			</div>
		</div>


		<form method="post" enctype="multipart/form-data" action="<?php echo
		base_url('product/updateFeatureProduct' ); ?>">
		<div class="row clearfix form">	

			<div class="col-md-6">
				<div class="form-group input-effects">
					<input type="hidden" name="pro[is_feature]" value="1" />
					<select  name="product_code" class="home-input" placeholder="product">
						<option class="form-input form-input-lg border-dark-1" value="" selected>Choose Product</option>
						<?php
						if($products){
							foreach ($products as $product) { ?>
								<option class="form-input form-input-lg border-dark-1" value="<?php echo $product['product_code']; ?>">
									<?php echo $product['product_name']; ?>
								</option>
								<?php
							}
						}
						?>
					</select>
					<label>Choose Product</label>
					<span class="focus-border"></span>
				</div>
			</div>

			<!--
			<div class="col-md-4">
				<div class="form-group input-effects">
					<input type="text" value=""  name = "pro[label]" class = "home-input" id="subject"  placeholder=""/>
					<label>Label</label>
					<span class="focus-border"></span>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group input-effects">
					<input type="text" value=""  name = "pro[label_color]" class = "home-input" id="subject"  placeholder=""/>
					<label>Label color</label>
					<span class="focus-border"></span>
				</div>
			</div>

			<div class="col-md-12">
				<label class="heading-md-grey">Label color:</label>
				<div class="form-group">
					<span style='background:red; padding:5px;margin-right:10px;color:#fff;'>red</span> 
					<span style='background:black; padding:5px;margin-right:10px;color:#fff;'>black</span>
					<span style='background:#468fb1 ; padding:5px;margin-right:10px;color:#fff;'>#468fb1 </span> 
					<span style='background:#74a857 ; padding:5px;margin-right:10px;color:#fff;'>#74a857 </span> 
					<span style='background:rgb(120,100,100); padding:5px;margin-right:10px;color:#fff;'>rgb(140,100,100)</span> 
					<span style='background:#ddd; padding:5px;margin-right:10px;color:#fff;'>#ddd</span> 
					<br>
					<br>
					<br>
				</div>
			</div>
		-->

		<div class="col-md-6 text-center">
			<input type="hidden" name="old_image_path" id="old_image_path" value="" />
			<input type="hidden" name="old_file" id="old_file" value="" />

			<label for="short-description-editor" class="heading-md-grey">Product image</label>
			<div id="imagePreview" style="background-image: url('<?php echo base_url() .
			"/site_assets/products/"; ?>');background-repeat: no-repeat;background-size: contain; background-position:center;" class="nauk-info-connections block-file-upload block-border-dotted text-center">

			<h3 class="heading-sm-grey text-center" style="display: none;">UPLOAD FILE</h3>
			<div class="form-group">

				<input style="display: none;" id="imfile" type="file" value="" name="image" class="file-input-product" onchange="preview_image(this)" placeholder="Select from computer">

			</div>
			<div class="form-group" style="margin-bottom:0px;">
				<input type="button" onclick="upload_img()" value="Upload Image" class="btn-sm-blue btn" />
			</div>

			<div class="form-group" style="display: none;">

				<input type="checkbox" class="border-dark-1" name="enable"> 
				<span class="paragraph-sm-grey">You can also drag and drop files.</span>

			</div>
			<!--<span class="text-danger">Validation error</span>-->
		</div> 
	</div> 


	<div class="col-md-12 editor ">
		<label for="short-description-editor" class=" heading-md-grey">Description</label>
		<div class="form-group">
			<textarea name="pro[feature_description]"  maxlength="3000" rows="20" class="form-control form-textarea "><?php echo $this->input->post('pro[feature_description]'); ?></textarea> 
		</div>
	</div> 
	<div class="form-footer">
		<input type="submit" class="btn-form btn" value="save"/>
	</div>

</div>

</form>



</div>
</div>
<script>
	function upload_img(){
		$('#imfile').click();
	}
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#imagePreview').css('background-image', 'url('+e.target.result +')');
				$('#imagePreview').css('background-repeat', 'no-repeat');
				$('#imagePreview').css('background-size', 'cover');
                    //$('#imagePreview').attr('src', e.target.result);
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
                $("#old_file").val("");
            }
        }

        function preview_image(eventt){
        	readURL(eventt);
        }
    </script>