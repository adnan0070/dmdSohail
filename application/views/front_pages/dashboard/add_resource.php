<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->
		<div class="col-md-12">
			<?php
			$success_msg = $this->session->flashdata('success_msg');
			$error_msg = $this->session->flashdata('error_msg');


			if ($success_msg) {
				?>
				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $success_msg; ?>
				</div>
				<?php
			}
			if ($error_msg) {
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php echo $error_msg; ?>
				</div>
				<?php
			}

			?>
		</div>
		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Add platform resources</h2>
							<p class="paragraph-text-sm-grey">Add content about resources to promote for other people from your platform.</p>
						</div>
					</div>
				</div>
			</div>

		</div>


		<form method="post" enctype="multipart/form-data" action="<?php echo
		base_url('resources/add'); ?>">
		<div class="row clearfix">
			<div class="col-md-12">
				<br>
				<label for="short-description-editor" class=" heading-md-grey">Select media type</label>	
			</div>
			<div class="col-3 col-xs-3 col-sm-3  col-md-3 col-lg-3 ">
				<label class="form-group" for="image-radio" >
					<input checked class="radio-button" type="radio" name ="fileType" value ="1" id="image-radio"> Image
				</label>
			</div> 
			<div class="col-3 col-xs-3 col-sm-3  col-md-3 col-lg-3">
				<label class="form-group" for="video-radio" >
					<input class="radio-button"  type="radio" name ="fileType" value ="2" id="video-radio"> Video
				</label>
			</div> 
			<div class="col-md-6 text-center" id="image-upload-section">
				<!-- <label for="short-description-editor" class="heading-md-grey">Banner image</label> -->
				<div id="imagePreview" style="background-repeat: no-repeat;background-size: contain; background-position:center;" class="nauk-info-connections block-file-upload block-border-dotted text-center">

					<h3 class="heading-sm-grey text-center" style="display: none;">Upload</h3>
					<div class="form-group">

						<input style="display: none;" id="imfile" type="file" value="" name="mediaFile" class="file-input-product" onchange="preview_image(this)" placeholder="Select from computer" accept="image/*, video/*"> 

					</div>
					<div class="form-group" style="margin-bottom:0px;">
						<input id="file-label" type="button" onclick="upload_img()" value="Upload Image" class="btn-sm-blue btn" />
					</div>

					<div class="form-group" style="display: none;">

						<input type="checkbox" class="border-dark-1" name="enable"> 
						<span class="paragraph-sm-grey">You can also drag and drop files.</span>

					</div>
					<!--<span class="text-danger">Validation error</span>-->
				</div> 
			</div> 


			<div class="col-md-6">
				<div class="item form-group">
					<label class="control-label" for="title">Name</label>
					<input  required type="text" id="name" name="name" value="<?php echo $this->input->post('name') ?>" class="form-control form-input">
					<span class="text-danger"><?php echo form_error('name');?></span>
				</div>
			</div>
			<div class="col-md-6">
				<div class="item form-group">
					<label class="control-label" for="title">Title</label>
					<input  required type="text" id="title" name="title" value="<?php echo $this->input->post('title') ?>" class="form-control form-input">
					<span class="text-danger"><?php echo form_error('title');?></span>
				</div>
			</div>
			<div class="col-md-6">
				<div class="item form-group">
					<label class="control-label" for="title">Button label</label>
					<input required type="text" id="label" name="label" value="<?php echo $this->input->post('label') ?>" class="form-control form-input">
					<span class="text-danger"><?php echo form_error('label');?></span>
				</div>
			</div>
			<div class="col-md-6">
				<div class="item form-group">
					<label class="control-label" for="title">URL</label>
					<input  required type="url" id="url" name="url" value="<?php echo $this->input->post('url') ?>" class="form-control form-input">
					<span class="text-danger"><?php echo form_error('url');?></span>
				</div>
			</div>


			<div class="col-md-12 editor ">
				<label for="short-description-editor" class=" heading-md-grey">Description</label>
				<div class="form-group">
					<textarea  required name="description"  maxlength="3000" rows="20" class="form-control form-textarea "><?php echo $this->input->post('label') ?></textarea>
					<span class="text-danger"><?php echo form_error('description');?></span>
				</div>
			</div> 

			<input name="submitted" type="hidden" value="add">
			<div class="form-footer">
				<input type="submit" class="btn-form btn" value="save">
			</div>


		</div>

	</form>

</div>

</div>
<script>
	$('.radio-button').change(function(){
		var value = $( 'input[name=fileType]:checked' ).val();
		if(value == 1){
			$("#file-label").val('Upload Image');
		}else{
			$("#file-label").val('Upload video');
		}
	});

	function upload_img(){
		$('#imfile').click();
	}
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#imagePreview').css('background-image', 'url('+e.target.result +')');
				$('#imagePreview').css('background-repeat', 'no-repeat');
				$('#imagePreview').css('background-size', 'cover');
                    //$('#imagePreview').attr('src', e.target.result);
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
                $("#old_file").val("");
            }
        }

        function preview_image(eventt){
        	readURL(eventt);
        }
    </script>

    <script>
    	function upload_banner(){
    		$('#bannerfile').click();
    	}
    	function readURLBanner(input) {
    		if (input.files && input.files[0]) {
    			var reader = new FileReader();
    			reader.onload = function(e) {
    				$('#bannerPreview').css('background-image', 'url('+e.target.result +')');
    				$('#bannerPreview').css('background-repeat', 'no-repeat');
    				$('#bannerPreview').css('background-size', 'cover');
                    //$('#imagePreview').attr('src', e.target.result);
                    $('#bannerPreview').hide();
                    $('#bannerPreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
                $("#old_file").val("");
            }
        }

        function preview_image_banner(eventt){
        	readURLBanner(eventt);
        }
    </script>