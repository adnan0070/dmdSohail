<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<div class="row">

			<?php 

			if($this->session->flashdata('error_msg')){
				?>
				<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
				</div>

				<?php
			}

			if($this->session->flashdata('success_msg')){
				?>
				
				<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
				</div>


			<?php } ?>

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Tax Management</h2>
							
						</div>
						<div class="pull-right">
							<a class="btn-form btn" href="<?php echo base_url('store/tax/create'); ?>">Create New</a>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		


		<div class="row purchases">

			<div class="col-md-12 block padding-right-none"><!-- full block start-->
				<div class="nauk-info-connections border-green">
					<table class="table purchase-table">
						<thead class="back-border">
							<tr>
								<th scope="col">Id</th>
								<th scope="col">Name</th>
								<th scope="col">Country</th>
								<th scope="col">Price</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody id="cBody">
							<?php
									 //echo "<pre>";
									 //print_r($data);
									 //echo "</pre>";
							foreach ($coupons as $key => $value) { ?>
								<tr>
									<td class="paragraph-text-sm-grey"><?php echo $value['id'];?></td>		 	
									<td class="paragraph-text-sm-grey"><?php echo $value['name'];?></td>		 	
									<td class="paragraph-text-sm-grey"><?php echo $value['country'];?></td>
									<td class="paragraph-text-sm-grey"><?php echo $value['price'];?></td>
									<td class="paragraph-text-sm-grey">
										<a class="btn btn-xs" href="<?php echo base_url('store/tax/edit') . '/' . $value['id']; ?>">Edit</a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>

			</div>

		</div>
	</div>		
</div>

