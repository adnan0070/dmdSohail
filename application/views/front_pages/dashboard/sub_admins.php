<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<?php 

		if($this->session->flashdata('error_msg')){
			?>
			<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				
				<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
			</div>

			<?php
		}

		if($this->session->flashdata('success_msg')){
			?>

			<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
			</div>


		<?php } ?>
		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Manage Sub admins</h2>
							<p class="paragraph-text-sm-grey">Manage all your sub admin for your account.</p>
						</div>
						<?php if(($package['sub_admins'] > 0) && ($package['sub_admins'] > count($users))): ?>
						<div class="pull-right">
							<a  href="<?php echo base_url().'subadmin/add'?>" class="btn default-btn-green"><i class="fa fa-plus"></i> Add new</a>
						</div>
					<?php endif;?>
					<div class="clearfix"></div>
				</div>
			</div>
		</div><!-- block end-->

	</div>
	<div class="row">
		<div class="col-md-12 inner-body-head"><!-- full block start-->

			<table id="tbl_articles" class="table table-bordered table-striped" style="width:100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Change Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php  foreach($users as $user): ?>
						<tr>
							<td><?php echo $user['first_name'].' '.$user['last_name']; ?></td>
							<td><?php echo $user['email']; ?></td>
							<td class="actions">
								<?php if($user['status'] == 0):?>
									<a data-toggle="modal" data-target="#activate-user-<?php echo $user['id'];?>"  class="btn default-btn-grey" ><i class="fa fa-pencil"></i> Activate</a>
									<?php else: ?>
										<a data-toggle="modal" data-target="#block-user-<?php echo $user['id'];?>"  class="btn default-btn-grey" ><i class="fa fa-pencil"></i> Block</a>

									<?php endif; ?>
								</td>
								<td class="actions"><!-- <a href="<?php echo base_url('subadmin/edit/') . encode($user['id']); ?>" class="btn default-btn-blue" ><i class="fa fa-pencil"></i> Edit</a> -->
									<a data-toggle="modal" data-target="#delete-user-<?php echo $user['id'];?>"  class="btn default-btn-red" ><i class="fa fa-trash"></i> Delete</a>
								</td>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#tbl_articles').DataTable({
			"pagingType": "full_numbers",
			"ordering": false,
			"searching": false,
			"bLengthChange": false,
	        //"bInfo": false,
	        "dom": '<"top"i>rt<"bottom"flp><"clear">'
	    });
	});
</script>

<?php  foreach($users as $user): ?>
	<div class="modal fade " id="block-user-<?php echo $user['id']?>">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Manage sub admins</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<form method="post" enctype="multipart" action="<?php echo
				base_url('subadmin/update/'.encode($user['id'])); ?>">
				<input type="hidden" value="" id="delete-user-id" name="user">
				<div class="modal-body">

					<div class="row">
						<div class="col-md-12 editor ">
							<div class="form-group">
								<p class="paragraph-text-sm-grey">Are you sure you want to block this user.</p> 
							</div>
						</div>
					</div> 
				</div>
				<input type="hidden" name="sub_admin" value="update">
				<input type="hidden" name="status" value="0">
				<div class="modal-footer text-right">
					<a class="btn default-btn-grey" data-dismiss="modal">No</a>
					<input type="submit" class="default-btn-grey" value="Yes">
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade " id="activate-user-<?php echo $user['id']?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Manage sub admins</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form method="post" enctype="multipart" action="<?php echo
			base_url('subadmin/update/'.encode($user['id'])); ?>">
			<input type="hidden" value="" id="delete-user-id" name="user">
			<div class="modal-body">

				<div class="row">
					<div class="col-md-12 editor ">
						<div class="form-group">
							<p class="paragraph-text-sm-grey">Are you sure you want to Activate this user.</p> 
						</div>
					</div>
				</div> 
			</div>
			<input type="hidden" name="sub_admin" value="update">
			<input type="hidden" name="status" value="1">
			<div class="modal-footer text-right">
				<a class="btn default-btn-grey" data-dismiss="modal">No</a>
				<input type="submit" class="default-btn-grey" value="Yes">
			</div>
		</form>
	</div>
</div>
</div>
<div class="modal fade " id="delete-user-<?php echo $user['id']?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Manage sub admins</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form method="post" enctype="multipart" action="<?php echo
			base_url('subadmin/update/'.encode($user['id'])); ?>">
			<input type="hidden" value="" id="delete-user-id" name="user">
			<div class="modal-body">

				<div class="row">
					<div class="col-md-12 editor ">
						<div class="form-group">
							<p class="paragraph-text-sm-grey">Are you sure you want to delete this user.</p> 
						</div>
					</div>
				</div> 
			</div>
			<input type="hidden" name="sub_admin" value="update">
			<input type="hidden" name="status" value="0">
			<div class="modal-footer text-right">
				<a class="btn default-btn-grey" data-dismiss="modal">No</a>
				<a class="btn default-btn-grey" href="<?php echo base_url('subadmin/delete/') . encode($user['id']); ?>">Yes</a>
			</div>
		</form>
	</div>
</div>
</div>
<?php endforeach;?>