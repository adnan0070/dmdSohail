<div class="row main-body mx-auto ">
	<?php $this->load->view('front_pages/dashboard/admin_dash_left'); ?>

	<div class="col-md-9 inner-body dashboard"><!-- inner-body-start-->

		<?php 


		if($this->session->flashdata('error_msg')){
			?>
			<div class="alert alert-danger alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
			</div>

			<?php
		}

		if($this->session->flashdata('success_msg')){
			?>

			<div class="alert alert-success alert-dismissible" style="margin-top: 50px">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Success!</strong> <?php echo trim($this->session->flashdata('success_msg')); ?>
			</div>


		<?php } ?>
		<div class="row">

			<div class="col-md-12 inner-body-head"><!-- full block start-->
				<div class="nauk-info-connections">
					<div class="page-header">
						<div class="pull-left">
							<h2 class="heading-lg-green">Store Data </h2>
							<p class="paragraph-text-sm-grey">Select store front and products.</p>
						</div>
						<div class="pull-right">
							<a href="<?php echo base_url('Customization/customize_form/'.encode($storeID).'/'.encode($requestID)); ?>" class="btn-form btn">Customize</a>

						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div><!-- block end-->





		</div>
		<br><br><br>
		<div class="row clearfix">	
			<div class="col-md-6 text-center">
				<label for="short-description-editor" class="heading-md-grey">Logo</label>
				<div id="imagePreview" style="background-image: url('<?php echo base_url() .
				"site_assets/stores/" . @$formData['logo']; ?>');background-repeat: no-repeat;background-size: contain; background-position:center;" class="nauk-info-connections block-file-upload block-border-dotted text-center">

				<h3 class="heading-sm-grey text-center" style="display: none;">Logo</h3>
				<div class="form-group">

					<input style="display: none;" id="imfile" type="file" value="" name="logo" class="file-input-product" onchange="preview_image(this)" placeholder="Select from computer">

				</div>
				<div class="form-group" style="margin-bottom:0px;">
					<a href="<?php echo base_url()."site_assets/stores/" . @$formData['logo']; ?>" download class="btn-sm-blue btn">Download</a>
				</div>

				<div class="form-group" style="display: none;">

					<input type="checkbox" class="border-dark-1" name="enable"> 
					<span class="paragraph-sm-grey">You can also drag and drop files.</span>

				</div>
				<!--<span class="text-danger">Validation error</span>-->
			</div> 
		</div> 


		<div class="col-md-6 text-center">
			<label for="short-description-editor" class=" heading-md-grey">Banner Image</label>
			<div id="imagePreview" style="background-image: url('<?php echo base_url() .
			"site_assets/stores/" . @$formData['banner']; ?>');background-repeat: no-repeat;background-size: contain; background-position:center;" class="nauk-info-connections block-file-upload block-border-dotted text-center">

			<h3 class="heading-sm-grey text-center" style="display: none;">Banner Image</h3>
			<div class="form-group">

				<input style="display: none;" id="bannerfile" type="file" value="" name="banner" class="file-input-product" onchange="preview_image_banner(this)" placeholder="Select from computer">

			</div>
			<div class="form-group" style="margin-bottom:0px;">
				<a href="<?php echo base_url()."site_assets/stores/" . @$formData['banner']; ?>" download class="btn-sm-blue btn">Download</a>
			</div>

			<div class="form-group" style="display: none;">

				<input type="checkbox" class="border-dark-1" name="enable"> 
				<span class="paragraph-sm-grey">You can also drag and drop files.</span>

			</div>

		</div> 
	</div> 
	<br><br><br>
	<div class="col-md-12 editor ">
		<br><br><br>
		<label for="short-description-editor" class=" heading-md-grey">Banner Description</label>
		<div class="form-group">
			<textarea name="bannerText"  maxlength="3000" rows="10" class="form-control form-textarea "><?php echo $formData['bannerText']; ?></textarea>
		</div>
	</div> 

	<div class="col-md-12 editor ">
		<label for="short-description-editor" class=" heading-md-grey">Welcome Description</label>
		<div class="form-group">
			<textarea name="selcome"  maxlength="3000" rows="10" class="form-control form-textarea"><?php echo $formData['welcome']; ?></textarea> 
		</div>
	</div>  

	<div class="col-md-12 editor ">
		<label for="short-description-editor" class=" heading-md-grey">About us Description</label>
		<div class="form-group">
			<textarea name="about"  maxlength="3000" rows="10" class="form-control form-textarea "><?php echo $formData['about']; ?></textarea> 
		</div>
	</div> 


</div>
<div class="col-md-12 inner-body-head"><!-- full block start-->
	<div class="nauk-info-connections">
		<div class="page-header">
			<div class="pull-left">
				<h2 class="heading-lg-green">Featured Products</h2>
			</div>
			<div class="pull-right">
				<a target="_blank" href="<?php echo $store['store_url']; ?>" class="btn-form btn"><?php echo $store['store_name']; ?></a>

			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div><!-- block end-->


<div class="row">
	<div class="col-md-12 inner-body-head"><!-- full block start-->

		<table id="tbl_articles" class="table table-bordered table-striped" style="width:100%">
			<thead>
				<tr>
					<th>Product ID</th>
					<!--<th>label</th>-->
					<th>Product name</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php  foreach($products as $product): ?>
					<tr>
						<td><?php echo $product['product_code']; ?></td>
						<td><?php echo $product['product_name']; ?></td>

						<td class="actions"><a href="<?php echo base_url('Customization/customize_product/'.encode($product['id']).'/'.encode($requestID)); ?>" class="btn default-btn-grey">Update Product</a> </td>
					</tr>
				<?php endforeach;?>
				<!-- <input  type="text" name="requestID" value="<?php echo $requestID;?>"> -->
			</tbody>
		</table>
	</div>
</div>

</div>
</div>

<!-- <script type="text/javascript">
	$(document).ready(function(){
		$('#tbl_articles').DataTable({
			"pagingType": "full_numbers",
			"ordering": false,
			"searching": false,
			"bLengthChange": false,
	        //"bInfo": false,
	        "dom": '<"top"i>rt<"bottom"flp><"clear">'
	    });
	});
</script> -->