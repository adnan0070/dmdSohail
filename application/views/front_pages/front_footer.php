
<div class="container-fluid home-container-full">
	<?php $store = $this->db->query('select * from settings');
	$store = $store->row_array(); ?>


	<div class="row footer">
		<div class="col-md-6  footer-left max-auto">
			<div class="nauk-info-connections text-left">
				<h4>About <?php echo $store['footer_left_heading']; ?> <!--About Digital Media Deliveries--></h4>
				<p><?php echo $store['footer_left_text']; ?>
			</p>
			<p>Copyright &copy; 2018 – Present Digital Media Deliveries. All Rights Reserved.</p>
		</div>
	</div>

	<div class="col-md-6  footer-right max-auto">
		<?php $this->load->view('front_pages/all_footer'); ?>
	</div>
</div>
</div>
<script>
	AOS.init({
   once: true
})

	$(document).ready(function() {
		$('.owl-carousel').owlCarousel({
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			loop: true,
			margin: 0,
			responsiveClass: true,
			nav: true,
			loop: true,
			stagePadding: 0,
			responsive: {
				0: {
					items: 1
				},
				640: {
					items: 2
				},
				768: {
					items: 3
				},
				992: {
					items: 4
				}
			}
		});
	});
</script>
</body>
</html>