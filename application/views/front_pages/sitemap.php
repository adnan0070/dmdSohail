	<!-- sitemap heading-->
	<div class="container-fluid main-container">
		<div class="row row-500">
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="section-content-block text-center">
					<div class="head"	>
						<h1 class="heading-level-1">Site Map</h1>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- sitemap heading-->


	<!-- sitemap chart-->
	<div class="container-fluid container-pad">
		<!-- blocks row 1 -->
		<div class="row row-blocks-1 row-stretched">
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-pad">
				<div class="blocks-row-1 text-center">
					<ul class="list-inline block-ul">
						<li class="list-inline-item item-li">
							<div class="block block-level-1 middle-block">
								<a href="<?php echo base_url(); ?>">
									<div class="block-head head-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-15 link-text vertical-middle-items">Home</h5>
									</div>
								</a>
							</div>
						</li>
						<li class="list-inline-item item-li">
							<div class="block block-level-1 middle-block">
								<a href="<?php echo base_url('pages/front/about_us');?>">
									<div class="block-head head-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-15 link-text vertical-middle-items">About Us</h5>
									</div>
								</a>
							</div>
						</li>
						<li class="list-inline-item item-li">
							<div class="block block-level-1 middle-block">
								<a href="<?php echo base_url('blog/features');?>">
									<div class="block-head head-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-15 link-text vertical-middle-items">Features</h5>
									</div>
								</a>
							</div>
						</li>
						<li class="list-inline-item item-li">
							<div class="block block-level-1 middle-block">
								<a href="<?php echo base_url('user/resources');?>">
									<div class="block-head head-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-15 link-text vertical-middle-items">Resources</h5>
									</div>
								</a>
							</div>
						</li>
						<li class="list-inline-item item-li">
							<div class="block block-level-1 middle-block">
								<a href="<?php echo base_url('user/login');?>">
									<div class="block-head head-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-15 link-text vertical-middle-items">Login</h5>
									</div>
								</a>
							</div>
						</li>
						<li class="list-inline-item item-li">
							<div class="block block-level-1 middle-block">
								<a href="<?php echo base_url('user/pricing');?>">
									<div class="block-head head-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-15 link-text vertical-middle-items">Pricing</h5>
									</div>
								</a>
							</div>
						</li>
						<li class="list-inline-item item-li">
							<div class="block block-level-1 middle-block">
								<a href="<?php echo base_url('user/demo');?>">
									<div class="block-head head-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-15 link-text vertical-middle-items">Demo</h5>
									</div>
								</a>
							</div>
						</li>
						<li class="list-inline-item item-li">
							<div class="block block-level-1 middle-block">
								<a href="<?php echo base_url('blog/blogs_with_categories') ?>">
									<div class="block-head head-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-15 link-text vertical-middle-items">Blog</h5>
									</div>
								</a>
							</div>
						</li>
						<li class="list-inline-item item-li">
							<div class="block block-level-1 middle-block">
								<a href="<?php echo base_url('user/register') ?>">
									<div class="block-head head-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-15 link-text vertical-middle-items">Get Started</h5>
									</div>
								</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- end blocks row 1 -->
		<!-- connection row 1 -->
		<div class="row row-stretched text-center">
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="connection-1 text-center">
					<div class="vertical-line circle"></div>
					<div class="horizontal-line horizontal-line-lg"></div>
				</div>
			</div>
		</div>
		<!-- connection row 1 -->
		<!-- connection row 2 -->
		<div class="row row-connection-1 row-stretched text-center">
			<div class="pull-left col-1 col-xs-1 col-sm-1 col-md-1 col-lg-1">
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<div class="conection-2 text-center">
					<div class="vertical-line sort-down"></div>
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<div class="conection-2 text-center">
					<div class="vertical-line sort-down"></div>
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 dashboard-connection">
				<div class="conection-2 text-center">
					<div class="vertical-line sort-down"></div>
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<div class="conection-2 text-center">
					<div class="vertical-line sort-down"></div>
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<div class="conection-2 text-center">
					<div class="vertical-line sort-down"></div>
				</div>
			</div>
			<div class="pull-left col-1 col-xs-1 col-sm-1 col-md-1 col-lg-1">
			</div>
		</div>
		<!-- connection row 2 -->
	</div>

	<div class="container-fluid">
		<!-- blocks row 2 -->
		<div class="row row-blocks-2 row-stretched">
			<div class="pull-left col-1 col-xs-1 col-sm-1 col-md-1 col-lg-1">
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 dash-col-pad">
				<div class="block block-level-2 middle-block">
					<a href="#">
						<div class="block-head head-purple text-left ">
							<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
								<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
							</svg>
						</div>
						<div class="block-content v-middle-main ">
							<h5 class="link-text text-13 vertical-middle-items">Store 1</h5>
						</div>
					</a>
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 dash-col-pad">
				<div class="block block-level-2 middle-block">
					<a href="#">
						<div class="block-head head-purple text-left ">
							<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
								<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
							</svg>
						</div>
						<div class="block-content v-middle-main ">
							<h5 class="link-text text-13 vertical-middle-items">Store</h5>
						</div>
					</a>

				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 dash-col-pad">
				<div class="block block-level-2 middle-block">
					<a href="#">
						<div class="block-head head-purple text-left ">
							<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
								<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
							</svg>
						</div>
						<div class="block-content v-middle-main ">
							<h5 class="link-text text-13 vertical-middle-items">Dashboard</h5>
						</div>
					</a>
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 dash-col-pad">
				<div class="block block-level-2 middle-block">
					<a href="#">
						<div class="block-head head-purple text-left ">
							<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
								<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
							</svg>
						</div>
						<div class="block-content v-middle-main ">
							<h5 class="link-text text-13 vertical-middle-items">Help</h5>
						</div>
					</a>
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 dash-col-pad">
				<div class="block block-level-2 middle-block">
					<a href="#">
						<div class="block-head head-purple text-left ">
							<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
								<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
							</svg>
						</div>
						<div class="block-content v-middle-main ">
							<h5 class="link-text text-13 vertical-middle-items">User</h5>
						</div>
					</a>
				</div>
			</div>
			<div class="pull-left col-1 col-xs-1 col-sm-1 col-md-1 col-lg-1">
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- end blocks row 2 -->
		<!-- connection row 3 -->
		<div class="row row-connection-1 row-stretched text-center">
			<div class="pull-left col-1 col-xs-1 col-sm-1 col-md-1 col-lg-1">
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<div class="conection-2 text-center">
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 padding-17">
				<!-- <div class="store-down-connection text-center">
					<img src="assets/images/img-line-6.png">
				</div> -->
				<div class="row store-down-connection store-start-linkage">
					<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class=" text-center">
							<div class="vertical-line circle"></div>
							<div class="horizontal-line horizontal-line-sm"></div>
						</div>
					</div>
					<!-- <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="vertical-line-sm circle-lg"></div>
						<div class="horizontal-line horizontal-line-sm"></div>
					</div> -->
					<div class="pull-left col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="vertical-line vertical-line-xs sort-down"></div>
					</div>
					<div class="pull-left col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="vertical-line vertical-line-xs sort-down"></div>
					</div>
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<div class="conection-2 text-center">
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<!-- <div class="conection-2 text-center">
					<div class="vertical-line sort-down"></div>
				</div> -->
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<!-- <div class="conection-2 text-center">
					<div class="vertical-line sort-down"></div>
				</div> -->
			</div>
			<div class="pull-left col-1 col-xs-1 col-sm-1 col-md-1 col-lg-1">
			</div>
		</div>
		<!-- end connection row 3 -->

		<!-- blocks row 3  -->
		<div class="row row-blocks-3 row-stretched text-center">
			<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@22 -->
			<!-- 3rd store-1 down link blocks -->
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 store-1-blocks padding-0">
				<div class="block-conection-3 text-center">
					<div class="block block-level-3 middle-block">
						<a href="#">
							<div class="block-head head-grey text-left ">
								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
									<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								</svg>
							</div>
							<div class="block-content v-middle-main ">
								<h5 class="link-text text-10 vertical-middle-items">Home</h5>
							</div>
						</a>
					</div>
					<div class="arrow-left text-center">
						<!-- <img src="assets/images/img-line-4.png"> -->
						<div class="line-to-left"></div>
					</div>
					<div class="circle-lg line-lg"></div>
				</div>
				<!-- row 3 vertical block 2 -->
				<div class="block-conection-3 text-center">
					<div class="block block-level-3 middle-block">
						<a href="#">
							<div class="block-head head-grey text-left ">
								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
									<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								</svg>
							</div>
							<div class="block-content v-middle-main ">
								<h5 class="link-text text-10 vertical-middle-items">Products</h5>
							</div>
						</a>
					</div>
					<div class="arrow-left text-center">
						<!-- <img src="assets/images/img-line-4.png"> -->
						<div class="line-to-left"></div>
					</div>
					<div class=" line-lg"></div>
				</div>
				<!-- row 3 vertical block 3 -->
				<div class="block-conection-3 text-center">
					<div class="block block-level-3 middle-block">
						<a href="#">
							<div class="block-head head-grey text-left ">
								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
									<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								</svg>
							</div>
							<div class="block-content v-middle-main ">
								<h5 class="link-text text-10 vertical-middle-items">Cart</h5>
							</div>
						</a>
					</div>
					<div class="arrow-left text-center">
						<!-- <img src="assets/images/img-line-4.png"> -->
						<div class="line-to-left"></div>
					</div>
					<div class=" line-lg"></div>
				</div>
				<!-- row 3 vertical block 4 -->
				<div class="block-conection-3 text-center">
					<div class="block block-level-3 middle-block">
						<a href="#">
							<div class="block-head head-grey text-left ">
								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
									<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								</svg>
							</div>
							<div class="block-content v-middle-main ">
								<h5 class="link-text text-10 vertical-middle-items">About Us</h5>
							</div>
						</a>
					</div>
					<div class="arrow-left text-center">
						<!-- <img src="assets/images/img-line-4.png"> -->
						<div class="line-to-left"></div>
					</div>
					<div class=" line-lg"></div>
				</div>


				<div class="row">
					<!-- row 3 vertical block 5 -->
					<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="block-conection-3 text-center">
							<div class="block block-level-3 middle-block">
								<a href="#">
									<div class="block-head head-grey text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-10 vertical-middle-items">Login</h5>
									</div>
								</a>
								<!-- <div class="vertical-line-sm circle-lg"></div> -->
							</div>
							<div class="arrow-left text-center">
								<!-- <img src="assets/images/img-line-4.png"> -->
								<div class="line-to-left"></div>
							</div>
							<div class=" line-lg login-vertical-line"></div>
						</div>
					</div>
				</div>
				<div class="row start-linkage">
					<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="vertical-line-sm circle-lg"></div>
						<div class="horizontal-line horizontal-line-sm"></div>
					</div>
					<div class="pull-left col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="vertical-line vertical-line-xs sort-down"></div>
					</div>
					<div class="pull-left col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="vertical-line vertical-line-xs sort-down"></div>
					</div>
				</div>
				<div class="row store-1-block-twice">
					<!-- row 3 vertical block 6 and 7 -->
					<div class="pull-left col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="block-conection-3-inner text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-blue text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-8 vertical-middle-items">ORDER DETAL</h5>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="pull-left col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="block-conection-3-inner text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-blue text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-8 vertical-middle-items">PURCHASES</h5>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end 3rd store-1 down link blocks -->


			<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

			<div class="pull-left col-1 col-xs-1 col-sm-1 col-md-1 col-lg-1">
			</div>
			<!-- 3rd store down link blocks -->
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 store-blocks padding-0">
				<div class="row store-block-twice">
					<!-- store vertical block 1 and 2 -->
					<div class="pull-left col-5 col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-0">
						<div class="block-conection-3 text-center">
							<div class="block block-level-3 middle-block">
								<a href="#">
									<div class="block-head head-grey text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-10 vertical-middle-items">Create</h5>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 padding-0">
						<div class="edit-store-down-connection">
							<!-- <img src="assets/images/img-line-5.png"> -->
							<div class="left-down"></div>
						</div>
					</div>
					<div class="pull-left col-5 col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-0">
						<div class="block-conection-3 text-center">
							<div class="block block-level-3 middle-block">
								<a href="#">
									<div class="block-head head-grey text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-10 vertical-middle-items">Edit Store</h5>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!-- ================================start===================================== -->
				<div class="row edit-store-link-block">
					<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-0">
						<div class="block-conection-3 text-center">
							<div class="block block-level-3 middle-block">
								<a href="#">
									<div class="block-head head-grey text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-10 vertical-middle-items">Sales</h5>
									</div>
								</a>
							</div>
						</div>
					</div>
					<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
					<div class="pull-left col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 padding-0">
						<!-- row 3 vertical block 2 -->
						<div class="block-conection-3 sales-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">PURCHASES</h5>
									</div>
								</a>
							</div>
							<div class="arrow-left text-center">
								<!-- <img src="assets/images/img-line-4.png"> -->
								<div class="line-to-left"></div>
							</div>
							<div class="circle-lg  line-lg"></div>
						</div>
						<!-- row 3 vertical block 3 -->
						<div class="block-conection-3 sales-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">CUSTOMERS</h5>
									</div>
								</a>
							</div>
							<div class="arrow-left text-center">
								<!-- <img src="assets/images/img-line-4.png"> -->
								<div class="line-to-left"></div>
							</div>
							<div class=" line-lg"></div>
						</div>
						<!-- row 3 vertical block 4 -->
						<div class="block-conection-3 sales-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">IMPORT CUSTOMERS</h5>
									</div>
								</a>
							</div>
							<div class="arrow-left text-center">
								<!-- <img src="assets/images/img-line-4.png"> -->
								<div class="line-to-left"></div>
							</div>
							<div class=" line-lg"></div>
						</div>
						<!-- row 3 vertical block 4 -->
						<div class="block-conection-3 sales-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">REPORTS</h5>
									</div>
								</a>
							</div>
							<div class="arrow-left text-center">
								<!-- <img src="assets/images/img-line-4.png"> -->
								<div class="line-to-left"></div>
							</div>
							<div class="login-vertical-line line-lg"></div>
						</div>
					</div>
					<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
					<!-- ----------------------------------------------------------------------------------------- -->
					<div class="pull-left col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 padding-0 sales-col edit-store-block-col">
						<!-- row 3 vertical block 1 -->
						<div class="block-conection-3 edit_store-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">INTEGRATION</h5>
									</div>
								</a>
							</div>
							<div class="arrow-right text-center">
								<div class="line-to-right"></div>
							</div>
							<div class="circle-lg  line-lg"></div>
						</div>
						<!-- row 3 vertical block 2 -->
						<div class="block-conection-3 edit_store-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">PREFERENCES</h5>
									</div>
								</a>
							</div>
							<div class="arrow-right text-center">
								<div class="line-to-right"></div>
							</div>
							<div class=" line-lg"></div>
						</div>
						<!-- row 3 vertical block 3 -->
						<div class="block-conection-3 edit_store-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">COUPONS</h5>
									</div>
								</a>
							</div>
							<div class="arrow-right text-center">
								<div class="line-to-right"></div>
							</div>
							<div class=" line-lg"></div>
						</div>
						<!-- row 3 vertical block 4 -->
						<div class="block-conection-3 edit_store-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">PRODUCT</h5>
									</div>
								</a>
							</div>
							<div class="arrow-right text-center">
								<div class="line-to-right"></div>
							</div>
							<div class=" line-lg"></div>
						</div>

						<!-- row 3 vertical block 5 -->
						<div class="block-conection-3 edit_store-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">TAX</h5>
									</div>
								</a>
							</div>
							<div class="arrow-right text-center">
								<div class="line-to-right"></div>
							</div>
							<div class="line-lg"></div>
						</div>
						<!-- row 3 vertical block 6 -->
						<div class="block-conection-3 edit_store-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">PAYMENT METHODS</h5>
									</div>
								</a>
							</div>
							<div class="arrow-right text-center">
								<div class="line-to-right"></div>
							</div>
							<div class=" line-lg"></div>
						</div>
						<!-- row 3 vertical block 7 -->
						<div class="block-conection-3 edit_store-down-blocks text-center">
							<div class="block block-level-4 middle-block">
								<a href="#">
									<div class="block-head head-light-orange text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="link-text text-9 vertical-middle-items">LOOK & FEEL</h5>
									</div>
								</a>
							</div>
							<div class="arrow-right text-center">
								<div class="line-to-right"></div>
							</div>
							<div class="login-vertical-line line-lg"></div>
						</div>

					</div>
					<!-- ------------------------------------------------------------------------------------------- -->
				</div>
				<!-- =======================================end===================================== -->
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<div class="conection-2 text-center">
					
				</div>
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 sales-col help-block-col">
				<!-- ----------------------------------------------------------------------------------------- -->
				<!-- row 3 vertical block 1 -->
				<div class="block-conection-3 help-down-blocks text-center">
					<div class="block block-level-3 middle-block">
						<a href="#">
							<div class="block-head head-grey text-left ">
								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
									<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								</svg>
							</div>
							<div class="block-content v-middle-main ">
								<h5 class="link-text text-9 vertical-middle-items">KNOWLEDGE</h5>
							</div>
						</a>
					</div>
					<div class="arrow-right text-center">
						<div class="line-to-right"></div>
					</div>
					<div class="circle-lg  line-lg"></div>
				</div>
				<!-- row 3 vertical block 2 -->
				<div class="block-conection-3 help-down-blocks text-center">
					<div class="block block-level-3 middle-block">
						<a href="#">
							<div class="block-head head-grey text-left ">
								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
									<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								</svg>
							</div>
							<div class="block-content v-middle-main ">
								<h5 class="link-text text-9 vertical-middle-items">VIDEOS</h5>
							</div>
						</a>
					</div>
					<div class="arrow-right text-center">
						<div class="line-to-right"></div>
					</div>
					<div class=" line-lg"></div>
				</div>
				<!-- row 3 vertical block 3 -->
				<div class="block-conection-3 help-down-blocks text-center">
					<div class="block block-level-3 middle-block">
						<a href="#">
							<div class="block-head head-grey text-left ">
								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
									<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								</svg>
							</div>
							<div class="block-content v-middle-main ">
								<h5 class="link-text text-9 vertical-middle-items">UPDATES</h5>
							</div>
						</a>
					</div>
					<div class="arrow-right text-center">
						<div class="line-to-right"></div>
					</div>
					<div class=" line-lg"></div>
				</div>
				<!-- ------------------------------------------------------------------------------------------- -->
			</div>
			<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 sales-col help-block-col">
				<!-- ----------------------------------------------------------------------------------------- -->
				<!-- row 3 vertical block 1 -->
				<div class="block-conection-3 help-down-blocks text-center">
					<div class="block block-level-3 middle-block">
						<a href="#">
							<div class="block-head head-grey text-left ">
								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
									<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								</svg>
							</div>
							<div class="block-content v-middle-main ">
								<h5 class="link-text text-9 vertical-middle-items">SUBSCRIPTION</h5>
							</div>
						</a>
					</div>
					<div class="arrow-right text-center">
						<div class="line-to-right"></div>
					</div>
					<div class="circle-lg  line-lg"></div>
				</div>
				<!-- row 3 vertical block 2 -->
				<div class="block-conection-3 help-down-blocks text-center">
					<div class="block block-level-3 middle-block">
						<a href="#">
							<div class="block-head head-grey text-left ">
								<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
									<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
									<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
								</svg>
							</div>
							<div class="block-content v-middle-main ">
								<h5 class="link-text text-9 vertical-middle-items">HISTORY</h5>
							</div>
						</a>
					</div>
					<div class="arrow-right text-center">
						<div class="line-to-right"></div>
					</div>
					<div class=" line-lg"></div>
				</div>
				<!-- ------------------------------------------------------------------------------------------- -->
			</div>
			<div class="pull-left col-1 col-xs-1 col-sm-1 col-md-1 col-lg-1">
			</div>
		</div>
		<!--end  blocks row 3  -->
	</div>
	<div class="container-fluid green-block-container">
		<!--product green blocks  row-->
		<div class="row product-green-block-section row-stretched">
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-pad">
				<!-- <div class="row green-blocks-line">
					<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 line-padding">
						<div class="horizontal-line"></div>
					</div>
				</div> -->
				<div class="blocks-row-1 text-center product-green-blocks-row">
					<ul class="list-inline block-ul">
						<li class=" item-li">

							<div class="left-down-line"></div>
							<div class="block block-level-5 middle-block">
								<a href="#">
									<div class="block-head head-green text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-8 link-text vertical-middle-items">PURCHASES</h5>
									</div>
								</a>
							</div>
						</li>
						<li class=" item-li">
							<div class="block green-block block-level-5 middle-block">
								<a href="#">
									<div class="block-head head-green text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-8 link-text vertical-middle-items">SEND FOR DOWNLOADS</h5>
									</div>
								</a>
							</div>
						</li>
						<li class=" item-li">
							<div class="block green-block block-level-5 middle-block">
								<a href="#">
									<div class="block-head head-green text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-8 link-text vertical-middle-items">ACTION PURCHASES</h5>
									</div>
								</a>
							</div>
						</li>
						<li class=" item-li">
							<div class="block green-block block-level-5 middle-block">
								<a href="#">
									<div class="block-head head-green text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-8 link-text vertical-middle-items">EMBEDED CODES</h5>
									</div>
								</a>
							</div>
						</li>
						<li class=" item-li">
							<div class="block green-block block-level-5 middle-block">
								<a href="#">
									<div class="block-head head-green text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-8 link-text vertical-middle-items">ADD FILES</h5>
									</div>
								</a>
							</div>
						</li>
						<li class=" item-li">
							<div class="block green-block block-level-5 middle-block">
								<a href="#">
									<div class="block-head head-green text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-8 link-text vertical-middle-items">ADD EMAILS FOR CUSTOMER</h5>
									</div>
								</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- product green blocks  row -->


		<!--look & feel green blocks  row-->
		<div class="row green-block-section row-stretched">
			<div class="pull-left col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-pad">
			</div>
			<div class="pull-left col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-pad">
				<!-- <div class="row green-blocks-line">
					<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 line-padding">
						<div class="horizontal-line"></div>
					</div>
				</div> -->
				<div class="blocks-row-1 text-center green-blocks-row">
					<ul class="list-inline block-ul">
						<li class=" item-li">
							<div class="look-and-feel left-down-line"></div>
							<div class="block block-level-5 middle-block">
								<a href="#">
									<div class="block-head head-green text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-8 link-text vertical-middle-items">STORE CUSTOMIZATION</h5>
									</div>
								</a>
							</div>
						</li>
						<li class=" item-li">
							<div class="block green-block block-level-5 middle-block">
								<a href="#">
									<div class="block-head head-green text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-8 link-text vertical-middle-items">FEATURED PRODUCTS</h5>
									</div>
								</a>
							</div>
						</li>
						<li class=" item-li">
							<div class="block green-block block-level-5 middle-block">
								<a href="#">
									<div class="block-head head-green text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-8 link-text vertical-middle-items">LOGO SETUP</h5>
									</div>
								</a>
							</div>
						</li>
						<li class=" item-li">
							<div class="block green-block block-level-5 middle-block">
								<a href="#">
									<div class="block-head head-green text-left ">
										<svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
											<path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
											<path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
										</svg>
									</div>
									<div class="block-content v-middle-main ">
										<h5 class="text-8 link-text vertical-middle-items">EMAIL TEMPLATES</h5>
									</div>
								</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- look & feel green blocks  row -->
	</div>
	<!-- sitemap chart-->