
<?php $store = $this->db->query('select * from settings');
$store = $store->row_array(); ?>
<div class="container-fluid home-container-full row-background">
	<div  id="site_about" class="row home-row-1 home-row" >
		<div class="col-md-12 col-overlay col1 text-left">
			<h2 class="heading-lg-yellow" data-aos="fade-up"
			data-aos-anchor-placement="center-center">
		Ready to launch your digital Product?</h2>
		<p class="paragraph-text-sm-white text-left">
			Digital Media Deliveries gives you all the tools for managing and selling your Digital Media products to <br>
			your audience across the world.
		</p>
	</div>

	<div class="col-md-12 text-left col2" >
		<a href="<?php echo base_url('blog/features');?>" class="btn-lg-default btn">Read More</a> 
	</div>
</div>
</div>

<div class="container home-container">
	<div class="row home-row-2 home-row">
		<div class="col-md-12 col-stretched col1 col-video text-center">
			<video class="img-responsive" src="<?php echo base_url('site_assets/video/') . $store['homefile']; ?>" controls controlsList="nodownload" width="100%" style="max-width: 850px; margin-top: -6.8vw;"> </video>
		</div>
		<div class="col-md-12 text-center col2" >
			<a  href="/user/register" class="btn-lg-default btn">14 Day FREE Trial - Click Here to Sign up</a> 
		</div>

		<div class="col-md-12 text-center col-stretched col3 margin-default">
			<p class="paragraph-text-md-black-blod" >
				Welcome to Digital Media Deliveries - The one-stop site for all of a digital publishers Digital Media needs... Whether you sell digital information products, eBooks, documents, pictures, music or even videos, we are sure we can help you every step of the way. We provide secure automated Digital Media and product delivery to your customers... create UNLIMITED unique URL's and shop front's for all of your products! 
			</p>
		</div>

		<div class="col-md-12 text-center  col-stretched col2 col-specs">
			<img src="<?php echo base_url(); ?>site_assets/images/home-specs.gif">
		</div>

	</div>
</div>


<div class="container home-container">
	<div class="row home-row-3 home-row margin-default"> 

		<div class="col-md-12 text-center col-stretched col1">
			<p class="paragraph-text-md-green-blod margin-none">
				All entirely automated, so you can go about your day to day business without having to worry!  
			</p>

		</div>
		<div class="col-md-12 text-center col2" >
			<a  href="<?php echo base_url('blog/features');?>" class="btn-lg-default btn">See all Features</a> 
		</div>
	</div>

	<div class="row home-row-2 home-row row-normal" >
		<div class="col-md-12 text-center col1 col-features">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-6  block">
					<div class="nauk-info-connections text-center" >
						<div class="image"><img src="<?php echo base_url(); ?>site_assets/images/block-icon-1.png"></div>
						<h3 class="block-heading">Unlimited stores </h3>
						<p class="paragraph-text-sm-grey">
							Launch unlimited stores, each with its own unique URL - Perfect if you offer different products in different niches.
						</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-6 col-xs-6  block">
					<div class="nauk-info-connections text-center" >
						<div class="image"><img src="<?php echo base_url(); ?>site_assets/images/block-icon-2.png"></div>
						<h3 class="block-heading">Let us do the work </h3>
						<p class="paragraph-text-sm-grey">
							Simply copy and paste links into your existing website(s) and we'll do the rest! Well deliver your product straight to your customer, automatically and immediately!
						</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-6 col-xs-6  block">
					<div class="nauk-info-connections text-center" >
						<div class="image"><img src="<?php echo base_url(); ?>site_assets/images/block-icon-3.png"></div>
						<h3 class="block-heading">Host your product </h3>
						<p class="paragraph-text-sm-grey">
							We host your product without any additional cost on our dedicated servers for your customers to get the best experience. 	
						</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-6 col-xs-6  block">
					<div class="nauk-info-connections text-center">
						<div class="image"><img src="<?php echo base_url(); ?>site_assets/images/block-icon-4.png"></div>
						<h3 class="block-heading">Use our expertise </h3>
						<p class="paragraph-text-sm-grey">
							No need to worry about constant upgrades of technology updates, or programming. We take care of everything to ensure you focus on your business.
						</p>
					</div>
				</div>

				<div class="col-md-12 text-center ">
					<a href="/user/register" class="btn-lg-default btn">14 Day Trial - Click Here to Sign up</a> 
				</div>
			</div>
		</div>

	</div>
</div>

<div class="container-fluid home-container-full row-background-2">
	<div class="row home-row-3 home-row margin-default">
		<div class="col-md-12 text-center col-stretched col-overlay col1 margin-default">
			<p class="paragraph-text-md-white-blod">
				Don't sell online but have Digital Media you need to safely send to your  Clients? Not a problem, with our 'Send File(s) to Client' feature, you can quickly and easily upload any file and send to any number of recipients.
			</p>
			<p class="paragraph-text-md-white-blod" >
				Our software makes it extremely simple for you to launch your Digital Media products immediately...
			</p>
		</div>
	</div>
</div>
<div class="container home-container" >
	<div class="row home-row-3 home-row margin-default">
		<div class="col-md-12 text-center col-stretched col1 margin-default">
			<p class="paragraph-text-md-black-blod margin-none">
				Simply configure your store and profile &#10158; upload your products &#10158;  copy and paste the links to your own website OR, use our website feature with your own unique URL to sell your products and services &#10158; as soon as your customers buy, Digital Media Deliveries will automatically deliver the products to your customers... 
			</p>

		</div>
	</div>
</div>

<div class="container-fluid home-container-full row-background-2">
	<div class="row home-row-3  home-row margin-default">

		<div class="col-md-12 text-center col-stretched col-overlay col1 margin-default" >
			<p class="paragraph-text-md-white-blod margin-none">
				<br>
				Another AMAZING feature... if you have videos you only want viewing online (i.e, not downloadable) you can create your own video vault! Perfect if you have or sell taining videos....
				<br><br><br>
			</p>
		</div>

	</div>
</div>

<div class="container home-container">
	<div class="row home-row-3 home-row margin-default" >
		<div class="col-md-12 text-center col-stretched col1 margin-default">
			<p class="paragraph-text-md-green-blod margin-none">
				No matter how big or small, Digital Media Deliveries offer many different packages, to suit all business sizes and budgets.
			</p>
			<br>
			<a  href="/user/pricing" class="btn-lg-default btn">Click Here to See Pricing</a> 

		</div>
		

		<div class="col-md-12 text-center col1 col-stretched">
			<p class="paragraph-text-md-black-blod">
				Get powerful analytics for all of your stores, see your best sellers and sales for any period, instantly, in your dashboard.
			</p>
		</div>

		<!-- <div class="col-md-12 text-center  col-stretched col2 col-specs">
			<img src="<?php echo base_url(); ?>site_assets/images/home-specs.gif">
		</div> -->
		<!--<div class="col-md-12 text-center col-stretched col2">
			<p class="paragraph-text-md-green-blod">
				I’m happy for either one that matches DMD or a generic graphic.
			</p>
		</div>-->
	</div>

	<div class="row home-row-2 home-row">

		<div class="col-md-12 text-center col1">
			<a  href="/user/register" class="btn-lg-default btn">14 Day FREE Trial - Click Here to Sign up</a> 
		</div>

		<div class="col-md-12 text-center col-stretched col2 margin-default">
			<p class="paragraph-text-md-black-blod">
				Want to Boost Your Sales by up to 337%? Get your FREE copy of the No. 1 Doodle Software that enables you to Sky Rocket engagement! Create Amazing Interactive Sketch Vidoes! Click below now to get Your FREE Copy as a thank you for visiting us today :)
			</p>
		</div>
		<div class="col-md-12 text-center col1">
			<a  href="<?php echo base_url('site_assets/video/') . $store['userfile']; ?>" download class="btn-lg-default btn">Get for Free</a> 
		</div>
		<div class="col-md-12 col-stretched col3 col-video text-center" >
			<video class="img-responsive" src="<?php echo base_url('site_assets/video/') . $store['userfile']; ?>"  controlsList="nodownload" width="100%" style="max-width:850px;" id="active-video"></video> 
		</div>

		<div class="col-md-12 text-center col1">
			<a  href="/user/register" class="btn-lg-default btn">14 Day FREE Trial - Click Here to Sign up</a> 
		</div>

	</div>

	<div class="row home-row-4 row-normal row-integrations row-normal mx-auto home-row  margin-default">
		<div class="col-md-12 text-center"> 
			<h4 class="heading-sm-black"> Integrations</h4>
		</div>
		<div class="col-md-12 text-center methods col2">
			<ul class="list-inline">
				<li class="list-inline-item payment-method-img">
					<img src="<?php echo base_url(); ?>site_assets/images/Authorize-net-logo.png">
				</li>

				<li class="list-inline-item payment-method-img">
					<img src="<?php echo base_url(); ?>site_assets/images/paypal-784404_960_720.png">
				</li>

				<li class="list-inline-item payment-method-img" >
					<img src="<?php echo base_url(); ?>site_assets/images/sage_payment.png">
				</li>

				<li class="list-inline-item payment-method-img" >
					<img src="<?php echo base_url(); ?>site_assets/images/Stripe_logo,_revised_2016.png">
				</li>

				<li class="list-inline-item payment-method-img">
					<img src="<?php echo base_url(); ?>site_assets/images/2checkout-logo-vector.png">
				</li>

				<li class="list-inline-item payment-method-img" >
					<img src="<?php echo base_url(); ?>site_assets/images/MailChimp-Logo.png">
				</li>

				<li class="list-inline-item payment-method-img" >
					<img src="<?php echo base_url(); ?>site_assets/images/aweber_integration_liveagent.png">
				</li>

				<li class="list-inline-item payment-method-img"  >
					<img src="<?php echo base_url(); ?>site_assets/images/getresponse_logotype.png">
				</li>

			</ul>
		</div>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/vissense/0.10.0/vissense.min.js"></script>
<script>
	var myVideo = document.getElementById('active-video');

	var videoElementArea = VisSense(myVideo);

	var monitorBuilder = VisSense.VisMon.Builder(videoElementArea);

	monitorBuilder.on('fullyvisible', function() {
		myVideo.play();  
	});
	monitorBuilder.on('hidden', function() {
		myVideo.pause();  
	});
	var videoVisibilityMonitor = monitorBuilder.build();
	videoVisibilityMonitor.start();


</script>