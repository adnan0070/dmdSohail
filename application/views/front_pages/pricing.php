<div class="container-fluid main-container pricing-section">
	<div class="row row-500">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
			<div class="section-content-block text-center">
				<div class="head"	>
					<h1 class="heading-level-1">Pricing</h1>
				</div>
				<div class="content">
					<p class="text-level-1">Digital Media Deliveries offers pricing based on the number of products, and space for products. Choose your package.
					</p>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>

	<?php if($period =='Month'){
		$month = 'btn-active';
		$annual ='';
	}else{
		$annual = 'btn-active';
		$month ='';
	}


	?>
	<div class="row row-stretched price-tabs">
		<div class="pull-left col-12 col-sm-12 col-md-12 col-lg-12 price-tab-menu text-center">
			<div class="tab-btns">
				<div class="btn-section">
					<a href="<?php echo base_url('user/pricing/Month'); ?>" class="tab-left <?php echo $month; ?>">MONTHLY</a>
					<a href="<?php echo base_url('user/pricing/Annual'); ?>" class="tab-right btn-status <?php echo $annual; ?>">ANNUALLY</a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>

</div>
<!-- end pricing section 1 -->
<!-- pricing section 2 -->
<div class="container-fluid ">
	<div class="row slider-row">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
			<div class="owl-carousel owl-theme">
				
				<?php  foreach($packages as $package):  ?>
					<div class="item grow">
						<div class="slider-block">
							<div class="slider-head">
								<h4 class="heading-level-4"><?php echo $package['pack_title'];?></h4>
							</div>
							<div class="slider-price">
								<?php if($package['pack_discount'] != ''): ?>
									<h1 class="heading-level-2 package-price-strike"><span class="price-text-sm">$</span><?php echo $package['pack_discount'];?></h1>
									
									<?php else: ?>
										<h1 style="opacity:0;" class="heading-level-2 package-price-strike"><span class="price-text-sm">$</span><?php echo $package['pack_discount'];?></h1>
									<?php endif; ?>

									<h1 class="heading-level-2"><span class="price-text-sm">$</span><?php echo $package['pack_price'];?><span class="price-text-sm">/<?php echo $package['pack_time_period'];?></span></h1>
								</div>
								<div class="slider-detail">
									<ul class="list-inline text-left">
										<li class="text-level-1"><img src="<?php echo base_url(); ?>site_assets/images/img-tick.png"><?php echo $package['pack_products'];?> Products</li>
										<li class="text-level-1"><img src="<?php echo base_url(); ?>site_assets/images/img-tick.png"><?php echo round(($package['memory_limit']/1024),2);?> GB Space</li>
										<li class="text-level-1"><img src="<?php echo base_url(); ?>site_assets/images/img-tick.png"><?php echo $package['pack_sub_admins'];?> Sub-Admins</li>
										<?php if(($package['pack_description'] != '') && ($package['pack_description'] != '<br>')): ?>
										<li class="text-level-1"><img src="<?php echo base_url(); ?>site_assets/images/img-tick.png"><?php echo $package['pack_description'];?></li>
									<?php endif; ?>
								</ul>
							</div>
							<div class="block-btn">
								<script>
									var package = <?php echo (json_encode($package));?> ;
									
									var method = <?php echo json_encode($this->session->userdata('user_id'));?> ;
									if(method == null){
										var method = 'register';
									}else{
										var method = 'subscription';
									}
								</script>
								<!-- <a  onclick="addSubscriptionClick(package, method); return !ga.loaded;" class="btn-sm-default btn pricing-btn">Purchase</a> -->
								<a  href="<?php echo base_url('user/register/'.$package['pack_code']); ?>" class="btn-sm-default btn pricing-btn">Purchase</a>
							</div>
						</div>
					</div>	
				<?php  endforeach; //href="<?php echo base_url('user/subscription'); " ?>

			</div>
		</div> 
		<div class="clearfix"></div>
	</div>
</div>
<script>
	$(document).ready(function(){
		var packages = <?php echo (json_encode($packages));?> 
		addImpressionSubscriptionAll(packages) ;
	});
</script>
<br><br><br><br>

<!-- end pricing section 2 -->


<!-- 
<div class="row main-body home-features packages-row mx-auto ">
	<div class="col-md-12 text-center">
		
		<h3 class="heading-xl-green">Pricing</h3>
		<br>
		<p class="paragraph-text-md-grey-blod text-center">
			Digital Media Deliveries offers pricing based on the number of <br>products, and space for products. Choose your package. 
		</p>
	</div>
</div>

<div class="row bhoechie-tab-container  packages-row">
	<div class="col-md-4 col-sm-4 col-xs-4 bhoechie-tab-menu">
		<div class="list-group packages">
			<?php $count=0; foreach($packages as $package){ 

				if ($count==0){ ?>
					<a href="#" class="list-group-item active text-left package-block">
					<?php } else{
						?>
						<a href="#" class="list-group-item text-left package-block">
						<?php } ?>

						<p class="heading-md-green"><?php echo $package['pack_title'];?></p>
						<p class="heading-md-green">$<?php echo $package['pack_price'];?></p>
						<p class="paragraph-text-xs-grey-bold">
							# of Products: <?php echo $package['pack_products'];?> &nbsp;Memory allowed: <?php echo round(($package['memory_limit']/1024),2);?> GB</p>
						</a>
						<?php  $count++; } ?>

					</div>
				</div>

				<div class="col-md-8 col-sm-8 col-xs-8 bhoechie-tab package-detail">

					<?php $count=0; foreach($packages as $package){ 

						if ($count == 0){ ?>
							<div class="bhoechie-tab-content active">
							<?php } else{
								?>
								<div class="bhoechie-tab-content">
								<?php } ?>
								<div class="row">
									<div class="col-md-3 text-center image">
										<img src="<?php echo base_url(); ?>site_assets/package/<?php echo $package['pack_image'];  ?>">
									</div>

									<div class="col-md-6 text-left detail">
										<p style="margin-bottom:5px;" class="heading-md-green"><?php echo $package['pack_title'];?></p>
										<p  style="margin-bottom:5px;" class="paragraph-text-xs-grey-bold"># of Products: <?php echo $package['pack_products'];?> </p>
										<p class="paragraph-text-xs-grey-bold">Memory allowed: <?php echo round(($package['memory_limit']/1024),2);?> GB</p>

										<a class="btn-sm-default btn pricing-btn" href="<?php echo base_url('user/subscription'); ?>">Purchase Now</a>
									</div>

									<div class="col-md-3 text-right price">
										<p class="heading-md-green">$<?php echo $package['pack_price'];?><br>per month</p>
									</div>

								</div> 
								<div class="row">
									<div class="col-md-12 text-left description">
										<p class="heading-md-green">Description</p>
										<p class="paragraph-text-xs-grey-bold">
											<?php echo $package['pack_description'];?>
										</p>
									</div>

								</div> 
							</div>
							<?php  $count++; } ?>

						</div>
					</div>
					<script>
						$(document).ready(function() {
							$("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
								e.preventDefault();
								$(this).siblings('a.active').removeClass("active");
								$(this).addClass("active");
								var index = $(this).index();
								$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
								$("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
							});
						});
					</script>
					<br> <br>
					<br>
					<br> -->