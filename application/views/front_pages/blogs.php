<div class="row main-body blogs-page mx-auto ">
	<div class="col-md-12 blog-head text-left">
		<h3 class="heading-lg-green"> Our Latest Blogs</h3> 
	</div>
	<div class="col-md-9 blog-body">
		<?php foreach($categories_detail as $blog):?>
			<div class="row blog">
				<div class="col-md-3 blog-image">
					<div class="nauk-info-connections text-left blog-cat-name-section">
						<p class="blog-cat-text"> <?php echo $blog['cat_title'] ;?></p>
					</div>
					<div class="nauk-info-connections blog-image-section text-center">
						<img src="<?php echo base_url('site_assets/articles/').$blog['file'];?>">
					</div>

				</div>

				<div class="col-md-7 blog-description-section">
					<p class="blog-title">
						<?php echo $blog['art_title'] ;?>
					</p>
					<p class="blog-description">
						<?php echo $blog['meta_description'] ;?>
					</p>
				</div>
				<div class="col-md-2 blog-link-section text-center">
					<a href="<?php echo base_url('blog/blog_detail/'.$blog['id']) ?>" class="btn default-btn-green">Read more</a>
				</div>
			</div>
		<?php endforeach;?>

		<!-- Show pagination links -->
		<ul>
			<?php foreach ($links as $link) {
			//echo "<li>". $link."</li>";
				echo $link;
			} ?>
		</ul>
	</div>

	<?php if($categories){ ?>
	<div class="col-md-3 blog-left">
		<div class="row blog-categories">
			<div class="col-md-12" style="padding:0px;">
				<p class="heading-md-blue">Categories</p>
			</div>
			<div class="col-md-12" style="padding-left:25px;">
				<ul>
					<li class="<?php if (isset($all)){ echo "active";}?>">
						<a  class="hvr-bounce-to-right" href="<?php echo base_url('blog/blogs_with_categories') ?>">

							All
						</a>
					</li>
					<?php foreach($categories as $category): ?>
						<li class="<?php if ((isset($select)) && ($category['id'] == $select ))  { echo "active"; } ?>"> 
							<a href="<?php echo base_url('blog/blogs_with_categories?cat='.$category['id']) ?>">
								<?php echo $category['title']; ?>
							</a>
						</li>
					<?php endforeach;?>
				</ul>
			</div>
		</div>
	</div>
<?php }?>
</div>