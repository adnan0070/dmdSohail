<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include("./vendor/autoload.php");

class Stripegateway {
	
	public function __construct($stripe){
		
		\Stripe\Stripe::setApiKey($stripe["secret_key"]);
	}

	public function create_customer($data)
	{
		$customer = \Stripe\Customer::create(array(
			"email" => $data["email"],
			"source" => $data["stripeSource"]
		));
		return $customer->id;
	}

	public function create_customer_without_source($data)
	{
		$customer = \Stripe\Customer::create(array(
			"email" => $data["email"],
		));
		return $customer->id;
	}


	public function existing_customer($data)
	{
		$cus_id = $data["stripe_customer_id"];
		$customer = \Stripe\Customer::retrieve($cus_id);
		return ($customer) ? true : false;
	}

	public function update_source($data)
	{	
		//Updating a card source expiration date
		$src_id = $data["source_id"];
		$source = \Stripe\Source::retrieve($src_id);
		$source->card["exp_month"] = $data["exp_month"];
		$source->card["exp_year"] = $data["exp_year"];
		$source->save();
		return true;
	}

	public function charge_payment($data)
	{
		try {
			$charge = \Stripe\Charge::create(array(
				"amount" => $data["amount"], // amount in cents
				"currency" => $data["currency"],
				"source" => $data["stripeToken"],
			   	//"customer" => $data["customer_id"]
				"metadata" => [
					"order_id" => $data["order_id"],
					"customer_id" => $data["customer_id"],
					"store_id" => $data["store_id"]
				]
			));

			if($charge->status == "succeeded"){
				return $charge->id;	
			}else{
				return "failed";
			}
		}
		catch(Exception $e) {
			return 'failed';
		}	
	}	



	public function create_product($data)
	{
		try {

			$charge = \Stripe\Product::create([
				'name' => $data["name"],
				'type' => 'service',

			]);
			
			if($charge->id){
				return $charge->id;	
			}else{
				return "failed";
			}
		}
		catch(Exception $e) {
			return 'failed';
		}	
	}	


	public function create_plan($data)
	{
		try {

			$charge = \Stripe\Plan::create([
				'product'  => $data["stripe_product_id"],
				'nickname' => $data["name"],
				'interval' => $data["interval"],
				'currency' => 'usd',
				'amount'   => $data["amount"]*100,
			]);

			if($charge->id){
				return $charge->id;	
			}else{
				return "failed";
			}
		}
		catch(Exception $e) {
			return 'failed';
		}	
	}


	public function create_subscription($data)
	{
// print_r($data);die;
		try {

			$subscription = \Stripe\Subscription::create([
				'customer' => $data['customer_id'],
				'items' => [['plan' => $data['plan_id']]],
			]);	
			// print_r($subscription);die;
			if($subscription->id){
				$res['status'] = 'success';
				$res['response'] = $subscription;
				return $res;
			}else{
				$res['status'] = 'failed';
				$res['response'] = '';
				return $res;
			}
		}
		catch(Exception $e) {
			// $e = json_decode($e->jsonBody, true);
			// print_r($e);die;
			$res['status'] = 'failed';
			$res['response'] = 'No Payment resource found.';
			return $res;
		}	
	}



}